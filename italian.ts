<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>AvionicsMenu</name>
    <message>
        <source>Glide - </source>
        <translation type="vanished">Efficienza - </translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="77"/>
        <source>TurnOff a. land - </source>
        <translation>Spegni dopo. atterr. - </translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="85"/>
        <source>IGC Name - </source>
        <translation>Nome IGC</translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="93"/>
        <source>Dist. mode - </source>
        <translation>Calcolo dist.</translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="100"/>
        <source>Pitot comp. - </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="108"/>
        <source>Pitot TEC - </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="116"/>
        <source>Pitot avg. - </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="123"/>
        <source>Trim speed - </source>
        <translation>Velocita a trim - </translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="138"/>
        <source>SET NUM. SAMPLES</source>
        <translation>SET CAMPIONI</translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="140"/>
        <source>SET VARIO AVERAGE</source>
        <translation>SET MEDIA VARIO</translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="142"/>
        <source>WIND</source>
        <translation>VENTO</translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="145"/>
        <source>GLIDE NUM. AVERAGE</source>
        <translation>EFFICIENZA MEDIA</translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="147"/>
        <source>THERMALING DETECT</source>
        <translation>RILEVA TERMICA</translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="149"/>
        <source>BACK</source>
        <translation>--INDIETRO--</translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="261"/>
        <source>Pressure coefficient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="330"/>
        <source>Set IGC naming mode. STD for name with date and location and COMP for competition naming with pilot name.</source>
        <translation>Imposta denominazione file IGC: STD per nome con data e decollo , COMP per denominazione con nome pilota</translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="332"/>
        <source>Set distance computation method for task navigation.</source>
        <translation>Imposta il metodo di calcolo della distanza per la navigazione delle Task</translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="344"/>
        <source>Activate/deactivate raw logging. WARNING! High space consumming.</source>
        <translation>Attiva/disattiva registrazione RAW. ATTENZIONE! Consumo di spazio memoria elevato.</translation>
    </message>
    <message>
        <source>Glide</source>
        <translation type="vanished">Efficienza</translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="244"/>
        <source>Trim Speed</source>
        <translation>Velocita a trim</translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="278"/>
        <source>TEC coefficient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="295"/>
        <source>Pitot average</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="334"/>
        <source>Set average for fast vario. 1s = 201 samples. Not used for instant vario.</source>
        <translation>Set media per vario veloce. 1s = 201 campioni. Non usato per vario instantaneo.</translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="336"/>
        <source>Set integration time for average climb.</source>
        <translation>Set tempo integrazione per media salita.</translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="338"/>
        <source>Modify wind algorithm parameters.</source>
        <translation>Modifica parametri algoritmo vento.</translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="340"/>
        <source>Set number of samples for glide algorithm.</source>
        <translation>Set numero di campioni per algoritmo efficienza.</translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="342"/>
        <source>Activate/deactivate and set parameters for thermaling detect.</source>
        <translation>Attiva/disattiva e imposta parametri per rilevamento termica.</translation>
    </message>
</context>
<context>
    <name>BTDeviceActionMenu</name>
    <message>
        <location filename="menu/general/BTDeviceAction.cpp" line="61"/>
        <source>PAIR</source>
        <translation>ACCOPPIA</translation>
    </message>
    <message>
        <location filename="menu/general/BTDeviceAction.cpp" line="64"/>
        <source>CONNECT</source>
        <translation>CONNETTI</translation>
    </message>
    <message>
        <location filename="menu/general/BTDeviceAction.cpp" line="67"/>
        <source>REMOVE</source>
        <translation>RIMUOVI</translation>
    </message>
    <message>
        <location filename="menu/general/BTDeviceAction.cpp" line="70"/>
        <source>SET AS REMOTE</source>
        <translation>IMPOSTA COME TELECOMANDO</translation>
    </message>
    <message>
        <source>SET AS BNEP</source>
        <translation type="vanished">IMPOSTA COME BNEP</translation>
    </message>
    <message>
        <location filename="menu/general/BTDeviceAction.cpp" line="73"/>
        <source>BACK</source>
        <translation>--INDIETRO--</translation>
    </message>
    <message>
        <location filename="menu/general/BTDeviceAction.cpp" line="100"/>
        <location filename="menu/general/BTDeviceAction.cpp" line="107"/>
        <location filename="menu/general/BTDeviceAction.cpp" line="116"/>
        <location filename="menu/general/BTDeviceAction.cpp" line="124"/>
        <location filename="menu/general/BTDeviceAction.cpp" line="150"/>
        <location filename="menu/general/BTDeviceAction.cpp" line="154"/>
        <source>INFO</source>
        <translation>INFO</translation>
    </message>
    <message>
        <location filename="menu/general/BTDeviceAction.cpp" line="100"/>
        <source>Pair started</source>
        <translation>Accoppiamento avviato</translation>
    </message>
    <message>
        <location filename="menu/general/BTDeviceAction.cpp" line="107"/>
        <source>Connect started</source>
        <translation>Connessione avviata</translation>
    </message>
    <message>
        <location filename="menu/general/BTDeviceAction.cpp" line="116"/>
        <source>Remove started</source>
        <translation>Rimozione avviata</translation>
    </message>
    <message>
        <location filename="menu/general/BTDeviceAction.cpp" line="124"/>
        <source>Remote set</source>
        <translation>Telecomando impostato</translation>
    </message>
    <message>
        <source>BNEP set</source>
        <translation type="vanished">BNEP impostato</translation>
    </message>
    <message>
        <location filename="menu/general/BTDeviceAction.cpp" line="150"/>
        <source>Device paired</source>
        <translation>Dispositivo accoppiato</translation>
    </message>
    <message>
        <location filename="menu/general/BTDeviceAction.cpp" line="154"/>
        <source>Device unpaired</source>
        <translation>Dispositivo disaccoppiato</translation>
    </message>
</context>
<context>
    <name>BTDeviceMenu</name>
    <message>
        <location filename="menu/general/BTDevice.cpp" line="71"/>
        <source>VISIBLE ON</source>
        <translation>VISIBILITA&apos; ON</translation>
    </message>
    <message>
        <location filename="menu/general/BTDevice.cpp" line="75"/>
        <source>VISIBLE OFF</source>
        <translation>VISIBILITA&apos; OFF</translation>
    </message>
    <message>
        <location filename="menu/general/BTDevice.cpp" line="82"/>
        <source>SCAN ON</source>
        <translation>SCAN ON</translation>
    </message>
    <message>
        <location filename="menu/general/BTDevice.cpp" line="86"/>
        <source>SCAN OFF</source>
        <translation>SCAN OFF</translation>
    </message>
    <message>
        <location filename="menu/general/BTDevice.cpp" line="90"/>
        <source>REMOVE REMOTE</source>
        <translation>RIMUOVI TELECOMANDO</translation>
    </message>
    <message>
        <location filename="menu/general/BTDevice.cpp" line="92"/>
        <location filename="menu/general/BTDevice.cpp" line="179"/>
        <source>BACK</source>
        <translation>--INDIETRO--</translation>
    </message>
    <message>
        <location filename="menu/general/BTDevice.cpp" line="99"/>
        <source>NULL</source>
        <translation></translation>
    </message>
    <message>
        <location filename="menu/general/BTDevice.cpp" line="157"/>
        <source>INFO</source>
        <translation>INFO</translation>
    </message>
    <message>
        <location filename="menu/general/BTDevice.cpp" line="157"/>
        <source>Remote removed</source>
        <translation>Telecomando rimosso</translation>
    </message>
    <message>
        <location filename="menu/general/BTDevice.cpp" line="175"/>
        <source>Set MipFly visibility over bluetooth for outside connectivity.</source>
        <translation>Imposta visibilita&apos; bluetooth Mipfly per connessione esterna</translation>
    </message>
    <message>
        <location filename="menu/general/BTDevice.cpp" line="177"/>
        <source>Toggle BT devices scan mode.</source>
        <translation>Attiva o disattiva la modalità di scansione dei dispositivi BT.</translation>
    </message>
    <message>
        <location filename="menu/general/BTDevice.cpp" line="189"/>
        <source>paired</source>
        <translation>Accoppiato</translation>
    </message>
    <message>
        <location filename="menu/general/BTDevice.cpp" line="193"/>
        <source>not paired</source>
        <translation>Non accoppiato</translation>
    </message>
</context>
<context>
    <name>BTServer</name>
    <message>
        <location filename="utils/BTServer.cpp" line="75"/>
        <source>Bt Chat Server</source>
        <translation></translation>
    </message>
    <message>
        <location filename="utils/BTServer.cpp" line="77"/>
        <source>Example bluetooth chat server</source>
        <translation>Esempio di server chat bluetooth</translation>
    </message>
    <message>
        <location filename="utils/BTServer.cpp" line="78"/>
        <source>qt-project.org</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>BootloaderMenu</name>
    <message>
        <location filename="menu/bootloader/bootloadermenu.cpp" line="48"/>
        <source>FLASH STANDARD</source>
        <translation>FLASH STANDARD</translation>
    </message>
    <message>
        <location filename="menu/bootloader/bootloadermenu.cpp" line="51"/>
        <source>FLASH INSTANT</source>
        <translation>FLASH INSTANT</translation>
    </message>
    <message>
        <location filename="menu/bootloader/bootloadermenu.cpp" line="53"/>
        <source>BACK</source>
        <translation>--INDIETRO--</translation>
    </message>
    <message>
        <location filename="menu/bootloader/bootloadermenu.cpp" line="95"/>
        <source>Load the standard firmware based only on pressure data.</source>
        <translation>Carica il firmware standard basato solo sui dati barometrici</translation>
    </message>
    <message>
        <location filename="menu/bootloader/bootloadermenu.cpp" line="97"/>
        <source>Load the instant vario firmware based on pressure + accelerometer.</source>
        <translation>Carica il firmware vario istantaneo basato sui dati barometrici + accelerometro</translation>
    </message>
</context>
<context>
    <name>CaptionBox</name>
    <message>
        <location filename="ui/captionbox.ui" line="14"/>
        <source>Form</source>
        <translation>Modulo</translation>
    </message>
</context>
<context>
    <name>ChooseInput</name>
    <message>
        <location filename="ChooseInput.ui" line="14"/>
        <source>Dialog</source>
        <translation>s</translation>
    </message>
</context>
<context>
    <name>Compass</name>
    <message>
        <location filename="compass.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ConnectivityMenu</name>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="74"/>
        <source>BLUETOOTH</source>
        <translation>BLUETOOTH</translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="76"/>
        <source>REMOTE</source>
        <translation>TELECOMANDO</translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="78"/>
        <source>WIFI</source>
        <translation>WIFI</translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="80"/>
        <source>ACCOUNT</source>
        <translation></translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="85"/>
        <source>GPSPostscale - NAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="88"/>
        <source>GPSPostscale - </source>
        <translation>GPS Freq.BT - </translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="94"/>
        <source>VarioPostscale - NAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="97"/>
        <source>VarioPostscale - </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="104"/>
        <source>Live ON</source>
        <translation>Live Track ON</translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="105"/>
        <source>Live OFF</source>
        <translation>Live Track OFF</translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="108"/>
        <source>ACTIVATE TTY</source>
        <translation>ATTIVA TTY</translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="110"/>
        <source>BACK</source>
        <translation>--INDIETRO--</translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="162"/>
        <location filename="menu/general/connectivityMenu.cpp" line="274"/>
        <location filename="menu/general/connectivityMenu.cpp" line="279"/>
        <source>INFO</source>
        <translation>INFO</translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="162"/>
        <source>Check Conn</source>
        <translation>Verifica Conn.</translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="178"/>
        <source>TTY error</source>
        <translation>Errore TTY</translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="178"/>
        <source>Active</source>
        <translation>Attiva</translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="182"/>
        <source>TTY mode</source>
        <translation>Modo TTY</translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="182"/>
        <source>ACTIVATED</source>
        <translation>ATTIVATO</translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="196"/>
        <source>GPS Postscale</source>
        <translation>GPS Freq.BT</translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="212"/>
        <source>Vario Postscale</source>
        <translation>Vario Freq.BT</translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="243"/>
        <source>No Bluetooth available.</source>
        <translation>Bluetooth non disponibile</translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="247"/>
        <source>Scan, pair and unpair bluetooth devices.</source>
        <translation>Scansione, accoppiamento e disaccoppiamento dispositivi bluetooth</translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="250"/>
        <source>Set remote settings and map keys.</source>
        <translation>Impostazioni telecomando e suoi pulsanti</translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="252"/>
        <source>Set variometer data postscaler for BT transmission. 1 is equivalent to one message every 100ms. 2 -&gt; 200ms etc</source>
        <translation>Imposta frequenza dati barometrici  per la trasmissione BT: 1 equivale ad un messaggio ogni 100ms. 2 -&gt; 400ms e così via</translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="254"/>
        <source>Set GPS data postscaler for BT transmission. 1 is equivalent to one message every 200ms. 2 -&gt; 400ms etc</source>
        <translation>Imposta frequenza dati GPS  per la trasmissione BT. 1 equivale ad un messaggio ogni 200ms. 2 -&gt; 400ms e così via</translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="256"/>
        <source>Enable or disable Live Tracking functionality.</source>
        <translation>Abilita o disabilita funionalità Live Tracking.</translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="274"/>
        <source>Success!</source>
        <translation>Successo!</translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="279"/>
        <source>Fail!</source>
        <translation>Fallito!</translation>
    </message>
</context>
<context>
    <name>CoordInput</name>
    <message>
        <location filename="CoordInput.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>FAINavigationMenu</name>
    <message>
        <location filename="menu/task/faiNavigationMenu.cpp" line="63"/>
        <source>Set TP1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/faiNavigationMenu.cpp" line="65"/>
        <source>Set TP2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/faiNavigationMenu.cpp" line="67"/>
        <source>Det Base</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/faiNavigationMenu.cpp" line="69"/>
        <source>Nav to FAI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/faiNavigationMenu.cpp" line="71"/>
        <source>Nav to Tp1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/faiNavigationMenu.cpp" line="73"/>
        <source>Zoom</source>
        <translation type="unfinished">Zoom</translation>
    </message>
    <message>
        <location filename="menu/task/faiNavigationMenu.cpp" line="75"/>
        <source>Back</source>
        <translation type="unfinished">--INDIETRO--</translation>
    </message>
    <message>
        <location filename="menu/task/faiNavigationMenu.cpp" line="118"/>
        <location filename="menu/task/faiNavigationMenu.cpp" line="150"/>
        <location filename="menu/task/faiNavigationMenu.cpp" line="181"/>
        <source>WARNING</source>
        <translation type="unfinished">ATTENZIONE</translation>
    </message>
    <message>
        <location filename="menu/task/faiNavigationMenu.cpp" line="119"/>
        <source>Reset TP1?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/faiNavigationMenu.cpp" line="120"/>
        <location filename="menu/task/faiNavigationMenu.cpp" line="152"/>
        <location filename="menu/task/faiNavigationMenu.cpp" line="183"/>
        <source>CANCEL</source>
        <translation type="unfinished">ANNULLA</translation>
    </message>
    <message>
        <location filename="menu/task/faiNavigationMenu.cpp" line="121"/>
        <location filename="menu/task/faiNavigationMenu.cpp" line="153"/>
        <location filename="menu/task/faiNavigationMenu.cpp" line="184"/>
        <source>CONFIRM</source>
        <translation type="unfinished">CONFERMA</translation>
    </message>
    <message>
        <location filename="menu/task/faiNavigationMenu.cpp" line="129"/>
        <location filename="menu/task/faiNavigationMenu.cpp" line="135"/>
        <location filename="menu/task/faiNavigationMenu.cpp" line="161"/>
        <location filename="menu/task/faiNavigationMenu.cpp" line="169"/>
        <source>INFO</source>
        <translation type="unfinished">INFO</translation>
    </message>
    <message>
        <location filename="menu/task/faiNavigationMenu.cpp" line="129"/>
        <location filename="menu/task/faiNavigationMenu.cpp" line="135"/>
        <source>TP1 marked!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/faiNavigationMenu.cpp" line="143"/>
        <source>ERROR</source>
        <translation type="unfinished">ERRORE</translation>
    </message>
    <message>
        <location filename="menu/task/faiNavigationMenu.cpp" line="143"/>
        <source>TP1 not marked!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/faiNavigationMenu.cpp" line="151"/>
        <source>Reset TP2?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/faiNavigationMenu.cpp" line="161"/>
        <location filename="menu/task/faiNavigationMenu.cpp" line="169"/>
        <source>TP2 marked!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/faiNavigationMenu.cpp" line="182"/>
        <source>Recompute area?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FaiAssistCompute</name>
    <message>
        <location filename="navUtils/faiassistcompute.cpp" line="91"/>
        <source>TASK INFO</source>
        <translation type="unfinished">INFO TASK</translation>
    </message>
    <message>
        <location filename="navUtils/faiassistcompute.cpp" line="91"/>
        <source>FAI AREA REACHED</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GeneralMenu</name>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="83"/>
        <source>PILOT NAME</source>
        <translation>NOME PILOTA</translation>
    </message>
    <message>
        <source>SET TIMEZONE</source>
        <translation type="vanished">IMPOSTA FUSO ORARIO</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="87"/>
        <source>BOOTLOAD</source>
        <translation></translation>
    </message>
    <message>
        <source>LANGUAGE</source>
        <translation type="vanished">LINGUA</translation>
    </message>
    <message>
        <source>REMOTE</source>
        <translation type="vanished">TELECOMANDO</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="91"/>
        <location filename="menu/general/generalMenu.cpp" line="140"/>
        <source>UPDATE</source>
        <translation>AGGIORNAMENTO</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="93"/>
        <source>USER INTERFACE</source>
        <translation>INTERFACCIA UTENTE</translation>
    </message>
    <message>
        <source>CONNECT ACCOUNT</source>
        <translation type="vanished">COLLEGA ACCOUNT</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="95"/>
        <source>MASS STORAGE</source>
        <translation>MEMORIA DI MASSA</translation>
    </message>
    <message>
        <source>Activate tty</source>
        <translation type="vanished">Attiva TTY</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="97"/>
        <source>BACK</source>
        <translation>--INDIETRO--</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="155"/>
        <location filename="menu/general/generalMenu.cpp" line="221"/>
        <location filename="menu/general/generalMenu.cpp" line="226"/>
        <location filename="menu/general/generalMenu.cpp" line="258"/>
        <location filename="menu/general/generalMenu.cpp" line="277"/>
        <source>INFO</source>
        <translation>INFO</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="155"/>
        <source>Check Conn</source>
        <translation>Verifica Conn.</translation>
    </message>
    <message>
        <source>TTY error</source>
        <translation type="vanished">Errore TTY</translation>
    </message>
    <message>
        <source>Active</source>
        <translation type="vanished">Attiva</translation>
    </message>
    <message>
        <source>TTY mode</source>
        <translation type="vanished">Modo TTY</translation>
    </message>
    <message>
        <source>ACTIVATED</source>
        <translation type="vanished">ATTIVATO</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="85"/>
        <source>LOCALIZATION</source>
        <translation>LOCALIZZAZIONE</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="89"/>
        <source>CONNECTIVITY</source>
        <translation>CONNETTIVITA&apos;</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="141"/>
        <source>TYPE</source>
        <translation>TIPO VERSIONE</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="142"/>
        <source>BETA</source>
        <translation>BETA</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="143"/>
        <source>CANCEL</source>
        <translation type="unfinished">ANNULLA</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="144"/>
        <source>STABLE</source>
        <translation>STABILE</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="163"/>
        <source>TTY mode active</source>
        <translation>Modalita TTY attiva</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="163"/>
        <source>Pls. Reboot!</source>
        <translation>Prego Riavviare!</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="169"/>
        <source>MASS STORAGE MODE</source>
        <translation>MODALITA MEMORIA DI MASSA</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="169"/>
        <source>HOLD F TO EXIT</source>
        <translation>TIENI F PER USCIRE</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="195"/>
        <source>Change the name of the owner. Used for IGC data and flight records.</source>
        <translation>Cambia il nome del proprietario. Usato per dati IGC e voli registrati</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="197"/>
        <source>Change settings like language, timezone or default units.</source>
        <translation>Cambia impostazioni quali lingua, fuso orario o unità di misura </translation>
    </message>
    <message>
        <source>Change the timezone you are in to adjust from the universal time.</source>
        <translation type="vanished">Cambia il fuso orario  rispetto a UTC</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="199"/>
        <source>Load a newer or different firmware to your device.</source>
        <translation>Carica un nuovo o diverso firmware sul dispositivo</translation>
    </message>
    <message>
        <source>Change the language of the device.</source>
        <translation type="vanished">Cambia la lingua del dispositivo</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="201"/>
        <source>Connectivity settings, Bluetooth/WiFi</source>
        <translation>Impostazioni connettività, Bluetooth/WiFi</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="203"/>
        <source>Update and select UI.</source>
        <translation>Aggiorna e seleziona UI (interfaccia utente)</translation>
    </message>
    <message>
        <source>No Bluetooth available.</source>
        <translation type="vanished">Bluetooth non disponibile</translation>
    </message>
    <message>
        <source>Scan, pair and unpair bluetooth devices.</source>
        <translation type="vanished">Scansione, accoppiamento e disaccoppiamento dispositivi bluetooth</translation>
    </message>
    <message>
        <source>Set remote settings and map keys.</source>
        <translation type="vanished">Impostazioni telecomando e suoi pulsanti</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="205"/>
        <source>Update software to latest version. Restart will be required for the changes to take place. Requires working internet connection.</source>
        <translation>Aggiorna il software all&apos;ultima versione. Perche&apos; le modifiche abbiano effetto verra&apos; richiesto Il riavvio. Indispensabile la connessione Internet</translation>
    </message>
    <message>
        <source>Pair your MipFly account wiht this device. Requires working internet connection.</source>
        <translation type="vanished">Accoppia il tuo account Mipfly con il dispositivo. Richiede connessione Internet</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="209"/>
        <source>Switch to mass storage mode for USB OTG.</source>
        <translation>Passa alla modalita&apos; memoria di massa per USB OTG.</translation>
    </message>
    <message>
        <source>Activate tty mode for USB OTG.</source>
        <translation type="vanished">Attivare modalita&apos; TTY per USB OTG.</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="221"/>
        <location filename="menu/general/generalMenu.cpp" line="226"/>
        <source>Conn OK</source>
        <translation></translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="233"/>
        <location filename="menu/general/generalMenu.cpp" line="263"/>
        <source>ERROR</source>
        <translation>ERRORE</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="233"/>
        <source>Not Connected</source>
        <translation>Non Connesso</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="252"/>
        <source>Success</source>
        <translation>Successo</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="252"/>
        <source>Powering Off!</source>
        <translation>Spegnimento!</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="258"/>
        <source>Success! Pls Reboot</source>
        <translation>Successo! Riavvia</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="263"/>
        <source>Not Updated</source>
        <translation>Non aggiornato</translation>
    </message>
    <message>
        <source>Success!</source>
        <translation type="vanished">Successo!</translation>
    </message>
    <message>
        <source>Fail!</source>
        <translation type="vanished">Fallito!</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="277"/>
        <source>Downloading</source>
        <translation>Scaricamento ...</translation>
    </message>
</context>
<context>
    <name>GlideMenu</name>
    <message>
        <location filename="menu/glideAverage/glidemenu.cpp" line="46"/>
        <source>UP</source>
        <translation>AUMENTA</translation>
    </message>
    <message>
        <location filename="menu/glideAverage/glidemenu.cpp" line="48"/>
        <source>DOWN</source>
        <translation>DIMINUISCI</translation>
    </message>
    <message>
        <location filename="menu/glideAverage/glidemenu.cpp" line="50"/>
        <source>BACK</source>
        <translation>--INDIETRO--</translation>
    </message>
</context>
<context>
    <name>GlidesMenu</name>
    <message>
        <location filename="menu/glides/glidesmenu.cpp" line="62"/>
        <source>FLIGHT</source>
        <translation>VOLO</translation>
    </message>
    <message>
        <location filename="menu/glides/glidesmenu.cpp" line="63"/>
        <source>ACTION?</source>
        <translation>AZIONE?</translation>
    </message>
    <message>
        <location filename="menu/glides/glidesmenu.cpp" line="64"/>
        <source>UPLOAD</source>
        <translation>CARICA</translation>
    </message>
    <message>
        <location filename="menu/glides/glidesmenu.cpp" line="65"/>
        <location filename="menu/glides/glidesmenu.cpp" line="102"/>
        <source>DELETE</source>
        <translation>ELIMINA</translation>
    </message>
    <message>
        <location filename="menu/glides/glidesmenu.cpp" line="66"/>
        <source>SHOW</source>
        <translation>VEDI</translation>
    </message>
    <message>
        <location filename="menu/glides/glidesmenu.cpp" line="103"/>
        <source>CONFIRM?</source>
        <translation>CONFERMI?</translation>
    </message>
    <message>
        <location filename="menu/glides/glidesmenu.cpp" line="104"/>
        <source>CANCEL</source>
        <translation>ANNULLA</translation>
    </message>
    <message>
        <location filename="menu/glides/glidesmenu.cpp" line="105"/>
        <source>OK</source>
        <translation>CONFERMA</translation>
    </message>
    <message>
        <location filename="menu/glides/glidesmenu.cpp" line="143"/>
        <source>-- BACK --</source>
        <translation>--INDIETRO--</translation>
    </message>
</context>
<context>
    <name>GoalTypeMenu</name>
    <message>
        <location filename="menu/task/goalTypeMenu.cpp" line="54"/>
        <source>LINE</source>
        <translation>LINEA</translation>
    </message>
    <message>
        <location filename="menu/task/goalTypeMenu.cpp" line="56"/>
        <source>CYLINDER ENTER</source>
        <oldsource>CILINDER ENTER</oldsource>
        <translation>INGRESSO CILINDRO </translation>
    </message>
    <message>
        <location filename="menu/task/goalTypeMenu.cpp" line="58"/>
        <source>CYLINDER EXIT</source>
        <oldsource>CILINDER EXIT</oldsource>
        <translation>USCITA CILINDRO</translation>
    </message>
</context>
<context>
    <name>IGCUploader</name>
    <message>
        <location filename="utils/igcuploader.cpp" line="67"/>
        <source>In progress!</source>
        <translation>In avanzamento!</translation>
    </message>
    <message>
        <location filename="utils/igcuploader.cpp" line="67"/>
        <location filename="utils/igcuploader.cpp" line="100"/>
        <location filename="utils/igcuploader.cpp" line="110"/>
        <location filename="utils/igcuploader.cpp" line="116"/>
        <location filename="utils/igcuploader.cpp" line="122"/>
        <location filename="utils/igcuploader.cpp" line="128"/>
        <location filename="utils/igcuploader.cpp" line="155"/>
        <source>ERROR</source>
        <translation>ERRORE</translation>
    </message>
    <message>
        <location filename="utils/igcuploader.cpp" line="100"/>
        <source>Fail 8</source>
        <translation>Errore 8</translation>
    </message>
    <message>
        <location filename="utils/igcuploader.cpp" line="110"/>
        <source>Fail 7</source>
        <translation>Errore 7</translation>
    </message>
    <message>
        <location filename="utils/igcuploader.cpp" line="116"/>
        <source>Not paired</source>
        <translation>Non accoppiato</translation>
    </message>
    <message>
        <location filename="utils/igcuploader.cpp" line="122"/>
        <source>File exists</source>
        <translation>Il file esiste</translation>
    </message>
    <message>
        <location filename="utils/igcuploader.cpp" line="128"/>
        <source>No space</source>
        <translation>Spazio insufficiente</translation>
    </message>
    <message>
        <location filename="utils/igcuploader.cpp" line="155"/>
        <source>No Internet</source>
        <translation>Internet non presente</translation>
    </message>
    <message>
        <location filename="utils/igcuploader.cpp" line="162"/>
        <source>File Sent</source>
        <translation>File inviato</translation>
    </message>
    <message>
        <location filename="utils/igcuploader.cpp" line="162"/>
        <source>INFO</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>InputLocation</name>
    <message>
        <location filename="ui/inputlocation.ui" line="14"/>
        <source>Form</source>
        <translation>Modulo</translation>
    </message>
</context>
<context>
    <name>InputNameMenu</name>
    <message>
        <location filename="menu/name/inputnamemenu.cpp" line="52"/>
        <source>INPUT</source>
        <translation>INSERISCI</translation>
    </message>
    <message>
        <location filename="menu/name/inputnamemenu.cpp" line="54"/>
        <source>CLEAR</source>
        <translation>CANCELLA</translation>
    </message>
    <message>
        <location filename="menu/name/inputnamemenu.cpp" line="56"/>
        <source>OK</source>
        <translation>CONFERMA</translation>
    </message>
    <message>
        <location filename="menu/name/inputnamemenu.cpp" line="58"/>
        <source>BACKSPACE</source>
        <translation></translation>
    </message>
    <message>
        <location filename="menu/name/inputnamemenu.cpp" line="60"/>
        <source>BACK</source>
        <translation>--INDIETRO--</translation>
    </message>
    <message>
        <location filename="menu/name/inputnamemenu.cpp" line="101"/>
        <source>INFO</source>
        <translation></translation>
    </message>
    <message>
        <location filename="menu/name/inputnamemenu.cpp" line="101"/>
        <source>Pilot name was saved</source>
        <translation>Nome del pilota salvato</translation>
    </message>
</context>
<context>
    <name>LanguageMenu</name>
    <message>
        <location filename="menu/general/languageMenu.cpp" line="63"/>
        <source>BACK</source>
        <translation>--INDIETRO--</translation>
    </message>
    <message>
        <location filename="menu/general/languageMenu.cpp" line="87"/>
        <source>INFO</source>
        <translation></translation>
    </message>
    <message>
        <location filename="menu/general/languageMenu.cpp" line="87"/>
        <source>Language was set</source>
        <translation>Lingua settata</translation>
    </message>
</context>
<context>
    <name>LetterChooser</name>
    <message>
        <location filename="letterchooser.ui" line="14"/>
        <source>Form</source>
        <translation>Modulo</translation>
    </message>
</context>
<context>
    <name>LocDeleteMenu</name>
    <message>
        <location filename="menu/location/locdeletemenu.cpp" line="39"/>
        <source>INFO</source>
        <translation></translation>
    </message>
    <message>
        <location filename="menu/location/locdeletemenu.cpp" line="39"/>
        <source>Location was removed</source>
        <translation>Decollo rimosso</translation>
    </message>
</context>
<context>
    <name>LocEditMenu</name>
    <message>
        <location filename="menu/location/loceditmenu.cpp" line="75"/>
        <source>COORDS FROM GPS</source>
        <translation>COORD. DA GPS</translation>
    </message>
    <message>
        <location filename="menu/location/loceditmenu.cpp" line="77"/>
        <source>INPUT NAME</source>
        <translation>NOME</translation>
    </message>
    <message>
        <location filename="menu/location/loceditmenu.cpp" line="79"/>
        <source>SET LATITUDE</source>
        <translation>SET LATITUDINE</translation>
    </message>
    <message>
        <location filename="menu/location/loceditmenu.cpp" line="81"/>
        <source>SET LONGITUDE</source>
        <translation>SET LONGITUDINE</translation>
    </message>
    <message>
        <location filename="menu/location/loceditmenu.cpp" line="83"/>
        <source>SET ALTITUDE</source>
        <translation>SET QUOTA</translation>
    </message>
    <message>
        <location filename="menu/location/loceditmenu.cpp" line="85"/>
        <source>SAVE</source>
        <translation>SALVA</translation>
    </message>
    <message>
        <location filename="menu/location/loceditmenu.cpp" line="87"/>
        <source>BACK</source>
        <translation>--INDIETRO--</translation>
    </message>
    <message>
        <location filename="menu/location/loceditmenu.cpp" line="129"/>
        <source>INFO</source>
        <translation></translation>
    </message>
    <message>
        <location filename="menu/location/loceditmenu.cpp" line="129"/>
        <source>Location was saved</source>
        <translation>Decollo salvato</translation>
    </message>
</context>
<context>
    <name>LocListMenu</name>
    <message>
        <location filename="menu/location/loclistmenu.cpp" line="31"/>
        <source>-- BACK --</source>
        <translation>--INDIETRO--</translation>
    </message>
</context>
<context>
    <name>LocSetCrtMenu</name>
    <message>
        <location filename="menu/location/locsetcrtmenu.cpp" line="28"/>
        <source>INFO</source>
        <translation></translation>
    </message>
    <message>
        <location filename="menu/location/locsetcrtmenu.cpp" line="28"/>
        <source>Location was saved</source>
        <translation>Decollo salvato</translation>
    </message>
</context>
<context>
    <name>LocalizationMenu</name>
    <message>
        <location filename="menu/general/localizationMenu.cpp" line="69"/>
        <source>SET TIMEZONE</source>
        <translation>IMPOSTA FUSO ORARIO</translation>
    </message>
    <message>
        <location filename="menu/general/localizationMenu.cpp" line="71"/>
        <source>LANGUAGE</source>
        <translation>LINGUA</translation>
    </message>
    <message>
        <location filename="menu/general/localizationMenu.cpp" line="73"/>
        <location filename="menu/general/localizationMenu.cpp" line="118"/>
        <source>SPEED</source>
        <translation>VELOCITA&apos;</translation>
    </message>
    <message>
        <location filename="menu/general/localizationMenu.cpp" line="75"/>
        <location filename="menu/general/localizationMenu.cpp" line="131"/>
        <source>VSPEED</source>
        <translation>VEL. VERTICALE</translation>
    </message>
    <message>
        <location filename="menu/general/localizationMenu.cpp" line="77"/>
        <source>DIST.</source>
        <translation>DIST.</translation>
    </message>
    <message>
        <location filename="menu/general/localizationMenu.cpp" line="79"/>
        <location filename="menu/general/localizationMenu.cpp" line="157"/>
        <source>HEIGHT</source>
        <translation>ALTEZZA</translation>
    </message>
    <message>
        <location filename="menu/general/localizationMenu.cpp" line="81"/>
        <source>BACK</source>
        <translation>--INDIETRO--</translation>
    </message>
    <message>
        <location filename="menu/general/localizationMenu.cpp" line="119"/>
        <location filename="menu/general/localizationMenu.cpp" line="132"/>
        <location filename="menu/general/localizationMenu.cpp" line="145"/>
        <location filename="menu/general/localizationMenu.cpp" line="158"/>
        <source>UNIT</source>
        <translation>UNITA&apos;</translation>
    </message>
    <message>
        <location filename="menu/general/localizationMenu.cpp" line="144"/>
        <source>DISTANCE</source>
        <translation>DISTANZA</translation>
    </message>
    <message>
        <location filename="menu/general/localizationMenu.cpp" line="185"/>
        <source>Change the timezone you are in to adjust from the universal time.</source>
        <translation>Cambia il fuso orario  rispetto a UTC</translation>
    </message>
    <message>
        <location filename="menu/general/localizationMenu.cpp" line="187"/>
        <source>Change the language of the device.</source>
        <translation>Cambia la lingua del dispositivo</translation>
    </message>
</context>
<context>
    <name>LocationsMenu</name>
    <message>
        <location filename="menu/location/locationsmenu.cpp" line="77"/>
        <source>ADD / EDIT</source>
        <translation>AGGIUNGI / MODIFICA</translation>
    </message>
    <message>
        <location filename="menu/location/locationsmenu.cpp" line="80"/>
        <source>AUTO-SET: OFF</source>
        <translation></translation>
    </message>
    <message>
        <location filename="menu/location/locationsmenu.cpp" line="82"/>
        <source>AUTO-SET: ON</source>
        <translation></translation>
    </message>
    <message>
        <location filename="menu/location/locationsmenu.cpp" line="84"/>
        <source>SET CURRENT</source>
        <translation>IMPOSTA MANUALE</translation>
    </message>
    <message>
        <location filename="menu/location/locationsmenu.cpp" line="86"/>
        <source>CLEAR CURRENT</source>
        <translation>CANCELLA ATTUALE</translation>
    </message>
    <message>
        <location filename="menu/location/locationsmenu.cpp" line="88"/>
        <source>DELETE</source>
        <translation>ELIMINA</translation>
    </message>
    <message>
        <location filename="menu/location/locationsmenu.cpp" line="90"/>
        <source>BACK</source>
        <translation>--INDIETRO--</translation>
    </message>
    <message>
        <location filename="menu/location/locationsmenu.cpp" line="115"/>
        <source>Modify data for existing location name or insert new location</source>
        <translation>Modifica dati per decollo esistente o inserisci nuova decollo</translation>
    </message>
    <message>
        <location filename="menu/location/locationsmenu.cpp" line="117"/>
        <source>Toggle manual vs automatic location set for flight recording</source>
        <translation>Commuta impostazione DECOLLO manuale/automatica per la registrazione volo</translation>
    </message>
    <message>
        <location filename="menu/location/locationsmenu.cpp" line="128"/>
        <source>INFO</source>
        <translation></translation>
    </message>
    <message>
        <location filename="menu/location/locationsmenu.cpp" line="128"/>
        <source>Location was cleared</source>
        <translation>Decollo cancellato</translation>
    </message>
</context>
<context>
    <name>MainMenu</name>
    <message>
        <location filename="menu/mainmenu.cpp" line="104"/>
        <source>SOUND</source>
        <translation>AUDIO</translation>
    </message>
    <message>
        <location filename="menu/mainmenu.cpp" line="107"/>
        <source>NAVIGATION/MAPS</source>
        <translation>NAVIGAZIONE/MAPPE</translation>
    </message>
    <message>
        <location filename="menu/mainmenu.cpp" line="109"/>
        <source>NAVIGATION/TASK</source>
        <translation>NAVIGAZIONE/TASK</translation>
    </message>
    <message>
        <location filename="menu/mainmenu.cpp" line="111"/>
        <source>GENERAL</source>
        <translation>GENERALE</translation>
    </message>
    <message>
        <source>GLIDES</source>
        <translation type="vanished">DIARIO VOLI</translation>
    </message>
    <message>
        <location filename="menu/mainmenu.cpp" line="113"/>
        <source>FLIGHTS</source>
        <translation>DIARIO VOLI</translation>
    </message>
    <message>
        <location filename="menu/mainmenu.cpp" line="115"/>
        <source>AVIONICS</source>
        <translation>AVIONICA</translation>
    </message>
    <message>
        <location filename="menu/mainmenu.cpp" line="117"/>
        <source>ABOUT</source>
        <translation>SOFTWARE</translation>
    </message>
    <message>
        <location filename="menu/mainmenu.cpp" line="120"/>
        <source>EXIT PROGRAM</source>
        <translation>SPEGNI</translation>
    </message>
    <message>
        <location filename="menu/mainmenu.cpp" line="123"/>
        <source>SWITCH THEME</source>
        <translation>CAMBIA TEMA</translation>
    </message>
    <message>
        <location filename="menu/mainmenu.cpp" line="125"/>
        <source>TURN OFF</source>
        <translation>SPEGNIMENTO</translation>
    </message>
    <message>
        <location filename="menu/mainmenu.cpp" line="197"/>
        <source>Maps and navigation options</source>
        <translation>Mappe e opzioni di navigazione</translation>
    </message>
    <message>
        <location filename="menu/mainmenu.cpp" line="199"/>
        <source>View recorded glides</source>
        <translation>Visualizza voli registrati</translation>
    </message>
    <message>
        <location filename="menu/mainmenu.cpp" line="201"/>
        <source>Task navigation options</source>
        <translation>Opzioni di navigazione TASK</translation>
    </message>
    <message>
        <location filename="menu/mainmenu.cpp" line="203"/>
        <source>Flight algorithms related settings</source>
        <translation>Impostazioni reltive agli algoritmi di volo</translation>
    </message>
    <message>
        <location filename="menu/mainmenu.cpp" line="205"/>
        <source>Volume and sound curve settings</source>
        <translation>Volume e impostazione della curva audio</translation>
    </message>
    <message>
        <location filename="menu/mainmenu.cpp" line="207"/>
        <source>General, localization and connectivity settings</source>
        <translation>Impostazioni Generali, localizzazione e connettività</translation>
    </message>
</context>
<context>
    <name>MainUI</name>
    <message>
        <location filename="mainui.ui" line="32"/>
        <source>MainUI</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainui.cpp" line="531"/>
        <location filename="mainui.cpp" line="551"/>
        <location filename="mainui.cpp" line="556"/>
        <source>INFO</source>
        <translation>INFO</translation>
    </message>
    <message>
        <location filename="mainui.cpp" line="531"/>
        <source>Landed</source>
        <translation>Atterrato</translation>
    </message>
    <message>
        <location filename="mainui.cpp" line="539"/>
        <source>TURN OFF</source>
        <translation>SPEGNIMENTO</translation>
    </message>
    <message>
        <location filename="mainui.cpp" line="551"/>
        <source>Takeoff</source>
        <translation>Decollo</translation>
    </message>
    <message>
        <location filename="mainui.cpp" line="715"/>
        <location filename="mainui.cpp" line="725"/>
        <source>SOFT TURN OFF</source>
        <translation>SPEGIMENTO SOFT</translation>
    </message>
    <message>
        <location filename="mainui.cpp" line="716"/>
        <location filename="mainui.cpp" line="726"/>
        <source>CONFIRM</source>
        <comment>power button</comment>
        <translation type="unfinished">CONFERMA</translation>
    </message>
    <message>
        <location filename="mainui.cpp" line="773"/>
        <source>CONFIRM</source>
        <translation>CONFERMA</translation>
    </message>
    <message>
        <location filename="mainui.cpp" line="717"/>
        <location filename="mainui.cpp" line="727"/>
        <source>OK</source>
        <translation>CONFERMA</translation>
    </message>
    <message>
        <location filename="mainui.cpp" line="793"/>
        <source>INFO Timeout</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainui.cpp" line="541"/>
        <location filename="mainui.cpp" line="718"/>
        <location filename="mainui.cpp" line="728"/>
        <source>CANCEL</source>
        <translation>ANNULLA</translation>
    </message>
    <message>
        <location filename="mainui.cpp" line="744"/>
        <source>INFO Side</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainui.cpp" line="744"/>
        <location filename="mainui.cpp" line="793"/>
        <source>Powering Off!</source>
        <translation>Spegnimento in corso!</translation>
    </message>
    <message>
        <location filename="mainui.cpp" line="771"/>
        <source>TURN ON</source>
        <translation>TIMEOUT ACCENSIONE</translation>
    </message>
</context>
<context>
    <name>MenuContainer</name>
    <message>
        <location filename="MenuContainer.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MenuDialog</name>
    <message>
        <location filename="menudialog.ui" line="17"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="menudialog.ui" line="120"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="menudialog.cpp" line="214"/>
        <source>CURRENT: </source>
        <translation>ATTUALE</translation>
    </message>
</context>
<context>
    <name>MixerMenu</name>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="62"/>
        <source>TEST ON</source>
        <translation>PROVA ON</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="63"/>
        <source>TEST OFF</source>
        <translation>PROVA OFF</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="66"/>
        <source>Test cm/s </source>
        <translation>Prova cm/s</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="71"/>
        <source>Lin. Increase: ON</source>
        <translation>Incremento Lin.: ON</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="75"/>
        <source>Lin. Increase: OFF</source>
        <translation>Incremento Lin.: OFF</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="81"/>
        <source>Sniff cm/s </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="82"/>
        <source>OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="87"/>
        <source>UpThr cm/s </source>
        <translation>Soglia salita cm/s </translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="89"/>
        <source>DownThr cm/s </source>
        <translation>Soglia discesa cm/s </translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="91"/>
        <location filename="menu/sound/mixermenu.cpp" line="213"/>
        <source>Up F0 Hz</source>
        <translation>Salita F0 Hz</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="93"/>
        <location filename="menu/sound/mixermenu.cpp" line="229"/>
        <source>Up F10 Hz</source>
        <translation>Salita F10 Hz</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="95"/>
        <location filename="menu/sound/mixermenu.cpp" line="246"/>
        <source>Up I0 ms</source>
        <translation>Salita I0 ms</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="97"/>
        <location filename="menu/sound/mixermenu.cpp" line="262"/>
        <source>Up I10 ms</source>
        <translation>Salita I10 ms</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="99"/>
        <location filename="menu/sound/mixermenu.cpp" line="278"/>
        <source>Down F0 Hz</source>
        <translation>Discesa F0 Hz</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="101"/>
        <location filename="menu/sound/mixermenu.cpp" line="293"/>
        <source>Down F10 Hz</source>
        <translation>Discesa F10 Hz</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="103"/>
        <source>BACK</source>
        <translation>--INDIETRO--</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="146"/>
        <source>Test cm/s</source>
        <translation>Prova cm/s</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="165"/>
        <source>Sniff. thr cm/s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="181"/>
        <source>Up thr cm/s</source>
        <translation>Soglia salita cm/s </translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="197"/>
        <source>Down thr cm/s</source>
        <translation>Soglia discesa cm/s </translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="321"/>
        <source>Toggle test on off. Use to test your settings and accomodate with the beeping.</source>
        <translation>Commuta on off prova. Usa per impostare regolare la segnalazione acustica</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="323"/>
        <source>Set test climb/sink rate.</source>
        <translation>imposta velocita&apos; di salita/discesa di prova</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="325"/>
        <source>Set climb tone threshold in cm per second.</source>
        <translation>Imposta soglia tono di salita in cm al secondo</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="327"/>
        <source>Set sink tone threshold in cm per second.</source>
        <translation>Imposta soglia tono di discesa in cm al secondo</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="329"/>
        <source>Set thermal sniffer threshold. Go all way to 0 to turn it off.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="331"/>
        <source>Set start frequency for climb.</source>
        <translation>Imposta frequenza iniziale per salita</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="333"/>
        <source>Set end frequency for climb. Equivalent to 10 m/s.</source>
        <translation>Imposta frequenza finale per salita. Equivale a 10 m/s.</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="335"/>
        <source>Set start sound interval for climb in ms.</source>
        <translation>Imposta suono intervallo iniziale salita in ms</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="337"/>
        <source>Set end sound interval for climb in ms. Equivalent to 10 m/s.</source>
        <translation>Imposta suono intervallo finale per salita in ms. Equivale a  10m/s</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="339"/>
        <source>Set start frequency for sink.</source>
        <translation>Imposta la frequenza iniziale per la discesa</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="341"/>
        <source>Set end frequency for sink. Equivalent to -10 m/s.</source>
        <translation>Imposta la frequenza finale per la discesa equivalente a -10 m/s.</translation>
    </message>
</context>
<context>
    <name>MixerMenuSelect</name>
    <message>
        <location filename="menu/sound/mixerMenuSelect.cpp" line="67"/>
        <source>Legacy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/sound/mixerMenuSelect.cpp" line="68"/>
        <source>Get mixers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/sound/mixerMenuSelect.cpp" line="76"/>
        <source>BACK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/sound/mixerMenuSelect.cpp" line="98"/>
        <location filename="menu/sound/mixerMenuSelect.cpp" line="105"/>
        <location filename="menu/sound/mixerMenuSelect.cpp" line="111"/>
        <location filename="menu/sound/mixerMenuSelect.cpp" line="126"/>
        <location filename="menu/sound/mixerMenuSelect.cpp" line="144"/>
        <location filename="menu/sound/mixerMenuSelect.cpp" line="164"/>
        <source>INFO</source>
        <translation type="unfinished">INFO</translation>
    </message>
    <message>
        <location filename="menu/sound/mixerMenuSelect.cpp" line="98"/>
        <location filename="menu/sound/mixerMenuSelect.cpp" line="111"/>
        <source>Config saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/sound/mixerMenuSelect.cpp" line="105"/>
        <source>Check Conn</source>
        <translation type="unfinished">Verifica Conn.</translation>
    </message>
    <message>
        <location filename="menu/sound/mixerMenuSelect.cpp" line="126"/>
        <source>Conn OK</source>
        <translation type="unfinished">Connessione OK</translation>
    </message>
    <message>
        <location filename="menu/sound/mixerMenuSelect.cpp" line="132"/>
        <location filename="menu/sound/mixerMenuSelect.cpp" line="150"/>
        <source>ERROR</source>
        <translation type="unfinished">ERRORE</translation>
    </message>
    <message>
        <location filename="menu/sound/mixerMenuSelect.cpp" line="132"/>
        <source>Not Connected</source>
        <translation type="unfinished">Non Connesso</translation>
    </message>
    <message>
        <location filename="menu/sound/mixerMenuSelect.cpp" line="144"/>
        <source>Success!</source>
        <translation type="unfinished">Successo!</translation>
    </message>
    <message>
        <location filename="menu/sound/mixerMenuSelect.cpp" line="150"/>
        <source>Not Updated</source>
        <translation type="unfinished">Non aggiornato</translation>
    </message>
    <message>
        <location filename="menu/sound/mixerMenuSelect.cpp" line="164"/>
        <source>Downloading</source>
        <translation type="unfinished">Scaricamento ...</translation>
    </message>
</context>
<context>
    <name>NavigationAirspaceM</name>
    <message>
        <location filename="menu/navigation/navigationAirspace.cpp" line="64"/>
        <source>BACK</source>
        <translation>--INDIETRO--</translation>
    </message>
    <message>
        <source>Airspace was saved</source>
        <translation type="vanished">Spazioa aereo salvato</translation>
    </message>
</context>
<context>
    <name>NavigationMapM</name>
    <message>
        <location filename="menu/navigation/navigationMap.cpp" line="47"/>
        <source>AUTO</source>
        <translation>AUTOMATICA</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMap.cpp" line="55"/>
        <source>BACK</source>
        <translation>--INDIETRO--</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMap.cpp" line="78"/>
        <location filename="menu/navigation/navigationMap.cpp" line="86"/>
        <source>INFO</source>
        <translation>INFO</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMap.cpp" line="78"/>
        <source>Map set to auto</source>
        <translation>Mappa automatica</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMap.cpp" line="86"/>
        <source>Map was saved</source>
        <translation>Mappa salvata</translation>
    </message>
</context>
<context>
    <name>NavigationMenu</name>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="63"/>
        <source>AIRSPACE</source>
        <translation>SPAZI AEREI</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="69"/>
        <source>MAP - AUTO</source>
        <translation>MAPPA - AUTOMATICA</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="70"/>
        <source>MAP - </source>
        <translation>MAPPA - </translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="77"/>
        <source>SHOW WOODS - ON</source>
        <translation>AREA BOSCHIVA - ON</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="79"/>
        <source>SHOW WOODS - OFF</source>
        <translation>AREA BOSCHIVA - OFF</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="84"/>
        <source>Airspace Front-Up - ON</source>
        <translation>Spazi Aerei in alto - ON</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="86"/>
        <source>Airspace Front-Up - OFF</source>
        <translation>Spazi Aerei in alto - OFF</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="97"/>
        <source>V.Near-</source>
        <oldsource>V.Near</oldsource>
        <translation>V. Vicino-</translation>
    </message>
    <message>
        <source>H.Near</source>
        <translation type="vanished">O. Vicino</translation>
    </message>
    <message>
        <source>H.Very Near</source>
        <translation type="vanished">O. molto vivino</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="105"/>
        <source>H.Near-</source>
        <translation>O. Vicino-</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="113"/>
        <source>H.Very Near-</source>
        <translation>O. Molto Vicino-</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="119"/>
        <source>LOCATION</source>
        <translation>DECOLLI</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="121"/>
        <source>BACK</source>
        <translation>--INDIETRO--</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="171"/>
        <source>Vertical warning</source>
        <oldsource>Horizontal warning</oldsource>
        <translation>Allarme orizzontale</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="196"/>
        <source>Near warning</source>
        <translation>Allerta vicino</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="220"/>
        <source>Very near warning</source>
        <translation>Allerta MOLTO vicino</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="236"/>
        <source>Ignore over</source>
        <translation>Ignora sopra ai</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="286"/>
        <source>Select airspace file to be used from root/airspace folder on SD card.</source>
        <translation>Seleziona spazio aereo da usare dalla cartella root/airspace sulla SD card</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="288"/>
        <source>Set vertical separation to airspace for warning.</source>
        <translation>Imposta distanza verticale per allerta spazio aereo </translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="290"/>
        <source>Set horizontal separation to airspace for NEAR warning.</source>
        <translation>Imposta distanza orizzontale per allerta spazio aereo VICINO </translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="292"/>
        <source>Set horizontal separation to airspace for VERY NEAR warning.</source>
        <translation>Imposta distanza orizzontale per allerta spazio aereo MOLTO VICINO </translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="294"/>
        <source>Maps are autodetected from root/maps folder on SD card.</source>
        <translation>Le mappe sono autorilevate dalla cartella root/maps sulla SD </translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="296"/>
        <source>Set superior limmit for airspace display. Note that the automatic airspace infrigement algorithm will still consider the hidden airspaces.</source>
        <translation>Imposta limite superiore per gli spazi aerei. Si noti che l&apos;algoritmo di violazione automatica dello spazio aereo continuerà a considerare lo spazio aereo nascosto.</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="298"/>
        <source>Toggle airspace plotting. If disables the map will always display with the N to the top. If enabled the map will rotate to display the pront position on the top of the map.</source>
        <translation>Attiva / disattiva il tracciamento dello spazio aereo. Se disabilitato la mappa verrà sempre visualizzata con N in alto. Se abilitato, la mappa ruoterà per visualizzare la posizione frontale nella parte superiore della mappa.</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="300"/>
        <source>Hide or show woods on the topo map.</source>
        <translation>Nasconde o visualizza le aree boschive sulla mappa</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="302"/>
        <source>Add or edit/delete location information.</source>
        <translation>Aggiungi o edita/cancella informazioni sul decollo</translation>
    </message>
    <message>
        <source>Select map to be used from root/maps folder on SD card.</source>
        <translation type="vanished">Seleziona mappa da usare dalla cartella root/maps sulla SD card</translation>
    </message>
</context>
<context>
    <name>NumericInput</name>
    <message>
        <location filename="NumericInput.ui" line="17"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>NumericInputDecimal</name>
    <message>
        <location filename="NumericInputDecimal.ui" line="17"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ObexThread</name>
    <message>
        <location filename="threads/obexThread.cpp" line="55"/>
        <source>Map received</source>
        <translation>Mappa ricevuta</translation>
    </message>
    <message>
        <location filename="threads/obexThread.cpp" line="60"/>
        <source>Airspace received</source>
        <translation>Spazio aereo ricevuto</translation>
    </message>
    <message>
        <location filename="threads/obexThread.cpp" line="65"/>
        <source>Waypoints received</source>
        <translation>Waypoint ricevuto</translation>
    </message>
    <message>
        <location filename="threads/obexThread.cpp" line="72"/>
        <source>*.xctsk received</source>
        <translation>*.xctsk ricevuto</translation>
    </message>
    <message>
        <location filename="threads/obexThread.cpp" line="77"/>
        <source>*.qm received</source>
        <translation>*.qm ricevuto</translation>
    </message>
</context>
<context>
    <name>RemoteMenu</name>
    <message>
        <location filename="menu/general/remoteMenu.cpp" line="54"/>
        <source>KEY:Left/Up</source>
        <translation>Tasto: Su/Sinistra</translation>
    </message>
    <message>
        <location filename="menu/general/remoteMenu.cpp" line="56"/>
        <source>KEY:Right/Down</source>
        <translation>Tasto: Giu/Destra</translation>
    </message>
    <message>
        <location filename="menu/general/remoteMenu.cpp" line="58"/>
        <source>KEY:Enter</source>
        <translation>Tasto: Invio</translation>
    </message>
    <message>
        <location filename="menu/general/remoteMenu.cpp" line="60"/>
        <source>KEY:Esc</source>
        <translation>Tasto: Uscita</translation>
    </message>
    <message>
        <location filename="menu/general/remoteMenu.cpp" line="62"/>
        <source>KEY:Function</source>
        <translation>Tasto: Funzione</translation>
    </message>
    <message>
        <location filename="menu/general/remoteMenu.cpp" line="64"/>
        <source>KEY:Menu</source>
        <translation>Tasto: Menu</translation>
    </message>
    <message>
        <location filename="menu/general/remoteMenu.cpp" line="66"/>
        <source>BACK</source>
        <translation>--INDIETRO--</translation>
    </message>
    <message>
        <location filename="menu/general/remoteMenu.cpp" line="78"/>
        <source>ACTION</source>
        <translation>AZIONE</translation>
    </message>
    <message>
        <location filename="menu/general/remoteMenu.cpp" line="78"/>
        <source>Press remote button</source>
        <translation>Premi tasto telecomando</translation>
    </message>
    <message>
        <location filename="menu/general/remoteMenu.cpp" line="85"/>
        <source>WARNING</source>
        <translation>ATTENZIONE</translation>
    </message>
    <message>
        <location filename="menu/general/remoteMenu.cpp" line="85"/>
        <source>Button not set</source>
        <translation>Tasto non settato</translation>
    </message>
    <message>
        <location filename="menu/general/remoteMenu.cpp" line="108"/>
        <source>INFO</source>
        <translation></translation>
    </message>
    <message>
        <location filename="menu/general/remoteMenu.cpp" line="108"/>
        <source>Key Mapped</source>
        <translation>Tasto mappato</translation>
    </message>
</context>
<context>
    <name>SatelliteStatusPage</name>
    <message>
        <location filename="ui/satelliteStatusPage.cpp" line="67"/>
        <source>Lon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/satelliteStatusPage.cpp" line="71"/>
        <source>Lat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/satelliteStatusPage.cpp" line="75"/>
        <source>Height</source>
        <translation type="unfinished">Quota</translation>
    </message>
    <message>
        <location filename="ui/satelliteStatusPage.cpp" line="81"/>
        <source>PRESS ENTER TO CLOSE</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SetNumSampleMenu</name>
    <message>
        <location filename="menu/numSamples/setnumsamplemenu.cpp" line="51"/>
        <source>UP</source>
        <translation>AUMENTA</translation>
    </message>
    <message>
        <location filename="menu/numSamples/setnumsamplemenu.cpp" line="53"/>
        <source>DOWN</source>
        <translation>DIMINUISCI</translation>
    </message>
    <message>
        <location filename="menu/numSamples/setnumsamplemenu.cpp" line="55"/>
        <source>OK</source>
        <translation></translation>
    </message>
    <message>
        <location filename="menu/numSamples/setnumsamplemenu.cpp" line="57"/>
        <source>BACK</source>
        <translation>--INDIETRO--</translation>
    </message>
    <message>
        <location filename="menu/numSamples/setnumsamplemenu.cpp" line="90"/>
        <source>INFO</source>
        <translation></translation>
    </message>
    <message>
        <location filename="menu/numSamples/setnumsamplemenu.cpp" line="90"/>
        <source>Value was saved</source>
        <translation>Nuovo valore salvato</translation>
    </message>
</context>
<context>
    <name>ShutdownMenu</name>
    <message>
        <location filename="menu/shutdown/shutdownmenu.cpp" line="67"/>
        <source>Exit program</source>
        <translation>Spegni vario</translation>
    </message>
    <message>
        <location filename="menu/shutdown/shutdownmenu.cpp" line="70"/>
        <source>Shutdown vario</source>
        <translation>Spegni vario</translation>
    </message>
    <message>
        <location filename="menu/shutdown/shutdownmenu.cpp" line="73"/>
        <source>Cancel shutdown</source>
        <translation>Annulla spegnimento</translation>
    </message>
    <message>
        <location filename="menu/shutdown/shutdownmenu.cpp" line="84"/>
        <location filename="menu/shutdown/shutdownmenu.cpp" line="87"/>
        <source>OK</source>
        <translation></translation>
    </message>
    <message>
        <location filename="menu/shutdown/shutdownmenu.cpp" line="90"/>
        <source>CANCEL</source>
        <translation>ANNULLA</translation>
    </message>
    <message>
        <location filename="menu/shutdown/shutdownmenu.cpp" line="110"/>
        <location filename="menu/shutdown/shutdownmenu.cpp" line="118"/>
        <source>INFO</source>
        <translation>INFO</translation>
    </message>
    <message>
        <location filename="menu/shutdown/shutdownmenu.cpp" line="110"/>
        <source>Exiting</source>
        <translation>Uscita</translation>
    </message>
    <message>
        <location filename="menu/shutdown/shutdownmenu.cpp" line="118"/>
        <source>Powering Off!</source>
        <translation>Spegnimento in corso!</translation>
    </message>
</context>
<context>
    <name>SoundMenu</name>
    <message>
        <location filename="menu/sound/soundMenu.cpp" line="58"/>
        <source>VOLUME</source>
        <translation>VOLUME</translation>
    </message>
    <message>
        <location filename="menu/sound/soundMenu.cpp" line="61"/>
        <source>SILENT ON GROUND ON</source>
        <translation>SILENZIA A TERRA ON</translation>
    </message>
    <message>
        <location filename="menu/sound/soundMenu.cpp" line="62"/>
        <source>SILENT ON GROUND OFF</source>
        <translation>SILENZIA A TERRA OFF</translation>
    </message>
    <message>
        <location filename="menu/sound/soundMenu.cpp" line="65"/>
        <source>SQUARE ON</source>
        <translation>ONDA QUADRA ON</translation>
    </message>
    <message>
        <location filename="menu/sound/soundMenu.cpp" line="66"/>
        <source>SQUARE OFF</source>
        <translation>ONDA QUADRA OFF</translation>
    </message>
    <message>
        <location filename="menu/sound/soundMenu.cpp" line="69"/>
        <source>SOUND MIXER</source>
        <translation>MIXER AUDIO</translation>
    </message>
    <message>
        <location filename="menu/sound/soundMenu.cpp" line="72"/>
        <source>SOUND MIXER SELECT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/sound/soundMenu.cpp" line="75"/>
        <source>BACK</source>
        <translation>--INDIETRO--</translation>
    </message>
    <message>
        <location filename="menu/sound/soundMenu.cpp" line="136"/>
        <source>Change system volume.</source>
        <translation>Modifica volume di sistema</translation>
    </message>
    <message>
        <location filename="menu/sound/soundMenu.cpp" line="138"/>
        <source>Set if vario is silent on ground.</source>
        <translation>Seleziona vario silenzioso a terra</translation>
    </message>
    <message>
        <location filename="menu/sound/soundMenu.cpp" line="140"/>
        <source>Toggle square/sine wave. Sine is more pleasant for the ear but less loud especially for low frequency.</source>
        <translation>Attiva / disattiva onda quadra / sinusoidale. Sinusoidale è più piacevole per l&apos;orecchio ma meno forte, soprattutto alle basse frequenze</translation>
    </message>
    <message>
        <location filename="menu/sound/soundMenu.cpp" line="142"/>
        <source>Use to set the vario tone.</source>
        <translation>Usare per impostare il tono vario</translation>
    </message>
    <message>
        <location filename="menu/sound/soundMenu.cpp" line="144"/>
        <source>Select sound mixer to be used.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SystemMenu</name>
    <message>
        <location filename="menu/system/systemMenu.cpp" line="73"/>
        <source>CPU LOAD</source>
        <translation>CARICO CPU</translation>
    </message>
    <message>
        <location filename="menu/system/systemMenu.cpp" line="75"/>
        <source>BOOT TIME</source>
        <translation>TEMPO DI BOOT</translation>
    </message>
    <message>
        <location filename="menu/system/systemMenu.cpp" line="77"/>
        <source>BACK</source>
        <translation>--INDIETRO--</translation>
    </message>
    <message>
        <location filename="menu/system/systemMenu.cpp" line="108"/>
        <source>sysd boot</source>
        <translation>sysd boot</translation>
    </message>
    <message>
        <location filename="menu/system/systemMenu.cpp" line="108"/>
        <source>Created</source>
        <translation>Creato</translation>
    </message>
</context>
<context>
    <name>Task</name>
    <message>
        <location filename="navWaypoint/task.cpp" line="644"/>
        <location filename="navWaypoint/task.cpp" line="655"/>
        <location filename="navWaypoint/task.cpp" line="676"/>
        <location filename="navWaypoint/task.cpp" line="681"/>
        <location filename="navWaypoint/task.cpp" line="697"/>
        <location filename="navWaypoint/task.cpp" line="702"/>
        <location filename="navWaypoint/task.cpp" line="707"/>
        <source>TASK INFO</source>
        <translation>INFO TASK</translation>
    </message>
    <message>
        <location filename="navWaypoint/task.cpp" line="644"/>
        <source>5 MIN TO SSS</source>
        <translation>5 MIN A SSS</translation>
    </message>
    <message>
        <location filename="navWaypoint/task.cpp" line="655"/>
        <source>START OPEN</source>
        <translation>START APERTO</translation>
    </message>
    <message>
        <location filename="navWaypoint/task.cpp" line="676"/>
        <source>TASK STARTED</source>
        <translation>TASK AVVIATA</translation>
    </message>
    <message>
        <location filename="navWaypoint/task.cpp" line="681"/>
        <source>TASK RESTARTED</source>
        <translation>TASK RIAVVIATA</translation>
    </message>
    <message>
        <location filename="navWaypoint/task.cpp" line="697"/>
        <source>ESS COMPLETED</source>
        <translation>ESS COMPLETATA</translation>
    </message>
    <message>
        <location filename="navWaypoint/task.cpp" line="702"/>
        <source>TP REACHED</source>
        <translation>BOA RAGGIUNTA</translation>
    </message>
    <message>
        <location filename="navWaypoint/task.cpp" line="707"/>
        <source>TASK FINISHED</source>
        <translation>TASK TERMINATA</translation>
    </message>
</context>
<context>
    <name>TaskEditMenu</name>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="66"/>
        <source>-TASK TYPE-</source>
        <translation>-TIPO DI TASK-</translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="69"/>
        <source>ELAPSED TIME</source>
        <translation>TEMPO TRASCORSO</translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="70"/>
        <source>RACE</source>
        <translation></translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="71"/>
        <location filename="menu/task/taskEditMenu.cpp" line="82"/>
        <source>TIME GATES</source>
        <translation>ORA START PILON</translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="74"/>
        <source>-SSS OPEN-</source>
        <translation>-SSS OPEN-</translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="78"/>
        <source>-TASK DEADLINE-</source>
        <translation>-FINE TASK-</translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="84"/>
        <source>TURN POINTS</source>
        <translation>BOE</translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="86"/>
        <source>SAVE/PREPARE/RESET</source>
        <oldsource>PREPARE/RESET</oldsource>
        <translation>SALVA/PREPARA/RIPRISTINA</translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="88"/>
        <location filename="menu/task/taskEditMenu.cpp" line="158"/>
        <source>CLEAR</source>
        <translation>CANCELLA</translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="90"/>
        <source>BACK</source>
        <translation>--INDIETRO--</translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="152"/>
        <source>INFO</source>
        <translation>INFO</translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="152"/>
        <source>TASK PREPARED</source>
        <translation>TASK PREPARATA</translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="159"/>
        <source>DELETE?</source>
        <translation>CANCELLARE?</translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="160"/>
        <source>CANCEL</source>
        <translation>ANNULLA</translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="161"/>
        <source>DELETE</source>
        <translation>CANCELLA</translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="191"/>
        <source>Select competition rules for the task.</source>
        <translation>Seleziona regole della task</translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="193"/>
        <source>Choose the opening time for the task. If this is a Time Gate competition this will also be the first time gate.</source>
        <translation>Scegli l&apos;orario di apertura della task. </translation>
    </message>
    <message>
        <source>Chouse the opening time for the task. If this is a Time Gate competition this will also be the first time gate.</source>
        <translation type="vanished">Impostare l&apos;orario di apertura della task. Se e&apos; una gara con Start Pilon questo sara&apos; anche il primo Start pilon</translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="195"/>
        <source>This is the time until you can score competition points.</source>
        <translation>Questo è l&apos;orario fino a cui la gara e&apos; valida</translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="197"/>
        <source>If this is a Time Gate competition you can add new Time Gates here.</source>
        <translation>Se questa è una competizione con Start Pilons puoi aggiungerli qui</translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="199"/>
        <source>Add/Edit turnpoints.</source>
        <translation>Agg./Mod. Boe</translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="201"/>
        <source>Prepare task to be flown. This action is also done automatically at takeoff.</source>
        <translation>Prepara la task per l&apos;involo. Questo avviene automaticamente al decollo</translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="203"/>
        <source>Clear task.</source>
        <translation>Cancella task</translation>
    </message>
</context>
<context>
    <name>TaskImportMenu</name>
    <message>
        <location filename="menu/task/taskImportMenu.cpp" line="55"/>
        <source>BACK</source>
        <translation>--INDIETRO--</translation>
    </message>
    <message>
        <location filename="menu/task/taskImportMenu.cpp" line="88"/>
        <source>FILE</source>
        <translation>FILE</translation>
    </message>
    <message>
        <location filename="menu/task/taskImportMenu.cpp" line="89"/>
        <source>ACTION?</source>
        <translation>AZIONE?</translation>
    </message>
    <message>
        <location filename="menu/task/taskImportMenu.cpp" line="90"/>
        <source>CANCEL</source>
        <translation>ANNULLA</translation>
    </message>
    <message>
        <location filename="menu/task/taskImportMenu.cpp" line="91"/>
        <source>DELETE</source>
        <translation>CANCELLA</translation>
    </message>
    <message>
        <location filename="menu/task/taskImportMenu.cpp" line="92"/>
        <source>IMPORT</source>
        <translation>IMPORTA</translation>
    </message>
    <message>
        <location filename="menu/task/taskImportMenu.cpp" line="108"/>
        <location filename="menu/task/taskImportMenu.cpp" line="110"/>
        <location filename="menu/task/taskImportMenu.cpp" line="119"/>
        <source>INFO</source>
        <translation>INFO</translation>
    </message>
    <message>
        <location filename="menu/task/taskImportMenu.cpp" line="108"/>
        <source>TASK IMPORTED</source>
        <translation>TASK IMPORTATA</translation>
    </message>
    <message>
        <location filename="menu/task/taskImportMenu.cpp" line="110"/>
        <source>TASK IMPORT FAILED</source>
        <translation>IMPORTAZ. TASK FALLITA</translation>
    </message>
    <message>
        <location filename="menu/task/taskImportMenu.cpp" line="119"/>
        <source>REMOVED</source>
        <translation>RIMOSSA</translation>
    </message>
</context>
<context>
    <name>TaskMenu</name>
    <message>
        <location filename="menu/task/taskMenu.cpp" line="57"/>
        <source>WAYPOINTS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="menu/task/taskMenu.cpp" line="59"/>
        <source>TASK</source>
        <translation></translation>
    </message>
    <message>
        <location filename="menu/task/taskMenu.cpp" line="61"/>
        <source>IMPORT TASK</source>
        <translation>IMPORTA TASK</translation>
    </message>
    <message>
        <location filename="menu/task/taskMenu.cpp" line="63"/>
        <source>GPS INFO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskMenu.cpp" line="65"/>
        <source>BACK</source>
        <translation>--INDIETRO--</translation>
    </message>
    <message>
        <location filename="menu/task/taskMenu.cpp" line="119"/>
        <source>Manage waypoints for tasks.</source>
        <translation>Gestione waypoints per tasks</translation>
    </message>
    <message>
        <location filename="menu/task/taskMenu.cpp" line="121"/>
        <source>Create/Edit task.</source>
        <translation>Crea/Edita task</translation>
    </message>
    <message>
        <location filename="menu/task/taskMenu.cpp" line="123"/>
        <source>Import task.</source>
        <translation>Importa task da altri dispositivi</translation>
    </message>
    <message>
        <location filename="menu/task/taskMenu.cpp" line="125"/>
        <source>Show gps info page</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TaskNavigationMenu</name>
    <message>
        <location filename="menu/task/taskNavigationMenu.cpp" line="63"/>
        <source>Nav to TO</source>
        <translation>Naviga verso DECOLLO</translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenu.cpp" line="65"/>
        <source>Nav to task Tp</source>
        <translation>Naviga verso boa task</translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenu.cpp" line="67"/>
        <source>Nav to WP</source>
        <translation>Naviga verso WP</translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenu.cpp" line="69"/>
        <source>Back to task</source>
        <translation>Torna alla task</translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenu.cpp" line="71"/>
        <source>Reset task</source>
        <translation>Ripristina task</translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenu.cpp" line="73"/>
        <source>Zoom</source>
        <translation>Zoom</translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenu.cpp" line="77"/>
        <source>C. to TP1 ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenu.cpp" line="78"/>
        <source>C. to TP1 OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenu.cpp" line="81"/>
        <source>Back</source>
        <translation>--INDIETRO--</translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenu.cpp" line="124"/>
        <location filename="menu/task/taskNavigationMenu.cpp" line="135"/>
        <location filename="menu/task/taskNavigationMenu.cpp" line="141"/>
        <source>NAV</source>
        <translation>NAVIGA</translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenu.cpp" line="124"/>
        <source>Nav to T.O.</source>
        <translation>Naviga verso decollo</translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenu.cpp" line="135"/>
        <source>Task reset</source>
        <translation>Task ripristinata</translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenu.cpp" line="141"/>
        <source>Nav to task</source>
        <translation>Naviga alla Task</translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenu.cpp" line="169"/>
        <source>Navigate to takeoff point</source>
        <translation>Navoga verso il punto di decollo</translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenu.cpp" line="171"/>
        <source>Reset task to first turnpoint</source>
        <oldsource>Reset task to firts turnpoint</oldsource>
        <translation>Rpristina la task dalla prima boa</translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenu.cpp" line="173"/>
        <source>Go back to task navigation after a waypoint navigation was activated. Task will be resumed from the last activated turnpoint</source>
        <translation>Torna alla navigazione alla task dopo che era stata attivata la navigazione ad un waypoint. La task verrà ripristinata dall&apos;ultima boa attivata</translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenu.cpp" line="175"/>
        <source>Navigate to a turnpoint defined in the task</source>
        <translation>Naviga verso una boa definita nella task</translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenu.cpp" line="177"/>
        <source>Navigate to a loaded waypoint, not necessary from the active task</source>
        <translation>Naviga verso un waypoint caricato, non necessariamente dalla task attiva</translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenu.cpp" line="179"/>
        <source>Zoom functionality for task widget.</source>
        <translation>Funzionalità Zoom per il widget della Task</translation>
    </message>
</context>
<context>
    <name>TaskNavigationMenuTp</name>
    <message>
        <location filename="menu/task/taskNavigationMenuTp.cpp" line="52"/>
        <source>Back</source>
        <translation>--INDIETRO--</translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenuTp.cpp" line="87"/>
        <source>NAV</source>
        <translation>NAVIGA</translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenuTp.cpp" line="87"/>
        <source>Turnpoint set</source>
        <translation>Boa impostata</translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenuTp.cpp" line="109"/>
        <source>Navigate to:</source>
        <translation>Naviga verso:</translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenuTp.cpp" line="118"/>
        <source>LINE</source>
        <translation>LINEA</translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenuTp.cpp" line="119"/>
        <source>CYLINDER ENTER</source>
        <oldsource>CILINDER ENTER</oldsource>
        <translation>INGRESSO CILINDRO </translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenuTp.cpp" line="120"/>
        <source>CYLINDER EXIT</source>
        <oldsource>CILINDER EXIT</oldsource>
        <translation>CILINDRO IN USCITA</translation>
    </message>
</context>
<context>
    <name>TaskNavigationMenuWp</name>
    <message>
        <location filename="menu/task/taskNavigationMenuWp.cpp" line="56"/>
        <source>Back</source>
        <translation>--INDIETRO--</translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenuWp.cpp" line="58"/>
        <source>Sort by dist</source>
        <translation>Elenca per dist.</translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenuWp.cpp" line="59"/>
        <source>Sort by name</source>
        <translation>Elenca per nome</translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenuWp.cpp" line="111"/>
        <source>NAV</source>
        <translation>NAVIGA</translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenuWp.cpp" line="111"/>
        <source>Waypoint set</source>
        <translation>Waypoint impostato</translation>
    </message>
</context>
<context>
    <name>TaskNavigationMenuZoom</name>
    <message>
        <location filename="menu/task/taskNavigationMenuZoom.cpp" line="57"/>
        <source>Zoom In</source>
        <translation>Ingrandisci</translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenuZoom.cpp" line="59"/>
        <source>Zoom Out</source>
        <translation>Riduci</translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenuZoom.cpp" line="61"/>
        <source>Zoom to Fit</source>
        <translation>Adatta</translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenuZoom.cpp" line="63"/>
        <source>Back</source>
        <translation>--INDIETRO--</translation>
    </message>
</context>
<context>
    <name>TaskTypeMenu</name>
    <message>
        <location filename="menu/task/taskTypeMenu.cpp" line="54"/>
        <source>RACE</source>
        <translation></translation>
    </message>
    <message>
        <location filename="menu/task/taskTypeMenu.cpp" line="56"/>
        <source>TIME GATES</source>
        <translation>START PILON</translation>
    </message>
    <message>
        <location filename="menu/task/taskTypeMenu.cpp" line="58"/>
        <source>ELAPSED TIME</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>TermalingDetectorMenu</name>
    <message>
        <location filename="menu/avionics/thermalingDetectorMenu.cpp" line="56"/>
        <source>DETECT ON</source>
        <translation>RILEVAMENTO ON</translation>
    </message>
    <message>
        <location filename="menu/avionics/thermalingDetectorMenu.cpp" line="58"/>
        <source>DETECT OFF</source>
        <translation>RILEVAMENTO OFF</translation>
    </message>
    <message>
        <location filename="menu/avionics/thermalingDetectorMenu.cpp" line="62"/>
        <source>SATURATION</source>
        <translation>SATURAZIONE</translation>
    </message>
    <message>
        <location filename="menu/avionics/thermalingDetectorMenu.cpp" line="68"/>
        <source>DECAY</source>
        <translation>DECADIMENTO</translation>
    </message>
    <message>
        <location filename="menu/avionics/thermalingDetectorMenu.cpp" line="74"/>
        <source>ACTIVATE THRESHOLD</source>
        <oldsource>ACTIVATE TRESHOLD</oldsource>
        <translation>SOGLIA DI ATTIVAZIONE</translation>
    </message>
    <message>
        <location filename="menu/avionics/thermalingDetectorMenu.cpp" line="80"/>
        <source>DEACTIVATE THRESHOLD</source>
        <oldsource>DEACTIVATE TRESHOLD</oldsource>
        <translation>SOGLIA DI DISATTIVAZIONE</translation>
    </message>
    <message>
        <location filename="menu/avionics/thermalingDetectorMenu.cpp" line="86"/>
        <source>BACK</source>
        <translation>--INDIETRO--</translation>
    </message>
    <message>
        <location filename="menu/avionics/thermalingDetectorMenu.cpp" line="149"/>
        <source>Saturation</source>
        <translation>Saturazione</translation>
    </message>
    <message>
        <location filename="menu/avionics/thermalingDetectorMenu.cpp" line="165"/>
        <source>Decay</source>
        <translation>Decadimento</translation>
    </message>
    <message>
        <location filename="menu/avionics/thermalingDetectorMenu.cpp" line="181"/>
        <source>Activate th.</source>
        <translation>Attiva rilevameto</translation>
    </message>
    <message>
        <location filename="menu/avionics/thermalingDetectorMenu.cpp" line="197"/>
        <source>Deactivate th.</source>
        <translation>Disattiva rilevamento</translation>
    </message>
    <message>
        <location filename="menu/avionics/thermalingDetectorMenu.cpp" line="214"/>
        <source>Activate/Deactivate thermaling detector.</source>
        <translation>Attiva/disattiva rilevamento termica</translation>
    </message>
    <message>
        <location filename="menu/avionics/thermalingDetectorMenu.cpp" line="216"/>
        <source>Saturation value. Used to limmit maximum turn integral to facilitate deactivation. Recommended 150.</source>
        <translation>Valore di saturazione. Usato per limitare la trasformazione integrale massima per facilitare la disattivazione. Raccomandato 150</translation>
    </message>
    <message>
        <location filename="menu/avionics/thermalingDetectorMenu.cpp" line="218"/>
        <source>Decay value. Each unit is equivalent to 5 deg/sec decay. Recommended 1.</source>
        <translation>Valore di decadimento. Ogni unita&apos; e&apos; equivalente a 5deg/sec. Raccomandato 1</translation>
    </message>
    <message>
        <location filename="menu/avionics/thermalingDetectorMenu.cpp" line="220"/>
        <source>Threshold that will trigger an activate event. Recommended 80.</source>
        <translation>Soglia che attiva  il passaggio all&apos;assistente in termica. Raccomandato 80</translation>
    </message>
    <message>
        <location filename="menu/avionics/thermalingDetectorMenu.cpp" line="222"/>
        <source>Threshold that will trigger an deactivate event. Recommended 60.</source>
        <translation>Soglia che attiva  il ritorno alla pagina principale. Raccomandato 60</translation>
    </message>
</context>
<context>
    <name>TextInput</name>
    <message>
        <location filename="TextInput.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>TimeGatesMenu</name>
    <message>
        <location filename="menu/task/timeGatesMenu.cpp" line="58"/>
        <source>ADD</source>
        <translation>AGGIUNGI</translation>
    </message>
    <message>
        <location filename="menu/task/timeGatesMenu.cpp" line="60"/>
        <location filename="menu/task/timeGatesMenu.cpp" line="131"/>
        <source>CLEAR</source>
        <translation>CANCELLA</translation>
    </message>
    <message>
        <location filename="menu/task/timeGatesMenu.cpp" line="62"/>
        <source>BACK</source>
        <translation>--INDIETRO--</translation>
    </message>
    <message>
        <location filename="menu/task/timeGatesMenu.cpp" line="64"/>
        <source>-TIME GATES-</source>
        <translation>-START PILON-</translation>
    </message>
    <message>
        <location filename="menu/task/timeGatesMenu.cpp" line="132"/>
        <source>DELETE?</source>
        <translation>CANCELLARE?</translation>
    </message>
    <message>
        <location filename="menu/task/timeGatesMenu.cpp" line="133"/>
        <source>CANCEL</source>
        <translation>ANNULLA</translation>
    </message>
    <message>
        <location filename="menu/task/timeGatesMenu.cpp" line="134"/>
        <source>DELETE</source>
        <translation>ELIMINA</translation>
    </message>
</context>
<context>
    <name>TimeInput</name>
    <message>
        <location filename="TimeInput.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>TimezoneMenu</name>
    <message>
        <location filename="menu/timezone/timezonemenu.cpp" line="39"/>
        <source>UP</source>
        <translation>AUMENTA</translation>
    </message>
    <message>
        <location filename="menu/timezone/timezonemenu.cpp" line="41"/>
        <source>DOWN</source>
        <translation>DIMINUISCI</translation>
    </message>
    <message>
        <location filename="menu/timezone/timezonemenu.cpp" line="43"/>
        <source>BACK</source>
        <translation>--INDIETRO--</translation>
    </message>
    <message>
        <location filename="menu/timezone/timezonemenu.cpp" line="76"/>
        <source>Timezone modifications requires reboot to take effect!</source>
        <translation>La modifica del fuso orario richiede il riavvio per avere effetto</translation>
    </message>
</context>
<context>
    <name>TurnpointEditMenu</name>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="74"/>
        <source>-TURN POINT-</source>
        <translation>-BOE-</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="78"/>
        <source>-GOAL-</source>
        <translation></translation>
    </message>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="81"/>
        <source>LINE</source>
        <translation>LINEA</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="82"/>
        <source>CILINDER ENTER</source>
        <translation>CILINDRO IN ENTRATA</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="83"/>
        <source>CILINDER EXIT</source>
        <translation>CIKINDRO IN USCITA</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="86"/>
        <source>-RADIUS/LENGTH-</source>
        <translation>-RAGGIO/LUNGHEZZA-</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="96"/>
        <source>ADD</source>
        <translation>AGGIUNGI</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="100"/>
        <source>SAVE</source>
        <translation>SALVA</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="104"/>
        <source>BACK</source>
        <translation>--INDIETRO--</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="106"/>
        <source>SET SSS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="108"/>
        <source>SET ESS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="110"/>
        <location filename="menu/task/turnpointEditMenu.cpp" line="187"/>
        <source>DELETE</source>
        <translation>ELIMINA</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="173"/>
        <location filename="menu/task/turnpointEditMenu.cpp" line="179"/>
        <location filename="menu/task/turnpointEditMenu.cpp" line="196"/>
        <location filename="menu/task/turnpointEditMenu.cpp" line="217"/>
        <location filename="menu/task/turnpointEditMenu.cpp" line="222"/>
        <source>INFO</source>
        <translation></translation>
    </message>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="217"/>
        <source>TP ADDED</source>
        <translation>BOA AGGIUNTA</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="222"/>
        <source>TP SAVED</source>
        <translation>BOA SALVATA</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="173"/>
        <source>SSS SAVED</source>
        <translation>SSS SALVATO</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="179"/>
        <source>ESS SAVED</source>
        <translation>ESS SALVATO</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="184"/>
        <source>CLEAR</source>
        <translation>CANCELLA</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="185"/>
        <source>DELETE?</source>
        <translation>CANCELLARE?</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="186"/>
        <source>CANCEL</source>
        <translation>ANNULLA</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="196"/>
        <source>TP DELETED</source>
        <translation>BOA CANCELLATA</translation>
    </message>
</context>
<context>
    <name>TurnpointMenu</name>
    <message>
        <location filename="menu/task/turnpointMenu.cpp" line="56"/>
        <source>ADD</source>
        <translation>AGGIUNGI</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointMenu.cpp" line="58"/>
        <location filename="menu/task/turnpointMenu.cpp" line="104"/>
        <source>CLEAR</source>
        <translation>CANCELLA</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointMenu.cpp" line="60"/>
        <source>BACK</source>
        <translation>--INDIETRO--</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointMenu.cpp" line="62"/>
        <source>-TPs-</source>
        <translation>-BOE-</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointMenu.cpp" line="105"/>
        <source>DELETE?</source>
        <translation>CANCELLARE?</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointMenu.cpp" line="106"/>
        <source>CANCEL</source>
        <translation>ANNULLA</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointMenu.cpp" line="107"/>
        <source>DELETE</source>
        <translation>ELIMINA</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointMenu.cpp" line="132"/>
        <source>Move/Edit</source>
        <translation>Sposta/Edita</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointMenu.cpp" line="136"/>
        <source>Up</source>
        <translation>Su</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointMenu.cpp" line="137"/>
        <source>Down</source>
        <translation>Giù</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointMenu.cpp" line="138"/>
        <source>Edit</source>
        <translation>Edita</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointMenu.cpp" line="165"/>
        <source>Add a new turnpoint from an existing waypoint.</source>
        <translation>Aggiungi una nuova boa da waypoint esistente</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointMenu.cpp" line="167"/>
        <source>Clear all turnpoints.</source>
        <translation>Cancella tutte le boe</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointMenu.cpp" line="171"/>
        <source>Edit/modify turnpoint and set custom SSS and ESS.</source>
        <translation>Edita/Modifica boe e imposta SSS e ESS personalizzati</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointMenu.cpp" line="179"/>
        <source>LINE</source>
        <translation>LINEA</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointMenu.cpp" line="180"/>
        <source>CYLINDER ENTER</source>
        <oldsource>CILINDER ENTER</oldsource>
        <translation>INGRESSO CILINDRO </translation>
    </message>
    <message>
        <location filename="menu/task/turnpointMenu.cpp" line="181"/>
        <source>CYLINDER EXIT</source>
        <oldsource>CILINDER EXIT</oldsource>
        <translation>CIKINDRO IN USCITA</translation>
    </message>
</context>
<context>
    <name>USBDialog</name>
    <message>
        <location filename="ui/usbdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>USBDialogMenu</name>
    <message>
        <location filename="menu/usbDialog/usbdialogmenu.cpp" line="42"/>
        <source>GLIDES TO USB</source>
        <translation>VOLI VIA USB</translation>
    </message>
    <message>
        <location filename="menu/usbDialog/usbdialogmenu.cpp" line="44"/>
        <source>COPY BOOTLOADER</source>
        <translation>COPIA BOOTLOADER</translation>
    </message>
    <message>
        <location filename="menu/usbDialog/usbdialogmenu.cpp" line="46"/>
        <source>DUMP TO USB</source>
        <translation>SCARICA VIA USB</translation>
    </message>
    <message>
        <location filename="menu/usbDialog/usbdialogmenu.cpp" line="48"/>
        <source>HIDE</source>
        <translation>NASCONDI</translation>
    </message>
</context>
<context>
    <name>USBListMenu</name>
    <message>
        <location filename="menu/usbDialog/usblistmenu.cpp" line="128"/>
        <source>COPYING GLIDES...</source>
        <translation>COPIA VOLI...</translation>
    </message>
    <message>
        <location filename="menu/usbDialog/usblistmenu.cpp" line="136"/>
        <location filename="menu/usbDialog/usblistmenu.cpp" line="162"/>
        <source>ERROR UNMOUNT /mnt/USB</source>
        <translation>ERRORE SMONTANDO /mnt/USB</translation>
    </message>
    <message>
        <location filename="menu/usbDialog/usblistmenu.cpp" line="173"/>
        <source>MOUNT FS TO /mnt/USB</source>
        <translation>MONTA FS SU /mnt/USB</translation>
    </message>
    <message>
        <location filename="menu/usbDialog/usblistmenu.cpp" line="177"/>
        <source>ERROR MOUNTING /mnt/USB</source>
        <translation>ERRORE MONTANDO /mnt/USB</translation>
    </message>
    <message>
        <source>ERROR UNMOUNT /mnt</source>
        <translation type="vanished">ERRORE SMONTANDO/mnt</translation>
    </message>
    <message>
        <location filename="menu/usbDialog/usblistmenu.cpp" line="139"/>
        <location filename="menu/usbDialog/usblistmenu.cpp" line="165"/>
        <source>DONE</source>
        <translation>FATTO</translation>
    </message>
    <message>
        <source>MOUNT FS TO /mnt</source>
        <translation type="vanished">MONTA FS SU/mnt</translation>
    </message>
    <message>
        <source>ERROR MOUNTING /mnt</source>
        <translation type="vanished">ERRORE MONTANDO /mnt</translation>
    </message>
</context>
<context>
    <name>UiMenu</name>
    <message>
        <location filename="menu/general/uiMenu.cpp" line="84"/>
        <source>GET UI</source>
        <translation>SCARICA UI</translation>
    </message>
    <message>
        <location filename="menu/general/uiMenu.cpp" line="86"/>
        <source>BACK</source>
        <translation>--INDIETRO--</translation>
    </message>
    <message>
        <location filename="menu/general/uiMenu.cpp" line="119"/>
        <location filename="menu/general/uiMenu.cpp" line="127"/>
        <location filename="menu/general/uiMenu.cpp" line="154"/>
        <location filename="menu/general/uiMenu.cpp" line="172"/>
        <location filename="menu/general/uiMenu.cpp" line="187"/>
        <location filename="menu/general/uiMenu.cpp" line="208"/>
        <source>INFO</source>
        <translation>INFO</translation>
    </message>
    <message>
        <location filename="menu/general/uiMenu.cpp" line="119"/>
        <source>Check Conn</source>
        <translation>Verifica Conn.</translation>
    </message>
    <message>
        <location filename="menu/general/uiMenu.cpp" line="127"/>
        <source>UI Set</source>
        <translation>UI IMPOSTATA</translation>
    </message>
    <message>
        <location filename="menu/general/uiMenu.cpp" line="140"/>
        <source>Get the user interfaces form your account or the standard recommended one if no account connected.</source>
        <translation>Scarica l&apos;interfaccia utente (UI) dal tuo Account o lo standard consigliato se nessun account è collegato</translation>
    </message>
    <message>
        <location filename="menu/general/uiMenu.cpp" line="142"/>
        <source>Back</source>
        <translation>--INDIETRO--</translation>
    </message>
    <message>
        <location filename="menu/general/uiMenu.cpp" line="154"/>
        <source>Conn OK</source>
        <translation>Connessione OK</translation>
    </message>
    <message>
        <location filename="menu/general/uiMenu.cpp" line="160"/>
        <location filename="menu/general/uiMenu.cpp" line="194"/>
        <source>ERROR</source>
        <translation>ERRORE</translation>
    </message>
    <message>
        <location filename="menu/general/uiMenu.cpp" line="160"/>
        <source>Not Connected</source>
        <translation>Non Connesso</translation>
    </message>
    <message>
        <location filename="menu/general/uiMenu.cpp" line="172"/>
        <source>Success!</source>
        <translation>Successo!</translation>
    </message>
    <message>
        <location filename="menu/general/uiMenu.cpp" line="187"/>
        <source>Set to default.xml</source>
        <translation>Imposta default .xml</translation>
    </message>
    <message>
        <location filename="menu/general/uiMenu.cpp" line="194"/>
        <source>Not Updated</source>
        <translation>Non aggiornato</translation>
    </message>
    <message>
        <location filename="menu/general/uiMenu.cpp" line="208"/>
        <source>Downloading</source>
        <translation>Scaricamento ...</translation>
    </message>
</context>
<context>
    <name>VarioAverageMenu</name>
    <message>
        <location filename="menu/varioAverage/varioaveragemenu.cpp" line="48"/>
        <source>UP</source>
        <translation>AUMENTA</translation>
    </message>
    <message>
        <location filename="menu/varioAverage/varioaveragemenu.cpp" line="50"/>
        <source>DOWN</source>
        <translation>DIMINUISCI</translation>
    </message>
    <message>
        <location filename="menu/varioAverage/varioaveragemenu.cpp" line="52"/>
        <source>BACK</source>
        <translation>--INDIETRO--</translation>
    </message>
</context>
<context>
    <name>VarioScale</name>
    <message>
        <location filename="varioscale.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>VolumeMenu</name>
    <message>
        <location filename="menu/sound/volumemenu.cpp" line="45"/>
        <source>UP</source>
        <translation>AUMENTA</translation>
    </message>
    <message>
        <location filename="menu/sound/volumemenu.cpp" line="47"/>
        <source>DOWN</source>
        <translation>RIDUCI</translation>
    </message>
    <message>
        <location filename="menu/sound/volumemenu.cpp" line="49"/>
        <source>BACK</source>
        <translation>--INDIETRO--</translation>
    </message>
</context>
<context>
    <name>Warning</name>
    <message>
        <location filename="Warning.ui" line="17"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>WaypointEditMenu</name>
    <message>
        <location filename="menu/task/waypointEdit.cpp" line="80"/>
        <source>-LATITUDE-</source>
        <translation>-LATITUDINE-</translation>
    </message>
    <message>
        <location filename="menu/task/waypointEdit.cpp" line="82"/>
        <source>-LONGITUDE-</source>
        <translation>-LONGITUDINE-</translation>
    </message>
    <message>
        <location filename="menu/task/waypointEdit.cpp" line="84"/>
        <source>-NAME-</source>
        <translation>-NOME-</translation>
    </message>
    <message>
        <location filename="menu/task/waypointEdit.cpp" line="86"/>
        <source>-DESC-</source>
        <translation></translation>
    </message>
    <message>
        <location filename="menu/task/waypointEdit.cpp" line="88"/>
        <source>-HEIGHT-</source>
        <translation>-QUOTA-</translation>
    </message>
    <message>
        <location filename="menu/task/waypointEdit.cpp" line="92"/>
        <source>ADD</source>
        <translation>AGGIUNGI</translation>
    </message>
    <message>
        <location filename="menu/task/waypointEdit.cpp" line="94"/>
        <source>SAVE</source>
        <translation>SALVA</translation>
    </message>
    <message>
        <location filename="menu/task/waypointEdit.cpp" line="97"/>
        <source>DELETE</source>
        <translation>CANCELLA</translation>
    </message>
    <message>
        <location filename="menu/task/waypointEdit.cpp" line="100"/>
        <source>BACK</source>
        <translation>--INDIETRO--</translation>
    </message>
    <message>
        <location filename="menu/task/waypointEdit.cpp" line="207"/>
        <source>Height</source>
        <translation>Quota</translation>
    </message>
    <message>
        <location filename="menu/task/waypointEdit.cpp" line="223"/>
        <location filename="menu/task/waypointEdit.cpp" line="228"/>
        <location filename="menu/task/waypointEdit.cpp" line="234"/>
        <source>INFO</source>
        <translation></translation>
    </message>
    <message>
        <location filename="menu/task/waypointEdit.cpp" line="223"/>
        <source>WP ADDED</source>
        <translation>WP AGGIUNTO</translation>
    </message>
    <message>
        <location filename="menu/task/waypointEdit.cpp" line="228"/>
        <source>WP INVALID</source>
        <translation>WP NON VALIDO</translation>
    </message>
    <message>
        <location filename="menu/task/waypointEdit.cpp" line="234"/>
        <source>WP UPDATED</source>
        <translation>WP AGGIORNATO</translation>
    </message>
</context>
<context>
    <name>WaypointLoad</name>
    <message>
        <location filename="menu/task/waypointLoad.cpp" line="57"/>
        <source>BACK</source>
        <translation>--INDIETRO--</translation>
    </message>
    <message>
        <location filename="menu/task/waypointLoad.cpp" line="90"/>
        <source>FILE</source>
        <translation></translation>
    </message>
    <message>
        <location filename="menu/task/waypointLoad.cpp" line="91"/>
        <source>ACTION?</source>
        <translation>AZIONE?</translation>
    </message>
    <message>
        <location filename="menu/task/waypointLoad.cpp" line="92"/>
        <source>REMOVE</source>
        <translation>RIMUOVI</translation>
    </message>
    <message>
        <location filename="menu/task/waypointLoad.cpp" line="93"/>
        <source>CANCEL</source>
        <translation>ANNULLA</translation>
    </message>
    <message>
        <location filename="menu/task/waypointLoad.cpp" line="94"/>
        <source>LOAD</source>
        <translation>CARICA</translation>
    </message>
    <message>
        <location filename="menu/task/waypointLoad.cpp" line="107"/>
        <location filename="menu/task/waypointLoad.cpp" line="113"/>
        <location filename="menu/task/waypointLoad.cpp" line="119"/>
        <location filename="menu/task/waypointLoad.cpp" line="122"/>
        <location filename="menu/task/waypointLoad.cpp" line="125"/>
        <source>ERROR</source>
        <translation type="unfinished">ERRORE</translation>
    </message>
    <message>
        <location filename="menu/task/waypointLoad.cpp" line="110"/>
        <location filename="menu/task/waypointLoad.cpp" line="116"/>
        <location filename="menu/task/waypointLoad.cpp" line="133"/>
        <source>INFO</source>
        <translation></translation>
    </message>
    <message>
        <location filename="menu/task/waypointLoad.cpp" line="110"/>
        <source>WPT LOADED</source>
        <translation>WP CARICATO</translation>
    </message>
    <message>
        <location filename="menu/task/waypointLoad.cpp" line="113"/>
        <source>LOAD FAILED</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/waypointLoad.cpp" line="116"/>
        <source>NOT ALL WPT LOADED</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/waypointLoad.cpp" line="119"/>
        <source>UNSUPPORTED FORMAT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/waypointLoad.cpp" line="122"/>
        <source>UNKNOWN FORMAT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/waypointLoad.cpp" line="125"/>
        <source>UNHANDLED STATE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/waypointLoad.cpp" line="133"/>
        <source>WPT REMOVED</source>
        <translation>WP RIMOSSO</translation>
    </message>
</context>
<context>
    <name>WaypointMenu</name>
    <message>
        <location filename="menu/task/waypointMenu.cpp" line="59"/>
        <source>ADD WP</source>
        <translation>AGGIUNGI WP</translation>
    </message>
    <message>
        <location filename="menu/task/waypointMenu.cpp" line="61"/>
        <source>CLEAR ALL WPs</source>
        <translation>CANCELLA TUTTI I WP</translation>
    </message>
    <message>
        <location filename="menu/task/waypointMenu.cpp" line="63"/>
        <source>SAVE WP LIST</source>
        <translation>SALVA LISTA WP</translation>
    </message>
    <message>
        <location filename="menu/task/waypointMenu.cpp" line="65"/>
        <source>LOAD WP LIST</source>
        <translation>CARICA LISTA WP</translation>
    </message>
    <message>
        <location filename="menu/task/waypointMenu.cpp" line="67"/>
        <source>BACK</source>
        <translation>--INDIETRO--</translation>
    </message>
    <message>
        <location filename="menu/task/waypointMenu.cpp" line="108"/>
        <source>CLEAR</source>
        <translation>CANCELLA</translation>
    </message>
    <message>
        <location filename="menu/task/waypointMenu.cpp" line="109"/>
        <source>DELETE?</source>
        <translation>CANCELLARE?</translation>
    </message>
    <message>
        <location filename="menu/task/waypointMenu.cpp" line="110"/>
        <location filename="menu/task/waypointMenu.cpp" line="138"/>
        <source>CANCEL</source>
        <translation>ANNULLA</translation>
    </message>
    <message>
        <location filename="menu/task/waypointMenu.cpp" line="111"/>
        <source>DELETE</source>
        <translation>CANCELLA</translation>
    </message>
    <message>
        <location filename="menu/task/waypointMenu.cpp" line="136"/>
        <source>FILE EXISTS</source>
        <translation>IL FILE ESISTE</translation>
    </message>
    <message>
        <location filename="menu/task/waypointMenu.cpp" line="137"/>
        <source>REPLACE?</source>
        <translation>RIMPIAZZA?</translation>
    </message>
    <message>
        <location filename="menu/task/waypointMenu.cpp" line="139"/>
        <source>REPLACE</source>
        <translation>RIMPIAZZA</translation>
    </message>
    <message>
        <location filename="menu/task/waypointMenu.cpp" line="150"/>
        <source>INFO</source>
        <translation></translation>
    </message>
    <message>
        <location filename="menu/task/waypointMenu.cpp" line="150"/>
        <source>File saved</source>
        <translation>File salvato</translation>
    </message>
    <message>
        <location filename="menu/task/waypointMenu.cpp" line="154"/>
        <source>ERROR</source>
        <translation>ERRORE</translation>
    </message>
    <message>
        <location filename="menu/task/waypointMenu.cpp" line="154"/>
        <source>File not saved</source>
        <translation>File non salvato</translation>
    </message>
    <message>
        <location filename="menu/task/waypointMenu.cpp" line="188"/>
        <source>Lat</source>
        <translation></translation>
    </message>
    <message>
        <location filename="menu/task/waypointMenu.cpp" line="189"/>
        <source>Lon</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>WaypointSelectMenu</name>
    <message>
        <location filename="menu/task/waypointSelectMenu.cpp" line="53"/>
        <source>BACK</source>
        <translation>--INDIETRO--</translation>
    </message>
    <message>
        <location filename="menu/task/waypointSelectMenu.cpp" line="98"/>
        <source>Lat</source>
        <translation></translation>
    </message>
    <message>
        <location filename="menu/task/waypointSelectMenu.cpp" line="99"/>
        <source>Lon</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>WiFiDeviceMenu</name>
    <message>
        <location filename="menu/general/WiFiDevice.cpp" line="81"/>
        <source>WIFI SEARCH</source>
        <translation>RICERCA WIFI</translation>
    </message>
    <message>
        <location filename="menu/general/WiFiDevice.cpp" line="83"/>
        <location filename="menu/general/WiFiDevice.cpp" line="156"/>
        <source>BACK</source>
        <translation>--INDIETRO--</translation>
    </message>
    <message>
        <location filename="menu/general/WiFiDevice.cpp" line="151"/>
        <source>Rescan WiFi networks</source>
        <translation>Ri-scansiona reti WIFI</translation>
    </message>
    <message>
        <location filename="menu/general/WiFiDevice.cpp" line="153"/>
        <source>No WiFiAdapter available
</source>
        <translation>Nessun adattatore WiFi presente</translation>
    </message>
</context>
<context>
    <name>WindAverageMenu</name>
    <message>
        <location filename="menu/wind/windaveragemenu.cpp" line="54"/>
        <source>UP</source>
        <translation>AUMENTA</translation>
    </message>
    <message>
        <location filename="menu/wind/windaveragemenu.cpp" line="56"/>
        <source>DOWN</source>
        <translation>DIMINUISCI</translation>
    </message>
    <message>
        <location filename="menu/wind/windaveragemenu.cpp" line="58"/>
        <source>BACK</source>
        <translation>--INDIETRO--</translation>
    </message>
</context>
<context>
    <name>WindDSpreadMenu</name>
    <message>
        <location filename="menu/wind/winddspreadmenu.cpp" line="46"/>
        <source>UP</source>
        <translation>AUMENTA</translation>
    </message>
    <message>
        <location filename="menu/wind/winddspreadmenu.cpp" line="48"/>
        <source>DOWN</source>
        <translation>DIMINUISCI</translation>
    </message>
    <message>
        <location filename="menu/wind/winddspreadmenu.cpp" line="50"/>
        <source>BACK</source>
        <translation>INDIETRO</translation>
    </message>
</context>
<context>
    <name>WindMenu</name>
    <message>
        <location filename="menu/wind/windmenu.cpp" line="58"/>
        <source>NUM READINGS</source>
        <translation>NUM LETTURE</translation>
    </message>
    <message>
        <location filename="menu/wind/windmenu.cpp" line="64"/>
        <source>NUM AVERAGE</source>
        <translation>NUM MEDIA</translation>
    </message>
    <message>
        <location filename="menu/wind/windmenu.cpp" line="70"/>
        <source>DATA SPREAD</source>
        <translation>DIFFUSIONE DATI</translation>
    </message>
    <message>
        <location filename="menu/wind/windmenu.cpp" line="74"/>
        <source>RMS ERROR</source>
        <translation>ERRORE RMS</translation>
    </message>
    <message>
        <location filename="menu/wind/windmenu.cpp" line="78"/>
        <source>BACK</source>
        <translation>INDIETRO</translation>
    </message>
    <message>
        <location filename="menu/wind/windmenu.cpp" line="129"/>
        <source>Num Readings</source>
        <translation>Numero di letture</translation>
    </message>
    <message>
        <location filename="menu/wind/windmenu.cpp" line="145"/>
        <source>Num Avg.</source>
        <translation>Numero media</translation>
    </message>
    <message>
        <location filename="menu/wind/windmenu.cpp" line="161"/>
        <source>Data spreed.</source>
        <translation>Diffusione dati</translation>
    </message>
    <message>
        <location filename="menu/wind/windmenu.cpp" line="177"/>
        <source>RMS Err.</source>
        <translation>Err. RMS</translation>
    </message>
    <message>
        <location filename="menu/wind/windmenu.cpp" line="194"/>
        <source>Set number of readings to for wind algorithm.</source>
        <translation>Imposta numero di letture per algoritmo vento</translation>
    </message>
    <message>
        <location filename="menu/wind/windmenu.cpp" line="196"/>
        <source>Set average number of readings for wind algorithm.</source>
        <translation>Imposta media numero letture per algoritmo vento</translation>
    </message>
    <message>
        <location filename="menu/wind/windmenu.cpp" line="198"/>
        <source>Set data spread for wind algorithm.</source>
        <translation>Imposta diffusione dati per algoritmo vento</translation>
    </message>
    <message>
        <location filename="menu/wind/windmenu.cpp" line="200"/>
        <source>Set RMS error for wind algorithm.</source>
        <translation>Immposta errore RMS per algoritmo vento</translation>
    </message>
</context>
<context>
    <name>WindReadingsMenu</name>
    <message>
        <location filename="menu/wind/windreadingsmenu.cpp" line="54"/>
        <source>UP</source>
        <translation>AUMENTA</translation>
    </message>
    <message>
        <location filename="menu/wind/windreadingsmenu.cpp" line="56"/>
        <source>DOWN</source>
        <translation>DIMINUISCI</translation>
    </message>
    <message>
        <location filename="menu/wind/windreadingsmenu.cpp" line="58"/>
        <source>BACK</source>
        <translation>INDIETRO</translation>
    </message>
</context>
<context>
    <name>WindRmsErrorMenu</name>
    <message>
        <location filename="menu/wind/windrmserrormenu.cpp" line="54"/>
        <source>UP</source>
        <translation>AUMENTA</translation>
    </message>
    <message>
        <location filename="menu/wind/windrmserrormenu.cpp" line="56"/>
        <source>DOWN</source>
        <translation>DIMINUISCI</translation>
    </message>
    <message>
        <location filename="menu/wind/windrmserrormenu.cpp" line="58"/>
        <source>BACK</source>
        <translation>INDIETRO</translation>
    </message>
</context>
<context>
    <name>glideDetails</name>
    <message>
        <location filename="GlideDetails.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
</context>
</TS>
