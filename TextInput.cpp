/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "TextInput.h"
#include "ui_TextInput.h"
#include "VarioQt.h"
#include "mainui.h"

TextInput::TextInput(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TextInput)
{
    ui->setupUi(this);

    connect(VarioQt::instance().getMainUI(),SIGNAL(mainGpioPressed(int)),this,SLOT(gpioPressed(int)));
    connect(VarioQt::instance().getMainUI(),SIGNAL(mainGpioLongPressed(int)),this,SLOT(gpioLongPressed(int)));
    connect(ui->letterChooser,SIGNAL(select(QChar)),this, SLOT(letterChoused(QChar)));

    /*
    ui->currentChar->setDrawBorders(false);
    ui->prewChar->setDrawBorders(false);
    ui->nextChar->setDrawBorders(false);

    //ui->currentChar->setNoBorder();
    ui->prewChar->setNoBorder();
    ui->nextChar->setNoBorder();

    ui->currentChar->setAlignment(Qt::AlignHCenter);
    ui->prewChar->setAlignment(Qt::AlignHCenter);
    ui->nextChar->setAlignment(Qt::AlignHCenter);

    ui->currentChar->setValue("A");
    ui->prewChar->setValue("B");
    ui->nextChar->setValue("C");
    */

    ui->textBox->setCaption("Text input");
    ui->textBox->setValueAlign(Qt::AlignLeft);

    move(240/2 - width()/2,320/2 - height()/2);
}

TextInput::~TextInput()
{
    delete ui;
}

QString TextInput::value() const
{
    return _value;
}

void TextInput::setValue(const QString &value)
{
    _value = value;
    ui->textBox->setValue(value);
}

void TextInput::setLetters()
{
    ui->letterChooser->setLetters();
}

void TextInput::setNumbers()
{
    ui->letterChooser->setNumbers();
}

void TextInput::setMixt()
{
    ui->letterChooser->setMixt();
}

void TextInput::setAllPrintAscii()
{
    ui->letterChooser->setAllPrintAscii();
}

void TextInput::showEvent(QShowEvent *e)
{
    QWidget::showEvent(e);
    VarioQt::instance().pushNewWidget(this);
}

void TextInput::hideEvent(QHideEvent *e)
{
    QWidget::hideEvent(e);
    VarioQt::instance().deleteLastWidget();
}

void TextInput::gpioPressed(int btn)
{
    if(btn == Vario::GPIO1)
    {
        ui->letterChooser->previous();
    }
    if(btn == Vario::GPIO2)
    {
        ui->letterChooser->next();
    }
    if(btn == Vario::GPIO3)
    {
        ui->letterChooser->choose();
    }
}

void TextInput::gpioLongPressed(int btn)
{
    if(btn == Vario::GPIO3)
    {
        close();
    }
    if(btn == Vario::GPIO1)
    {
        _value.chop(1);
        setValue(_value);
    }
    if(btn == Vario::GPIO2)
    {
        setValue("");
    }
}

void TextInput::letterChoused(QChar letter)
{
    _value += letter;
    setValue(_value);
}
