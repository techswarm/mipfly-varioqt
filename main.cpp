/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mainui.h"

#include <QApplication>
#include <QMetaType>
#include <QFontDatabase>
#include "navUtils/OpenAirParser.h"

#include "serial/serialconnection.h"

#include "NMEA/NMEAParser.h"

#include "navUtils/coord.hpp"
#include "navUtils/glide.hpp"
#include "navUtils/Wind.hpp"
#include "navUtils/variocompute.h"
#include "navUtils/headingcorrector.h"

#include "settings/glidemanager.h"
#include "settings/locationmanager.h"

#include "utils/ublox_commands.h"
#include "utils/datetimemanager.h"
#include "utils/init.h"
#include "utils/loggerinterface.h"


#include "testing/gpioemulator.h"

#include "threads/gpsthread.h"
#include "threads/sendcorrectionsthread.h"
#include "threads/takeoffthread.h"
#include "threads/igcthread.h"
#include "threads/playsoundthread.h"
#include "threads/gpiolistenthread.h"
#include "threads/fb2spithread.h"
#include "threads/inputThread.h"
#include "threads/BTRemoteConn.h"
#include "threads/AirSpaceThread.h"
#include "threads/ADBTCPThread.h"
#include "threads/obexThread.h"
#include "threads/flightlogthread.h"

#include <QtDebug>
#include "stats/glidesmanager.h"
#include "settings/windmanager.h"
#include "settings/tzmanager.h"

#include "Geo/locationsthreadmgr.h"
#include "sound/varioMixer.h"

#include "VarioQt.h"
#include <qabstractanimation.h>

#include <QTranslator>

#include "navUtils/elevation.h"

#include "Warning.h"

#include "utils/BTServer.h"
#include "utils/updatemanager.h"

#include "navUtils/livetrack.h"
#include "navUtils/hgtcontainer.h"
#include "navUtils/termalingDetector.h"

#include "utils/glider.h"
#include "GPIO/gpiowatcher.h"



#ifdef FAKE_GPS
#define USE_GPS
#endif

const char *VARIO_SERIAL = "/dev/ttyS5";



int main(int argc, char *argv[])
{
#ifdef __ARMEL__
    system("rm -r /var/log/*");
#endif
    qRegisterMetaType<Coord>("Coord");

    // Metadata for settings
    QApplication::setOrganizationName("Techswarm");
    QApplication::setOrganizationDomain("techswarm.ro");
    QApplication::setApplicationName("MipFLY");
    QApplication a(argc, argv);
    QFontDatabase::addApplicationFont("resources/fonts/PixelOperator.ttf");
    QFontDatabase::addApplicationFont("resources/fonts/PixelOperator8.ttf");

    bool confirmTurnon = false;

    if(argc > 1)
    {
        if(strcmp(argv[1],"confirmTurnOn")==0)
        {
            confirmTurnon = true;
        }
        if(strcmp(argv[1],"recoveryMode")==0)
        {
            VarioQt::instance().setRecoveryMode(true);
        }
    }
#ifdef __ARMEL__
#ifndef USE_MIPFLY_LOGGER
    //setup GPIOS if mipflylogger is not used
    setupGPIOs();
#endif
    TzManager tzm;
    tzm.setActTZ();
    system("hwclock --hctosys --localtime");
    QSettings::setDefaultFormat(QSettings::IniFormat);
    QSettings::setPath(QSettings::IniFormat,QSettings::UserScope,"/home/techswarm/");
    system("mv /mnt/mipfly/version.txt /root/");
    system("rm /mnt/mipfly/xinitrc");
    system("systemctl enable getty@ttyGS0.service");

    //workarround for bt not active
    system("modprobe btusb");
    system("systemctl start bluetooth");
#else
    QSettings::setDefaultFormat(QSettings::IniFormat);
    QSettings::setPath(QSettings::IniFormat,QSettings::UserScope,"~");
#endif
    QSettings settings;
    qDebug()<<settings.fileName();
    QList<StoppableThread*> threads;
    GPIOListenThread gpio1(Vario::GPIO1);
    threads.append(&gpio1);
    GPIOListenThread gpio2(Vario::GPIO2);
    threads.append(&gpio2);
    GPIOListenThread gpio3(Vario::GPIO3);
    threads.append(&gpio3);
    gpio1.exportPin();
    gpio2.exportPin();
    gpio3.exportPin();

    //GPIOWatcher gpio1w(Vario::GPIO1_INT,Vario::GPIO1);
    //gpio1w.setThreadToDisable(&gpio1);
    //gpio1w.watch();

    //GPIOWatcher gpio2w(Vario::GPIO2_INT,Vario::GPIO2);
    //gpio2w.setThreadToDisable(&gpio2);
    //gpio2w.watch();

    //GPIOWatcher gpio3w(Vario::GPIO3_INT,Vario::GPIO3);
    //gpio3w.setThreadToDisable(&gpio3);
    //gpio3w.watch();

    Glider glider;
    VarioQt::instance().setGlider(&glider);

    VarioQt::instance().setVarioParser(new NmeaParser());
    VarioQt::instance().setGpsParser(new NmeaParser());

    VarioQt::instance().setNameManager(new NameManager());


    //VarioCompute varioCompute(varioParser);
    VarioQt::instance().setVarioCompute(new VarioCompute(
                                            *(VarioQt::instance().getVarioParser())));

    OLC olc(100,10);
    VarioQt::instance().setOlc(&olc);

    GlideManager glideMgr;
    VarioQt::instance().setGlideManager(&glideMgr);
    WindManager wndMgr;

    VarioQt::instance().setWindManager(&wndMgr);

    VarioQt::instance().getGpsParser()->setTimeFromGPS = setTimeFromGPS;

    Glide glide(*(VarioQt::instance().getGpsParser()),
                *(VarioQt::instance().getVarioParser()),
                glideMgr);


    VarioQt::instance().setLastLift(new LastLift(nullptr,
                                                 VarioQt::instance().getVarioCompute()));
    QObject::connect(VarioQt::instance().getGpsParser(), SIGNAL(coordUpdate(Coord)), VarioQt::instance().getLastLift(), SLOT(pushCoord(Coord)));

    VarioQt::instance().setWind(new Wind(
                                    *(VarioQt::instance().getGpsParser()),
                                    wndMgr
                                    ));

    VarioSound varioSound;
    SoundManager *sndMgr = new SoundManager(&varioSound);
    delete (sndMgr);

    TermalingDetector td;

    VarioQt::instance().setTermalingDetector(&td);


    VarioQt::instance().setVarioSound(&varioSound);

    LocationManager locManager(*(VarioQt::instance().getGpsParser()),
                               *(VarioQt::instance().getVarioParser())
                               );
    VarioQt::instance().setLocationManager(&locManager);


    LocationsThreadMgr locThMgr(locManager);
    VarioQt::instance().setLocationsThreadMgr(&locThMgr);
    locThMgr.startThread();

    //    SendCorrectionsThread sendThread(serial2, headingCrtr);
#ifdef USE_GPS
    QThread::msleep(100);
    SerialConnection gpsSerial("/dev/ttyS2", 115200);

    SerialConnection varioSerial(VARIO_SERIAL, 115200); // vario serial

    GPSThread gpsThread(gpsSerial, *(VarioQt::instance().getGpsParser()), "resources/recorded/raw.log");
#ifdef FAKE_CONDOR
    gpsThread.udpSocket = new QUdpSocket(&gpsThread);
    gpsThread.udpSocket->bind(QHostAddress::Any, 44000);
#endif
    threads.append(&gpsThread);

    ADBTCPThread tcpThread;



    GPSThread varThread(varioSerial,
                        *(VarioQt::instance().getVarioParser())
                        , "resources/recorded/raw.log");
    VarioQt::instance().setVarioSerialThread(&varThread);
    LoggerInterface li;
    VarioQt::instance().setLoggerInterface(&li);

#ifdef FAKE_CONDOR
    varThread.udpSocket = new QUdpSocket(&gpsThread);
    varThread.udpSocket->bind(QHostAddress::Any, 44001);
#endif

    threads.append(&varThread);

    InputThread inputThread;
    inputThread.start();

    VarioQt::instance().setInputThread(&inputThread);

    VarioQt::instance().setElevation(new Elevation());

    //threads.append(&inputThread);

#endif

#ifdef USE_SOUND
    PlaySoundThread soundThread;
    soundThread.setPriority(QThread::TimeCriticalPriority);
    soundThread.setObjectName("Sound thread");
    threads.append(&soundThread);

    soundThread.playAppBing();
#endif

    TakeoffThread takeoffThread(*(VarioQt::instance().getGpsParser()),
                                *(VarioQt::instance().getVarioParser())
                            #ifdef USE_SOUND
                                , soundThread
                                , varioSound
                            #endif
                                );
    VarioQt::instance().setTaleOffThread(&takeoffThread);
    IGCThread igcThread(*(VarioQt::instance().getGpsParser()),*(VarioQt::instance().getVarioParser()), takeoffThread);
    FlightLogThread flightlogThread(*(VarioQt::instance().getGpsParser()),*(VarioQt::instance().getVarioParser()), takeoffThread);

    threads.append(&takeoffThread);
    threads.append(&igcThread);
    threads.append(&flightlogThread);

    FaiAssistCompute fac;
    VarioQt::instance().setFaiAssistCompute(&fac);

#ifdef USE_GPS
#ifndef ONE_SERIAL
    gpsThread.start();
#endif
    varThread.start();
#endif

    initMicrocontroller(varioSerial);
    takeoffThread.start();
    igcThread.start();
    flightlogThread.start();

#ifdef USE_SOUND
    soundThread.start();
#endif
    gpio1.start();
    gpio2.start();
    gpio3.start();


#ifdef __ARMEL__
    Fb2SPIThread spiThread;

    spiThread.setObjectName("FBToSPI");

    VarioQt::instance().setFb2SPIThread(&spiThread);

    threads.append(&spiThread);
    QObject::connect(&gpio1,&GPIOListenThread::forceUpdate,&spiThread,&Fb2SPIThread::forceUpdate);
    QObject::connect(&gpio2,&GPIOListenThread::forceUpdate,&spiThread,&Fb2SPIThread::forceUpdate);
    QObject::connect(&gpio3,&GPIOListenThread::forceUpdate,&spiThread,&Fb2SPIThread::forceUpdate);

#endif



    QTranslator translator;
    VarioQt::instance().setTranslator(&translator);

    translator.load("languages/"+settings.value("language","").toString());
    a.installTranslator(&translator);

    /* Font + size */
    VarioQt::instance().setMnuFontName(settings.value("mnuFontName", "Pixel Operator").toString());
    VarioQt::instance().setMnuFontSize(settings.value("mnuFontSize", 16).toInt());

    QFontDatabase::addApplicationFont("resources/fonts/"+VarioQt::instance().getMnuFontName()+".ttf");

    AirSpaceThread airSpaceThread;
    threads.append(&airSpaceThread);
    VarioQt::instance().setAirSpaceThread(&airSpaceThread);
    airSpaceThread.start();

    NetStats ns;
    VarioQt::instance().setNetStats(&ns);

    HGTContainer hGTContainer;
    VarioQt::instance().setHGTContainer(&hGTContainer);

    waypointManager wpm;
    VarioQt::instance().setWaypointManager(&wpm);

    Task task;
    VarioQt::instance().setTask(&task);

    ObexThread obexThread;
    threads.append(&obexThread);
    obexThread.start();
    MainUI w;

    w.setConfirmTurnOn(confirmTurnon);

    w.connectTo(gpio1);
    w.connectTo(gpio2);
    w.connectTo(gpio3);
    //w.connectTo(gpio1w);
    //w.connectTo(gpio2w);
    //w.connectTo(gpio3w);

    w.connectTo(inputThread);

    VarioQt::instance().setMainUI(&w);
    BTRemoteConnectThread btConnThr;
    btConnThr.start();

    w.show();

    QObject::connect(&takeoffThread, &TakeoffThread::takeoff, &w, &MainUI::startDisplayElapsed);
    QObject::connect(&takeoffThread, &TakeoffThread::landed, &w, &MainUI::stopDisplayElapsed);
    QObject::connect(&takeoffThread, &TakeoffThread::takeoff, &w, &MainUI::resetInitValues);


    QObject::connect(VarioQt::instance().getGpsParser(),SIGNAL(messageReceived(QString)),
                     &tcpThread, SLOT(GPSNMEAReceived(QString)),Qt::QueuedConnection);
    QObject::connect(VarioQt::instance().getVarioParser(),SIGNAL(vSpeed(int)),
                     &tcpThread, SLOT(setVspeed(int)),Qt::QueuedConnection);
    QObject::connect(VarioQt::instance().getVarioParser(),SIGNAL(baroHeight(int)),
                     &tcpThread, SLOT(setBaroHeight(int)),Qt::QueuedConnection);

    QObject::connect(&obexThread,SIGNAL(fileReceivedMsg(QString)),&w, SLOT(displayMessageAlarm(QString)));

    LiveTrack lt;

    VarioQt::instance().setLiveTrack(&lt);





    QObject::connect(VarioQt::instance().getGpsParser(),SIGNAL(coordUpdate(Coord)),
                     &lt,SLOT(setCoord(Coord)));
    QObject::connect(VarioQt::instance().getGpsParser(),SIGNAL(gpsHeightChanged(int)),
                     &lt,SLOT(setHeight(int)));
    QObject::connect(VarioQt::instance().getGpsParser(),SIGNAL(speedKmph(qreal)),
                     &lt,SLOT(setSpeed(qreal)));
    QObject::connect(VarioQt::instance().getGpsParser(),SIGNAL(course(qreal)),
                     &lt,SLOT(setHeading(qreal)));


    BTServer *btServer = BTServer::BTServerFab();
    if(btServer != nullptr)
    {
        qDebug()<<"Starting BT server";
        btServer->startServer();
#ifdef __ARMEL__
        system("systemctl start obexpush.service");
#endif
    }
    else
    {
        //wait for a few seconds and try again
        QThread::msleep(2000);
        btServer = BTServer::BTServerFab();
        if(btServer != nullptr)
        {
            qDebug()<<"Starting BT server";
            btServer->startServer();
#ifdef __ARMEL__
        system("systemctl start obexpush.service");
#endif
        }
    }


    if(btServer != nullptr)
    {
        QObject::connect(VarioQt::instance().getGpsParser(),SIGNAL(messageReceived(QString)),
                         btServer, SLOT(GPSNMEAReceived(QString)),Qt::QueuedConnection);
        QObject::connect(VarioQt::instance().getVarioParser(),SIGNAL(vSpeed(int)),
                         btServer, SLOT(setVspeed(int)),Qt::QueuedConnection);
        QObject::connect(VarioQt::instance().getVarioParser(),SIGNAL(baroHeight(int)),
                         btServer, SLOT(setBaroHeight(int)),Qt::QueuedConnection);
        QObject::connect(VarioQt::instance().getVarioParser(),SIGNAL(tasChanged(qreal)),
                         btServer, SLOT(setTASkmh(qreal)),Qt::QueuedConnection);
    }

    VarioQt::instance().setBTServer(btServer);

    QObject::connect(&takeoffThread, &TakeoffThread::takeoff, &w, &MainUI::takeoffDetected);
    QObject::connect(&takeoffThread, &TakeoffThread::landed, &w, &MainUI::landedDetected);
    QObject::connect(&glide, &Glide::glideChanged, &w, &MainUI::setGlide);


#ifdef __ARMEL__
    spiThread.start();
#endif

    int returnCode = a.exec();

    for (auto thread : threads) {
        if (thread != nullptr) {
            thread->stop();
            thread->wait();
        }
    }

    qDebug()<<"Threads closed";

    if(btServer != nullptr)
    {
        delete btServer;
    }

    inputThread.saveSettings();
    inputThread.terminate();
    inputThread.stop();
    inputThread.wait();
    qDebug()<<"inputThread thread closed";
    btConnThr.stop();
    btConnThr.wait();
    qDebug()<<"btConnThr thread closed";

    qDebug()<<"Exiting with code "<<returnCode;
    return returnCode;
}
