/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef COMPASS_H
#define COMPASS_H

#include <QWidget>
#include <QtMath>

namespace Ui {
class Compass;
}

class Compass : public QWidget
{
    Q_OBJECT
public:
    explicit Compass(QWidget *parent = 0);
    ~Compass();

    void setUnit(const QString &value);

public slots:
    void setWindSpeed(qreal speed);
    void setHeading(qreal headingDegrees);
    void setWindDir(qreal dirDegrees);
    void setThermalDir(qreal dirDegrees);

protected:
    void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;

private:
    Ui::Compass *ui;

    qreal _angle = 0.0;
    qreal _windDir = 0.0;
    //if > 2pi there is no termal computed yet
    qreal _thermalDir = 10.0;

    qreal _wpDir = M_PI_2;
    qreal _wpDirOptimised = M_PI_2;

    QString unit;


    int _xc;
    int _yc;
    int _r1;
    int _r2;

    int bottom = 20;
    int right = 15;

    void drawCircles(QPainter &painter);
    void drawLetters(QPainter &painter);
    void drawArrow(QPainter &painter);
    void drawTaskArrow(QPainter &painter);
};

#endif // COMPASS_H
