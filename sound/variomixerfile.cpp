/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "variomixerfile.h"
#include <QFile>

VarioMixerFile::VarioMixerFile(QString filename, QObject *parent) : QObject(parent)
{
    numIntervalSlots = -1;
    loadData(filename);
}

VarioMixerFile::VarioMixerFile(QObject *parent)
{
    numIntervalSlots = -1;
}

bool VarioMixerFile::loadData(QString filename)
{
    QFile file(filename);
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text))return false;
    int toneIterator = 0;
    while(!file.atEnd())
    {
        QString data = file.readLine();
        data = data.simplified();
        if(data.contains("tone"))
        {
            data = data.right(data.size()-data.indexOf("=")-1);
            QStringList dataTokens = data.split(",");
            sndData[toneIterator].interval = dataTokens.at(0).toFloat();
            sndData[toneIterator].freqHz = dataTokens.at(1).toInt();
            sndData[toneIterator].cycleMs = dataTokens.at(2).toInt();
            sndData[toneIterator].duty = dataTokens.at(3).toInt();
            toneIterator++;
            numIntervalSlots = toneIterator;
        }
        if(data.contains("ClimbToneOnThreshold"))
        {
            climbToneOnThreshold = data.right(data.size()-data.indexOf("=")-1).toFloat();
        }
        if(data.contains("ClimbToneOffThreshold"))
        {
            climbToneOffThreshold = data.right(data.size()-data.indexOf("=")-1).toFloat();
        }
        if(data.contains("SinkToneOnThreshold"))
        {
            sinkToneOnThreshold = data.right(data.size()-data.indexOf("=")-1).toFloat();
        }
        if(data.contains("SinkToneOffThreshold"))
        {
            sinkToneOffThreshol = data.right(data.size()-data.indexOf("=")-1).toFloat();
        }
    }
    //TODO implement std fallback in case of error
    return true;
}

int VarioMixerFile::getItIndex(float vSpeedMps)
{
    if(numIntervalSlots == -1)return -1;
    if(vSpeedMps<=-10)return 0;
    for(int i=1;i<numIntervalSlots;i++)
    {
        if(vSpeedMps>=sndData[i-1].interval && vSpeedMps<sndData[i].interval)return i;
    }
    return numIntervalSlots;
}

float VarioMixerFile::getRatio(float vSpeedMps, int itIndex)
{
    if(numIntervalSlots == -1)return 0;
    if(itIndex == 0)return 0;
    if(itIndex == numIntervalSlots)return 0;
    float totalDiff = sndData[itIndex-1].interval - sndData[itIndex].interval;
    float intervalToVSpeedDiff = vSpeedMps - sndData[itIndex].interval;
    float ratio = intervalToVSpeedDiff/totalDiff;
    return ratio;
}

float VarioMixerFile::getFreq(float ratio, int itIndex)
{
    if(numIntervalSlots == -1)return 1000;
    if(itIndex == 0)return sndData[0].freqHz;
    if(itIndex == numIntervalSlots)return sndData[numIntervalSlots-1].freqHz;
    return (sndData[itIndex-1].freqHz - sndData[itIndex].freqHz)*ratio + sndData[itIndex].freqHz;
}

int VarioMixerFile::getCycleMs(float ratio, int itIndex)
{
    if(numIntervalSlots == -1)return 100;
    if(itIndex == 0)return sndData[0].cycleMs;
    if(itIndex == numIntervalSlots)return sndData[numIntervalSlots-1].cycleMs;
    return (float)(sndData[itIndex-1].cycleMs - sndData[itIndex].cycleMs)*ratio + sndData[itIndex].cycleMs;
}
int VarioMixerFile::getDuty(float ratio, int itIndex)
{
    if(numIntervalSlots == -1)return 50;
    if(itIndex == 0)return sndData[0].duty;
    if(itIndex == numIntervalSlots)return sndData[numIntervalSlots-1].duty;
    return (float)(sndData[itIndex-1].duty - sndData[itIndex].duty)*ratio + sndData[itIndex].duty;
}

VarioMixerFileData VarioMixerFile::getMixerData(float vSpeedMps)
{
    VarioMixerFileData ret;
    int itIndex = getItIndex(vSpeedMps);
    float ratio = getRatio(vSpeedMps,itIndex);
    ret.interval = vSpeedMps;
    ret.freqHz = getFreq(ratio,itIndex);
    ret.duty = getDuty(ratio,itIndex);
    ret.cycleMs = getCycleMs(ratio,itIndex);
    return ret;
}

float VarioMixerFile::getClimbToneOnThreshold() const
{
    return climbToneOnThreshold;
}

float VarioMixerFile::getClimbToneOffThreshold() const
{
    return climbToneOffThreshold;
}

float VarioMixerFile::getSinkToneOnThreshold() const
{
    return sinkToneOnThreshold;
}

float VarioMixerFile::getSinkToneOffThreshol() const
{
    return sinkToneOffThreshol;
}
