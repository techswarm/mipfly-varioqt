/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "varioMixer.h"
#include "varioSound.h"
#include <QSettings>
#include "utils/vario_common.h"
#include <QDebug>

#include <iostream>

// SoundMixer

SoundMixer::SoundMixer() : card("default") {
    snd_mixer_open(&handle, 0);
    snd_mixer_attach(handle, card);
    snd_mixer_selem_register(handle, NULL, NULL);
    snd_mixer_load(handle);
}

SoundMixer::~SoundMixer() {
    snd_mixer_close(handle);
}

SimpleElement SoundMixer::findSimpleElement(SimpleMixer &simpleMixer) {
    snd_mixer_elem_t *elem = snd_mixer_find_selem(handle, simpleMixer.getSid());
    return SimpleElement(elem);
}

// SimpleMixer

SimpleMixer::SimpleMixer(const char *name) {
    QSettings settings;
    snd_mixer_selem_id_malloc(&sid);
    snd_mixer_selem_id_set_index(sid, 0);
    snd_mixer_selem_id_set_name(sid, name);
}

SimpleMixer::~SimpleMixer() {
    snd_mixer_selem_id_free(sid);
}

snd_mixer_selem_id_t* SimpleMixer::getSid() const {
    return sid;
}

// SimpleElement

SimpleElement::SimpleElement(snd_mixer_elem_t *elem) {
    this->elem = elem;
}

VolumeRange SimpleElement::getPlaybackVolRange() {
    VolumeRange range;
    snd_mixer_selem_get_playback_volume_range(elem, &range.min, &range.max);
    return range;
}

int SimpleElement::setPlaybackVol(long value) {
    return snd_mixer_selem_set_playback_volume_all(elem, value);
}

// Volume

SoundManager::SoundManager(VarioSound *varioSound) :
    #ifndef __ARMEL__
    simpleMixer("Master")
  #else
    simpleMixer("Power Amplifier")
  #endif

{
    QSettings settings;
    percent = settings.value(Vario::SETTINGS_VOL,40).toInt();
    set();


    square_ = settings.value("SoundManager_SQUARE",true).toBool();
    silentOnGround_ = settings.value("SoundManager_SILENTONGROUND",true).toBool();
    linear_ = settings.value("SoundManager_LINEAR",true).toBool();
    upThr_  = settings.value("SoundManager_UPTHR",10).toInt();
    upF0_   = settings.value("SoundManager_UPF0",500).toInt();
    upF1000_= settings.value("SoundManager_UPF1000",1000).toInt();
    upI0_   = settings.value("SoundManager_UPI0",600).toInt();
    upI1000_= settings.value("SoundManager_UPI1000",200).toInt();
    downThr_= settings.value("SoundManager_DOWNTHR",-300).toInt();
    downF0_ = settings.value("SoundManager_DOWNF0",450).toInt();
    downF1000_= settings.value("SoundManager_DOWNF1000",150).toInt();
    sniffThr_ = settings.value("SoundManager_SNIFF",0).toInt();

    testing_ = false;
    testCMPS_ = 10;
    varioSound_ = varioSound;

    if(varioSound_ != nullptr)
        varioSound_->varioSoundChangeSetting(this);
}

void SoundManager::saveSettings()
{
    QSettings settings;

    settings.setValue("SoundManager_SQUARE",square_);
    settings.setValue("SoundManager_SILENTONGROUND",silentOnGround_);
    settings.setValue("SoundManager_LINEAR",linear_);
    settings.setValue("SoundManager_UPTHR",upThr_);
    settings.setValue("SoundManager_UPF0",upF0_);
    settings.setValue("SoundManager_UPF1000",upF1000_);
    settings.setValue("SoundManager_UPI0",upI0_);
    settings.setValue("SoundManager_UPI1000",upI1000_);
    settings.setValue("SoundManager_DOWNTHR",downThr_);
    settings.setValue("SoundManager_DOWNF0",downF0_);
    settings.setValue("SoundManager_DOWNF1000",downF1000_);
    settings.setValue("SoundManager_SNIFF",sniffThr_);
    settings.sync();
}

SoundManager::~SoundManager()
{
    testing_ = false;
    if(varioSound_ != nullptr)
        varioSound_->varioSoundChangeSetting(this);
}

bool SoundManager::getLinear() const
{
    return linear_;
}

void SoundManager::setLinear(bool linear)
{
    linear_ = linear;
    saveSettings();
    updateSettingsToSoundDriver();
}

int SoundManager::getSniffThr() const
{
    return sniffThr_;
}

void SoundManager::setSniffThr(int sniffThr)
{
    sniffThr_ = sniffThr;
    saveSettings();
    updateSettingsToSoundDriver();
}

void SoundManager::updateSettingsToSoundDriver()
{
    if(varioSound_ != nullptr)
        varioSound_->varioSoundChangeSetting(this);
}

void SoundManager::setPercent(int value) {
    if (value >= 0 && value <= 100) {
        percent = value;
    }
    set();
}

void SoundManager::set() {
    SimpleElement elem = mixer.findSimpleElement(simpleMixer);
    const VolumeRange range = elem.getPlaybackVolRange();
    elem.setPlaybackVol(percent * range.max / 100);
}

void SoundManager::increase() {
    if (percent <= 100 - SoundManager::STEP) {
        percent += SoundManager::STEP;
    }
    set();
}

void SoundManager::decrease() {
    if (percent >= 0 + SoundManager::STEP) {
        percent -= SoundManager::STEP;
    }
    set();
}

int SoundManager::getPercent() const {
    return percent;
}

bool SoundManager::getSquare() const
{
    return square_;
}

void SoundManager::setSquare(bool square)
{
    square_ = square;
    saveSettings();
    updateSettingsToSoundDriver();
}

bool SoundManager::getSilentOnGround() const
{
    return silentOnGround_;
}

void SoundManager::setSilentOnGround(bool silent)
{
    silentOnGround_ = silent;
    saveSettings();
    updateSettingsToSoundDriver();
}

int SoundManager::getUpThr() const
{
    return upThr_;
}

void SoundManager::setUpThr(int upThr)
{
    upThr_ = upThr;
    saveSettings();
    updateSettingsToSoundDriver();
}

int SoundManager::getUpF0() const
{
    return upF0_;
}

void SoundManager::setUpF0(int upF0)
{
    upF0_ = upF0;
    saveSettings();
    updateSettingsToSoundDriver();
}

int SoundManager::getUpF1000() const
{
    return upF1000_;
}

void SoundManager::setUpF1000(int upF1000)
{
    upF1000_ = upF1000;
    saveSettings();
    updateSettingsToSoundDriver();
}

int SoundManager::getUpI0() const
{
    return upI0_;
}

void SoundManager::setUpI0(int upI0)
{
    upI0_ = upI0;
    saveSettings();
    updateSettingsToSoundDriver();
}

int SoundManager::getUpI1000() const
{
    return upI1000_;
}

void SoundManager::setUpI1000(int upI1000)
{
    upI1000_ = upI1000;
    saveSettings();
    updateSettingsToSoundDriver();
}

int SoundManager::getDownThr() const
{
    return downThr_;
}

void SoundManager::setDownThr(int downThr)
{
    downThr_ = downThr;
    saveSettings();
    updateSettingsToSoundDriver();
}

int SoundManager::getDownF0() const
{
    return downF0_;
}

void SoundManager::setDownF0(int downF0)
{
    downF0_ = downF0;
    saveSettings();
    updateSettingsToSoundDriver();
}

int SoundManager::getDownF1000() const
{
    return downF1000_;
}

void SoundManager::setDownF1000(int downF1000)
{
    downF1000_ = downF1000;
    saveSettings();
    updateSettingsToSoundDriver();
}

bool SoundManager::getTesting() const
{
    return testing_;
}

void SoundManager::setTesting(bool testing)
{
    testing_ = testing;
    saveSettings();
    updateSettingsToSoundDriver();
}

int SoundManager::getTestCMPS() const
{
    return testCMPS_;
}

void SoundManager::setTestCMPS(int testCMPS)
{
    testCMPS_ = testCMPS;
    updateSettingsToSoundDriver();
}
