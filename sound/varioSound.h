/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __VARIOGUI_VARIOSOUND_H
#define __VARIOGUI_VARIOSOUND_H

#include <QObject>

#include <alsa/asoundlib.h>
#include <alsa/pcm.h>
#include <math.h>
#include <stdint.h>
#include "varioMixer.h"
#include <QString>
#include <QStringList>
#include "variomixerfile.h"

class SoundManager;

class VarioSound:public QObject {
    Q_OBJECT
public:
    VarioSound(QObject *parent = nullptr);
    ~VarioSound();

    void varioSoundTask();
    void varioSoundSetCMPS(int localCMPS);
    void varioSoundInit();
    void varioSoundPlayFile(const QString filenameToPlay);
    void varioSoundChangeSetting(const SoundManager *soundMgr);

    void setFlyingStatus(bool flying);
    bool getFlyingStatus();

    void setUseConfigFile(bool value);
    void setConfigFilename(QString name);

private:
    VarioMixerFile vmf;

    static const int BUFFER_LEN=44100;
    static const int _1MS_LEN = (BUFFER_LEN / 1000);

    char* device = (char*)"default";
    snd_output_t* output = NULL;
    int16_t buffer1Hz[BUFFER_LEN];
    int16_t buffer[BUFFER_LEN];
    int fs = BUFFER_LEN;

    bool useConfigFile;
    int configFileComputedFreq;

    int computedBuffLen;
    int offset;
    int oldFreqk = 0;
    int oldFreqkMinus1 = 0;
    int reinitPostscale = 0;

    bool silentZone;

    snd_pcm_t* handle;
    snd_pcm_sframes_t frames;

    double s = 0;

    int sinePos = 0;
    int sineWaveComputedLength=0;
    int beep=0;
    int halfBeep=0;
    int beepActive=0;
    signed int cmps = 0;
    int playFile=0;
    void computeSineWave1Hz(bool square = false);
    void computeSineWave(int f, int ms);
    int computeFreqvency();
    int computeInterval();

    QString filename;

    bool square_;
    bool silentOnGround_;
    bool flying_ = false;
    int upThr_;
    int upF0_;
    int upI0_;
    int downThr_;
    int downF0_;
    int sniffThr_;
    bool linear_;

    int upFRange;
    int downFRange;
    int upIRange;

    bool testPlay_;

};
#endif
