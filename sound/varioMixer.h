/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __VARIOGUI_VARIOMIXER_H
#define __VARIOGUI_VARIOMIXER_H

#include <alsa/asoundlib.h>
#include <alsa/mixer.h>
#include <QObject>
#include "varioSound.h"

class VarioSound;

struct VolumeRange
{
    long max;
    long min;
};

class SimpleElement
{
    snd_mixer_elem_t *elem;

public:
    SimpleElement(snd_mixer_elem_t *elem);

    VolumeRange getPlaybackVolRange();
    int setPlaybackVol(long value);
};

class SimpleMixer
{
    snd_mixer_selem_id_t *sid;

public:
    SimpleMixer(const char *name);
    ~SimpleMixer();

    snd_mixer_selem_id_t* getSid() const;
};

class SoundMixer
{
    const char *card;
    snd_mixer_t *handle;
public:
    SoundMixer();
    ~SoundMixer();

    SimpleElement findSimpleElement(SimpleMixer &simpleMixer);
};



// Class encapsulating all components

class SoundManager
{
    int percent = 40;
    SoundMixer mixer;
    SimpleMixer simpleMixer;
    void set();

public:
    const static int STEP = 2;

    SoundManager(VarioSound *varioSound = nullptr);

    void increase();
    void decrease();
    void setPercent(int value);
    int getPercent() const;

    bool getSquare() const;
    void setSquare(bool square);

    bool getSilentOnGround() const;
    void setSilentOnGround(bool silent);

    int getUpThr() const;
    void setUpThr(int upThr);

    int getUpF0() const;
    void setUpF0(int upF0);

    int getUpF1000() const;
    void setUpF1000(int upF1000);

    int getUpI0() const;
    void setUpI0(int upI0);

    int getUpI1000() const;
    void setUpI1000(int upI1000);

    int getDownThr() const;
    void setDownThr(int downThr);

    int getDownF0() const;
    void setDownF0(int downF0);

    int getDownF1000() const;
    void setDownF1000(int downF1000);

    bool getTesting() const;
    void setTesting(bool testing);

    int getTestCMPS() const;
    void setTestCMPS(int testCMPS);

    ~SoundManager();
    bool getLinear() const;
    void setLinear(bool linear);

    int getSniffThr() const;
    void setSniffThr(int sniffThr);

private:
    bool square_;
    bool silentOnGround_;
    bool linear_;
    int sniffThr_;
    int upThr_;
    int upF0_;
    int upF1000_;
    int upI0_;
    int upI1000_;
    int downThr_;
    int downF0_;
    int downF1000_;

    bool testing_;
    int testCMPS_;

    VarioSound *varioSound_;

    void saveSettings();
    void updateSettingsToSoundDriver();
};

#endif
