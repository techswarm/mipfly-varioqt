/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VARIOMIXERFILE_H
#define VARIOMIXERFILE_H

#include <QObject>

class VarioMixerFileData
{
public:
    float interval;
    int freqHz;
    int cycleMs;
    int duty;
};

class VarioMixerFile : public QObject
{
    Q_OBJECT
public:
    explicit VarioMixerFile(QString filename, QObject *parent = nullptr);
    explicit VarioMixerFile(QObject *parent = nullptr);
    bool loadData(QString filename);

    float getClimbToneOnThreshold() const;

    float getClimbToneOffThreshold() const;

    float getSinkToneOnThreshold() const;

    float getSinkToneOffThreshol() const;

    int getItIndex(float vSpeedMps);
    float getRatio(float vSpeedMps, int itIndex);
    float getFreq(float ratio, int itIndex);
    int getCycleMs(float ratio, int itIndex);
    int getDuty(float ratio, int itIndex);
    VarioMixerFileData getMixerData(float vSpeedMps);

private:
    VarioMixerFileData sndData[20];
    float climbToneOnThreshold;
    float climbToneOffThreshold;
    float sinkToneOnThreshold;
    float sinkToneOffThreshol;
    int numIntervalSlots;
signals:

public slots:
};

#endif // VARIOMIXERFILE_H
