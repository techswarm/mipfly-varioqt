/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "varioSound.h"
#include "varioMixer.h"

#include <alsa/asoundlib.h>
#include <alsa/pcm.h>
#include <math.h>
#include <stdint.h>
#include <QDebug>

#include <iostream>
#include <QSettings>

void VarioSound::computeSineWave1Hz(bool square)
{
    if(square)
    {
        int k;
        for(k = 0; k < BUFFER_LEN / 2; k++) {
            //		buffer1Hz[k] = (int16_t)((sin(2 * M_PI * 1 / fs * k))*(float)INT16_MAX); // sine wave value generation
            buffer1Hz[k] = INT16_MIN;
        }
        for (k = BUFFER_LEN / 2; k < BUFFER_LEN; k++) {
            buffer1Hz[k] = INT16_MAX;
        }
    }
    else
    {
        int k;
        for(k = 0; k < BUFFER_LEN; k++) {
            buffer1Hz[k] = (int16_t)((sin(2 * M_PI * 1 / fs * k))*(float)INT16_MAX/4); // sine wave value generation
            buffer1Hz[k] += (int16_t)((sin(2 * M_PI * 1 / fs * k*2))*(float)INT16_MAX/4); // sine wave value generation
            buffer1Hz[k] += (int16_t)((sin(2 * M_PI * 1 / fs * k*3))*(float)INT16_MAX/4); // sine wave value generation
            buffer1Hz[k] += (int16_t)((sin(2 * M_PI * 1 / fs * k*4))*(float)INT16_MAX/4); // sine wave value generation
        }
    }
}

void VarioSound::varioSoundSetCMPS(int localCMPS)
{
    if(!testPlay_)
        cmps = localCMPS;
    if(useConfigFile)
    {
        float vSpeedMps = (float)localCMPS/100.0;
        int id = vmf.getItIndex(vSpeedMps);
        float ratio = vmf.getRatio(vSpeedMps,id);
        configFileComputedFreq = vmf.getFreq(ratio,id);
    }
}

void VarioSound::computeSineWave(int f, int ms)
{
    //return;
    int k;
    ms = ms * _1MS_LEN;
    sineWaveComputedLength = ms;
    for(k = 0; k < sineWaveComputedLength; k++) {
        buffer[k] = buffer1Hz[sinePos];
        sinePos += f;
        if(sinePos >= BUFFER_LEN)
            sinePos = sinePos - BUFFER_LEN;
    }
}

void VarioSound::varioSoundInit()
{
    // ERROR HANDLING
    int err;
    device = "default";
    if((err = snd_pcm_open(&handle, device, SND_PCM_STREAM_PLAYBACK, 0)) < 0) {
        printf("Playback open error: %s\n", snd_strerror(err));
        exit(EXIT_FAILURE);
    }

    if((err = snd_pcm_set_params(
            handle, SND_PCM_FORMAT_S16_LE, SND_PCM_ACCESS_RW_INTERLEAVED, 1, BUFFER_LEN, 1, 100000)) < 0) {
        printf("Playback open error: %s\n", snd_strerror(err));
        exit(EXIT_FAILURE);
    }
    computeSineWave1Hz(square_);

}

void VarioSound::setFlyingStatus(bool flying)
{
    flying_ = flying;
}

bool VarioSound::getFlyingStatus()
{
    return flying_;
}

void VarioSound::setUseConfigFile(bool value)
{
    useConfigFile = value;
    QSettings settings;
    settings.setValue("sndMixer/UseConfigFile",useConfigFile);
}

void VarioSound::setConfigFilename(QString name)
{
    setUseConfigFile(true);
    QSettings settings;
    settings.setValue("sndMixer/ConfigFileName",name);
    useConfigFile = vmf.loadData("soundm/"+name);
}

int VarioSound::computeFreqvency()
{
    //return 600;
    if(cmps > downThr_ && cmps < upThr_)
        return 0;
    else if(cmps >= upThr_) {
        double range = (double)cmps / 1000;
        if(range > 1)
            range = 1;
        return upF0_ + upFRange * range;
    } else {
        double range = (double)cmps / 1000;
        if(range < -1)
            range = -1;
        return downF0_ + downFRange * range;
    }
}

int VarioSound::computeInterval()
{
    int ret = 0;
    if(cmps >= upThr_) {
        double range = (double)cmps / 1000;
        if(range > 1)
            range = 1;
        if(!linear_)range = (log(range/2)/7+1);
        ret = upI0_ - upIRange * range;
    }
    else
    {
        return 0;
    }
    if(ret < 0)return 1;
    return ret;
}


void VarioSound::varioSoundPlayFile(const QString filenameToPlay)
{
    filename = filenameToPlay;
    playFile = 1;
}

void VarioSound::varioSoundChangeSetting(const SoundManager *soundMgr)
{
    upThr_ = soundMgr->getUpThr();
    downThr_ = soundMgr->getDownThr();

    square_ = soundMgr->getSquare();

    silentOnGround_ = soundMgr->getSilentOnGround();

    upI0_ = soundMgr->getUpI0();
    downF0_ = soundMgr->getDownF0();
    upF0_ = soundMgr->getUpF0();

    upIRange = soundMgr->getUpI0() - soundMgr->getUpI1000();
    downFRange = soundMgr->getDownF0() - soundMgr->getDownF1000();
    upFRange = soundMgr->getUpF1000() - soundMgr->getUpF0();
    linear_ = soundMgr->getLinear();
    sniffThr_ = soundMgr->getSniffThr();

    if(soundMgr->getTesting() == true)
    {
        testPlay_ = true;
        cmps = soundMgr->getTestCMPS();
    }
    else testPlay_ = false;

    computeSineWave1Hz(square_);
}

VarioSound::VarioSound(QObject *parent) : QObject(parent)
{
    QSettings settings;
    useConfigFile = settings.value("sndMixer/UseConfigFile",false).toBool();
    if(useConfigFile)
        useConfigFile = vmf.loadData("soundm/"+settings.value("sndMixer/ConfigFileName","default.txt").toString());
    varioSoundInit();
}

VarioSound::~VarioSound()
{
    qDebug()<<"VarioSound destructed";
}

void VarioSound::varioSoundTask()
{
    //reinit sound to prevent hang bug
    //happens ~ every 15 minutes
    if(++reinitPostscale>18000)
    {
        reinitPostscale = 0;
        snd_pcm_prepare(handle);
    }
    if(playFile)
    {
        qDebug()<<"PLAY:"<<filename;
        system(qPrintable(QString("aplay ")+ QString(filename) + QString(" &")));
        playFile = 0;
        beep = 0;
    }


    if(silentOnGround_ == true && flying_ == false && testPlay_ == false){
        //without sdn_pcm_prepare the sound stream closes and needs to be reopened
        sleep(1);
        snd_pcm_prepare(handle);
        return;
    }

    if(useConfigFile)
    {
        float vSpeedMps = (float)cmps/100;
        int i;

        if(!silentZone && (vSpeedMps < vmf.getClimbToneOffThreshold() && vSpeedMps > vmf.getSinkToneOffThreshol()))
            silentZone = true;
        if(silentZone && (vSpeedMps > vmf.getClimbToneOnThreshold() || vSpeedMps < vmf.getSinkToneOnThreshold()))
            silentZone = false;

        if(!silentZone)
        {
            VarioMixerFileData sndData = vmf.getMixerData(vSpeedMps);
            int beep = sndData.cycleMs;
            halfBeep = beep * sndData.duty / 100;
            for(i = 10; i <= halfBeep; i += 10) {
                //            printf("%d \r\n", i);
                if(halfBeep - i < 10)
                    computeSineWave(configFileComputedFreq, 10 + halfBeep - i);
                else
                    computeSineWave(configFileComputedFreq, 10);
                frames = snd_pcm_writei(handle, buffer, sineWaveComputedLength); // sending values to sound driver
            }
            for(; i <= beep; i += 10) {
                if(beep - i < 10)
                    computeSineWave(0, 10 + beep - i);
                else
                    computeSineWave(0, 10);
                frames = snd_pcm_writei(handle, buffer, sineWaveComputedLength); // sending values to sound driver
            }
        }
        else
        {
            computeSineWave(0, 50);
            frames = snd_pcm_writei(handle, buffer, sineWaveComputedLength); // sending values to sound driver
        }
    }
    else
    {
        if(beep > 0) {
            int i;
            halfBeep = beep / 2;
            for(i = 10; i < halfBeep; i += 10) {
                //            printf("%d \r\n", i);
                if(halfBeep - i < 10)
                    computeSineWave(computeFreqvency(), 10 + halfBeep - i);
                else
                    computeSineWave(computeFreqvency(), 10);
                frames = snd_pcm_writei(handle, buffer, sineWaveComputedLength); // sending values to sound driver
            }
            for(i = 10; i < halfBeep; i += 10) {
                if(halfBeep - i < 10)
                    computeSineWave(0, 10 + halfBeep - i);
                else
                    computeSineWave(0, 10);
                frames = snd_pcm_writei(handle, buffer, sineWaveComputedLength); // sending values to sound driver
            }
            beep = 0;
        } else {
            if(cmps < upThr_) { // continous sound
                if(cmps < sniffThr_ || cmps >=0)
                {
                    computeSineWave(computeFreqvency(), 10);
                    frames = snd_pcm_writei(handle, buffer, sineWaveComputedLength); // sending values to sound driver
                }
                else
                {
                    if(cmps<0)
                    {
                        computeSineWave(1200, 20);
                        frames = snd_pcm_writei(handle, buffer, sineWaveComputedLength); // sending values to sound driver
                        computeSineWave(0, 40);
                        frames = snd_pcm_writei(handle, buffer, sineWaveComputedLength); // sending values to sound driver
                        computeSineWave(1200, 100);
                        frames = snd_pcm_writei(handle, buffer, sineWaveComputedLength); // sending values to sound driver
                        for(int i=0;i<=10 - cmps/3 && cmps<=9;i++)
                        {
                            computeSineWave(0, 10);
                            frames = snd_pcm_writei(handle, buffer, sineWaveComputedLength); // sending values to sound driver
                        }
                    }
                }
            } else {
                beep = computeInterval();
            }
        }
    }
}
