/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "letterchooser.h"
#include "ui_letterchooser.h"

#include <QtDebug>
#include <QKeyEvent>
#include <QPainter>

#include "utils/cstgraphics.h"
using Vario::drawStr;

static const QString UPPERCASE ("ABCDEFGHIJKLMNOPQRSTUVWXYZ ");
static const QString NUMBERS ("-.0123456789");

static const QString MIXT ("ABCDEFGHIJKLMNOPQRSTUVWXYZ -.0123456789");

static const QString ALLPRINTASCII ("!\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~");

LetterChooser::LetterChooser(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LetterChooser)
{
    ui->setupUi(this);

    letterSrc = &UPPERCASE;

//    updateView();
}

LetterChooser::~LetterChooser()
{
    if(isVisible())hide();
    delete ui;
}

void LetterChooser::setLetters() {
    letterSrc = &UPPERCASE;
    update();
}

void LetterChooser::setNumbers() {
    letterSrc = &NUMBERS;
    update();
}

void LetterChooser::setMixt() {
    letterSrc = &MIXT;
    update();
}

void LetterChooser::setAllPrintAscii() {
    letterSrc = &ALLPRINTASCII;
    update();
}

void LetterChooser::reset() {
    pos = 0;
}

void LetterChooser::next()  {
    pos++;
    if (pos == letterSrc->length()) {
        pos = 0;
    }
//    updateView();
    update();
}

void LetterChooser::previous()  {
    pos--;
    if (pos < 0) {
        pos = letterSrc->length() - 1;
    }
//    updateView();
    update();
}

/*
void LetterChooser::updateView() {
    ui->valCrt->setText(QString(UPPERCASE[pos]));
    ui->valPrev->setText(QString(prevLetter()));
    ui->valNext->setText(QString(nextLetter()));
}*/

const QChar LetterChooser::prevLetter() {
    int index = (pos - 1 < 0 ? letterSrc->length() - 1 : pos - 1);
    return (*letterSrc)[index];
}

const QChar LetterChooser::nextLetter() {
    int index = (pos + 1 == letterSrc->length() ? 0 : pos + 1);
    return (*letterSrc)[index];
}

void LetterChooser::choose()  {
    emit select((*letterSrc)[pos]);
}

void LetterChooser::esc()  {
    emit escape();
}

void LetterChooser::paintEvent(QPaintEvent *event) {
    QPainter painter(this);

    int x_bar1 = width() / 3;
    int x_bar2 = 2 * width() / 3;
    int d = 13;

    painter.drawLine(width() / 3, 0, width() / 3, height() - 1);
    painter.drawLine(2 * width() / 3, 0, 2 * width() / 3, height() - 1);

    drawStr(x_bar1 - d, 8, QString(prevLetter()), 16, painter, Qt::AlignRight);
    drawStr(width() / 2, 8, letterSrc->mid(pos, 1), 16, painter, Qt::AlignHCenter);
    drawStr(x_bar2 + d, 8, QString(nextLetter()), 16, painter);
}

