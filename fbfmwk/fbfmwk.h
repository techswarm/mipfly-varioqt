/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FBFMWK_H
#define FBFMWK_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

const int SCREEN_W = 640;
const int SCREEN_H = 480;

typedef uint32_t pixel_t;

void fbfmwk_init(int grayscale, int bpp);
void fbfmwk_cleanup(void);
pixel_t pixel(long x, long y, pixel_t *p);

#ifdef __cplusplus
}
#endif

#endif
