/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "fbfmwk.h"

#include <linux/fb.h> // framebuffer structs, defines etc.
#include <stdio.h> // perror
#include <fcntl.h>
#include <unistd.h> // close
#include <sys/ioctl.h>
#include <sys/mman.h>

static int fb_fd = -1;
static uint8_t* fbp; // Front buffer base pointer

static struct fb_fix_screeninfo finfo;
static struct fb_var_screeninfo vinfo;

void fbfmwk_cleanup()
{
#ifdef DEBUG
	puts("Framework cleanup");
#endif
	if(close(fb_fd) < 0) {
		perror("close");
	}
	long screensize = vinfo.yres_virtual * finfo.line_length;
	if(munmap(fbp, screensize) < 0) {
		perror("munmap");
	}
}

void fbfmwk_init(int grayscale, int bpp)
{

	if((fb_fd = open("/dev/fb0", O_RDWR)) < 0) {
		perror("open");
	}

	// Get variable screen information
	ioctl(fb_fd, FBIOGET_VSCREENINFO, &vinfo);
	vinfo.grayscale = grayscale;
	vinfo.bits_per_pixel = bpp;
	ioctl(fb_fd, FBIOPUT_VSCREENINFO, &vinfo);
	ioctl(fb_fd, FBIOGET_VSCREENINFO, &vinfo);

	// Get fixed screen information
	ioctl(fb_fd, FBIOGET_FSCREENINFO, &finfo);

	long screensize = vinfo.yres_virtual * finfo.line_length;

	fbp = mmap(0, screensize, PROT_READ | PROT_WRITE, MAP_SHARED, fb_fd, (off_t)0);
	/*
	if(MAP_FAILED == (bbp = mmap(0,
	                             vinfo.yres_virtual * finfo.line_length,
	                             PROT_READ | PROT_WRITE,
	                             MAP_PRIVATE | MAP_ANONYMOUS,
	                             -1,
	                             (off_t)0))) {
		perror("mmap");
	}*/
	// bbp = fbp + screensize;
}

#if 0
void draw_pixel(long x, long y, uint32_t color)
{
	if(x>=1 && x<=240 && y>=1 && y<=320) {

#ifdef DEBUG
		printf("px %d, %d\n", x, y);
#endif
		long location = (x + vinfo.xoffset) * (vinfo.bits_per_pixel / 8) + (y + vinfo.yoffset) * finfo.line_length;
#ifdef __ARMEL__
		*((uint16_t*)(bbp + location)) = color;
#else
		*((uint32_t*)(bbp + location)) = color;
#endif
	}
}
#endif

pixel_t pixel(long x, long y, pixel_t *p)
{
	long location = (x + vinfo.xoffset) * (vinfo.bits_per_pixel / 8) + (y + vinfo.yoffset) * finfo.line_length;
    //printf("loc %d, x %d, y %d\n", location, x, y);
    return *((pixel_t*)(p + location));
}
