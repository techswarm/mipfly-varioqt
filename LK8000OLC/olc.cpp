/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "olc.h"
#include <QDebug>


OLC::OLC(int handicap, int altLoss, QObject *parent) : QObject(parent),contest(handicap,altLoss)
{
    lastPush = QTime::currentTime();
}

void OLC::Dump(const CContestMgr::TType type) const
{
  const CContestMgr::CResult &result = contest.Result(type);
  qDebug() << "Contest '" << CContestMgr::TypeToString(type) << "':";
  qDebug() << " - Distance: " << result.Distance();
  qDebug() << " - Score: " << result.Score();
  //for(CPointGPSArray::const_iterator it=result.PointArray().begin(); it!=result.PointArray().end(); ++it)
    //qDebug() << " - " << TimeToString(it->Time());

  //_kml.Dump(result);
}

void OLC::processGpsCoord(Coord coord)
{
    if(lastPush.secsTo(QTime::currentTime())>5)
    {
        lastPush = QTime::currentTime();
        contest.Add(new CPointGPS(coord.getSnaptime(),coord.getLat(),coord.getLon(),coord.getGpsHeight()));
        Dump(CContestMgr::TYPE_OLC_CLASSIC);
        Dump(CContestMgr::TYPE_OLC_CLASSIC_PREDICTED);
    }
}

int OLC::getOptimisedDist(const CContestMgr::TType type)
{
    const CContestMgr::CResult &result = contest.Result(type);
    return result.Distance();
}
