/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef OLC_H
#define OLC_H

#include <QObject>
#include "ContestMgr.h"
#include "navUtils/coord.hpp"
#include <QTime>
#include "PointGPS.h"

class OLC : public QObject
{
    Q_OBJECT
public:
    explicit OLC(int handicap, int altLoss, QObject *parent = nullptr);

    int getOptimisedDist(const CContestMgr::TType type);
signals:

public slots:
    void processGpsCoord(Coord coord);

private:
    CContestMgr contest;
    QTime lastPush;
    void Dump(const CContestMgr::TType type) const;
};

#endif // OLC_H
