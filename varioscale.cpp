/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "varioscale.h"
#include "ui_varioscale.h"

#include <QPainter>

#include "utils/cstgraphics.h"
#include "VarioQt.h"
#include "utils/unitconverter.h"
using Vario::drawStr;

static const int STEP = 15;

inline int fractionOf(int value, int numerator, int denominator) {
    return int(double(value) * (double(numerator) / denominator));
}

VarioScale::VarioScale(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::VarioScale)
{
    ui->setupUi(this);

    vComp = VarioQt::instance().getVarioCompute();
}

void VarioScale::setUnit(QString u)
{
    unit = u;
}

VarioScale::~VarioScale()
{
    delete ui;
}

void VarioScale::paintEvent(QPaintEvent *event) {
    QPainter painter(this);

    int x = width() - _Width - 1;
    painter.setPen(QPen(QBrush("black"),2));
    painter.drawRect(x, 0, _Width, height() - 1);
    painter.setPen(QPen(QBrush("black"),1));
    for (int i=1; i < 10; i++) {
        int y = i * STEP;
        painter.drawLine(i == 5 ? x : x + _Width/2, y, x + _Width, y);
    }

    if (_height != 0) {
        painter.fillRect(x, _start, 2 * _Width / 3, _height, Qt::black);
    }

    if(unit == "ft/min")
    {
        drawStr(50, 1, QString::number(UnitConverter::mPerSecToFtPerMin(this->_max/100),'f',0), 18, painter,Qt::AlignRight);
        drawStr(50, 19, "Max", 16, painter,Qt::AlignRight);

        drawStr(50, 60, QString::number(UnitConverter::mPerSecToFtPerMin(this->_avg/100),'f',0), 22, painter,Qt::AlignRight);
        drawStr(50, 22+60, "ft/min", 16, painter,Qt::AlignRight);

        drawStr(50, 120, QString::number(UnitConverter::mPerSecToFtPerMin(this->_min/100),'f',0), 18, painter,Qt::AlignRight);
        drawStr(50, 18+120, "Min", 16, painter,Qt::AlignRight);
    }
    else
    {
        drawStr(50, 1, QString::number(this->_max/100,'f',1), 18, painter,Qt::AlignRight);
        drawStr(50, 19, "Max", 16, painter,Qt::AlignRight);

        drawStr(50, 60, QString::number(this->_avg/100,'f',1), 22, painter,Qt::AlignRight);
        drawStr(50, 22+60, "m/s", 16, painter,Qt::AlignRight);

        drawStr(50, 120, QString::number(this->_min/100,'f',1), 18, painter,Qt::AlignRight);
        drawStr(50, 18+120, "Min", 16, painter,Qt::AlignRight);
    }

    QPolygon triangle;
    triangle.append(QPoint(0,-5));
    triangle.append(QPoint(6,0));
    triangle.append(QPoint(0,5));

    QBrush fillbrush;
    fillbrush.setColor(Qt::black);
    fillbrush.setStyle(Qt::SolidPattern);

    QPainterPath trianglePath;
    trianglePath.addPolygon(triangle);

    float climbAvg = vComp->getAverageClimb();
    if(!isnan(climbAvg) && climbAvg < 10)
    {
        QPainterPath trianglePathMax;
        trianglePathMax = trianglePath.translated(88,75);
        trianglePathMax.translate(0,climbAvg * -15);
        painter.fillPath(trianglePathMax,fillbrush);

    }
    float sinkAvg = vComp->getAverageSink();
    if(!isnan(sinkAvg) && sinkAvg > -10)
    {
        QPainterPath trianglePathMin;
        trianglePathMin = trianglePath.translated(88,75);
        trianglePathMin.translate(0,sinkAvg * -15);
        painter.fillPath(trianglePathMin,fillbrush);
    }

}

void VarioScale::setVSpeed(int cmps) {
    _height = 0;

    int total = 5 * STEP;

    if (cmps > 0) {
        if (cmps < 500) {
            _height = fractionOf(total, cmps, 500);
            _start = total - _height;
        } else if (cmps < 1000) {
            _height = total - fractionOf(total, cmps-500, 500) + 1;
            _start = 0;
        }
    } else if (cmps < 0) {
        if (cmps > -500) {
            _height = fractionOf(total, cmps, -500) + 1;
            _start = total;
        } else if (cmps > -1000) {
            _height = total - fractionOf(total, cmps+500, -500);
            _start = 2 * total - _height;
        }
    }

    this->update();
}


void VarioScale::setAverage(qreal avg) {
    //this->ui->valAvg->setText(QString::number(avg));;
    this->_avg = avg;
}

void VarioScale::setMax(qreal max) {
    //this->ui->valMax->setText(QString::number(max));
    this->_max = max;
}

void VarioScale::setMin(qreal min) {
    //this->ui->valMin->setText(QString::number(min));
    this->_min = min;
}

