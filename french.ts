<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr" sourcelanguage="en">
<context>
    <name>AvionicsMenu</name>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="77"/>
        <source>TurnOff a. land - </source>
        <translation>Eteindre aprés  poser</translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="85"/>
        <source>IGC Name - </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="93"/>
        <source>Dist. mode - </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="100"/>
        <source>Pitot comp. - </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="108"/>
        <source>Pitot TEC - </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="116"/>
        <source>Pitot avg. - </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="123"/>
        <source>Trim speed - </source>
        <translation>Vitesse bras hauts</translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="138"/>
        <source>SET NUM. SAMPLES</source>
        <translation>DÉF NUM ÉCHANTILLONS</translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="140"/>
        <source>SET VARIO AVERAGE</source>
        <translation>DÉF MOYENNE VARIO</translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="142"/>
        <source>WIND</source>
        <translation>PARAMÈTRES VENT</translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="145"/>
        <source>GLIDE NUM. AVERAGE</source>
        <translation>DÉF NUM ÉCHANT VOL PLANÉ</translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="147"/>
        <source>THERMALING DETECT</source>
        <translation>PARAMETRES DETECTEUR ASCENDANCE</translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="149"/>
        <source>BACK</source>
        <translation>RETOUR</translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="261"/>
        <source>Pressure coefficient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="330"/>
        <source>Set IGC naming mode. STD for name with date and location and COMP for competition naming with pilot name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="332"/>
        <source>Set distance computation method for task navigation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="344"/>
        <source>Activate/deactivate raw logging. WARNING! High space consumming.</source>
        <translation>Activer/desactiver enregistrer á cru ATTENTION grosse utlilisation mémoire</translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="244"/>
        <source>Trim Speed</source>
        <translation>Vitesse bras hauts</translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="278"/>
        <source>TEC coefficient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="295"/>
        <source>Pitot average</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="334"/>
        <source>Set average for fast vario. 1s = 201 samples. Not used for instant vario.</source>
        <translation>Définir la moyenne pour vario rapide. 1 s = 201 échantillons. Ne pas utilisé pour vario instant.</translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="336"/>
        <source>Set integration time for average climb.</source>
        <translation>Définir le temps d&apos;intégration de la montée moyenne.</translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="338"/>
        <source>Modify wind algorithm parameters.</source>
        <translation>Modifier les paramètres des algorithmes de vent.</translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="340"/>
        <source>Set number of samples for glide algorithm.</source>
        <translation>Définir le nombre d&apos;échantillons des algorithmes du vol plané.</translation>
    </message>
    <message>
        <location filename="menu/avionics/avionicsMenu.cpp" line="342"/>
        <source>Activate/deactivate and set parameters for thermaling detect.</source>
        <translation>Activer/Désactiver et établir paramétres pour detect thermi</translation>
    </message>
</context>
<context>
    <name>BTDeviceActionMenu</name>
    <message>
        <location filename="menu/general/BTDeviceAction.cpp" line="61"/>
        <source>PAIR</source>
        <translation>LIAISON</translation>
    </message>
    <message>
        <location filename="menu/general/BTDeviceAction.cpp" line="64"/>
        <source>CONNECT</source>
        <translation>CONNECTER</translation>
    </message>
    <message>
        <location filename="menu/general/BTDeviceAction.cpp" line="67"/>
        <source>REMOVE</source>
        <translation>RETIRER</translation>
    </message>
    <message>
        <location filename="menu/general/BTDeviceAction.cpp" line="70"/>
        <source>SET AS REMOTE</source>
        <translation>LIAISON TELECOMANDE</translation>
    </message>
    <message>
        <source>SET AS BNEP</source>
        <translation type="vanished">LIAISON BNEP</translation>
    </message>
    <message>
        <location filename="menu/general/BTDeviceAction.cpp" line="73"/>
        <source>BACK</source>
        <translation>RETOUR</translation>
    </message>
    <message>
        <location filename="menu/general/BTDeviceAction.cpp" line="100"/>
        <location filename="menu/general/BTDeviceAction.cpp" line="107"/>
        <location filename="menu/general/BTDeviceAction.cpp" line="116"/>
        <location filename="menu/general/BTDeviceAction.cpp" line="124"/>
        <location filename="menu/general/BTDeviceAction.cpp" line="150"/>
        <location filename="menu/general/BTDeviceAction.cpp" line="154"/>
        <source>INFO</source>
        <translation>INFORMATION</translation>
    </message>
    <message>
        <location filename="menu/general/BTDeviceAction.cpp" line="100"/>
        <source>Pair started</source>
        <translation>Liaison en cours</translation>
    </message>
    <message>
        <location filename="menu/general/BTDeviceAction.cpp" line="107"/>
        <source>Connect started</source>
        <translation>Connection en cours</translation>
    </message>
    <message>
        <location filename="menu/general/BTDeviceAction.cpp" line="116"/>
        <source>Remove started</source>
        <translation>Retirer en cours</translation>
    </message>
    <message>
        <location filename="menu/general/BTDeviceAction.cpp" line="124"/>
        <source>Remote set</source>
        <translation>Télécommande connectée</translation>
    </message>
    <message>
        <source>BNEP set</source>
        <translation type="vanished">BNEP établi</translation>
    </message>
    <message>
        <location filename="menu/general/BTDeviceAction.cpp" line="150"/>
        <source>Device paired</source>
        <translation>Appareil jumelé</translation>
    </message>
    <message>
        <location filename="menu/general/BTDeviceAction.cpp" line="154"/>
        <source>Device unpaired</source>
        <translation>Appareil Déjumelé</translation>
    </message>
</context>
<context>
    <name>BTDeviceMenu</name>
    <message>
        <location filename="menu/general/BTDevice.cpp" line="71"/>
        <source>VISIBLE ON</source>
        <translation>VISIBILITÉ ON</translation>
    </message>
    <message>
        <location filename="menu/general/BTDevice.cpp" line="75"/>
        <source>VISIBLE OFF</source>
        <translation>VISIBILITÉ OFF</translation>
    </message>
    <message>
        <location filename="menu/general/BTDevice.cpp" line="82"/>
        <source>SCAN ON</source>
        <translation>RECHERCHE ON</translation>
    </message>
    <message>
        <location filename="menu/general/BTDevice.cpp" line="86"/>
        <source>SCAN OFF</source>
        <translation>RECHERCHE OFF</translation>
    </message>
    <message>
        <location filename="menu/general/BTDevice.cpp" line="90"/>
        <source>REMOVE REMOTE</source>
        <translation>DECONNECTER TÉLÉCOMMANDE</translation>
    </message>
    <message>
        <location filename="menu/general/BTDevice.cpp" line="92"/>
        <location filename="menu/general/BTDevice.cpp" line="179"/>
        <source>BACK</source>
        <translation>RETOUR</translation>
    </message>
    <message>
        <location filename="menu/general/BTDevice.cpp" line="99"/>
        <source>NULL</source>
        <translation>NUL</translation>
    </message>
    <message>
        <location filename="menu/general/BTDevice.cpp" line="157"/>
        <source>INFO</source>
        <translation>INFORMATION</translation>
    </message>
    <message>
        <location filename="menu/general/BTDevice.cpp" line="157"/>
        <source>Remote removed</source>
        <translation>Télécommande retirée</translation>
    </message>
    <message>
        <location filename="menu/general/BTDevice.cpp" line="175"/>
        <source>Set MipFly visibility over bluetooth for outside connectivity.</source>
        <translation>Paramétres visibilté pour Mipfly par connection exterieur bluetooth</translation>
    </message>
    <message>
        <location filename="menu/general/BTDevice.cpp" line="177"/>
        <source>Toggle BT devices scan mode.</source>
        <translation>Recherche appareils BT </translation>
    </message>
    <message>
        <location filename="menu/general/BTDevice.cpp" line="189"/>
        <source>paired</source>
        <translation>connecté</translation>
    </message>
    <message>
        <location filename="menu/general/BTDevice.cpp" line="193"/>
        <source>not paired</source>
        <translation>non connecté</translation>
    </message>
</context>
<context>
    <name>BTServer</name>
    <message>
        <location filename="utils/BTServer.cpp" line="75"/>
        <source>Bt Chat Server</source>
        <translation>BT chat reseau</translation>
    </message>
    <message>
        <location filename="utils/BTServer.cpp" line="77"/>
        <source>Example bluetooth chat server</source>
        <translation>Exemple chat réseau</translation>
    </message>
    <message>
        <location filename="utils/BTServer.cpp" line="78"/>
        <source>qt-project.org</source>
        <translation>qt-project.org</translation>
    </message>
</context>
<context>
    <name>BootloaderMenu</name>
    <message>
        <location filename="menu/bootloader/bootloadermenu.cpp" line="48"/>
        <source>FLASH STANDARD</source>
        <translation type="unfinished">FLASH STANDARD</translation>
    </message>
    <message>
        <location filename="menu/bootloader/bootloadermenu.cpp" line="51"/>
        <source>FLASH INSTANT</source>
        <translation>FLASH INSTANTANÉ</translation>
    </message>
    <message>
        <location filename="menu/bootloader/bootloadermenu.cpp" line="53"/>
        <source>BACK</source>
        <translation>RETOUR</translation>
    </message>
    <message>
        <location filename="menu/bootloader/bootloadermenu.cpp" line="95"/>
        <source>Load the standard firmware based only on pressure data.</source>
        <translation type="unfinished">Installer le micrologiciel standard se basant seulement sur les données de pression.</translation>
    </message>
    <message>
        <location filename="menu/bootloader/bootloadermenu.cpp" line="97"/>
        <source>Load the instant vario firmware based on pressure + accelerometer.</source>
        <translation>Installer le micrologiciel instant vario se basant sur les données de pression et l&apos;accéléromètre.</translation>
    </message>
</context>
<context>
    <name>CaptionBox</name>
    <message>
        <location filename="ui/captionbox.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
</context>
<context>
    <name>ChooseInput</name>
    <message>
        <location filename="ChooseInput.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialogue</translation>
    </message>
</context>
<context>
    <name>Compass</name>
    <message>
        <location filename="compass.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
</context>
<context>
    <name>ConnectivityMenu</name>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="74"/>
        <source>BLUETOOTH</source>
        <translation>BLUETOOTH</translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="76"/>
        <source>REMOTE</source>
        <translation>TÉLÉCOMMANDE</translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="78"/>
        <source>WIFI</source>
        <translation>WIFI</translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="80"/>
        <source>ACCOUNT</source>
        <translation>COMPTE</translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="85"/>
        <source>GPSPostscale - NAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="88"/>
        <source>GPSPostscale - </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="94"/>
        <source>VarioPostscale - NAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="97"/>
        <source>VarioPostscale - </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="104"/>
        <source>Live ON</source>
        <translation>Live ON</translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="105"/>
        <source>Live OFF</source>
        <translation>Live OFF</translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="108"/>
        <source>ACTIVATE TTY</source>
        <translation>ACTIVATION TTY</translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="110"/>
        <source>BACK</source>
        <translation>RETOUR</translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="162"/>
        <location filename="menu/general/connectivityMenu.cpp" line="274"/>
        <location filename="menu/general/connectivityMenu.cpp" line="279"/>
        <source>INFO</source>
        <translation>INFORMATION</translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="162"/>
        <source>Check Conn</source>
        <translation>Vérification Connection</translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="178"/>
        <source>TTY error</source>
        <translation>Erreur TTY</translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="178"/>
        <source>Active</source>
        <translation>En cours</translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="182"/>
        <source>TTY mode</source>
        <translation>Mode TTY</translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="182"/>
        <source>ACTIVATED</source>
        <translation>ACTIVÉ</translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="196"/>
        <source>GPS Postscale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="212"/>
        <source>Vario Postscale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="243"/>
        <source>No Bluetooth available.</source>
        <translation>Pas de bluetooth détecté</translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="247"/>
        <source>Scan, pair and unpair bluetooth devices.</source>
        <translation>Rechercher,connecter,déconnecter appareils avec Bluetooth</translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="250"/>
        <source>Set remote settings and map keys.</source>
        <translation>Paramétres télécommande et Cartes</translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="252"/>
        <source>Set variometer data postscaler for BT transmission. 1 is equivalent to one message every 100ms. 2 -&gt; 200ms etc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="254"/>
        <source>Set GPS data postscaler for BT transmission. 1 is equivalent to one message every 200ms. 2 -&gt; 400ms etc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="256"/>
        <source>Enable or disable Live Tracking functionality.</source>
        <translation>Activer,désactiver fonction live-tracking</translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="274"/>
        <source>Success!</source>
        <translation>Succés</translation>
    </message>
    <message>
        <location filename="menu/general/connectivityMenu.cpp" line="279"/>
        <source>Fail!</source>
        <translation>sans succés</translation>
    </message>
</context>
<context>
    <name>CoordInput</name>
    <message>
        <location filename="CoordInput.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>FAINavigationMenu</name>
    <message>
        <location filename="menu/task/faiNavigationMenu.cpp" line="63"/>
        <source>Set TP1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/faiNavigationMenu.cpp" line="65"/>
        <source>Set TP2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/faiNavigationMenu.cpp" line="67"/>
        <source>Det Base</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/faiNavigationMenu.cpp" line="69"/>
        <source>Nav to FAI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/faiNavigationMenu.cpp" line="71"/>
        <source>Nav to Tp1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/faiNavigationMenu.cpp" line="73"/>
        <source>Zoom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/faiNavigationMenu.cpp" line="75"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/faiNavigationMenu.cpp" line="118"/>
        <location filename="menu/task/faiNavigationMenu.cpp" line="150"/>
        <location filename="menu/task/faiNavigationMenu.cpp" line="181"/>
        <source>WARNING</source>
        <translation type="unfinished">ATTENTION</translation>
    </message>
    <message>
        <location filename="menu/task/faiNavigationMenu.cpp" line="119"/>
        <source>Reset TP1?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/faiNavigationMenu.cpp" line="120"/>
        <location filename="menu/task/faiNavigationMenu.cpp" line="152"/>
        <location filename="menu/task/faiNavigationMenu.cpp" line="183"/>
        <source>CANCEL</source>
        <translation type="unfinished">ANNULER</translation>
    </message>
    <message>
        <location filename="menu/task/faiNavigationMenu.cpp" line="121"/>
        <location filename="menu/task/faiNavigationMenu.cpp" line="153"/>
        <location filename="menu/task/faiNavigationMenu.cpp" line="184"/>
        <source>CONFIRM</source>
        <translation type="unfinished">CONFIRMER</translation>
    </message>
    <message>
        <location filename="menu/task/faiNavigationMenu.cpp" line="129"/>
        <location filename="menu/task/faiNavigationMenu.cpp" line="135"/>
        <location filename="menu/task/faiNavigationMenu.cpp" line="161"/>
        <location filename="menu/task/faiNavigationMenu.cpp" line="169"/>
        <source>INFO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/faiNavigationMenu.cpp" line="129"/>
        <location filename="menu/task/faiNavigationMenu.cpp" line="135"/>
        <source>TP1 marked!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/faiNavigationMenu.cpp" line="143"/>
        <source>ERROR</source>
        <translation type="unfinished">ERREUR</translation>
    </message>
    <message>
        <location filename="menu/task/faiNavigationMenu.cpp" line="143"/>
        <source>TP1 not marked!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/faiNavigationMenu.cpp" line="151"/>
        <source>Reset TP2?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/faiNavigationMenu.cpp" line="161"/>
        <location filename="menu/task/faiNavigationMenu.cpp" line="169"/>
        <source>TP2 marked!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/faiNavigationMenu.cpp" line="182"/>
        <source>Recompute area?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FaiAssistCompute</name>
    <message>
        <location filename="navUtils/faiassistcompute.cpp" line="91"/>
        <source>TASK INFO</source>
        <translation type="unfinished">INFO MANCHE</translation>
    </message>
    <message>
        <location filename="navUtils/faiassistcompute.cpp" line="91"/>
        <source>FAI AREA REACHED</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GeneralMenu</name>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="83"/>
        <source>PILOT NAME</source>
        <translation>NOM DU PILOTE</translation>
    </message>
    <message>
        <source>SET TIMEZONE</source>
        <translation type="vanished">DÉF FUSEAU HORAIRE</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="87"/>
        <source>BOOTLOAD</source>
        <translation>INSTALLATION MICROLOGICIEL</translation>
    </message>
    <message>
        <source>LANGUAGE</source>
        <translation type="vanished">LANGUE</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="91"/>
        <location filename="menu/general/generalMenu.cpp" line="140"/>
        <source>UPDATE</source>
        <translation>MISE A JOUR</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="93"/>
        <source>USER INTERFACE</source>
        <translation>ACTION UTILISATEUR</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="95"/>
        <source>MASS STORAGE</source>
        <translation>MÉMOIRE APPAREIL</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="97"/>
        <source>BACK</source>
        <translation>RETOUR</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="155"/>
        <location filename="menu/general/generalMenu.cpp" line="221"/>
        <location filename="menu/general/generalMenu.cpp" line="226"/>
        <location filename="menu/general/generalMenu.cpp" line="258"/>
        <location filename="menu/general/generalMenu.cpp" line="277"/>
        <source>INFO</source>
        <translation>INFORMATION</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="155"/>
        <source>Check Conn</source>
        <translation>Vérification Connection</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="89"/>
        <source>CONNECTIVITY</source>
        <translation>CONNECTION</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="85"/>
        <source>LOCALIZATION</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="141"/>
        <source>TYPE</source>
        <translation>GENRE</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="142"/>
        <source>BETA</source>
        <translation>BETA</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="143"/>
        <source>CANCEL</source>
        <translation type="unfinished">ANNULER</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="144"/>
        <source>STABLE</source>
        <translation>STABLE</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="163"/>
        <source>TTY mode active</source>
        <translation>mode TTY activé</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="163"/>
        <source>Pls. Reboot!</source>
        <translation>Redémarrer svp</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="169"/>
        <source>MASS STORAGE MODE</source>
        <translation>MODE MÉMOIRE</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="169"/>
        <source>HOLD F TO EXIT</source>
        <translation>APPUYER LONGUEMENT  F POUR SORTIR</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="195"/>
        <source>Change the name of the owner. Used for IGC data and flight records.</source>
        <translation>Modifier le nom de l&apos;utilisateur. Utilisé pour enrégistrer les données IGC et de vol.</translation>
    </message>
    <message>
        <source>Change the timezone you are in to adjust from the universal time.</source>
        <translation type="vanished">Modifier le fuseau horaire pour ajuster l&apos;heure en rapport avec le temps universel (GMT).</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="199"/>
        <source>Load a newer or different firmware to your device.</source>
        <translation>Installer un micrologicel plus récent ou différent sur cet appareil.</translation>
    </message>
    <message>
        <source>Change the language of the device.</source>
        <translation type="vanished">Changer la langue de l&apos;appareil</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="197"/>
        <source>Change settings like language, timezone or default units.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="201"/>
        <source>Connectivity settings, Bluetooth/WiFi</source>
        <translation>Paramétres de connection Bluetooth/WIFI</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="203"/>
        <source>Update and select UI.</source>
        <translation>Mise á jour et selection UI</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="205"/>
        <source>Update software to latest version. Restart will be required for the changes to take place. Requires working internet connection.</source>
        <translation>Mise á jour de la version du micrologiciel,besoin de remettre en route pour la mise jour de prendre effet,connection internet necessaire</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="209"/>
        <source>Switch to mass storage mode for USB OTG.</source>
        <translation>Changer vers Mode Mémoire pour USB OTG</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="221"/>
        <location filename="menu/general/generalMenu.cpp" line="226"/>
        <source>Conn OK</source>
        <translation>Connection OK</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="233"/>
        <location filename="menu/general/generalMenu.cpp" line="263"/>
        <source>ERROR</source>
        <translation>ERREUR</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="233"/>
        <source>Not Connected</source>
        <translation>Non connecté</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="252"/>
        <source>Success</source>
        <translation>Succés</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="252"/>
        <source>Powering Off!</source>
        <translation>Éteindre</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="258"/>
        <source>Success! Pls Reboot</source>
        <translation>Succés Redémarrer svp</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="263"/>
        <source>Not Updated</source>
        <translation>Mise á jour non effectué</translation>
    </message>
    <message>
        <location filename="menu/general/generalMenu.cpp" line="277"/>
        <source>Downloading</source>
        <translation>Téléchargement en cours</translation>
    </message>
</context>
<context>
    <name>GlideMenu</name>
    <message>
        <location filename="menu/glideAverage/glidemenu.cpp" line="46"/>
        <source>UP</source>
        <translation>VERS LE HAUT</translation>
    </message>
    <message>
        <location filename="menu/glideAverage/glidemenu.cpp" line="48"/>
        <source>DOWN</source>
        <translation>VERS LE BAS</translation>
    </message>
    <message>
        <location filename="menu/glideAverage/glidemenu.cpp" line="50"/>
        <source>BACK</source>
        <translation>RETOUR</translation>
    </message>
</context>
<context>
    <name>GlidesMenu</name>
    <message>
        <location filename="menu/glides/glidesmenu.cpp" line="62"/>
        <source>FLIGHT</source>
        <translation>VOL</translation>
    </message>
    <message>
        <location filename="menu/glides/glidesmenu.cpp" line="63"/>
        <source>ACTION?</source>
        <translation>ACTION?</translation>
    </message>
    <message>
        <location filename="menu/glides/glidesmenu.cpp" line="64"/>
        <source>UPLOAD</source>
        <translation>TRANSFÉRER</translation>
    </message>
    <message>
        <location filename="menu/glides/glidesmenu.cpp" line="65"/>
        <location filename="menu/glides/glidesmenu.cpp" line="102"/>
        <source>DELETE</source>
        <translation>EFFACER</translation>
    </message>
    <message>
        <location filename="menu/glides/glidesmenu.cpp" line="66"/>
        <source>SHOW</source>
        <translation>MONTRER</translation>
    </message>
    <message>
        <location filename="menu/glides/glidesmenu.cpp" line="103"/>
        <source>CONFIRM?</source>
        <translation>CONFIRMER?</translation>
    </message>
    <message>
        <location filename="menu/glides/glidesmenu.cpp" line="104"/>
        <source>CANCEL</source>
        <translation>ANNULER</translation>
    </message>
    <message>
        <location filename="menu/glides/glidesmenu.cpp" line="105"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="menu/glides/glidesmenu.cpp" line="143"/>
        <source>-- BACK --</source>
        <translation>RETOUR</translation>
    </message>
</context>
<context>
    <name>GoalTypeMenu</name>
    <message>
        <location filename="menu/task/goalTypeMenu.cpp" line="54"/>
        <source>LINE</source>
        <translation>LIGNE</translation>
    </message>
    <message>
        <location filename="menu/task/goalTypeMenu.cpp" line="56"/>
        <source>CYLINDER ENTER</source>
        <oldsource>CILINDER ENTER</oldsource>
        <translation>ENTRÉE CYLINDRE</translation>
    </message>
    <message>
        <location filename="menu/task/goalTypeMenu.cpp" line="58"/>
        <source>CYLINDER EXIT</source>
        <oldsource>CILINDER EXIT</oldsource>
        <translation>SORTIE CYLINDRE</translation>
    </message>
</context>
<context>
    <name>IGCUploader</name>
    <message>
        <location filename="utils/igcuploader.cpp" line="67"/>
        <source>In progress!</source>
        <translation>En cours!</translation>
    </message>
    <message>
        <location filename="utils/igcuploader.cpp" line="67"/>
        <location filename="utils/igcuploader.cpp" line="100"/>
        <location filename="utils/igcuploader.cpp" line="110"/>
        <location filename="utils/igcuploader.cpp" line="116"/>
        <location filename="utils/igcuploader.cpp" line="122"/>
        <location filename="utils/igcuploader.cpp" line="128"/>
        <location filename="utils/igcuploader.cpp" line="155"/>
        <source>ERROR</source>
        <translation>ERREUR</translation>
    </message>
    <message>
        <location filename="utils/igcuploader.cpp" line="100"/>
        <source>Fail 8</source>
        <translation>Non réussi 8</translation>
    </message>
    <message>
        <location filename="utils/igcuploader.cpp" line="110"/>
        <source>Fail 7</source>
        <translation>Non ré◘ussi 7</translation>
    </message>
    <message>
        <location filename="utils/igcuploader.cpp" line="116"/>
        <source>Not paired</source>
        <translation>Non jumelé</translation>
    </message>
    <message>
        <location filename="utils/igcuploader.cpp" line="122"/>
        <source>File exists</source>
        <translation>Fichier existe</translation>
    </message>
    <message>
        <location filename="utils/igcuploader.cpp" line="128"/>
        <source>No space</source>
        <translation>Mémoire pleine</translation>
    </message>
    <message>
        <location filename="utils/igcuploader.cpp" line="155"/>
        <source>No Internet</source>
        <translation>Internet non connecté</translation>
    </message>
    <message>
        <location filename="utils/igcuploader.cpp" line="162"/>
        <source>File Sent</source>
        <translation>Fichier envoyé</translation>
    </message>
    <message>
        <location filename="utils/igcuploader.cpp" line="162"/>
        <source>INFO</source>
        <translation>INFORMATION</translation>
    </message>
</context>
<context>
    <name>InputLocation</name>
    <message>
        <location filename="ui/inputlocation.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished">Formulaire</translation>
    </message>
</context>
<context>
    <name>InputNameMenu</name>
    <message>
        <location filename="menu/name/inputnamemenu.cpp" line="52"/>
        <source>INPUT</source>
        <translation>ENREGISTRER</translation>
    </message>
    <message>
        <location filename="menu/name/inputnamemenu.cpp" line="54"/>
        <source>CLEAR</source>
        <translation>EFFACER</translation>
    </message>
    <message>
        <location filename="menu/name/inputnamemenu.cpp" line="56"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="menu/name/inputnamemenu.cpp" line="58"/>
        <source>BACKSPACE</source>
        <translation>RETOUR D&apos;UN ESPACE</translation>
    </message>
    <message>
        <location filename="menu/name/inputnamemenu.cpp" line="60"/>
        <source>BACK</source>
        <translation>RETOUR</translation>
    </message>
    <message>
        <location filename="menu/name/inputnamemenu.cpp" line="101"/>
        <source>INFO</source>
        <translation>INFORMATION</translation>
    </message>
    <message>
        <location filename="menu/name/inputnamemenu.cpp" line="101"/>
        <source>Pilot name was saved</source>
        <translation>Nom du pilot enregistré</translation>
    </message>
</context>
<context>
    <name>LanguageMenu</name>
    <message>
        <location filename="menu/general/languageMenu.cpp" line="63"/>
        <source>BACK</source>
        <translation>RETOUR</translation>
    </message>
    <message>
        <location filename="menu/general/languageMenu.cpp" line="87"/>
        <source>INFO</source>
        <translation>INFO</translation>
    </message>
    <message>
        <location filename="menu/general/languageMenu.cpp" line="87"/>
        <source>Language was set</source>
        <translation>Choix Langue enregistré</translation>
    </message>
</context>
<context>
    <name>LetterChooser</name>
    <message>
        <location filename="letterchooser.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished">Formulaire</translation>
    </message>
</context>
<context>
    <name>LocDeleteMenu</name>
    <message>
        <location filename="menu/location/locdeletemenu.cpp" line="39"/>
        <source>INFO</source>
        <translation>INFO</translation>
    </message>
    <message>
        <location filename="menu/location/locdeletemenu.cpp" line="39"/>
        <source>Location was removed</source>
        <translation>Position enlevée</translation>
    </message>
</context>
<context>
    <name>LocEditMenu</name>
    <message>
        <location filename="menu/location/loceditmenu.cpp" line="75"/>
        <source>COORDS FROM GPS</source>
        <translation>COORDONÉES GPS</translation>
    </message>
    <message>
        <location filename="menu/location/loceditmenu.cpp" line="77"/>
        <source>INPUT NAME</source>
        <translation>ENREGISTRER NOM</translation>
    </message>
    <message>
        <location filename="menu/location/loceditmenu.cpp" line="79"/>
        <source>SET LATITUDE</source>
        <translation>ENREGISTRER LATITUDE</translation>
    </message>
    <message>
        <location filename="menu/location/loceditmenu.cpp" line="81"/>
        <source>SET LONGITUDE</source>
        <translation>ENREGISTRER LONGITUDE</translation>
    </message>
    <message>
        <location filename="menu/location/loceditmenu.cpp" line="83"/>
        <source>SET ALTITUDE</source>
        <translation type="unfinished">ENREGISTRER ALTITUDE</translation>
    </message>
    <message>
        <location filename="menu/location/loceditmenu.cpp" line="85"/>
        <source>SAVE</source>
        <translation>MÉmoriser</translation>
    </message>
    <message>
        <location filename="menu/location/loceditmenu.cpp" line="87"/>
        <source>BACK</source>
        <translation>RETOUR</translation>
    </message>
    <message>
        <location filename="menu/location/loceditmenu.cpp" line="129"/>
        <source>INFO</source>
        <translation>INFO</translation>
    </message>
    <message>
        <location filename="menu/location/loceditmenu.cpp" line="129"/>
        <source>Location was saved</source>
        <translation>Position enregistrée</translation>
    </message>
</context>
<context>
    <name>LocListMenu</name>
    <message>
        <location filename="menu/location/loclistmenu.cpp" line="31"/>
        <source>-- BACK --</source>
        <translation>RETOUR</translation>
    </message>
</context>
<context>
    <name>LocSetCrtMenu</name>
    <message>
        <location filename="menu/location/locsetcrtmenu.cpp" line="28"/>
        <source>INFO</source>
        <translation>INFO</translation>
    </message>
    <message>
        <location filename="menu/location/locsetcrtmenu.cpp" line="28"/>
        <source>Location was saved</source>
        <translation>Position enregistrée</translation>
    </message>
</context>
<context>
    <name>LocalizationMenu</name>
    <message>
        <location filename="menu/general/localizationMenu.cpp" line="69"/>
        <source>SET TIMEZONE</source>
        <translation type="unfinished">DÉF FUSEAU HORAIRE</translation>
    </message>
    <message>
        <location filename="menu/general/localizationMenu.cpp" line="71"/>
        <source>LANGUAGE</source>
        <translation type="unfinished">LANGUE</translation>
    </message>
    <message>
        <location filename="menu/general/localizationMenu.cpp" line="73"/>
        <location filename="menu/general/localizationMenu.cpp" line="118"/>
        <source>SPEED</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/general/localizationMenu.cpp" line="75"/>
        <location filename="menu/general/localizationMenu.cpp" line="131"/>
        <source>VSPEED</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/general/localizationMenu.cpp" line="77"/>
        <source>DIST.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/general/localizationMenu.cpp" line="79"/>
        <location filename="menu/general/localizationMenu.cpp" line="157"/>
        <source>HEIGHT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/general/localizationMenu.cpp" line="81"/>
        <source>BACK</source>
        <translation type="unfinished">RETOUR</translation>
    </message>
    <message>
        <location filename="menu/general/localizationMenu.cpp" line="119"/>
        <location filename="menu/general/localizationMenu.cpp" line="132"/>
        <location filename="menu/general/localizationMenu.cpp" line="145"/>
        <location filename="menu/general/localizationMenu.cpp" line="158"/>
        <source>UNIT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/general/localizationMenu.cpp" line="144"/>
        <source>DISTANCE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/general/localizationMenu.cpp" line="185"/>
        <source>Change the timezone you are in to adjust from the universal time.</source>
        <translation type="unfinished">Modifier le fuseau horaire pour ajuster l&apos;heure en rapport avec le temps universel (GMT).</translation>
    </message>
    <message>
        <location filename="menu/general/localizationMenu.cpp" line="187"/>
        <source>Change the language of the device.</source>
        <translation type="unfinished">Changer la langue de l&apos;appareil</translation>
    </message>
</context>
<context>
    <name>LocationsMenu</name>
    <message>
        <location filename="menu/location/locationsmenu.cpp" line="77"/>
        <source>ADD / EDIT</source>
        <translation>AJOUTER/CHANGER</translation>
    </message>
    <message>
        <location filename="menu/location/locationsmenu.cpp" line="80"/>
        <source>AUTO-SET: OFF</source>
        <translation>AUTO PARAMETRES OFF</translation>
    </message>
    <message>
        <location filename="menu/location/locationsmenu.cpp" line="82"/>
        <source>AUTO-SET: ON</source>
        <translation>AUTO PARAMETRES ON</translation>
    </message>
    <message>
        <location filename="menu/location/locationsmenu.cpp" line="84"/>
        <source>SET CURRENT</source>
        <translation>ENREGISTRER ACTUEL</translation>
    </message>
    <message>
        <location filename="menu/location/locationsmenu.cpp" line="86"/>
        <source>CLEAR CURRENT</source>
        <translation>EFFACER ACTUEL</translation>
    </message>
    <message>
        <location filename="menu/location/locationsmenu.cpp" line="88"/>
        <source>DELETE</source>
        <translation>EFFACER</translation>
    </message>
    <message>
        <location filename="menu/location/locationsmenu.cpp" line="90"/>
        <source>BACK</source>
        <translation>RETOUR</translation>
    </message>
    <message>
        <location filename="menu/location/locationsmenu.cpp" line="115"/>
        <source>Modify data for existing location name or insert new location</source>
        <translation>Changer paramétres pour position existante ou entrerr nouvelle position</translation>
    </message>
    <message>
        <location filename="menu/location/locationsmenu.cpp" line="117"/>
        <source>Toggle manual vs automatic location set for flight recording</source>
        <translation>Changer entre manuel ou automatique pour position des paramétres et enregistrement des vols</translation>
    </message>
    <message>
        <location filename="menu/location/locationsmenu.cpp" line="128"/>
        <source>INFO</source>
        <translation>INFO</translation>
    </message>
    <message>
        <location filename="menu/location/locationsmenu.cpp" line="128"/>
        <source>Location was cleared</source>
        <translation>Position effacée</translation>
    </message>
</context>
<context>
    <name>MainMenu</name>
    <message>
        <location filename="menu/mainmenu.cpp" line="104"/>
        <source>SOUND</source>
        <translation>SON</translation>
    </message>
    <message>
        <location filename="menu/mainmenu.cpp" line="107"/>
        <source>NAVIGATION/MAPS</source>
        <translation>NAVIGATION/CARTES</translation>
    </message>
    <message>
        <location filename="menu/mainmenu.cpp" line="109"/>
        <source>NAVIGATION/TASK</source>
        <translation>NAVIGATION/MANCHE</translation>
    </message>
    <message>
        <location filename="menu/mainmenu.cpp" line="111"/>
        <source>GENERAL</source>
        <translation>GÉNÉRAL</translation>
    </message>
    <message>
        <source>GLIDES</source>
        <translation type="obsolete">VOLS</translation>
    </message>
    <message>
        <source>WIND</source>
        <translation type="obsolete">Wcaca</translation>
    </message>
    <message>
        <location filename="menu/mainmenu.cpp" line="113"/>
        <source>FLIGHTS</source>
        <translation>VOLS</translation>
    </message>
    <message>
        <location filename="menu/mainmenu.cpp" line="115"/>
        <source>AVIONICS</source>
        <translation>AVIONICS</translation>
    </message>
    <message>
        <location filename="menu/mainmenu.cpp" line="117"/>
        <source>ABOUT</source>
        <translation>VERSION</translation>
    </message>
    <message>
        <location filename="menu/mainmenu.cpp" line="120"/>
        <source>EXIT PROGRAM</source>
        <translation>SORTIE</translation>
    </message>
    <message>
        <location filename="menu/mainmenu.cpp" line="123"/>
        <source>SWITCH THEME</source>
        <translation>THÈME</translation>
    </message>
    <message>
        <location filename="menu/mainmenu.cpp" line="125"/>
        <source>TURN OFF</source>
        <translation>ÉTEINDRE</translation>
    </message>
    <message>
        <location filename="menu/mainmenu.cpp" line="197"/>
        <source>Maps and navigation options</source>
        <translation>Cartes et options de navigation</translation>
    </message>
    <message>
        <location filename="menu/mainmenu.cpp" line="199"/>
        <source>View recorded glides</source>
        <translation>Voir les enregistrements des vols</translation>
    </message>
    <message>
        <location filename="menu/mainmenu.cpp" line="201"/>
        <source>Task navigation options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/mainmenu.cpp" line="203"/>
        <source>Flight algorithms related settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/mainmenu.cpp" line="205"/>
        <source>Volume and sound curve settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/mainmenu.cpp" line="207"/>
        <source>General, localization and connectivity settings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainUI</name>
    <message>
        <location filename="mainui.ui" line="32"/>
        <source>MainUI</source>
        <translation>UI Général</translation>
    </message>
    <message>
        <location filename="mainui.cpp" line="531"/>
        <location filename="mainui.cpp" line="551"/>
        <location filename="mainui.cpp" line="556"/>
        <source>INFO</source>
        <translation>INFORMATION</translation>
    </message>
    <message>
        <location filename="mainui.cpp" line="531"/>
        <source>Landed</source>
        <translation>Posé</translation>
    </message>
    <message>
        <location filename="mainui.cpp" line="539"/>
        <source>TURN OFF</source>
        <translation>ÉTEINDRE</translation>
    </message>
    <message>
        <location filename="mainui.cpp" line="551"/>
        <source>Takeoff</source>
        <translation>Décollage</translation>
    </message>
    <message>
        <location filename="mainui.cpp" line="715"/>
        <location filename="mainui.cpp" line="725"/>
        <source>SOFT TURN OFF</source>
        <translation>ETEINDRE DOUCEMENT</translation>
    </message>
    <message>
        <location filename="mainui.cpp" line="716"/>
        <location filename="mainui.cpp" line="726"/>
        <source>CONFIRM</source>
        <comment>power button</comment>
        <translation type="unfinished">CONFIRMER</translation>
    </message>
    <message>
        <location filename="mainui.cpp" line="773"/>
        <source>CONFIRM</source>
        <translation>CONFIRMER</translation>
    </message>
    <message>
        <location filename="mainui.cpp" line="717"/>
        <location filename="mainui.cpp" line="727"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="mainui.cpp" line="793"/>
        <source>INFO Timeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainui.cpp" line="541"/>
        <location filename="mainui.cpp" line="718"/>
        <location filename="mainui.cpp" line="728"/>
        <source>CANCEL</source>
        <translation>ANNULER</translation>
    </message>
    <message>
        <location filename="mainui.cpp" line="744"/>
        <source>INFO Side</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainui.cpp" line="744"/>
        <location filename="mainui.cpp" line="793"/>
        <source>Powering Off!</source>
        <translation type="unfinished">Éteindre</translation>
    </message>
    <message>
        <location filename="mainui.cpp" line="771"/>
        <source>TURN ON</source>
        <translation>ALLUMER</translation>
    </message>
</context>
<context>
    <name>MenuContainer</name>
    <message>
        <location filename="MenuContainer.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialogue</translation>
    </message>
</context>
<context>
    <name>MenuDialog</name>
    <message>
        <location filename="menudialog.ui" line="17"/>
        <source>Dialog</source>
        <translation>Dialogue</translation>
    </message>
    <message>
        <location filename="menudialog.ui" line="120"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="menudialog.cpp" line="214"/>
        <source>CURRENT: </source>
        <translation>COURANT ACTUEL</translation>
    </message>
</context>
<context>
    <name>MixerMenu</name>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="62"/>
        <source>TEST ON</source>
        <translation>TESTER ON</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="63"/>
        <source>TEST OFF</source>
        <translation>TESTER OFF</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="66"/>
        <source>Test cm/s </source>
        <translation>Test cm/s</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="71"/>
        <source>Lin. Increase: ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="75"/>
        <source>Lin. Increase: OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="81"/>
        <source>Sniff cm/s </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="82"/>
        <source>OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="87"/>
        <source>UpThr cm/s </source>
        <translation>Seuil max cm/s</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="89"/>
        <source>DownThr cm/s </source>
        <translation>Seuil min cm/s</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="91"/>
        <location filename="menu/sound/mixermenu.cpp" line="213"/>
        <source>Up F0 Hz</source>
        <translation>Montée F0 Hz</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="93"/>
        <location filename="menu/sound/mixermenu.cpp" line="229"/>
        <source>Up F10 Hz</source>
        <translation>Montée F10 Hz</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="95"/>
        <location filename="menu/sound/mixermenu.cpp" line="246"/>
        <source>Up I0 ms</source>
        <translation>Montée I0 ms</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="97"/>
        <location filename="menu/sound/mixermenu.cpp" line="262"/>
        <source>Up I10 ms</source>
        <translation>Montée I10 ms</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="99"/>
        <location filename="menu/sound/mixermenu.cpp" line="278"/>
        <source>Down F0 Hz</source>
        <translation>Chute F0 Hz</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="101"/>
        <location filename="menu/sound/mixermenu.cpp" line="293"/>
        <source>Down F10 Hz</source>
        <translation>Chute F10 Hz</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="103"/>
        <source>BACK</source>
        <translation>RETOUR</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="146"/>
        <source>Test cm/s</source>
        <translation>Test cm/s</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="165"/>
        <source>Sniff. thr cm/s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="181"/>
        <source>Up thr cm/s</source>
        <translation>Seuil max cm/s</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="197"/>
        <source>Down thr cm/s</source>
        <translation>Seuil min cm/s</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="321"/>
        <source>Toggle test on off. Use to test your settings and accomodate with the beeping.</source>
        <translation>Activer/désactiver le mode teste. À utiliser afin de tester les paramètres et s&apos;accommoder avec les alertes.</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="323"/>
        <source>Set test climb/sink rate.</source>
        <translation>Définir le taux de montée/chute en mode teste.</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="325"/>
        <source>Set climb tone threshold in cm per second.</source>
        <translation>Définir le seuil du ton de montée.en centimètres par seconde.</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="327"/>
        <source>Set sink tone threshold in cm per second.</source>
        <translation>Définir le seuil du ton de chute.en centimètres par seconde.</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="329"/>
        <source>Set thermal sniffer threshold. Go all way to 0 to turn it off.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="331"/>
        <source>Set start frequency for climb.</source>
        <translation>Définir la fréquence initiale de la montée.</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="333"/>
        <source>Set end frequency for climb. Equivalent to 10 m/s.</source>
        <translation>Définir la fréquence initialede la montée,Équivalent á 10m/s.</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="335"/>
        <source>Set start sound interval for climb in ms.</source>
        <translation>Définir l&apos;intervalle du son intial pour la montée (en ms).</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="337"/>
        <source>Set end sound interval for climb in ms. Equivalent to 10 m/s.</source>
        <translation>Définir l&apos;intervalle du son final pour la montée (en ms). Equivalent á 10 m/s.</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="339"/>
        <source>Set start frequency for sink.</source>
        <translation>Définir la fréquence initiale de la chute.</translation>
    </message>
    <message>
        <location filename="menu/sound/mixermenu.cpp" line="341"/>
        <source>Set end frequency for sink. Equivalent to -10 m/s.</source>
        <translation>Définir la fréquence finale de la chute. Equivalente á 10 m/s.</translation>
    </message>
</context>
<context>
    <name>MixerMenuSelect</name>
    <message>
        <location filename="menu/sound/mixerMenuSelect.cpp" line="67"/>
        <source>Legacy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/sound/mixerMenuSelect.cpp" line="68"/>
        <source>Get mixers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/sound/mixerMenuSelect.cpp" line="76"/>
        <source>BACK</source>
        <translation type="unfinished">RETOUR</translation>
    </message>
    <message>
        <location filename="menu/sound/mixerMenuSelect.cpp" line="98"/>
        <location filename="menu/sound/mixerMenuSelect.cpp" line="105"/>
        <location filename="menu/sound/mixerMenuSelect.cpp" line="111"/>
        <location filename="menu/sound/mixerMenuSelect.cpp" line="126"/>
        <location filename="menu/sound/mixerMenuSelect.cpp" line="144"/>
        <location filename="menu/sound/mixerMenuSelect.cpp" line="164"/>
        <source>INFO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/sound/mixerMenuSelect.cpp" line="98"/>
        <location filename="menu/sound/mixerMenuSelect.cpp" line="111"/>
        <source>Config saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/sound/mixerMenuSelect.cpp" line="105"/>
        <source>Check Conn</source>
        <translation type="unfinished">Vérification Connection</translation>
    </message>
    <message>
        <location filename="menu/sound/mixerMenuSelect.cpp" line="126"/>
        <source>Conn OK</source>
        <translation type="unfinished">Connection OK</translation>
    </message>
    <message>
        <location filename="menu/sound/mixerMenuSelect.cpp" line="132"/>
        <location filename="menu/sound/mixerMenuSelect.cpp" line="150"/>
        <source>ERROR</source>
        <translation type="unfinished">ERREUR</translation>
    </message>
    <message>
        <location filename="menu/sound/mixerMenuSelect.cpp" line="132"/>
        <source>Not Connected</source>
        <translation type="unfinished">Non connecté</translation>
    </message>
    <message>
        <location filename="menu/sound/mixerMenuSelect.cpp" line="144"/>
        <source>Success!</source>
        <translation type="unfinished">Succés</translation>
    </message>
    <message>
        <location filename="menu/sound/mixerMenuSelect.cpp" line="150"/>
        <source>Not Updated</source>
        <translation type="unfinished">Mise á jour non effectué</translation>
    </message>
    <message>
        <location filename="menu/sound/mixerMenuSelect.cpp" line="164"/>
        <source>Downloading</source>
        <translation type="unfinished">Téléchargement en cours</translation>
    </message>
</context>
<context>
    <name>NavigationAirspaceM</name>
    <message>
        <location filename="menu/navigation/navigationAirspace.cpp" line="64"/>
        <source>BACK</source>
        <translation>RETOUR</translation>
    </message>
</context>
<context>
    <name>NavigationMapM</name>
    <message>
        <location filename="menu/navigation/navigationMap.cpp" line="47"/>
        <source>AUTO</source>
        <translation>AUTO</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMap.cpp" line="55"/>
        <source>BACK</source>
        <translation>RETOUR</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMap.cpp" line="78"/>
        <location filename="menu/navigation/navigationMap.cpp" line="86"/>
        <source>INFO</source>
        <translation>INFO</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMap.cpp" line="78"/>
        <source>Map set to auto</source>
        <translation>Carte en automatique</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMap.cpp" line="86"/>
        <source>Map was saved</source>
        <translation>Carte enregistrée</translation>
    </message>
</context>
<context>
    <name>NavigationMenu</name>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="63"/>
        <source>AIRSPACE</source>
        <translation>ESPACE AERIEN</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="69"/>
        <source>MAP - AUTO</source>
        <translation>CARTE AUTOMATIQUE</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="70"/>
        <source>MAP - </source>
        <translation>CARTE - </translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="77"/>
        <source>SHOW WOODS - ON</source>
        <translation>MONTRE FORÉTS ON</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="79"/>
        <source>SHOW WOODS - OFF</source>
        <translation></translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="84"/>
        <source>Airspace Front-Up - ON</source>
        <translation>Bordure éspace aérien ON</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="86"/>
        <source>Airspace Front-Up - OFF</source>
        <translation>Bordure éspace aérien OFF</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="97"/>
        <source>V.Near-</source>
        <oldsource>V.Near</oldsource>
        <translation type="unfinished">Proximité Verticale</translation>
    </message>
    <message>
        <source>H.Near</source>
        <translation type="vanished">Proximité Horizontale</translation>
    </message>
    <message>
        <source>H.Very Near</source>
        <translation type="vanished">Trés Proche Horizontal</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="105"/>
        <source>H.Near-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="113"/>
        <source>H.Very Near-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="119"/>
        <source>LOCATION</source>
        <translation>POSITION</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="121"/>
        <source>BACK</source>
        <translation>RETOUR</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="171"/>
        <source>Vertical warning</source>
        <oldsource>Horizontal warning</oldsource>
        <translation>Alarme verticale</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="196"/>
        <source>Near warning</source>
        <translation>Alarme proximitée</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="220"/>
        <source>Very near warning</source>
        <translation>Alarme proximitée trés proche</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="236"/>
        <source>Ignore over</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="286"/>
        <source>Select airspace file to be used from root/airspace folder on SD card.</source>
        <oldsource>Select airspace file to be used from root/airspace folder on SD card</oldsource>
        <translation>Selectionner fichier espace aérien á utiliser depuis root/airspace dossier sur carte SD</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="288"/>
        <source>Set vertical separation to airspace for warning.</source>
        <translation>Définir séparation verticale pour alarme espace aérien</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="290"/>
        <source>Set horizontal separation to airspace for NEAR warning.</source>
        <translation>Définir séparation horizontale espace aérien pour alarme proche</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="292"/>
        <source>Set horizontal separation to airspace for VERY NEAR warning.</source>
        <translation>Définir séparation horizontale pour Alarme proximitée trés proche</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="294"/>
        <source>Maps are autodetected from root/maps folder on SD card.</source>
        <translation>Cartes sont detectées automatiquement depuis root/maps dossier sur SD card</translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="296"/>
        <source>Set superior limmit for airspace display. Note that the automatic airspace infrigement algorithm will still consider the hidden airspaces.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="298"/>
        <source>Toggle airspace plotting. If disables the map will always display with the N to the top. If enabled the map will rotate to display the pront position on the top of the map.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="300"/>
        <source>Hide or show woods on the topo map.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/navigation/navigationMenu.cpp" line="302"/>
        <source>Add or edit/delete location information.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NumericInput</name>
    <message>
        <location filename="NumericInput.ui" line="17"/>
        <source>Dialog</source>
        <translation>Dialogue</translation>
    </message>
</context>
<context>
    <name>NumericInputDecimal</name>
    <message>
        <location filename="NumericInputDecimal.ui" line="17"/>
        <source>Dialog</source>
        <translation>Dialogue</translation>
    </message>
</context>
<context>
    <name>ObexThread</name>
    <message>
        <location filename="threads/obexThread.cpp" line="55"/>
        <source>Map received</source>
        <translation>Carte enregistrée</translation>
    </message>
    <message>
        <location filename="threads/obexThread.cpp" line="60"/>
        <source>Airspace received</source>
        <translation>Espace aérien enregistré</translation>
    </message>
    <message>
        <location filename="threads/obexThread.cpp" line="65"/>
        <source>Waypoints received</source>
        <translation>Waypoint enregistré</translation>
    </message>
    <message>
        <location filename="threads/obexThread.cpp" line="72"/>
        <source>*.xctsk received</source>
        <translation>*,xctsk enregistré</translation>
    </message>
    <message>
        <location filename="threads/obexThread.cpp" line="77"/>
        <source>*.qm received</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RemoteMenu</name>
    <message>
        <location filename="menu/general/remoteMenu.cpp" line="54"/>
        <source>KEY:Left/Up</source>
        <translation>BOUTON:Gauche/UP</translation>
    </message>
    <message>
        <location filename="menu/general/remoteMenu.cpp" line="56"/>
        <source>KEY:Right/Down</source>
        <translation>BOUTON:Droit/Down</translation>
    </message>
    <message>
        <location filename="menu/general/remoteMenu.cpp" line="58"/>
        <source>KEY:Enter</source>
        <translation>BOUTON:Enregistrer</translation>
    </message>
    <message>
        <location filename="menu/general/remoteMenu.cpp" line="60"/>
        <source>KEY:Esc</source>
        <translation>BOUTON Quitter</translation>
    </message>
    <message>
        <location filename="menu/general/remoteMenu.cpp" line="62"/>
        <source>KEY:Function</source>
        <translation>BOUTON Fonction</translation>
    </message>
    <message>
        <location filename="menu/general/remoteMenu.cpp" line="64"/>
        <source>KEY:Menu</source>
        <translation>BOUTON Menu</translation>
    </message>
    <message>
        <location filename="menu/general/remoteMenu.cpp" line="66"/>
        <source>BACK</source>
        <translation>RETOUR</translation>
    </message>
    <message>
        <location filename="menu/general/remoteMenu.cpp" line="78"/>
        <source>ACTION</source>
        <translation>ACTION</translation>
    </message>
    <message>
        <location filename="menu/general/remoteMenu.cpp" line="78"/>
        <source>Press remote button</source>
        <translation>Appuyer Bouton Télécommande</translation>
    </message>
    <message>
        <location filename="menu/general/remoteMenu.cpp" line="85"/>
        <source>WARNING</source>
        <translation>ATTENTION</translation>
    </message>
    <message>
        <location filename="menu/general/remoteMenu.cpp" line="85"/>
        <source>Button not set</source>
        <translation>Bouton non enregistré</translation>
    </message>
    <message>
        <location filename="menu/general/remoteMenu.cpp" line="108"/>
        <source>INFO</source>
        <translation>INFO</translation>
    </message>
    <message>
        <location filename="menu/general/remoteMenu.cpp" line="108"/>
        <source>Key Mapped</source>
        <translation>Bouton mappé</translation>
    </message>
</context>
<context>
    <name>SatelliteStatusPage</name>
    <message>
        <location filename="ui/satelliteStatusPage.cpp" line="67"/>
        <source>Lon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/satelliteStatusPage.cpp" line="71"/>
        <source>Lat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/satelliteStatusPage.cpp" line="75"/>
        <source>Height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/satelliteStatusPage.cpp" line="81"/>
        <source>PRESS ENTER TO CLOSE</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SetNumSampleMenu</name>
    <message>
        <location filename="menu/numSamples/setnumsamplemenu.cpp" line="51"/>
        <source>UP</source>
        <translation>VERS LE HAUT</translation>
    </message>
    <message>
        <location filename="menu/numSamples/setnumsamplemenu.cpp" line="53"/>
        <source>DOWN</source>
        <translation>VERS LE BAS</translation>
    </message>
    <message>
        <location filename="menu/numSamples/setnumsamplemenu.cpp" line="55"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="menu/numSamples/setnumsamplemenu.cpp" line="57"/>
        <source>BACK</source>
        <translation>RETOUR</translation>
    </message>
    <message>
        <location filename="menu/numSamples/setnumsamplemenu.cpp" line="90"/>
        <source>INFO</source>
        <translation>INFO</translation>
    </message>
    <message>
        <location filename="menu/numSamples/setnumsamplemenu.cpp" line="90"/>
        <source>Value was saved</source>
        <translation>Valeur enregistrée</translation>
    </message>
</context>
<context>
    <name>ShutdownMenu</name>
    <message>
        <location filename="menu/shutdown/shutdownmenu.cpp" line="67"/>
        <source>Exit program</source>
        <translation>Sortir Logiciel</translation>
    </message>
    <message>
        <location filename="menu/shutdown/shutdownmenu.cpp" line="70"/>
        <source>Shutdown vario</source>
        <translation>Eteindre Vario</translation>
    </message>
    <message>
        <location filename="menu/shutdown/shutdownmenu.cpp" line="73"/>
        <source>Cancel shutdown</source>
        <translation>Annuler Eteindre</translation>
    </message>
    <message>
        <location filename="menu/shutdown/shutdownmenu.cpp" line="84"/>
        <location filename="menu/shutdown/shutdownmenu.cpp" line="87"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="menu/shutdown/shutdownmenu.cpp" line="90"/>
        <source>CANCEL</source>
        <translation>ANNULER</translation>
    </message>
    <message>
        <location filename="menu/shutdown/shutdownmenu.cpp" line="110"/>
        <location filename="menu/shutdown/shutdownmenu.cpp" line="118"/>
        <source>INFO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/shutdown/shutdownmenu.cpp" line="110"/>
        <source>Exiting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/shutdown/shutdownmenu.cpp" line="118"/>
        <source>Powering Off!</source>
        <translation type="unfinished">Éteindre</translation>
    </message>
</context>
<context>
    <name>SoundMenu</name>
    <message>
        <location filename="menu/sound/soundMenu.cpp" line="58"/>
        <source>VOLUME</source>
        <translation>VOLUME</translation>
    </message>
    <message>
        <location filename="menu/sound/soundMenu.cpp" line="61"/>
        <source>SILENT ON GROUND ON</source>
        <translation>SILENCIEUX AU SOL ON</translation>
    </message>
    <message>
        <location filename="menu/sound/soundMenu.cpp" line="62"/>
        <source>SILENT ON GROUND OFF</source>
        <translation>SILENCIEUX AU SOL OFF</translation>
    </message>
    <message>
        <location filename="menu/sound/soundMenu.cpp" line="65"/>
        <source>SQUARE ON</source>
        <translation>ONDE QUARÉE ALLUMÉE</translation>
    </message>
    <message>
        <location filename="menu/sound/soundMenu.cpp" line="66"/>
        <source>SQUARE OFF</source>
        <translation>ONDE QUARÉE ÉTEINTE</translation>
    </message>
    <message>
        <location filename="menu/sound/soundMenu.cpp" line="69"/>
        <source>SOUND MIXER</source>
        <translation>MIXER SON</translation>
    </message>
    <message>
        <location filename="menu/sound/soundMenu.cpp" line="72"/>
        <source>SOUND MIXER SELECT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/sound/soundMenu.cpp" line="142"/>
        <source>Use to set the vario tone.</source>
        <translation>Utiliser pour changer le son du vario</translation>
    </message>
    <message>
        <location filename="menu/sound/soundMenu.cpp" line="144"/>
        <source>Select sound mixer to be used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Test ON</source>
        <translation type="obsolete">Mode test ALLUMÉ</translation>
    </message>
    <message>
        <source>Test OFF</source>
        <translation type="obsolete">Mode test ÉTEINT</translation>
    </message>
    <message>
        <source>Test cm/s </source>
        <translation type="obsolete">Test cm/s</translation>
    </message>
    <message>
        <source>UpThr cm/s </source>
        <translation type="obsolete">Seuil max cm/s</translation>
    </message>
    <message>
        <source>DownThr cm/s </source>
        <translation type="obsolete">Seuil min cm/s</translation>
    </message>
    <message>
        <source>Up F0 Hz</source>
        <translation type="obsolete">Montée F0 Hz</translation>
    </message>
    <message>
        <source>Up F10 Hz</source>
        <translation type="obsolete">Montée F10 Hz</translation>
    </message>
    <message>
        <source>Up I0 ms</source>
        <translation type="obsolete">Montée I0 ms</translation>
    </message>
    <message>
        <source>Up I10 ms</source>
        <translation type="obsolete">Montée I10 ms</translation>
    </message>
    <message>
        <source>Down F0 Hz</source>
        <translation type="obsolete">Chute F0 Hz</translation>
    </message>
    <message>
        <source>Down F10 Hz</source>
        <translation type="obsolete">Chute F10 Hz</translation>
    </message>
    <message>
        <location filename="menu/sound/soundMenu.cpp" line="75"/>
        <source>BACK</source>
        <translation>RETOUR</translation>
    </message>
    <message>
        <source>Test cm/s</source>
        <translation type="obsolete">Test cm/s</translation>
    </message>
    <message>
        <source>Up thr cm/s</source>
        <translation type="obsolete">Seuil max cm/s</translation>
    </message>
    <message>
        <source>Down thr cm/s</source>
        <translation type="obsolete">Seuil min cm/s</translation>
    </message>
    <message>
        <location filename="menu/sound/soundMenu.cpp" line="136"/>
        <source>Change system volume.</source>
        <oldsource>Change system volume</oldsource>
        <translation>Changer le volume du système.</translation>
    </message>
    <message>
        <location filename="menu/sound/soundMenu.cpp" line="138"/>
        <source>Set if vario is silent on ground.</source>
        <translation>Définir si vario silencieux au sol</translation>
    </message>
    <message>
        <location filename="menu/sound/soundMenu.cpp" line="140"/>
        <source>Toggle square/sine wave. Sine is more pleasant for the ear but less loud especially for low frequency.</source>
        <oldsource>Toggle square/syne wave. Sine is more pleasant for the ear but less loud especially for low frequency.</oldsource>
        <translation>Activer/désactiver l&apos;onde carrée/sinusoïdale. L&apos;onde sinusoïdale est plus plaisante aux oreilles mais moins fort surtout aux basses fréquences.</translation>
    </message>
    <message>
        <source>Toggle test on off. Use to test your settings and accomodate with the beeping.</source>
        <translation type="obsolete">Activer/désactiver le mode teste. À utiliser afin de tester les paramètres et s&apos;accommoder avec les alertes.</translation>
    </message>
    <message>
        <source>Set test climb/sink rate.</source>
        <oldsource>Set test climb/synk rate.</oldsource>
        <translation type="obsolete">Définir le taux de montée/chute en mode teste.</translation>
    </message>
    <message>
        <source>Set climb tone threshold in cm per second.</source>
        <oldsource>Set climb tone threshold in cm per second</oldsource>
        <translation type="obsolete">Définir le seuil du ton de montée.en centimètres per seconde.</translation>
    </message>
    <message>
        <source>Set sink tone threshold in cm per second.</source>
        <oldsource>Set sync tone threshold in cm per second.</oldsource>
        <translation type="obsolete">Définir le seuil du ton de chute.en centimètres per seconde.</translation>
    </message>
    <message>
        <source>Set start frequency for climb.</source>
        <oldsource>Set start freqvency for climb</oldsource>
        <translation type="obsolete">Définir la fréquence initiale de la montée.</translation>
    </message>
    <message>
        <source>Set end frequency for climb. Equivalent to -10 m/s.</source>
        <oldsource>Set end freqvency for climb. Equivalent to -10 m/s</oldsource>
        <translation type="obsolete">Définir la fréquence finale de la montée. Equivalente de 10 m/s.</translation>
    </message>
    <message>
        <source>Set start sound interval for climb in ms.</source>
        <oldsource>Set start sound interval for climb in ms</oldsource>
        <translation type="obsolete">Définir l&apos;intervalle du son intial pour la montée (en ms).</translation>
    </message>
    <message>
        <source>Set end sound interval for climb in ms. Equivalent to 10 m/s.</source>
        <oldsource>Set end sound interval for climb in ms. Equivalent to 10 m/s</oldsource>
        <translation type="obsolete">Définir l&apos;intervalle du son final pour la montée (en ms). Equivalent de 10 m/s.</translation>
    </message>
    <message>
        <source>Set start frequency for sink.</source>
        <oldsource>Set start frequency for sync.</oldsource>
        <translation type="obsolete">Définir la fréquence initiale de la chute.</translation>
    </message>
    <message>
        <source>Set end frequency for sink. Equivalent to -10 m/s.</source>
        <oldsource>Set end frequency for sync. Equivalent to -10 m/s.</oldsource>
        <translation type="obsolete">Définir la fréquence finale de la chute. Equivalente de 10 m/s.</translation>
    </message>
</context>
<context>
    <name>SystemMenu</name>
    <message>
        <location filename="menu/system/systemMenu.cpp" line="73"/>
        <source>CPU LOAD</source>
        <translation>CHARGE PROCESSEUR</translation>
    </message>
    <message>
        <location filename="menu/system/systemMenu.cpp" line="75"/>
        <source>BOOT TIME</source>
        <translation>TEMPS DÉMARRAGE</translation>
    </message>
    <message>
        <location filename="menu/system/systemMenu.cpp" line="77"/>
        <source>BACK</source>
        <translation>RETOUR</translation>
    </message>
    <message>
        <location filename="menu/system/systemMenu.cpp" line="108"/>
        <source>sysd boot</source>
        <translation>démarrage sysd</translation>
    </message>
    <message>
        <location filename="menu/system/systemMenu.cpp" line="108"/>
        <source>Created</source>
        <translation>Crée</translation>
    </message>
</context>
<context>
    <name>Task</name>
    <message>
        <location filename="navWaypoint/task.cpp" line="644"/>
        <location filename="navWaypoint/task.cpp" line="655"/>
        <location filename="navWaypoint/task.cpp" line="676"/>
        <location filename="navWaypoint/task.cpp" line="681"/>
        <location filename="navWaypoint/task.cpp" line="697"/>
        <location filename="navWaypoint/task.cpp" line="702"/>
        <location filename="navWaypoint/task.cpp" line="707"/>
        <source>TASK INFO</source>
        <translation>INFO MANCHE</translation>
    </message>
    <message>
        <location filename="navWaypoint/task.cpp" line="644"/>
        <source>5 MIN TO SSS</source>
        <translation>5 MINUTES VERS SSS</translation>
    </message>
    <message>
        <location filename="navWaypoint/task.cpp" line="655"/>
        <source>START OPEN</source>
        <translation>START OUVERT</translation>
    </message>
    <message>
        <location filename="navWaypoint/task.cpp" line="676"/>
        <source>TASK STARTED</source>
        <translation>MANCHE DÉMARRÉE</translation>
    </message>
    <message>
        <location filename="navWaypoint/task.cpp" line="681"/>
        <source>TASK RESTARTED</source>
        <translation>MANCHE REDÉMARRÉE</translation>
    </message>
    <message>
        <location filename="navWaypoint/task.cpp" line="697"/>
        <source>ESS COMPLETED</source>
        <translation>ESS ACHEVÉ♣</translation>
    </message>
    <message>
        <location filename="navWaypoint/task.cpp" line="702"/>
        <source>TP REACHED</source>
        <translation>TP ACOMPLI</translation>
    </message>
    <message>
        <location filename="navWaypoint/task.cpp" line="707"/>
        <source>TASK FINISHED</source>
        <translation>MANCHE FINIE</translation>
    </message>
</context>
<context>
    <name>TaskEditMenu</name>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="66"/>
        <source>-TASK TYPE-</source>
        <translation>-GENRE DE MANCHE-</translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="69"/>
        <source>ELAPSED TIME</source>
        <translation>TEMPS ÉCOULÉ</translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="70"/>
        <source>RACE</source>
        <translation>COURSE</translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="71"/>
        <location filename="menu/task/taskEditMenu.cpp" line="82"/>
        <source>TIME GATES</source>
        <translation>TEMPS PORTES</translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="74"/>
        <source>-SSS OPEN-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="78"/>
        <source>-TASK DEADLINE-</source>
        <translation>FIN MANCHE</translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="84"/>
        <source>TURN POINTS</source>
        <translation>TP/PORTES</translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="86"/>
        <source>SAVE/PREPARE/RESET</source>
        <oldsource>PREPARE/RESET</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="88"/>
        <location filename="menu/task/taskEditMenu.cpp" line="158"/>
        <source>CLEAR</source>
        <translation>EFFACER</translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="90"/>
        <source>BACK</source>
        <translation>RETOUR</translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="152"/>
        <source>INFO</source>
        <translation>INFO</translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="152"/>
        <source>TASK PREPARED</source>
        <translation>MANCHE PREPARÉE</translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="159"/>
        <source>DELETE?</source>
        <translation>EFFACER?</translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="160"/>
        <source>CANCEL</source>
        <translation>ANNULER</translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="161"/>
        <source>DELETE</source>
        <translation>EFFACER</translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="191"/>
        <source>Select competition rules for the task.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="193"/>
        <source>Choose the opening time for the task. If this is a Time Gate competition this will also be the first time gate.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="195"/>
        <source>This is the time until you can score competition points.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="197"/>
        <source>If this is a Time Gate competition you can add new Time Gates here.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="199"/>
        <source>Add/Edit turnpoints.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="201"/>
        <source>Prepare task to be flown. This action is also done automatically at takeoff.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskEditMenu.cpp" line="203"/>
        <source>Clear task.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TaskImportMenu</name>
    <message>
        <location filename="menu/task/taskImportMenu.cpp" line="55"/>
        <source>BACK</source>
        <translation type="unfinished">RETOUR</translation>
    </message>
    <message>
        <location filename="menu/task/taskImportMenu.cpp" line="88"/>
        <source>FILE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskImportMenu.cpp" line="89"/>
        <source>ACTION?</source>
        <translation type="unfinished">ACTION?</translation>
    </message>
    <message>
        <location filename="menu/task/taskImportMenu.cpp" line="90"/>
        <source>CANCEL</source>
        <translation type="unfinished">ANNULER</translation>
    </message>
    <message>
        <location filename="menu/task/taskImportMenu.cpp" line="91"/>
        <source>DELETE</source>
        <translation type="unfinished">EFFACER</translation>
    </message>
    <message>
        <location filename="menu/task/taskImportMenu.cpp" line="92"/>
        <source>IMPORT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskImportMenu.cpp" line="108"/>
        <location filename="menu/task/taskImportMenu.cpp" line="110"/>
        <location filename="menu/task/taskImportMenu.cpp" line="119"/>
        <source>INFO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskImportMenu.cpp" line="108"/>
        <source>TASK IMPORTED</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskImportMenu.cpp" line="110"/>
        <source>TASK IMPORT FAILED</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskImportMenu.cpp" line="119"/>
        <source>REMOVED</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TaskMenu</name>
    <message>
        <location filename="menu/task/taskMenu.cpp" line="57"/>
        <source>WAYPOINTS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskMenu.cpp" line="59"/>
        <source>TASK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskMenu.cpp" line="61"/>
        <source>IMPORT TASK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskMenu.cpp" line="63"/>
        <source>GPS INFO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskMenu.cpp" line="65"/>
        <source>BACK</source>
        <translation type="unfinished">RETOUR</translation>
    </message>
    <message>
        <location filename="menu/task/taskMenu.cpp" line="119"/>
        <source>Manage waypoints for tasks.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskMenu.cpp" line="121"/>
        <source>Create/Edit task.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskMenu.cpp" line="123"/>
        <source>Import task.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskMenu.cpp" line="125"/>
        <source>Show gps info page</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TaskNavigationMenu</name>
    <message>
        <location filename="menu/task/taskNavigationMenu.cpp" line="63"/>
        <source>Nav to TO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenu.cpp" line="65"/>
        <source>Nav to task Tp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenu.cpp" line="67"/>
        <source>Nav to WP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenu.cpp" line="69"/>
        <source>Back to task</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenu.cpp" line="71"/>
        <source>Reset task</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenu.cpp" line="73"/>
        <source>Zoom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenu.cpp" line="77"/>
        <source>C. to TP1 ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenu.cpp" line="78"/>
        <source>C. to TP1 OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenu.cpp" line="81"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenu.cpp" line="124"/>
        <location filename="menu/task/taskNavigationMenu.cpp" line="135"/>
        <location filename="menu/task/taskNavigationMenu.cpp" line="141"/>
        <source>NAV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenu.cpp" line="124"/>
        <source>Nav to T.O.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenu.cpp" line="135"/>
        <source>Task reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenu.cpp" line="141"/>
        <source>Nav to task</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenu.cpp" line="169"/>
        <source>Navigate to takeoff point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenu.cpp" line="171"/>
        <source>Reset task to first turnpoint</source>
        <oldsource>Reset task to firts turnpoint</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenu.cpp" line="173"/>
        <source>Go back to task navigation after a waypoint navigation was activated. Task will be resumed from the last activated turnpoint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenu.cpp" line="175"/>
        <source>Navigate to a turnpoint defined in the task</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenu.cpp" line="177"/>
        <source>Navigate to a loaded waypoint, not necessary from the active task</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenu.cpp" line="179"/>
        <source>Zoom functionality for task widget.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TaskNavigationMenuTp</name>
    <message>
        <location filename="menu/task/taskNavigationMenuTp.cpp" line="52"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenuTp.cpp" line="87"/>
        <source>NAV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenuTp.cpp" line="87"/>
        <source>Turnpoint set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenuTp.cpp" line="109"/>
        <source>Navigate to:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenuTp.cpp" line="118"/>
        <source>LINE</source>
        <translation type="unfinished">LIGNE</translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenuTp.cpp" line="119"/>
        <source>CYLINDER ENTER</source>
        <oldsource>CILINDER ENTER</oldsource>
        <translation type="unfinished">ENTRÉE CYLINDRE</translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenuTp.cpp" line="120"/>
        <source>CYLINDER EXIT</source>
        <oldsource>CILINDER EXIT</oldsource>
        <translation type="unfinished">SORTIE CYLINDRE</translation>
    </message>
</context>
<context>
    <name>TaskNavigationMenuWp</name>
    <message>
        <location filename="menu/task/taskNavigationMenuWp.cpp" line="56"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenuWp.cpp" line="58"/>
        <source>Sort by dist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenuWp.cpp" line="59"/>
        <source>Sort by name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenuWp.cpp" line="111"/>
        <source>NAV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenuWp.cpp" line="111"/>
        <source>Waypoint set</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TaskNavigationMenuZoom</name>
    <message>
        <location filename="menu/task/taskNavigationMenuZoom.cpp" line="57"/>
        <source>Zoom In</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenuZoom.cpp" line="59"/>
        <source>Zoom Out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenuZoom.cpp" line="61"/>
        <source>Zoom to Fit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/taskNavigationMenuZoom.cpp" line="63"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TaskTypeMenu</name>
    <message>
        <location filename="menu/task/taskTypeMenu.cpp" line="54"/>
        <source>RACE</source>
        <translation type="unfinished">COURSE</translation>
    </message>
    <message>
        <location filename="menu/task/taskTypeMenu.cpp" line="56"/>
        <source>TIME GATES</source>
        <translation type="unfinished">TEMPS PORTES</translation>
    </message>
    <message>
        <location filename="menu/task/taskTypeMenu.cpp" line="58"/>
        <source>ELAPSED TIME</source>
        <translation type="unfinished">TEMPS ÉCOULÉ</translation>
    </message>
</context>
<context>
    <name>TermalingDetectorMenu</name>
    <message>
        <location filename="menu/avionics/thermalingDetectorMenu.cpp" line="56"/>
        <source>DETECT ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/avionics/thermalingDetectorMenu.cpp" line="58"/>
        <source>DETECT OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/avionics/thermalingDetectorMenu.cpp" line="62"/>
        <source>SATURATION</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/avionics/thermalingDetectorMenu.cpp" line="68"/>
        <source>DECAY</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/avionics/thermalingDetectorMenu.cpp" line="74"/>
        <source>ACTIVATE THRESHOLD</source>
        <oldsource>ACTIVATE TRESHOLD</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/avionics/thermalingDetectorMenu.cpp" line="80"/>
        <source>DEACTIVATE THRESHOLD</source>
        <oldsource>DEACTIVATE TRESHOLD</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/avionics/thermalingDetectorMenu.cpp" line="86"/>
        <source>BACK</source>
        <translation type="unfinished">RETOUR</translation>
    </message>
    <message>
        <location filename="menu/avionics/thermalingDetectorMenu.cpp" line="149"/>
        <source>Saturation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/avionics/thermalingDetectorMenu.cpp" line="165"/>
        <source>Decay</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/avionics/thermalingDetectorMenu.cpp" line="181"/>
        <source>Activate th.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/avionics/thermalingDetectorMenu.cpp" line="197"/>
        <source>Deactivate th.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/avionics/thermalingDetectorMenu.cpp" line="214"/>
        <source>Activate/Deactivate thermaling detector.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/avionics/thermalingDetectorMenu.cpp" line="216"/>
        <source>Saturation value. Used to limmit maximum turn integral to facilitate deactivation. Recommended 150.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/avionics/thermalingDetectorMenu.cpp" line="218"/>
        <source>Decay value. Each unit is equivalent to 5 deg/sec decay. Recommended 1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/avionics/thermalingDetectorMenu.cpp" line="220"/>
        <source>Threshold that will trigger an activate event. Recommended 80.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/avionics/thermalingDetectorMenu.cpp" line="222"/>
        <source>Threshold that will trigger an deactivate event. Recommended 60.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TextInput</name>
    <message>
        <location filename="TextInput.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished">Dialogue</translation>
    </message>
</context>
<context>
    <name>TimeGatesMenu</name>
    <message>
        <location filename="menu/task/timeGatesMenu.cpp" line="58"/>
        <source>ADD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/timeGatesMenu.cpp" line="60"/>
        <location filename="menu/task/timeGatesMenu.cpp" line="131"/>
        <source>CLEAR</source>
        <translation type="unfinished">EFFACER</translation>
    </message>
    <message>
        <location filename="menu/task/timeGatesMenu.cpp" line="62"/>
        <source>BACK</source>
        <translation type="unfinished">RETOUR</translation>
    </message>
    <message>
        <location filename="menu/task/timeGatesMenu.cpp" line="64"/>
        <source>-TIME GATES-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/timeGatesMenu.cpp" line="132"/>
        <source>DELETE?</source>
        <translation type="unfinished">EFFACER?</translation>
    </message>
    <message>
        <location filename="menu/task/timeGatesMenu.cpp" line="133"/>
        <source>CANCEL</source>
        <translation type="unfinished">ANNULER</translation>
    </message>
    <message>
        <location filename="menu/task/timeGatesMenu.cpp" line="134"/>
        <source>DELETE</source>
        <translation type="unfinished">EFFACER</translation>
    </message>
</context>
<context>
    <name>TimeInput</name>
    <message>
        <location filename="TimeInput.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished">Dialogue</translation>
    </message>
</context>
<context>
    <name>TimezoneMenu</name>
    <message>
        <location filename="menu/timezone/timezonemenu.cpp" line="39"/>
        <source>UP</source>
        <translation type="unfinished">VERS LE HAUT</translation>
    </message>
    <message>
        <location filename="menu/timezone/timezonemenu.cpp" line="41"/>
        <source>DOWN</source>
        <translation type="unfinished">VERS LE BAS</translation>
    </message>
    <message>
        <location filename="menu/timezone/timezonemenu.cpp" line="43"/>
        <source>BACK</source>
        <translation type="unfinished">RETOUR</translation>
    </message>
    <message>
        <location filename="menu/timezone/timezonemenu.cpp" line="76"/>
        <source>Timezone modifications requires reboot to take effect!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TurnpointEditMenu</name>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="74"/>
        <source>-TURN POINT-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="78"/>
        <source>-GOAL-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="81"/>
        <source>LINE</source>
        <translation type="unfinished">LIGNE</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="82"/>
        <source>CILINDER ENTER</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="83"/>
        <source>CILINDER EXIT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="86"/>
        <source>-RADIUS/LENGTH-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="96"/>
        <source>ADD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="100"/>
        <source>SAVE</source>
        <translation type="unfinished">MÉmoriser</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="104"/>
        <source>BACK</source>
        <translation type="unfinished">RETOUR</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="106"/>
        <source>SET SSS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="108"/>
        <source>SET ESS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="110"/>
        <location filename="menu/task/turnpointEditMenu.cpp" line="187"/>
        <source>DELETE</source>
        <translation type="unfinished">EFFACER</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="173"/>
        <location filename="menu/task/turnpointEditMenu.cpp" line="179"/>
        <location filename="menu/task/turnpointEditMenu.cpp" line="196"/>
        <location filename="menu/task/turnpointEditMenu.cpp" line="217"/>
        <location filename="menu/task/turnpointEditMenu.cpp" line="222"/>
        <source>INFO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="217"/>
        <source>TP ADDED</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="222"/>
        <source>TP SAVED</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="173"/>
        <source>SSS SAVED</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="179"/>
        <source>ESS SAVED</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="184"/>
        <source>CLEAR</source>
        <translation type="unfinished">EFFACER</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="185"/>
        <source>DELETE?</source>
        <translation type="unfinished">EFFACER?</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="186"/>
        <source>CANCEL</source>
        <translation type="unfinished">ANNULER</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointEditMenu.cpp" line="196"/>
        <source>TP DELETED</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TurnpointMenu</name>
    <message>
        <location filename="menu/task/turnpointMenu.cpp" line="56"/>
        <source>ADD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/turnpointMenu.cpp" line="58"/>
        <location filename="menu/task/turnpointMenu.cpp" line="104"/>
        <source>CLEAR</source>
        <translation type="unfinished">EFFACER</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointMenu.cpp" line="60"/>
        <source>BACK</source>
        <translation type="unfinished">RETOUR</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointMenu.cpp" line="62"/>
        <source>-TPs-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/turnpointMenu.cpp" line="105"/>
        <source>DELETE?</source>
        <translation type="unfinished">EFFACER?</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointMenu.cpp" line="106"/>
        <source>CANCEL</source>
        <translation type="unfinished">ANNULER</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointMenu.cpp" line="107"/>
        <source>DELETE</source>
        <translation type="unfinished">EFFACER</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointMenu.cpp" line="132"/>
        <source>Move/Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/turnpointMenu.cpp" line="136"/>
        <source>Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/turnpointMenu.cpp" line="137"/>
        <source>Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/turnpointMenu.cpp" line="138"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/turnpointMenu.cpp" line="165"/>
        <source>Add a new turnpoint from an existing waypoint.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/turnpointMenu.cpp" line="167"/>
        <source>Clear all turnpoints.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/turnpointMenu.cpp" line="171"/>
        <source>Edit/modify turnpoint and set custom SSS and ESS.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/turnpointMenu.cpp" line="179"/>
        <source>LINE</source>
        <translation type="unfinished">LIGNE</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointMenu.cpp" line="180"/>
        <source>CYLINDER ENTER</source>
        <oldsource>CILINDER ENTER</oldsource>
        <translation type="unfinished">ENTRÉE CYLINDRE</translation>
    </message>
    <message>
        <location filename="menu/task/turnpointMenu.cpp" line="181"/>
        <source>CYLINDER EXIT</source>
        <oldsource>CILINDER EXIT</oldsource>
        <translation type="unfinished">SORTIE CYLINDRE</translation>
    </message>
</context>
<context>
    <name>USBDialog</name>
    <message>
        <location filename="ui/usbdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished">Dialogue</translation>
    </message>
</context>
<context>
    <name>USBDialogMenu</name>
    <message>
        <location filename="menu/usbDialog/usbdialogmenu.cpp" line="42"/>
        <source>GLIDES TO USB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/usbDialog/usbdialogmenu.cpp" line="44"/>
        <source>COPY BOOTLOADER</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/usbDialog/usbdialogmenu.cpp" line="46"/>
        <source>DUMP TO USB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/usbDialog/usbdialogmenu.cpp" line="48"/>
        <source>HIDE</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>USBListMenu</name>
    <message>
        <location filename="menu/usbDialog/usblistmenu.cpp" line="128"/>
        <source>COPYING GLIDES...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/usbDialog/usblistmenu.cpp" line="136"/>
        <location filename="menu/usbDialog/usblistmenu.cpp" line="162"/>
        <source>ERROR UNMOUNT /mnt/USB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/usbDialog/usblistmenu.cpp" line="173"/>
        <source>MOUNT FS TO /mnt/USB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/usbDialog/usblistmenu.cpp" line="177"/>
        <source>ERROR MOUNTING /mnt/USB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/usbDialog/usblistmenu.cpp" line="139"/>
        <location filename="menu/usbDialog/usblistmenu.cpp" line="165"/>
        <source>DONE</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UiMenu</name>
    <message>
        <location filename="menu/general/uiMenu.cpp" line="84"/>
        <source>GET UI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/general/uiMenu.cpp" line="86"/>
        <source>BACK</source>
        <translation type="unfinished">RETOUR</translation>
    </message>
    <message>
        <location filename="menu/general/uiMenu.cpp" line="119"/>
        <location filename="menu/general/uiMenu.cpp" line="127"/>
        <location filename="menu/general/uiMenu.cpp" line="154"/>
        <location filename="menu/general/uiMenu.cpp" line="172"/>
        <location filename="menu/general/uiMenu.cpp" line="187"/>
        <location filename="menu/general/uiMenu.cpp" line="208"/>
        <source>INFO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/general/uiMenu.cpp" line="119"/>
        <source>Check Conn</source>
        <translation type="unfinished">Vérification Connection</translation>
    </message>
    <message>
        <location filename="menu/general/uiMenu.cpp" line="127"/>
        <source>UI Set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/general/uiMenu.cpp" line="140"/>
        <source>Get the user interfaces form your account or the standard recommended one if no account connected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/general/uiMenu.cpp" line="142"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/general/uiMenu.cpp" line="154"/>
        <source>Conn OK</source>
        <translation type="unfinished">Connection OK</translation>
    </message>
    <message>
        <location filename="menu/general/uiMenu.cpp" line="160"/>
        <location filename="menu/general/uiMenu.cpp" line="194"/>
        <source>ERROR</source>
        <translation type="unfinished">ERREUR</translation>
    </message>
    <message>
        <location filename="menu/general/uiMenu.cpp" line="160"/>
        <source>Not Connected</source>
        <translation type="unfinished">Non connecté</translation>
    </message>
    <message>
        <location filename="menu/general/uiMenu.cpp" line="172"/>
        <source>Success!</source>
        <translation type="unfinished">Succés</translation>
    </message>
    <message>
        <location filename="menu/general/uiMenu.cpp" line="187"/>
        <source>Set to default.xml</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/general/uiMenu.cpp" line="194"/>
        <source>Not Updated</source>
        <translation type="unfinished">Mise á jour non effectué</translation>
    </message>
    <message>
        <location filename="menu/general/uiMenu.cpp" line="208"/>
        <source>Downloading</source>
        <translation type="unfinished">Téléchargement en cours</translation>
    </message>
</context>
<context>
    <name>VarioAverageMenu</name>
    <message>
        <location filename="menu/varioAverage/varioaveragemenu.cpp" line="48"/>
        <source>UP</source>
        <translation type="unfinished">VERS LE HAUT</translation>
    </message>
    <message>
        <location filename="menu/varioAverage/varioaveragemenu.cpp" line="50"/>
        <source>DOWN</source>
        <translation type="unfinished">VERS LE BAS</translation>
    </message>
    <message>
        <location filename="menu/varioAverage/varioaveragemenu.cpp" line="52"/>
        <source>BACK</source>
        <translation type="unfinished">RETOUR</translation>
    </message>
</context>
<context>
    <name>VarioScale</name>
    <message>
        <location filename="varioscale.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished">Formulaire</translation>
    </message>
</context>
<context>
    <name>VolumeMenu</name>
    <message>
        <location filename="menu/sound/volumemenu.cpp" line="45"/>
        <source>UP</source>
        <translation type="unfinished">VERS LE HAUT</translation>
    </message>
    <message>
        <location filename="menu/sound/volumemenu.cpp" line="47"/>
        <source>DOWN</source>
        <translation type="unfinished">VERS LE BAS</translation>
    </message>
    <message>
        <location filename="menu/sound/volumemenu.cpp" line="49"/>
        <source>BACK</source>
        <translation type="unfinished">RETOUR</translation>
    </message>
</context>
<context>
    <name>Warning</name>
    <message>
        <location filename="Warning.ui" line="17"/>
        <source>Dialog</source>
        <translation type="unfinished">Dialogue</translation>
    </message>
</context>
<context>
    <name>WaypointEditMenu</name>
    <message>
        <location filename="menu/task/waypointEdit.cpp" line="80"/>
        <source>-LATITUDE-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/waypointEdit.cpp" line="82"/>
        <source>-LONGITUDE-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/waypointEdit.cpp" line="84"/>
        <source>-NAME-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/waypointEdit.cpp" line="86"/>
        <source>-DESC-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/waypointEdit.cpp" line="88"/>
        <source>-HEIGHT-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/waypointEdit.cpp" line="92"/>
        <source>ADD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/waypointEdit.cpp" line="94"/>
        <source>SAVE</source>
        <translation type="unfinished">MÉmoriser</translation>
    </message>
    <message>
        <location filename="menu/task/waypointEdit.cpp" line="97"/>
        <source>DELETE</source>
        <translation type="unfinished">EFFACER</translation>
    </message>
    <message>
        <location filename="menu/task/waypointEdit.cpp" line="100"/>
        <source>BACK</source>
        <translation type="unfinished">RETOUR</translation>
    </message>
    <message>
        <location filename="menu/task/waypointEdit.cpp" line="207"/>
        <source>Height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/waypointEdit.cpp" line="223"/>
        <location filename="menu/task/waypointEdit.cpp" line="228"/>
        <location filename="menu/task/waypointEdit.cpp" line="234"/>
        <source>INFO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/waypointEdit.cpp" line="223"/>
        <source>WP ADDED</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/waypointEdit.cpp" line="228"/>
        <source>WP INVALID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/waypointEdit.cpp" line="234"/>
        <source>WP UPDATED</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WaypointLoad</name>
    <message>
        <location filename="menu/task/waypointLoad.cpp" line="57"/>
        <source>BACK</source>
        <translation type="unfinished">RETOUR</translation>
    </message>
    <message>
        <location filename="menu/task/waypointLoad.cpp" line="90"/>
        <source>FILE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/waypointLoad.cpp" line="91"/>
        <source>ACTION?</source>
        <translation type="unfinished">ACTION?</translation>
    </message>
    <message>
        <location filename="menu/task/waypointLoad.cpp" line="92"/>
        <source>REMOVE</source>
        <translation type="unfinished">RETIRER</translation>
    </message>
    <message>
        <location filename="menu/task/waypointLoad.cpp" line="93"/>
        <source>CANCEL</source>
        <translation type="unfinished">ANNULER</translation>
    </message>
    <message>
        <location filename="menu/task/waypointLoad.cpp" line="94"/>
        <source>LOAD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/waypointLoad.cpp" line="107"/>
        <location filename="menu/task/waypointLoad.cpp" line="113"/>
        <location filename="menu/task/waypointLoad.cpp" line="119"/>
        <location filename="menu/task/waypointLoad.cpp" line="122"/>
        <location filename="menu/task/waypointLoad.cpp" line="125"/>
        <source>ERROR</source>
        <translation type="unfinished">ERREUR</translation>
    </message>
    <message>
        <location filename="menu/task/waypointLoad.cpp" line="110"/>
        <location filename="menu/task/waypointLoad.cpp" line="116"/>
        <location filename="menu/task/waypointLoad.cpp" line="133"/>
        <source>INFO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/waypointLoad.cpp" line="110"/>
        <source>WPT LOADED</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/waypointLoad.cpp" line="113"/>
        <source>LOAD FAILED</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/waypointLoad.cpp" line="116"/>
        <source>NOT ALL WPT LOADED</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/waypointLoad.cpp" line="119"/>
        <source>UNSUPPORTED FORMAT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/waypointLoad.cpp" line="122"/>
        <source>UNKNOWN FORMAT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/waypointLoad.cpp" line="125"/>
        <source>UNHANDLED STATE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/waypointLoad.cpp" line="133"/>
        <source>WPT REMOVED</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WaypointMenu</name>
    <message>
        <location filename="menu/task/waypointMenu.cpp" line="59"/>
        <source>ADD WP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/waypointMenu.cpp" line="61"/>
        <source>CLEAR ALL WPs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/waypointMenu.cpp" line="63"/>
        <source>SAVE WP LIST</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/waypointMenu.cpp" line="65"/>
        <source>LOAD WP LIST</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/waypointMenu.cpp" line="67"/>
        <source>BACK</source>
        <translation type="unfinished">RETOUR</translation>
    </message>
    <message>
        <location filename="menu/task/waypointMenu.cpp" line="108"/>
        <source>CLEAR</source>
        <translation type="unfinished">EFFACER</translation>
    </message>
    <message>
        <location filename="menu/task/waypointMenu.cpp" line="109"/>
        <source>DELETE?</source>
        <translation type="unfinished">EFFACER?</translation>
    </message>
    <message>
        <location filename="menu/task/waypointMenu.cpp" line="110"/>
        <location filename="menu/task/waypointMenu.cpp" line="138"/>
        <source>CANCEL</source>
        <translation type="unfinished">ANNULER</translation>
    </message>
    <message>
        <location filename="menu/task/waypointMenu.cpp" line="111"/>
        <source>DELETE</source>
        <translation type="unfinished">EFFACER</translation>
    </message>
    <message>
        <location filename="menu/task/waypointMenu.cpp" line="136"/>
        <source>FILE EXISTS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/waypointMenu.cpp" line="137"/>
        <source>REPLACE?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/waypointMenu.cpp" line="139"/>
        <source>REPLACE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/waypointMenu.cpp" line="150"/>
        <source>INFO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/waypointMenu.cpp" line="150"/>
        <source>File saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/waypointMenu.cpp" line="154"/>
        <source>ERROR</source>
        <translation type="unfinished">ERREUR</translation>
    </message>
    <message>
        <location filename="menu/task/waypointMenu.cpp" line="154"/>
        <source>File not saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/waypointMenu.cpp" line="188"/>
        <source>Lat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/waypointMenu.cpp" line="189"/>
        <source>Lon</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WaypointSelectMenu</name>
    <message>
        <location filename="menu/task/waypointSelectMenu.cpp" line="53"/>
        <source>BACK</source>
        <translation type="unfinished">RETOUR</translation>
    </message>
    <message>
        <location filename="menu/task/waypointSelectMenu.cpp" line="98"/>
        <source>Lat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/task/waypointSelectMenu.cpp" line="99"/>
        <source>Lon</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WiFiDeviceMenu</name>
    <message>
        <location filename="menu/general/WiFiDevice.cpp" line="81"/>
        <source>WIFI SEARCH</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/general/WiFiDevice.cpp" line="83"/>
        <location filename="menu/general/WiFiDevice.cpp" line="156"/>
        <source>BACK</source>
        <translation type="unfinished">RETOUR</translation>
    </message>
    <message>
        <location filename="menu/general/WiFiDevice.cpp" line="151"/>
        <source>Rescan WiFi networks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/general/WiFiDevice.cpp" line="153"/>
        <source>No WiFiAdapter available
</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WindAverageMenu</name>
    <message>
        <location filename="menu/wind/windaveragemenu.cpp" line="54"/>
        <source>UP</source>
        <translation type="unfinished">VERS LE HAUT</translation>
    </message>
    <message>
        <location filename="menu/wind/windaveragemenu.cpp" line="56"/>
        <source>DOWN</source>
        <translation type="unfinished">VERS LE BAS</translation>
    </message>
    <message>
        <location filename="menu/wind/windaveragemenu.cpp" line="58"/>
        <source>BACK</source>
        <translation type="unfinished">RETOUR</translation>
    </message>
</context>
<context>
    <name>WindDSpreadMenu</name>
    <message>
        <location filename="menu/wind/winddspreadmenu.cpp" line="46"/>
        <source>UP</source>
        <translation type="unfinished">VERS LE HAUT</translation>
    </message>
    <message>
        <location filename="menu/wind/winddspreadmenu.cpp" line="48"/>
        <source>DOWN</source>
        <translation type="unfinished">VERS LE BAS</translation>
    </message>
    <message>
        <location filename="menu/wind/winddspreadmenu.cpp" line="50"/>
        <source>BACK</source>
        <translation type="unfinished">RETOUR</translation>
    </message>
</context>
<context>
    <name>WindMenu</name>
    <message>
        <location filename="menu/wind/windmenu.cpp" line="58"/>
        <source>NUM READINGS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/wind/windmenu.cpp" line="64"/>
        <source>NUM AVERAGE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/wind/windmenu.cpp" line="70"/>
        <source>DATA SPREAD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/wind/windmenu.cpp" line="74"/>
        <source>RMS ERROR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/wind/windmenu.cpp" line="78"/>
        <source>BACK</source>
        <translation type="unfinished">RETOUR</translation>
    </message>
    <message>
        <location filename="menu/wind/windmenu.cpp" line="129"/>
        <source>Num Readings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/wind/windmenu.cpp" line="145"/>
        <source>Num Avg.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/wind/windmenu.cpp" line="161"/>
        <source>Data spreed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/wind/windmenu.cpp" line="177"/>
        <source>RMS Err.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/wind/windmenu.cpp" line="194"/>
        <source>Set number of readings to for wind algorithm.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/wind/windmenu.cpp" line="196"/>
        <source>Set average number of readings for wind algorithm.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/wind/windmenu.cpp" line="198"/>
        <source>Set data spread for wind algorithm.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="menu/wind/windmenu.cpp" line="200"/>
        <source>Set RMS error for wind algorithm.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WindReadingsMenu</name>
    <message>
        <location filename="menu/wind/windreadingsmenu.cpp" line="54"/>
        <source>UP</source>
        <translation type="unfinished">VERS LE HAUT</translation>
    </message>
    <message>
        <location filename="menu/wind/windreadingsmenu.cpp" line="56"/>
        <source>DOWN</source>
        <translation type="unfinished">VERS LE BAS</translation>
    </message>
    <message>
        <location filename="menu/wind/windreadingsmenu.cpp" line="58"/>
        <source>BACK</source>
        <translation type="unfinished">RETOUR</translation>
    </message>
</context>
<context>
    <name>WindRmsErrorMenu</name>
    <message>
        <location filename="menu/wind/windrmserrormenu.cpp" line="54"/>
        <source>UP</source>
        <translation type="unfinished">VERS LE HAUT</translation>
    </message>
    <message>
        <location filename="menu/wind/windrmserrormenu.cpp" line="56"/>
        <source>DOWN</source>
        <translation type="unfinished">VERS LE BAS</translation>
    </message>
    <message>
        <location filename="menu/wind/windrmserrormenu.cpp" line="58"/>
        <source>BACK</source>
        <translation type="unfinished">RETOUR</translation>
    </message>
</context>
<context>
    <name>glideDetails</name>
    <message>
        <location filename="GlideDetails.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished">Dialogue</translation>
    </message>
</context>
</TS>
