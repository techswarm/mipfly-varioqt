/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MENUDIALOG_H
#define MENUDIALOG_H

#include <QDialog>

#include "serial/serialconnection.h"
#include "menu/mainmenu.h"
#include "menu/task/taskNavigationMenu.h"

namespace Ui {
class MenuDialog;
}

class WindManager;
class GlideManager;
class GPIOEmulator;
class nmeaParser;
class Fb2SPIThread;

class LocationManager;
class VarioCompute;
class GPSThread;

class MenuDialog : public QDialog
{
    Q_OBJECT

public:
    int state = 1;

    explicit MenuDialog(QWidget *parent = 0);
    ~MenuDialog();

    GPIOEmulator& gpioEmulator() const;

    void resetToRoot();

    Ui::MenuDialog& getUi() const;

    void setChooserConn(const QMetaObject::Connection &conn);
    void selectIndex(int index) const;

    void connectLocThreadMgr() const;
    void disconnectLocThreadMgr() const;

    void checkDescAtLastIndex();
    void goBack();
    void goToNav();
    void goToFAI();

public slots:
    void choicePressed(const QModelIndex &index);
    void nameChanged(QString name);
    void rowChanged(const QModelIndex &current, const QModelIndex &previous);

    void currentLocationSet(const QString &name);
    void currentLocationCleared();

    void gpioPressed(int btn);
    void gpioLongPressed(int btn);

private:

    int caca;
    Ui::MenuDialog *ui;

    MainMenu rootMenu;
    TaskNavigationMenu rootMenuTaskNavigation;
    MenuListModel *crtMenu;
    MenuListModel *crtMenuNav;

    QMetaObject::Connection rowChangeConn;
    QMetaObject::Connection chooserConn;
    QMetaObject::Connection dlgRowChangeConn;



    void setupUI();

    void showEvent(QShowEvent *e);

    void hideEvent(QHideEvent *e);

    void checkDescAtIndex(int index);
};

#endif // MENUDIALOG_H
