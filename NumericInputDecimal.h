/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NUMERICINPUTDECIMAL_H
#define NUMERICINPUTDECIMAL_H

#include <QDialog>

namespace Ui {
class NumericInputDecimal;
}

class NumericInputDecimal : public QDialog
{
    Q_OBJECT

public:
    explicit NumericInputDecimal(QWidget *parent = 0);
    ~NumericInputDecimal();

    int value() const;
    void setValue(int value);

    int max() const;
    void setMax(int max);

    void setCaption(QString caption);

    float displayDecimals() const;
    void setDisplayDecimals(float displayDecimals);

signals:
    void valueChanged(int value);

public slots:
    void gpioPressed(int btn);
    void gpioLongPressed(int btn);

private:
    Ui::NumericInputDecimal *ui;

    void showEvent(QShowEvent *e) Q_DECL_OVERRIDE;
    void hideEvent(QHideEvent *e) Q_DECL_OVERRIDE;

    int value_;
    int step_;
    int max_;
    int min_;
    float displayDecimals_;

};

#endif // NUMERICINPUTDECIMAL_H
