/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "NumericInput.h"
#include "ui_NumericInput.h"
#include "VarioQt.h"
#include "mainui.h"
#include "utils/vario_common.h"

NumericInput::NumericInput(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NumericInput)
{
    ui->setupUi(this);

    value_ = 0;
    step_ = 1;

    max_ = INT_MAX;
    min_ = INT_MIN;

    ui->valueBox->setValue(QString::number((float)value_*displayScale_,'f',precision));

    displayScale_ = 1;

    connect(VarioQt::instance().getMainUI(),SIGNAL(mainGpioPressed(int)),this,SLOT(gpioPressed(int)));
    connect(VarioQt::instance().getMainUI(),SIGNAL(mainGpioLongPressed(int)),this,SLOT(gpioLongPressed(int)));

    move(240/2 - width()/2,320/2 - height()/2);
}

NumericInput::~NumericInput()
{
    if(isVisible())hide();
    delete ui;
}

void NumericInput::showEvent(QShowEvent *e)
{
    QWidget::showEvent(e);
    VarioQt::instance().pushNewWidget(this);
}

void NumericInput::hideEvent(QHideEvent *e)
{
    QWidget::hideEvent(e);
    VarioQt::instance().deleteLastWidget();
}

void NumericInput::setPrecision(int value)
{
    precision = value;
    ui->valueBox->setValue(QString::number((float)value_*displayScale_,'f',precision));
}

float NumericInput::displayScale() const
{
    return displayScale_;
}

void NumericInput::setDisplayScale(float displayScale)
{
    displayScale_ = displayScale;
}

void NumericInput::setCaption(QString caption)
{
    ui->valueBox->setCaption(caption);
    update();
}




void NumericInput::gpioPressed(int btn)
{
    if(btn == Vario::GPIO1)
    {
        value_+=step_;
        if(value_>max_)value_=max_;
        setValue(value_);
        emit valueChanged(value_);
    }
    if(btn == Vario::GPIO2)
    {
        value_ -= step_;
        if(value_ < min_)value_ = min_;
        setValue(value_);
        emit valueChanged(value_);
    }
    if(btn == Vario::GPIO3)
    {
        close();
    }
}

void NumericInput::gpioLongPressed(int btn)
{

}

void NumericInput::setMin(int min)
{
    min_ = min;
}

int NumericInput::max() const
{
    return max_;
}

void NumericInput::setMax(int max)
{
    max_ = max;
}

int NumericInput::step() const
{
    return step_;
}

void NumericInput::setStep(float step)
{
    step_ = step;
    //round the value to the sett stepp
    value_ = round(value_/step_) * step_;
}

float NumericInput::value() const
{
    return value_;
}

void NumericInput::setValue(float value)
{
    value_ = value;
    ui->valueBox->setValue(QString::number((float)value_*displayScale_,'f',precision));
    repaint();
}
