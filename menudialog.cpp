/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "menudialog.h"
#include "ui_menudialog.h"

#include <QtDebug>
#include <QLayout>

#include <iostream>

#include "settings/locationmanager.h"
#include "settings/namemanager.h"
#include "utils/qfonts.h"
#include "mainui.h"
#include "threads/fb2spithread.h"
#include "Geo/locationsthreadmgr.h"
#include "VarioQt.h"
#include "menu/task/faiNavigationMenu.h"

using Vario::getFont;

MenuDialog::MenuDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MenuDialog),
    rootMenu(*this ,nullptr, this),
    rootMenuTaskNavigation(&rootMenu, this),
    crtMenu(&rootMenu),
    crtMenuNav(&rootMenuTaskNavigation)
{
    ui->setupUi(this);

    setupUI();

    connect(VarioQt::instance().getNameManager(), SIGNAL(nameChange(QString)), this, SLOT(nameChanged(QString)));

    ui->listView->setModel(crtMenu);

    this->ui->listView->setCurrentIndex(crtMenu->index(0, 0));

    rowChangeConn = connect(ui->listView->selectionModel(), &QItemSelectionModel::currentRowChanged, crtMenu, &MenuListModel::rowChanged);
    dlgRowChangeConn = connect(ui->listView->selectionModel(), &QItemSelectionModel::currentRowChanged, this, &MenuDialog::rowChanged);

    connect(this->ui->listView, SIGNAL(activated(QModelIndex)),
            this, SLOT(choicePressed(QModelIndex)));

}

MenuDialog::~MenuDialog()
{
    delete ui;
}

void MenuDialog::resetToRoot() {
    state = 1;
    ui->locWidget->setNoBorders();
    while (crtMenu->parentMenu() != nullptr) {
        goBack();
    }
}

Ui::MenuDialog& MenuDialog::getUi() const
{
    return *ui;
}

void MenuDialog::setChooserConn(const QMetaObject::Connection &conn)
{
    disconnect(chooserConn);
    chooserConn = conn;
}

void MenuDialog::selectIndex(int idx) const
{
    ui->listView->setCurrentIndex(ui->listView->model()->index(idx, 0));
}

void MenuDialog::connectLocThreadMgr() const
{
    connect(VarioQt::instance().getLocationsThreadMgr(), &LocationsThreadMgr::locationChanged, this, &MenuDialog::currentLocationSet);
    connect(VarioQt::instance().getLocationsThreadMgr(), &LocationsThreadMgr::locationCleared, this, &MenuDialog::currentLocationCleared);
}

void MenuDialog::disconnectLocThreadMgr() const
{
    disconnect(VarioQt::instance().getLocationsThreadMgr(), &LocationsThreadMgr::locationChanged, this, &MenuDialog::currentLocationSet);
    disconnect(VarioQt::instance().getLocationsThreadMgr(), &LocationsThreadMgr::locationCleared, this, &MenuDialog::currentLocationCleared);
}


void MenuDialog::showEvent(QShowEvent *e)
{
    QWidget::showEvent(e);
    VarioQt::instance().pushNewWidget(this);
}

void MenuDialog::hideEvent(QHideEvent *e)
{
    QWidget::hideEvent(e);
    VarioQt::instance().deleteLastWidget();
}


GPIOEmulator &MenuDialog::gpioEmulator() const
{
    return static_cast<MainUI*>(parent())->gpioEmulator;
}

void MenuDialog::gpioPressed(int btn) {

    if(!VarioQt::instance().isActiveWidget(this))return;

    int row = ui->listView->currentIndex().row();

    if (state == 1) {

        if (btn == Vario::GPIO2) {
            row++;
            if (row == ui->listView->model()->rowCount())
                row = 0;

            while(crtMenu->typeForIndex(row) == MenuType::Caption)
            {
                row++;
                if (row >= ui->listView->model()->rowCount())
                    row = 0;
            }

            if(row == 1)
                ui->listView->scrollToTop();


            ui->listView->setCurrentIndex(ui->listView->model()->index(row, 0));
        } else if (btn == Vario::GPIO1) {
            row--;

            if (row < 0)
                row = ui->listView->model()->rowCount() - 1;
            while(crtMenu->typeForIndex(row) == MenuType::Caption)
            {
                row--;
                if (row < 0)
                    row = ui->listView->model()->rowCount() - 1;
            }

            if(row == 1)
                ui->listView->scrollToTop();

            ui->listView->setCurrentIndex(ui->listView->model()->index(row, 0));
        } else if (btn == Vario::GPIO3) {
            choicePressed(ui->listView->currentIndex());
        }
    } else {
        if (btn == Vario::GPIO2) {
            ui->chooser->previous();
        } else if (btn == Vario::GPIO1) {
            ui->chooser->next();
        } else if (btn == Vario::GPIO3) {
            ui->chooser->choose();
        }
    }

}

void MenuDialog::gpioLongPressed(int btn) {
    if(!VarioQt::instance().isActiveWidget(this))return;
    if (state == 2) {
        if (btn == Vario::GPIO1) {
            disconnect(chooserConn);
            ui->chooser->setVisible(false);
            state = 1;
        }
    } else
    {
        if (btn == Vario::GPIO1) {
            if (crtMenu->parentMenu() != nullptr) {
                goBack();
            }
        }
    }
    if(btn == Vario::GPIO3)
    {
        resetToRoot();
        hide();
    }
}

void MenuDialog::nameChanged(QString name) {
    ui->valName->setText(name);
}

void MenuDialog::rowChanged(const QModelIndex &current, const QModelIndex &previous)
{
    checkDescAtIndex(current.row());
}

void MenuDialog::currentLocationSet(const QString &name)
{
    ui->valLabel->setText(QString(tr("CURRENT: ")).append(name));
    ui->valLabel->setVisible(true);
}

void MenuDialog::currentLocationCleared()
{
    ui->valLabel->setVisible(false);
}

void MenuDialog::checkDescAtLastIndex()
{
    checkDescAtIndex(crtMenu->savedIndex());
}

void MenuDialog::choicePressed(const QModelIndex &index)
{
    int row = index.row();
    MenuType type = crtMenu->typeForIndex(row);

    if (type == MenuType::Choice) {
        crtMenu->execAtIndex(row, *this);
        crtMenu->execAtIndexNC(row, *this);

    } else if (type == MenuType::List) {
        MenuListModel *model = crtMenu->modelForIndex(row);

        if (model != nullptr) {
            crtMenu = model;
            crtMenu->before(*this);
            crtMenu->beforeNC(*this);

            disconnect(rowChangeConn);
            disconnect(dlgRowChangeConn);

            ui->listView->setModel(crtMenu);

            int row = 0;
            while(row != ui->listView->model()->rowCount() && crtMenu->typeForIndex(row) == MenuType::Caption)row++;
            //this->ui->listView->setCurrentIndex(crtMenu->index(row, 0));

            ui->listView->setCurrentIndex(crtMenu->index(row));
            checkDescAtIndex(0);

            rowChangeConn = connect(ui->listView->selectionModel(), &QItemSelectionModel::currentRowChanged, crtMenu, &MenuListModel::rowChanged);
            dlgRowChangeConn = connect(ui->listView->selectionModel(), &QItemSelectionModel::currentRowChanged, this, &MenuDialog::rowChanged);
        }
    } else if (crtMenu->parentMenu() != nullptr) {
        goBack();
    }
}

void MenuDialog::goBack()
{
    MenuListModel *old = crtMenu;
    crtMenu = crtMenu->parentMenu();
    crtMenu->before(*this);
    crtMenu->beforeNC(*this);

    disconnect(rowChangeConn);
    disconnect(dlgRowChangeConn);

    ui->listView->setModel(crtMenu);
    ui->listView->setCurrentIndex(crtMenu->index(crtMenu->savedIndex()));
    checkDescAtIndex(crtMenu->savedIndex());

    rowChangeConn = connect(ui->listView->selectionModel(), &QItemSelectionModel::currentRowChanged, crtMenu, &MenuListModel::rowChanged);
    dlgRowChangeConn = connect(ui->listView->selectionModel(), &QItemSelectionModel::currentRowChanged, this, &MenuDialog::rowChanged);

    //hide menu sliders and labels when we go back a menu
    ui->valLabel->setVisible(false);
    ui->chooser->setVisible(false);
    ui->valName->setVisible(false);

    ui->hSlider->setVisible(false);
    ui->locWidget->setVisible(false);

    delete old;
}

void MenuDialog::goToNav()
{
    MenuListModel *model = new TaskNavigationMenu(crtMenu);

    if (model != nullptr) {
        crtMenu = model;
        crtMenu->before(*this);
        crtMenu->beforeNC(*this);

        disconnect(rowChangeConn);
        disconnect(dlgRowChangeConn);

        ui->listView->setModel(crtMenu);

        int row = 0;
        while(row != ui->listView->model()->rowCount() && crtMenu->typeForIndex(row) == MenuType::Caption)row++;
        //this->ui->listView->setCurrentIndex(crtMenu->index(row, 0));

        ui->listView->setCurrentIndex(crtMenu->index(row));
        checkDescAtIndex(0);

        rowChangeConn = connect(ui->listView->selectionModel(), &QItemSelectionModel::currentRowChanged, crtMenu, &MenuListModel::rowChanged);
        dlgRowChangeConn = connect(ui->listView->selectionModel(), &QItemSelectionModel::currentRowChanged, this, &MenuDialog::rowChanged);
    }
}

void MenuDialog::goToFAI()
{
    MenuListModel *model = new FAINavigationMenu(crtMenu);

    if (model != nullptr) {
        crtMenu = model;
        crtMenu->before(*this);
        crtMenu->beforeNC(*this);

        disconnect(rowChangeConn);
        disconnect(dlgRowChangeConn);

        ui->listView->setModel(crtMenu);

        int row = 0;
        while(row != ui->listView->model()->rowCount() && crtMenu->typeForIndex(row) == MenuType::Caption)row++;
        //this->ui->listView->setCurrentIndex(crtMenu->index(row, 0));

        ui->listView->setCurrentIndex(crtMenu->index(row));
        checkDescAtIndex(0);

        rowChangeConn = connect(ui->listView->selectionModel(), &QItemSelectionModel::currentRowChanged, crtMenu, &MenuListModel::rowChanged);
        dlgRowChangeConn = connect(ui->listView->selectionModel(), &QItemSelectionModel::currentRowChanged, this, &MenuDialog::rowChanged);
    }
}

void MenuDialog::setupUI()
{
    this->setFixedSize(180, 300);

    ui->valLabel->setVisible(false);
    ui->chooser->setVisible(false);
    ui->valName->setVisible(false);

    ui->hSlider->setVisible(false);
    ui->locWidget->setVisible(false);
    //ui->descTextEdit->setVisible(false);


    ui->listView->setFont(getFont(VarioQt::instance().getMnuFontName(),VarioQt::instance().getMnuFontSize()));
    ui->valLabel->setFont(getFont(VarioQt::instance().getMnuFontName(),VarioQt::instance().getMnuFontSize()));
    ui->valName->setFont(getFont(VarioQt::instance().getMnuFontName(),VarioQt::instance().getMnuFontSize()));
    ui->descTextEdit->setFont(getFont(VarioQt::instance().getMnuFontName(),VarioQt::instance().getMnuFontSize()));
}

void MenuDialog::checkDescAtIndex(int index)
{
    QString desc = crtMenu->descAtIndex(index);

    if (!desc.isEmpty()) {
        ui->descTextEdit->setPlainText(desc);
        ui->descTextEdit->setVisible(true);
    } else {
        //ui->descTextEdit->setVisible(false);
        ui->descTextEdit->setPlainText("");
    }
}
