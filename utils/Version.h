/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VERSION_H
#define VERSION_H
#include <QString>


class Version
{
    static QString OSVersionPath;
    static QString firmwareVersion;
    static QString firmwareType;

public:
    Version();
    static QString VarioQTVersion();
    static QString OSVersion();
    static QString getFirmwareVersion();
    static void setFirmwareVersion(const QString &value);
    static QString getFirmwareType();
    static void setFirmwareType(const QString &value);
};

#endif // VERSION_H
