/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FONTTABLES_H
#define FONTTABLES_H

extern const unsigned char fontArial14h_data_table[];
extern const unsigned int fontArial14h_offset_table[];
extern const unsigned char fontArial14h_width_table[];
extern const unsigned char fontArial14h_index_table[];

extern const unsigned char fontArial15h_data_table[];
extern const unsigned int fontArial15h_offset_table[];
extern const unsigned char fontArial15h_width_table[];
extern const unsigned char fontArial15h_index_table[];

extern const unsigned char fontArial16h_data_table[];
extern const unsigned int fontArial16h_offset_table[];
extern const unsigned char fontArial16h_width_table[];
extern const unsigned char fontArial16h_index_table[];

extern const unsigned char fontArial18h_data_table[];
extern const unsigned int fontArial18h_offset_table[];
extern const unsigned char fontArial18h_width_table[];
extern const unsigned char fontArial18h_index_table[];

extern const unsigned char fontArial19h_data_table[];
extern const unsigned int fontArial19h_offset_table[];
extern const unsigned char fontArial19h_width_table[];
extern const unsigned char fontArial19h_index_table[];

extern const unsigned char fontArial22h_data_table[];
extern const unsigned int fontArial22h_offset_table[];
extern const unsigned char fontArial22h_width_table[];
extern const unsigned char fontArial22h_index_table[];

extern const unsigned char fontArial24h_data_table[];
extern const unsigned int fontArial24h_offset_table[];
extern const unsigned char fontArial24h_width_table[];
extern const unsigned char fontArial24h_index_table[];

extern const unsigned char fontArial29h_data_table[];
extern const unsigned int fontArial29h_offset_table[];
extern const unsigned char fontArial29h_width_table[];
extern const unsigned char fontArial29h_index_table[];

extern const unsigned char fontArial32h_data_table[];
extern const unsigned int fontArial32h_offset_table[];
extern const unsigned char fontArial32h_width_table[];
extern const unsigned char fontArial32h_index_table[];

extern const unsigned char fontArial34h_data_table[];
extern const unsigned int fontArial34h_offset_table[];
extern const unsigned char fontArial34h_width_table[];
extern const unsigned char fontArial34h_index_table[];

extern const unsigned char fontArial37h_data_table[];
extern const unsigned int fontArial37h_offset_table[];
extern const unsigned char fontArial37h_width_table[];
extern const unsigned char fontArial37h_index_table[];

#endif // FONTTABLES_H
