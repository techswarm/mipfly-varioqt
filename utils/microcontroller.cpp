/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "microcontroller.h"

#include <QThread>
#include <sstream>

#include "serial/serialconnection.h"

Microcontroller::Microcontroller(const SerialConnection &conn)
    : serial(conn)
{

}


void Microcontroller::setNumSample(int num) const
{
    STOM(1);
    QThread::usleep(20000);
    STNUMS(num);
    STOM(0);
}

void Microcontroller::setTasCompensation(int num) const
{
    STOM(1);
    QThread::usleep(20000);
    STTC(num);
    STOM(0);
}

void Microcontroller::reqId() const
{
    STOM(1);
    QThread::usleep(20000);
    STID(1);
    STOM(0);
}

void Microcontroller::STOM(int val) const
{
    std::ostringstream ss;
    ss << "$STOM," << val << '\r';
    std::string str = ss.str();
    serial.puts(str);
}

void Microcontroller::STNUMS(int val) const
{
    std::ostringstream ss;
    ss << "$STNUMS," << val << '\r';

    std::string str = ss.str();
    serial.puts(str);
}

void Microcontroller::STTC(int val) const
{
    std::ostringstream ss;
    ss << "$STTC," << val << '\r';

    std::string str = ss.str();
    serial.puts(str);
}

void Microcontroller::STID(int val) const
{
    std::ostringstream ss;
    ss << "$STID," << val << '\r';
    std::string str = ss.str();
    serial.puts(str);
}
