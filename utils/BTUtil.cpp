/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "BTUtil.h"
#include "QProcess"
#include <QDebug>
#include <QThread>
#include <qbluetoothaddress.h>
#include <qbluetoothdevicediscoveryagent.h>
#include <qbluetoothlocaldevice.h>

BTUtil::BTUtil()
{

}

bool BTUtil::hciDeviceIsAvailable()
{
    QProcess pingProcess;
    QString exe = "hcitool dev";
    pingProcess.start(exe);
    pingProcess.waitForFinished();
    QString output(pingProcess.readAll());
    if (output.length()<10)return false;
    return true;
}

bool BTUtil::performBTConnect(QString address)
{
#ifdef __ARMEL__
    system(qPrintable("/mnt/mipfly/BTConnect.sh "+address +" &"));
#else
    qDebug()<<"performBTConnect not implemented on PC simulation";
#endif
    return true;
}

void BTUtil::connectBnep(QString address)
{
    /*
    QProcess connectProcess;
    QString cmd = "python2 /usr/share/doc/bluez-test-scripts/examples/test-network-mipfly ";
    cmd += address;
    cmd += " nap";
    connectProcess.start(cmd);
    connectProcess.waitForFinished();
    cmd = "ip link show";
    QThread::msleep(5000);
    connectProcess.start(cmd);
    connectProcess.waitForFinished();
    QString output(connectProcess.readAll());
    qDebug()<<"ip link show:\r\n"<<output;
    if(output.contains("bnep0"))
        system("dhclient bnep0");
    */
    QString command = "nmcli connection show";
    QProcess nmcliProcess;
    nmcliProcess.start(command);
    nmcliProcess.waitForFinished();
    QString output(nmcliProcess.readAll());
    QStringList respLines = output.split("\n");
    int nameEndIndex = output.indexOf("UUID");
    foreach (QString line, respLines) {
        if(line.contains("bluetooth"))
        {
            QString connName = line.left(nameEndIndex);
            connName = connName.simplified();
            command = "nmcli connection up \"" + connName + "\"";
            nmcliProcess.execute(command);
            nmcliProcess.waitForFinished();
            output = QString(nmcliProcess.readAll());
            if(output.contains("Connection successfully activated"))return;
        }
    }
    qDebug()<<output;
}

void BTUtil::removeBTEntry(QString address)
{
    QString command = "echo \"remove ";
    command += address;
    command += "\nquit\" | bluetoothctl";
    qDebug()<<command;
    system(command.toStdString().c_str());
}

bool BTUtil::bnep0Exists()
{
    QProcess pingProcess;
    QString exe = "ifconfig";
    pingProcess.start(exe);
    pingProcess.waitForFinished();
    QString output(pingProcess.readAll());
    if(output.contains("bnep0"))return true;
    return false;
}
