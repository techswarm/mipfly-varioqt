/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "datetimemanager.h"

#include "NMEA/NMEAParser.h"

#include <iostream>
#include <cstdio>
#include "settings/tzmanager.h"
#include <QDebug>

void setTimeFromGPS(NmeaParser &gpsParser, int year, int month, int day, int hour, int min, int sec)
{
    //gpsParser.setTimeFromGPS = nullptr;

    //increment with 1 s to align times
    //settime will take care of the 60S case
    sec++;

    char buf[200];

    //qDebug()<<"GPS H:"<<hour;


    //hour += tzm.getTimezone();

    if(year != 0)
        sprintf(buf, "date -s \'%d-%d-%dT%d:%d:%dZ \'", year, month, day, hour, min, sec);
    else
        sprintf(buf, "date -s \'%d:%d:%dZ \'", hour, min, sec);

    //strptime(buf, "%D %T", &tm_struct);
    qDebug()<<buf;
#ifdef __ARMEL__
    TzManager tzm;
    system(buf);
    tzm.setActTZ();
#endif

    std::cout << "Time set from string: " << buf << std::endl;
}
