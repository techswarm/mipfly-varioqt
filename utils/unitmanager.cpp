/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "unitmanager.h"
#include <QSettings>
#include "unitconverter.h"

//********************
//SISpeed is considered kmh
//********************
QString SISpeed::prefferedUnit()
{
    QSettings settings;
    return settings.value("SISpeed","kmh").toString();
}

void SISpeed::setPrefferedUnit(QString unit)
{
    QSettings settings;
    settings.setValue("SISpeed",unit);
}

QStringList SISpeed::unitList()
{
    QStringList list;
    list.append("kmh");
    list.append("mph");
    list.append("kts");
    return list;
}

SISpeed::SISpeed(float SIUnit)
{
    valueSI = SIUnit;
}

void SISpeed::setValue(float SIUnit)
{
    valueSI = SIUnit;
}

void SISpeed::setValuePrefferedFormat(float value)
{
    if(getPrefferedFormat() == "kmh")
    {
        valueSI = value;
    }
    if(getPrefferedFormat() == "mph")
    {
        valueSI = UnitConverter::MiPerHourTokmPerH(value);
    }
    if(getPrefferedFormat() == "kts")
    {
        valueSI = UnitConverter::KnotsTokmPerH(value);
    }
}

float SISpeed::getValuePrefferedFormat()
{
    if(getPrefferedFormat() == "kmh")
    {
        return valueSI;
    }
    if(getPrefferedFormat() == "mph")
    {
        return UnitConverter::kmPerHToMiPerHour(valueSI);
    }
    if(getPrefferedFormat() == "kts")
    {
        return UnitConverter::kmPerHToKnots(valueSI);
    }
    return 0;
}

QString SISpeed::getPrefferedFormat()
{
    return SISpeed::prefferedUnit();
}

float SISpeed::value()
{
    return valueSI;
}

//********************
//SIVerticalSpeed is considered mps
//********************

QString SIVerticalSpeed::prefferedUnit()
{
    QSettings settings;
    return settings.value("SIVerticalSpeed","mps").toString();
}

void SIVerticalSpeed::setPrefferedUnit(QString unit)
{
    QSettings settings;
    settings.setValue("SIVerticalSpeed",unit);
}

QStringList SIVerticalSpeed::unitList()
{
    QStringList list;
    list.append("mps");
    list.append("ftpm");
    return list;
}

SIVerticalSpeed::SIVerticalSpeed(float SIUnit)
{
    valueSI = SIUnit;
}

void SIVerticalSpeed::setValue(float SIUnit)
{
    valueSI = SIUnit;
}

void SIVerticalSpeed::setValuePrefferedFormat(float value)
{
    if(getPrefferedFormat() == "mps")
    {
        valueSI = value;
    }
    if(getPrefferedFormat() == "ftpm")
    {
        valueSI = UnitConverter::ftPerMinTomPerSec(value);
    }
}

float SIVerticalSpeed::getValuePrefferedFormat()
{
    if(getPrefferedFormat() == "mps")
    {
        return valueSI;
    }
    if(getPrefferedFormat() == "ftpm")
    {
        return UnitConverter::mPerSecToFtPerMin(valueSI);
    }
    return 0;
}

QString SIVerticalSpeed::getPrefferedFormat()
{
    return SIVerticalSpeed::prefferedUnit();
}

float SIVerticalSpeed::value()
{
    return valueSI;
}


//********************
//SIHeight is considered m
//********************
QString SIHeight::prefferedUnit()
{
    QSettings settings;
    return settings.value("SIHeight","m").toString();
}

void SIHeight::setPrefferedUnit(QString unit)
{
    QSettings settings;
    settings.setValue("SIHeight",unit);
}

QStringList SIHeight::unitList()
{
    QStringList list;
    list.append("m");
    list.append("ft");
    return list;
}

SIHeight::SIHeight(float SIUnit)
{
    valueSI = SIUnit;
}

void SIHeight::setValue(float SIUnit)
{
    valueSI = SIUnit;
}

void SIHeight::setValuePrefferedFormat(float value)
{
    if(getPrefferedFormat() == "m")
    {
        valueSI = value;
    }
    if(getPrefferedFormat() == "ft")
    {
        valueSI = UnitConverter::ftToM(value);
    }
}

float SIHeight::getValuePrefferedFormat()
{
    if(getPrefferedFormat() == "m")
    {
        return valueSI;
    }
    if(getPrefferedFormat() == "ft")
    {
        return UnitConverter::mToFt(valueSI);
    }
    return 0;
}

QString SIHeight::getPrefferedFormat()
{
    return SIHeight::prefferedUnit();
}

float SIHeight::value()
{
    return valueSI;
}

//********************
//SIDistance is considered km
//********************


QString SIDistance::prefferedUnit()
{
    QSettings settings;
    return settings.value("SIDistanceKm","km").toString();
}

void SIDistance::setPrefferedUnit(QString unit)
{
    QSettings settings;
    settings.setValue("SIDistanceKm",unit);
}

QStringList SIDistance::unitList()
{
    QStringList list;
    list.append("km");
    list.append("mi");
    return list;
}

SIDistance::SIDistance(float SIUnit)
{
    valueSI = SIUnit;
}

void SIDistance::setValue(float SIUnit)
{
    valueSI = SIUnit;
}

void SIDistance::setValuePrefferedFormat(float value)
{
    if(getPrefferedFormat() == "km")
    {
        valueSI = value;
    }
    if(getPrefferedFormat() == "mi")
    {
        valueSI = UnitConverter::MiTokm(value);
    }
}

float SIDistance::getValuePrefferedFormat()
{
    if(getPrefferedFormat() == "km")
    {
        return valueSI;
    }
    if(getPrefferedFormat() == "mi")
    {
        return UnitConverter::kmToMi(valueSI);
    }
    return 0;
}

QString SIDistance::getPrefferedFormat()
{
    return SIDistance::prefferedUnit();
}

float SIDistance::value()
{
    return valueSI;
}
