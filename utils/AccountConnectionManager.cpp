/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "AccountConnectionManager.h"
#include <QtNetwork>
#include <QString>
#include <utils/vario_common.h>

AccountConnectionManager::AccountConnectionManager(QObject *parent) : QObject(parent)
{
    manager = new QNetworkAccessManager();
    connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(managerFinished(QNetworkReply*)),Qt::UniqueConnection);
}

void AccountConnectionManager::connectAccount(int pin)
{
    QString url = Vario::host+"pairconnect?pairCode=";
    url+=QString::number(pin);
    qDebug()<<url;
    request.setUrl(QUrl(url));
    manager->get(request);
}

void AccountConnectionManager::managerFinished(QNetworkReply *reply)
{
    if (reply->error()) {
        emit pairFinished(false);
        qDebug() << reply->errorString();
        return;
    }

    QString answer = reply->readLine();
    answer = answer.remove('\n');
    if(answer != "ok")
    {
        emit pairFinished(false);
    }
    else
    {
        QString key = reply->readLine();
        key = key.remove('\n');
        QString id = reply->readLine();
        QSettings settings;
        settings.setValue("accountKey",key);
        settings.setValue("accountId",id);
        settings.sync();
        emit pairFinished(true);
    }
}

