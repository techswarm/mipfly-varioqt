/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VARIOPOWER_H
#define VARIOPOWER_H

#include <cstdint>
#include <string>

class varioPower
{
public:
    const int32_t voltageFull = 4050000;
    const int32_t voltageEmpty = 3150000;
    const int32_t chargeTotal = 5500000;

    static bool chargeReseted;
    varioPower();
    ~varioPower();
    int16_t GetBatteryCapacityPercent();
    int32_t GetBatteryVoltageNow();
    std::string GetBatteryStatus();
    int32_t GetBatteryChargeNow();
    int32_t GetBatteryAmperageNow();
    void ResetBatteryCharge();

};

#endif // VARIOPOWER_H
