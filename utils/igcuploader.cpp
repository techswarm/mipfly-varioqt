/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "igcuploader.h"
#include "utils/netutils.h"
#include "utils/vario_common.h"

IGCUploader::IGCUploader(QObject *parent) : QObject(parent)
{
    connect(&internetExistsWtcher,SIGNAL(finished()),this,SLOT(internetExistsFinished()));
    connect(&igcUploadWatcher,SIGNAL(finished()),this,SLOT(IGCUploadFinished()));
    manager = new QNetworkAccessManager();
    connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(managerFinished(QNetworkReply*)),Qt::UniqueConnection);
}

IGCUploader::IGCUploader(QString filename, QString userId, QString userKey, QObject *parent)
{
    setFilename(filename);
    setUserId(userId);
    setUserKey(userKey);
    connect(&internetExistsWtcher,SIGNAL(finished()),this,SLOT(internetExistsFinished()));
    connect(&igcUploadWatcher,SIGNAL(finished()),this,SLOT(IGCUploadFinished()));
    manager = new QNetworkAccessManager();
    connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(managerFinished(QNetworkReply*)),Qt::UniqueConnection);
}

void IGCUploader::setFilename(const QString &value)
{
    filename = value;
}

void IGCUploader::setUserId(const QString &value)
{
    userId = value;
}

void IGCUploader::setUserKey(const QString &value)
{
    userKey = value;
}

void IGCUploader::upload()
{
    if(!uploadInProgress)
    {
        internetExistsFuture = QtConcurrent::run(NetUtils::internetExists);
        internetExistsWtcher.setFuture(internetExistsFuture);
        uploadInProgress = true;
    }
    else
    {
        emit message(tr("In progress!"),tr("ERROR"));
    }
}

void IGCUploader::checkUpload()
{
    QFile file(filename);
    if(!file.exists())
    {
        return;
    }

    qint64 kSize = file.size()/1024;
    float sizeM = (float)kSize/1024;
    QString sizeStr = QString::number(sizeM,'f',2);

    QString url = Vario::host+"validateIgc?id=";
    url+=userId;
    url+="&name="+QFileInfo(file).fileName();
    url+="&key="+userKey;
    url+="&dimension="+sizeStr;


    qDebug()<<url;
    request.setUrl(QUrl(url));
    manager->get(request);
}


void IGCUploader::managerFinished(QNetworkReply *reply)
{
    if (reply->error()) {
        qDebug() << reply->errorString();
        emit message(tr("Fail 8"),tr("ERROR"));
        uploadInProgress = false;
        return;
    }
    QString replyStr = reply->readLine();
    qDebug()<<replyStr;
    int resultCode = replyStr.toInt();

    if(resultCode == 1)
    {
        emit message(tr("Fail 7"),tr("ERROR"));
        uploadInProgress = false;
        return;
    }
    if(resultCode == 2)
    {
        emit message(tr("Not paired"),tr("ERROR"));
        uploadInProgress = false;
        return;
    }
    if(resultCode == 3)
    {
        emit message(tr("File exists"),tr("ERROR"));
        uploadInProgress = false;
        return;
    }
    if(resultCode == 4)
    {
        emit message(tr("No space"),tr("ERROR"));
        uploadInProgress = false;
        return;
    }

    if(resultCode == 0)
    {
        //if all is ok
        QFile file(filename);
        if(!file.exists())
        {
            return;
        }
        QString url = Vario::host+"uploadIgc";

        igcUploadFuture = QtConcurrent::run(NetUtils::uploadFile,url,filename,userId,userKey);
        igcUploadWatcher.setFuture(igcUploadFuture);
    }
}


void IGCUploader::internetExistsFinished()
{
    if(internetExistsFuture.result() == true)
        checkUpload();
    else
    {
        emit message(tr("No Internet"),tr("ERROR"));
        uploadInProgress = false;
    }
}

void IGCUploader::IGCUploadFinished()
{
    emit message(tr("File Sent"),tr("INFO"));
    uploadInProgress = false;
}
