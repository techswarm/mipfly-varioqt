/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NETSTATS_H
#define NETSTATS_H

#include <QObject>
#include <QTimer>
#include <QFuture>
#include <QFutureWatcher>

class NetStats : public QObject
{
    Q_OBJECT
public:
    enum connectionStatus{NoConnection, BNEPConnected, InternetConnected};
    NetStats();
    void resetTimeout();
    void checkStatus();
private:
    QTimer tick;
    int timeout = 0;
    QFuture<bool> internetExistsFuture;
    QFutureWatcher<bool> internetExistsWtcher;
    connectionStatus stats = NoConnection;

private slots:
    void timerTick();
    void internetExistsFinished();

signals:
    void statusChanged(connectionStatus status);

};

#endif // NETSTATS_H
