/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "cstgraphics.h"

#include <QPainter>
#include <QDebug>

#include "fonttables.h"

static const int ARIAL = 1;

static const unsigned char* data_table;
static const unsigned int* offset_table;
static const unsigned char* width_table;
static const unsigned char* index_table;

static unsigned int step;

int str_width(unsigned char* s, int height, const int font);
void prepareOffsets(int height, const int font);

#include <iostream>

void Vario::drawStr(int x, int y, const QString &str, int height, QPainter &painter, Qt::Alignment pan, int drawLimit) {

    QByteArray ba;
    ba = str.toLocal8Bit();

    unsigned char *st = (unsigned char*)(ba.data());
    unsigned char *s = st;

    int determinedLength = str_width(s, height, ARIAL);


    if(drawLimit != 0 && determinedLength >= drawLimit)
        return;

    switch(pan) {
    case Qt::AlignLeft:
        // do nothing
        break;
    case Qt::AlignRight:
        x -= determinedLength;
        break;
    case Qt::AlignHCenter:
        x -= determinedLength / 2;
        break;
    }

    while(*s) {
        unsigned char index = index_table[*s];
//		unsigned char index = index_table[0xb0];
        unsigned int offset = offset_table[index];
        unsigned char size = width_table[index];
        const unsigned char* pData = &data_table[offset];

        int xx, yy;
        for(yy = 0; yy < height; yy++) {
            for(xx = 0; xx < size; xx++) {
                if(((*(pData + yy * ((size-1) / 8 + 1) + (xx / 8))) >> 7 - (xx % 8)) & 0b00000001) {
//                    draw_pixel(x + xx, y + yy, color);
                    painter.drawPoint(x + xx, y + yy);
                }
            }
        }
        x += step + size;
        s++;
    }
    //qDebug()<<(QString)st;
}

void Vario::drawStrWithCursor(int x, int y, const QString &str, int height, QPainter &painter, Qt::Alignment pan, int cursorPos)
{
    QByteArray ba;
    ba = str.toLocal8Bit();

    unsigned char *st = (unsigned char*)(ba.data());
    unsigned char *s = st;

    int determinedLength = str_width(s, height, ARIAL);

    switch(pan) {
    case Qt::AlignLeft:
        // do nothing
        break;
    case Qt::AlignRight:
        x -= determinedLength;
        break;
    case Qt::AlignHCenter:
        x -= determinedLength / 2;
        break;
    }

    int charPos = 0;

    while(*s) {
        unsigned char index = index_table[*s];
        unsigned int offset = offset_table[index];
        unsigned char size = width_table[index];
        const unsigned char* pData = &data_table[offset];

        int xx, yy;
        for(yy = 0; yy < height; yy++) {
            for(xx = 0; xx < size; xx++) {
                if(((*(pData + yy * ((size-1) / 8 + 1) + (xx / 8))) >> 7 - (xx % 8)) & 0b00000001) {
//                    draw_pixel(x + xx, y + yy, color);
                    painter.drawPoint(x + xx, y + yy);
                }
            }
        }
        if(charPos == cursorPos)
        {
            unsigned char index = index_table['_'];
            unsigned int offset = offset_table[index];
            unsigned char size = width_table[index];
            const unsigned char* pData = &data_table[offset];

            int xx, yy;
            for(yy = 0; yy < height; yy++) {
                for(xx = 0; xx < size; xx++) {
                    if(((*(pData + yy * ((size-1) / 8 + 1) + (xx / 8))) >> 7 - (xx % 8)) & 0b00000001) {
    //                    draw_pixel(x + xx, y + yy, color);
                        painter.drawPoint(x + xx, y + yy);
                    }
                }
            }
        }

        charPos++;
        x += step + size;
        s++;
    }
}

int Vario::strWidth(QString &str, int height)
{
    QByteArray ba;
    ba = str.toLocal8Bit();
    return str_width((unsigned char*)(ba.data()),height, ARIAL);
}

int str_width(unsigned char *s, int height, const int font)
{
    prepareOffsets(height, font);

    unsigned char* str = s;
    int determinedLength = 0;
    while(*str) {
        unsigned char index = index_table[*str];
        //unsigned int offset = offset_table[index];
        unsigned char size = width_table[index];
        //const unsigned char* pData = &data_table[offset];

        determinedLength += step + size;
        str++;
    }

    return determinedLength;
}

void prepareOffsets(int height, const int font)
{
    switch(font) {
    case ARIAL:
        switch(height) {
        case 14:
            data_table = fontArial14h_data_table;
            offset_table = fontArial14h_offset_table;
            width_table = fontArial14h_width_table;
            index_table = fontArial14h_index_table;
//            height = 14;
            step = 1;
            break;
        case 15:
            data_table = fontArial15h_data_table;
            offset_table = fontArial15h_offset_table;
            width_table = fontArial15h_width_table;
            index_table = fontArial15h_index_table;
//            height = 15;
            step = 1;
            break;
        case 16:
            data_table = fontArial16h_data_table;
            offset_table = fontArial16h_offset_table;
            width_table = fontArial16h_width_table;
            index_table = fontArial16h_index_table;
//            height = 16;
            step = 1;
            break;
        case 18:
            data_table = fontArial18h_data_table;
            offset_table = fontArial18h_offset_table;
            width_table = fontArial18h_width_table;
            index_table = fontArial18h_index_table;
//            height = 18;
            step = 1;
            break;
        case 19:
            data_table = fontArial19h_data_table;
            offset_table = fontArial19h_offset_table;
            width_table = fontArial19h_width_table;
            index_table = fontArial19h_index_table;
//            height = 19;
            step = 1;
            break;
        case 22:
            data_table = fontArial22h_data_table;
            offset_table = fontArial22h_offset_table;
            width_table = fontArial22h_width_table;
            index_table = fontArial22h_index_table;
//            height = 22;
            step = 1;
            break;
        case 24:
            data_table = fontArial24h_data_table;
            offset_table = fontArial24h_offset_table;
            width_table = fontArial24h_width_table;
            index_table = fontArial24h_index_table;
//            height = 24;
            step = 1;
            break;
        case 29:
            data_table = fontArial29h_data_table;
            offset_table = fontArial29h_offset_table;
            width_table = fontArial29h_width_table;
            index_table = fontArial29h_index_table;
//            height = 29;
            step = 1;
            break;
        case 32:
            data_table = fontArial32h_data_table;
            offset_table = fontArial32h_offset_table;
            width_table = fontArial32h_width_table;
            index_table = fontArial32h_index_table;
//            height = 32;
            step = 1;
            break;
        case 34:
            data_table = fontArial34h_data_table;
            offset_table = fontArial34h_offset_table;
            width_table = fontArial34h_width_table;
            index_table = fontArial34h_index_table;
//            height = 34;
            step = 1;
            break;
        case 37:
            data_table = fontArial37h_data_table;
            offset_table = fontArial37h_offset_table;
            width_table = fontArial37h_width_table;
            index_table = fontArial37h_index_table;
//            height = 37;
            step = 1;
            break;
        default:
            data_table = fontArial14h_index_table;
            offset_table = fontArial14h_offset_table;
            width_table = fontArial14h_width_table;
            index_table = fontArial14h_index_table;
//            height = 14;
            step = 1;
            break;
        }
        break;
    default:
        data_table = fontArial14h_index_table;
        offset_table = fontArial14h_offset_table;
        width_table = fontArial14h_width_table;
        index_table = fontArial14h_index_table;
        height = 14;
        step = 1;
        break;
    }
}
