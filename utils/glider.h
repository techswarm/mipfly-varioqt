/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GLIDER_H
#define GLIDER_H

#include <QObject>
#include "navUtils/coord.hpp"

class Glider : public QObject
{
    Q_OBJECT
public:
    explicit Glider(QObject *parent = nullptr);
    ~Glider();

    float getGlide() const;
    void setGlide(float value);

    float getTrimSpeed() const;
    void setTrimSpeed(float value);

    int getHeight() const;

    float getHeading() const;

    float getGroundSpeed() const;

    Coord getPosition() const;

signals:

public slots:
    void setHeight(int hgt);
    void setHeading(qreal hdg);
    void setGroundSpeed(qreal gndSpeed);
    void setPosition(Coord pos);

private:
    float glide = 9;
    float trimSpeed = 40;
    int height;
    float heading;
    float groundSpeed = 0;
    Coord position;


};

#endif // GLIDER_H
