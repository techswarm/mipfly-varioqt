/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef UPDATEMANAGER_H
#define UPDATEMANAGER_H

#include <QObject>
#include <QString>
#include "QFuture"
#include <QtCore>
#include <QtConcurrent/QtConcurrent>

class UpdateManager : public QObject
{
    Q_OBJECT
public:
    //const QString
    UpdateManager();
    void checkConnection();
    void grabUpdateFiles();
    void grabUiUpdateFiles();
    void grabMixerUpdateFiles();
    void grabOsUpdateFiles();
    void deploy();
    void deployOs();
    void deployUi();
    void deployMixer();

    void setUserId(const QString &value);
    void setUserKey(const QString &value);
    bool getUpdateToBeta() const;
    void setUpdateToBeta(bool value);

signals:
    void connectionStatusResult(bool connected);
    void grabUpdateResult(bool connected);

private:
    bool updateToBeta = false;
    QFuture<bool> internetExistsFuture;
    QFutureWatcher<bool> internetExistsWtcher;

    QFuture<void> grabUpdateFuture;
    QFutureWatcher<void> grabUpdateWatcher;

    QString userId;
    QString userKey;

private slots:
    void internetExistsFinished();
    void grabUpdateFinished();

};

#endif // UPDATEMANAGER_H
