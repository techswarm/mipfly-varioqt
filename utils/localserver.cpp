/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "localserver.h"

void LocalServer::setParser(NmeaParser *value)
{
    parser = value;
}

void LocalServer::sendMessage(QByteArray msg)
{
    socket->write(msg);
    socket->flush();
}

LocalServer::LocalServer(QString socketName,QObject *parent) : QObject(parent)
{
    socket = new QLocalSocket(this);
    socket->abort();
    socket->connectToServer(socketName);
    connect(socket,SIGNAL(readyRead()),this,SLOT(dataReceived()));
    qDebug()<<"#######Setup done!";
}

void LocalServer::dataReceived()
{
    while(socket->canReadLine())
    {
        QByteArray buffer = socket->readLine();
        //qDebug()<<buffer;
        if(parser!=nullptr)
        {
            parser->pushLine((char*)(buffer.data()));
        }
    }
}
