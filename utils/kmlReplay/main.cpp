/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QtWidgets>
#include "kmlReplay.h"

int main(int argc, char *argv[])
{
    // Creates an instance of QApplication
    QApplication application(argc, argv);

    // This is our MainWidget class containing our GUI and functionality
    KmlReplay kmlReplay;
    kmlReplay.show(); // Show main window

    // run the application and return execs() return value/code
    return application.exec();
}
