/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef KMLREPLAY_H
#define KMLREPLAY_H

#include <QWidget>
#include <QPushButton>
#include <QTextBrowser>
#include <QGridLayout>
#include <QProcess>
#include <QFileDialog>
#include <QLabel>
#include <QSpinBox>
#include <QTimer>
#include <QUdpSocket>
#include <QComboBox>
#include <QDoubleSpinBox>
#include <QGroupBox>
#include <QLineEdit>


class KmlReplay : public QWidget
{
    Q_OBJECT

public:
    explicit KmlReplay(QWidget *parent = 0); //Constructor
    ~KmlReplay(); // Destructor

private slots:
    void onButtonReleased(); // Handler for button presses
    void motionHandler();

void onPlayButtonReleased();
void onPauseButtonReleased();


private:

   QTimer eventTimer;

   QPushButton* btnLoad_;
   QPushButton* btnPlay_;
   QPushButton* btnPause_;
   QLabel* labelFileName_;
   QSpinBox *spinBoxSatts;
   QDoubleSpinBox  *spinBoxHdop;
   QLabel* labelLongitude_;
   QLabel* labelLatitude_;
   QComboBox* comboBoxGnrmcDataValid_;
   QComboBox* comboBoxPosMode_;
   QComboBox* comboBoxFixStatus_;

   QComboBox* comboBoxDataValid_;

   QDoubleSpinBox  *spinGeoIDSeparation;
   QSpinBox *spinBoxAltOffset;

    QDoubleSpinBox *spinBoxGngsaPdop;
    QDoubleSpinBox *spinBoxGngsaHdop;
    QDoubleSpinBox *spinBoxGngsaVdop;
    QComboBox* comboBoxGngsaPosMode_;
    QComboBox* comboBoxGngsaFixStatus_;
    QLineEdit * lineEdit;

   QList <QString>secList;
   QList <QString>altList;
   QList <QString>posList;
   QString strLongitudeDegMin;
   QString strLatitudeDegMin;
    QUdpSocket *gpsSocket;
    QUdpSocket *varioSocket;


    int intAltOffset;
    QString strGnrmcDataValid;
    QString strGnrmcPositionMode;
    QString strGnggaDataValid;
    QString strGnggaFixStatus;
    QString strGnggaNumSats;
    QString strGnggaHDop;
    QString strGnggaGeoIDSepHeight;
    QString strGngsaPosMode;
    QString strGngsaFixStatus;
    QString strGngsaHDop;
    QString strGngsaVDop;
    QString strGngsaPDop;
    QString strGngsaSats;


   int sampleIndex;
   void handleDatapoint(int idx);
   int numberOfSecounds;
    int numSattelites;

        enum MODE { STOP, PLAY, PAUSE};

    MODE appMode;

    void sendVarioData(QString strVario);
    void sendGpsData(QString strNmea);
    QString composeVarStr(int altitude, QString vario);
    QString composeGnrmc(QString strTime, QString strLati, QString strLatiInd, QString strLong, QString strLongInd, QString strSpeed, QString strCourse, QString strDate);
    QString composeGngga(QString strTime, QString strLati, QString strLatiInd, QString strLong, QString strLongInd, QString strGpsAlti);
    QString composeGngsa();
   int loadKmlFile(QString fileName);
   void intermediatePoint (const double *lat1, const double *lon1, const double *lat2, const double *lon2, const double *fraction, double *latresult, double *lonresult);
   double bearingToNext(double lat1, double lon1, double lat2, double lon2);
   double getDistance(const double* lat1, const double* lon1, const double* lat2, const double* lon2);

};

#endif // KMLREPLAY_H

