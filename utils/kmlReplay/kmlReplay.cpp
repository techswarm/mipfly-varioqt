/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "kmlReplay.h"
#include <algorithm>
#include <fstream>
#include <string>
#include <iostream>
#include <qapplication.h>
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QXmlStreamReader>
#include <QList>
#include <QHostAddress>
#include <QUdpSocket>
#include <math.h>
#include <time.h>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QFileInfo>
#include <QDebug>


#define R2D 57.295779513082320876798154814105  //multiply radian with R2D to get degrees
#define D2R 0.01745329251994329576923690768489 //multiply degrees with D2R to get radians
/*
class QPushButton;
class QTextBrowser;
*/
// Constructor for main window
KmlReplay::KmlReplay(QWidget *parent) :

    QWidget(parent)
{

    QGroupBox *grpKmlFile = new QGroupBox("Kmlfile");
    grpKmlFile->setStyleSheet("QGroupBox {font: bold;border: 1px solid silver;border-radius: 6px;margin-top: 6px;}QGroupBox::title {subcontrol-origin: margin;left: 7px;padding: 0px 5px 0px 5px;}");
    QHBoxLayout *hboxKmlFile = new QHBoxLayout;
    btnLoad_ = new QPushButton(tr("Load KML file"));
    btnPlay_ = new QPushButton(tr("Play"));
    btnPause_ = new QPushButton(tr("Pause"));
    labelFileName_ = new QLabel();

    hboxKmlFile->addWidget(btnLoad_);
    hboxKmlFile->addWidget(btnPlay_);
    hboxKmlFile->addWidget(btnPause_);
//    hbox->addWidget(labelFileName_);
    grpKmlFile->setLayout(hboxKmlFile);

    QGroupBox *grpVarioSettings = new QGroupBox("Vario settings");
    grpVarioSettings->setStyleSheet("QGroupBox {font: bold;border: 1px solid silver;border-radius: 6px;margin-top: 6px;}QGroupBox::title {subcontrol-origin: margin;left: 7px;padding: 0px 5px 0px 5px;}");
    QLabel *lblAltOffset = new QLabel(tr("Altitude offset vs. gpsAlt"));
    spinBoxAltOffset = new QSpinBox;
    spinBoxAltOffset->setRange(-500, 500);
    spinBoxAltOffset->setSingleStep(10);
    spinBoxAltOffset->setValue(0);
    QVBoxLayout *vboxVarioSettings = new QVBoxLayout;
    vboxVarioSettings->addWidget(lblAltOffset);
    vboxVarioSettings->addWidget(spinBoxAltOffset);
    grpVarioSettings->setLayout(vboxVarioSettings);

    QGroupBox *grpGnrmcSettings = new QGroupBox("$GNRMC");
    grpGnrmcSettings->setStyleSheet("QGroupBox {font: bold;border: 1px solid silver;border-radius: 6px;margin-top: 6px;}QGroupBox::title {subcontrol-origin: margin;left: 7px;padding: 0px 5px 0px 5px;}");
    QLabel *lblGnrmcDataValid = new QLabel(tr("Data valid V:Invalid, A:Valid"));
    comboBoxGnrmcDataValid_ = new QComboBox();
    comboBoxGnrmcDataValid_->addItem("A");
    comboBoxGnrmcDataValid_->addItem("V");

    QLabel *lblPosMode = new QLabel(tr("RMC Positioning mode, No fix, Autonomous, Differental"));
    comboBoxPosMode_ = new QComboBox();
    comboBoxPosMode_->addItem("A");
    comboBoxPosMode_->addItem("N");
    comboBoxPosMode_->addItem("D");
    QVBoxLayout *vboxGnrmcSettings = new QVBoxLayout;
    vboxGnrmcSettings->addWidget(lblGnrmcDataValid);
    vboxGnrmcSettings->addWidget(comboBoxGnrmcDataValid_);
    vboxGnrmcSettings->addWidget(lblPosMode);
    vboxGnrmcSettings->addWidget(comboBoxPosMode_);
    grpGnrmcSettings->setLayout(vboxGnrmcSettings);

    QGroupBox *grpGnggaSettings = new QGroupBox("$GNGGA");
    grpGnggaSettings->setStyleSheet("QGroupBox {font: bold;border: 1px solid silver;border-radius: 6px;margin-top: 6px;}QGroupBox::title {subcontrol-origin: margin;left: 7px;padding: 0px 5px 0px 5px;}");
    QLabel *lblDataValid = new QLabel(tr("Data valid V:Invalid, A:Valid"));
    comboBoxDataValid_ = new QComboBox();
    comboBoxDataValid_->addItem("A");
    comboBoxDataValid_->addItem("V");
    QLabel *lblFixStatus = new QLabel(tr("Fix status 0:Invalid 1:GNSS fix 2:DGPS fix"));
    comboBoxFixStatus_ = new QComboBox();
    comboBoxFixStatus_->addItem("1");
    comboBoxFixStatus_->addItem("0");
    comboBoxFixStatus_->addItem("2");
    QLabel *lblNumSats = new QLabel(tr("Num sattelites 0-12"));
    spinBoxSatts = new QSpinBox;
    spinBoxSatts->setRange(0, 12);
    spinBoxSatts->setSingleStep(1);
    spinBoxSatts->setValue(11);
    QLabel *lblHdop = new QLabel(tr("HDOP"));
    spinBoxHdop = new QDoubleSpinBox;
    spinBoxHdop->setRange(0.0, 3.0);
    spinBoxHdop->setSingleStep(0.1);
    spinBoxHdop->setValue(0.8);
    QLabel *lblGeoIDSeparation = new QLabel(tr("GeoID separation Heigh"));
    spinGeoIDSeparation = new QDoubleSpinBox;
    spinGeoIDSeparation->setRange(-100.0, 100.0);
    spinGeoIDSeparation->setSingleStep(0.5);
    spinGeoIDSeparation->setValue(36.5);
    QVBoxLayout *vboxGnggaSettings = new QVBoxLayout;
    vboxGnggaSettings->addWidget(lblDataValid);
    vboxGnggaSettings->addWidget(comboBoxDataValid_);
    vboxGnggaSettings->addWidget(lblFixStatus);
    vboxGnggaSettings->addWidget(comboBoxFixStatus_);
    vboxGnggaSettings->addWidget(lblNumSats);
    vboxGnggaSettings->addWidget(spinBoxSatts);
    vboxGnggaSettings->addWidget(lblHdop);
    vboxGnggaSettings->addWidget(spinBoxHdop);
    vboxGnggaSettings->addWidget(lblGeoIDSeparation);
    vboxGnggaSettings->addWidget(spinGeoIDSeparation);
    grpGnggaSettings->setLayout(vboxGnggaSettings);

    QGroupBox *grpGngsaSettings = new QGroupBox("$GNGSA");
    grpGngsaSettings->setStyleSheet("QGroupBox {font: bold;border: 1px solid silver;border-radius: 6px;margin-top: 6px;}QGroupBox::title {subcontrol-origin: margin;left: 7px;padding: 0px 5px 0px 5px;}");

    QLabel *lblGngsaMode = new QLabel(tr("Mode M:Manual, A:Auto (2D/3D fix)"));
    comboBoxGngsaPosMode_ = new QComboBox();
    comboBoxGngsaPosMode_->addItem("A");
    comboBoxGngsaPosMode_->addItem("M");

    QLabel *lblGngsaFixStatus = new QLabel(tr("Fix status 0:Invalid 1:GNSS fix 2:DGPS fix"));
    comboBoxGngsaFixStatus_ = new QComboBox();
    comboBoxGngsaFixStatus_->addItem("3");
    comboBoxGngsaFixStatus_->addItem("2");
    comboBoxGngsaFixStatus_->addItem("1");

    QLabel *lblSatIds = new QLabel(tr("Sattelites"));

    lineEdit = new QLineEdit();
    lineEdit->setText("1,2,3,4,5,6,7,8,9,10,11,12");
    QLabel *lblGngsaPdop = new QLabel(tr("PDOP"));
    spinBoxGngsaPdop = new QDoubleSpinBox;
    spinBoxGngsaPdop->setRange(0.0, 3.0);
    spinBoxGngsaPdop->setSingleStep(0.1);
    spinBoxGngsaPdop->setValue(0.8);
    QLabel *lblGngsaHdop = new QLabel(tr("HDOP"));
    spinBoxGngsaHdop = new QDoubleSpinBox;
    spinBoxGngsaHdop->setRange(0.0, 3.0);
    spinBoxGngsaHdop->setSingleStep(0.1);
    spinBoxGngsaHdop->setValue(0.8);
    QLabel *lblGngsaVdop = new QLabel(tr("VDOP"));
    spinBoxGngsaVdop = new QDoubleSpinBox;
    spinBoxGngsaVdop->setRange(0.0, 3.0);
    spinBoxGngsaVdop->setSingleStep(0.1);
    spinBoxGngsaVdop->setValue(0.8);

    QVBoxLayout *vboxGngsaSettings = new QVBoxLayout;
    vboxGngsaSettings->addWidget(lblGngsaMode);
    vboxGngsaSettings->addWidget(comboBoxGngsaPosMode_);
    vboxGngsaSettings->addWidget(lblGngsaFixStatus);
    vboxGngsaSettings->addWidget(comboBoxGngsaFixStatus_);
    vboxGngsaSettings->addWidget(lblSatIds);
    vboxGngsaSettings->addWidget(lineEdit);
    vboxGngsaSettings->addWidget(lblGngsaHdop);
    vboxGngsaSettings->addWidget(spinBoxGngsaPdop);
    vboxGngsaSettings->addWidget(lblGngsaPdop);
    vboxGngsaSettings->addWidget(spinBoxGngsaHdop);
    vboxGngsaSettings->addWidget(lblGngsaVdop);
    vboxGngsaSettings->addWidget(spinBoxGngsaVdop);

    grpGngsaSettings->setLayout(vboxGngsaSettings);

    labelLongitude_ = new QLabel();
    labelLatitude_ = new QLabel();

    QGridLayout *mainLayout = new QGridLayout;
    mainLayout->addWidget(grpKmlFile,0,0);
    mainLayout->addWidget(grpVarioSettings,1,0);
    mainLayout->addWidget(grpGnrmcSettings,2,0);
    mainLayout->addWidget(grpGnggaSettings,3,0);
    mainLayout->addWidget(grpGngsaSettings,3,1);

   mainLayout->addWidget(labelLongitude_,6,0);
   mainLayout->addWidget(labelLatitude_,6,1);

   setLayout(mainLayout);

   setWindowTitle(tr("KMLReplay"));

   eventTimer.start(1000);
   connect(&eventTimer,SIGNAL(timeout()),this,SLOT(motionHandler()));

   connect(btnLoad_, SIGNAL(released()), this, SLOT(onButtonReleased()));
   connect(btnPlay_, SIGNAL(released()), this, SLOT(onPlayButtonReleased()));
   connect(btnPause_, SIGNAL(released()), this, SLOT(onPauseButtonReleased()));

    gpsSocket = new QUdpSocket();
    gpsSocket->connectToHost(QHostAddress("127.0.0.1") , 44000);
    if (gpsSocket->waitForConnected(1000))
        qDebug("Gps Connected!");

    varioSocket = new QUdpSocket();
    varioSocket->connectToHost(QHostAddress("127.0.0.1") , 44001);
    if (varioSocket->waitForConnected(1000))
        qDebug("Vario Connected!");

    btnLoad_->setEnabled(true);
    btnPlay_->setEnabled(false);
    btnPause_->setEnabled(false);


}

// Destructor   QPushButton* button_;

KmlReplay::~KmlReplay()
{
    delete btnPlay_;
}

// Handler for button click
void KmlReplay::onButtonReleased()
{
    QString fileName = QFileDialog::getOpenFileName(this,tr("Open kml"), "",tr("kml (*.kml);;All Files (*)"));

    if (fileName.isEmpty())
        return;
    else {
        QFileInfo info(fileName);
        loadKmlFile(fileName);
        labelFileName_->setText(info.fileName());
        btnPlay_->setEnabled(true);
        setWindowTitle("KMLReplay - "+info.fileName());
    }
}

void KmlReplay::onPlayButtonReleased()
{
    appMode = PLAY;
    btnPlay_->setEnabled(false);
    btnPause_->setEnabled(true);
    btnLoad_->setEnabled(false);
}

void KmlReplay::onPauseButtonReleased()
{
    appMode = PAUSE;
    btnPlay_->setEnabled(true);
    btnPause_->setEnabled(false);
    btnLoad_->setEnabled(true);
}

double KmlReplay::getDistance(const double* lat1, const double* lon1, const double* lat2, const double* lon2)
{
    return 1852*(60 * R2D * 2 * asin(sqrt(pow(sin(D2R*(*lat1-*lat2)/2),2) +
                                    pow(sin(D2R*(*lon2-*lon1)/2),2) * cos(D2R* *lat1) * cos(D2R* *lat2)
                                   )
                              ));
}

double KmlReplay::bearingToNext(double lat1, double lon1, double lat2, double lon2)
{
    double originPointLat = lat2-lat1;
    double originPointLon = lon2 -lon1;

    float bearingRadians = atan2f(originPointLon, originPointLat); // get bearing in radians
    float bearingDegrees = bearingRadians * (180.0 / M_PI); // convert to degrees
    bearingDegrees = (bearingDegrees > 0.0 ? bearingDegrees : (360.0 + bearingDegrees)); // correct discontinuity
    return (double)bearingDegrees;
}

void KmlReplay::intermediatePoint (const double *lat1, const double *lon1, const double *lat2, const double *lon2, const double *fraction, double *latresult, double *lonresult)
{
    double A, B, x, y, z, d;

    double radLat1 = D2R * *lat1;
    double radLon1 = D2R * *lon1;
    double radLat2 = D2R * *lat2;
    double radLon2 = D2R * *lon2;

    //d = distance in radians between point 1 and point 2
    d = 2 * asin(sqrt(pow(sin((radLat1-radLat2)/2),2) +
                      pow(sin((radLon2-radLon1)/2),2) * cos(radLat1) * cos(radLat2)
                     )
                );

    A = sin((1-*fraction)*d)/sin(d);
    B = sin(*fraction*d)/sin(d);
    x = A*cos(radLat1)*cos(radLon1) +  B*cos(radLat2)*cos(radLon2);
    y = A*cos(radLat1)*sin(radLon1) +  B*cos(radLat2)*sin(radLon2);
    z = A*sin(radLat1)              +  B*sin(radLat2);
    *latresult = R2D * atan2(z,sqrt(pow(x,2)+pow(y,2)));
    *lonresult = R2D * atan2(y,x);
}

int KmlReplay::loadKmlFile(QString fileName) {

    QFile file(fileName);

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qDebug() << "File open error:" << file.errorString();
        return false;
    }

    QXmlStreamReader xml(&file);

    QString type = "";
    QString secs = "";
    QString alt = "";
    QString coord = "";

    sampleIndex = 0;
    numberOfSecounds = 0;

    while (!xml.atEnd()) {

        xml.readNext();

        if (xml.name() == "Metadata") {
            if (xml.isStartElement()) {
                type = xml.attributes().value("type").toString();
            }
        }

        if (xml.name() == "FsInfo") {
            qDebug() << xml.attributes().value("time_of_first_point").toString();
        }

        if (type == "track") {
            if (xml.name() == "SecondsFromTimeOfFirstPoint") {
                secs = xml.readElementText();
                secs = secs.simplified();
                qDebug() << secs;
            }

            if (xml.name() == "PressureAltitude") {
                alt = xml.readElementText();
                alt = alt.simplified();
                qDebug() << alt;
            }
            if (xml.name() == "coordinates") {
                coord = xml.readElementText();
                coord = coord.simplified();
                qDebug() << coord;
            }
        }
    }

    secList = secs.split(" ");
    altList = alt.split(" ");
    posList = coord.split(" ");

    qDebug() << "Secs:" << secList.count();
    qDebug() << "Alt:" << altList.count();
    qDebug() << "coord:" << posList.count();

    numberOfSecounds = secList.last().toInt();

}
/*



*/
void KmlReplay::motionHandler() {

    /*comboBoxGngsaFixStatus_
        Copy values from UI to variables
    */

    intAltOffset = spinBoxAltOffset->value();
    strGnrmcDataValid = comboBoxGnrmcDataValid_->currentText();
    strGnrmcPositionMode = comboBoxPosMode_->currentText();
    strGnggaDataValid = comboBoxDataValid_->currentText();
    strGnggaFixStatus = comboBoxFixStatus_->currentText();
    strGnggaNumSats = QString::number(spinBoxSatts->value());
    strGnggaHDop = QString::number(spinBoxHdop->value());
    strGnggaGeoIDSepHeight = QString::number(spinGeoIDSeparation->value());

    strGngsaPosMode = comboBoxGngsaPosMode_->currentText();
    strGngsaFixStatus = comboBoxGngsaFixStatus_->currentText();
    strGngsaHDop = QString::number(spinBoxGngsaHdop->value());
    strGngsaPDop = QString::number(spinBoxGngsaPdop->value());
    strGngsaVDop = QString::number(spinBoxGngsaVdop->value());

    strGngsaSats = lineEdit->text();
    qDebug() << strGngsaSats.count(QLatin1Char(','));

    //myString.count(QLatin1Char('#'));

    if ((numberOfSecounds > 0)&&(appMode == PLAY)) {
        qDebug() << "motionHandler() - process datapoint: " << QString::number(sampleIndex);
        handleDatapoint(sampleIndex);
        sampleIndex++;
    } else {
        qDebug() << "motionHandler() - idle";
    }

    labelLongitude_->setText(strLongitudeDegMin);
    labelLatitude_->setText(strLatitudeDegMin);

}

void KmlReplay::sendVarioData(QString strVario) {
    int send;
    QByteArray byteArrayVario = strVario.toLatin1();
    send = varioSocket->write(byteArrayVario);
}

void KmlReplay::sendGpsData(QString strNmea) {
    int send;
    QByteArray byteArrayNmea = strNmea.toLatin1();
    send = gpsSocket->write(byteArrayNmea);
}


QString KmlReplay::composeVarStr(int altitude, QString vario) {

    QString strAlt;

    strAlt = QString::number((altitude+intAltOffset));

    QString strVario = QString("FAKE_CONDOR\r\nFAKE_CONDOR\r\nalt=" + strAlt + "\r\nvario="+vario+"\r\n");
    qDebug() << strVario;

    return strVario;
}

QString KmlReplay::composeGnrmc(QString strTime, QString strLati, QString strLatiInd, QString strLong, QString strLongInd, QString strSpeed, QString strCourse, QString strDate) {

    QString strGnrmc = "$GNRMC,"+strTime+","+strGnrmcDataValid+","+strLati+","+strLatiInd+","+strLong+","+strLongInd+","+strSpeed+","+strCourse+","+strDate+",,,"+strGnrmcPositionMode+"*64";


    qDebug() << strGnrmc << " fields:" << strGnrmc.count(QLatin1Char(','));

    return  strGnrmc;
}

QString KmlReplay::composeGngga(QString strTime, QString strLati, QString strLatiInd, QString strLong, QString strLongInd, QString strGpsAlti) {

    QString strGngga;

    strGngga = "$GNGGA,"+strTime+","+strLati+","+strLatiInd+","+strLong+","+strLongInd+","+strGnggaFixStatus+","+strGnggaNumSats+","+strGnggaHDop+","+strGpsAlti+",M,"+strGnggaGeoIDSepHeight+",M,,*45";

    qDebug() << strGngga << " fields:" << strGngga.count(QLatin1Char(','));

    return strGngga;
}

QString KmlReplay::composeGngsa() {

    QString strGngsa;

    if (strGngsaSats.count(QLatin1Char(',')) == 11) {
        strGngsa = "$GNGSA,"+strGngsaPosMode+","+strGngsaFixStatus+","+strGngsaSats+","+strGngsaPDop+","+strGngsaHDop+","+strGngsaVDop+"*45";
    } else {
        strGngsa = "$GNGSA,"+strGngsaPosMode+","+strGngsaFixStatus+",03,22,01,23,31,11,17,14,09,19,,,"+strGngsaPDop+","+strGngsaHDop+","+strGngsaVDop+"*45";
    }
    qDebug() << strGngsa << " fields:" << strGngsa.count(QLatin1Char(','));


    return strGngsa;
}

void KmlReplay::handleDatapoint(int idx) {

    time_t now;
    char buffer [80];
    char dateBuffer[80];
    struct tm * timeinfo;
    int    currSample = 0;
    bool ok(false);
    double vario;
    double distance;
    double speed_f;
    double latitude;
    double longitude;
    double nextLat;
    double nextLong;
    double timeDiffSec;
    double course;
    double deadReckDist;
    double deadReckLat;
    double deadReckLong;
    int deadReckoning;
    double deadReckAlt;
    double deadRecLongDeg;
    double deadRecLongMin;
    double deadRecLatDeg;
    double deadRecLatMin;
    int longDeg;
    int latDeg;
    double longMin;
    double latMin;


    QString strDeadRecLongDegMin;
    QString strDeadRecLatDegMin;
    QString strLongitude;
    QString strLatitude;
    QString strLastLongitude;
    QString strLastLatitude;
    QString strNextLongitude;
    QString strNextLatitude;
    QString chrLatInd;
    QString chrLongInd;
    QString chrDeadRecLatInd;
    QString chrDeadRecLongInd;
    QString varStr;
    QString gnrmcStr;
    QString gnggaStr;
    QString gngsaStr;

    time(&now);
    timeinfo = localtime (&now);
    strftime (buffer,80,"%H%M%S.00",timeinfo);
    strftime (dateBuffer,80, "%d%m%y", timeinfo);

    std::ofstream trace ("trace.txt");

    while (secList[currSample+1].toInt() < idx) {
        currSample++;
    }

    qDebug() << "Using currSample:" << QString::number(currSample) << " time offset:" << QString::number(secList[currSample].toInt());

    timeDiffSec = (secList[currSample+1].toInt() - secList[currSample].toInt());
    vario=0.0;

    strLongitude = posList[currSample].split(",")[0];
    strLatitude = posList[currSample].split(",")[1];
    longitude = strLongitude.toDouble(&ok);
    latitude = strLatitude.toDouble(&ok);
    vario = (double)(((altList[currSample+1].toInt() - altList[currSample].toInt()))/timeDiffSec);

    strNextLongitude = posList[currSample+1].split(",")[0];
    strNextLatitude = posList[currSample+1].split(",")[1];
    nextLong = strNextLongitude.toDouble(&ok);
    nextLat = strNextLatitude.toDouble(&ok);
    distance = getDistance(&latitude, &longitude, &nextLat, &nextLong);
    speed_f = ((distance/timeDiffSec)*1.944);

    if (latitude > 0) {
        chrLatInd = "N";
    } else {
        chrLatInd = "S";
    }

    if (longitude > 0) {
        chrLongInd = "E";
    } else {
        chrLongInd = "W";
    }

    if (secList[currSample].toInt() == idx) {
        deadReckoning = 0;
        course = bearingToNext(latitude, longitude, nextLat, nextLong);
    } else {
        deadReckoning++;
        deadReckDist = (speed_f*deadReckoning);

        if (deadReckDist != 0.0) {
            deadReckAlt = altList[currSample].toDouble() + (vario*deadReckoning);
            intermediatePoint (&latitude, &longitude, &nextLat, &nextLong, &deadReckDist, &deadReckLat, &deadReckLong);
            course = bearingToNext(latitude, longitude, deadReckLat, deadReckLong);
        } else {
            course = 0.0;
            deadReckAlt = altList[currSample].toDouble() + (vario*deadReckoning);
            deadReckLat = latitude;
            deadReckLong = longitude;
        }

        if (deadReckLat > 0) {
            chrDeadRecLatInd = "N";
        } else {
            chrDeadRecLatInd = "S";
        }

        if (deadReckLong > 0) {
            chrDeadRecLongInd = "E";
        } else {
            chrDeadRecLongInd = "W";
        }
    }

    longDeg = (int)longitude;
    longMin = (longitude-(double)longDeg)*60;

    latDeg = (int)latitude;
    latMin = (latitude-(double)latDeg)*60;

    strLongitudeDegMin = QString::number(longDeg).rightJustified(3, '0',true)+QString::number(longMin);
    strLatitudeDegMin = QString::number(latDeg).rightJustified(2, '0', true)+QString::number(latMin);

    QString strVario = QString::number((int)(vario));
    QString strTrace;

    if (deadReckoning == 0) {

        strTrace = QString::fromUtf8(buffer)+"\nIdx:"+QString::number(idx) + " Sample: "+ QString::number(currSample) + " Delta sec:"+QString::number(timeDiffSec)+"\n";
        strTrace += "Alt:"+altList[currSample]+" Vario:"+strVario+"\n";
        strTrace += "Dist:"+ QString::number(distance)+" Speed:"+QString::number(speed_f)+" Course:" + QString::number(course)+"\n";
        strTrace += "Real sample: ("+strLongitudeDegMin+"/"+strLatitudeDegMin+")\n\n";
        qDebug().noquote() << strTrace;

        varStr = composeVarStr(altList[currSample].toInt(), strVario);
        gnrmcStr = composeGnrmc(QString::fromUtf8(buffer), strLatitudeDegMin, chrLatInd, strLongitudeDegMin, chrLongInd, QString::number(speed_f), QString::number((int)course),QString::fromUtf8(dateBuffer));
        gnggaStr = composeGngga(QString::fromUtf8(buffer), strLatitudeDegMin, chrLatInd, strLongitudeDegMin, chrLongInd, altList[currSample]);

    } else {

        deadRecLongDeg = (int)longitude;
        deadRecLongMin = (longitude-(double)longDeg)*60;
        deadRecLatDeg = (int)latitude;
        deadRecLatMin = (latitude-(double)latDeg)*60;

        strDeadRecLongDegMin = QString::number(deadRecLongDeg).rightJustified(3, '0',true)+QString::number(deadRecLongMin);
        strDeadRecLatDegMin = QString::number(deadRecLatDeg).rightJustified(2, '0', true)+QString::number(deadRecLatMin);

        strTrace = QString::fromUtf8(buffer)+"\nIdx:"+QString::number(idx) + " Sample: "+ QString::number(currSample) + " Delta sec:"+QString::number(timeDiffSec)+"\n";
        strTrace += "Alt:"+QString::number(deadReckAlt)+" Vario:"+strVario+"\n";
        strTrace += "Dist:"+ QString::number(distance)+" Speed:"+QString::number(speed_f)+" Course:" + QString::number(course)+"\n";
        strTrace+="Dead recon: ("+strDeadRecLongDegMin+"/"+strDeadRecLatDegMin+")\n\n";
        qDebug().noquote() << strTrace;

        varStr = composeVarStr( (int)deadReckAlt , strVario);
        gnrmcStr = composeGnrmc(QString::fromUtf8(buffer), strDeadRecLatDegMin, chrDeadRecLatInd, strDeadRecLongDegMin, chrDeadRecLongInd, QString::number(speed_f), QString::number((int)course),QString::fromUtf8(dateBuffer));
        gnggaStr = composeGngga(QString::fromUtf8(buffer), strDeadRecLatDegMin, chrDeadRecLatInd, strDeadRecLongDegMin, chrDeadRecLongInd, QString::number((int)deadReckAlt));
    }

    gngsaStr = composeGngsa();

    strTrace+="\n";

    sendVarioData(varStr);
    sendGpsData(gnrmcStr);
    sendGpsData(gngsaStr);
    sendGpsData(gnggaStr);

    QByteArray traceCStr = strTrace.toLatin1();
    trace << traceCStr.data();
}



