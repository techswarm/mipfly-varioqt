/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MICROCONTROLLER_H
#define MICROCONTROLLER_H

class SerialConnection;

class Microcontroller
{
public:
    Microcontroller(const SerialConnection &conn);

    void setNumSample(int num) const;
    void setTasCompensation(int num) const;
    void reqId() const;
private:
    const SerialConnection &serial;

    void STOM(int val) const;
    void STID(int val) const;
    void STNUMS(int val) const;
    void STTC(int val) const;
};

#endif // MICROCONTROLLER_H
