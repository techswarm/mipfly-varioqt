/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "qfonts.h"

QFont Vario::get16pxFont() {
    QFont font;
    font.setPixelSize(16);
    font.setFamily("Pixel Operator");
    font.setStyleStrategy(QFont::NoAntialias);

    return font;
}

QFont Vario::get8pxFont() {
    QFont font;
    font.setPixelSize(8);
    font.setFamily("Pixel Operator 8");
    font.setStyleStrategy(QFont::NoAntialias);

    return font;
}

QFont Vario::getFont(QString mnuFontName, int mnuFontSize) {

    QFont font;
    font.setPixelSize(mnuFontSize);
    font.setFamily(mnuFontName);
    font.setStyleStrategy(QFont::NoAntialias);

    return font;
}
