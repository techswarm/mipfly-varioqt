/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "glider.h"
#include <QSettings>

Glider::Glider(QObject *parent) : QObject(parent)
{
#ifndef FAKE_CONDOR
    QSettings settings;
    glide = settings.value("glide",9).toFloat();
    trimSpeed = settings.value("trimSpeed",40).toFloat();
#else
    glide = 40;
    trimSpeed = 100;
#endif
}

Glider::~Glider()
{
#ifndef FAKE_CONDOR
    QSettings settings;
    settings.setValue("glide",glide);
    settings.setValue("trimSpeed",trimSpeed);
#endif
}

void Glider::setHeight(int hgt)
{
    height = hgt;
}

void Glider::setHeading(qreal hdg)
{
    heading = hdg;
}

void Glider::setGroundSpeed(qreal gndSpeed)
{
    groundSpeed = gndSpeed;
}

void Glider::setPosition(Coord pos)
{
    position = pos;
}

void Glider::setTrimSpeed(float value)
{
    trimSpeed = value;
}

void Glider::setGlide(float value)
{
    glide = value;
}

Coord Glider::getPosition() const
{
    return position;
}

float Glider::getGroundSpeed() const
{
    return groundSpeed;
}

float Glider::getHeading() const
{
    return heading;
}

int Glider::getHeight() const
{
    return height;
}

float Glider::getTrimSpeed() const
{
    return trimSpeed;
}

float Glider::getGlide() const
{
    return glide;
}
