/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __VARIOGUI_UTILS_H
#define __VARIOGUI_UTILS_H

#include <time.h>
#include <vector>
#include <string>

#define MIN(A,B) ((A) < (B) ? (A) : (B))

#define sprinttm(BUFF, S) sprintf(BUFF, "%.2d:%.2d:%.2d", (S)->tm_hour, (S)->tm_min, (S)->tm_sec)
#define sprinttmShort(BUFF, S) sprintf(BUFF, "%.2d:%.2d", (S)->tm_hour, (S)->tm_min)

struct tm break_timediff(double diff);
double timeval_diff(struct timeval end, struct timeval start);

// Do not end following macro with semicolon, it is supposed to be called
// as a function (thus the user will provide the final semicolon needed).
#define copy_timeval(DEST, SRC) (DEST).tv_sec = (SRC).tv_sec;\
								(DEST).tv_usec = (SRC).tv_usec

char getch();

std::vector<std::string> explode(const std::string& src, const std::string& regexp);
const std::string timestampStr();

bool dirExists(const std::string& path);
int mkdirIfNotExists(const std::string& path);



#endif
