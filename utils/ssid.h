/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SSID_H
#define SSID_H

#include <QString>

class Ssid
{

public:
    Ssid(QString name, QString active, QString channel, QString security);
    ~Ssid();

    QString getName();
    QString getChannel();
    QString getSecurity();
    QString getActive();
    bool    isConnected();
    bool    isProtected();

private:
    QString name;
    QString active;
    QString channel;
    QString security;
};

#endif // SSID_H
