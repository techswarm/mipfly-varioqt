/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "utils.h"

#include <sys/stat.h>
#include <unistd.h>
#include <termios.h>
#include <cstdio>
#include <QDir>

#if (__GNUC__ == 4 && __GNUC_MINOR__ > 8) || __GNUC__ > 4
#define REGEX_IMPLEMENTED
#endif

using std::vector;
using std::string;
using std::perror;

#ifdef REGEX_IMPLEMENTED2
#include <regex>
using std::regex;
using std::sregex_token_iterator;
#else
#include <sstream>
using std::getline;
using std::stringstream;
#endif


struct tm break_timediff(double diff)
{
	struct tm ret;

	ret.tm_hour = (int)diff / 3600;
	ret.tm_min = ((int)diff % 3600) / 60;
	ret.tm_sec = ((int)diff % 3600) % 60;

	return ret;
}

double timeval_diff(struct timeval end, struct timeval start) {
    return (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_usec - start.tv_usec);
}

// For reading character without echo & wait for enter
char getch() {
        char buf = 0;
        struct termios old = {0};
        if (tcgetattr(0, &old) < 0)
                perror("tcsetattr()");
        old.c_lflag &= ~ICANON;
        old.c_lflag &= ~ECHO;
        old.c_cc[VMIN] = 1;
        old.c_cc[VTIME] = 0;
        if (tcsetattr(0, TCSANOW, &old) < 0)
                perror("tcsetattr ICANON");
        if (read(0, &buf, 1) < 0)
                perror ("read()");
        old.c_lflag |= ICANON;
        old.c_lflag |= ECHO;
        if (tcsetattr(0, TCSADRAIN, &old) < 0)
                perror ("tcsetattr ~ICANON");
        return (buf);
}

#ifdef REGEX_IMPLEMENTED2

vector<string> explode(const string& src, const string& regexp)
{
	vector<string> results;
	regex reg(regexp);

	sregex_token_iterator pos(src.cbegin(), src.cend(), reg, -1);
	sregex_token_iterator end;

	for (; pos != end; ++pos) {
		//if(!pos->str().empty())
        string str(pos->str());
        results.push_back(str);
	}
	return results;
}

#else

/*
 * getline() implementation taken from:
 * https://ysonggit.github.io/coding/2014/12/16/split-a-string-using-c.html
 */

vector<string> explode(const string &s, const string& regexp) {
	char delim = regexp.front();
    stringstream ss(s);
    string item;
    vector<string> tokens;
    while (getline(ss, item, delim)) {
        tokens.push_back(item);
    }
    return tokens;
}

#endif

const std::string timestampStr() {
    time_t now = time(NULL);
    struct tm *ptr_time = localtime(&now);
    char buffer[10];

    strftime(buffer, 10, "%Y%m%d", ptr_time);

    return std::string(buffer);
}

bool dirExists(const std::string& path) {
    struct stat buf;
    return stat(path.c_str(), &buf) == 0 && S_ISDIR(buf.st_mode);
    return QDir(QString(path.c_str())).exists();
}

int mkdirIfNotExists(const std::string& path) {

    if (!dirExists(path)) {
        return mkdir(path.c_str(), 0700);
    }
    return 0;
}
