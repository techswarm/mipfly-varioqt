/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LOGGERINTERFACE_H
#define LOGGERINTERFACE_H

#include <QObject>
#include "localserver.h"
#include <QTimer>

class LoggerInterface : public QObject
{
    Q_OBJECT
public:
    explicit LoggerInterface(QObject *parent = nullptr);

    void enableReceive();
    void disableReceive();
    void startRec(QString igcName);
    void stopRec();

private:
    LocalServer *lsGPS;
    LocalServer *lsVario;
    LocalServer *lsComm;
    QTimer timer10s;

signals:

public slots:
    void timerTask();
};

#endif // LOGGERINTERFACE_H
