/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "unitconverter.h"

UnitConverter::UnitConverter()
{

}

float UnitConverter::mPerSecToFtPerMin(float mPerSec)
{
    return mPerSec * 196.850394;
}

float UnitConverter::ftPerMinTomPerSec(float ftPerMinute)
{
    return ftPerMinute / 196.850394;
}

float UnitConverter::mToFt(float m)
{
    return m * 3.2808399;
}

float UnitConverter::ftToM(float ft)
{
    return ft / 3.2808399;
}

float UnitConverter::kmPerHToKnots(float kmPerH)
{
    return kmPerH * 0.539956803;
}

float UnitConverter::KnotsTokmPerH(float knots)
{
    return knots * 1.85200;
}

float UnitConverter::mToMi(float m)
{
    return m * 0.000621371192;
}

float UnitConverter::kmPerHToMiPerHour(float kmPerH)
{
    return kmPerH * 0.621371192;
}

float UnitConverter::MiPerHourTokmPerH(float miPerH)
{
    return miPerH * 1.609344;
}

float UnitConverter::kmToMi(float km)
{
    return km * 0.621371192;
}

float UnitConverter::MiTokm(float mi)
{
    return mi * 1.609344;
}


