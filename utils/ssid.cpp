/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ssid.h"

#include <QDebug>

Ssid::Ssid(QString name, QString active, QString channel, QString security)
{
    this->name = name;
    this->active = active;
    this->channel = channel;

    if (!security.isEmpty())
        this->security = security;
    else
        this->security = "Open";

}

Ssid::~Ssid()
{

}

QString Ssid::getName()
{
    return name;
}

QString Ssid::getChannel()
{
    return channel;
}

QString Ssid::getSecurity()
{
    return security;
}

bool Ssid::isConnected()
{
    if (active.contains("yes", Qt::CaseInsensitive)) return true;
    return false;
}

QString Ssid::getActive() {
    return active;
}

bool Ssid::isProtected() {
    if (active.contains("Open", Qt::CaseInsensitive)) return false;
    return true;
}


