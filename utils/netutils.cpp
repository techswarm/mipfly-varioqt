/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "netutils.h"
#include <QThread>
#include <QString>
#include "vario_common.h"
#include <QFile>
#include <QFileInfo>

NetUtils::NetUtils()
{

}

bool NetUtils::internetExists()
{
    QString cmd = "ping -c1 -s1 "+ Vario::hostNoHttp;
    if (system(qPrintable(cmd)))
    {
        qDebug()<<"There is no internet connection";
        //QThread::sleep(1);
        return false;
    }
    else
    {
        qDebug()<<"Internet connection available";
        //QThread::sleep(1);
        return true;
    }
}

void NetUtils::wgetNamed(QString url, QString path)
{
    QString cmd = "wget -O "+path+"/ui.zip '";
    cmd += url;
    cmd += "'";
    system(qPrintable(cmd));
}

void NetUtils::wget(QString url, QString path)
{
    QString cmd = "wget ";
    cmd += url;
    cmd += " --directory-prefix=";
    cmd += path;
    system(qPrintable(cmd));
}

void NetUtils::uploadFile(QString url, QString path, QString usrId, QString usrKey)
{
    //curl -F "file=@localfile;filename=nameinpost" url.com
    QString cmd = "curl -F 'igc=@";
    cmd+=path+"' ";

    cmd+="-F id='"+usrId+"' ";
    cmd+="-F key='"+usrKey+"' ";

    cmd+=url;
    qDebug()<<cmd;
    system(qPrintable(cmd));
}
