/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "netstats.h"
#include <QObject>
#include <QFuture>
#include <QtConcurrent/QtConcurrent>
#include "utils/BTUtil.h"
#include "netutils.h"

NetStats::NetStats()
{
    connect(&tick,SIGNAL(timeout()),this,SLOT(timerTick()));
    tick.setInterval(1000);
    tick.start();

    connect(&internetExistsWtcher,SIGNAL(finished()),this,SLOT(internetExistsFinished()),Qt::UniqueConnection);
}

void NetStats::resetTimeout()
{
    timeout = 20;
    if(stats != NetStats::InternetConnected)
    {
        stats = NetStats::InternetConnected;
        emit statusChanged(NetStats::InternetConnected);
    }
}

void NetStats::checkStatus()
{
    internetExistsFuture = QtConcurrent::run(NetUtils::internetExists);
    internetExistsWtcher.setFuture(internetExistsFuture);
}

void NetStats::timerTick()
{
    timeout --;
    if(timeout < 0)
    {
        if(!BTUtil::bnep0Exists())
        {
            if(stats != NetStats::NoConnection)
            {
                stats = NetStats::NoConnection;
                emit statusChanged(NetStats::NoConnection);
            }
        }
        timeout = 20;
    }

    if(timeout == 5)
    {
        checkStatus();
    }
}

void NetStats::internetExistsFinished()
{
    if(internetExistsFuture.result() == true)
        resetTimeout();
    else
    {
        if(stats != NetStats::BNEPConnected)
        {
            stats = NetStats::BNEPConnected;
            emit statusChanged(NetStats::BNEPConnected);
        }
    }
}
