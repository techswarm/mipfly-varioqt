/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "fileUtils.h"
#include <QProcess>

FileUtils::FileUtils()
{

}

bool FileUtils::inUse(QString filename)
{
    filename = filename.replace(" ","\\ ");
    QProcess pingProcess;
    QString exe = "lsof "+filename;
    pingProcess.start(exe);
    pingProcess.waitForFinished();
    QString output(pingProcess.readAll());
    if (output.length()<5)return false;
    return true;
}

void FileUtils::remove(QString filename)
{
    filename = filename.replace(" ","\\ ");
    QString cmd = "rm -r "+filename;
    system(qPrintable(cmd));
}

void FileUtils::move(QString filename, QString destPath)
{
    filename = filename.replace(" ","\\ ");
    QString cmd = "mv "+filename+" "+destPath;
    system(qPrintable(cmd));
}
