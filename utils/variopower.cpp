/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "variopower.h"

#include <string>
#include <sstream>
#include <fstream>

#include <QtDebug>

#ifdef __ARMEL__
static const std::string SysFS_Base = "/sys/power/axp_pmu";
#else
static const std::string SysFS_Base = "/hdd/dutzu/workspace/Techswarm/build-VarioQt-Desktop-Debug/sysfs";
#endif


std::string pathTo(std::string value)
{
    std::stringstream ss;
    ss << SysFS_Base << "/" << value;
    return ss.str();
}

bool varioPower::chargeReseted = false;

varioPower::varioPower()
{
}

varioPower::~varioPower()
{
}

int16_t varioPower::GetBatteryCapacityPercent()
{
    //chargeReseted = true;
    //ResetBatteryCharge();

    /*
    if(chargeReseted == false)
    {
        if(GetBatteryStatus()=="Discharging")
        {
            if(GetBatteryVoltageNow()>voltageFull)
            {
                ResetBatteryCharge();
                chargeReseted = true;
            }
        }
    }

    int32_t charge = 0;
    std::ifstream f(pathTo("battery/charge"));
    f >> charge;

    if(charge>100000)
    {
        ResetBatteryCharge();
    }

    charge = charge * -1;

    double percent = 100 - ((double)charge/(double)chargeTotal)*100;
    */

    int32_t voltageNow = GetBatteryVoltageNow();
    int32_t voltageRange = voltageFull - voltageEmpty;

    //int32_t currentNow = GetBatteryAmperageNow();
    //float voltageDrop = currentNow * 0.1;
    //float voltageDrop = 0;

    float unity = (float)(voltageNow  - voltageEmpty)/(float)voltageRange;
    if(unity > 1)unity = 1;
    if(unity < 0)unity = 0;


    return (int16_t)(unity*100);
}


int32_t varioPower::GetBatteryVoltageNow()
{
    int val = 0;
    std::ifstream f(pathTo("battery/voltage"));
    f >> val;
    return val;
}

std::string varioPower::GetBatteryStatus()
{
    int val;
    std::ifstream f(pathTo("battery/charging"));
    f >> val;
    if(val == 1)
        return "Charging";
    else
        return "Discharging";
}

int32_t varioPower::GetBatteryChargeNow()
{
    int32_t val = 0;
    std::ifstream f(pathTo("battery/charge"));
    f >> val;
    return val;
}

int32_t varioPower::GetBatteryAmperageNow()
{
    int32_t val = 0;
    std::ifstream f(pathTo("battery/amperage"));
    f >> val;
    return val;
}

void varioPower::ResetBatteryCharge()
{

    std::ofstream f(pathTo("control/reset_charge_counter"));
    f << "0";
    f.flush();
    f << "1";
    f.flush();
}
