/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CHATSERVER_H
#define CHATSERVER_H

#include <qbluetoothserviceinfo.h>
#include <qbluetoothaddress.h>

#include <QtCore/QObject>
#include <QtCore/QList>

QT_FORWARD_DECLARE_CLASS(QBluetoothServer)
QT_FORWARD_DECLARE_CLASS(QBluetoothSocket)

QT_USE_NAMESPACE

class BTServer : public QObject
{
    Q_OBJECT

public:
    explicit BTServer(QObject *parent = 0);
    ~BTServer();
    static BTServer *BTServerFab(QObject *parent = 0);

    void startServer(const QBluetoothAddress &localAdapter = QBluetoothAddress());
    void stopServer();

    void setVarioPostscale(int value);

    void setGpsPostscale(int value);

    int getVarioPostscale() const;

    int getGpsPostscale() const;

public slots:
    void sendMessage(const QString &message);

    void GPSNMEAReceived(QString msg);
    void setVspeed(int vSpeedCMPS);
    void setBaroHeight(int baroHeightm);
    void setTASkmh(qreal speed);

signals:
    void messageReceived(const QString &sender, const QString &message);
    void clientConnected(const QString &name);
    void clientDisconnected(const QString &name);

private slots:
    void clientConnected();
    void clientDisconnected();
    void readSocket();

private:
    QBluetoothServer *rfcommServer;
    QBluetoothServiceInfo serviceInfo;
    QList<QBluetoothSocket *> clientSockets;
    int baroHeightMeters = 0;
    float taskmh = 0;
    double getSeaLevel(double pressure, double altitude);

    int varioPostscale = 1;
    int gpsPostscale = 1;

    int varioPostscaleCounter=0;
    int gnrmcPostscalerCounter = 0;
    int gnggaPostscalerCounter = 0;
};

#endif // CHATSERVER_H

