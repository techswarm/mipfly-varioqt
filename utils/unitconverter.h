/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef UNITCONVERTER_H
#define UNITCONVERTER_H


class UnitConverter
{
public:
    UnitConverter();
    static float mPerSecToFtPerMin(float mPerSec);
    static float mToFt(float m);
    static float kmPerHToKnots(float kmPerH);
    static float KnotsTokmPerH(float knots);
    static float mToMi(float m);
    static float kmPerHToMiPerHour(float kmPerH);
    static float MiPerHourTokmPerH(float miPerH);
    static float kmToMi(float km);
    static float MiTokm(float mi);
    static float ftPerMinTomPerSec(float ftPerMinute);
    static float ftToM(float ft);
};

#endif // UNITCONVERTER_H
