/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "vario_common.h"

QString Vario::SETTINGS_TZ("timezone");
QString Vario::SETTINGS_SMPL("num_samples");
QString Vario::SETTINGS_WNUMRD("wind_num_readings");
QString Vario::SETTINGS_WNUMAVG("wind_num_avg");
QString Vario::SETTINGS_WDTSPRD("wind_data_spread");
QString Vario::SETTINGS_RMSERROR("wind_rms_error4");
QString Vario::SETTINGS_GNUMAVG("glide_num_avg");
QString Vario::SETTINGS_PILOT("pilot_name");
QString Vario::SETTINGS_VOL("volume_percent");
QString Vario::SETTINGS_TAKEOFFSND("takeoff_sound_filename");
QString Vario::SETTINGS_LANDSND("landing_sound_filename");
QString Vario::SETTINGS_ONSND("app_on_sound_filename");
QString Vario::SETTINGS_DARKTHEME("use_dark_theme");

QString Vario::SETTINGS_VARIOINTSEC("vario_int_sec");
QString Vario::SETTINGS_IGCSTDNM("igc_std_filename");
QString Vario::SETTINGS_CRTLOC("saved_crt_loc");
QString Vario::SETTINGS_AUTOCRTLOC("auto_set_crt_loc");

QString Vario::DATE_FORMAT("dd.MM.yyyy");
QString Vario::DEFAULT_PILOT("default");


QString getFixStr(int fixType) {
    switch (fixType)
    {
    case FIX_UNKNOWN:
        return QString("UNKNOWN");
    case FIX_NONE:
        return QString("NO");
    case FIX_2D:
        return QString("2D");
    case FIX_3D:
        return QString("3D");
    default:
    return QString("ERROR");
    }
}
