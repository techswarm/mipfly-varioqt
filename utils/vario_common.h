/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VARIO_COMMON_H
#define VARIO_COMMON_H

#include <QString>

enum FixType {
    FIX_UNKNOWN,
    FIX_NONE,
    FIX_2D,
    FIX_3D
};

QString getFixStr(int fixType);

namespace Vario
{
    extern QString SETTINGS_TZ;         // timezone
    extern QString SETTINGS_SMPL;       // num samples
    extern QString SETTINGS_WNUMRD;     // wind num readings
    extern QString SETTINGS_WNUMAVG;    // wind num average
    extern QString SETTINGS_WDTSPRD;    // wind data spread
    extern QString SETTINGS_RMSERROR;  // wind rms error
    extern QString SETTINGS_GNUMAVG;    // glide num average
    extern QString SETTINGS_PILOT;      // pilot name

    extern QString SETTINGS_VOL;        // volume percent
    extern QString SETTINGS_SND_SQUARE;
    extern QString SETTINGS_SND_UP_THR;
    extern QString SETTINGS_SND_DOWN_THR;
    extern QString SETTINGS_SND_UP_F0;
    extern QString SETTINGS_SND_UP_F1000;
    extern QString SETTINGS_SND_DOWN_F0;
    extern QString SETTINGS_SND_DOWN_F1000;

    extern QString SETTINGS_TAKEOFFSND; // takeoff sound file name
    extern QString SETTINGS_LANDSND;    // landing sound file name
    extern QString SETTINGS_ONSND;      // application start sound file name
    extern QString SETTINGS_DARKTHEME;  // display dark theme?

    extern QString SETTINGS_VARIOINTSEC;
    extern QString SETTINGS_IGCSTDNM;   // use IGC standard file name
    extern QString SETTINGS_CRTLOC;     // current location name (saved)
    extern QString SETTINGS_AUTOCRTLOC; // current location is set automatically

    extern QString DATE_FORMAT;
    extern QString DEFAULT_PILOT;

    const int SCREEN_WIDTH = 240;
    const int SCREEN_HEIGHT = 320;

    const int GPIO1 = 136;
    const int GPIO2 = 137;
    const int GPIO3 = 135;

    const int GPIO1_INT = 244;
    const int GPIO2_INT = 243;
    const int GPIO3_INT = 245;

    const int GPIO_POWER = 139;

#ifdef __ARMEL__
    const QString host = "https://mipfly.com/";
#else
    const QString host = "http://www.mipfly.com/";
#endif


    const QString hostNoHttp = "mipfly.com";
}

#endif // VARIO_COMMON_H
