/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Version.h"
#include <QFile>
#include <QString>

#ifdef __ARMEL__
    QString Version::OSVersionPath = "/root/version.txt";
#else
    QString Version::OSVersionPath = "/home/techswarm/version.txt";
#endif

QString Version::firmwareVersion = "Unknown";
QString Version::firmwareType = "Unknown";


QString Version::VarioQTVersion()
{
    return "V1.18Beta";
}

QString Version::OSVersion()
{
    QFile ov(OSVersionPath);
    if(!ov.exists())return "V1.0.0";
    if(!ov.open(QIODevice::ReadOnly | QIODevice::Text))return "V1.0.0";
    QString versionString = ov.readLine();
    return versionString;
}

QString Version::getFirmwareVersion()
{
    return firmwareVersion;
}

void Version::setFirmwareVersion(const QString &value)
{
    firmwareVersion = value;
}

QString Version::getFirmwareType()
{
    return firmwareType;
}

void Version::setFirmwareType(const QString &value)
{
    firmwareType = value;
}

Version::Version()
{
}

