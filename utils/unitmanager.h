/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef UNITMANAGER_H
#define UNITMANAGER_H

#include <QObject>

class SISpeed
{
public:
    static QString prefferedUnit();
    static void setPrefferedUnit(QString unit);
    static QStringList unitList();


    SISpeed(float SIUnit);
    void setValue(float SIUnit);
    void setValuePrefferedFormat(float value);
    float getValuePrefferedFormat();
    QString getPrefferedFormat();
    float value();
private:
    float valueSI;
};

class SIVerticalSpeed
{
public:
    static QString prefferedUnit();
    static void setPrefferedUnit(QString unit);
    static QStringList unitList();


    SIVerticalSpeed(float SIUnit);
    void setValue(float SIUnit);
    void setValuePrefferedFormat(float value);
    float getValuePrefferedFormat();
    QString getPrefferedFormat();
    float value();
private:
    float valueSI;
};

class SIHeight
{
public:
    static QString prefferedUnit();
    static void setPrefferedUnit(QString unit);
    static QStringList unitList();


    SIHeight(float SIUnit);
    void setValue(float SIUnit);
    void setValuePrefferedFormat(float value);
    float getValuePrefferedFormat();
    QString getPrefferedFormat();
    float value();
private:
    float valueSI;
};

class SIDistance
{
public:
    static QString prefferedUnit();
    static void setPrefferedUnit(QString unit);
    static QStringList unitList();


    SIDistance(float SIUnit);
    void setValue(float SIUnit);
    void setValuePrefferedFormat(float value);
    float getValuePrefferedFormat();
    QString getPrefferedFormat();
    float value();
private:
    float valueSI;
};

#endif // UNITMANAGER_H
