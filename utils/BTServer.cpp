/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "BTServer.h"
#include "NMEA/NMEAParser.h"

#include <qbluetoothserver.h>
#include <qbluetoothsocket.h>
#include <qbluetoothlocaldevice.h>

#include "utils/BTUtil.h"
#include <QSettings>

static const QLatin1String serviceUuid("e8e10f95-1a70-4b27-9ccf-02010264e9c8");

BTServer::BTServer(QObject *parent)
    :   QObject(parent), rfcommServer(0)
{
    varioPostscale = QSettings().value("BTServerVarioPostscale",1).toInt();
    gpsPostscale = QSettings().value("BTServerGpsPostscale",1).toInt();
}

BTServer::~BTServer()
{
    stopServer();
}

BTServer* BTServer::BTServerFab(QObject *parent)
{
    if(BTUtil::hciDeviceIsAvailable())
    {
        return new BTServer(parent);
    }
    return nullptr;
}

void BTServer::startServer(const QBluetoothAddress& localAdapter)
{
    if (rfcommServer)
        return;

    rfcommServer = new QBluetoothServer(QBluetoothServiceInfo::RfcommProtocol, this);
    connect(rfcommServer, SIGNAL(newConnection()), this, SLOT(clientConnected()));
    bool result = rfcommServer->listen(localAdapter);
    if (!result) {
        qWarning() << "Cannot bind chat server to" << localAdapter.toString();
        return;
    }

    QBluetoothServiceInfo::Sequence classId;

    classId << QVariant::fromValue(QBluetoothUuid(QBluetoothUuid::SerialPort));
    serviceInfo.setAttribute(QBluetoothServiceInfo::BluetoothProfileDescriptorList,
                             classId);

    classId.prepend(QVariant::fromValue(QBluetoothUuid(serviceUuid)));

    serviceInfo.setAttribute(QBluetoothServiceInfo::ServiceClassIds, classId);

    serviceInfo.setAttribute(QBluetoothServiceInfo::ServiceName, tr("Bt Chat Server"));
    serviceInfo.setAttribute(QBluetoothServiceInfo::ServiceDescription,
                             tr("Example bluetooth chat server"));
    serviceInfo.setAttribute(QBluetoothServiceInfo::ServiceProvider, tr("qt-project.org"));

    serviceInfo.setServiceUuid(QBluetoothUuid(serviceUuid));

    QBluetoothServiceInfo::Sequence publicBrowse;
    publicBrowse << QVariant::fromValue(QBluetoothUuid(QBluetoothUuid::PublicBrowseGroup));
    serviceInfo.setAttribute(QBluetoothServiceInfo::BrowseGroupList,
                             publicBrowse);

    QBluetoothServiceInfo::Sequence protocolDescriptorList;
    QBluetoothServiceInfo::Sequence protocol;
    protocol << QVariant::fromValue(QBluetoothUuid(QBluetoothUuid::L2cap));
    protocolDescriptorList.append(QVariant::fromValue(protocol));
    protocol.clear();
    protocol << QVariant::fromValue(QBluetoothUuid(QBluetoothUuid::Rfcomm))
             << QVariant::fromValue(quint8(rfcommServer->serverPort()));
    protocolDescriptorList.append(QVariant::fromValue(protocol));
    serviceInfo.setAttribute(QBluetoothServiceInfo::ProtocolDescriptorList,
                             protocolDescriptorList);

    serviceInfo.registerService(localAdapter);
}

void BTServer::stopServer()
{
    // Unregister service
    serviceInfo.unregisterService();

    // Close sockets
    qDeleteAll(clientSockets);

    // Close server
    delete rfcommServer;
    rfcommServer = 0;
}

void BTServer::sendMessage(const QString &message)
{
    QByteArray text = message.toUtf8();

    foreach (QBluetoothSocket *socket, clientSockets)
        socket->write(text);
}

void BTServer::GPSNMEAReceived(QString msg)
{
    if(clientSockets.count()>0)
    {
        if(msg.startsWith("$GNRMC") && ++gnrmcPostscalerCounter>=gpsPostscale)
        {
            gnrmcPostscalerCounter = 0;
            sendMessage(msg);
        }
        if(msg.startsWith("$GNGGA") && ++gnggaPostscalerCounter>=gpsPostscale)
        {
            gnggaPostscalerCounter = 0;
            sendMessage(msg);
        }
    }
}

double BTServer::getSeaLevel(double pressure, double altitude) {
    return ((double) pressure
            / pow(1.0f - ((double) altitude / 44330.0f), 5.255f));
}

int BTServer::getGpsPostscale() const
{
    return gpsPostscale;
}

int BTServer::getVarioPostscale() const
{
    return varioPostscale;
}

void BTServer::setGpsPostscale(int value)
{
    gpsPostscale = value;
    QSettings().setValue("BTServerGpsPostscale",value);
}

void BTServer::setVarioPostscale(int value)
{
    varioPostscale = value;
    QSettings().setValue("BTServerVarioPostscale",value);
}

void BTServer::setVspeed(int vSpeedCMPS)
{
    if(clientSockets.count()>0)
    {
        if(++varioPostscaleCounter>=varioPostscale)
        {
            varioPostscaleCounter = 0;

            //$POV,E,2.15
            QString msg = "$POV,E,";
            msg+=QString::number((float)vSpeedCMPS/100);

            double pressure = getSeaLevel(1013.25,0-baroHeightMeters);
            msg+=",P,"+QString::number(pressure);

            msg = NmeaParser::addChecksum(msg);
            msg+="\r\n";

            sendMessage(msg);

            msg = "$LK8EX1,"+QString::number(pressure*100)+",";
            msg+=QString::number(baroHeightMeters)+","+QString::number(vSpeedCMPS)+",99,999,";
            msg = NmeaParser::addChecksum(msg);
            msg+="\r\n";
            sendMessage(msg);

            msg = "$LXWP0,N,"+QString::number(taskmh)+","+ QString::number(baroHeightMeters) +",,,,,,,,,,";
            msg = NmeaParser::addChecksum(msg);
            msg+="\r\n";
            sendMessage(msg);
        }
    }
}

void BTServer::setBaroHeight(int baroHeightm)
{
    baroHeightMeters = baroHeightm;
}

void BTServer::setTASkmh(qreal speed)
{
    taskmh = speed;
}

void BTServer::clientConnected()
{
    QBluetoothSocket *socket = rfcommServer->nextPendingConnection();
    if (!socket)
        return;

    connect(socket, SIGNAL(readyRead()), this, SLOT(readSocket()));
    connect(socket, SIGNAL(disconnected()), this, SLOT(clientDisconnected()));
    clientSockets.append(socket);
    emit clientConnected(socket->peerName());
}

void BTServer::clientDisconnected()
{
    QBluetoothSocket *socket = qobject_cast<QBluetoothSocket *>(sender());
    if (!socket)
        return;

    emit clientDisconnected(socket->peerName());

    clientSockets.removeOne(socket);

    socket->deleteLater();
}

void BTServer::readSocket()
{
    QBluetoothSocket *socket = qobject_cast<QBluetoothSocket *>(sender());
    if (!socket)
        return;

    while (socket->canReadLine()) {
        QByteArray line = socket->readLine().trimmed();
        emit messageReceived(socket->peerName(),
                             QString::fromUtf8(line.constData(), line.length()));
    }
}

