/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef UBLOX_COMMANDS_H
#define UBLOX_COMMANDS_H

class SerialConnection;

namespace Ublox {
    void setBaudRate(const SerialConnection &serial, int baudRate);
    void setPoolingRate5Hz(const SerialConnection &serial);
    void setAirplane2G(const SerialConnection &serial);
    void pollTimeMessage(const SerialConnection &serial);
    void performColdStart(const SerialConnection &serial);
}

#endif // UBLOX_COMMANDS_H
