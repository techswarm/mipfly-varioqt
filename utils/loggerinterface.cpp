/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "loggerinterface.h"
#include "VarioQt.h"

LoggerInterface::LoggerInterface(QObject *parent) : QObject(parent)
{
    lsGPS = new LocalServer("MipFlyLoggerServerGPS");
    lsGPS->setParser(VarioQt::instance().getGpsParser());

    lsVario = new LocalServer("MipFlyLoggerServerVario");
    lsVario->setParser(VarioQt::instance().getVarioParser());

    lsComm = new LocalServer("MipFlyLoggerServerCmd");

    timer10s.setInterval(10000);
    connect(&timer10s,SIGNAL(timeout()),this,SLOT(timerTask()));
    timer10s.start();
}

void LoggerInterface::enableReceive()
{
    lsComm->sendMessage("$CRECEN\r\n");
    qDebug()<<"$CRECEN\r\n";
}

void LoggerInterface::disableReceive()
{
    lsComm->sendMessage("$CRECDIS\r\n");
    qDebug()<<"$CRECDIS\r\n";
}

void LoggerInterface::startRec(QString igcName)
{
    QString msg = "$STARTREC,"+igcName+"\r\n";
    lsComm->sendMessage(msg.toLocal8Bit());
    qDebug()<<"$CRECDIS\r\n";
}

void LoggerInterface::stopRec()
{
    lsComm->sendMessage("$STOPREC\r\n");
    qDebug()<<"$STOPREC\r\n";
}

void LoggerInterface::timerTask()
{
    if(!VarioQt::instance().getGpsParser()->getTimeIsValid())
    {
        lsGPS->sendMessage("\r\n\r\n$PUBX,04 \r\n");
        qDebug()<<"$PUBX,04";
    }
}
