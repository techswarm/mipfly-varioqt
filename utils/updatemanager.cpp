/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "updatemanager.h"
#include "QFuture"
#include <QtCore>
#include <QtConcurrent/QtConcurrent>
#include "netutils.h"
#include <QFileInfo>
#include <QFile>
#include "utils/vario_common.h"

UpdateManager::UpdateManager()
{
    connect(&internetExistsWtcher,SIGNAL(finished()),this,SLOT(internetExistsFinished()),Qt::UniqueConnection);
    connect(&grabUpdateWatcher,SIGNAL(finished()),this,SLOT(grabUpdateFinished()),Qt::UniqueConnection);
}


void UpdateManager::checkConnection()
{
    internetExistsFuture = QtConcurrent::run(NetUtils::internetExists);
    internetExistsWtcher.setFuture(internetExistsFuture);
}

void UpdateManager::grabUpdateFiles()
{
    system("rm -r /mnt/mipfly/temp/update/*");
    if(updateToBeta == false)
    {
        grabUpdateFuture = QtConcurrent::run(NetUtils::wget,Vario::host + QString("downloads/directUpdates/update.zip"),QString("/mnt/mipfly/temp/update"));
    }
    else
    {
        grabUpdateFuture = QtConcurrent::run(NetUtils::wget,Vario::host + QString("downloads/directUpdatesBeta/update.zip"),QString("/mnt/mipfly/temp/update"));
    }
    grabUpdateWatcher.setFuture(grabUpdateFuture);
}

void UpdateManager::grabUiUpdateFiles()
{
    system("rm -r /mnt/mipfly/temp/update/*");
    QString downloadUrl = Vario::host + QString("ui/get?id=") + userId + QString("&key=") + userKey;
    grabUpdateFuture = QtConcurrent::run(NetUtils::wgetNamed,downloadUrl,QString("/mnt/mipfly/temp/update"));
    grabUpdateWatcher.setFuture(grabUpdateFuture);
}

void UpdateManager::grabMixerUpdateFiles()
{
    system("rm -r /mnt/mipfly/temp/update/*");
    QString downloadUrl = Vario::host + QString("soundm/get?id=") + userId + QString("&key=") + userKey;
    grabUpdateFuture = QtConcurrent::run(NetUtils::wgetNamed,downloadUrl,QString("/mnt/mipfly/temp/update"));
    grabUpdateWatcher.setFuture(grabUpdateFuture);
}

void UpdateManager::grabOsUpdateFiles()
{
    system("rm -r /mnt/mipfly/temp/update/*");
    grabUpdateFuture = QtConcurrent::run(NetUtils::wget,Vario::host + QString("downloads/directUpdates/qt5.8.tar.xz"),QString("/mnt/mipfly/temp/update"));
    grabUpdateWatcher.setFuture(grabUpdateFuture);
}

void UpdateManager::deploy()
{
    system("pkill MipFlyLogger");
    system("unzip -o -qq /mnt/mipfly/temp/update/update.zip -d /mnt/mipfly/temp/update/");
    system("rm -r /mnt/mipfly/temp/update/update.zip");
    system("mv /mnt/mipfly/temp/update/VarioQt /mnt/mipfly/temp/update/VarioQt.upg");
    system("mv /mnt/mipfly/temp/update/xinitrc /etc/X11/xinit/");

    system("cp -r /mnt/mipfly/temp/update/* .");
    system("rm -r /mnt/mipfly/temp/update/*");
    system("sync");
}

void UpdateManager::deployOs()
{
    system("tar xf /mnt/mipfly/temp/update/qt5.8.tar.xz -C /usr/local/");
    QFile f("/root/version.txt");
    f.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate);
    f.write("V2.0.0");
}

void UpdateManager::deployUi()
{
    system("unzip -o -qq /mnt/mipfly/temp/update/ui.zip -d /mnt/mipfly/temp/update/");
    //system("rm -r ./ui/*");
    system("rm -r /mnt/mipfly/temp/update/update.zip");
    system("rm -r /mnt/mipfly/temp/update/ui.zip");
    system("cp -r /mnt/mipfly/temp/update/* ./ui/");
    system("sync");
}

void UpdateManager::deployMixer()
{
    system("unzip -o -qq /mnt/mipfly/temp/update/ui.zip -d /mnt/mipfly/temp/update/");
    //system("rm -r ./ui/*");
    system("rm -r /mnt/mipfly/temp/update/update.zip");
    system("rm -r /mnt/mipfly/temp/update/ui.zip");
    system("cp -r /mnt/mipfly/temp/update/* ./soundm/");
    system("sync");
}

void UpdateManager::internetExistsFinished()
{
    if(internetExistsFuture.result() == true)
        emit connectionStatusResult(true);
    else
        emit connectionStatusResult(false);
}

void UpdateManager::grabUpdateFinished()
{
    QFileInfo updateFile("/mnt/mipfly/temp/update/update.zip");
    QFileInfo updateOsFile("/mnt/mipfly/temp/update/qt5.8.tar.xz");
    QFileInfo updateUiFile("/mnt/mipfly/temp/update/ui.zip");
    if((updateFile.exists() && updateFile.isFile())
            || (updateOsFile.exists() && updateOsFile.isFile())
            || (updateUiFile.exists() && updateUiFile.isFile()))
    {
        emit grabUpdateResult(true);
    }
    else
    {
        emit grabUpdateResult(false);
    }
}

void UpdateManager::setUserId(const QString &value)
{
    userId = value;
}

void UpdateManager::setUserKey(const QString &value)
{
    userKey = value;
}

bool UpdateManager::getUpdateToBeta() const
{
    return updateToBeta;
}

void UpdateManager::setUpdateToBeta(bool value)
{
    updateToBeta = value;
}
