/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef IGCUPLOADER_H
#define IGCUPLOADER_H

#include <QObject>
#include <QString>
#include "QFuture"
#include <QtCore>
#include <QtConcurrent/QtConcurrent>
#include <QtNetwork>

class IGCUploader : public QObject
{
    Q_OBJECT
public:
    explicit IGCUploader(QObject *parent = nullptr);
    IGCUploader(QString filename, QString userId, QString userKey, QObject *parent = nullptr);

    void setFilename(const QString &value);

    void setUserId(const QString &value);

    void setUserKey(const QString &value);

    void upload();

signals:
    void message(QString msg,QString caption);

public slots:

private:
    bool uploadInProgress = false;
    QFuture<bool> internetExistsFuture;
    QFutureWatcher<bool> internetExistsWtcher;

    QFuture<void> igcUploadFuture;
    QFutureWatcher<void> igcUploadWatcher;

    QString filename;
    QString userId;
    QString userKey;

    QNetworkAccessManager *manager;
    QNetworkRequest request;

    void checkUpload();

private slots:
    void internetExistsFinished();
    void IGCUploadFinished();
    void managerFinished(QNetworkReply *reply);
};

#endif // IGCUPLOADER_H
