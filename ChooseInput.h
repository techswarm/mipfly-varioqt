/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CHOOSEINPUT_H
#define CHOOSEINPUT_H

#include <QDialog>

namespace Ui {
class ChooseInput;
}

class ChooseInput : public QDialog
{
    Q_OBJECT

public:
    explicit ChooseInput(QWidget *parent = 0);
    ~ChooseInput();

    void setValue(QString value);
    void setCaption(QString caption);

    void setChouse1(QString value);
    void setChouse2(QString value);
    void setChouse3(QString value);

    int choice() const;

private:
    Ui::ChooseInput *ui;

    void showEvent(QShowEvent *e) Q_DECL_OVERRIDE;
    void hideEvent(QHideEvent *e) Q_DECL_OVERRIDE;

    int _choice = -1;

private slots:
    void gpioPressed(int btn);
    void gpioLongPressed(int btn);
};

#endif // CHOOSEINPUT_H
