/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GLIDEDETAILS_H
#define GLIDEDETAILS_H

#include <QDialog>
#include <QString>


namespace Ui {
class glideDetails;
}

class glideDetails : public QDialog
{
    Q_OBJECT

public:
    explicit glideDetails(QWidget *parent = 0);
    ~glideDetails();
    void setIgcPath(QString path);

    void gpioPressed(int btn);
    void gpioLongPressed(int btn);

private:
    Ui::glideDetails *ui;
    QString igcPath;
    void showEvent(QShowEvent *e);
    void hideEvent(QHideEvent *e);



protected:
    virtual void keyPressEvent(QKeyEvent *event) Q_DECL_OVERRIDE;
};

#endif // GLIDEDETAILS_H
