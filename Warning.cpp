/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Warning.h"
#include "ui_Warning.h"
#include <QtDebug>
#include <QTimer>
#include "VarioQt.h"
#include "threads/playsoundthread.h"

Warning::Warning(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Warning)
{
    ui->setupUi(this);
    ui->valueBox->setValueAlign(Qt::AlignCenter);
    connect(VarioQt::instance().getMainUI(),SIGNAL(mainGpioPressed(int)),this,SLOT(gpioPressed(int)),Qt::DirectConnection);
    connect(VarioQt::instance().getMainUI(),SIGNAL(mainGpioLongPressed(int)),this,SLOT(gpioLongPressed(int)),Qt::DirectConnection);
}

Warning::~Warning()
{
    delete ui;
}

void Warning::showWarning(QString caption, QString message, int timeout)
{
    Warning *w = new Warning();
    w->setCaption(caption);
    w->setMessage(message);
    w->setTimeout(timeout);
    w->show();
}

void Warning::showWarningSound(QString caption, QString message, int timeout)
{
    Warning *w = new Warning();
    w->setCaption(caption);
    w->setMessage(message);
    w->setTimeout(timeout);
    w->show();
    VarioQt::instance().getVarioSound()->varioSoundPlayFile(QSettings().value(Vario::SETTINGS_ONSND, "resources/sounds/bell.wav").toString().toUtf8().data());
}

void Warning::showWarningSound(QString caption, QString message, int timeout, QString alarmName)
{
    Warning *w = new Warning();
    w->setCaption(caption);
    w->setMessage(message);
    w->setTimeout(timeout);
    w->show();
    VarioQt::instance().getVarioSound()->varioSoundPlayFile(QSettings().value(Vario::SETTINGS_ONSND, "resources/sounds/warning.wav resources/sounds/"+alarmName).toString().toUtf8().data());
}

void Warning::setCaption(QString caption)
{
    ui->valueBox->setCaption(caption);
}

void Warning::setMessage(QString message)
{
    ui->valueBox->setValue(message);
}

void Warning::setTimeout(int seconds)
{
    timeoutSec = seconds;
}

void Warning::showEvent(QShowEvent *event)
{
    QWidget::showEvent(event);
    QTimer::singleShot(timeoutSec * 1000, this, SLOT(timeout()));
    QTimer::singleShot(500, this, SLOT(GPIOActivationTimeout()));
    VarioQt::instance().pushNewWidget(this);
}

void Warning::hideEvent(QHideEvent *event)
{
    QWidget::hideEvent(event);
    VarioQt::instance().deleteLastWidget();
}

void Warning::timeout()
{
    close();
    delete this;
}

void Warning::GPIOActivationTimeout()
{
    ignoreFirstGPIO = false;
}

void Warning::gpioPressed(int btn)
{
    if(ignoreFirstGPIO)
        ignoreFirstGPIO = false;
    else
        close();
}

void Warning::gpioLongPressed(int btn)
{
    if(ignoreFirstGPIO)
        ignoreFirstGPIO = false;
    else
        close();
}
