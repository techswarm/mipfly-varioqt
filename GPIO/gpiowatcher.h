/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GPIOWATCHER_H
#define GPIOWATCHER_H

#include <QObject>
#include "threads/stoppablethread.h"
#include "GPIOpins.h"
#include <QFileSystemWatcher>
#include <qstring.h>
#include <QElapsedTimer>
#include <QTimer>

class GPIOWatcher : public QObject
{
    Q_OBJECT
public:
    GPIOWatcher(int pinNumber, int reportNumber);
    void setThreadToDisable(StoppableThread* th);
    void watch();

private:
    int pinNumber;
    StoppableThread* threadToDisable=nullptr;
    QFileSystemWatcher watcher;
    QTimer eventTimer;
    QElapsedTimer elTimer;
    int reportNumber;

    GPIOPin pin;
signals:
    void pressed(int number);
    void longPressed(int number);

private slots:
    void switchChanged(QString);
    void timerEventSlot();
};

#endif // GPIOWATCHER_H
