/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gpiowatcher.h"
#include <qdebug.h>

GPIOWatcher::GPIOWatcher(int pinNumber, int reportNumber) : pin(pinNumber)
{
    this->pinNumber = pinNumber;
    this->reportNumber = reportNumber;
    eventTimer.setInterval(300);
    connect(&eventTimer,SIGNAL(timeout()),this,SLOT(timerEventSlot()));
}

void GPIOWatcher::setThreadToDisable(StoppableThread *th)
{
    threadToDisable = th;
}

void GPIOWatcher::watch()
{
    pin.exportPin();
    pin.setInterruptBoth();
    watcher.addPath(QString::fromStdString(pin.pathValue()));
    connect(&watcher, SIGNAL(fileChanged(QString)),this, SLOT(switchChanged(QString)),Qt::DirectConnection);
}

void GPIOWatcher::switchChanged(QString str)
{
    //qDebug()<<"Switch changed";
    if(threadToDisable != nullptr)
    {
        threadToDisable->stop();
        threadToDisable = nullptr;
    }
    if(pin.rawRead()>0)
    {
        //button is released
        qDebug()<<"\r\n\r\nbutton is released";
        if(elTimer.elapsed()>20)
        {
            if(eventTimer.isActive())
            {
                emit pressed(reportNumber);
                eventTimer.stop();
            }
        }
        else
        {
            eventTimer.stop();
        }
    }
    else
    {
        //button is pressed
        qDebug()<<"\r\n\r\nbutton is pressed";
        eventTimer.start();
        elTimer.start();
    }
}

void GPIOWatcher::timerEventSlot()
{
    qDebug()<<"Timer event";
    emit longPressed(reportNumber);
    eventTimer.stop();
}
