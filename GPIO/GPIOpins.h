/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GPIOPIN_H
#define GPIOPIN_H

#include <string>
#include <iostream>

class GPIOPin
{
protected:
    int number;

    virtual std::string pathPinBase();

public:
    std::string pathEdge();
    std::string pathValue();
    std::string pathDirection();
    std::string pathExport();

    static const int NOT_AVAILABLE = -1;

    enum Direction {In, Out};

    GPIOPin(int number);
    int getNumber() const;

    bool isExported();

    bool exportPin();
    int read();
    void write(int val);
    bool setDirection(GPIOPin::Direction direction, int val=0);
    bool setInterruptBoth();
    int rawRead();
};

class A20GPIOPin : public GPIOPin
{
    int phNumber;

    std::string pathPinBase();

public:
    A20GPIOPin(int number, int phNumber);


    int getPhNumber() const;

};

void setupGPIOs();

#endif // GPIOPIN_H
