/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "GPIOpins.h"

#include <sstream>
#include <iostream>
#include <fstream>

#include <QThread>

#include "utils/utils.h"

#include <QtDebug>

#ifndef QT_DEBUG
static const std::string SysFS_Base = "/sys/class/gpio";
#else
static const std::string SysFS_Base = "/home/mipi/workspace/simulation";
#endif

static const int DEBOUNCE_DELAY = 5000; // microseconds

GPIOPin::GPIOPin(int number)
{
    this->number = number;
}

std::string GPIOPin::pathPinBase()
{
#if 0
    std::stringstream ss;
    ss << SysFS_Base << "/gpio" << number;
    return ss.str();
#endif
    std::string path = SysFS_Base + std::string("/gpio") + std::to_string(number);
    return path;
}

std::string GPIOPin::pathValue()
{
#if 0
    std::stringstream ss;
    ss << pathPinBase() << "/value";
    return ss.str();
#endif

    return pathPinBase() + std::string("/value");
}

std::string GPIOPin::pathEdge()
{
#if 0
    std::stringstream ss;
    ss << pathPinBase() << "/edge";
    return ss.str();
#endif

    return pathPinBase() + std::string("/edge");
}

std::string GPIOPin::pathDirection()
{
#if 0
    std::stringstream ss;
    ss << pathPinBase() << "/direction";
    return ss.str();
#endif
    return pathPinBase() + std::string("/direction");
}

std::string GPIOPin::pathExport()
{
#if 0
    std::stringstream ss;
    ss << SysFS_Base << "/export";
    return ss.str();
#endif
    return SysFS_Base + std::string("/export");
}

int GPIOPin::getNumber() const
{
    return number;
}

// Utility

bool GPIOPin::isExported()
{
    qDebug() << QString::fromStdString(pathPinBase());
    return dirExists(pathPinBase());
}

// Main GPIO pin functions: export, set direction, read, write

bool GPIOPin::setDirection(GPIOPin::Direction direction, int val) {
    std::ofstream f(pathDirection());
    if (!f) {
        return false;
    }
    if (direction == GPIOPin::Direction::Out) {
        if(val == 1)
            f << "high";
        else
            f << "low";
    } else {
        f << "in";
    }
    return true;
}

bool GPIOPin::setInterruptBoth() {
    std::ofstream f(pathEdge());
    if (!f) {
        return false;
    }
    f << "both";
    return true;
}

bool GPIOPin::exportPin()
{
#ifdef __ARMEL__
    std::ofstream f(pathExport().c_str());

    //f << std::to_string(number);
    f << number;
    f.flush();
    f.close();

    if (!dirExists(pathPinBase())) {
        std::cerr << "Path not created after export: " << pathPinBase() << std::endl;
        return false;
    } else
        return true;
#else
    return true;
#endif
}

#include "utils/vario_common.h"

int GPIOPin::rawRead()
{
#ifdef __ARMEL__
    int val = NOT_AVAILABLE;

//    std::string valuePath = pathValue();
//    const char *filePath = valuePath.c_str();
//    char buf[44];
//    strcpy(buf, pathValue().c_str());

    std::ifstream f;

    switch(number) {
    case Vario::GPIO1:
        f.open("/sys/class/gpio/gpio136/value");
        break;
    case Vario::GPIO2:
        f.open("/sys/class/gpio/gpio137/value");
        break;
    case Vario::GPIO3:
        f.open("/sys/class/gpio/gpio135/value");
        break;
    case Vario::GPIO1_INT:
        f.open("/sys/class/gpio/gpio244/value");
        break;
    case Vario::GPIO2_INT:
        f.open("/sys/class/gpio/gpio243/value");
        break;
    case Vario::GPIO3_INT:
        f.open("/sys/class/gpio/gpio245/value");
        break;
    case Vario::GPIO_POWER:
        f.open("/sys/class/gpio/gpio139/value");
        break;
    }

//    std::ifstream f("/home/mipi/workspace/simulation/gpio1/value");

    if (!f) {
//        std::cerr << "Could not open file: " << pathValue() << std::endl;
        return NOT_AVAILABLE;
    }

//    char buf;
//    f.read(&buf, 1);

    f >> val;


    return val;
#else
    return 1;
#endif
}

int GPIOPin::read()
{
    int highCount = 0;
    int lowCount = 0;

    int state;
        state = rawRead();
        if (state > 0) {
            highCount++;
        } else {
            lowCount++;
        }
    return (highCount > lowCount ? 1 : 0);
}

void GPIOPin::write(int val)
{
    std::ofstream f(pathValue());
    f << val;
    f.close();
}

// A20GPIOPin

std::string A20GPIOPin::pathPinBase()
{
    std::stringstream ss;
    ss << SysFS_Base << "/gpio" << number << "_ph" << phNumber;
    return ss.str();
}

A20GPIOPin::A20GPIOPin(int number, int phNumber) : GPIOPin(number)
{
    this->phNumber = phNumber;
}

int A20GPIOPin::getPhNumber() const
{
    return phNumber;
}

void setupGPIOs()
{
    std::cout << "Export GPIOs and set init value for out" << std::endl;

    GPIOPin soundEnablePin(130);

    if (!soundEnablePin.isExported()) {
        if (!soundEnablePin.exportPin()) {
            std::cerr << "Unable to export GPIO 6 for sound enablement" << std::endl;
        }
    }
    soundEnablePin.setDirection(GPIOPin::Direction::Out);
    soundEnablePin.write(1);

    GPIOPin soundEnablePinLegacy(111);

    if (!soundEnablePinLegacy.isExported()) {
        if (!soundEnablePinLegacy.exportPin()) {
            std::cerr << "Unable to export GPIO 6 for sound enablement" << std::endl;
        }
    }
    soundEnablePinLegacy.setDirection(GPIOPin::Direction::Out);
    soundEnablePinLegacy.write(1);

    GPIOPin powerEnable1(128);
    if (!powerEnable1.isExported()) {
        if (!powerEnable1.exportPin()) {
            std::cerr << "Unable to export GPIO 10 for internal USB power" << std::endl;
        }
    }
    powerEnable1.setDirection(GPIOPin::Direction::Out);
    powerEnable1.write(1);

    GPIOPin powerEnable2(129);
    if (!powerEnable2.isExported()) {
        if (!powerEnable2.exportPin()) {
            std::cerr << "Unable to export GPIO 11 for internal USB power" << std::endl;
        }
    }
    powerEnable2.setDirection(GPIOPin::Direction::Out);
    powerEnable2.write(1);

    GPIOPin gpsEnablePin(132);

    if (!gpsEnablePin.isExported()) {
        if (!gpsEnablePin.exportPin()) {
            std::cerr << "Error exporting GPIO 7 -- GPS enable pin" << std::endl;
        }
    }

    gpsEnablePin.setDirection(GPIOPin::Direction::Out);
    gpsEnablePin.write(1);


    GPIOPin displayEnablePin(138);
    if (!displayEnablePin.isExported()) {
        if (!displayEnablePin.exportPin()) {
            std::cerr << "Error exporting GPIO 8 -- display enable pin" << std::endl;
        }
    }

    displayEnablePin.setDirection(GPIOPin::Direction::Out);
    displayEnablePin.write(1);


    GPIOPin resetMcu(134);
    if (!resetMcu.isExported()) {
        if (!resetMcu.exportPin()) {
            std::cerr << "Error exporting GPIO 5 -- Mcu reset pic" << std::endl;
        }
    }

    resetMcu.setDirection(GPIOPin::Direction::In,1);
}
