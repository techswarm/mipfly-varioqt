/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef COORDINPUT_H
#define COORDINPUT_H

#include <QDialog>
#include "navUtils/coord.hpp"

namespace Ui {
class CoordInput;
}

class CoordInput : public QDialog
{
    Q_OBJECT

    Coord::CoordInputFormat format=Coord::Decimal;

public:
    explicit CoordInput(QDialog *parent = 0);
    ~CoordInput();

    enum CoordType
    {
        LAT,
        LON
    };
    CoordInput::CoordType type() const;
    void setType(const CoordInput::CoordType &type);

    void setValue(float value);

    float value() const;

private:
    Ui::CoordInput *ui;
    void showEvent(QShowEvent *e) Q_DECL_OVERRIDE;
    void hideEvent(QHideEvent *e) Q_DECL_OVERRIDE;

    CoordInput::CoordType _type = CoordType::LAT;

    int virtualCursorPoint = 0;

    float _value = 0;
    int sign = 1;

    float clampValue(float v);

    void advanceVirtualPointer();
    void reverseVirtualPointer();

public slots:
    void gpioPressed(int btn);
    void gpioLongPressed(int btn);

};

#endif // COORDINPUT_H
