#-------------------------------------------------
#
# Project created by QtCreator 2017-01-09T15:32:20
#
#-------------------------------------------------

QT       += core gui bluetooth network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets network sql core

TARGET = VarioQt
TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++11

#QMAKE_CXXFLAGS+=-Werror
QMAKE_CXXFLAGS+=-Wno-missing-field-initializers
QMAKE_CXXFLAGS+=-Wno-unused-parameter
QMAKE_CXXFLAGS+=-Wno-int-to-pointer-cast
QMAKE_CXXFLAGS+=-Wno-parentheses
QMAKE_CXXFLAGS+=-Wno-unused-result

DEFINES += FAKE_GPS
#DEFINES += FAKE_CONDOR
DEFINES += FAKE_SOUND

DEFINES += USE_SOUND

QMAKE_CXXFLAGS_DEBUG += -O0
CONFIG += debug

SOURCES += main.cpp\
    mainui.cpp \
    varioscale.cpp \
    compass.cpp \
    serial/serial.cpp \
    serial/serialconnection.cpp \
    threads/gpsthread.cpp \
    threads/stoppablethread.cpp \
    NMEA/NMEAParser.cpp \
    utils/utils.cpp \
    Geo/Geo.cpp \
    navUtils/Wind.cpp \
    navUtils/glide.cpp \
    navUtils/coord.cpp \
    circRegression/circle.cpp \
    circRegression/CircleFitByLevenbergMarquardtReduced.cpp \
    circRegression/data.cpp \
    circRegression/Utilities.cpp \
    threads/sendcorrectionsthread.cpp \
    threads/takeoffthread.cpp \
    IGC/IGCFile.cpp \
    IGC/IGCFileHeader.cpp \
    IGC/IGCWriter.cpp \
    threads/igcthread.cpp \
    threads/playsoundthread.cpp \
    navUtils/variocompute.cpp \
    letterchooser.cpp \
    GPIO/GPIOpins.cpp \
    threads/gpiolistenthread.cpp \
    settings/glidemanager.cpp \
    settings/namemanager.cpp \
    settings/samplesmanager.cpp \
    settings/tzmanager.cpp \
    settings/windmanager.cpp \
    utils/datetimemanager.cpp \
    utils/ublox_commands.cpp \
    utils/vario_common.cpp \
    testing/printserial2thread.cpp \
    testing/sendstuffthread.cpp \
    navUtils/headingcorrector.cpp \
    threads/fb2spithread.cpp \
    fbfmwk/fbfmwk.c \
    ui/captionbox.cpp \
    ui/unitbox.cpp \
    utils/cstgraphics.cpp \
    fonts/Arial14h.c \
    fonts/Arial15h.c \
    fonts/Arial16h.c \
    fonts/Arial18h.c \
    fonts/Arial19h.c \
    fonts/Arial22h.c \
    fonts/Arial24h.c \
    fonts/Arial29h.c \
    fonts/Arial32h.c \
    fonts/Arial34h.c \
    fonts/Arial37h.c \
    ui/labelbox.cpp \
    ui/borderbox.cpp \
    ui/slider.cpp \
    ui/battery.cpp \
    ui/satelliteStatus.cpp \
    ui/satelliteStatusPage.cpp \
    utils/variopower.cpp \
    utils/qfonts.cpp \
    testing/gpioemulator.cpp \
    ui/menulistview.cpp \
    ui/inputlocation.cpp \
    settings/locationmanager.cpp \
    Geo/locationsservice.cpp \
    stats/glidesmanager.cpp \
    utils/microcontroller.cpp \
    utils/init.cpp \
    libQDeviceWatcher/qdevicewatcher.cpp \
    libQDeviceWatcher/qdevicewatcher_linux.cpp \
    ui/gpiodialog.cpp \
    threads/locationsthread.cpp \
    Geo/locationsthreadmgr.cpp \
    ui/thermalcenteringui.cpp \
    IGC/igcparser.cpp \
    ui/flightplotui.cpp \
    navUtils/lastlift.cpp \
    ui/dynamicui.cpp \
    navUtils/OpenAirParser.cpp \
    ui/AirSpacePlot.cpp \
    xcsoar/Math/Angle.cpp \
    ui/VerticalPlot.cpp \
    GlideDetails.cpp \
    globals.cpp \
    VarioQt.cpp \
    MenuContainer.cpp \
    menu/mainmenu.cpp \
    menu/menulistmodel.cpp \
    menudialog.cpp \
    menu/name/inputnamemenu.cpp \
    menu/location/locationsmenu.cpp \
    menu/location/locdeletemenu.cpp \
    menu/location/loceditaltmenu.cpp \
    menu/location/loceditinputmenu.cpp \
    menu/location/loceditlatmenu.cpp \
    menu/location/loceditlonmenu.cpp \
    menu/location/loceditmenu.cpp \
    menu/location/loclistmenu.cpp \
    menu/location/locsetcrtmenu.cpp \
    menu/inputboxmenu.cpp \
    menu/glides/glidesmenu.cpp \
    menu/timezone/timezonemenu.cpp \
    menu/menuutils.cpp \
    menu/numSamples/setnumsamplemenu.cpp \
    menu/varioAverage/varioaveragemenu.cpp \
    menu/wind/windaveragemenu.cpp \
    menu/wind/winddspreadmenu.cpp \
    menu/wind/windmenu.cpp \
    menu/wind/windreadingsmenu.cpp \
    menu/glideAverage/glidemenu.cpp \
    bootloader/bootloadermanager.cpp \
    bootloader/flashbootloader.cpp \
    menu/bootloader/bootloadermenu.cpp \
    ui/usbdialog.cpp \
    ui/usblistview.cpp \
    menu/usbDialog/usbdialogmenu.cpp \
    menu/usbDialog/usblistmenu.cpp \
    menu/sound/soundMenu.cpp \
    menu/sound/volumemenu.cpp \
    sound/varioMixer.cpp \
    sound/varioSound.cpp \
    threads/inputThread.cpp \
    NumericInput.cpp \
    maps/MapUtils.cpp \
    ui/MapPlot.cpp \
    menu/navigation/navigationMenu.cpp \
    menu/navigation/navigationAirspace.cpp \
    menu/navigation/navigationMap.cpp \
    menu/system/systemMenu.cpp \
    menu/general/generalMenu.cpp \
    menu/avionics/avionicsMenu.cpp \
    menu/general/languageMenu.cpp \
    menu/general/connectivityMenu.cpp \
    menu/general/WiFiDevice.cpp \
    navUtils/distance.cpp \
    threads/AirSpaceThread.cpp \
    navUtils/elevation.cpp \
    Warning.cpp \
    utils/PowerOffManager.cpp \
    utils/Version.cpp \
    threads/SystemThread.cpp \
    menu/general/remoteMenu.cpp \
    threads/ADBTCPThread.cpp \
    menu/general/BTDevice.cpp \
    menu/general/BTDeviceAction.cpp \
    threads/BTRemoteConn.cpp \
    utils/BTServer.cpp \
    menu/shutdown/shutdownmenu.cpp \
    menu/wind/windrmserrormenu.cpp \
    menu/sound/mixermenu.cpp \
    utils/BTUtil.cpp \
    utils/netutils.cpp \
    utils/updatemanager.cpp \
    navUtils/livetrack.cpp \
    NumericInputDecimal.cpp \
    utils/AccountConnectionManager.cpp \
    ui/magicbox.cpp \
    ui/gpswidget.cpp \
    ui/recordwidget.cpp \
    utils/netstats.cpp \
    ui/bnepwidget.cpp \
    navUtils/weatherstation.cpp \
    navUtils/metartaf.cpp \
    ui/weatherwidget.cpp \
    navUtils/crosssectionpoints.cpp \
    ui/verticalcut.cpp \
    navUtils/hgtcontainer.cpp \
    ui/cutplot.cpp \
    CoordInput.cpp \
    TextInput.cpp \
    TimeInput.cpp \
    navWaypoint/waypoint.cpp \
    navWaypoint/waypointManager.cpp \
    menu/task/taskMenu.cpp \
    menu/task/waypointMenu.cpp \
    menu/task/waypointEdit.cpp \
    menu/task/taskEditMenu.cpp \
    menu/task/taskTypeMenu.cpp \
    navWaypoint/task.cpp \
    menu/task/timeGatesMenu.cpp \
    navWaypoint/turnpoint.cpp \
    menu/task/turnpointMenu.cpp \
    menu/task/waypointSelectMenu.cpp \
    menu/task/turnpointEditMenu.cpp \
    menu/task/goalTypeMenu.cpp \
    utils/fileUtils.cpp \
    threads/obexThread.cpp \
    ui/taskPlot.cpp \
    ChooseInput.cpp \
    menu/task/waypointLoad.cpp \
    navWaypoint/turnpointBoundary.cpp \
    navWaypoint/boundaryFoundry.cpp \
    navWaypoint/taskOptimiser.cpp \
    utils/glider.cpp \
    navUtils/e6b.cpp \
    utils/igcuploader.cpp \
    utils/unitconverter.cpp \
    navUtils/termalingDetector.cpp \
    menu/avionics/thermalingDetectorMenu.cpp \
    menu/general/uiMenu.cpp \
    GPIO/gpiowatcher.cpp \
    menu/task/taskNavigationMenu.cpp \
    menu/task/taskNavigationMenuTp.cpp \
    menu/task/taskNavigationMenuWp.cpp \
    navUtils/faiassistcompute.cpp \
    menu/task/taskNavigationMenuZoom.cpp \
    ui/uiElements/mapscaleindicator.cpp \
    ui/trianglePlot.cpp \
    utils/ssid.cpp \
    NMEA/rawlogger.cpp \
    menu/task/taskImportMenu.cpp \
    navWaypoint/WptFileHandler.cpp \
    utils/unitmanager.cpp \
    menu/general/localizationMenu.cpp \
    utils/localserver.cpp \
    utils/loggerinterface.cpp \
    menu/task/faiNavigationMenu.cpp \
    threads/flightlogthread.cpp \
    flightlog/flightsummarylog.cpp \
    flightlog/flightsummary.cpp \
    LK8000OLC/ContestMgr.cpp \
    LK8000OLC/Trace.cpp \
    LK8000OLC/Tools.cpp \
    LK8000OLC/Utils.cpp \
    LK8000OLC/olc.cpp \
    sound/variomixerfile.cpp \
    menu/sound/mixerMenuSelect.cpp \
    flightlog/flightturnpoint.cpp \
    menu/recovery/recoverymenu.cpp

HEADERS  += mainui.h \
    varioscale.h \
    compass.h \
    serial/serial.h \
    serial/serialconnection.h \
    threads/gpsthread.h \
    threads/stoppablethread.h \
    NMEA/NMEAParser.h \
    utils/utils.h \
    navUtils/navUtils.hpp \
    Geo/Geo.hpp \
    circRegression/data.h \
    circRegression/Utilities.h \
    circRegression/mystuff.h \
    circRegression/circle.h \
    navUtils/glide.hpp \
    threads/sendcorrectionsthread.h \
    threads/takeoffthread.h \
    IGC/IGCFile.hpp \
    threads/igcthread.h \
    threads/playsoundthread.h \
    navUtils/Wind.hpp \
    navUtils/variocompute.h \
    letterchooser.h \
    GPIO/GPIOpins.h \
    threads/gpiolistenthread.h \
    settings/glidemanager.h \
    settings/namemanager.h \
    settings/samplesmanager.h \
    settings/tzmanager.h \
    settings/windmanager.h \
    utils/datetimemanager.h \
    utils/ublox_commands.h \
    utils/vario_common.h \
    testing/printserial2thread.h \
    testing/sendstuffthread.h \
    navUtils/headingcorrector.h \
    threads/fb2spithread.h \
    fbfmwk/fbfmwk.h \
    utils/spidev.h \
    ui/captionbox.h \
    utils/cstgraphics.h \
    ui/unitbox.h \
    utils/fonttables.h \
    ui/labelbox.h \
    ui/borderbox.h \
    ui/slider.h \	
    ui/battery.h \
    ui/satelliteStatus.h \
    ui/satelliteStatusPage.h \
    utils/variopower.h \
    utils/qfonts.h \
    testing/gpioemulator.h \
    ui/menulistview.h \
    ui/inputlocation.h \
    settings/locationmanager.h \
    Geo/locationsservice.h \
    stats/glidesmanager.h \
    utils/microcontroller.h \
    utils/init.h \
    libQDeviceWatcher/qdevicewatcher.h \
    libQDeviceWatcher/qdevicewatcher_p.h \
    ui/gpiodialog.h \
    IGC/IGCWriter.h \
    threads/locationsthread.h \
    Geo/locationsthreadmgr.h \
    ui/thermalcenteringui.h \
    IGC/igcparser.h \
    ui/flightplotui.h \
    navUtils/lastlift.h \
    ui/dynamicui.h \
    navUtils/OpenAirParser.h \
    ui/AirSpacePlot.h \
    xcsoar/Math/Angle.hpp \
    xcsoar/Compiler.h \
    xcsoar/Math/Trig.hpp \
    xcsoar/Math/FastTrig.hpp \
    xcsoar/Math/Constants.hpp \
    ui/VerticalPlot.h \
    GlideDetails.h \
    globals.h \
    VarioQt.h \
    MenuContainer.h \
    menu/mainmenu.h \
    menu/menulistmodel.h \
    menudialog.h \
    menu/name/inputnamemenu.h \
    menu/location/locationsmenu.h \
    menu/location/locdeletemenu.h \
    menu/location/loceditaltmenu.h \
    menu/location/loceditinputmenu.h \
    menu/location/loceditlatmenu.h \
    menu/location/loceditlonmenu.h \
    menu/location/loceditmenu.h \
    menu/location/loclistmenu.h \
    menu/location/locsetcrtmenu.h \
    menu/inputboxmenu.h \
    menu/glides/glidesmenu.h \
    menu/timezone/timezonemenu.h \
    menu/menuutils.h \
    menu/numSamples/setnumsamplemenu.h \
    menu/varioAverage/varioaveragemenu.h \
    menu/wind/windaveragemenu.h \
    menu/wind/winddspreadmenu.h \
    menu/wind/windmenu.h \
    menu/wind/windreadingsmenu.h \
    menu/glideAverage/glidemenu.h \
    bootloader/bootloadermanager.h \
    bootloader/flashbootloader.h \
    menu/bootloader/bootloadermenu.h \
    ui/usbdialog.h \
    ui/usblistview.h \
    menu/usbDialog/usbdialogmenu.h \
    menu/usbDialog/usblistmenu.h \
    menu/sound/soundMenu.h \
    menu/sound/volumemenu.h \
    sound/varioMixer.h \
    sound/varioSound.h \
    threads/inputThread.h \
    NumericInput.h \
    maps/MapUtils.h \
    ui/MapPlot.h \
    menu/navigation/navigationMenu.h \
    menu/navigation/navigationAirspace.h \
    menu/navigation/navigationMap.h \
    menu/system/systemMenu.h \
    menu/general/generalMenu.h \
    menu/avionics/avionicsMenu.h \
    menu/general/languageMenu.h \
    menu/general/connectivityMenu.h \
    menu/general/WiFiDevice.h \
    navUtils/distance.h \
    threads/AirSpaceThread.h \
    navUtils/elevation.h \
    Warning.h \
    utils/PowerOffManager.h \
    utils/Version.h \
    threads/SystemThread.h \
    menu/general/remoteMenu.h \
    threads/ADBTCPThread.h \
    menu/general/BTDevice.h \
    menu/general/BTDeviceAction.h \
    threads/BTRemoteConn.h \
    utils/BTServer.h \
    menu/shutdown/shutdownmenu.h \
    menu/wind/windrmserrormenu.h \
    menu/sound/mixermenu.h \
    utils/BTUtil.h \
    utils/netutils.h \
    utils/updatemanager.h \
    navUtils/livetrack.h \
    NumericInputDecimal.h \
    utils/AccountConnectionManager.h \
    ui/magicbox.h \
    ui/gpswidget.h \
    ui/recordwidget.h \
    utils/netstats.h \
    ui/bnepwidget.h \
    navUtils/weatherstation.h \
    navUtils/metartaf.h \
    ui/weatherwidget.h \
    navUtils/crosssectionpoints.h \
    ui/verticalcut.h \
    navUtils/hgtcontainer.h \
    ui/cutplot.h \
    CoordInput.h \
    TextInput.h \
    TimeInput.h \
    navWaypoint/waypoint.h \
    navWaypoint/waypointManager.h \
    menu/task/taskMenu.h \
    menu/task/waypointMenu.h \
    menu/task/waypointEdit.h \
    menu/task/taskEditMenu.h \
    menu/task/taskTypeMenu.h \
    navWaypoint/task.h \
    menu/task/timeGatesMenu.h \
    navWaypoint/turnpoint.h \
    menu/task/turnpointMenu.h \
    menu/task/waypointSelectMenu.h \
    menu/task/turnpointEditMenu.h \
    menu/task/goalTypeMenu.h \
    utils/fileUtils.h \
    threads/obexThread.h \
    ui/taskPlot.h \
    ChooseInput.h \
    menu/task/waypointLoad.h \
    navWaypoint/turnpointBoundary.h \
    navWaypoint/boundaryFoundry.h \
    navWaypoint/taskOptimiser.h \
    utils/glider.h \
    navUtils/e6b.h \
    utils/igcuploader.h \
    IGC/sign-xmi-lib.h \
    utils/unitconverter.h \
    navUtils/termalingDetector.h \
    menu/avionics/thermalingDetectorMenu.h \
    menu/general/uiMenu.h \
    GPIO/gpiowatcher.h \
    menu/task/taskNavigationMenu.h \
    menu/task/taskNavigationMenuTp.h \
    menu/task/taskNavigationMenuWp.h \
    navUtils/faiassistcompute.h \
    menu/task/taskNavigationMenuZoom.h \
    ui/uiElements/mapscaleindicator.h \
    ui/trianglePlot.h \
    utils/ssid.h \
    NMEA/rawlogger.h \
    menu/task/taskImportMenu.h \
    utils/unitmanager.h \
    menu/general/localizationMenu.h \
    utils/loggerinterface.h \
    utils/localserver.h \
    menu/task/faiNavigationMenu.h \
    threads/flightlogthread.h \
    flightlog/flightsummarylog.h \
    flightlog/flightsummary.h \
    LK8000OLC/ContestMgr.h \
    LK8000OLC/Trace.h \
    LK8000OLC/PointGPS.h \
    LK8000OLC/Tools.h \
    LK8000OLC/Utils.h \
    LK8000OLC/olc.h \
    sound/variomixerfile.h \
    menu/sound/mixerMenuSelect.h \
    flightlog/flightturnpoint.h \
    menu/recovery/recoverymenu.h

FORMS    += mainui.ui \
    varioscale.ui \
    compass.ui \
    letterchooser.ui \
    ui/captionbox.ui \
    ui/inputlocation.ui \
    ui/usbdialog.ui \
    GlideDetails.ui \
    MenuContainer.ui \
    menudialog.ui \
    NumericInput.ui \
    Warning.ui \
    NumericInputDecimal.ui \
    CoordInput.ui \
    TextInput.ui \
    TimeInput.ui \
    ChooseInput.ui

    unix:!macx: LIBS += -lasound -lcrypto

# Copy resource dir to compile destination

copydata.commands = $(COPY_DIR) $$PWD/resources $$OUT_PWD
first.depends = $(first) copydata
export(first.depends)
export(copydata.commands)
QMAKE_EXTRA_TARGETS += first copydata

SUBDIRS += \
    VarioQt_Release.pro \
    VarioQt_Debug.pro \
    VarioQt_Release.pro

RESOURCES +=

TRANSLATIONS = french.ts swedish.ts italian.ts german.ts turkish.ts
