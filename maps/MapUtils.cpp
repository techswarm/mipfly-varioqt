/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "MapUtils.h"
#include <math.h>

MapUtils::MapUtils()
{

}

int MapUtils::long2TileX(double lon, int z)
{
    return (int)(floor((lon + 180.0) / 360.0 * pow(2.0, z)));
}

int MapUtils::lat2TileY(double lat, int z)
{
    return (int)(floor((1.0 - log( tan(lat * M_PI/180.0) + 1.0 / cos(lat * M_PI/180.0)) / M_PI) / 2.0 * pow(2.0, z)));
}


int MapUtils::long2TilePixelX(double lon, int z)
{
    double f=(lon + 180.0) / 360.0 * pow(2.0, z);
    double fraction = f - floor(f);
    return (int)(floor(fraction * 256));
}

int MapUtils::lat2TilePixelY(double lat, int z)
{
    double f=(1.0 - log( tan(lat * M_PI/180.0) + 1.0 / cos(lat * M_PI/180.0)) / M_PI) / 2.0 * pow(2.0, z);
    double fraction = f - floor(f);
    return (int)(floor(fraction * 256));
}

double MapUtils::tileX2Long(int x, int z)
{
    return x / pow(2.0, z) * 360.0 - 180;
}

double MapUtils::tileY2Lat(int y, int z)
{
    double n = M_PI - 2.0 * M_PI * y / pow(2.0, z);
    return 180.0 / M_PI * atan(0.5 * (exp(n) - exp(-n)));
}
