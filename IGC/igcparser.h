/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef IGCPARSER_H
#define IGCPARSER_H

#include <qstring.h>
#include <qfile.h>
#include "navUtils/coord.hpp"
#include <QTime>
#include <QList>
#include <QString>

typedef struct
{
    QTime snapTime;
    Coord coordonates;
    int altitude;
}igcLogEntry;

class igcParser
{
private:
    QFile igcFile;

public:
    igcParser(const QString& igcPath);
    igcParser();
    void resetHead();
    bool getRecord(QTime &snapTime, Coord &coordonates, int &altitude);
    ~igcParser();
    QList<igcLogEntry> igcLogEntrys;
    void parse();
    double latMin, latMax, lonMin, lonMax;
    void setPath(QString path);
};

#endif // IGCPARSER_H
