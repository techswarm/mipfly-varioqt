/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "IGCWriter.h"

#include <sstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>
#include <cstdio>
#include <iomanip>

#include <QtDebug>
#include <QSettings>
#include <QFile>
#include <QTextStream>
#include <QThread>

#include "utils/utils.h"
#include "utils/vario_common.h"
#include "Geo/locationsservice.h"

static const std::string BASEPATH (".");

inline static char fmtLatCard(const Latitude& lat) {
    return lat.card() == Latitude::Cardinal::NORTH ? 'N' : 'S';
}

inline static char fmtLonCard(const Longitude& lon) {
    return lon.card() == Longitude::Cardinal::EAST ? 'E' : 'W';
}

static std::string timestamp() {
    time_t now = time(NULL);
    struct tm *time = gmtime(&now);
    char buff[7];
    sprintf(buff, "%.2d%.2d%.2d", time->tm_hour, time->tm_min, time->tm_sec);
    return std::string(buff);
}

// 2 int digits 3 decimal digits, no separator point, 0-padded
static std::string formatMinutes(double minutes) {
    std::ostringstream ss;
    ss << std::fixed << std::setprecision(3);
    ss << std::setfill('0') << std::setw(6);
    ss << minutes;

    std::string str = ss.str();

    str.erase(str.find("."), 1);

    return str;
}

static std::string filenameDate() {
    time_t now = time(NULL);
    struct tm *time = localtime(&now);
    char buff[11];
    sprintf(buff, "%.4d-%.2d-%.2d", time->tm_year + 1900, time->tm_mon + 1, time->tm_mday);
    return std::string(buff);
}

static std::string formatAlt(int alt) {
    std::ostringstream ss;
    //ss << std::setfill('0') << std::setw(5) << alt;
    ss << std::setw(5) << std::setfill('0') << std::internal << alt;
    return ss.str();
}

// IGCWriter
void IGCWriter::newFlight(const IGCFile& file) {
    igcf = file;
    crtDir = timestampStr();

    createDirIfNotExists();

    if (f.is_open()) {
        f.close();
    }

    evalFlightNumber();

    std::cout << "Log path: " << getFilePath().c_str() << std::endl;
    fName = QString(getFilePath().c_str());
    f.open(fName.toStdString());

    if (!f) {
        std::cerr << "error opening IGC file for writing: " << getFilePath().c_str() << std::endl;
        perror("ostream::open ");
    } else {
        f << igcf;
    }
}

QString IGCWriter::getFName() const
{
    return fName;
}

void IGCWriter::closeFile()
{
    if (f.is_open()) {
        f.close();
    }
}

std::__cxx11::string IGCWriter::getFilePath() {
    std::ostringstream ss;
    ss << BASEPATH << "/flights/" << crtDir << '/' << filenameDate();

    if (QSettings().value(Vario::SETTINGS_IGCSTDNM, false).toBool()) {
        ss << "-" << A_UID << "-";
        QString pilotName;
        pilotName = QSettings().value(Vario::SETTINGS_PILOT,"No Name").toString();
        pilotName = pilotName.simplified();
        pilotName = pilotName.toUpper();
        pilotName = pilotName.replace(" ","_");
        ss << pilotName.toStdString();
    } else {
        const Vario::Location &crtLoc = LocationsService().currentLocation();
        ss << '-';
        if (!crtLoc.name.isEmpty()) {
            QByteArray ba = crtLoc.name.toLocal8Bit();
            ss << ba.data();
        } else {
            ss << "NO-LOCATION";
        }
    }

    ss << '-' << getFlightNumberStr() << ".IGC";

    return ss.str();
}

/*

B083513 5755161N01146760E A 00161 00000 132 012
B083514 5755161N01146760E A 00160 000-1 132 012
B083515 5755161N01146760E A 00160 000-1 132 012
B083517 5755161N01146761E A 00160 00000 132 012

*/

void IGCWriter::append(Latitude lat, Longitude lon, int altPressure, int altGPS, int satellitesUsed, int hdop, int fixType,std::string GPSTimestamp) {
    if (f) {
        char fixTypeChar = 'A';
        if(fixType!=3)
        {
            fixTypeChar = 'V';
            GPSTimestamp = timestamp();
        }
        f << 'B' << GPSTimestamp;
        f << std::setfill('0') << std::setw(2) << lat.deg() << formatMinutes(lat.decMin()) << fmtLatCard(lat);
        f << std::setfill('0') << std::setw(3) << lon.deg() << formatMinutes(lon.decMin()) << fmtLonCard(lon);
        f << fixTypeChar << formatAlt(altPressure) << formatAlt(altGPS);
        f << std::setfill('0') << std::setw(3) << 2 * hdop;
        f << std::setfill('0') << std::setw(3) << satellitesUsed;
        f << "\r\n";
        flushPostscaler++;
        if(flushPostscaler > 50)
        {
            flushPostscaler = 0;
            f.flush();
        }
    } else {
        std::cerr << "error trying to write to file: " << getFilePath() << std::endl;
        perror("ostream::operator<< ");
    }
}

void IGCWriter::writeLine(std::string line) {
    if (f) {
        f << line;
        flushPostscaler++;
        if(flushPostscaler > 50)
        {
            flushPostscaler = 0;
            f.flush();
        }
    } else {
        std::cerr << "error trying to write to file: " << getFilePath() << std::endl;
        perror("ostream::operator<< ");
    }
}

std::string IGCWriter::returnLine(Latitude lat, Longitude lon, int altPressure, int altGPS, int satellitesUsed, int hdop) {

    std::string line;

    line = 'B' + timestamp();

    std::ostringstream ss;
    ss << std::fixed << std::setprecision(0) << std::setfill('0') << std::setw(2);
    ss << lat.deg();

    std::string lat_deg = ss.str();

    line += lat_deg + formatMinutes(lat.decMin()) + fmtLatCard(lat);

    std::ostringstream ssa;
    ssa << std::fixed << std::setprecision(0);
    ssa << std::setfill('0') << std::setw(3);
    ssa << lon.deg();

    std::string lon_deg = ssa.str();

    line += lon_deg + formatMinutes(lon.decMin()) + fmtLonCard(lon);
    line += 'A' + formatAlt(altPressure) + formatAlt(altGPS);

    std::ostringstream ssb;
    ssb << std::fixed << std::setprecision(0);
    ssb << std::setfill('0') << std::setw(3);
    ssb << 2 * hdop;

    std::string hdop_str = ssb.str();

    line += hdop_str;

    std::ostringstream ssc;
    ssc << std::fixed << std::setprecision(0);
    ssc << std::setfill('0') << std::setw(3);
    ssc << satellitesUsed;

    std::string satUsed_str = ssc.str();

    line += satUsed_str;
    line += "\r\n";

    return line;
}

void IGCWriter::flushStream()
{
    if (f) {
        f.flush();
        system("sync");
    }
}

void IGCWriter::copyFlightWithDetails(std::__cxx11::string locName, double duration)
{
    if (f.is_open()) {
        f.close();
    }

    const char *targetName = "TEMPFILE.IGC";
    const char *targetNameG = "TEMPFILE.IGC.g";


    std::ifstream src(fName.toStdString());
    std::ofstream dest(targetName);
    QFile error;
    error.setFileName("igcerror.log");
    error.open(QIODevice::WriteOnly | QIODevice::Append);
    if(!error.isOpen())return;
    QTextStream errorStream( &error );

    if (!src) {
        std::cerr << "Error opening " << fName.toStdString().c_str() << " for reading" << std::endl;
        errorStream << "Error opening " << QString(fName.toStdString().c_str()) << " for reading\r\n";
        error.close();
        return;
    }

    if (!dest) {
        std::cerr << "Error opening " << targetName << " for writing" << std::endl;
        errorStream << "Error opening " << targetName << " for writing \r\n";
        error.close();
        return;
    }


    std::string line;
    bool inserted = false;

    while (std::getline(src, line)) {
        if (line[0] == 'B' && !inserted) {
            dest << "LXMI LOC " << locName << "\r\n";
            dest << "LXMI DUR " << duration << "\r\n";
            inserted = true;
        }
        dest << line << "\n";
    }

    dest << std::flush;

    src.close();
    dest.close();
    error.close();

    //remnents of the
    system("cp TEMPFILE.IGC TEMPFILE.IGC.g");

    if (rename(targetNameG, fName.toStdString().c_str()) < 0) {
        std::cerr << "Rename (move) error: " << targetName << " -> " << fName.toStdString().c_str() << std::endl;
    }
}

void IGCWriter::createDirIfNotExists() {
    std::ostringstream ss;
    ss << BASEPATH << "/flights";

    if (mkdirIfNotExists(ss.str()) < 0) {
        char *errnoDesc = strerror(errno);
        qCritical("error creating directory %s (%s)", ss.str().c_str(), errnoDesc);
    }

    if (mkdirIfNotExists(ss.str()) < 0) {
        char *errnoDesc = strerror(errno);
        qCritical("error creating directory %s (%s)", ss.str().c_str(), errnoDesc);
    }

    ss << '/' << crtDir;

    if (mkdirIfNotExists(ss.str()) < 0) {
        char *errnoDesc = strerror(errno);
        qCritical("error creating directory %s (%s)", ss.str().c_str(), errnoDesc);
    }
}

std::string IGCWriter::getFlightNumberStr() {
    if (flightNumber <= 9 || flightNumber > 35)
        return std::to_string(flightNumber);
    else {
        char c = flightNumber + 55;
        return std::string(&c);
    }
}

void IGCWriter::evalFlightNumber()
{
    flightNumber = 1;
    while(std::ifstream(getFilePath().c_str())) {
        flightNumber++;
    }
}
