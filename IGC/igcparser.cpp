/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "igcparser.h"
#include <QTime>

igcParser::igcParser(const QString& igcPath):igcFile(igcPath)
{
    igcFile.open(QIODevice::ReadOnly|QIODevice::Text);
}

igcParser::igcParser()
{

}

igcParser::~igcParser()
{
    igcFile.close();
}

void igcParser::resetHead()
{
    igcFile.close();
    igcFile.open(QIODevice::ReadOnly|QIODevice::Text);
}

void igcParser::setPath(QString path)
{
    igcFile.setFileName(path);
    igcFile.open(QIODevice::ReadOnly|QIODevice::Text);
}

bool igcParser::getRecord(QTime &snapTime, Coord &coordonates, int &altitude)
{
    while(!igcFile.atEnd())
    {
        QString line(igcFile.readLine());
        if(line[0]!=QChar('B'))continue;

        QString timeStr = line.mid(1,6);
        snapTime = QTime::fromString(timeStr,"hhmmss");

        QString latStr = line.mid(7,2);
        double lat = latStr.toDouble();
        latStr = line.mid(9,2);
        double latMinutes = latStr.toDouble();
        latStr = line.mid(11,3);
        latMinutes += latStr.toDouble()/1000;
        lat += (latMinutes*1.666666667)/100;
        if(line[14]==QChar('S'))lat*=-1;
        coordonates.setLat(lat);


        QString lonStr = line.mid(15,3);
        double lon = lonStr.toDouble();
        lonStr = line.mid(18,2);
        double lonMinutes = lonStr.toDouble();
        lonStr = line.mid(20,3);
        lonMinutes += lonStr.toDouble()/1000;
        lon += (lonMinutes*1.666666667)/100;
        if(line[23]==QChar('V'))lon*=-1;
        coordonates.setLon(lon);

        altitude = line.mid(25,5).toInt();
        return true;

    }
    return false;
}

void igcParser::parse()
{
    QTime recordTime;
    Coord recordCoord;
    int recordAltitude;

    latMax = -1000;
    lonMax = -1000;
    latMin = 1000;
    lonMin = 1000;

    resetHead();

    igcLogEntrys.clear();

    while(getRecord(recordTime,recordCoord,recordAltitude))
    {
        if(recordCoord.getLat()>latMax)latMax=recordCoord.getLat();
        if(recordCoord.getLat()<latMin)latMin=recordCoord.getLat();

        if(recordCoord.getLon()>lonMax)lonMax=recordCoord.getLon();
        if(recordCoord.getLon()<lonMin)lonMin=recordCoord.getLon();

        igcLogEntry ile;
        ile.snapTime = recordTime;
        ile.coordonates = recordCoord;
        ile.altitude = recordAltitude;

        igcLogEntrys.append(ile);
    }

    igcFile.close();
}
