/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "IGCFile.hpp"

#include <iomanip>

const std::string A_UID ("XMI");

// Other constants
static const std::string NOT_RECORDED ("not recorded");

static std::string dateUTC() {
    time_t now = time(NULL);
    struct tm *time = localtime(&now);
    char buff[7];
    int year = (time->tm_year + 1900) % 100 ;
    sprintf(buff, "%.2d%.2d%.2d",  time->tm_mday, time->tm_mon+1, year);
    return std::string(buff);
}

// IGCFile::Header

IGCFile::Header::Header()
    : pilot(NOT_RECORDED),
      secondPilot(NOT_RECORDED),
      gliderType(NOT_RECORDED),
      gliderId(NOT_RECORDED),
      GPSDatum("WGS-1984"),
      manufacturer("Techswarm"),
      flightRecorder("MipFLY VarioQt"),
      gpsRcvManufacturer("uBlox"),
      gpsRcvModel("M8N"),
      pSnsrManufacturer("TEConnectivity"),
      pSnsrModel("MS5611")
{}

std::ostream& operator<< (std::ostream& out, const IGCFile::Header& header) {
    out << std::setfill('0');

    out << "A" << A_UID << "000," << header.flightRecorder << "\r\n";
    out << "HFDTE" << dateUTC() << "\r\n";
    out << "HFPLTPILOT: " << header.pilot << "\r\n";
    out << "HFGTYGLIDERTYPE:" << header.gliderType << "\r\n";
    out << "HFDTM100GPSDATUM:WGS84" << "\r\n";
    out << "HFFXA" << std::setw(3) << header.fixAccuracy << "\r\n";
    out << "HFGIDGLIDERID:" << header.gliderId << "\r\n";
    out << "HFDTM" << std::setw(3) << header.GPSDatumIGCCode << "GPSDATUM: " << header.GPSDatum << "\r\n";
    out << "HFRFWFIRMWAREVERSION: " << header.firmware << "\r\n";
    out << "HFRHWHARDWAREVERSION: " << header.hardware << "\r\n";
    out << "HFFTYFRTYPE: " << header.manufacturer << ',' << header.flightRecorder << "\r\n";
    out << "HFGPS:" << header.gpsRcvManufacturer << ',' << header.gpsRcvModel
        << ',' << header.gpsRcvChannels << ',' << header.gpsRcvMaxAlt << "m\r\n";
    out << "HFPRSPRESSALTSENSOR: " << header.pSnsrManufacturer << ',' << header.pSnsrModel << ','
        << header.pSnsrMaxAlt << "m\r\n";

    if (!header.competitionId.empty()) {
        out << "HFCIDCOMPETITIONID: " << header.competitionId << "\r\n";
    }

    if (!header.competitionClass.empty()) {
        out << "HFCCLCOMPETITIONCLASS: " << header.competitionClass << "\r\n";
    }
    out << "I023638FXA3941SIU\r\n";
    return out;
}
