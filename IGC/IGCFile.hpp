/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __VARIOGUI_IGC_FILE_HPP
#define __VARIOGUI_IGC_FILE_HPP

#include <string>
#include <fstream>

#include <Geo/Geo.hpp>

extern const std::string A_UID;

class IGCFile
{
	struct Header
	{
		int fixAccuracy = 0;
		std::string pilot;
		std::string secondPilot;
		std::string gliderType;
		std::string gliderId;

		int GPSDatumIGCCode = 100;
		std::string GPSDatum;

		float firmware = 0.1;
		float hardware = 0.1;

		std::string manufacturer;
		std::string flightRecorder;

		// GPS Receiver details
		std::string gpsRcvManufacturer;
		std::string gpsRcvModel;
		int gpsRcvChannels = 12;
		int gpsRcvMaxAlt = 10000;	// in meters

		// Pressure sensor details
		std::string pSnsrManufacturer;
		std::string pSnsrModel;
		int pSnsrMaxAlt = 11000;	// in meters

		// Optional fields
		std::string competitionId;
		std::string competitionClass;

		Header();
	};

	Header header;
public:
	void setPilot(std::string name);
	std::string getPilot() const;

	void setCompetitionId(std::string);
	std::string getCompetitionId() const;

	void setCompetitionClass(std::string);
	std::string getCompetitionClass() const;

	friend std::ostream& operator<< (std::ostream&, const IGCFile::Header&);
	friend std::ostream& operator<< (std::ostream&, const IGCFile&);
};

std::ostream& operator<< (std::ostream&, const IGCFile::Header&);
std::ostream& operator<< (std::ostream&, const IGCFile&);

#endif
