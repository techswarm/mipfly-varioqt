/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "IGCFile.hpp"

// IGCFile
void IGCFile::setPilot(std::string name) {
	header.pilot = name;
}

std::string IGCFile::getPilot() const {
	return header.pilot;
}


std::ostream& operator<< (std::ostream& out, const IGCFile& igcf) {
	out << igcf.header;
	return out;
}

void IGCFile::setCompetitionId(std::string id) {
	header.competitionId = id;
}
std::string IGCFile::getCompetitionId() const {
	return header.competitionId;
}

void IGCFile::setCompetitionClass(std::string cls) {
	header.competitionClass = cls;
}
std::string IGCFile::getCompetitionClass() const {
	return header.competitionClass;
}
