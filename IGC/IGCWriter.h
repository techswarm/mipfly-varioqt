/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef IGCWRITER
#define IGCWRITER

#include <fstream>
#include <string>

#include "IGCFile.hpp"
#include <QString>

class IGCWriter
{
    IGCFile igcf;
    std::ofstream f;
    QString fName;


    std::string crtDir;
    int flightNumber = 0;
    int flushPostscaler = 0;

    std::string getFilePath();
    void createDirIfNotExists();
    std::string getFlightNumberStr();

    void evalFlightNumber();

public:
    void newFlight(const IGCFile&);
    void append(Latitude lat, Longitude lon, int altPressure, int altGPS, int satellitesUsed, int hdop, int fixType, std::string GPSTimestamp);
    void flushStream();
    void copyFlightWithDetails(std::string locName, double duration);
    std::string returnLine(Latitude lat, Longitude lon, int altPressure, int altGPS, int satellitesUsed, int hdop);
    void writeLine(std::string line);
    QString getFName() const;
    void closeFile();
};

#endif // IGCWRITER

