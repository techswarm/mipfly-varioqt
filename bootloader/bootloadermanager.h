/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef BOOTLOADERMANAGER_H
#define BOOTLOADERMANAGER_H

#include <QObject>

class MenuDialog;
class GPSThread;

class BootloaderManager : public QObject
{
    Q_OBJECT
public:
    static const QString BOOT_FILENAME;
    static const QString BOOT_FILENAME_EXP;

    BootloaderManager(GPSThread &thread, const MenuDialog &menuDialog);

    int flash(bool flashExperimental);

signals:
    void pause();
    void resume();

private:
    GPSThread &serialThread;
    const MenuDialog &menuDlg;
};

#endif // BOOTLOADERMANAGER_H
