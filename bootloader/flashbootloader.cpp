/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "flashbootloader.h"

#include <QThread>
#include <iostream>
#include <fstream>
#include <QtDebug>

#include <termios.h>
#include <unistd.h>
#include <sys/stat.h>

#include "GPIO/GPIOpins.h"

#include "menudialog.h"
#include "ui_menudialog.h"
#include "serial/serialconnection.h"
#include "threads/gpsthread.h"


#define BAUD_RATE B115200

//#include "xorarray.c"

static const char *FILENAME  = "VarioSTM.bin";
//static const char *SERIAL_PORT = "/dev/ttyS2";

extern uint8_t xorArray[];

int flashBootloader(MenuDialog &menuDlg, GPSThread &serialThread)
{
    menuDlg.getUi().valLabel->setText("FLASH ZA THING");

    return 1;

    const SerialConnection &conn = serialThread.connection();
    //    menuDlg.hSlider->setValue(0);

    GPIOPin resetPin(5);

    if (!resetPin.isExported()) {
        resetPin.exportPin();

    }
    resetPin.setDirection(GPIOPin::Direction::Out);
    std::ifstream f(FILENAME, std::ios::binary);

    if (!f) {
        std::cerr << "Error opening file: " << FILENAME << std::endl;
        return 1;
    }

    // Get file size
    struct stat st;
    stat(FILENAME, &st);
    //unsigned int size = st.st_size;

    // Serial
    //    int serialFd = SerialInit(SERIAL_PORT, BAUD_RATE);

    // writeCh('T');
    uint8_t ch;

    for (int i=0; i < 15; i++) {
        conn.putc('W');
    }

    //    menuDlg.valLabel->setText("RESET");

    std::cout << "Put reset line on '0'" << std::endl;
    resetPin.write(0);
    std::cout << "Waiting 200 ms...";

    //    tcflush(serialFd, TCIOFLUSH);
    conn.flush();
    QThread::usleep(200 * 1000);

    resetPin.write(1);
    std::cout << " put reset line on '1'" << std::endl;

    //    menuDlg.valLabel->setText("ERASE MEMORY");

    qDebug() << "Waiting to receive g";
    ch = conn.getc();

    std::cout << "Received " << ch << '\n';
    if (ch != 'g') {
        std::cerr << "Did not receive 'g' on serial, quit" << std::endl;
        return 2;
    }
    std::cout <<  "send 't'\n";
    conn.putc('t');

    //    menuDlg.valLabel->setText("SEND BINARY");

    std::cout << "Transmit file data \n";

    unsigned xorPos = 0;
    char buffer[4];

    do {
        // ch = readCh();
        std::cout << "Waiting to receive 'g'" << std::endl;
        ch = conn.getc();

        std::cout << "Received: " << ch << std::endl;

        f.read(buffer, 4);

        std::cout << "Send: ";
        for (char c : buffer) {
            // std::cout << std::ios::hex << std::setw(2) << c << ' ';
            printf("%.2x ", c);

            c = c ^ xorArray[xorPos % 1024];
            xorPos++;

            conn.putc(c);

            //int progress = xorPos * 100 / size;
            //            menuDlg.hSlider->setValue(progress);
        }
        printf("\n");

    } while (ch == 'g' && !f.eof());

    std::cout << "File transfer done, wait 1sec and reset\n";



    QThread::sleep(1);
    resetPin.write(0);
    QThread::msleep(100);
    resetPin.write(1);

    std::cout << "Done flashing, close serial" << std::endl;

    //    menuDlg.valLabel->setText("DONE");
    resetPin.setDirection(GPIOPin::Direction::Out);
    return 0;
}
