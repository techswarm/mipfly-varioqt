/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "bootloadermanager.h"

#include <iostream>
#include <fstream>
#include <QtDebug>

#include <sys/stat.h>

#include "menudialog.h"
#include "ui_menudialog.h"
#include "threads/gpsthread.h"
#include "GPIO/GPIOpins.h"
#include "VarioQt.h"

const QString BootloaderManager::BOOT_FILENAME ("VarioSTM.bin");
const QString BootloaderManager::BOOT_FILENAME_EXP ("VarioSTMExp.bin");

BootloaderManager::BootloaderManager(GPSThread &thread, const MenuDialog &menuDialog)
    : serialThread(thread) , menuDlg(menuDialog)
{
    connect(this, &BootloaderManager::pause, &thread, &GPSThread::pauseRead);
    connect(this, &BootloaderManager::resume, &thread, &GPSThread::resumeRead);
}

int BootloaderManager::flash(bool flashExperimental=false)
{
    menuDlg.getUi().hSlider->setValue(0);

    VarioQt::instance().getLoggerInterface()->disableReceive();
    QThread::sleep(1);

    const SerialConnection conn("/dev/ttyS5", 115200);
    GPIOPin resetPin(134);

    if (!resetPin.isExported()) {
        resetPin.exportPin();
        resetPin.setDirection(GPIOPin::Direction::Out);
    }
    resetPin.setDirection(GPIOPin::Direction::Out);
    std::ifstream f;
    if(flashExperimental == false)
    {
        QByteArray ba = BOOT_FILENAME.toLocal8Bit();
        f.open(ba.data(), std::ios::binary);
    }
    else
    {
        QByteArray ba = BOOT_FILENAME_EXP.toLocal8Bit();
        f.open(ba.data(), std::ios::binary);
    }

    if (!f) {
        QByteArray ba = BOOT_FILENAME.toLocal8Bit();
        std::cerr << "Error opening file: " << ba.data() << std::endl;
        return 1;
    }

    // Get file size
    //unsigned int size = 100;


    /*
    if(flashExperimental == false)
    {
        QByteArray ba = BOOT_FILENAME.toLocal8Bit();
        f.open(ba.data(), std::ios::binary);
        struct stat st;
        stat(ba.data(), &st);
        size = st.st_size;
    }
    else
    {
        QByteArray ba = BOOT_FILENAME_EXP.toLocal8Bit();
        f.open(ba.data(), std::ios::binary);
        struct stat st;
        stat(ba.data(), &st);
        size = st.st_size;
    }
    */

    // writeCh('T');
    uint8_t ch;

    emit pause();
    menuDlg.getUi().valLabel->setText("STOPPING DEAMON");
    QThread::usleep(50000);

    for (int i=0; i < 15; i++) {
        conn.putc('W');
    }

    menuDlg.getUi().valLabel->setText("RESET");

    std::cout << "Put reset line on '0'" << std::endl;
    resetPin.write(0);
    std::cout << "Waiting 200 ms...";

    conn.flush();
    QThread::usleep(200 * 1000);

    resetPin.write(1);
    std::cout << " put reset line on '1'" << std::endl;

    menuDlg.getUi().valLabel->setText("ERASE MEMORY");

    qDebug() << "Waiting to receive g";
    while(!conn.numBytesAvailableRead());
    ch = conn.getc();

    std::cout << "Received '" << ch << "'\n";
    if (ch != 'g') {
        std::cerr << "Did not receive 'g' on serial, quit" << std::endl;
        return 2;
    }
    std::cout <<  "send 't'\n";
    conn.putc('t');

    menuDlg.getUi().valLabel->setText("SEND BINARY");

    std::cout << "Transmit file data \n";

    unsigned pos = 0;
    char buffer[4];

    do {
        std::cout << "Waiting to receive 'g'" << std::endl;
        while(!conn.numBytesAvailableRead());
        ch = conn.getc();

        std::cout << "Received: " << ch << std::endl;

        f.read(buffer, 4);

        std::cout << "Send: ";
        for (char c : buffer) {
            // std::cout << std::ios::hex << std::setw(2) << c << ' ';
            printf("%.2x ", c);

            pos++;

            conn.putc(c);

            int progress = pos / 100;
            //menuDlg.getUi().hSlider->setValue(progress);
            QString meg = "SEND BINARY "+QString::number(progress);
            menuDlg.getUi().valLabel->setText(meg);
        }
        printf("\n");

    } while (ch == 'g' && !f.eof());

    conn.close();
    menuDlg.getUi().valLabel->setText("RESTARTING");
    QThread::sleep(5);
    resetPin.write(0);
    QThread::usleep(200 * 1000);
    resetPin.write(1);

    menuDlg.getUi().valLabel->setText("WAIT FOR READY");
    emit resume();
    std::cout << "Done flashing, resume serial read" << std::endl;

    QThread::sleep(15);
    menuDlg.getUi().valLabel->setText("DONE");
    resetPin.setDirection(GPIOPin::Direction::In);
    VarioQt::instance().getLoggerInterface()->enableReceive();
    return 0;
}
