/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "GlideDetails.h"
#include "ui_GlideDetails.h"
#include "testing/gpioemulator.h"
#include <QKeyEvent>
#include "globals.h"
#include <QTime>
#include "IGC/igcparser.h"
#include <QDebug>
#include "VarioQt.h"

glideDetails::glideDetails(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::glideDetails)
{
    ui->setupUi(this);

    ui->lblDistance->setValue("Dist km:");
    ui->lblFlightTime->setValue("Flight time:");
}

glideDetails::~glideDetails()
{
    delete ui;
}

void glideDetails::keyPressEvent(QKeyEvent *event) {

    gpioEmulatorPtr->keyPressed(event->key());
}


void glideDetails::setIgcPath(QString path)
{
    igcPath = path;
    ui->flightPlotUi->setIGCPath(path);

    igcParser igcP(path);
    igcP.parse();


    int startTimeSecOfDay = (igcP.igcLogEntrys.first().snapTime).hour() * 3600 +
            (igcP.igcLogEntrys.first().snapTime).minute() * 60 +
            (igcP.igcLogEntrys.first().snapTime).second();
    int endTimeSecOfDay = (igcP.igcLogEntrys.last().snapTime).hour() * 3600 +
            (igcP.igcLogEntrys.last().snapTime).minute() * 60 +
            (igcP.igcLogEntrys.last().snapTime).second();

    int totalFlightTimeSeconds = endTimeSecOfDay - startTimeSecOfDay;

    QTime t(0,0,0,0);
    t = t.addSecs(totalFlightTimeSeconds);

    ui->lblFlightTime->setValue("FTtime:" + t.toString("hh:mm:ss"));

    double flightDistanceKm = (igcP.igcLogEntrys.first().coordonates).distanceKm(igcP.igcLogEntrys.last().coordonates);

    ui->lblDistance->setValue("Dist km: " + QString::number(flightDistanceKm,'f',1));

    qDebug()<<"Total s:"<<totalFlightTimeSeconds;

}

void glideDetails::showEvent(QShowEvent *e)
{
    QWidget::showEvent(e);
    VarioQt::instance().pushNewWidget(this);
}

void glideDetails::hideEvent(QHideEvent *e)
{
    QWidget::hideEvent(e);
    VarioQt::instance().deleteLastWidget();
}

void glideDetails::gpioLongPressed(int btn)
{
    if(btn == Vario::GPIO1)
    {
        hide();
    }
    else
    {
        ui->flightPlotUi->gpioLongPressed(btn);
    }
}

void glideDetails::gpioPressed(int btn)
{
    ui->flightPlotUi->gpioPressed(btn);
}
