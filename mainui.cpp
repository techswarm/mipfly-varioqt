/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mainui.h"
#include "ui_mainui.h"

#include <QKeyEvent>
#include <QDialog>
#include <QSettings>
#include <QtDebug>

#include "NMEA/NMEAParser.h"
#include "NMEA/rawlogger.h"
#include "utils/utils.h"
#include "navUtils/Wind.hpp"
#include "navUtils/variocompute.h"
#include "settings/namemanager.h"
#include "threads/gpiolistenthread.h"
#include "threads/inputThread.h"
#include "settings/locationmanager.h"
#include "Geo/locationsthreadmgr.h"

#include "sound/varioSound.h"
#include "threads/fb2spithread.h"
#include "settings/glidemanager.h"

#include "ui/dynamicui.h"
#include "ui/usbdialog.h"
#include "globals.h"

#include <QApplication>
#include "VarioQt.h"
#include "Warning.h"
#include "navUtils/elevation.h"
#include "navUtils/faiassistcompute.h"
#include "navUtils/crosssectionpoints.h"
#include "GPIO/gpiowatcher.h"
#include "GPIO/GPIOpins.h"

#include "utils/unitconverter.h"
#include <utils/PowerOffManager.h>
#include <Warning.h>

static const QString timeQStr(const time_t &time);
static const QString timeShortQStr(const time_t &time);
static const QString timeDiffQStr(const time_t &end, const time_t &start);
static const QString timeDiffShortQStr(const time_t &end, const time_t &start);

MainUI::MainUI(QWidget *parent) : QWidget(parent),
    ui(new Ui::MainUI),
    powerPin(139)
{
    nameMgr = VarioQt::instance().getNameManager();
    ui->setupUi(this);

    setupUserInterface();

    connect(VarioQt::instance().getVarioParser(), SIGNAL(temperature(int)), this, SLOT(setTemp(int)));
    connect(VarioQt::instance().getVarioParser(), SIGNAL(tasChanged(qreal)), this, SLOT(setTAS(qreal)));
    connect(VarioQt::instance().getVarioParser(), SIGNAL(vSpeed(int)), this->ui->vario, SLOT(setVSpeed(int)));
    connect(VarioQt::instance().getVarioParser(), SIGNAL(baroHeight(int)), this, SLOT(setPressureAltitude(int)));

    connect(VarioQt::instance().getGpsParser(), SIGNAL(gpsHeightChanged(int)), this, SLOT(setAltitude(int)));
    connect(VarioQt::instance().getGpsParser(), SIGNAL(gpsHeightChanged(int)), this, SLOT(setInitAltitude(int)));

    connect(VarioQt::instance().getGpsParser(), SIGNAL(speedKmph(qreal)), this, SLOT(setGndSpeed(qreal)));
    connect(VarioQt::instance().getGpsParser(), SIGNAL(coordUpdate(Coord)), this, SLOT(setStartCoord(Coord)));

    connect(VarioQt::instance().getGpsParser(), SIGNAL(coordUpdate(Coord)), VarioQt::instance().getFaiAssistCompute(), SLOT(doPushTaskPoint(Coord)));

    connect(VarioQt::instance().getGpsParser(), SIGNAL(coordUpdate(Coord)), ui->airSpacePlot, SLOT(setCenter(Coord)));
    connect(VarioQt::instance().getGpsParser(), SIGNAL(course(qreal)), ui->airSpacePlot, SLOT(setCourse(qreal)));
    connect(VarioQt::instance().getGpsParser(), SIGNAL(coordUpdate(Coord)), VarioQt::instance().getAirSpaceThread(), SLOT(setCenter(Coord)));
    connect(VarioQt::instance().getGpsParser(), SIGNAL(gpsHeightChanged(int)), VarioQt::instance().getAirSpaceThread(), SLOT(setAltitude(int)));

    connect(VarioQt::instance().getVarioParser(), SIGNAL(baroHeight(int)), ui->verticalPlotUI, SLOT(pushHeight(int)));

    connect(VarioQt::instance().getGpsParser(), SIGNAL(fixChange(int)), this, SLOT(setFix(int)));

    connect(VarioQt::instance().getGpsParser(), SIGNAL(course(qreal)), this->ui->compass, SLOT(setHeading(qreal)));

    connect(VarioQt::instance().getWind(), SIGNAL(windSpeed(qreal)), this->ui->compass, SLOT(setWindSpeed(qreal)));
    connect(VarioQt::instance().getWind(), SIGNAL(windDir(qreal)), this->ui->compass, SLOT(setWindDir(qreal)));

    connect(nameMgr, SIGNAL(nameSet(QString)), this, SLOT(setPilot(QString)));

    connect(VarioQt::instance().getVarioCompute(), SIGNAL(average(qreal)), this->ui->vario, SLOT(setAverage(qreal)));
    connect(VarioQt::instance().getVarioCompute(), SIGNAL(average(qreal)), this, SLOT(setAvgClimb(qreal)));
    connect(VarioQt::instance().getVarioCompute(), SIGNAL(averageMax(qreal)), this->ui->vario, SLOT(setMax(qreal)));
    connect(VarioQt::instance().getVarioCompute(), SIGNAL(averageMin(qreal)), this->ui->vario, SLOT(setMin(qreal)));

    connect(VarioQt::instance().getGpsParser(), SIGNAL(speedKmph(qreal)),
            VarioQt::instance().getGlider(), SLOT(setGroundSpeed(qreal)));
    connect(VarioQt::instance().getGpsParser(), SIGNAL(gpsHeightChanged(int)),
            VarioQt::instance().getGlider(), SLOT(setHeight(int)));
    connect(VarioQt::instance().getGpsParser(), SIGNAL(course(qreal)),
            VarioQt::instance().getGlider(), SLOT(setHeading(qreal)));
    connect(VarioQt::instance().getGpsParser(), SIGNAL(coordUpdate(Coord)),
            VarioQt::instance().getGlider(), SLOT(setPosition(Coord)));


    connect(&gpioEmulator, &GPIOEmulator::gpioPressed, this, &MainUI::gpioPressed);
    connect(&gpioEmulator, &GPIOEmulator::gpioLongPressed, this, &MainUI::gpioLongPressed);
    gpioEmulatorPtr = &gpioEmulator;

    connect(VarioQt::instance().getLastLift(),SIGNAL(liftDir(qreal)),this->ui->compass,SLOT(setThermalDir(qreal)));
    connect(VarioQt::instance().getLastLift(),SIGNAL(liftDist(int)),this,SLOT(setDistanceThermal(int)));

    //connect(&gpioEmulator, &GPIOEmulator::gpioPressed, this->ui->flightPlotUI, SLOT(gpioPressed(int)));
    //connect(&gpioEmulator, &GPIOEmulator::gpioLongPressed, this->ui->flightPlotUI, SLOT(gpioLongPressed(int)));

    uiTimerId = this->startTimer(250);
    ui->battery->startTimer(200);

    connect(&watcher, &QDeviceWatcher::deviceAdded, this, &MainUI::deviceAdded);
    connect(&watcher, &QDeviceWatcher::deviceRemoved, this, &MainUI::deviceRemoved);

    connect(VarioQt::instance().getGpsParser(), &NmeaParser::course, ui->thermalPlot, &thermalCenteringUI::pushHeading);
    connect(VarioQt::instance().getGpsParser(), &NmeaParser::coordUpdate, ui->thermalPlot, &thermalCenteringUI::pushPoint);
    connect(VarioQt::instance().getGpsParser(), &NmeaParser::coordUpdate, ui->mapPlot, &MapPlot::setCenter);

    connect(VarioQt::instance().getGpsParser(), SIGNAL(coordUpdate(Coord)), ui->cutPlot, SLOT(setCenter(Coord)));
    connect(VarioQt::instance().getGpsParser(), SIGNAL(gpsHeightChanged(int)), ui->cutPlot, SLOT(setHeight(int)));
    connect(VarioQt::instance().getGpsParser(), SIGNAL(course(qreal)),ui->cutPlot,SLOT(setHeading(qreal)));

    connect(VarioQt::instance().getGpsParser(), SIGNAL(course(qreal)),ui->mapPlot,SLOT(setHeading(qreal)));

    connect(VarioQt::instance().getGpsParser(), &NmeaParser::coordUpdate, this, &MainUI::setGpsCoord);

    connect(VarioQt::instance().getVarioParser(), &NmeaParser::vSpeed, ui->thermalPlot, &thermalCenteringUI::pushCMPS);
    connect(VarioQt::instance().getNetStats(),&NetStats::statusChanged,ui->BNEPBox,&BNEPWidget::setStatus);

    //ui->compass->setThermalDir(0);

    dynamicUI = new DynamicUI(this->ui);
    dynamicUI->hideElements();

    QSettings settings;

    /* Allows the user to select the GPS Status page as first page (100), by adding startPage=100 to properties file */
    startPage_ = settings.value("startPage",1).toInt();
    dynamicUI->setActualPage(startPage_);
    dynamicUI->showPage(startPage_);

    connect(VarioQt::instance().getTermalingDetector(),SIGNAL(termalingStart()),dynamicUI,SLOT(activateTermal()));
    connect(VarioQt::instance().getTermalingDetector(),SIGNAL(termalingStop()),dynamicUI,SLOT(deactivateTermal()));
    connect(VarioQt::instance().getGpsParser(), SIGNAL(course(qreal)),VarioQt::instance().getTermalingDetector(),SLOT(doHeadingPush(qreal)));
    connect(dynamicUI,SIGNAL(showNavMenu()),this,SLOT(showNavMenu()));
    connect(dynamicUI,SIGNAL(showFAIMenu()),this,SLOT(showFAIMenu()));

    watcher.start();

    if (ui->airSpacePlot->startTimer(200) == 0) {
        qDebug() << "Could not start timer for airspacePlot";
    }
    if (ui->mapPlot->startTimer(1000) == 0) {
        qDebug() << "Could not start timer for mapPlot";
    }

    grabKeyboard();
    activateWindow();

    double lat = settings.value("navigation/lastLatitude", 42.21).toDouble();
    double lon = settings.value("navigation/lastLongitude", 21.42).toDouble();

    coord_.setLat(lat);
    coord_.setLon(lon);

    //set task connections
    ui->taskPlot->setPTask(VarioQt::instance().getTask());

    connect(VarioQt::instance().getGpsParser(), SIGNAL(coordUpdate(Coord)),
            VarioQt::instance().getTask(),SLOT(setCenter(Coord)));
    connect(VarioQt::instance().getGpsParser(), SIGNAL(course(qreal)),
            VarioQt::instance().getTask(),SLOT(setCourse(qreal)));
    connect(VarioQt::instance().getTakeOffThread(),SIGNAL(takeoff()),
            VarioQt::instance().getTask(),SLOT(doTakeoff()));
    connect(VarioQt::instance().getTakeOffThread(),SIGNAL(landed()),
            VarioQt::instance().getTask(),SLOT(doLand()));

    connect(VarioQt::instance().getGpsParser(),SIGNAL(coordUpdate(Coord)),
            VarioQt::instance().getOlc(),SLOT(processGpsCoord(Coord)));

    //setup power pin
    powerPin.exportPin();

    VarioQt::instance().setTaskPlot(ui->taskPlot);
    VarioQt::instance().setTrianglePlot(ui->trianglePlot);
}

MainUI::~MainUI()
{
    this->killTimer(uiTimerId);
    while (timerTaskFlag);
    if(turnOnConfirm != nullptr)
    {
        turnOnConfirm->hide();
        turnOnConfirm->deleteLater();
    }
    delete ui;
    if(turnOffConfirm != nullptr)
    {
        turnOffConfirm->hide();
        turnOffConfirm->deleteLater();
    }
    deleteLater();
}

void MainUI::connectTo(const GPIOListenThread &btnThread) {
    connect(&btnThread, SIGNAL(pressed(int)), this, SLOT(gpioPressed(int)));
    connect(&btnThread, SIGNAL(longPressed(int)), this, SLOT(gpioLongPressed(int)));
}

void MainUI::connectTo(const InputThread &btnThread) {
    connect(&btnThread, SIGNAL(pressed(int)), this, SLOT(gpioPressed(int)));
    connect(&btnThread, SIGNAL(longPressed(int)), this, SLOT(gpioLongPressed(int)));
}

void MainUI::showNavMenu()
{
    menuDialog.move(this->frameGeometry().x() + (this->width() - menuDialog.width()) / 2, this->frameGeometry().y() + (this->height() - menuDialog.height()) / 2);
    menuDialog.goToNav();
    menuDialog.checkDescAtLastIndex();
    menuDialog.show();
}

void MainUI::showFAIMenu()
{
    menuDialog.move(this->frameGeometry().x() + (this->width() - menuDialog.width()) / 2, this->frameGeometry().y() + (this->height() - menuDialog.height()) / 2);
    menuDialog.goToFAI();
    menuDialog.checkDescAtLastIndex();
    menuDialog.show();
}

void MainUI::connectTo(const GPIOWatcher &btnThread) {
    connect(&btnThread, SIGNAL(pressed(int)), this, SLOT(gpioPressed(int)));
    connect(&btnThread, SIGNAL(longPressed(int)), this, SLOT(gpioLongPressed(int)));
}

void MainUI::setAvgClimb(double avgClimb)
{
    if(ui->boxAvgClimb->getBaseUnit() == "ft/min")
    {
        ui->boxAvgClimb->setValue(QString::number(UnitConverter::mPerSecToFtPerMin(avgClimb/100),'f',0));
    }
    else
    {
        ui->boxAvgClimb->setValue(QString::number(avgClimb/100,'f',1));
    }
}



void MainUI::setupUserInterface() {

    //ui->thermalPlot->setVisible(true);

    ui->boxCrtTime->setValue(timeQStr(start));
    ui->boxCrtTimeShort->setValue(timeShortQStr(start));

    ui->boxElapsed->setValue("--:--:--");
    ui->boxElapsedShort->setValue("--:--");

    QString name = nameMgr->getName();

    ui->lblPilot->setValue(name.isNull() ? "- no pilot -" : name);

    ui->cboxTemp->setValue("--");

    ui->cboxHDiff->setValue("--");

    ui->cboxGlide->setValue("--");

    ui->boxAltitude->setValue("-");

    ui->cboxAGL->setValue("-");

    ui->boxGndSpeed->setValue("-");

    ui->lblPilot->setAlignment(Qt::AlignLeft);

    ui->lblDistStart->setValue("0");

    ui->lblDistThermal->setValue("0");

    ui->lblFix->setValue("No fix");

    performWeatherStationTasks();
}

void MainUI::hideUSBDialog()
{
    if(usbDialog != nullptr)
    {
        usbDialog->hide();
        delete usbDialog;
        usbDialog = nullptr;
    }
}

// Slots

void MainUI::deviceAdded(const QString &dev)
{
    if (menuDialog.isVisible()) {
        hideMenuDialog();
    }
    if (usbDialog == nullptr) {
        usbDialog = new USBDialog(*nameMgr, this);
        usbDialog->show();
    }
    usbDialog->addDevice(dev);
}

void MainUI::deviceRemoved(const QString &dev)
{
    if (usbDialog != nullptr) {
        usbDialog->removeDevice(dev);
        if (usbDialog->deviceCount() == 0)
            hideUSBDialog();
    }
}

void MainUI::startDisplayElapsed() {
    resetTime();
    displayElapsed = true;
}

void MainUI::stopDisplayElapsed() {
    displayElapsed = false;
}

void MainUI::resetInitValues()
{
    // Distance to takeoff point
    disconnect(VarioQt::instance().getGpsParser(), SIGNAL(coordUpdate(Coord)), this, SLOT(setDistance(Coord)));
    connect(VarioQt::instance().getGpsParser(), SIGNAL(coordUpdate(Coord)), this, SLOT(setStartCoord(Coord)));

    //activate data logging
    rawLogger.open();
    connect(VarioQt::instance().getGpsParser(), SIGNAL(messageReceived(QString)),&rawLogger,SLOT(doMessageLog(QString)));
    connect(VarioQt::instance().getVarioParser(), SIGNAL(messageReceived(QString)),&rawLogger,SLOT(doMessageLog(QString)));
}

void MainUI::setFix(int fix) {

    fix_ = fix;

    if (fix == 2) {
        ui->GPSBox->setValue(2);
        ui->lblFix->setValue("2D fix");
    } else if (fix == 3) {
        ui->GPSBox->setValue(2);
        ui->lblFix->setValue("3D fix");
    } else {
        ui->GPSBox->setValue(1);
        ui->lblFix->setValue("No fix");
    }

}

void MainUI::setTemp(int temp) {
    ui->cboxTemp->setValue(QString::number(temp));
}

void MainUI::setInitAltitude(int alt) {
    startAlt = alt;

    TakeoffThread* toth = VarioQt::instance().getTakeOffThread();
    toth->setTakeoffHeight(alt);

    disconnect(VarioQt::instance().getGpsParser(), SIGNAL(gpsHeightChanged(int)), this, SLOT(setInitAltitude(int)));
    connect(VarioQt::instance().getGpsParser(), SIGNAL(gpsHeightChanged(int)), this, SLOT(setHDiff(int)));
}

void MainUI::setHDiff(int alt) {
    if(setHDiffPostscale == 0)
    {
        if(ui->cboxHDiff->getBaseUnit() == "ft")
        {
            ui->cboxHDiff->setValue(QString::number((int)UnitConverter::mToFt(alt - startAlt)));
        }
        else
        {
            ui->cboxHDiff->setValue(QString::number(alt - startAlt));
        }
        setHDiffPostscale = 10;
    }
    setHDiffPostscale--;
}

void MainUI::setGlide(qreal glide) {
    if (glide < 0 || glide > GlideManager::GLIDE_MAX) {
        ui->cboxGlide->setValue("---");
    } else {
        //        this->ui->valGlide->setText(QString::number(glide));
        ui->cboxGlide->setValue(QString::number(glide,'f',1));
    }
}

void MainUI::setGndSpeed(qreal gndSpeed) {
    this->gndSpeed = gndSpeed;
    if(ui->boxGndSpeed->getBaseUnit() == "knots")
    {
        ui->boxGndSpeed->setValue(QString::number(UnitConverter::kmPerHToKnots(gndSpeed),'f',0));
    }
    else if(ui->boxGndSpeed->getBaseUnit() == "mi/h")
    {
        ui->boxGndSpeed->setValue(QString::number(UnitConverter::kmPerHToMiPerHour(gndSpeed),'f',0));
    }
    else
    {
        ui->boxGndSpeed->setValue(QString::number(gndSpeed,'f',0));
    }
}

void MainUI::setTAS(qreal tas)
{
    if(tas<5)ui->tas->setValue("--");
    else
    {
        if(ui->tas->getBaseUnit() == "knots")
        {
            ui->tas->setValue(QString::number(UnitConverter::kmPerHToKnots(tas),'f',0));
        }
        else if(ui->tas->getBaseUnit() == "mi/h")
        {
            ui->tas->setValue(QString::number(UnitConverter::kmPerHToMiPerHour(tas),'f',0));
        }
        else
        {
            ui->tas->setValue(QString::number(tas,'f',0));
        }
    }

    //update headwind
    float headwind = tas - gndSpeed;
    if(ui->headwind->getBaseUnit() == "knots")
    {
        ui->headwind->setValue(QString::number(UnitConverter::kmPerHToKnots(headwind),'f',0));
    }
    else if(ui->headwind->getBaseUnit() == "mi/h")
    {
        ui->headwind->setValue(QString::number(UnitConverter::kmPerHToMiPerHour(headwind),'f',0));
    }
    else
    {
        ui->headwind->setValue(QString::number(headwind,'f',0));
    }
}

void MainUI::setAltitude(int altitude) {
    altitude_ = altitude;
    if(ui->boxAltitude->getBaseUnit() == "ft")
    {
        altitude = UnitConverter::mToFt(altitude);
        ui->boxAltitude->setValue(QString::number(altitude));
    }
    else
    {
        ui->boxAltitude->setValue(QString::number(altitude));
        ui->boxAltitude->setUnit("m");
    }
}

void MainUI::setPressureAltitude(int pressureAltitude) {

    double currVdop;

    pressureAltitudeAsl_ = pressureAltitude;

    if (VarioQt::instance().getTakeOffThread()->getFlying() == false) {
        currVdop = VarioQt::instance().getGpsParser()->getVdop();
        if (currVdop < 2.0) {
            if (currVdop <= pressureAltitudeAslOffsetAtVdop_) {
                pressureAltitudeAslOffset_ = pressureAltitudeAsl_-altitude_;
                pressureAltitudeAslOffsetAtVdop_ = currVdop;
                pressureAltitudeRef1Offset_ = pressureAltitude;
            }
        }
    }

    ui->boxPressAltAsl->setValue(QString::number(pressureAltitudeAsl_-pressureAltitudeAslOffset_));

    /* ToDo, implement offset based upon user selected value (zero at takeoff) */
    ui->boxPressAltRef1->setValue(QString::number(pressureAltitude-pressureAltitudeRef1Offset_));

    /* ToDo, implement offset based upon user selected value (alt above lz? By using location?) */
    ui->boxPressAltRef2->setValue(QString::number(pressureAltitude));

}

void MainUI::setPilot(QString pilot) {
    ui->lblPilot->setValue(pilot);
}

void MainUI::setStartCoord(Coord coord) {
    startCoord = coord;
    TakeoffThread* toth = VarioQt::instance().getTakeOffThread();
    toth->setTakeoffPos(coord);

    disconnect(VarioQt::instance().getGpsParser(), SIGNAL(coordUpdate(Coord)), this, SLOT(setStartCoord(Coord)));
    connect(VarioQt::instance().getGpsParser(), SIGNAL(coordUpdate(Coord)), this, SLOT(setDistance(Coord)));
}

void MainUI::setGpsCoord(Coord coord)
{
    //ui->mapPlot->setCenter(coord);
    coord_.setLat(coord.getLat());
    coord_.setLon(coord.getLon());
}

void MainUI::landedDetected()
{
    ui->RecBox->stopRecording();
    Warning::showWarning(tr("INFO"),tr("Landed"));
    QSettings settings;
    bool turnOffAfterLand = settings.value("turnOffAfterLand",true).toBool();
    if(turnOffAfterLand)
    {
        turnOnTimeout = 60;
        delete turnOnConfirm;
        turnOnConfirm = new ChooseInput();
        turnOnConfirm->setCaption(tr("TURN OFF"));
        turnOnConfirm->setValue(QString::number(turnOnTimeout));
        turnOnConfirm->setChouse2(tr("CANCEL"));

        turnOnConfirm->show();
        confirmTurnOn = true;
    }
}

void MainUI::takeoffDetected()
{
    ui->RecBox->startRecording();
    Warning::showWarning(tr("INFO"),tr("Takeoff"));
}

void MainUI::displayMessageAlarm(QString message)
{
    Warning::showWarningSound(tr("INFO"),message,5);
}

void MainUI::setDistance(Coord crtCoord) {

    int distanceMeters = int(startCoord.distanceKm(crtCoord) * 1000);

    if(ui->lblDistStart->getBaseUnit() == "mi")
    {
        if(distanceMeters > 1000000) {
            ui->lblDistStart->setValue("---");
            ui->lblDistStart->setUnit(QString());
        }
        else
        {
            float distanceMi = UnitConverter::mToMi(distanceMeters);

            if(distanceMi < 0.01) ui->lblDistStart->setValue(QString::number(distanceMi, 'f', 3));
            else if(distanceMi < 0.1) ui->lblDistStart->setValue(QString::number(distanceMi, 'f', 2));
            else ui->lblDistStart->setValue(QString::number(distanceMi, 'f', 1));
        }
    }
    else
    {
        if(distanceMeters > 1000000) {
            ui->lblDistStart->setValue("---");
            ui->lblDistStart->setUnit(QString());
        }
        else {
            ui->lblDistStart->setValue(QString::number(qreal(distanceMeters) / 1000.0, 'f', 1));
            ui->lblDistStart->setUnit("km");
        }
    }
}

void MainUI::setDistanceThermal(int distanceMeters) {

    if(ui->lblDistThermal->getBaseUnit() == "mi")
    {
        if(distanceMeters > 1000000) {
            ui->lblDistThermal->setValue("---");
            ui->lblDistThermal->setUnit(QString());
        }
        else
        {
            float distanceMi = UnitConverter::mToMi(distanceMeters);

            if(distanceMi < 0.01) ui->lblDistThermal->setValue(QString::number(distanceMi, 'f', 3));
            else if(distanceMi < 0.1) ui->lblDistThermal->setValue(QString::number(distanceMi, 'f', 2));
            else ui->lblDistThermal->setValue(QString::number(distanceMi, 'f', 1));
        }
    }
    else
    {
        if(distanceMeters > 1000000) {
            ui->lblDistThermal->setValue("---");
            ui->lblDistThermal->setUnit(QString());
        }
        else {
            float distanceKm = (float)distanceMeters/1000;
            if(distanceKm < 0.1) ui->lblDistThermal->setValue(QString::number(distanceKm, 'f', 2));
            else ui->lblDistThermal->setValue(QString::number(distanceKm, 'f', 1));
            ui->lblDistThermal->setUnit("km");
        }
    }
}

void MainUI::resetTime() {
    start = time(NULL);
    ui->boxElapsed->setValue("00:00:00");
    ui->boxElapsedShort->setValue("00:00");
}

// GPIO

void MainUI::gpioPressed(int btn) {
    if(VarioQt::instance().isActiveWidget(this))
    {
        if(btn == Vario::GPIO1)
        {
            dynamicUI->keyDown();
        }
        if(btn == Vario::GPIO2)
        {
            dynamicUI->keyUp();
        }
        if (btn == Vario::GPIO3) {
            dynamicUI->keyEnter();
        }
    }
    else
    {
        menuDialog.gpioPressed(btn);
        if(glideDetailsUi != nullptr)glideDetailsUi->gpioPressed(btn);
        if(usbDialog != nullptr)usbDialog->gpioPressed(btn);

        emit mainGpioPressed(btn);
    }
    menuHideTimeout = 30;
}

void MainUI::gpioLongPressed(int btn) {
    if(VarioQt::instance().isActiveWidget(this))
    {
        if (btn == Vario::GPIO3) {
            menuDialog.move(this->frameGeometry().x() + (this->width() - menuDialog.width()) / 2, this->frameGeometry().y() + (this->height() - menuDialog.height()) / 2);
            menuDialog.checkDescAtLastIndex();
            menuDialog.show();
        }
        if (btn == Vario::GPIO2)
        {
            dynamicUI->keyF();
        }
    }
    else
    {
        menuDialog.gpioLongPressed(btn);
        if(glideDetailsUi != nullptr)glideDetailsUi->gpioLongPressed(btn);
        emit mainGpioLongPressed(btn);
    }
    menuHideTimeout = 30;
}

// Protected

void MainUI::keyPressEvent(QKeyEvent *event) {
    event->accept();
    gpioEmulator.keyPressed(event->key());

#ifdef MAKE_TESTS
    else if (event->key() == Qt::Key_X) {
        sndt.toggleSend();
    }
#endif
}

void MainUI::timerEvent(QTimerEvent *event) {
    timerTaskFlag = true;
    time(&crtTimer);
    tickCount++;

    if(ui->verticalCut->isVisible())
        ui->verticalCut->redraw();

    if (ui->satelliteStatusPage->isVisible()) {
        ui->satelliteStatusPage->updateStatus(coord_, altitude_, fix_, timeQStr(crtTimer),
                                              VarioQt::instance().getGpsParser()->getSatellitesUsed(),
                                              VarioQt::instance().getGpsParser()->getVdop());
    }

    if(powerPin.read()==0)
    {
        qDebug()<<"Power pin pressed!";
        if(turnOffConfirm != nullptr)
        {
            if(!(turnOffConfirm->isVisible()))
            {
                delete turnOffConfirm;
                turnOffConfirm = new ChooseInput;
                turnOffConfirm->setCaption(tr("SOFT TURN OFF"));
                turnOffConfirm->setValue(tr("CONFIRM","power button"));
                turnOffConfirm->setChouse3(tr("OK"));
                turnOffConfirm->setChouse1(tr("CANCEL"));
                turnOffConfirm->show();
            }
        }
        else
        {
            turnOffConfirm = new ChooseInput;
            turnOffConfirm->setCaption(tr("SOFT TURN OFF"));
            turnOffConfirm->setValue(tr("CONFIRM","power button"));
            turnOffConfirm->setChouse3(tr("OK"));
            turnOffConfirm->setChouse1(tr("CANCEL"));
            turnOffConfirm->show();
        }
    }

    if(turnOffConfirm != nullptr && turnOffConfirm->choice() == 3)
    {
#ifndef __ARMEL__
        PowerOffManager *p = new PowerOffManager();
        p->exitProgram();
        Warning::showWarning("INFO", "Exiting", 5);
#else
        PowerOffManager *p = new PowerOffManager();
        p->powerOff();
        //workarround for shutdown not happening bug.
        system("shutdown -P 2");
        Warning::showWarning(tr("INFO Side"), tr("Powering Off!"), 5);
#endif
    }
    if(turnOffConfirm != nullptr && (turnOffConfirm->choice() == 1 || turnOffConfirm->choice() == 2 || turnOffConfirm->choice() == 3))
    {
        delete turnOffConfirm;
        turnOffConfirm = nullptr;
    }


    if ((tickCount % 4)==0) {
        if(menuHideTimeout >= 0)
        {
            menuHideTimeout --;
            if(menuHideTimeout == 0)
            {
                hideMenuDialog();
                hideUSBDialog();
            }
        }

        //check the startup timer
        if(confirmTurnOn)
        {
            if(turnOnConfirm == nullptr)
            {
                turnOnConfirm = new ChooseInput;
                turnOnConfirm->setCaption(tr("TURN ON"));
                turnOnConfirm->setValue(QString::number(turnOnTimeout));
                turnOnConfirm->setChouse2(tr("CONFIRM"));

                turnOnConfirm->show();
            }
            else
            {
                if(turnOnTimeout>0)
                    turnOnTimeout--;
                if(turnOnTimeout == 0)
                {
                    //perform power off
#ifndef __ARMEL__
                    PowerOffManager *p = new PowerOffManager();
                    p->exitProgram();
                    Warning::showWarning("INFO", "Exiting", 5);
#else
                    PowerOffManager *p = new PowerOffManager();
                    p->powerOff();
                    //workarround for shutdown not happening bug.
                    system("shutdown -P 2");
                    Warning::showWarning(tr("INFO Timeout"), tr("Powering Off!"), 5);
#endif
                }
                turnOnConfirm->setValue(QString::number(turnOnTimeout));

                if(turnOnConfirm->choice() == 2 || turnOnConfirm->choice() == 1 || turnOnConfirm->choice() == 3)
                {
                    turnOnTimeout = -1;
                }

                if(turnOnTimeout > 0 && !turnOnConfirm->isVisible())turnOnConfirm->show();
            }
        }


        //tickCount = 0;

        performWeatherStationTasks();
        performTaskTasks();

        if(VarioQt::instance().getGpsParser()->getTimeSetComplete()) {
            ui->boxCrtTime->setValue(timeQStr(crtTimer));
            ui->boxCrtTimeShort->setValue(timeShortQStr(crtTimer));
        }

        if (displayElapsed && VarioQt::instance().getGpsParser()->getTimeSetComplete()) {
            ui->boxElapsed->setValue(timeDiffQStr(crtTimer, start));
            ui->boxElapsedShort->setValue(timeDiffShortQStr(crtTimer, start));
        }

        if (ui->satelliteStatus->isVisible()) {
            ui->satelliteStatus->updateVDop(VarioQt::instance().getGpsParser()->getVdop(), VarioQt::instance().getGpsParser()->getFixType());
        }

    }
    if ((tickCount % 4)==0 && ui->cboxAGL->isVisible()) {
        Elevation * el = VarioQt::instance().getElevation();
        int elevationMeters = el->getElevation(coord_);

        if(elevationMeters < 0)
        {
            ui->cboxAGL->setValue("---");
        }
        else
        {
            int diff = altitude_ - elevationMeters;
            if(diff<=25)ui->cboxAGL->setValue("--");
            else
            {
                if(ui->cboxAGL->getBaseUnit() == "ft")
                {
                    ui->cboxAGL->setValue(QString::number(UnitConverter::mToFt(diff),'f',0));
                }
                else
                {
                    ui->cboxAGL->setValue(QString::number(diff));
                }
            }
        }
    }
    timerTaskFlag = false;
}

void MainUI::setConfirmTurnOn(bool value)
{
    confirmTurnOn = value;
}

void MainUI::performWeatherStationTasks()
{
    if(ui->BoxWeather->isVisible())
    {
        ui->BoxWeather->setStationName(_weatherStation.getStationName());
        ui->BoxWeather->setWindSpeed(_weatherStation.getSpeed());
        ui->BoxWeather->setWindGust(_weatherStation.getGust());
        ui->BoxWeather->setUnit(_weatherStation.getUnit());
        ui->BoxWeather->setWindHeading(_weatherStation.getWindDir());
    }
}

void MainUI::performTaskTasks()
{
    Task *pTask = VarioQt::instance().getTask();
    OLC *pOLC = VarioQt::instance().getOlc();
    FaiAssistCompute *pFac = VarioQt::instance().getFaiAssistCompute();

    if(ui->dist5Tp->isVisible())
    {
        QString value;
        float distkm = pOLC->getOptimisedDist(CContestMgr::TYPE_OLC_CLASSIC);
        distkm/=1000;
        if(distkm <= 0 || distkm >1000)value="--";
        else
        {
            if(ui->dist5Tp->getBaseUnit() == "mi")
            {
                float distanceMi = UnitConverter::kmToMi(distkm);

                if(distanceMi < 0.01) value = QString::number(distanceMi, 'f', 3);
                else if(distanceMi < 0.1) value = QString::number(distanceMi, 'f', 2);
                else value = QString::number(distanceMi, 'f', 1);
            }
            else
            {
                if(distkm<1)
                {
                    value = QString::number(distkm,'f',2);
                }
                else
                {
                    value = QString::number(distkm,'f',1);
                }
            }
            ui->dist5Tp->setValue(value);
        }
    }

    if(ui->dist5TpP->isVisible())
    {
        QString value;
        float distkm = pOLC->getOptimisedDist(CContestMgr::TYPE_OLC_CLASSIC_PREDICTED);
        distkm/=1000;
        if(distkm <= 0 || distkm >1000)value="--";
        else
        {
            if(ui->dist5TpP->getBaseUnit() == "mi")
            {
                float distanceMi = UnitConverter::kmToMi(distkm);

                if(distanceMi < 0.01) value = QString::number(distanceMi, 'f', 3);
                else if(distanceMi < 0.1) value = QString::number(distanceMi, 'f', 2);
                else value = QString::number(distanceMi, 'f', 1);
            }
            else
            {
                if(distkm<1)
                {
                    value = QString::number(distkm,'f',2);
                }
                else
                {
                    value = QString::number(distkm,'f',1);
                }
            }
            ui->dist5TpP->setValue(value);
        }
    }

    if(ui->distWp->isVisible())
    {
        QString value;
        float distkm = pTask->getDistWp();
        if(distkm <= 0 || distkm >1000)value="--";
        else
        {
            if(ui->distWp->getBaseUnit() == "mi")
            {
                float distanceMi = UnitConverter::kmToMi(distkm);

                if(distanceMi < 0.01) value = QString::number(distanceMi, 'f', 3);
                else if(distanceMi < 0.1) value = QString::number(distanceMi, 'f', 2);
                else value = QString::number(distanceMi, 'f', 1);
            }
            else
            {
                if(distkm<1)
                {
                    value = QString::number(distkm,'f',2);
                }
                else
                {
                    value = QString::number(distkm,'f',1);
                }
            }
            ui->distWp->setValue(value);
        }
    }

    if(ui->tpRadius->isVisible())
    {
        QString value;
        float radius = pTask->getNextTargetRadius()/1000;
        if(radius <= 0 || radius >1000)value="--";
        else
        {
            if(ui->tpRadius->getBaseUnit() == "mi")
            {
                float radiusMi = UnitConverter::kmToMi(radius);

                if(radiusMi < 0.01) value = QString::number(radiusMi, 'f', 3);
                else if(radiusMi < 0.1) value = QString::number(radiusMi, 'f', 2);
                else value = QString::number(radiusMi, 'f', 1);
            }
            else
            {
                value = QString::number(radius,'f',1);
            }
        }
        ui->tpRadius->setValue(value);
    }

    if(ui->distWpCenter->isVisible())
    {
        QString value;
        float distkm = pTask->getDistWp(true);
        if(distkm <= 0 || distkm >1000)value="--";
        else
        {
            if(ui->distWpCenter->getBaseUnit() == "mi")
            {
                float distanceMi = UnitConverter::kmToMi(distkm);
                value = QString::number(distanceMi, 'f', 2);
            }
            else
            {
                value = QString::number(distkm,'f',2);
            }
        }
        ui->distWpCenter->setValue(value);
    }

    if(ui->glideWp->isVisible())
    {
        QString value;
        float glide =pTask->getGlideWp();
        if(glide <= 0 || glide >100)value="--";
        else
        {
            value = QString::number(glide,'f',1);
            ui->glideWp->setValue(value);
        }
    }

    if(ui->heightWp->isVisible())
    {
        QString value;
        int height = pTask->getHeightWp();
        if(abs(height)>=10000)
        {
            value = "--";
        }
        else
        {
            if(ui->heightWp->getBaseUnit() == "ft")
            {
                value = QString::number(UnitConverter::mToFt(height));
            }
            else
            {
                value = QString::number(height);
            }
        }
        ui->heightWp->setValue(value);
    }
    if(ui->heightGoal->isVisible())
    {
        QString value;
        int height = pTask->getHeightGoal();
        if(abs(height)>=10000)
        {
            value = "--";
        }
        else
        {
            if(ui->heightGoal->getBaseUnit() == "ft")
            {
                value = QString::number(UnitConverter::mToFt(height));
            }
            else
            {
                value = QString::number(height);
            }
        }
        ui->heightGoal->setValue(value);
    }

    if(ui->distGoal->isVisible())
    {
        QString value;
        float distkm = pTask->getDistGoal();
        if(distkm <= 0 || distkm >1000)value="--";
        else
        {
            if(ui->distWp->getBaseUnit() == "mi")
            {
                float distanceMi = UnitConverter::kmToMi(distkm);

                if(distanceMi < 0.01) value = QString::number(distanceMi, 'f', 3);
                else if(distanceMi < 0.1) value = QString::number(distanceMi, 'f', 2);
                else value = QString::number(distanceMi, 'f', 1);
            }
            else
            {
                value = QString::number(distkm,'f',1);
            }

            ui->distGoal->setValue(value);
        }
    }

    if(ui->boxTrackLength->isVisible())
    {
        QString value;
        float distkm = pFac->getTotalPathLength();
        if(distkm <= 0 || distkm >1000)value="--";
        else
        {
            if(ui->boxTrackLength->getBaseUnit() == "mi")
            {
                float distanceMi = UnitConverter::kmToMi(distkm);

                if(distanceMi < 0.01) value = QString::number(distanceMi, 'f', 3);
                else if(distanceMi < 0.1) value = QString::number(distanceMi, 'f', 2);
                else value = QString::number(distanceMi, 'f', 1);
            }
            else
            {
                value = QString::number(distkm,'f',1);
            }
            ui->boxTrackLength->setValue(value);
        }
    }

    if(ui->glideGoal->isVisible())
    {
        QString value;
        float glide = pTask->getGlideGoal();
        if(glide <= 0 || glide >500)value="--";
        else
        {
            value = QString::number(glide,'f',1);
        }
        ui->glideGoal->setValue(value);
    }

    if(ui->timeGate->isVisible())
    {
        QTime time = pTask->getNextTimeGate();
        QString value = time.toString();
        if(value == "00:00:00")value = "--:--:--";
        ui->timeGate->setValue(value);
    }
    if(ui->elapsedTime->isVisible())
    {
        QTime time = pTask->getElapsedTime();
        QString value = time.toString();
        ui->elapsedTime->setValue(value);
    }

    if(ui->distFlown->isVisible())
    {
        QString value;
        float distFlown = pTask->getDistFlown();
        if(abs(distFlown) >= 1000)value = "--";
        else
        {
            if(ui->distFlown->getBaseUnit() == "mi")
            {
                float distanceMi = UnitConverter::kmToMi(distFlown);
                value = QString::number(distanceMi, 'f', 1);
            }
            else
            {
                value = QString::number(distFlown,'f',1);
            }
        }
        ui->distFlown->setValue(value);
    }
    if(ui->avgSpeed->isVisible())
    {
        QString value;
        float avgSpeed = pTask->getAverageSpeed();
        if(avgSpeed >= 100 || avgSpeed <= -100)value = "--";
        else
        {
            if(ui->avgSpeed->getBaseUnit() == "knots")
            {
                value = QString::number(UnitConverter::kmPerHToKnots(avgSpeed),'f',1);
            }
            else if(ui->avgSpeed->getBaseUnit() == "mi/h")
            {
                value = QString::number(UnitConverter::kmPerHToMiPerHour(avgSpeed),'f',1);
            }
            else
            {
                value = QString::number(avgSpeed,'f',1);
            }
            value = QString::number(avgSpeed,'f',1);
        }
        ui->avgSpeed->setValue(value);
    }

    if(ui->speedSSS->isVisible())
    {
        QString value;
        float avgSpeed = pTask->getSpeedToSSS();
        if(avgSpeed >= 100 || avgSpeed <= -100)value = "--";
        else
        {
            if(ui->avgSpeed->getBaseUnit() == "knots")
            {
                value = QString::number(UnitConverter::kmPerHToKnots(avgSpeed),'f',1);
            }
            else if(ui->speedSSS->getBaseUnit() == "mi/h")
            {
                value = QString::number(UnitConverter::kmPerHToMiPerHour(avgSpeed),'f',1);
            }
            else
            {
                value = QString::number(avgSpeed,'f',1);
            }
            value = QString::number(avgSpeed,'f',1);
        }
        ui->speedSSS->setValue(value);
    }
    if(ui->avgClimb->isVisible())
    {
        QString value;
        if(ui->avgClimb->getBaseUnit() == "ft/min")
        {
            value = QString::number(UnitConverter::mPerSecToFtPerMin(VarioQt::instance().getVarioCompute()->getAverageClimb()),'f',0);
        }
        else
        {
            value = QString::number(VarioQt::instance().getVarioCompute()->getAverageClimb(),'f',1);
        }
        ui->avgClimb->setValue(value);
    }
    if(ui->avgSink->isVisible())
    {
        QString value;
        if(ui->avgSink->getBaseUnit() == "ft/min")
        {
            value = QString::number(UnitConverter::mPerSecToFtPerMin(VarioQt::instance().getVarioCompute()->getAverageSink()),'f',0);
        }
        else
        {
            value = QString::number(VarioQt::instance().getVarioCompute()->getAverageSink(),'f',1);
        }
        ui->avgSink->setValue(value);
    }

    if (ui->boxActiveWaypoint->isVisible()) {
        ui->boxActiveWaypoint->setValue(pTask->getNextWpName());
    }

    if (ui->boxCourse->isVisible()) {
        qDebug() << VarioQt::instance().getGlider()->getHeading();
        ui->boxCourse->setValue(QString::number((int)VarioQt::instance().getGlider()->getHeading()));
    }
}

void MainUI::setupFonts() {
    QFont f8px;
    f8px.setPixelSize(8);
    f8px.setFamily("Pixel Operator 8");
    f8px.setStyleStrategy(QFont::NoAntialias);

    QFont f16px;
    f16px.setPixelSize(16);
    f16px.setFamily("Pixel Operator");
    f16px.setStyleStrategy(QFont::NoAntialias);

    // Pilot name
    QFont f;
    f.setPixelSize(8);
    f.setFamily("PF Ronda Seven");
    f.setStyleStrategy(QFont::NoAntialias);
}

void MainUI::hideMenuDialog()
{
    menuDialog.hide();
    //menuDialog.resetToRoot();
}

void MainUI::showEvent(QShowEvent *e)
{
    QWidget::showEvent(e);
    VarioQt::instance().pushNewWidget(this);
}

void MainUI::hideEvent(QHideEvent *e)
{
    QWidget::hideEvent(e);
    VarioQt::instance().deleteLastWidget();
}

// Local functions

const QString timeQStr(const time_t &time) {
    char timestr[9];
    struct tm* now = localtime(&time);
    sprinttm(timestr, now);
    return QString(timestr);
}

const QString timeShortQStr(const time_t &time) {
    char timestr[9];
    struct tm* now = localtime(&time);
    sprinttmShort(timestr, now);
    return QString(timestr);
}

const QString timeDiffQStr(const time_t &end, const time_t &start) {
    char timestr[9];
    double elapsedSecs = difftime(end, start);
    struct tm elapsed = break_timediff(elapsedSecs);
    sprinttm(timestr, &elapsed);
    return QString(timestr);
}

const QString timeDiffShortQStr(const time_t &end, const time_t &start) {
    char timestr[9];
    double elapsedSecs = difftime(end, start);
    struct tm elapsed = break_timediff(elapsedSecs);
    sprinttmShort(timestr, &elapsed);
    return QString(timestr);
}
