/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "locationsservice.h"

#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QSettings>

#include <iostream>

#include "utils/vario_common.h"

static const QString KEY_LOCATIONS ("locations");
static const QString KEY_NAME      ("name");
static const QString KEY_LATITUDE  ("lat");
static const QString KEY_LONGITUDE ("lon");
static const QString KEY_ALTITUDE  ("alt");

LocationsService::LocationsService()
    : locationsFile("locations.json")
{
    if (locationsFile.exists()) {
        if (locationsFile.open(QIODevice::ReadOnly)) {
                QByteArray data = locationsFile.readAll();
                QJsonObject rootObj = (QJsonDocument::fromJson(data)).object();
                QJsonArray locArray = rootObj[KEY_LOCATIONS].toArray();
                for (auto it = locArray.begin(); it != locArray.end(); ++it) {
                    QJsonObject locObj = (*it).toObject();
                    Vario::Location loc;

                    loc.name = locObj[KEY_NAME].toString();
                    loc.lat = locObj[KEY_LATITUDE].toDouble();
                    loc.lon = locObj[KEY_LONGITUDE].toDouble();
                    loc.alt = locObj[KEY_ALTITUDE].toInt();

                    list.append(loc);
                }
                locationsFile.close();
        } else {
            std::cerr << "Could not open locations.json for reading saved locations" << std::endl;
        }
    }
}


void LocationsService::addLocation(const Vario::Location &location) {
    if (location.name.length() > 0) {
        list.append(location);
    } else {
        std::cout << "Warning: location name must not be empty" << std::endl;
    }
}

const QList<Vario::Location>& LocationsService::getAll() const {
    return list;
}

const Vario::Location LocationsService::currentLocation() {
    QSettings settings;
    QString crtLocName = settings.value(Vario::SETTINGS_CRTLOC, QString()).toString();
    if (crtLocName.isEmpty()) {
        return Vario::Location();
    }
    Vario::Location *loc = locationByName(crtLocName);
    if (loc == nullptr) {
        settings.remove(Vario::SETTINGS_CRTLOC);
        settings.sync();    // not sure if this is necessary after remove()
        return Vario::Location();
    }
    return *loc;
}

void LocationsService::setCurrent(const Vario::Location &loc) {
    if(loc.name!=lastLocation)
    {
        lastLocation = loc.name;
        QSettings settings;
        settings.setValue(Vario::SETTINGS_CRTLOC, loc.name);
        settings.sync();
        emit locationChanged(loc.name);
    }
}

void LocationsService::clearCurrent()
{
    if(lastLocation != "No Location")
    {
        lastLocation = "";
        QSettings settings;
        settings.remove(Vario::SETTINGS_CRTLOC);
        settings.sync();    // not sure if this is necessary after remove()
        emit locationCleared();
    }
}

bool LocationsService::isSetManually() const
{
    QSettings settings;
    return settings.value(Vario::SETTINGS_AUTOCRTLOC, false).toBool();
}

void LocationsService::setManually(bool val) const
{
    QSettings settings;
    settings.setValue(Vario::SETTINGS_AUTOCRTLOC, val);
    settings.sync();
}

const Vario::Location& LocationsService::locationAtIndex(int i) const
{
    return list.at(i);
}

#include <QtDebug>

void LocationsService::remove(const Vario::Location &location) {
    QMutableListIterator<Vario::Location> i(list);

    while(i.hasNext()) {
        if (i.next().name == location.name) {
            i.remove();
        }
    }

//    for (auto it = list.begin(); it != list.end(); ++it) {
//        if ((*it).name == location.name) {
//            list.removeOne(*it);
//            break;
//        }
//    }
}

void LocationsService::save() {
    QJsonArray locArray;

    for (auto location : list) {
        QJsonObject locObj;
        locObj[KEY_NAME] = location.name;
        locObj[KEY_LATITUDE] = location.lat;
        locObj[KEY_LONGITUDE] = location.lon;
        locObj[KEY_ALTITUDE] = location.alt;

        locArray.append(locObj);
    }

    QJsonObject rootObj;
    rootObj["locations"] = locArray;

    QJsonDocument doc(rootObj);

    if (locationsFile.open(QIODevice::WriteOnly | QIODevice::Text)) {
        locationsFile.write(doc.toJson());
        locationsFile.close();
    } else {
        std::cerr << "Could not open file locations.json for writing" << std::endl;
    }
}

Vario::Location* LocationsService::locationByName(const QString &name)
{
    for (int i=0; i < list.size(); i++) {
        if (list[i].name == name) {
            return &(list[i]);
        }
    }
    return nullptr;
}
