/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "locationsthreadmgr.h"

#include "threads/locationsthread.h"
#include "Geo/locationsservice.h"


LocationsThreadMgr::LocationsThreadMgr(const LocationManager &locMgr, QObject *parent)
    : QObject(parent), locMgr(locMgr)
{

}

void LocationsThreadMgr::startThread()
{
    if (!LocationsService().isSetManually()) {
        if (thread == nullptr) {
            thread = new LocationsThread(locMgr);
        }

        connChanged = connect(thread, &LocationsThread::locationChanged, this, &LocationsThreadMgr::_locationChanged);
        connCleared = connect(thread, &LocationsThread::locationCleared, this, &LocationsThreadMgr::_locationCleared);

        thread->start();
    }
}

void LocationsThreadMgr::stopThread()
{
    if (thread != nullptr) {

        disconnect(connChanged);
        disconnect(connCleared);

        thread->stop();
        thread->wait();
        delete thread;
        thread = nullptr;
    }
}

// Transitive slots to reemit signals

void LocationsThreadMgr::_locationChanged(const QString &name)
{
    emit locationChanged(name);
}

void LocationsThreadMgr::_locationCleared()
{
    emit locationCleared();
}
