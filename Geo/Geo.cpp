/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	Reference for coordinate format conversion formulas:
	https://en.wikipedia.org/wiki/Geographic_coordinate_conversion
*/
#include "Geo.hpp"

#include <cmath>

#include <iostream>

// Degree

Degree::Degree(double val) {
	decDegrees = val;
}

double Degree::deg() const {
	double intpart;
	modf(fabs(decDegrees), &intpart);
	return intpart;
}

double Degree::decMin() const {
	return 60 * (fabs(decDegrees) - deg());
}

double Degree::dec() const {
	return decDegrees;
}

// Latitude

Latitude::Latitude(double val) : Degree(val) {}

Latitude::Cardinal Latitude::card() const {
	return decDegrees > 0 ? Latitude::Cardinal::NORTH : Latitude::Cardinal::SOUTH;
}

// Longitude

Longitude::Longitude(double val) : Degree(val) {}

Longitude::Cardinal Longitude::card() const {
	return decDegrees > 0 ? Longitude::Cardinal::EAST : Longitude::Cardinal::WEST;
}
