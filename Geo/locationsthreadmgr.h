/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LOCATIONSTHREADMGR_H
#define LOCATIONSTHREADMGR_H

#include <QObject>

class LocationsThread;
class LocationManager;

class LocationsThreadMgr: public QObject
{
    Q_OBJECT

public:
    LocationsThreadMgr(const LocationManager& locMgr, QObject *parent = Q_NULLPTR);

    void startThread();
    void stopThread();

signals:
    void locationChanged(const QString &name);
    void locationCleared();

private:
    LocationsThread *thread = nullptr;
    const LocationManager& locMgr;

    QMetaObject::Connection connChanged;
    QMetaObject::Connection connCleared;

private slots:
    void _locationChanged(const QString &name);
    void _locationCleared();
};

#endif // LOCATIONSTHREADMGR_H
