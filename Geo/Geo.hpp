/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __VARIOGUI_GEO_HPP
#define __VARIOGUI_GEO_HPP

class Degree
{
protected:
	double decDegrees;

public:
	Degree(double val);

	double deg() const;		// integer degrees only
	double decMin() const;	// decimal minutes
	double dec() const;		// decimal degrees
};

class Latitude : public Degree
{
public:
	enum Cardinal {NORTH, SOUTH};

	Latitude(double val);

	Latitude::Cardinal card() const;
};

class Longitude: public Degree
{
public:
	enum Cardinal {EAST, WEST};

	Longitude(double val);

	Longitude::Cardinal card() const;
};

#endif
