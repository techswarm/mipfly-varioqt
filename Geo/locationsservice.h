/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LOCATIONSSERVICE_H
#define LOCATIONSSERVICE_H

#include <QString>
#include <QList>
#include <QFile>

#include <memory>

namespace Vario
{
    struct Location {
        QString name;
        qreal lat;
        qreal lon;
        int alt;
    };
}


class LocationsService: public QObject
{
    Q_OBJECT
public:
    LocationsService();

    void addLocation(const Vario::Location &location);
    void remove(const Vario::Location &location);
    Vario::Location* locationByName(const QString &name);

    void save();
    const QList<Vario::Location> &getAll() const;

    const Vario::Location currentLocation();
    void setCurrent(const Vario::Location &loc);
    void clearCurrent();

    const Vario::Location& locationAtIndex(int i) const;

    bool isSetManually() const;
    void setManually(bool val) const;

signals:
    void locationChanged(const QString &name) const;
    void locationCleared() const;

private:
    QList<Vario::Location> list;
    QFile locationsFile;
    QString lastLocation = "";
};

#endif // LOCATIONSSERVICE_H
