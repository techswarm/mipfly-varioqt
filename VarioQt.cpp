/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "VarioQt.h"
#include <QDebug>

NmeaParser *VarioQt::getGpsParser() const
{
    return gpsParser_;
}

void VarioQt::setGpsParser(NmeaParser *gpsParser)
{
    gpsParser_ = gpsParser;
}

VarioCompute *VarioQt::getVarioCompute() const
{
    return varioCompute_;
}

void VarioQt::setVarioCompute(VarioCompute *varioCompute)
{
    varioCompute_ = varioCompute;
}

NmeaParser *VarioQt::getVarioParser() const
{
    return varioParser_;
}

void VarioQt::setVarioParser(NmeaParser *varioParser)
{
    varioParser_ = varioParser;
}

LastLift *VarioQt::getLastLift() const
{
    return lastLift_;
}

void VarioQt::setLastLift(LastLift *lastLift)
{
    lastLift_ = lastLift;
}

Wind *VarioQt::getWind() const
{
    return wind_;
}

void VarioQt::setWind(Wind *wind)
{
    wind_ = wind;
}

NameManager *VarioQt::getNameManager() const
{
    return nameManager_;
}

void VarioQt::setNameManager(NameManager *nameManager)
{
    nameManager_ = nameManager;
}

LocationManager *VarioQt::getLocationManager() const
{
    return locationManager_;
}

void VarioQt::setLocationManager(LocationManager *value)
{
    locationManager_ = value;
}

TakeoffThread *VarioQt::getTakeOffThread() const
{
    return takeOffThread_;
}

void VarioQt::setTaleOffThread(TakeoffThread *taleOffThread)
{
    takeOffThread_ = taleOffThread;
}

LocationsThreadMgr *VarioQt::getLocationsThreadMgr() const
{
    return locationsThreadMgr_;
}

void VarioQt::setLocationsThreadMgr(LocationsThreadMgr *locationsThreadMgr)
{
    locationsThreadMgr_ = locationsThreadMgr;
}

GPSThread *VarioQt::getVarioSerialThread() const
{
    return varioSerialThread_;
}

void VarioQt::setVarioSerialThread(GPSThread *varioSerialThread)
{
    varioSerialThread_ = varioSerialThread;
}

WindManager *VarioQt::getWindManager() const
{
    return windManager_;
}

void VarioQt::setWindManager(WindManager *windManager)
{
    windManager_ = windManager;
}

GlideManager *VarioQt::getGlideManager() const
{
    return glideManager_;
}

void VarioQt::setGlideManager(GlideManager *glideManager)
{
    glideManager_ = glideManager;
}

Fb2SPIThread *VarioQt::getFb2SPIThread() const
{
    return fb2SPIThread_;
}

void VarioQt::setFb2SPIThread(Fb2SPIThread *fb2SPIThread)
{
    fb2SPIThread_ = fb2SPIThread;
}

void VarioQt::pushNewWidget(QWidget *wdg)
{
    orderedWindgetList_.append(wdg);
}

void VarioQt::deleteLastWidget()
{
    orderedWindgetList_.removeLast();
}

QWidget *VarioQt::getLastWidget()
{
    return orderedWindgetList_.last();
}

bool VarioQt::isActiveWidget(QWidget *wdg)
{
    if(wdg == orderedWindgetList_.last())return true;
    return false;
}

VarioSound *VarioQt::getVarioSound() const
{
    return varioSound_;
}

void VarioQt::setVarioSound(VarioSound *varioSound)
{
    varioSound_ = varioSound;
}

InputThread *VarioQt::getInputThread() const
{
    return inputThread_;
}

void VarioQt::setInputThread(InputThread *inputThread)
{
    inputThread_ = inputThread;
}

MainUI *VarioQt::getMainUI() const
{
    return mainUI_;
}

void VarioQt::setMainUI(MainUI *mainUI)
{
    mainUI_ = mainUI;
}

QTranslator *VarioQt::getTranslator() const
{
    return translator_;
}

void VarioQt::setTranslator(QTranslator *translator)
{
    translator_ = translator;
}

AirSpaceThread *VarioQt::getAirSpaceThread() const
{
    return airSpaceThread_;
}

void VarioQt::setAirSpaceThread(AirSpaceThread *airSpaceThread)
{
    airSpaceThread_ = airSpaceThread;
}

Elevation *VarioQt::getElevation() const
{
    return elevation_;
}

void VarioQt::setElevation(Elevation *elevation)
{
    elevation_ = elevation;
}

LiveTrack *VarioQt::getLiveTrack() const
{
    return liveTrack_;
}

void VarioQt::setLiveTrack(LiveTrack *liveTrack)
{
    liveTrack_ = liveTrack;
}

NetStats *VarioQt::getNetStats() const
{
    return netStats_;
}

void VarioQt::setNetStats(NetStats *netStats)
{
    netStats_ = netStats;
}

HGTContainer *VarioQt::getHGTContainer() const
{
    return hGTContainer_;
}

void VarioQt::setHGTContainer(HGTContainer *hGTContainer)
{
    hGTContainer_ = hGTContainer;
}

waypointManager *VarioQt::getWaypointManager() const
{
    return waypointManager_;
}

void VarioQt::setWaypointManager(waypointManager *waypointManager)
{
    waypointManager_ = waypointManager;
}

Task *VarioQt::getTask() const
{
    return task_;
}

void VarioQt::setTask(Task *task)
{
    task_ = task;
}

Glider *VarioQt::getGlider() const
{
    return glider_;
}

void VarioQt::setGlider(Glider *glider)
{
    glider_ = glider;
}

TermalingDetector *VarioQt::getTermalingDetector() const
{
    return termalingDetector_;
}

void VarioQt::setTermalingDetector(TermalingDetector *termalingDetector)
{
    termalingDetector_ = termalingDetector;
}

bool VarioQt::getTtyMode() const
{
    return ttyMode;
}

void VarioQt::setTtyMode(bool value)
{
    ttyMode = value;
}

QString VarioQt::getMnuFontName() const
{
    return mnuFontName;
}
void VarioQt::setMnuFontName(QString mnuFontName) {
    qDebug()<<"Menu font name : "<<mnuFontName;
    this->mnuFontName = mnuFontName;
}
int VarioQt::getMnuFontSize() const
{
    return mnuFontSize;
}
void VarioQt::setMnuFontSize(int mnuFontSize) {
    qDebug()<<"Menu font size : "<<mnuFontSize;
    this->mnuFontSize = mnuFontSize;
}

TaskPlot *VarioQt::getTaskPlot() const
{
    return taskPlot_;
}

void VarioQt::setTaskPlot(TaskPlot *taskPlot)
{
    taskPlot_ = taskPlot;
}

FaiAssistCompute *VarioQt::getFaiAssistCompute() const
{
    return faiAssistCompute_;
}

void VarioQt::setFaiAssistCompute(FaiAssistCompute *faiAssistCompute)
{
    faiAssistCompute_ = faiAssistCompute;
}

BTServer *VarioQt::getBTServer() const
{
    return BTServer_;
}

void VarioQt::setBTServer(BTServer *BTServer)
{
    BTServer_ = BTServer;
}

LoggerInterface *VarioQt::getLoggerInterface() const
{
    return loggerInterface_;
}

void VarioQt::setLoggerInterface(LoggerInterface *loggerInterface)
{
    loggerInterface_ = loggerInterface;
}

TrianglePlot *VarioQt::getTrianglePlot() const
{
    return trianglePlot_;
}

void VarioQt::setTrianglePlot(TrianglePlot *trianglePlot)
{
    trianglePlot_ = trianglePlot;
}

OLC *VarioQt::getOlc() const
{
    return olc_;
}

void VarioQt::setOlc(OLC *olc)
{
    olc_ = olc;
}

FlightLogThread *VarioQt::getFlightlogThread() const
{
    return flightlogThread_;
}

void VarioQt::setFlightlogThread(FlightLogThread *flightlogThread)
{
    flightlogThread_ = flightlogThread;
}

bool VarioQt::getRecoveryMode() const
{
    return recoveryMode;
}

void VarioQt::setRecoveryMode(bool value)
{
    recoveryMode = value;
}
