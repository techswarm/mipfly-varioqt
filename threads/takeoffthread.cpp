/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "takeoffthread.h"

#include <QSettings>
#include <QtMath>

#include "NMEA/NMEAParser.h"
#ifdef USE_SOUND
#include "threads/playsoundthread.h"
#endif

#include <QtDebug>

TakeoffThread::TakeoffThread(const NmeaParser &parser, const NmeaParser &varParser,
                             #ifdef USE_SOUND
                             const PlaySoundThread &soundThread,
                             VarioSound &varioSound,
                             #endif
                             QObject *parent)
    : StoppableThread(parent) , parser(parser)
    #ifdef USE_SOUND
    , soundThread(soundThread)
    #endif
{
    connect(&parser, SIGNAL(speedKmph(qreal)), this, SLOT(setSpeed(qreal)));
    connect(&varParser, SIGNAL(vSpeed(int)), this, SLOT(setVSpeed(int)));

    varioSound_ = &varioSound;
}

void TakeoffThread::setSpeed(qreal newSpeed) {
    speed = newSpeed;
}

void TakeoffThread::setVSpeed(int speed) {
    vSpeed = speed;
}

void TakeoffThread::setTakeoffSpeed(qreal speed)
{
    takeoffSpeed = speed;
}

int TakeoffThread::getFlightId() const
{
    return flightId;
}

int TakeoffThread::getTakeoffHeight() const
{
    return takeoffHeight;
}

void TakeoffThread::setTakeoffHeight(int value)
{
    takeoffHeight = value;
}

Coord TakeoffThread::getTakeoffPos() const
{
    return takeoffPos;
}

void TakeoffThread::setTakeoffPos(const Coord &value)
{
    takeoffPos = value;
}

bool TakeoffThread::getFlying() const
{
    return flying;
}

void TakeoffThread::updateVarioFlyingStatus(){
#ifdef BICICLE_MODE
#else
    if(varioSound_ != nullptr)
        varioSound_->setFlyingStatus(flying);
#endif
}

void TakeoffThread::run() {
#ifdef BICICLE_MODE
    const int takeoffLimit = 5;
    const int landLimit = 3*60;
    const int vspeedLimit = 0;
#else
    const int takeoffLimit = 5;
    const int landLimit = 15;
    const int vspeedLimit = 70;
#endif

    while (!done) {
        if(flying)
        {
            if (speed < 3.0 && qAbs(vSpeed) < 50) {
                count++;
                //                qDebug() << count << " " << speed;

                if (count == landLimit) {
                    // Glider landed
                    count = 0;
                    flying = false;
#ifdef USE_SOUND
                    soundThread.playLanded();
#endif
                    emit landed();
                    updateVarioFlyingStatus();

                }
            }
            else
            {
                count = 0;
            }
        }

        if (!flying) {
            if (speed > 10.0 && true) {

                count++;

                //                qDebug() << count << " " << speed;
                //if (count == 5) {
                if (count >= takeoffLimit && qAbs(vSpeed) > vspeedLimit) {
                    // Glider takeoff
                    qDebug()<<"Takeoff";
                    count = 0;
                    flying = true;
#ifdef USE_SOUND
                    soundThread.playTakeoff();
#endif
                    emit takeoff();
                    QSettings settings;
                    flightId = settings.value("takeOffThread/flightId",0).toInt();
                    flightId++;
                    if(flightId==1000)flightId=0;
                    settings.setValue("takeOffThread/flightId",flightId);
                    updateVarioFlyingStatus();
                }
            }
            else
            {
                count = 0;
            }
        }

        QThread::sleep(1);
    }
}
