/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FB2SPITHREAD_H
#define FB2SPITHREAD_H

#include <fstream>

#include <linux/fb.h> // framebuffer structs, defines etc.


#include "fbfmwk/fbfmwk.h"
#include "stoppablethread.h"

class Fb2SPIThread: public StoppableThread
{
public:
    Fb2SPIThread();
    ~Fb2SPIThread();

    void switchTheme();

protected:
    void run() Q_DECL_OVERRIDE;

private:

    bool darkTheme = false;
    bool performForcedUpdate = false;

    static const int HEIGHT = 240;
    static const int WIDTH = 320;

    int fd;

    struct fb_fix_screeninfo finfo;
     struct fb_var_screeninfo vinfo;


    void writeSpi(const uint8_t *data, size_t size);
    void writeMem(uint8_t *vmem);
    void initSpi();
    pixel_t getPixel(long x, long y, uint8_t *p);
    long getPixelLocation(long x, long y, uint8_t *p);
    void computeLuTable(uint8_t *vmem16);

    void fillBuffer(uint8_t *buf, int startX, int endX, unsigned int *lookupPtr);

public slots:
    void forceUpdate();
};

#endif // FB2SPITHREAD_H
