/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "inputThread.h"
#include "utils/vario_common.h"
#include <QDebug>
#include <QSettings>

InputThread::InputThread()
{
    //read key codes from settings
    QSettings settings;

    keyShortPress1 = settings.value("keyShortPress1",208).toInt();
    keyShortPress2 = settings.value("keyShortPress2",168).toInt();
    keyShortPress3 = settings.value("keyShortPress3",163).toInt();

    keyLongPress1 = settings.value("keyLongPress1",165).toInt();
    keyLongPress2 = settings.value("keyLongPress2",0).toInt();
    keyLongPress3 = settings.value("keyLongPress3",158).toInt();

    init();
}

void InputThread::setKeyShortPress1(int value)
{
    keyShortPress1 = value;
    //save remote key settings
    QSettings settings;
    settings.setValue("keyShortPress1",keyShortPress1);
    settings.sync();
}

void InputThread::setKeyShortPress2(int value)
{
    keyShortPress2 = value;
    //save remote key settings
    QSettings settings;
    settings.setValue("keyShortPress2",keyShortPress2);
    settings.sync();
}

void InputThread::setKeyShortPress3(int value)
{
    keyShortPress3 = value;
    //save remote key settings
    QSettings settings;
    settings.setValue("keyShortPress3",keyShortPress3);
    settings.sync();
}

void InputThread::setKeyLongPress1(int value)
{
    keyLongPress1 = value;
    //save remote key settings
    QSettings settings;
    settings.setValue("keyLongPress1",keyLongPress1);
    settings.sync();
}

void InputThread::setKeyLongPress2(int value)
{
    keyLongPress2 = value;
    //save remote key settings
    QSettings settings;
    settings.setValue("keyLongPress2",keyLongPress2);
    settings.sync();
}

void InputThread::setKeyLongPress3(int value)
{
    keyLongPress3 = value;
    //save remote key settings
    QSettings settings;
    settings.setValue("keyLongPress3",keyLongPress3);
    settings.sync();
}

void InputThread::setEmitKeypress(bool value)
{
    emitKeypress = value;
}

void InputThread::saveSettings()
{
    QSettings settings;
    settings.setValue("keyShortPress1",keyShortPress1);
    settings.setValue("keyShortPress2",keyShortPress2);
    settings.setValue("keyShortPress3",keyShortPress3);

    settings.setValue("keyLongPress1",keyLongPress1);
    settings.setValue("keyLongPress2",keyLongPress2);
    settings.setValue("keyLongPress3",keyLongPress3);
    settings.sync();
}

void InputThread::init()
{
#ifndef __ARMEL__
    device = (char*)"/dev/input/event13";
#else
    device = (char*)"/dev/input/event1";
#endif
    if ((fd = open (device, O_RDONLY)) == -1){
        qDebug()<<device<<" is not a vaild device.";
    }
}

void InputThread::run()
{
    while (!done){
        if ((rd = read (fd, ev, size * 64)) < size)
        {
            qDebug()<<"Error reading input";
            QThread::msleep(10000);
            //try to reinitialise
            init();
            continue;
        }

        value = ev[0].value;


        if (value != ' ' && ev[1].value == 1 && ev[1].type == 1){ // Only read the key press event
            qDebug()<<ev[1].code;

            emit keyCode(ev[1].code);

            /*
            switch (ev[1].code) {
            case 208:
                emit pressed(1);
                break;
            case 168:
                emit pressed(2);
                break;
            case 163:
                emit pressed(3);
                break;
            case 158:
                emit longPressed(3);
                break;
            case 165:
                emit longPressed(1);
                break;
            default:
                break;
            }
            */
            if(emitKeypress == true)
            {
                if(ev[1].code == keyShortPress1)emit pressed(Vario::GPIO1);
                if(ev[1].code == keyShortPress2)emit pressed(Vario::GPIO2);
                if(ev[1].code == keyShortPress3)emit pressed(Vario::GPIO3);

                if(ev[1].code == keyLongPress1)emit longPressed(Vario::GPIO1);
                if(ev[1].code == keyLongPress2)emit longPressed(Vario::GPIO2);
                if(ev[1].code == keyLongPress3)emit longPressed(Vario::GPIO3);
            }
        }
    }
}

