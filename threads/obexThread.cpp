/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "obexThread.h"
#include <QDir>
#include "utils/fileUtils.h"
#include <QDebug>
#include <QThread>
#include <QSettings>
#include <Warning.h>
#include "VarioQt.h"

ObexThread::ObexThread()
{
    FileUtils::remove(basePath+"/*");
}

void ObexThread::run()
{
    while(!done)
    {
        QThread::sleep(5);
        processFiles();
    }
}

void ObexThread::processFiles()
{
    QDir dir(basePath);
    QStringList files = dir.entryList();
    foreach (QString file, files) {
        //check if the file isn't still downloading
        if(file == "." || file == "..")continue;
        if(!FileUtils::inUse(basePath+"/"+file))
        {
            QFileInfo fi(basePath+"/"+file);
            QString ext = fi.suffix();
            if(ext == "mbtiles")
            {
                emit fileReceivedMsg(tr("Map received"));
                FileUtils::move(basePath+"/"+file,"/mnt/mipfly/maps");
            }
            if(ext == "txt")
            {
                emit fileReceivedMsg(tr("Airspace received"));
                FileUtils::move(basePath+"/"+file,"/mnt/mipfly/airspace");
            }
            if(ext == "wpt")
            {
                emit fileReceivedMsg(tr("Waypoints received"));
                FileUtils::move(basePath+"/"+file,"/mnt/mipfly/waypointFiles");
            }
            if(ext == "xctsk")
            {
                //create dir if it doesn't exist
                system("mkdir /mnt/mipfly/taskFiles");
                emit fileReceivedMsg(tr("*.xctsk received"));
                FileUtils::move(basePath+"/"+file,"/mnt/mipfly/taskFiles");
            }
            if(ext == "qm")
            {
                emit fileReceivedMsg(tr("*.qm received"));
                FileUtils::move(basePath+"/"+file,"/mnt/mipfly/languages");
                QSettings settings;
                QString language = settings.value("language","english.qm").toString();
                VarioQt::instance().getTranslator()->load("languages/"+language);
            }

            FileUtils::remove(basePath+"/"+file);
        }
        else
        {
            qDebug()<<"File is still downloading";
        }
    }
}
