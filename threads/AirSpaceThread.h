/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef AIRSPACETHREAD_H
#define AIRSPACETHREAD_H
#include <QWidget>
#include "stoppablethread.h"
#include "navUtils/coord.hpp"
#include "navUtils/OpenAirParser.h"
#include <QMutex>

class AirSpaceThread : public StoppableThread
{
    Q_OBJECT
    void process();
public:
    QMutex accessible;
    AirSpaceThread();
    ~AirSpaceThread();

    Coord mapCenter;
    Coord drawableAssert;
    int altitudeMeters;
    OpenAirParser *_p = nullptr;
    QString oldAirspaceFile;
    AirSpaceDistanceType violationLevel = AirSpaceDistanceType::OK;

    float getNear() const;
    void setNear(float value);

    float getVeryNear() const;
    void setVeryNear(float value);

    float getVerticalWarningMeters() const;
    void setVerticalWarningMeters(float value);

    float getDrawHLimit() const;
    void setDrawHLimit(float value);

    bool getAirspaceFrontUp() const;
    void setAirspaceFrontUp(bool value);

public slots:
    void setCenter(Coord coord);
    void setAltitude(int meters);
protected:
    void run() Q_DECL_OVERRIDE;
private:
    QPainterPath pathNear;
    QPainterPath pathVeryNear;
    QPainterPath pathDrawDistance;

    bool airspaceFrontUp;

    float drawHLimit = 5000;
    float near;
    float veryNear;
    int drawDistance = 100000;
    float verticalWarningMeters;
    void assesConflicts(int airspaceIndex, const QPainterPath &local, bool drawAssert);
};

#endif // AIRSPACETHREAD_H
