/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ADBTCPThread.h"
#include <QTcpSocket>
#include <QHostAddress>
#include "NMEA/NMEAParser.h"
#include <QTimer>



ADBTCPThread::ADBTCPThread()
{
    _pSocket = new QTcpSocket(this);
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(run()));
    timer->start(5000);
    connect(this,SIGNAL(connectToHost()),this,SLOT(doConnectToHost()),Qt::QueuedConnection);
}

ADBTCPThread::~ADBTCPThread()
{
    if(_pSocket == nullptr)return;
    if(_pSocket->state() == QAbstractSocket::ConnectedState)
    {
        _pSocket->disconnectFromHost();
        delete _pSocket;
    }
}

void ADBTCPThread::GPSNMEAReceived(QString msg)
{
    if(_pSocket->state() == QAbstractSocket::ConnectedState)
    {
        _pSocket->write(msg.toUtf8(),msg.length());
        //_pSocket->write((NmeaParser::addChecksum("$POV,E,2.15")+"\r\n").toUtf8());
        _pSocket->flush();
    }
}

void ADBTCPThread::run()
{
    if(_pSocket->state() == QAbstractSocket::UnconnectedState)
    {
        if(doTCPfw)
        {
            setupTCPfw();
            doTCPfw = false;
        }
        else
        {
            emit doConnectToHost();
            doTCPfw = true;
        }

    }
}

void ADBTCPThread::setupADBUSB()
{
    qDebug()<<"adb usb";
    system("adb usb");
}

void ADBTCPThread::setupTCPfw()
{
    qDebug()<<"adb forward tcp:4353 tcp:4353";
    system("adb forward tcp:4353 tcp:4353");
}

void ADBTCPThread::doConnectToHost()
{
    _pSocket->connectToHost(QHostAddress("127.0.0.1"), 4353);
    qDebug()<<"Connecting to TCP host";
    _pSocket->waitForConnected();
}

double ADBTCPThread::getSeaLevel(double pressure, double altitude) {
    return ((double) pressure
            / pow(1.0f - ((double) altitude / 44330.0f), 5.255f));
}

void ADBTCPThread::setVspeed(int vSpeedCMPS)
{
    if(_pSocket == nullptr)return;
    if(_pSocket->state() == QAbstractSocket::ConnectedState)
    {
        //$POV,E,2.15
        QString msg = "$POV,E,";
        msg+=QString::number((float)vSpeedCMPS/100);

        double pressure = getSeaLevel(1013.25,0-baroHeightMeters);
        msg+=",P,"+QString::number(pressure);

        msg = NmeaParser::addChecksum(msg);
        msg+="\r\n";
        _pSocket->write(msg.toUtf8(),msg.length());
        _pSocket->flush();

    }
}

void ADBTCPThread::setBaroHeight(int baroHeightm)
{
    baroHeightMeters = baroHeightm;
}


