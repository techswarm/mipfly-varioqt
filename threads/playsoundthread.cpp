/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "playsoundthread.h"

#include <QSettings>

#include "sound/varioSound.h"
#include "GPIO/GPIOpins.h"
#include "utils/vario_common.h"
#include "VarioQt.h"

#include <iostream>

PlaySoundThread::PlaySoundThread()
{
    VarioQt::instance().getVarioSound()->varioSoundInit();
}

void PlaySoundThread::playAppBing() const {
    VarioQt::instance().getVarioSound()->varioSoundPlayFile(QSettings().value(Vario::SETTINGS_ONSND, "resources/sounds/bell.wav").toString().toUtf8().data());
}

void PlaySoundThread::playTakeoff() const {
    VarioQt::instance().getVarioSound()->varioSoundPlayFile(QSettings().value(Vario::SETTINGS_TAKEOFFSND, "resources/sounds/airland.wav").toString().toUtf8().data());
}

void PlaySoundThread::playLanded() const {
    VarioQt::instance().getVarioSound()->varioSoundPlayFile(QSettings().value(Vario::SETTINGS_LANDSND, "resources/sounds/applause3.wav").toString().toUtf8().data());
}

void PlaySoundThread::playWarning() const {
    VarioQt::instance().getVarioSound()->varioSoundPlayFile(QSettings().value(Vario::SETTINGS_LANDSND, "resources/sounds/warning.wav").toString().toUtf8().data());
}

void PlaySoundThread::run() {
    while (!done) {
        VarioQt::instance().getVarioSound()->varioSoundTask();
    }
}
