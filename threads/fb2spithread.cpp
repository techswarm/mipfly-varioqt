/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "fb2spithread.h"

#include <QtDebug>
#include <QSettings>

#include <iostream>
#include <fstream>
#include <cstdio>

//#include <linux/spi/spidev.h>
#include "utils/spidev.h"
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <sys/mman.h>

#include <chrono>

#include "utils/vario_common.h"

typedef std::chrono::high_resolution_clock Clock;

static const int BITS_PER_PIXEL = 32;
static const int VAL1 = 1;

static const uint8_t CMDWRLINE = 0x80;
static const uint8_t TRAILER = 0x00;

const int Fb2SPIThread::HEIGHT;
const int Fb2SPIThread::WIDTH;

static uint8_t mode;
static uint8_t bits = 8;
static uint32_t speed = 2000000;
static uint16_t delay;

static const int MSGLINES = 97;
static const int MSGBUFLEN = MSGLINES * 42 + 2;

static const pixel_t NON_BLACK = 4278190080;
static const pixel_t VALUE1 = 4293913575;

static const pixel_t VALUE = NON_BLACK;

unsigned int luTable[240*320];
unsigned int luTablePos = 0;

unsigned int *luptr;
uint8_t shadow[240*320*4];

static void pabort(const char *s)
{
    perror(s);
    abort();
}

char reverseByte(char b) {
  b = (b & 0xF0) >> 4 | (b & 0x0F) << 4;
  b = (b & 0xCC) >> 2 | (b & 0x33) << 2;
  b = (b & 0xAA) >> 1 | (b & 0x55) << 1;
  return b;
}

#define TXBUFLEN       80*(40+2)+2

#define ORIG_Y_MAX 40

#define DISPLAY_WIDTH 1920

void Fb2SPIThread::computeLuTable(uint8_t *vmem16)
{

    for (int x=0;x<240;x++) {
        for (int y=0;y<40;y++) {
            for (int i=1;i<=8;i++) {
                luTable[luTablePos++] = (unsigned int) getPixelLocation(239 - x, y * 8 + (8 - i), vmem16);
            }
        }
    }

}

void Fb2SPIThread::writeMem(uint8_t *vmem16)
{
    uint8_t arr[TXBUFLEN];
    uint8_t arr2[TXBUFLEN];
    uint8_t arr3[TXBUFLEN];

    //return;


    memcpy((void *)shadow,(void *)vmem16,240*320*4);

    luTablePos = 0;

    luptr = luTable;

#if 0
    fillBuffer(arr, 0, 80, luptr);
    fillBuffer(arr2, 80, 160, luptr);
    fillBuffer(arr3, 160, 240, luptr);
#endif

    uint8_t *buf = arr;
    *buf = CMDWRLINE;
    buf++;
    for (int x=0;x<80;x++) {
        *buf = reverseByte(x+1);
        buf++;
        for (int y=0;y<40;y++) {
            *buf = 0x00;
            for (int i=0;i<8;i++) {
                //val = getPixel(239 - x, y * 8 + (8 - i), vmem16);
                // *buf |=  (val != 4278190080 ? 1 : 0) << i;

//                if(*((pixel_t*)(luTable[luTablePos++])) == 4278190080)
//                if ((*((pixel_t*) *luptr) != NON_BLACK))
//                    std::cout << *((pixel_t*) *luptr) << std::endl;

                bool match;


                if (darkTheme) {
                    pixel_t pixel = *((pixel_t*) *luptr);
                    if((pixel & 0xFF) > 0x80 && (pixel & 0xFF00) > 0x8000 && (pixel & 0xFF0000) > 0x800000) match = 0;
                    else match = 1;
                } else {
                    pixel_t pixel = *((pixel_t*) *luptr);
                    if((pixel & 0xFF) > 0x80 && (pixel & 0xFF00) > 0x8000 && (pixel & 0xFF0000) > 0x800000) match = 1;
                    else match = 0;
                }

                if(match)

                {
                    *buf |= 1 << i;
                }
                luptr++;
            }
            buf++;
        }
        *buf = TRAILER;
        buf++;
    }
    *buf = TRAILER;
    buf = arr2;
    *buf = CMDWRLINE;
    buf++;
    for (int x=80;x<160;x++) {
        *buf = reverseByte(x+1);
        buf++;
        for (int y=0;y<40;y++) {
            *buf = 0x00;
            for (int i=0;i<8;i++) {
                //val = getPixel(239 - x, y * 8 + (8 - i), vmem16);
                // *buf |=  (val != 4278190080 ? 1 : 0) << i;
                bool match;

                if (darkTheme) {
                    pixel_t pixel = *((pixel_t*) *luptr);
                    if((pixel & 0xFF) > 0x80 && (pixel & 0xFF00) > 0x8000 && (pixel & 0xFF0000) > 0x800000) match = 0;
                    else match = 1;
                } else {
                    pixel_t pixel = *((pixel_t*) *luptr);
                    if((pixel & 0xFF) > 0x80 && (pixel & 0xFF00) > 0x8000 && (pixel & 0xFF0000) > 0x800000) match = 1;
                    else match = 0;
                }

                if(match)

                {
                    *buf |= 1 << i;
                }
                luptr++;
            }
            buf++;
        }
        *buf = TRAILER;
        buf++;
    }
    *buf = TRAILER;

    buf = arr3;
    *buf = CMDWRLINE;
    buf++;
    for (int x=160;x<240;x++) {
        *buf = reverseByte(x+1);
        buf++;
        for (int y=0;y<40;y++) {
            *buf = 0x00;
            for (int i=0;i<8;i++) {
                //val = getPixel(239 - x, y * 8 + (8 - i), vmem16);
                // *buf |=  (val != 4278190080 ? 1 : 0) << i;
                bool match;

                if (darkTheme) {
                    pixel_t pixel = *((pixel_t*) *luptr);
                    if((pixel & 0xFF) > 0x80) match = 0;
                    else match = 1;
                } else {
                    pixel_t pixel = *((pixel_t*) *luptr);
                    if((pixel & 0xFF) > 0x80) match = 1;
                    else match = 0;
                }

                if(match)

                {
                    *buf |= 1 << i;
                }
                luptr++;
            }
            buf++;
        }
        *buf = TRAILER;
        buf++;
    }
    *buf = TRAILER;





    //auto t1 = Clock::now();
    writeSpi(arr, TXBUFLEN);
    //auto t2 = Clock::now();
    writeSpi(arr2, TXBUFLEN);
    writeSpi(arr3, TXBUFLEN);



//    std::cout << "Delta t2-t1: "
//                  << std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count()
//                  << " us" << std::endl;

}

void Fb2SPIThread::fillBuffer(uint8_t *arr, int startX, int endX, unsigned int *lookupPtr)
{
    uint8_t *buf = arr;
    *buf = CMDWRLINE;
    buf++;
    for (int x=startX; x<endX; x++) {
        *buf = reverseByte(x+1);
        buf++;
        for (int y=0;y<40;y++) {
            *buf = 0x00;
            for (int i=0;i<8;i++) {
                //val = getPixel(239 - x, y * 8 + (8 - i), vmem16);
                // *buf |=  (val != 4278190080 ? 1 : 0) << i;

//                if(*((pixel_t*)(luTable[luTablePos++])) == 4278190080)
                bool match;

                if (darkTheme) {
                    match = (*((pixel_t*) *lookupPtr) == NON_BLACK);
                } else {
                    match = (*((pixel_t*) *lookupPtr) != NON_BLACK);
                }
                if (match) {
                    *buf |= 1 << i;
                }
                lookupPtr++;
            }
            buf++;
        }
        *buf = TRAILER;
        buf++;
    }
    *buf = TRAILER;
}

void Fb2SPIThread::forceUpdate()
{
    performForcedUpdate = true;
}

Fb2SPIThread::Fb2SPIThread()
{
    QSettings settings;
    darkTheme = settings.value(Vario::SETTINGS_DARKTHEME, true).toBool();

    fd = open( "/dev/spidev2.0", O_RDWR);

    mode |= SPI_CS_HIGH;

    initSpi();
}

Fb2SPIThread::~Fb2SPIThread()
{
    close(fd);
}

void Fb2SPIThread::switchTheme()
{
    darkTheme = !darkTheme;
    QSettings settings;
    settings.setValue(Vario::SETTINGS_DARKTHEME, darkTheme);
    settings.sync();
}

#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))

void Fb2SPIThread::run()
{
    int fb_fd = -1;

    qDebug() << "spi thread started";

/*
    fbfmwk_init(0, BITS_PER_PIXEL);

    for (x = 0; x < HEIGHT; x++) {
        for (y = 0; y < WIDTH; y++) {
            pixel_t val = pixel(x,y);

            std::cout << (val == 60391 ? 0 : 1) << ' ';

//            writeLine(val, x, y);
        }
        std::cout << '\n';
    }

    fbfmwk_cleanup();
*/

    // -- Framebuffer setup --
    if((fb_fd = open("/dev/fb0", O_RDWR)) < 0) {
        perror("open");
    }

    // Get variable screen information
    ioctl(fb_fd, FBIOGET_VSCREENINFO, &vinfo);
    vinfo.grayscale = 0;
    vinfo.bits_per_pixel = BITS_PER_PIXEL;
    ioctl(fb_fd, FBIOPUT_VSCREENINFO, &vinfo);
    ioctl(fb_fd, FBIOGET_VSCREENINFO, &vinfo);

    // Get fixed screen information
    ioctl(fb_fd, FBIOGET_FSCREENINFO, &finfo);

    //qDebug()<<"vinfo.bits_per_pixel"<<vinfo.bits_per_pixel;
    //qDebug()<<"finfo.line_length"<<finfo.line_length;

    long screensize = vinfo.yres_virtual * finfo.line_length;

    uint8_t *fbp = (uint8_t *) mmap(0, screensize, PROT_READ, MAP_SHARED, fb_fd, (off_t)0);

    computeLuTable(shadow);

    while (!done) {
        writeMem(fbp);
        for(int i=0;i<8;i++)
        {
            QThread::msleep(10);
            if(performForcedUpdate)
            {
                performForcedUpdate = false;
                break;
            }
        }
    }

    // Close fb stuff
    if(close(fb_fd) < 0) {
        perror("close");
    }

    if(munmap(fbp, screensize) < 0) {
        perror("munmap");
    }

    qDebug() << "spi thread finished";
}

void Fb2SPIThread::initSpi() {
    qDebug() << "Sizeof SPI " << sizeof(struct spi_ioc_transfer);

    /*
         * spi mode
         */
        int ret = ioctl(fd, SPI_IOC_WR_MODE, &mode);
        if (ret == -1)
            pabort("can't set spi mode");

        ret = ioctl(fd, SPI_IOC_RD_MODE, &mode);
        if (ret == -1)
            pabort("can't get spi mode");

        /*
         * bits per word
         */
        ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
        if (ret == -1)
            pabort("can't set bits per word");

        ret = ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &bits);
        if (ret == -1)
            pabort("can't get bits per word");

        /*
         * max speed hz
         */
        ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
        if (ret == -1)
            pabort("can't set max speed hz");

        ret = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
        if (ret == -1)
            pabort("can't get max speed hz");

        printf("spi mode: %d\n", mode);
        printf("bits per word: %d\n", bits);
        printf("max speed: %d Hz (%d KHz)\n", speed, speed/1000);
}


void Fb2SPIThread::writeSpi(const uint8_t *data, size_t size) {
    spi_ioc_transfer tr;

    tr.tx_buf = (unsigned long)data;
    tr.rx_buf = 0;
    tr.len = size;
    tr.delay_usecs = delay;
    tr.speed_hz = speed;
    tr.bits_per_word = bits;
    tr.pad = 0;
    tr.cs_change = 0;
    tr.rx_nbits = 0;
    tr.tx_nbits = 0;

    int ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);

    if (ret < 1) {
        pabort("cant send spi message: ");
    }
}

pixel_t Fb2SPIThread::getPixel(long x, long y, uint8_t *p)
{
    //long location = (x + vinfo.xoffset) * (vinfo.bits_per_pixel / 8) + (y + vinfo.yoffset) * finfo.line_length;
    long location = (x + vinfo.xoffset) * (vinfo.bits_per_pixel / 8) + (y + vinfo.yoffset) * finfo.line_length;
//    printf("loc %d, x %d, y %d\n", location, x, y);
    return *((pixel_t*)(p + location));
//    return 0;
}

long Fb2SPIThread::getPixelLocation(long x, long y, uint8_t *p)
{
    long location = (x + vinfo.xoffset) * (vinfo.bits_per_pixel / 8) + (y + vinfo.yoffset) * finfo.line_length;
    return (long)p + location;
}

void printBuf(uint8_t buf[], int len) {
    for (int i = 0; i < len; i++) {
        std::cout << std::hex << unsigned(buf[i]) << ' ';
    }
    std::cout << std::endl;
}
