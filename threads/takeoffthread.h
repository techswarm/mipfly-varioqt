/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TAKEOFFTHREAD_H
#define TAKEOFFTHREAD_H

#include "stoppablethread.h"
#include "NMEA/NMEAParser.h"
#include "sound/varioSound.h"
#include "navUtils/coord.hpp"

class NmeaParser;
#ifdef USE_SOUND
class PlaySoundThread;
class VarioSound;
#endif

class TakeoffThread: public StoppableThread
{
    Q_OBJECT
public:

    TakeoffThread(const NmeaParser &parser, const NmeaParser &varParser,
#ifdef USE_SOUND
                  const PlaySoundThread &soundThread,
                  VarioSound &varioSound,
#endif
                  QObject *parent = 0);

    void updateVarioFlyingStatus();

    void run() Q_DECL_OVERRIDE;

    bool getFlying() const;

    Coord getTakeoffPos() const;
    void setTakeoffPos(const Coord &value);

    int getTakeoffHeight() const;
    void setTakeoffHeight(int value);

    int getFlightId() const;

signals:
    void takeoff();
    void landed();

public slots:
    void setSpeed(qreal speed);
    void setVSpeed(int speed);
    void setTakeoffSpeed(qreal speed);

private:
    bool flying = false;
    qreal speed = 0.0;
    int count = 0;

    int flightId=0;

    int vSpeed = 0;

    Coord takeoffPos;
    int takeoffHeight;

    const NmeaParser &parser;

    qreal takeoffSpeed = 8.0;   // used to be 15.0

#ifdef USE_SOUND
    const PlaySoundThread &soundThread;
    VarioSound *varioSound_;
#endif
};

#endif // TAKEOFFTHREAD_H
