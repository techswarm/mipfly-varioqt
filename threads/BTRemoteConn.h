/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef BTCONNECTTHREAD_H
#define BTCONNECTTHREAD_H

#include "stoppablethread.h"
#include <qbluetooth.h>
#include <qbluetoothlocaldevice.h>
#include <QBluetoothDeviceDiscoveryAgent>

class BTRemoteConnectThread : public StoppableThread
{
    Q_OBJECT
public:
    BTRemoteConnectThread();
    ~BTRemoteConnectThread() Q_DECL_OVERRIDE;
    void initBluetooth();
public slots:
    void deviceDiscovered(const QBluetoothDeviceInfo &device);
protected:
    void run() Q_DECL_OVERRIDE;
private:
    void performRemoteConnection();
    QBluetoothLocalDevice *localDevice;
    QBluetoothDeviceDiscoveryAgent *discoveryAgent;
    bool bluetoothInitialised = false;
    bool bnepCreated = false;
};

#endif // BTCONNECTTHREAD_H
