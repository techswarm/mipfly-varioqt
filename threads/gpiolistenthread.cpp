/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gpiolistenthread.h"

#include <QtDebug>

const int GPIO::PIN1 = 33;
const int GPIO::PIN2 = 31;
const int GPIO::PIN3 = 29;
const int GPIO::PH1 = 26;
const int GPIO::PH2 = 24;
const int GPIO::PH3 = 22;

static const double LONGPRESSED_DURATION = 1;
static const int DELAY_READBTNS_uS = 5000;
static const int TRIGGER_VAL = 2;
static const int HIGH_TRIGGER_VAL = 6;

static inline bool PIN_HIGH(int value)
{
    return value == 0;
}

static inline bool PIN_LOW(int value)
{
    return value > 0;
}

GPIOListenThread::GPIOListenThread(int pinNumber)
    : pin(pinNumber)
{}

void GPIOListenThread::exportPin() {
    if (!pin.isExported()) {
        if (!pin.exportPin()) {
            qWarning() << "Could not export GPIO pin " << pin.getNumber();
            return;
        }
    }
}

void GPIOListenThread::run() {
    time_t startTime, endTime;
    bool handled = false;
    int counter = 0, times = 0;

    if (!pin.isExported()) {
            qWarning() << "GPIO pin " << pin.getNumber() << " is not exported, exiting GPIO thread";
            return;
    }

    int oldval = pin.read();

    if (oldval == GPIOPin::NOT_AVAILABLE) {
        qWarning() << "Pin " << pin.getNumber() << " state not available";
        return;
    }

    while (!done) {
        int val = pin.read();

        //qDebug()<<"Sleep count"<<sleepCount;
        if(val == 1 && sleepCount > 100)
        {
            QThread::usleep(DELAY_READBTNS_uS*20);
        }
        else
        {
            sleepCount++;
            QThread::usleep(DELAY_READBTNS_uS*2);
            //qDebug()<<"Sleep count"<<sleepCount;
        }

        if (PIN_LOW(oldval) && PIN_HIGH(val)) {
            time(&startTime);
        } else if (PIN_HIGH(oldval) && PIN_LOW(val)) {
            // pressed
            sleepCount = 0;
            if (!handled) {
                emit pressed(pin.getNumber());
                emit forceUpdate();
            } else {
                handled = false;
            }
            times = 0;
        } else if (PIN_HIGH(oldval) && PIN_HIGH(val)) {
            if (burstMode) {
                handled = true;
                const int trigger = (slowStart && (times < slowStartTimes) ? HIGH_TRIGGER_VAL : TRIGGER_VAL);
                if (counter < trigger) {
                    counter++;
                } else {
                    emit burst(pin.getNumber());
                    counter = 0;
                    times++;
                }
            } else if (!handled) {
                time(&endTime);
                double diff = difftime(endTime, startTime);
                if (diff > LONGPRESSED_DURATION) {
                    // long pressed
                    handled = true;
                    emit longPressed(pin.getNumber());
                    emit forceUpdate();
                }
            }
        }
        oldval = val;
        QThread::usleep(DELAY_READBTNS_uS);
    }
}
