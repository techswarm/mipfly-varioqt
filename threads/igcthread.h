/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef IGCTHREAD_H
#define IGCTHREAD_H

#include "threads/stoppablethread.h"
#include "IGC/IGCFile.hpp"
#include "IGC/IGCWriter.h"
#include "Geo/locationsservice.h"

class NmeaParser;
class TakeoffThread;

class IGCThread: public StoppableThread
{
    Q_OBJECT
public:
    IGCThread(const NmeaParser &gpsParser,const NmeaParser &varioParser, const TakeoffThread &takeoffThread, QObject *parent = 0);

public slots:
    void setName(QString name);
    void startRecording();
    void stopRecording();

protected:
    void run() Q_DECL_OVERRIDE;

private:
    IGCFile igcFile;
    IGCWriter writer;
    const NmeaParser &gpsParser;
    const NmeaParser &varioParser;
    bool record = false;
    int ind = 0;
    std::string buffer[20];

    bool circBufferFilled = false;

    time_t startTime;

    LocationsService locSvc;
};

#endif // IGCTHREAD_H
