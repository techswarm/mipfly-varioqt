/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "BTRemoteConn.h"
#include <QDebug>
#include <QBluetoothSocket>
#include <QSettings>
#include "utils/BTUtil.h"


BTRemoteConnectThread::BTRemoteConnectThread()
{
    if(BTUtil::hciDeviceIsAvailable())
    {
        initBluetooth();
    }
}

void BTRemoteConnectThread::initBluetooth()
{
    localDevice = new QBluetoothLocalDevice;
    discoveryAgent = new QBluetoothDeviceDiscoveryAgent(this);
}

BTRemoteConnectThread::~BTRemoteConnectThread()
{
    if(localDevice != nullptr)
        delete(localDevice);
}

// In your local slot, read information about the found devices
void BTRemoteConnectThread::deviceDiscovered(const QBluetoothDeviceInfo &device)
{
    qDebug() << "Found new device:" << device.name() << '(' << device.address().toString() << ')';
    QBluetoothLocalDevice::Pairing pairingStatus = localDevice->pairingStatus(device.address());
    QSettings settings;
    QString address = settings.value("remoteAddr","").toString();

    if(device.address().toString() == address)
    {
        if (pairingStatus == QBluetoothLocalDevice::Paired || pairingStatus == QBluetoothLocalDevice::AuthorizedPaired )
        {
            qDebug() << "Device " << device.name() << "is paired";

            if(!localDevice->connectedDevices().contains(device.address()))
            {
                QString command = "echo \"connect ";
                command += device.address().toString();
                command += "\nquit\" | bluetoothctl";
                qDebug()<<command;
                system(command.toStdString().c_str());
            }
            else
            {
                qDebug()<<"Device is connected";
            }
        }
        else
            qDebug() << "Device " << device.name() << "is not paired";
    }
}

void BTRemoteConnectThread::run()
{
    while (!done){
        //qDebug()<<"Connection rewind";
        if(BTUtil::hciDeviceIsAvailable())
        {
            //qDebug()<<"HCI available";
            if(localDevice == nullptr)
                initBluetooth();
            performRemoteConnection();
            if(!BTUtil::bnep0Exists())
            {
                QSettings settings;
                QString address = settings.value("BNEPAddr","").toString();
                BTUtil::connectBnep(address);
            }
            else
            {
                //qDebug()<<"BNEP exists";
            }
        }
        for(int i=0;i<6; i++)
        {
            if(done)break;
            //qDebug()<<"Connection sleep";
            QThread::sleep(10);
        }
    }
}

void BTRemoteConnectThread::performRemoteConnection()
{
    QSettings settings;
    QString address = settings.value("remoteAddr","").toString();
    if(address == "")return;
    //QBluetoothLocalDevice::Pairing pairingStatus = localDevice->pairingStatus(QBluetoothAddress(address));
    localDevice->pairingStatus(QBluetoothAddress(address));


    if(!localDevice->connectedDevices().contains(QBluetoothAddress(address)))
    {
        /*
        QString command = "echo \"connect ";
        command += address;
        command += "\nquit\" | bluetoothctl";
        qDebug()<<command;
        system(command.toStdString().c_str());
        */
        BTUtil::performBTConnect(address);
    }
}

