/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FLIGHTLOGTHREAD_H
#define FLIGHTLOGTHREAD_H

#include "threads/stoppablethread.h"
#include "Geo/locationsservice.h"
#include "navUtils/variocompute.h"
#include <QDateTime>
#include "navUtils/coord.hpp"
#include "navWaypoint/task.h"

class NmeaParser;
class TakeoffThread;
class Task;

class FlightLogThread: public StoppableThread
{
    Q_OBJECT
public:
    FlightLogThread(const NmeaParser &gpsParser,const NmeaParser &varioParser, const TakeoffThread &takeoffThread, QObject *parent = 0);   

public slots:
  void  startLogging();
  void  stopLogging();
  void addFlightLogTurnpoint(int tpIndex);

private:
    const NmeaParser &gpsParser;
    const NmeaParser &varioParser;

    void setTempSummaryLog();
    void updateSummaryLog();
    void writeSummaryLog();

    bool isLogging;

    VarioCompute *vComp;
    QDateTime qdatetime;
    Coord startCoord;
    Task * vtask;

    int counter=0;
    int delaycount=0;

protected:
  void run() Q_DECL_OVERRIDE;

};

#endif // FLIGHTLOGTHREAD_H

