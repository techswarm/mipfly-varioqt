/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "flightlogthread.h"

#include <QSettings>
#include <QtDebug>

#include "utils/vario_common.h"
#include "NMEA/NMEAParser.h"
#include "VarioQt.h"
#include "threads/takeoffthread.h"
#include "flightlog/flightsummarylog.h"

FlightSummaryLog flightsummarylog;
FlightSummary flightsummary;
QSettings settings;

FlightLogThread::FlightLogThread(const NmeaParser &parser, const NmeaParser &varioParser, const TakeoffThread &takeoffThread, QObject *parent)
    : StoppableThread(parent), gpsParser(parser), varioParser(varioParser)
{
    vtask = VarioQt::instance().getTask();
    connect(&takeoffThread, SIGNAL(takeoff()), this, SLOT(startLogging()));
    connect(&takeoffThread, SIGNAL(landed()), this, SLOT(stopLogging()));

    vComp = VarioQt::instance().getVarioCompute();
    isLogging = false;
}


void FlightLogThread::startLogging() {    
    QThread::sleep(1);    //TODO: Create a global log Number.
    //For now counting the igc files including the new created file

    flightsummary.newFlightSummary();
    isLogging = true;

    QString name = settings.value(Vario::SETTINGS_PILOT, "default").toString();
    flightsummary.setPilot(name);
    flightsummary.setStartTime(qdatetime.currentDateTime().toString());
    flightsummary.setStartLocation("N/A"); //TODO
    flightsummary.setStartLat(gpsParser.getLatitudeE7());
    flightsummary.setStartLon(gpsParser.getLongitudeE7());
    flightsummary.setStartAltitude(varioParser.getBaroHeight());
    startCoord = gpsParser.coord();
    flightsummarylog.newflightsummarylog(&flightsummary);

}

void FlightLogThread::stopLogging() {

    flightsummary.setLandingTime(qdatetime.currentDateTime().toString());
    flightsummary.setLandingLocation("N/A");
    flightsummary.setLandingLat(gpsParser.getLatitudeE7());
    flightsummary.setLandingLon(gpsParser.getLongitudeE7());
    flightsummary.setLandingAltitude(varioParser.getBaroHeight());
    flightsummary.setMaxAltitudeGain(flightsummary.getMaxAltitude() - flightsummary.getStartAltitude());
    flightsummary.setDistance( (double) startCoord.distanceKm(gpsParser.coord())*1000);
    writeSummaryLog();
    isLogging = false;
}

void FlightLogThread::setTempSummaryLog() {
    flightsummary.setLandingTime(qdatetime.currentDateTime().toString());
    flightsummary.setMaxAltitudeGain(flightsummary.getMaxAltitude() - flightsummary.getStartAltitude());
    flightsummary.setDistance( (double) startCoord.distanceKm(gpsParser.coord())*1000);

}

void FlightLogThread::updateSummaryLog() {

    if (isLogging) {
        if (vComp->getAverageCmps() > flightsummary.getMaxClimb() ) {
            flightsummary.setMaxClimb( vComp->getAverageCmps());
        }

        if (vComp->getAverageCmps() < flightsummary.getMaxSink() ) {
            flightsummary.setMaxSink(vComp->getAverageCmps());
        }

        if (varioParser.getBaroHeight() > flightsummary.getMaxAltitude()) {
            flightsummary.setMaxAltitude(varioParser.getBaroHeight());
        }

    }

}

void FlightLogThread::addFlightLogTurnpoint(int tpIndex){
    //we are in a different thread so we need to get a new pointer
    Task *t_task = VarioQt::instance().getTask();
    QList<TurnPoint> tp = t_task->getTurnPoints();

    FlightTurnPoint turnpoint;
    turnpoint.setTurnPointNumber(tpIndex);
    turnpoint.setTurnPointType(t_task->getTurnpointTypeStr(tpIndex));
    turnpoint.setTurnPointName(tp[tpIndex].getWaypoint().name());
    turnpoint.setTurnPointTime(qdatetime.currentDateTime().toString());
    turnpoint.setTurnPointAltitude(varioParser.getBaroHeight());
    turnpoint.setTurnPointLocation("N/A");
    turnpoint.setTurnPointLat(gpsParser.getLatitudeE7());
    turnpoint.setTurnPointLon(gpsParser.getLongitudeE7());
    flightsummary.addTurnpoint(&turnpoint);


}


void FlightLogThread::writeSummaryLog() {
    flightsummarylog.writeSummaryLogFile(&flightsummary);
}


void FlightLogThread::run() {
    while (!done) {
        QThread::msleep(500);
        counter++;
        updateSummaryLog();

        //non-blocking delay to wait for other threads and tasks to start
        if (delaycount >=10 && delaycount < 100 && vtask != nullptr) { //after n/2 seconds
            qDebug()<<"START Task Logging";
            connect(vtask, SIGNAL(tasktp(int)) ,this,SLOT(addFlightLogTurnpoint(int)));
            delaycount = 101;
        } else {
            delaycount ++;
            vtask = VarioQt::instance().getTask();
        }
        if (counter >= 60) { //every 30 seconds
            counter =0;
            if (isLogging) {
                setTempSummaryLog();
                writeSummaryLog();
            }
        }
    }
}
