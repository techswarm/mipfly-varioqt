/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GPIOLISTENTHREAD_H
#define GPIOLISTENTHREAD_H

#include "stoppablethread.h"
#include "GPIO/GPIOpins.h"

namespace GPIO{
    extern const int PIN1;
    extern const int PIN2;
    extern const int PIN3;
    extern const int PH1;
    extern const int PH2;
    extern const int PH3;
}

class GPIOListenThread: public StoppableThread
{
    Q_OBJECT
public:
    GPIOListenThread(int pinNumber);

    void exportPin();

signals:
    void pressed(int number);
    void longPressed(int number);
    void forceUpdate();
    void burst(int number);

protected:
    void run() Q_DECL_OVERRIDE;

private:
    GPIOPin pin;
    bool burstMode = false;
    bool slowStart = true;
    int sleepCount = 0;

    static const int slowStartTimes = 5;
};

#endif // GPIOLISTENTHREAD_H
