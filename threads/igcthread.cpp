/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "igcthread.h"

#include <QSettings>
#include <QtDebug>

#include "utils/vario_common.h"
#include "NMEA/NMEAParser.h"
#include "VarioQt.h"
#include "threads/takeoffthread.h"

IGCThread::IGCThread(const NmeaParser &parser, const NmeaParser &varioParser, const TakeoffThread &takeoffThread, QObject *parent)
    : StoppableThread(parent), gpsParser(parser), varioParser(varioParser)
{
    QSettings settings;
    QString name = settings.value(Vario::SETTINGS_PILOT, "default").toString();
    igcFile.setPilot(name.toStdString());

    connect(&takeoffThread, SIGNAL(takeoff()), this, SLOT(startRecording()));
    connect(&takeoffThread, SIGNAL(landed()), this, SLOT(stopRecording()));
}

void IGCThread::setName(QString name) {
    igcFile.setPilot(name.toStdString());
}

void IGCThread::startRecording() {
#ifdef USE_MIPFLY_LOGGER
#pragma message("START RECORDING USING MIPFLY_LOGGER")
    qDebug() << "## Start recording IGC ##";
    writer.newFlight(igcFile);
    startTime = time(nullptr);
    writer.closeFile();
    VarioQt::instance().getLoggerInterface()->startRec(writer.getFName());
    record = true;
#else
    qDebug() << "## Start recording IGC ##";
    writer.newFlight(igcFile);
    startTime = time(nullptr);

    if(circBufferFilled)
    {
        //write circular buffer to file
        int i,m;
        for(i=0;i<20;i++){
            ind + i > 19 ? m = (ind + i) % 20 :  m = ind + i;
            writer.writeLine(buffer[m]);
        }
    }

    record = true;
#endif
}

void IGCThread::stopRecording() {
#ifdef USE_MIPFLY_LOGGER
#pragma message("STOP RECORDING USING MIPFLY_LOGGER")
    qDebug() << "## STOP recording IGC ##";
    record = false;
    VarioQt::instance().getLoggerInterface()->stopRec();
#else
    qDebug() << "## STOP recording IGC ##";
    record = false;
    writer.flushStream();

    time_t end = time(nullptr);
    const Vario::Location &crtLoc = locSvc.currentLocation();
    writer.copyFlightWithDetails(crtLoc.name.toStdString(), difftime(end, startTime));
#endif
}

void IGCThread::run() {
#ifdef PRINTTHREADS
    std::cout << "IGC thread" << std::endl;
#endif
    while (!done) {
#ifndef USE_MIPFLY_LOGGER
        qDebug()<<"IGC entry pushed VarioQt!";
        Latitude lat = gpsParser.getLatitude();
        Longitude lon = gpsParser.getLongitude();
        int altPressure = varioParser.getBaroHeight();
        int altGPS = gpsParser.getGPSHeight();
        int satellitesUsed = gpsParser.getSatellitesUsed();
        int hdop = gpsParser.getHdop();

        if (record) {
            //write to file
            writer.append(lat, lon, altPressure, altGPS, satellitesUsed, hdop, gpsParser.getFixType(),gpsParser.getGPSTimestamp());

        } else {
            //write to circular buffer
            std::string value = writer.returnLine(lat, lon, altPressure, altGPS, satellitesUsed, hdop);
            buffer[ind] = value;
            ind = (ind + 1) % 20;
            if(ind==19)circBufferFilled = true;
        }
#endif
        QThread::sleep(1);      // cannot record more often than once a second, see IGC spec
    }
}
