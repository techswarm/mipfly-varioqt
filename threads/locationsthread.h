/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LOCATIONSTHREAD_H
#define LOCATIONSTHREAD_H

#include "threads/stoppablethread.h"
#include "Geo/locationsservice.h"

class LocationManager;

class LocationsThread: public StoppableThread
{
    Q_OBJECT
public:
    LocationsThread(const LocationManager& locMgr, QObject *parent = nullptr);

signals:
    void locationChanged(const QString &name);
    void locationCleared();

protected:
    void run() Q_DECL_OVERRIDE;

private:
    LocationsService locSvc;
    const LocationManager& locMgr;

private slots:
    void _locationChanged(const QString &name);
    void _locationCleared();

};

#endif // LOCATIONSTHREAD_H
