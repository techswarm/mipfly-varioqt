/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "sendcorrectionsthread.h"

#include <sstream>
#include <iomanip>
#include <iostream>
#include <cstdio>

#include "serial/serialconnection.h"
#include "navUtils/headingcorrector.h"

SendCorrectionsThread::SendCorrectionsThread(const SerialConnection &serial, HeadingCorrector &crtr)
    : serial(serial), crtr(crtr)
{
}

void SendCorrectionsThread::run() {
    char buf[50];

    while (!done) {
        qreal ax = crtr.ax();
        qreal ay = crtr.ay();

        std::ostringstream ss;


//        ax = 0.00;
//        ay = 0.00;
//        std::cout << "AY == " << ay << std::endl;

        sprintf(buf, "$STACC,%d,%d\n", int(ax * -100), int(ay * 100));

//        ss << "$STACC," << std::setprecision(3)  << ax << ',' << ay << '\n';

//        std::cout << std::setprecision(3) << ax << ' ' << ay << '\n';
        if (send) {
        std::cout << std::string(buf);

        serial.puts(std::string(buf));
        } else {
            std::cout << "-- not sending corrections --\n";
        }

        QThread::usleep(200 * 1000);
    }
}


void SendCorrectionsThread::toggleSend() {
    send = !send;
}
