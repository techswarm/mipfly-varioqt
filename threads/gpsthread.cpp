/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gpsthread.h"
#include "utils/ublox_commands.h"

#ifdef FAKE_GPS
#include <fstream>
#endif

#include <iostream>

#include "serial/serialconnection.h"
#include "NMEA/NMEAParser.h"
#include <QUdpSocket>
#include <QNetworkDatagram>
#include <QTime>


GPSThread::GPSThread(const SerialConnection &serial, NmeaParser &parser, std::__cxx11::string fakeNMEAsrc)
    : serial(serial), parser(parser), fakeNMEAsrc(fakeNMEAsrc)
{}

void GPSThread::run() {
#ifdef PRINTTHREADS
    std::cout << "GPS thread" << std::endl;
#endif

#ifndef FAKE_GPS
    #ifndef USE_MIPFLY_LOGGER
    QTime lastTransmit = QTime(QTime::currentTime());
    while(!done) {
        if (!paused) {
            uint8_t chr = serial.getc();

            //        std::cout << chr;

            if (chr != 0) {
                parser.pushCh(chr);
            }
            if(!parser.getTimeIsValid() && QTime::currentTime().secsTo(lastTransmit)<-10)
            {
                lastTransmit = QTime(QTime::currentTime());
                Ublox::pollTimeMessage(serial);
            }
        } else {
            while(paused)
            {
                QThread::usleep(250000);
            }
            QThread::sleep(15);
            //reset parser
            parser.pushCh(0);
        }
    }
#else
    QTime lastTransmit = QTime(QTime::currentTime());
    while(!done) {
        if (!paused) {
            if(!parser.getTimeIsValid() && QTime::currentTime().secsTo(lastTransmit)<-10)
            {
                lastTransmit = QTime(QTime::currentTime());
                Ublox::pollTimeMessage(serial);
            }
        }
        QThread::usleep(250000);
    }
#endif
#else
#ifndef FAKE_CONDOR
    if (!fakeNMEAsrc.empty()) {
        std::ifstream input(fakeNMEAsrc.c_str());
        while(!done) {
            std::string line;
            std::getline( input, line );

            if (line.length() == 0) {
                continue;
            }

            int i;
            for(i=0; line[i]; i++) {

                parser.pushCh(line[i]);
            }
            parser.pushCh('\n');
            //            std::cout<<line << std::endl;
            if(line[2]=='V')QThread::usleep(100 * 1000);
        }
    }
#else
    if(udpSocket != nullptr)
    {
        udpSocket->moveToThread(this);
    }
    while(!done) {
        if(udpSocket != nullptr && udpSocket->hasPendingDatagrams())
        {
            QNetworkDatagram datagram = udpSocket->receiveDatagram();
            QString data = QString(datagram.data());
            if(data[0]=='$' && data[1]=='G')
            {
                int i;
                data[2]='N';
                for(i=0; i<data.length(); i++) {

                    parser.pushCh(data[i].toLatin1());
                }
                parser.pushCh('\n');

                //fake data to account for 5Hz
                for(i=0; i<data.length(); i++) {

                    parser.pushCh(data[i].toLatin1());
                }
                parser.pushCh('\n');
                for(i=0; i<data.length(); i++) {

                    parser.pushCh(data[i].toLatin1());
                }
                parser.pushCh('\n');
                for(i=0; i<data.length(); i++) {

                    parser.pushCh(data[i].toLatin1());
                }
                parser.pushCh('\n');
                for(i=0; i<data.length(); i++) {

                    parser.pushCh(data[i].toLatin1());
                }
                parser.pushCh('\n');
            }
            else if(data[0]!='$')
            {
                QStringList lineTokens = data.split("\r\n");
                QString altitudeString = lineTokens.at(2);
                QString varioString = lineTokens.at(3);
                int altitude = (int)(QString(altitudeString.split("=")[1]).toDouble());
                int vario = (int)(QString(varioString.split("=")[1]).toDouble()*100);

                QString nmea = "$GVAR,"+QString::number(vario)+","+QString::number(altitude)+",0\r\n";

                int i;
                for(i=0; i<nmea.length(); i++) {

                    parser.pushCh(nmea[i].toLatin1());
                }
                qDebug()<<"Ba inchepe";
            }
        }
    }
#endif
#endif
    std::cout << "NMEA thread finished." << std::endl;
}

void GPSThread::pauseRead()
{
    paused = true;
}

void GPSThread::resumeRead()
{
    paused = false;
}

const SerialConnection& GPSThread::connection() const
{
    return serial;
}
