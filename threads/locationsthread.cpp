/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "locationsthread.h"

#include "settings/locationmanager.h"
#include "navUtils/coord.hpp"

#include <QtDebug>

static const double SEARCHRADIUS_km = 2;

LocationsThread::LocationsThread(const LocationManager &locMgr, QObject *parent)
    :  StoppableThread(parent), locMgr(locMgr)
{
    connect(&locSvc, &LocationsService::locationChanged, this, &LocationsThread::_locationChanged);
    connect(&locSvc, &LocationsService::locationCleared, this, &LocationsThread::_locationCleared);
}

void LocationsThread::run()
{
    qDebug() << "Locations thread run() just started";
    while (!done) {
        Latitude lat = locMgr.latitude();
        Longitude lon = locMgr.longitude();
        Coord crtCoord(lat, lon);

        Vario::Location closest;
        double minDist = 1000000;


        for (const auto &loc : locSvc.getAll()) {
            Latitude locLat(loc.lat);
            Longitude locLon(loc.lon);
            Coord locCoord(locLat, locLon);

            double distKm = crtCoord.distanceKm(locCoord);

            if (distKm < minDist && distKm < SEARCHRADIUS_km) {
                minDist = distKm;
                closest = loc;
            }
        }



        if (!closest.name.isEmpty()) {
            locSvc.setCurrent(closest);
        } else {
            locSvc.clearCurrent();
        }

        QThread::usleep(250000);
    }
    qDebug() << "Locations thread will terminate now";
}

// -- Private slots --

void LocationsThread::_locationChanged(const QString &name)
{
    emit locationChanged(name);
}

void LocationsThread::_locationCleared()
{
    emit locationCleared();
}
