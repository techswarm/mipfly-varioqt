/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "AirSpaceThread.h"
#include "navUtils/OpenAirParser.h"
#include <QWidget>
#include <QPainter>
#include <QDebug>
#include <QList>
#include <QPointF>
#include <QSettings>
#include "VarioQt.h"

#include <chrono>

typedef std::chrono::high_resolution_clock Clock;

AirSpaceThread::AirSpaceThread()
{
    QSettings settings;

    double lat = settings.value("navigation/lastLatitude", 42.21).toDouble();
    double lon = settings.value("navigation/lastLongitude", 21.42).toDouble();

    mapCenter.setLat(lat);
    mapCenter.setLon(lon);

    //mapCenter.setLat(45.05);
    //mapCenter.setLon(9.884);

    pathNear.setFillRule(Qt::WindingFill);
    pathVeryNear.setFillRule(Qt::WindingFill);
    pathDrawDistance.setFillRule(Qt::WindingFill);

    near = settings.value("horisontalNear",500).toFloat();
    veryNear = settings.value("horisontalVeryNear",250).toFloat();
    verticalWarningMeters = settings.value("verticalNear",100).toFloat();
    drawHLimit = settings.value("ignoreAirspaceOver",5000).toFloat();
    airspaceFrontUp = settings.value("navigation/airspaceFrontUp",false).toBool();
}

AirSpaceThread::~AirSpaceThread()
{
    QSettings settings;
    settings.setValue("horisontalNear",near);
    settings.setValue("horisontalVeryNear",veryNear);
    settings.setValue("verticalNear",verticalWarningMeters);
    settings.setValue("ignoreAirspaceOver",drawHLimit);
    settings.setValue("navigation/airspaceFrontUp",airspaceFrontUp);
}

void AirSpaceThread::setCenter(Coord coord)
{
    accessible.lock();
    mapCenter.setLat(coord.getLat());
    mapCenter.setLon(coord.getLon());

    //mapCenter.setLat(45.05);
    //mapCenter.setLon(9.884);

    accessible.unlock();
}

void AirSpaceThread::setAltitude(int meters)
{
    accessible.lock();
    altitudeMeters = meters;
    accessible.unlock();
}

void AirSpaceThread::run()
{
    QThread::sleep(10);
    while(!done)
    {
        //do things
        accessible.lock();
        process();
        accessible.unlock();
        QThread::sleep(10);
    }
}

bool AirSpaceThread::getAirspaceFrontUp() const
{
    return airspaceFrontUp;
}

void AirSpaceThread::setAirspaceFrontUp(bool value)
{
    airspaceFrontUp = value;
}

float AirSpaceThread::getDrawHLimit() const
{
    return drawHLimit;
}

void AirSpaceThread::setDrawHLimit(float value)
{
    drawHLimit = value;
}

float AirSpaceThread::getVerticalWarningMeters() const
{
    return verticalWarningMeters;
}

void AirSpaceThread::setVerticalWarningMeters(float value)
{
    verticalWarningMeters = value;
}

float AirSpaceThread::getVeryNear() const
{
    return veryNear;
}

void AirSpaceThread::setVeryNear(float value)
{
    veryNear = value;
}

float AirSpaceThread::getNear() const
{
    return near;
}

void AirSpaceThread::setNear(float value)
{
    near = value;
}

void AirSpaceThread::process()
{
    auto t1 = Clock::now();
    QSettings settings;
    QString airspaceFile = settings.value("navigation/Airspace","Romania.txt").toString();

    bool drawAssert = false;

    if(oldAirspaceFile != airspaceFile)
    {
        oldAirspaceFile = airspaceFile;
        if(_p != nullptr)
        {
            delete(_p);
            _p = nullptr;
        }
    }

    if(_p == nullptr)
    {
        QStringList selectedAirspaces = settings.value("navigation/Airspaces").toStringList();
        _p = new OpenAirParser(selectedAirspaces);
        drawAssert = true;

        int errorAtLine = _p->parse();
        if(errorAtLine != 0)
        {
            qDebug()<<"Airspace parse error at line "<<errorAtLine;
            delete(_p);
            _p=nullptr;
            return;
        }

    }
    int i;

    drawDistance = 150000;


    //full drawable determination at every 5 km
    if(mapCenter.distanceKm(drawableAssert)>5)
    {
        drawableAssert = mapCenter;
        drawAssert = true;
    }


    pathNear = QPainterPath();
    pathVeryNear = QPainterPath();
    pathDrawDistance = QPainterPath();
    pathNear.addEllipse(0-near/2,0-near/2,near,near);
    pathVeryNear.addEllipse(0-veryNear/2,0-veryNear/2,veryNear,veryNear);
    pathDrawDistance.addEllipse(0-drawDistance/2,0-drawDistance/2,drawDistance,drawDistance);
    violationLevel = AirSpaceDistanceType::OK;

    for(i=0;i<_p->airSpaceUnits.count();i++)
    {
        if(_p->airSpaceUnits[i].isDrawable() || drawAssert)
        {
            foreach (AirSpacePrimitive primitive, _p->airSpaceUnits[i].aPrimitives) {
                if(primitive.type == AirSpacePrimitivesType::CIRCLE)
                {
                    //determine distance to circle center
                    double distanceMeters = mapCenter.distanceKm(primitive.x) * 1000;

                    double angleToCenter = mapCenter.bearingTo(primitive.x);

                    double distancePixels = distanceMeters;
                    //qDebug()<<distancePixels;

                    QPainterPath local;

                    local.setFillRule(Qt::WindingFill);

                    local.addEllipse(distancePixels*sin(angleToCenter) - primitive.radiusMeters,
                                     distancePixels*cos(angleToCenter) *-1 - primitive.radiusMeters,
                                     primitive.radiusMeters * 2,
                                     primitive.radiusMeters * 2);
                    assesConflicts(i,local,drawAssert);
                }
                if(primitive.type == AirSpacePrimitivesType::POLY)
                {
                    //QList<QPointF> poly;
                    QPolygonF poly;
                    foreach (Coord c, primitive.point) {
                        double distanceMeters = mapCenter.distanceKm(c) * 1000;
                        double angleToCenter = mapCenter.bearingTo(c);
                        double distancePixels = distanceMeters;

                        poly.append(QPointF(
                                        distancePixels*sin(angleToCenter),
                                        distancePixels*cos(angleToCenter) *-1
                                        ));
                    }

                    QPainterPath local;
                    local.setFillRule(Qt::WindingFill);
                    local.addPolygon(poly);
                    assesConflicts(i,local,drawAssert);
                }
            }
        }
        else
        {
            //qDebug()<<"Ignore airspace "<< _p->airSpaceUnits[i].aName;
        }
    }




    if(violationLevel == AirSpaceDistanceType::NEAR)
    {
        qDebug()<<"WARNING! Airspace NEAR!";
        VarioQt::instance().getVarioSound()->varioSoundPlayFile("resources/sounds/warning.wav resources/sounds/airspaceNear.wav");
    }
    if(violationLevel == AirSpaceDistanceType::VERYNEAR)
    {
        qDebug()<<"WARNING! Airspace VERYNEAR!";
        VarioQt::instance().getVarioSound()->varioSoundPlayFile("resources/sounds/warning.wav resources/sounds/airspaceVeryNear.wav");
    }
    if(violationLevel == AirSpaceDistanceType::VIOLATED)
    {
        qDebug()<<"WARNING! Airspace VIOLATED!";
        VarioQt::instance().getVarioSound()->varioSoundPlayFile("resources/sounds/warning.wav resources/sounds/airspaceViolated.wav");
    }
    auto t2 = Clock::now();

    std::cout << "Delta t2-t1: "
              << std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count()
              << " us" << std::endl;
}

void AirSpaceThread::assesConflicts(int airspaceIndex, const QPainterPath& local, bool drawAssert)
{
    //TODO asses drawAssert in order to reduce some of the intersections
    AirSpaceDistanceType distance = AirSpaceDistanceType::OK;
    if(pathDrawDistance.intersects(local))
    {
        _p->airSpaceUnits[airspaceIndex].setDrawable();
        //qDebug()<<"Draw airspace "<< _p->airSpaceUnits[airspaceIndex].aName;
        if(pathNear.intersects(local))
        {
            distance = AirSpaceDistanceType::NEAR;
            //qDebug()<<"Near airspace "<< _p->airSpaceUnits[airspaceIndex].aName;
            if(pathVeryNear.intersects(local))
            {
                distance = AirSpaceDistanceType::VERYNEAR;
                //qDebug()<<"Very near airspace "<< _p->airSpaceUnits[airspaceIndex].aName;
                if(local.contains(QPoint(0,0)))
                {
                    distance = AirSpaceDistanceType::VIOLATED;
                    //qDebug()<<"In airspace "<< _p->airSpaceUnits[airspaceIndex].aName;
                }
            }
        }
    }
    else
        _p->airSpaceUnits[airspaceIndex].setDrawable(false);
    //determine vertical distance for airspace if necessarly
    AirSpaceDistanceType verticalDistance = AirSpaceDistanceType::OK;
    if(distance != AirSpaceDistanceType::OK)
    {
        //we are getting close to an airspace. Determine vertical spacing
        if(altitudeMeters - verticalWarningMeters < _p->airSpaceUnits[airspaceIndex].getAHiMeters()  &&
                altitudeMeters + verticalWarningMeters > _p->airSpaceUnits[airspaceIndex].getALowMeters())
        {
            verticalDistance = AirSpaceDistanceType::VERYNEAR;
        }
        if(altitudeMeters < _p->airSpaceUnits[airspaceIndex].getAHiMeters()  &&
                altitudeMeters > _p->airSpaceUnits[airspaceIndex].getALowMeters())
        {
            verticalDistance = AirSpaceDistanceType::VIOLATED;
        }
    }

    AirSpaceDistanceType violation = AirSpaceDistanceType::OK;

    if(verticalDistance == AirSpaceDistanceType::VIOLATED && distance == AirSpaceDistanceType::VIOLATED)
    {
        //airspace has been violated
        violation = AirSpaceDistanceType::VIOLATED;
    }
    else if(distance == AirSpaceDistanceType::VIOLATED && verticalDistance == AirSpaceDistanceType::VERYNEAR)
    {
        //airspace very near
        violation = AirSpaceDistanceType::VERYNEAR;
    }
    else if(distance == AirSpaceDistanceType::VERYNEAR && verticalDistance != AirSpaceDistanceType::OK)
    {
        //airspace very near
        violation = AirSpaceDistanceType::VERYNEAR;
    }
    else if(distance == AirSpaceDistanceType::NEAR && verticalDistance != AirSpaceDistanceType::OK)
    {
        //airspace near
        violation = AirSpaceDistanceType::NEAR;
    }

    if(_p->airSpaceUnits[airspaceIndex].getDistance() != violation)
    {
        _p->airSpaceUnits[airspaceIndex].setDistance(violation);
        if(violation>violationLevel)violationLevel = violation;
    }
}
