/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef INPUTTHREAD_H
#define INPUTTHREAD_H

#include "stoppablethread.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <dirent.h>
#include <linux/input.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/select.h>
#include <sys/time.h>
#include <termios.h>
#include <signal.h>

class InputThread : public StoppableThread
{
    Q_OBJECT
public:
    InputThread();
    void setKeyShortPress1(int value);
    void setKeyShortPress2(int value);
    void setKeyShortPress3(int value);
    void setKeyLongPress1(int value);
    void setKeyLongPress2(int value);
    void setKeyLongPress3(int value);

    void setEmitKeypress(bool value);

    void saveSettings();

private:

    int keyShortPress1;
    int keyShortPress2;
    int keyShortPress3;

    int keyLongPress1;
    int keyLongPress2;
    int keyLongPress3;

    bool emitKeypress = true;


    struct input_event ev[64];
    int fd, rd, value, size = sizeof (struct input_event);
    char name[256] = "Unknown";
    char *device = NULL;

    void init();

protected:
    void run() Q_DECL_OVERRIDE;

signals:
    void pressed(int number);
    void longPressed(int number);
    void keyCode(int key);
};

#endif // INPUTTHREAD_H
