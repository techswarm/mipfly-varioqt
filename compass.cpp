/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "compass.h"
#include "ui_compass.h"

#include <QPainter>
#include <QtMath>
#include <cmath>    // for M_PI and M_PI_2
#include "utils/cstgraphics.h"
#include "utils/unitconverter.h"
#include "VarioQt.h"
#include "navWaypoint/task.h"

using Vario::drawStr;

//inline static void drawStr(int xCenter, int yCenter, const QString &str,QPainter &painter) {
//    painter.drawText(xCenter - 6, yCenter - 7, 12, 14, Qt::AlignCenter, str);
//}

static inline qreal nav2trigo(qreal directionRadians)
{
    return directionRadians;
    //return 2 * M_PI - directionRadians;
}

// Public

Compass::Compass(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Compass)
{
    ui->setupUi(this);

    ui->boxLblHeading->setValue("H");
    ui->boxLblWindDir->setValue("W");

    ui->boxHeading->setValue("0");
    ui->boxWindDir->setValue("0");
    ui->boxWindSpeed->setValue("0");

    ui->boxLblWindDir->setAlignment(Qt::AlignRight);
    ui->boxWindDir->setAlignment(Qt::AlignRight);
    ui->boxWindSpeed->setAlignment(Qt::AlignRight);
    ui->boxLblHeading->setAlignment(Qt::AlignLeft);
    ui->boxHeading->setAlignment(Qt::AlignLeft);

    ui->boxLblHeading->setNoBorder();
    ui->boxHeading->setNoBorder();
    ui->boxLblWindDir->setNoBorder();
    ui->boxWindDir->setNoBorder();
    ui->boxWindSpeed->setNoBorder();

    _wpDir = 10;
    _wpDirOptimised = 10;
}

Compass::~Compass()
{
    delete ui;
}

#include <QtDebug>

void Compass::setWindSpeed(qreal speed) {
    if(unit == "knots")
    {
        ui->boxWindSpeed->setValue(QString::number(UnitConverter::kmPerHToKnots(speed),'f',0));
    }
    else if(unit == "mi/h")
    {
        ui->boxWindSpeed->setValue(QString::number(UnitConverter::kmPerHToMiPerHour(speed),'f',0));
    }
    else
    {
        ui->boxWindSpeed->setValue(QString::number(speed,'f',0));
    }
}

void Compass::setHeading(qreal headingDegrees) {
    ui->boxHeading->setValue(QString::number(int(headingDegrees)));
    _angle = nav2trigo(qDegreesToRadians(headingDegrees));
    this->update();
}

void Compass::setWindDir(qreal dirDegrees) {
    ui->boxWindDir->setValue(QString::number(int(dirDegrees)));
    _windDir = nav2trigo(qDegreesToRadians(dirDegrees));
    update();
}

void Compass::setThermalDir(qreal dirDegrees)
{
    _thermalDir = dirDegrees;
}

// Protected

void Compass::paintEvent(QPaintEvent *event) {
    int d = 20;

    Task *pTask = VarioQt::instance().getTask();

    _wpDir = pTask->getCourseToWp();
    _wpDirOptimised = pTask->getCourseToWpOptimised();

    _xc = (width() - right) / 2;
    _yc = (height() - bottom) / 2 + 2;
    _r1 = (width() - right) / 2-1;
    _r2 = _r1 - d;

    QPainter painter(this);

    drawCircles(painter);
    drawLetters(painter);
    drawArrow(painter);
    drawTaskArrow(painter);
}

void Compass::setUnit(const QString &value)
{
    unit = value;
}

// Private

void Compass::drawCircles(QPainter &painter) {
    painter.setPen(QPen(QBrush("black"),2));
    painter.drawEllipse(_xc - _r1, _yc - _r1, 2 * _r1 - 1, 2 * _r1 - 1);
    painter.setPen(QPen(QBrush("black"),1));
    painter.drawEllipse(_xc - _r2, _yc - _r2, 2 * _r2 - 1, 2 * _r2 - 1);
    painter.setPen(QPen(QBrush("black"),3));
    painter.drawLine(_xc, _yc-_r2,_xc, _yc-_r2-4);
    painter.setPen(QPen(QBrush("black"),1));
}

void Compass::drawLetters(QPainter &painter) {
    qreal r = qreal((_r1 + _r2) / 2);

    //_thermalDir = M_PI;

    int xE = _xc + int(r * qCos(_angle));
    int yE = _yc - int(r * qSin(_angle));

    int xN = _xc + int(r * qCos(_angle + M_PI_2));
    int yN = _yc - int(r * qSin(_angle + M_PI_2));

    int xW = _xc + int(r * qCos(_angle + M_PI));
    int yW = _yc - int(r * qSin(_angle + M_PI));

    int xS = _xc + int(r * qCos(_angle + 3 * M_PI_2));
    int yS = _yc - int(r * qSin(_angle + 3 * M_PI_2));

    int xT = _xc + int(r * qCos(_angle - _thermalDir + M_PI_2));
    int yT = _yc - int(r * qSin(_angle - _thermalDir + M_PI_2));

    int xWp = _xc + int((r-12) * qCos(_angle - _wpDir + M_PI_2));
    int yWp = _yc - int((r-12) * qSin(_angle - _wpDir + M_PI_2));

    drawStr(xE, yE-8, "E", 16, painter,Qt::AlignHCenter);
    drawStr(xN, yN-8, "N", 16, painter,Qt::AlignHCenter);
    drawStr(xW, yW-8, "W", 16, painter,Qt::AlignHCenter);
    drawStr(xS, yS-8, "S", 16, painter,Qt::AlignHCenter);
    if(_thermalDir != 10)
        drawStr(xT, yT-8, "T", 16, painter,Qt::AlignHCenter);

    if(_wpDir != 10)
    {
        painter.setBrush(Qt::black);
        painter.drawEllipse(xWp-4,yWp-4,8,8);
        painter.setBrush(Qt::transparent);
    }
}

void Compass::drawArrow(QPainter &painter) {
    int h1 = 20, h2 = 7, h3 = 18, wd = 16;
    QPoint arrPts[] { {_xc, _yc-h1}, {_xc-wd, _yc+h3}, {_xc, _yc+h2}, {_xc+wd, _yc+h3} };

    //qDebug()<<_windDir;

    float angle = _windDir + _angle + M_PI;


    for  (QPoint &pt : arrPts) {
        qreal oldX = qreal(pt.x() - _xc);
        qreal oldY = qreal(pt.y() - _yc);

        int x = int(oldX * qCos(angle) + oldY * qSin(angle));
        int y = int(oldY * qCos(angle) - oldX * qSin(angle));

        pt.setX(x + _xc);
        pt.setY(y + _yc);
    }
    painter.setPen(QPen(QBrush("black"),2));
    painter.drawPolygon(arrPts, 4);
}

void Compass::drawTaskArrow(QPainter &painter)
{
    if(_wpDirOptimised == 10)return;
    int h1 = -30, h3 = +20, wd = 10;
    QPoint arrPts[] { {_xc, _yc+h1}, {_xc+wd, _yc-h3},{_xc, _yc+h1}, {_xc-wd, _yc-h3} };

    //qDebug()<<_wpDirOptimised;

    float angle = _angle - _wpDirOptimised;


    for  (QPoint &pt : arrPts) {
        qreal oldX = qreal(pt.x() - _xc);
        qreal oldY = qreal(pt.y() - _yc);

        int x = int(oldX * qCos(angle) + oldY * qSin(angle));
        int y = int(oldY * qCos(angle) - oldX * qSin(angle));

        pt.setX(x + _xc);
        pt.setY(y + _yc);
    }
    painter.setPen(QPen(QBrush("black"),2));
    painter.drawLine(arrPts[0],arrPts[1]);
    painter.drawLine(arrPts[2],arrPts[3]);
}
