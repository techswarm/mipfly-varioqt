/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VARIOQT_H
#define VARIOQT_H

#include <QObject>

#include "navUtils/variocompute.h"
#include "navUtils/lastlift.h"
#include "navUtils/Wind.hpp"
#include "NMEA/NMEAParser.h"
#include "settings/namemanager.h"
#include "settings/locationmanager.h"

#include "threads/takeoffthread.h"
#include "threads/gpsthread.h"
#include "threads/fb2spithread.h"
#include "threads/inputThread.h"
#include "threads/AirSpaceThread.h"
#include "threads/flightlogthread.h"

#include "settings/windmanager.h"
#include "settings/glidemanager.h"

#include "Geo/locationsthreadmgr.h"

#include "mainui.h"
#include "Warning.h"

#include "navUtils/elevation.h"
#include "navUtils/livetrack.h"
#include "navUtils/hgtcontainer.h"
#include "navUtils/termalingDetector.h"
#include "navUtils/faiassistcompute.h"

#include "utils/netstats.h"
#include "utils/glider.h"

#include "navWaypoint/waypointManager.h"
#include "navWaypoint/task.h"

#include "ui/taskPlot.h"
#include "utils/BTServer.h"
#include "utils/loggerinterface.h"

#include "LK8000OLC/olc.h"

class TermalingDetector;

class VarioQt
{
public:
    static VarioQt& instance() {
            static VarioQt *instance = new VarioQt();
            return *instance;
        }

    NmeaParser *getGpsParser() const;
    void setGpsParser(NmeaParser *gpsParser);

    VarioCompute *getVarioCompute() const;
    void setVarioCompute(VarioCompute *varioCompute);

    NmeaParser *getVarioParser() const;
    void setVarioParser(NmeaParser *varioParser);

    LastLift *getLastLift() const;
    void setLastLift(LastLift *lastLift);

    Wind *getWind() const;
    void setWind(Wind *wind);

    NameManager *getNameManager() const;
    void setNameManager(NameManager *nameManager);

    LocationManager *getLocationManager() const;
    void setLocationManager(LocationManager *value);

    TakeoffThread *getTakeOffThread() const;
    void setTaleOffThread(TakeoffThread *taleOffThread);

    LocationsThreadMgr *getLocationsThreadMgr() const;
    void setLocationsThreadMgr(LocationsThreadMgr *locationsThreadMgr);

    GPSThread *getVarioSerialThread() const;
    void setVarioSerialThread(GPSThread *varioSerialThread);

    WindManager *getWindManager() const;
    void setWindManager(WindManager *windManager);

    GlideManager *getGlideManager() const;
    void setGlideManager(GlideManager *glideManager);

    Fb2SPIThread *getFb2SPIThread() const;
    void setFb2SPIThread(Fb2SPIThread *fb2SPIThread);

    void pushNewWidget(QWidget *wdg);
    void deleteLastWidget();
    QWidget *getLastWidget();
    bool isActiveWidget(QWidget *wdg);


    VarioSound *getVarioSound() const;
    void setVarioSound(VarioSound *varioSound);

    InputThread *getInputThread() const;
    void setInputThread(InputThread *inputThread);

    MainUI *getMainUI() const;
    void setMainUI(MainUI *mainUI);

    QTranslator *getTranslator() const;
    void setTranslator(QTranslator *translator);

    AirSpaceThread *getAirSpaceThread() const;
    void setAirSpaceThread(AirSpaceThread *airSpaceThread);

    Elevation *getElevation() const;
    void setElevation(Elevation *elevation);

    LiveTrack *getLiveTrack() const;
    void setLiveTrack(LiveTrack *liveTrack);

    NetStats *getNetStats() const;
    void setNetStats(NetStats *netStats);

    HGTContainer *getHGTContainer() const;
    void setHGTContainer(HGTContainer *hGTContainer);

    waypointManager *getWaypointManager() const;
    void setWaypointManager(waypointManager *waypointManager);

    Task *getTask() const;
    void setTask(Task *task);

    Glider *getGlider() const;
    void setGlider(Glider *glider);

    TermalingDetector *getTermalingDetector() const;
    void setTermalingDetector(TermalingDetector *termalingDetector);

    bool getTtyMode() const;
    void setTtyMode(bool value);

    QString getMnuFontName() const;
    void setMnuFontName(QString mnuFontName);
    int getMnuFontSize() const;
    void setMnuFontSize(int mnuFontSize);

    TaskPlot *getTaskPlot() const;
    void setTaskPlot(TaskPlot *taskPlot);

    FaiAssistCompute *getFaiAssistCompute() const;
    void setFaiAssistCompute(FaiAssistCompute *faiAssistCompute);

    BTServer *getBTServer() const;
    void setBTServer(BTServer *BTServer);

    LoggerInterface *getLoggerInterface() const;
    void setLoggerInterface(LoggerInterface *loggerInterface);

    TrianglePlot *getTrianglePlot() const;
    void setTrianglePlot(TrianglePlot *trianglePlot);

    OLC *getOlc() const;
    void setOlc(OLC *olc);

    FlightLogThread *getFlightlogThread() const;
    void setFlightlogThread(FlightLogThread *flightlogThread);

    bool getRecoveryMode() const;
    void setRecoveryMode(bool value);

private:
    static VarioQt instance_;
    VarioCompute *varioCompute_;
    NmeaParser *gpsParser_;
    NmeaParser *varioParser_;
    LastLift *lastLift_;
    Wind *wind_;

    NameManager *nameManager_;
    LocationManager *locationManager_;
    TakeoffThread *takeOffThread_;
    FlightLogThread *flightlogThread_;

    LocationsThreadMgr *locationsThreadMgr_;
    GPSThread * varioSerialThread_;
    WindManager * windManager_;
    GlideManager * glideManager_;
    Fb2SPIThread * fb2SPIThread_;

    VarioSound * varioSound_;

    QList<QWidget *> orderedWindgetList_;

    InputThread * inputThread_;

    MainUI * mainUI_;

    QTranslator * translator_;

    AirSpaceThread * airSpaceThread_;

    Elevation * elevation_;

    LiveTrack * liveTrack_;

    NetStats * netStats_;

    HGTContainer * hGTContainer_;

    waypointManager * waypointManager_;

    Glider * glider_;

    Task * task_;

    TermalingDetector * termalingDetector_;

    FaiAssistCompute * faiAssistCompute_;

    bool ttyMode=false;

    bool recoveryMode = false;

    QString mnuFontName;

    int mnuFontSize;

    TaskPlot * taskPlot_;

    BTServer * BTServer_;

    LoggerInterface * loggerInterface_;

    TrianglePlot * trianglePlot_;

    OLC * olc_;

    VarioQt () {}
};

#endif // VARIOQT_H
