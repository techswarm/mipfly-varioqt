/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "CoordInput.h"
#include "ui_CoordInput.h"
#include "VarioQt.h"
#include "mainui.h"
#include "navUtils/coord.hpp"

CoordInput::CoordInput(QDialog *parent) :
    QDialog(parent),
    ui(new Ui::CoordInput)
{
    ui->setupUi(this);


    connect(VarioQt::instance().getMainUI(),SIGNAL(mainGpioPressed(int)),this,SLOT(gpioPressed(int)));
    connect(VarioQt::instance().getMainUI(),SIGNAL(mainGpioLongPressed(int)),this,SLOT(gpioLongPressed(int)));

    ui->valueBox->setCaption("Coordonate");

    QSettings settings;
    format = (Coord::CoordInputFormat)settings.value("coordFormat",Coord::Decimal).toUInt();

    move(240/2 - width()/2,320/2 - height()/2);
}

CoordInput::~CoordInput()
{
    if(isVisible())hide();
    delete ui;
}

void CoordInput::showEvent(QShowEvent *e)
{
    QWidget::showEvent(e);
    VarioQt::instance().pushNewWidget(this);
}

void CoordInput::hideEvent(QHideEvent *e)
{
    QWidget::hideEvent(e);
    VarioQt::instance().deleteLastWidget();
}

float CoordInput::value() const
{
    return _value * sign;
}

float CoordInput::clampValue(float v)
{
    if(_type == CoordInput::LAT)
    {
        if(v<0)return 0;
        if(v>90)return 90;
    }
    else
    {
        if(v<0)return 0;
        if(v>180)return 180;
    }
    return v;
}

void CoordInput::advanceVirtualPointer()
{
    virtualCursorPoint++;
    if(format == Coord::CoordInputFormat::Decimal)
    {
        if(_type == CoordInput::LAT)
        {
            if(virtualCursorPoint == 1)
                virtualCursorPoint++;
            if(virtualCursorPoint == 4)
                virtualCursorPoint++;
            if(virtualCursorPoint == 9)
                close();
        }
        else
        {
            if(virtualCursorPoint == 4)
                virtualCursorPoint++;
            if(virtualCursorPoint == 9)
                close();
        }
    }
    if(format == Coord::CoordInputFormat::DMS)
    {
        if(_type == CoordInput::LAT)
        {
            if(virtualCursorPoint == 1)
                virtualCursorPoint++;
            if(virtualCursorPoint == 4)
                virtualCursorPoint++;
            if(virtualCursorPoint == 7)
                virtualCursorPoint++;
            if(virtualCursorPoint == 10)
                close();
        }
        else
        {
            if(virtualCursorPoint == 4)
                virtualCursorPoint++;
            if(virtualCursorPoint == 7)
                virtualCursorPoint++;
            if(virtualCursorPoint == 10)
                close();
        }
    }
    ui->valueBox->setCursorPos(virtualCursorPoint);
    repaint();
}

void CoordInput::reverseVirtualPointer()
{
    if(format == Coord::CoordInputFormat::Decimal)
    {
        if(_type == CoordInput::LAT)
        {
            if(virtualCursorPoint > 0)
                virtualCursorPoint--;
            if(virtualCursorPoint == 1)
                virtualCursorPoint--;
            if(virtualCursorPoint == 4)
                virtualCursorPoint--;
        }
        else
        {
            if(virtualCursorPoint > 0)
                virtualCursorPoint--;
            if(virtualCursorPoint == 4)
                virtualCursorPoint--;
        }
    }
    if(format == Coord::CoordInputFormat::DMS)
    {
        if(_type == CoordInput::LAT)
        {
            if(virtualCursorPoint > 0)
                virtualCursorPoint--;
            if(virtualCursorPoint == 1)
                virtualCursorPoint--;
            if(virtualCursorPoint == 4)
                virtualCursorPoint--;
            if(virtualCursorPoint == 7)
                virtualCursorPoint--;
        }
        else
        {
            if(virtualCursorPoint > 0)
                virtualCursorPoint--;
            if(virtualCursorPoint == 4)
                virtualCursorPoint--;
            if(virtualCursorPoint == 7)
                virtualCursorPoint--;
        }
    }
    ui->valueBox->setCursorPos(virtualCursorPoint);
    repaint();
}

void CoordInput::setValue(float value)
{
    _value = clampValue(value);

    QString valueStr;
    if(_type == CoordInput::LAT)
    {
        if(sign >=0)valueStr += "N";
        else valueStr += "S";

        if(format == Coord::CoordInputFormat::Decimal)
        {
            valueStr += QString("%1").arg(fabs(_value),8,'f',4,'0');
            valueStr[1]=' ';
        }
        if(format == Coord::CoordInputFormat::DMS)
        {
            valueStr += QString::number(Coord::getDegrees(_value)).rightJustified(3,'0');
            valueStr += "*";
            valueStr += QString::number(Coord::getMinutes(_value)).rightJustified(2,'0');
            valueStr += "'";
            valueStr += QString::number(Coord::getSeconds(_value)).rightJustified(2,'0');
            valueStr += "\"";
            valueStr[1]=' ';
        }
    }
    else
    {
        if(sign >=0)valueStr += "E";
        else valueStr += "W";

        if(format == Coord::CoordInputFormat::Decimal)
        {
            valueStr += QString("%1").arg(fabs(_value),8,'f',4,'0');
        }
        if(format == Coord::CoordInputFormat::DMS)
        {
            valueStr += QString::number(Coord::getDegrees(_value)).rightJustified(3,'0');
            valueStr += "*";
            valueStr += QString::number(Coord::getMinutes(_value)).rightJustified(2,'0');
            valueStr += "'";
            valueStr += QString::number(Coord::getSeconds(_value)).rightJustified(2,'0');
            valueStr += "\"";
        }
    }
    ui->valueBox->setCursorPos(virtualCursorPoint);
    ui->valueBox->setValue(valueStr);
    repaint();
}

CoordInput::CoordType CoordInput::type() const
{
    return _type;
}

void CoordInput::setType(const CoordInput::CoordType &type)
{
    _type = type;
    repaint();
}

void CoordInput::gpioPressed(int btn)
{
    if(btn == Vario::GPIO1)
    {
        if(format == Coord::CoordInputFormat::Decimal)
        {
            if(virtualCursorPoint == 0)
            {
                sign*=-1;
                setValue(_value);
            }
            if(virtualCursorPoint == 1)
            {
                setValue(_value+100);
            }
            if(virtualCursorPoint == 2)
            {
                setValue(_value+10);
            }
            if(virtualCursorPoint == 3)
            {
                setValue(_value+1);
            }
            if(virtualCursorPoint == 5)
            {
                setValue(_value+0.1);
            }
            if(virtualCursorPoint == 6)
            {
                setValue(_value+0.01);
            }
            if(virtualCursorPoint == 7)
            {
                setValue(_value+0.001);
            }
            if(virtualCursorPoint == 8)
            {
                setValue(_value+0.0001);
            }
        }
        if(format == Coord::CoordInputFormat::DMS)
        {
            if(virtualCursorPoint == 0)
            {
                sign*=-1;
                setValue(_value);
            }
            if(virtualCursorPoint == 1)
            {
                setValue(_value+100);
            }
            if(virtualCursorPoint == 2)
            {
                setValue(_value+10);
            }
            if(virtualCursorPoint == 3)
            {
                setValue(_value+1);
            }
            if(virtualCursorPoint == 5)
            {
                setValue(_value+1.0/6);
            }
            if(virtualCursorPoint == 6)
            {
                setValue(_value+1.0/60);
            }
            if(virtualCursorPoint == 8)
            {
                setValue(_value+1.0/360);
            }
            if(virtualCursorPoint == 9)
            {
                setValue(_value+1.0/3600);
            }
        }
    }
    if(btn == Vario::GPIO2)
    {
        if(format == Coord::CoordInputFormat::Decimal)
        {
            if(virtualCursorPoint == 0)
            {
                sign*=-1;
                setValue(_value);
            }
            if(virtualCursorPoint == 1)
            {
                setValue(_value-100);
            }
            if(virtualCursorPoint == 2)
            {
                setValue(_value-10);
            }
            if(virtualCursorPoint == 3)
            {
                setValue(_value-1);
            }
            if(virtualCursorPoint == 5)
            {
                setValue(_value-0.1);
            }
            if(virtualCursorPoint == 6)
            {
                setValue(_value-0.01);
            }
            if(virtualCursorPoint == 7)
            {
                setValue(_value-0.001);
            }
            if(virtualCursorPoint == 8)
            {
                setValue(_value-0.0001);
            }
        }
        if(format == Coord::CoordInputFormat::DMS)
        {
            if(virtualCursorPoint == 0)
            {
                sign*=-1;
                setValue(_value);
            }
            if(virtualCursorPoint == 1)
            {
                setValue(_value-100);
            }
            if(virtualCursorPoint == 2)
            {
                setValue(_value-10);
            }
            if(virtualCursorPoint == 3)
            {
                setValue(_value-1);
            }
            if(virtualCursorPoint == 5)
            {
                setValue(_value-1.0/6);
            }
            if(virtualCursorPoint == 6)
            {
                setValue(_value-1.0/60);
            }
            if(virtualCursorPoint == 8)
            {
                setValue(_value-1.0/360);
            }
            if(virtualCursorPoint == 9)
            {
                setValue(_value-1.0/3600);
            }
        }
    }

    if(btn == Vario::GPIO3)
    {
        advanceVirtualPointer();
    }
}

void CoordInput::gpioLongPressed(int btn)
{
    if(btn == Vario::GPIO3)
    {
        close();
    }
    if(btn == Vario::GPIO1)
        reverseVirtualPointer();
    if(btn == Vario::GPIO2)
    {
        switch(format)
        {
            case Coord::Decimal:
            format = Coord::DMS;
            break;
        case Coord::DMS:
            format = Coord::Decimal;
            break;
        }
        QSettings settings;
        settings.setValue("coordFormat",format);
        virtualCursorPoint = 0;
        setValue(_value);
    }
}
