/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "TimeInput.h"
#include "ui_TimeInput.h"
#include "VarioQt.h"
#include "mainui.h"

TimeInput::TimeInput(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TimeInput)
{
    ui->setupUi(this);

    connect(VarioQt::instance().getMainUI(),SIGNAL(mainGpioPressed(int)),this,SLOT(gpioPressed(int)));
    connect(VarioQt::instance().getMainUI(),SIGNAL(mainGpioLongPressed(int)),this,SLOT(gpioLongPressed(int)));

    ui->valueBox->setCaption("Time");
    setTime(QTime(0,0,0));

    move(240/2 - width()/2,320/2 - height()/2);
}

TimeInput::~TimeInput()
{
    delete ui;
}

void TimeInput::showEvent(QShowEvent *e)
{
    QWidget::showEvent(e);
    VarioQt::instance().pushNewWidget(this);
}

void TimeInput::hideEvent(QHideEvent *e)
{
    QWidget::hideEvent(e);
    VarioQt::instance().deleteLastWidget();
}

void TimeInput::advanceVirtualPointer()
{
    virtualCursorPoint++;
    if(virtualCursorPoint == 2)
        virtualCursorPoint++;
    if(virtualCursorPoint == 5)
        virtualCursorPoint++;
    if(virtualCursorPoint == 8)
        close();
    ui->valueBox->setCursorPos(virtualCursorPoint);
    repaint();
}

void TimeInput::reverseVirtualPointer()
{
    if(virtualCursorPoint > 0)
        virtualCursorPoint--;
    if(virtualCursorPoint == 2)
        virtualCursorPoint--;
    if(virtualCursorPoint == 5)
        virtualCursorPoint--;
    ui->valueBox->setCursorPos(virtualCursorPoint);
    repaint();
}

QTime TimeInput::time() const
{
    return _time;
}

void TimeInput::setTime(const QTime &time)
{
    _time = time;
    QString valueStr;
    valueStr += QString("%1").arg(fabs(_time.hour()),2,'f',0,'0');
    valueStr += ":";
    valueStr += QString("%1").arg(fabs(_time.minute()),2,'f',0,'0');
    valueStr += ":";
    valueStr += QString("%1").arg(fabs(_time.second()),2,'f',0,'0');
    ui->valueBox->setCursorPos(virtualCursorPoint);
    ui->valueBox->setValue(valueStr);
    repaint();
}

void TimeInput::gpioPressed(int btn)
{
    if(btn == Vario::GPIO3)
    {
        advanceVirtualPointer();
    }
    if(btn == Vario::GPIO1)
    {
        if(virtualCursorPoint == 0)
        {
            _time = _time.addSecs(36000);
        }
        if(virtualCursorPoint == 1)
        {
            _time = _time.addSecs(3600);
        }
        if(virtualCursorPoint == 3)
        {
            _time = _time.addSecs(600);
        }
        if(virtualCursorPoint == 4)
        {
            _time = _time.addSecs(60);
        }
        if(virtualCursorPoint == 6)
        {
            _time = _time.addSecs(10);
        }
        if(virtualCursorPoint == 7)
        {
            _time = _time.addSecs(1);
        }
        setTime(_time);
    }
    if(btn == Vario::GPIO2)
    {
        if(virtualCursorPoint == 0)
        {
            _time = _time.addSecs(-36000);
        }
        if(virtualCursorPoint == 1)
        {
            _time = _time.addSecs(-3600);
        }
        if(virtualCursorPoint == 3)
        {
            _time = _time.addSecs(-600);
        }
        if(virtualCursorPoint == 4)
        {
            _time = _time.addSecs(-60);
        }
        if(virtualCursorPoint == 6)
        {
            _time = _time.addSecs(-10);
        }
        if(virtualCursorPoint == 7)
        {
            _time = _time.addSecs(-1);
        }
        setTime(_time);
    }
}

void TimeInput::gpioLongPressed(int btn)
{
    if(btn == Vario::GPIO3)
    {
        close();
    }
    if(btn == Vario::GPIO1)
    {
        reverseVirtualPointer();
    }
}
