/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "AirSpacePlot.h"
#include "navUtils/OpenAirParser.h"
#include <QWidget>
#include <QPainter>
#include <QDebug>
#include <QList>
#include <QPointF>
#include <QSettings>
#include "VarioQt.h"
#include "threads/AirSpaceThread.h"
#include "utils/qfonts.h"
#include <QDebug>
#include <QElapsedTimer>
#include <QtConcurrent/QtConcurrent>
#include "uiElements/mapscaleindicator.h"

AirSpacePlot::AirSpacePlot(QWidget *parent):QWidget(parent)
{
    QSettings settings;

    connect(&computeWtcher,SIGNAL(finished()),this,SLOT(computeFinished()));

    double lat = settings.value("navigation/lastLatitude", 42.21).toDouble();
    double lon = settings.value("navigation/lastLongitude", 21.42).toDouble();

    mapCenter.setLat(lat);
    mapCenter.setLon(lon);
    //mapCenter.setLat(45.05);
    //mapCenter.setLon(9.884);
}

void AirSpacePlot::focus()
{
    gotFocus = true;
    update();
}

void AirSpacePlot::unfocus()
{
    gotFocus = false;
    update();
}

void AirSpacePlot::shortPress(int btn)
{
    if(gotFocus)
    {
        if(btn == Vario::GPIO1)
        {
            //zoom in
            _metersToPixels *= 1.8;
            if(_metersToPixels > 0.5)_metersToPixels = 0.5;
        }
        if(btn == Vario::GPIO2)
        {
            //zoom out
            _metersToPixels /= 1.8;
            if(_metersToPixels < 0.0005)_metersToPixels = 0.0005;
        }
        update();
    }
}

void AirSpacePlot::setCenter(Coord coord)
{
    mapCenter.setLat(coord.getLat());
    mapCenter.setLon(coord.getLon());
    //mapCenter.setLat(45.05);
    //mapCenter.setLon(9.884);
}

void AirSpacePlot::setCourse(qreal course)
{
    this->course = course;
}

void AirSpacePlot::computeFinished()
{
    update();
}

void AirSpacePlot::timerEvent(QTimerEvent *event)
{
    //qDebug() << "redraw air";
    update();
}

void AirSpacePlot::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    QSettings settings;
    bool airspaceFrontUp = settings.value("navigation/airspaceFrontUp",false).toBool();

    //painter.fillRect(0, 0, width(), height(), Qt::white);
    QElapsedTimer timer;
    timer.start();

    AirSpaceThread* ast = VarioQt::instance().getAirSpaceThread();
    _p = ast->_p;

    if(_p == nullptr)return;

    ast->accessible.lock();

    painter.setPen(QPen(QColor("black")));
    int i;

    QImage image(240, 320, QImage::Format_RGB16);

    QPainter paint;
    paint.begin(&image);
    image.fill(QColor(Qt::white).rgb());

    if(_p->repathRequired(mapCenter,_metersToPixels))
    {
        _p->repathPath(mapCenter,_metersToPixels);

        computeFuture = QtConcurrent::run(_p,&OpenAirParser::repathInset);
        computeWtcher.setFuture(computeFuture);
    }

    QPen pen(Qt::blue, 3);
    paint.setPen(pen);

    int translateX,translateY;
    translateX = 0;//width()/2;
    translateY = 0;//height()/2;

    //determine distance to circle center
    double distanceMeters = mapCenter.distanceKm(_p->getPathCenter()) * 1000;

    double angleToCenter = mapCenter.bearingTo(_p->getPathCenter());

    double distancePixels = distanceMeters * _metersToPixels;
    //qDebug()<<distancePixels;

    translateX += distancePixels*sin(angleToCenter);
    translateY += distancePixels*cos(angleToCenter) *-1;

    for(i=0;i<_p->airSpaceUnits.count();i++)
    {
        if(!_p->airSpaceUnits[i].isDrawable() || _p->airSpaceUnits[i].getALowMeters() > VarioQt::instance().getAirSpaceThread()->getDrawHLimit())continue;
        QPainterPath pp = _p->airSpaceUnits[i].inset.translated(translateX,translateY);
        QTransform t;
        if(airspaceFrontUp)t.rotate(-course);
        paint.drawPath(t.map(pp).translated(width()/2,height()/2));
    }

    pen = QPen(Qt::black, 3);
    paint.setPen(pen);
    for(i=0;i<_p->airSpaceUnits.count();i++)
    {
        if(!_p->airSpaceUnits[i].isDrawable() || _p->airSpaceUnits[i].getALowMeters() > VarioQt::instance().getAirSpaceThread()->getDrawHLimit())continue;
        QPainterPath pp = _p->airSpaceUnits[i].path.translated(translateX,translateY);
        QTransform t;
        if(airspaceFrontUp)t.rotate(-course);
        paint.drawPath(t.map(pp).translated(width()/2,height()/2));
    }
    paint.end();

    //painter.drawImage(0,0,image);

    for(int i=0;i<240;i++)
    {
        for(int j=0;j<320;j++)
        {
            QRgb rgb = image.pixel(i,j);
            //blue line
            if(qBlue(rgb)>20 && qRed(rgb) < 20 && qGreen(rgb) < 20 && (i+j)%2)painter.drawPoint(i,j);
            //black line
            if(qBlue(rgb)<20 && qRed(rgb) < 20 && qGreen(rgb) < 20)painter.drawPoint(i,j);
        }
    }

    ast->accessible.unlock();
    painter.setBrush(QColor("black"));
    painter.drawEllipse(width()/2-4,height()/2-4,8,8);

    painter.setFont(Vario::get8pxFont());
    painter.setPen(QColor("white"));

    int violationNumber = 1;
    //painter.fillRect(1,height()-10*violationNumber,width(),10,QBrush("black"));
    for(i=0;i<_p->airSpaceUnits.count();i++)
    {
        if(_p->airSpaceUnits[i].getDistance() == AirSpaceDistanceType::VIOLATED)
        {
            //found one violated airspace
            painter.fillRect(0,height()-10*violationNumber,width(),10,QBrush("black"));

            QString msg;

            msg = OpenAirParser::airSpaceClassToString(_p->airSpaceUnits[i].aClass);
            msg += ":";
            msg += _p->airSpaceUnits[i].aLowStr + "->"+_p->airSpaceUnits[i].aHiStr;
            painter.drawText(0,height()-10*(violationNumber-1),msg);
            violationNumber++;

            painter.fillRect(0,height()-10*violationNumber,width(),10,QBrush("black"));
            painter.drawText(0,height()-10*(violationNumber-1),_p->airSpaceUnits[i].aName);
            violationNumber++;
        }
    }

    if(gotFocus)
    {
        painter.fillRect(width()-20,10,10,10,QBrush("black"));
        painter.setPen(QColor("white"));
        painter.drawText(width()-18,18,"F");
    }
    qDebug() << timer.elapsed();

    MapScaleIndicator mapScale(this);
    mapScale.setMetersToPixels(_metersToPixels);
    mapScale.setUnit(unit);
    mapScale.setTargetWidth(50);
    mapScale.paint(5,0);
}

QString AirSpacePlot::getUnit() const
{
    return unit;
}

void AirSpacePlot::setUnit(const QString &value)
{
    unit = value;
}
