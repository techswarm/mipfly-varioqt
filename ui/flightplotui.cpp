/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "flightplotui.h"
#include <qwidget.h>
#include <QPainter>
#include <IGC/igcparser.h>
#include <QTime>
#include <qdebug.h>
#include "utils/cstgraphics.h"
#include "utils/vario_common.h"
#include <math.h>

FlightPlotUI::FlightPlotUI(QWidget *parent):QWidget(parent)
{
}

void FlightPlotUI::zoomIn()
{
    _actionBtn = "Key: In";
    if(_metersToPixels<0.1)
    {
        _metersToPixels += 0.01;
    }
    else
        if(_metersToPixels<0.65)
            _metersToPixels += 0.05;
    qDebug()<<_metersToPixels;
}

void FlightPlotUI::zoomOut()
{
    _actionBtn = "Key: Out";
    if(_metersToPixels > 0.1)
        _metersToPixels -= 0.05;
    else
    {
        if(_metersToPixels > 0.02)
        {
            _metersToPixels -= 0.01;
        }
    }
    qDebug()<<_metersToPixels;
}

void FlightPlotUI::panLeft()
{
    _actionBtn = "Key: Left";
    if (_xOffset > -230)
        _xOffset -= 10;
}

void FlightPlotUI::panRight()
{
    _actionBtn = "Key: Right";
    if (_xOffset < 230)
        _xOffset += 10;
}

void FlightPlotUI::panUp()
{
    _actionBtn = "Key: Up";
    if (_yOffset > -150)
        _yOffset -= 10;
}

void FlightPlotUI::panDown()
{
    _actionBtn = "Key: Down";
    if (_yOffset < 150)
        _yOffset += 10;
}

void FlightPlotUI::gpioPressed(int btn)
{
    qDebug()<<"pressed "<<btn;
    if(btn == Vario::GPIO3)
    {
        //reset caption key
        _actionBtn = "";

        _action = FlightPlotUIActions(_action+1);

        if(_action >= FlightPlotUIActions::Size)
        {
            //restart FlightPlotUIActions mode
            _action = FlightPlotUIActions::Zoom;
        }

    }
    if(btn == Vario::GPIO1)
    {
        switch (_action)
        {
        case FlightPlotUIActions::Zoom:
        {
            zoomIn();
            break;
        }
        case FlightPlotUIActions::PanUpDown:
        {
            panUp();
            break;
        }
        case FlightPlotUIActions::PanRightLeft:
        {
            panLeft();
            break;
        }
        default:
            break;
        }
    }

    if(btn == Vario::GPIO2)
    {
        switch (_action)
        {
        case FlightPlotUIActions::Zoom:
        {
            zoomOut();
            break;
        }
        case FlightPlotUIActions::PanUpDown:
        {
            panDown();
            break;
        }
        case FlightPlotUIActions::PanRightLeft:
        {
            panRight();
            break;
        }
        default:
            break;
        }
    }
    repaint();
}

void FlightPlotUI::gpioLongPressed(int btn)
{
}

void FlightPlotUI::setIGCPath(QString path)
{
    IGCPath = path;
    igcP.setPath(path);
    igcP.parse();
}

void FlightPlotUI::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);

    painter.fillRect(0, 0, width(), height(), Qt::white);

    qDebug()<<"startIGCProcess";

    Coord center;
    center.setLat( (igcP.latMax + igcP.latMin)/2 );
    center.setLon( (igcP.lonMax + igcP.lonMin)/2 );



    double distanceM = center.distanceKm(igcP.igcLogEntrys[0].coordonates)*1000;
    double aob = center.bearingTo(igcP.igcLogEntrys[0].coordonates);
    QPoint pPoint;
    pPoint.setX((width()+_xOffset)/2+distanceM*_metersToPixels*sin(aob));
    pPoint.setY((height()+_yOffset)/2-distanceM*_metersToPixels*cos(aob));

    foreach(igcLogEntry ile,igcP.igcLogEntrys)
    {
        QPoint aPoint;
        distanceM = center.distanceKm(ile.coordonates)*1000;
        aob = center.bearingTo(ile.coordonates);
        aPoint.setX((width()+_xOffset)/2+distanceM*_metersToPixels*sin(aob));
        aPoint.setY((height()+_yOffset)/2-distanceM*_metersToPixels*cos(aob));
        painter.drawLine(pPoint,aPoint);
        pPoint.setX(aPoint.x());
        pPoint.setY(aPoint.y());
    }



    painter.fillRect(0,0,width(),25,Qt::black);
    painter.setPen(QPen(QColor("white")));

    painter.fillRect(0,height()-25,width(),25,Qt::black);


    if(10000*_metersToPixels <=150)
    {
        Vario::drawStr(4, 6, "10 km", 14, painter);
        painter.fillRect(50,6,10000*_metersToPixels,14,Qt::white);
    } else if(5000*_metersToPixels <=150)
    {
        Vario::drawStr(4, 6, "5 km", 14, painter);
        painter.fillRect(50,6,5000*_metersToPixels,14,Qt::white);
    } else if(2000*_metersToPixels <=150)
    {
        Vario::drawStr(4, 6, "2 km", 14, painter);
        painter.fillRect(50,6,2000*_metersToPixels,14,Qt::white);
    } else if(1000 *_metersToPixels <=150)
    {
        Vario::drawStr(4, 6, "1 km", 14, painter);
        painter.fillRect(50,6,1000*_metersToPixels,14,Qt::white);
    } else if(500 *_metersToPixels <=150)
    {
        Vario::drawStr(4, 6, "500 m", 14, painter);
        painter.fillRect(50,6,500*_metersToPixels,14,Qt::white);
    } else if(200 *_metersToPixels <=150)
    {
        Vario::drawStr(4, 6, "200 m", 14, painter);
        painter.fillRect(50,6,200*_metersToPixels,14,Qt::white);
    }

    switch (_action)
    {
    case FlightPlotUIActions::Zoom:
    {
        Vario::drawStr(4, 182, "Mode: Zoom_In_Out. " + _actionBtn, 14, painter);
        break;
    }
    case FlightPlotUIActions::PanUpDown:
    {
        Vario::drawStr(4, 182, "Mode: Up_Down. " + _actionBtn, 14, painter);
        break;
    }
    case FlightPlotUIActions::PanRightLeft:
    {
        Vario::drawStr(4, 182, "Mode: Right_Left. " + _actionBtn, 14, painter);
        break;
    }
    default:
        break;
    }

    qDebug()<<"endIGCProcess";

}
