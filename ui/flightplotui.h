/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FLIGHTPLOTUI_H
#define FLIGHTPLOTUI_H

#include <QWidget>
#include <QString>
#include "IGC/igcparser.h"

enum FlightPlotUIActions
{
    Zoom,
    PanUpDown,
    PanRightLeft,
    Size=3
};

class FlightPlotUI : public QWidget
{
    private:
        void paintEvent(QPaintEvent *event)override;
    //meters to pixels shoould be a maximum of 1
    //increment should be 0.025
    double _metersToPixels = 0.25;
    int _xOffset = 0;
    int _yOffset = 0;
    QString _actionBtn = "";
    FlightPlotUIActions _action = FlightPlotUIActions::Size;
    QString IGCPath = "";
    igcParser igcP;

public:
    FlightPlotUI(QWidget *parent);
    void zoomIn();
    void zoomOut();
    void panRight();
    void panLeft();
    void panUp();
    void panDown();
    void center();
    void setIGCPath(QString path);


    void gpioPressed(int btn);
    void gpioLongPressed(int btn);
};

#endif // FLIGHTPLOTUI_H
