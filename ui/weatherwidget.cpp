/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "weatherwidget.h"
#include <QPainter>

#include <sstream>

#include "utils/cstgraphics.h"
#include <QtMath>
#include <cmath>    // for M_PI and M_PI_2

using Vario::drawStr;
using Vario::strWidth;

WeatherWidget::WeatherWidget(QWidget *parent) : BorderBox(parent)
{
    resize(widthM,heightM);
}

void WeatherWidget::paintEvent(QPaintEvent *event) {

    QPainter painter(this);
    painter.setPen(QPen(QColor("black")));

    if(border)
    {
        painter.drawLine(0, 0, width() - 1, 0);
        painter.drawLine(width() - 1, 0, width() - 1, height() - 1);
        painter.drawLine(0, height() - 1, width() - 1, height() - 1);
        painter.drawLine(0, 0, 0, height() - 1);
    }

    int captionHeight = 0;

    if(caption !="")
    {
        painter.fillRect(0,0, width() - 1, 14, QColor("black"));
        painter.setPen(QPen(QColor("white")));
        drawStr(width()/2, 0, caption, 14, painter, Qt::AlignHCenter);
        captionHeight = 14;
    }
    painter.setPen(QPen(QColor("black")));

    int y = (height() + captionHeight - valHeight * 2) / 2;
    int x = 2;

    _xc = 15;
    _yc = height() / 2 + 7;

    int h1 = 12, h2 = 4, h3 = 12, wd = 10;
    QPoint arrPts[] { {_xc, _yc-h1}, {_xc-wd, _yc+h3}, {_xc, _yc+h2}, {_xc+wd, _yc+h3} };

    float angle =  ( _windHeading * M_PI ) / 180;
    angle += M_PI;

    for  (QPoint &pt : arrPts) {
        qreal oldX = qreal(pt.x() - _xc);
        qreal oldY = qreal(pt.y() - _yc);

        int x = int(oldX * qCos(angle) - oldY * qSin(angle));
        int y = int(oldY * qCos(angle) + oldX * qSin(angle));

        pt.setX(x + _xc);
        pt.setY(y + _yc);
    }
    painter.setPen(QPen(QBrush("black"),2));
    painter.drawPolygon(arrPts, 4);

    painter.setPen(QPen(QColor("black")));
    drawStr(x + 5 + valHeight * 2, y, _stationName, valHeight, painter, Qt::AlignCenter);
    y = y + valHeight;
    drawStr(x + 5 + valHeight * 2, y, QString::number(_windSpeed) + "-" + QString::number(_windGust) + " " + unit, valHeight, painter, Qt::AlignCenter);
}

void WeatherWidget::setStationName(QString value){
    _stationName = value;
}

void WeatherWidget::setWindSpeed(double value){
    _windSpeed = value;
}

void WeatherWidget::setWindGust(double value){
    _windGust = value;
}

void WeatherWidget::setWindHeading(int value){
    _windHeading = value;
    update();
}

void WeatherWidget::setWidth(int value)
{
    widthM = value;
    resize(widthM,height());
}

void WeatherWidget::setHeight(int value)
{
    heightM = value;
    resize(width(),heightM);
}

void WeatherWidget::setBorder(bool value)
{
    border = value;
}

void WeatherWidget::setUnit(const QString &value)
{
    unit = value;
}

void WeatherWidget::setCaption(const QString &value)
{
    caption = value;
    //we need border for caption
    if(caption != ""){
        border = true;
    }
}

void WeatherWidget::setSize(int size)
{
    valHeight = size;
}
