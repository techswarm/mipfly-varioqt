/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef UNITBOX_H
#define UNITBOX_H

#include <QWidget>

#include "borderbox.h"

class UnitBox : public BorderBox
{
    Q_OBJECT
public:
    explicit UnitBox(QWidget *parent = 0);

    void setDrawBorders(bool borders);
signals:

public slots:
    void setUnit(const QString &str);
    void setValue(const QString &str);
    void setSize(int size);

protected:
    virtual void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;

private:
    static const int padding = 3;

    QString unit;
    QString val;

    int unitHeight = 14;
    int valHeight = 34;
    bool drawBorders = false;
};

#endif // UNITBOX_H
