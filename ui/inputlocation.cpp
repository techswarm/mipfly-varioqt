/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "inputlocation.h"
#include "ui_inputlocation.h"
#include "utils/qfonts.h"
#include "VarioQt.h"

using Vario::getFont;

InputLocation::InputLocation(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::InputLocation)
{
    ui->setupUi(this);

    ui->lblLat->setValue("Lat");
    ui->lblLon->setValue("Lon");
    ui->lblAlt->setValue("Alt");
    ui->lblName->setValue("Name");

    setNoBorders();

    ui->valLat->setFont(getFont(VarioQt::instance().getMnuFontName(),VarioQt::instance().getMnuFontSize()));
    ui->valLon->setFont(getFont(VarioQt::instance().getMnuFontName(),VarioQt::instance().getMnuFontSize()));
    ui->valAlt->setFont(getFont(VarioQt::instance().getMnuFontName(),VarioQt::instance().getMnuFontSize()));
    ui->valName->setFont(getFont(VarioQt::instance().getMnuFontName(),VarioQt::instance().getMnuFontSize()));
}

InputLocation::~InputLocation()
{
    delete ui;
}

void InputLocation::setNoBorders() {
    ui->lblLat->setNoBorder();
    ui->lblLon->setNoBorder();
    ui->lblAlt->setNoBorder();
    ui->lblName->setNoBorder();
}

void InputLocation::highlightLat() {
    ui->lblLat->setAllBorders();
}
void InputLocation::highlightLon() {
    ui->lblLon->setAllBorders();
}
void InputLocation::highlightAlt() {
    ui->lblAlt->setAllBorders();
}
void InputLocation::highlightName() {
    ui->lblName->setAllBorders();
}

void InputLocation::setLatitude(Latitude lat) {
    latStr = QString::number(lat.dec());
    ui->valLat->setText(latStr);
}

void InputLocation::setLongitude(Longitude lon) {
    lonStr = QString::number(lon.dec());
    ui->valLon->setText(lonStr);
}

void InputLocation::setAltitude(int val) {
    altStr = QString::number(val);
    ui->valAlt->setText(altStr);
}

void InputLocation::accept(QChar chr) {
    nameStr.append(chr);
    ui->valName->setText(nameStr);
}

void InputLocation::clear() {
    nameStr.clear();
    ui->valName->setText(nameStr);
}

void InputLocation::backspace() {
    nameStr.remove(nameStr.length()- 1, 1);
    ui->valName->setText(nameStr);
}

void InputLocation::acceptLat(QChar chr) {
    latStr.append(chr);
    ui->valLat->setText(latStr);
}

void InputLocation::acceptLon(QChar chr) {
    lonStr.append(chr);
    ui->valLon->setText(lonStr);
}

void InputLocation::acceptAlt(QChar chr) {
    altStr.append(chr);
    ui->valAlt->setText(altStr);
}

void InputLocation::clearLat() {
    latStr.clear();
    ui->valLat->setText(latStr);
}

void InputLocation::clearLon() {
    lonStr.clear();
    ui->valLon->setText(lonStr);
}

void InputLocation::clearAlt() {
    altStr.clear();
    ui->valAlt->setText(altStr);
}

void InputLocation::backspaceLat() {
    latStr.remove(latStr.length()- 1, 1);
    ui->valLat->setText(latStr);
}

void InputLocation::backspaceLon() {
    lonStr.remove(lonStr.length()- 1, 1);
    ui->valLon->setText(lonStr);
}

void InputLocation::backspaceAlt() {
    altStr.remove(altStr.length()- 1, 1);
    ui->valAlt->setText(altStr);
}

int InputLocation::altitude() const {
    return altStr.toInt();
}

double InputLocation::latitude() const {
    return latStr.toDouble();
}

double InputLocation::longitude() const {
    return lonStr.toDouble();
}

QString InputLocation::name() const {
    return nameStr;
}
