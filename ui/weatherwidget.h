/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef WEATHERWIDGET_H
#define WEATHERWIDGET_H

#include <ui/magicbox.h>


class WeatherWidget : public BorderBox
{
    Q_OBJECT
public:
    explicit WeatherWidget(QWidget *parent = nullptr);

    void setStationName(QString value);
    void setWindSpeed(double value);
    void setWindGust(double value);
    void setWindHeading(int value);

    void setSize(int size);
    void setCaption(const QString &value);
    void setBorder(bool value);
    void setUnit(const QString &value);
    void setHeight(int value);
    void setWidth(int value);
private:
    QString _stationName="";
    double _windSpeed=0,_windGust=0;
    int _windHeading=0;
    static const int padding = 3;
    int heightM = 1, widthM = 1;

    QString val;
    int valHeight = 22;
    int unitHeight = 16;

    Qt::Alignment alignment = Qt::AlignHCenter;

    QString caption;
    QString unit;

    bool border = false;

    int _xc;
    int _yc;

    virtual void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;
signals:

public slots:
};

#endif // WEATHERWIDGET_H
