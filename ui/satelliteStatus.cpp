/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "satelliteStatus.h"
#include <QPainter>
#include <QtDebug>
#include <string>

#include "utils/cstgraphics.h"

SatelliteStatus::SatelliteStatus(QWidget *parent) : QWidget(parent)
{
    fix_ = FIX_UNKNOWN;
}

void SatelliteStatus::updateVDop(double vDop, int fix) {
    if ((this->vDop_ != vDop) || (this->fix_ != vDop)) {
        this->vDop_ = vDop;

        //(3) Possible values for navMode: 1 = No fix, 2 = 2D fix, 3 = 3D fix
        switch (fix) {
        case 1:
            this->fix_ = FIX_NONE;
            break;
        case 2:
            this->fix_ = FIX_2D;
            break;
        case 3:
            this->fix_  = FIX_3D;
            break;
        }

        update();
    }
}

void SatelliteStatus::paintStatusBars(QPainter *painter) {

        bool markup;

        for (int markerIdx=0;markerIdx<5;markerIdx++) {

            markup = false;

            switch (markerIdx) {
                case 0:
                    if ((fix_ == FIX_2D) || (fix_ == FIX_3D)) markup = true;
                    break;
                case 1:
                    if (fix_ == FIX_3D && vDop_ < 99.0) markup = true;
                    break;
                case 2:
                    if (fix_ == FIX_3D && vDop_ < 3.0) markup = true;
                    break;
                case 3:
                    if (fix_ == FIX_3D && vDop_ < 2.0) markup = true;
                    break;
                case 4:
                    if (fix_ == FIX_3D && vDop_ < 1.5) markup = true;
                    break;
            }

            if (markup) {
                painter->drawRect( markerIdx+(markerIdx*3), 0, 2, height()-1);
                painter->fillRect( (markerIdx+(markerIdx*3)), 0, 2, height()-1, QBrush(QColor(Qt::black)));
            } else {
                painter->drawRect( (markerIdx+(markerIdx*3)), 0, 2, height()-1);
            }
        }
}


void SatelliteStatus::paintEvent(QPaintEvent *event) {

    QPainter painter(this);

    painter.setPen(QColor(Qt::black));
    paintStatusBars(&painter);

}



