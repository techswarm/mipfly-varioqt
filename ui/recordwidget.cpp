/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "recordwidget.h"
#include <QPainter>
#include <QLabel>
#include <iostream>
#include "threads/takeoffthread.h"

RecordWidget::RecordWidget(QWidget *parent) : QWidget(parent)
{

    connect(&tick,SIGNAL(timeout()),this,SLOT(timerTick()));
    tick.setInterval(1000);
    tick.start();
}

void RecordWidget::startRecording()
{
    _recording = 1;
}

void RecordWidget::stopRecording()
{
    _recording = 0;
}

void RecordWidget::paintEvent(QPaintEvent *event) {

    QPainter painter(this);
    if(_recording == 1){
        QPixmap image("resources/bmp/record.bmp");
        painter.drawPixmap(QPoint(0,0),image);
    }
}

void RecordWidget::timerTick()
{
    update();
}
