/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "thermalcenteringui.h"
#include <QPainter>
#include <QSettings>
#include <QtDebug>
#include <QtMath>
#include "VarioQt.h"

namespace
{
double amap(double x, double in_min, double in_max, double out_min, double out_max)
{
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
#define constrain(amt,low,high) ((amt)<(low)?(low):((amt)>(high)?(high):(amt)))
}

thermalCenteringUI::thermalCenteringUI(QWidget *parent, const int numSamples) : QWidget(parent)
{
    _lastPosIndex = 0;
    _numSamples = numSamples;
    metersToPixels = 0.5;

    QSettings settings;
    pathMarkerSize = settings.value("termaling/pathMarkerSize",1).toInt();

}



void thermalCenteringUI::pushCMPS(const int cmps)
{
    //qDebug() << "[thermalPlot] Pushed CMPS " << cmps;
    _vspeedCMPS = cmps;
}

void thermalCenteringUI::pushPoint(const Coord location)
{
    //    qDebug() << "[thermalPlot] Pushed point";
    //should be pushed once every second
    _lastPosIndex++;
    if(_lastPosIndex >= _numSamples)
    {
        _lastPosIndex = 0;
        dispEnable = true;
    }
    _loc[_lastPosIndex] = location;
    _vspeedCMPSLoc[_lastPosIndex] = _vspeedCMPS;
    repaint();
}

void thermalCenteringUI::pushHeading(const double headingDgr)
{
    //    qDebug() << "[thermalPlot] Pushed heading " << headingDgr;
    _heading = qDegreesToRadians( headingDgr);
    update();
}

void thermalCenteringUI::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);

    painter.setPen(QColor(Qt::black));
    //painter.fillRect(0, 0, width(), height(), Qt::white);
    painter.drawRect(0, 0, width(), height());

    painter.drawLine(width()/2,0,width()/2,height());
    painter.drawLine(0,height()/2,width(),height()/2);

    if(!dispEnable)return;


    int i;

    int vSpeedMax = -1; //negative speeds are not taken into account

    for(i=0;i<_numSamples;i++)
    {
        if(_vspeedCMPSLoc[i]>vSpeedMax)
        {
            vSpeedMax = _vspeedCMPSLoc[i];
        }
    }

    double vspeedScaleCMPS = 0.1;

    //if(vSpeedMax > 300)vspeedScaleCMPS = 0.05;
    vspeedScaleCMPS = amap((double)vSpeedMax,100,500,0.1,0.03);
    vspeedScaleCMPS = constrain(vspeedScaleCMPS,0.03,0.1);
    vspeedScaleCMPS /= 3;

    //qDebug()<<vspeedScaleCMPS<<" "<<vSpeedMax;

    double distanceMax = 0;

    painter.drawLine(width()-12,height()/2,width(),height()/2);

    for(int i=-5;i<=5;i++)
    {
        painter.drawLine(width()-10,height()/2 - i*10,width(),height()/2 - i*10);
    }

    painter.setBrush(QBrush(Qt::black));
    if(_vspeedCMPS>0)painter.drawRect(width()-7,height()/2,10,-_vspeedCMPS/10);
    else painter.drawRect(width()-7,height()/2,10,-_vspeedCMPS/10);

    for(i=0;i<_numSamples;i++)
    {
        //_vspeedCMPSLoc[i]=50;
        if(_vspeedCMPSLoc[i]>=10)
        {
            //determine the distance and heading from center to point
            double distance = _loc[_lastPosIndex].distanceKm(_loc[i])*1000;
            double aob = _loc[_lastPosIndex].bearingTo(_loc[i]);

            //distance = 50;
            //aob = M_PI_2;

            //compute true heading
            aob -= _heading;
            distance *= metersToPixels;
            if(distance>distanceMax)distanceMax = distance;

            //             qDebug() << "[thermalPlot] Pushed CMPS " << _vspeedCMPSLoc[i]/100;
            painter.setBrush(Qt::black);
            //painter.drawEllipse(width()/2 + cos(aob)*distance,height()/2 + sin(aob)*distance,_vspeedCMPSLoc[i]*vspeedScaleCMPS + 3,_vspeedCMPSLoc[i]*vspeedScaleCMPS+3);
            painter.drawEllipse(QPoint(width()/2 + sin(aob)*distance,
                                       height()/2 - cos(aob)*distance),
                                (int)(_vspeedCMPSLoc[i] * vspeedScaleCMPS + 3),
                                (int)(_vspeedCMPSLoc[i] * vspeedScaleCMPS + 3));
        }
        else
        {
            //determine the distance and heading from center to point
            double distance = _loc[_lastPosIndex].distanceKm(_loc[i])*1000;
            double aob = _loc[_lastPosIndex].bearingTo(_loc[i]);

            //distance = 50;
            //aob = M_PI_2;

            //compute true heading
            aob -= _heading;
            distance *= metersToPixels;
            if(distance>distanceMax)distanceMax = distance;
            //             qDebug() << "[thermalPlot] Pushed CMPS " << _vspeedCMPSLoc[i]/100;
            painter.setBrush(Qt::black);
            //painter.drawEllipse(width()/2 + cos(aob)*distance,height()/2 + sin(aob)*distance,_vspeedCMPSLoc[i]*vspeedScaleCMPS + 3,_vspeedCMPSLoc[i]*vspeedScaleCMPS+3);


            if (pathMarkerSize == 1) {
                painter.drawPoint(width()/2 + sin(aob)*distance,
                                  height()/2 - cos(aob)*distance);
            } else {
                painter.drawRect(width()/2 + sin(aob)*distance, height()/2 - cos(aob)*distance, pathMarkerSize, pathMarkerSize);
            }
        }
    }

    //Test to draw lastlift
    double distanceLastLift = _loc[_lastPosIndex].distanceKm(VarioQt::instance().getLastLift()->getCoordonates())*1000;
    double aobLastLift = _loc[_lastPosIndex].bearingTo(VarioQt::instance().getLastLift()->getCoordonates());
    aobLastLift -= _heading;
    distanceLastLift *= metersToPixels;

    painter.setBrush(Qt::black);
    painter.drawLine(width()/2 + sin(aobLastLift)*distanceLastLift-2, height()/2 - cos(aobLastLift)*distanceLastLift-2,
                     width()/2 + sin(aobLastLift)*distanceLastLift+2, height()/2 - cos(aobLastLift)*distanceLastLift+2);
    painter.drawLine(width()/2 + sin(aobLastLift)*distanceLastLift+2, height()/2 - cos(aobLastLift)*distanceLastLift-2,
                     width()/2 + sin(aobLastLift)*distanceLastLift-2, height()/2 - cos(aobLastLift)*distanceLastLift+2);


    if(distanceMax>height()/2-10)
    {
        //decrease draw size by decreaseing metersToPixels
        metersToPixels-=0.0025;
        if(metersToPixels<0.2)metersToPixels=0.2;
    }
    if(distanceMax<height()/2-15)
    {
        //increase draw size by increaseing metersToPixels
        metersToPixels+=0.0025;
        if(metersToPixels>1)metersToPixels=1;
    }
    //qDebug()<<metersToPixels;
#ifdef __ARMEL__
    VarioQt::instance().getFb2SPIThread()->forceUpdate();
#endif
}

thermalCenteringUI::~thermalCenteringUI()
{

}
