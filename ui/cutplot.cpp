/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "cutplot.h"
#include "VarioQt.h"
#include "navUtils/hgtcontainer.h"
#include <QPainter>
#include "utils/cstgraphics.h"
using Vario::drawStr;

CutPlot::CutPlot(QWidget *parent) : QWidget(parent)
{
    mapCenter.setLat(47);
    mapCenter.setLon(22);
    heightGPS = 180;

    tick.setInterval(1000);
    connect(&tick,SIGNAL(timeout()),this,SLOT(timerTick()));
    tick.start();
}

void CutPlot::setCenter(Coord coord)
{
    mapCenter.setLat(coord.getLat());
    mapCenter.setLon(coord.getLon());
    //mapCenter.setLat(47);
    //mapCenter.setLon(22);
}

void CutPlot::setHeight(int h)
{
    //show all obstacles 50 meters below
    heightGPS = h;
}

void CutPlot::setHeading(qreal h)
{
    heading = h;
}

void CutPlot::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);

    HGTContainer * hgtc;
    hgtc = VarioQt::instance().getHGTContainer();

    int xOffset = hgtc->getXOffset(mapCenter);
    int yOffset = hgtc->getYOffset(mapCenter);

    xOffset -= width()/2;
    yOffset -= height()/2;


    if(!hgtc->getValid())
    {
        drawStr(20, 20, "Data invalid", 14, painter);
        //return;
    }


    for(int i=0;i<width();i++)
    {
        for(int j=0;j<height();j++)
        {
            int xOffsetGet = xOffset+i;
            int yOffsetGet = yOffset+j;

            int localHeight = hgtc->getHeight(xOffsetGet,yOffsetGet);

            if(localHeight>heightGPS)painter.drawPoint(i,j);
            else
            {
                if(localHeight>heightGPS-75)
                {
                    if((i+j)%2==0)painter.drawPoint(i,j);
                }
                else
                    if(localHeight>heightGPS-150)
                    {
                        if((i+j)%3==0)painter.drawPoint(i,j);
                    }
            }
        }
    }

    //heading = 0;
    int _xc = width() / 2;
    int _yc = height() / 2;

    int h1 = 6, h2 = 2, h3 = 6, wd = 5;
    //int h1 = 5, h2 = 3, h3 = 5, wd = 3;
    QPoint arrPts[] { {_xc, _yc-h1}, {_xc-wd, _yc+h3}, {_xc, _yc+h2}, {_xc+wd, _yc+h3} };

    float angle =  ( heading * M_PI ) / 180;

    for  (QPoint &pt : arrPts) {
        qreal oldX = qreal(pt.x() - _xc);
        qreal oldY = qreal(pt.y() - _yc);

        int x = int(oldX * qCos(angle) - oldY * qSin(angle));
        int y = int(oldY * qCos(angle) + oldX * qSin(angle));

        pt.setX(x + _xc);
        pt.setY(y + _yc);
    }
    //painter.setBrush(Qt::white);
    //painter.drawEllipse(QPoint(width()/2,height()/2),7,7);

    painter.setPen(QPen(QBrush("white"),4));
    painter.drawPolygon(arrPts, 4);
    painter.setPen(QPen(QBrush("black"),2));
    painter.drawPolygon(arrPts, 4);
}

void CutPlot::timerTick()
{
    //force a offset compute to determine if HGT martric recalculation is required
    if(this->isVisible()==false)
    {
        HGTContainer * hgtc;
        hgtc = VarioQt::instance().getHGTContainer();
        hgtc->validateCenter(mapCenter);
    }
    update();
}
