/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MAPPLOT_H
#define MAPPLOT_H

#include <QWidget>
#include <QtSql>
#include "navUtils/coord.hpp"

class MapPlot : public QWidget
{
    Q_OBJECT
public:
    explicit MapPlot(QWidget *parent);
    void setCenter(Coord coord);

    QString getMapFolder() const;
    void setMapFolder(const QString &value);

    int getZoomLevel() const;
    void setZoomLevel(int value);

    void openDatabase();

    void focus();
    void unfocus();
    void shortPress(int btn);
protected:
    void timerEvent(QTimerEvent *event);

private:
    Coord mapCenter;
    qreal heading;
    QString mapName;
    QString mapNameSettings;
    int zoomLevel;
    void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;
    void drawTile(QImage tile, int x, int y);
    QByteArray getTileData(int x, int y);
    QSqlDatabase databaseGlobal;
    QStringList mapNames;
    int flipY(int zoom, int y);
    int loadedXTile,loadedYtile;
    QImage tile1, tile2, tile3, tile4, tile5, tile6, tile7, tile8, tile9;
    bool gotFocus = false;
    bool querySuccesfull = false;

public slots:
    void setHeading(qreal h);
};

#endif // MAPPLOT_H
