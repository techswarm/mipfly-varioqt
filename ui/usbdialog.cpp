/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "usbdialog.h"
#include "ui_usbdialog.h"

#include "mainui.h"
#include "utils/qfonts.h"
#include "VarioQt.h"

using Vario::getFont;

USBDialog::USBDialog(const NameManager &nameMgr, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::USBDialog),
    listMenu(nameMgr, this)
{
    ui->setupUi(this);

    ui->listView->setFont(getFont(VarioQt::instance().getMnuFontName(),VarioQt::instance().getMnuFontSize()));

    connect(ui->listView, &QListView::activated, this, &USBDialog::choicePressed);

    ui->listView->setModel(&rootMenu);
    ui->listView->setCurrentIndex(rootMenu.index(0));

    ui->statusBox->setSize(16);

    resetHideTimeout();
    connect(&secTimer,SIGNAL(timeout()),this,SLOT(secTick()));
}

USBDialog::~USBDialog()
{
    delete ui;
}

GPIOEmulator &USBDialog::gpioEmulator() const
{
    return reinterpret_cast<MainUI*>(parent())->gpioEmulator;
}

#include <QtDebug>

void USBDialog::gpioPressed(int btn)
{
    if(!VarioQt::instance().isActiveWidget(this))return;
    int row = ui->listView->currentIndex().row();

    if (btn == Vario::GPIO2) {
        row++;

        if (row == ui->listView->model()->rowCount())
            row = 0;


        ui->listView->setCurrentIndex(ui->listView->model()->index(row, 0));
    } else if (btn == Vario::GPIO1) {
        row--;

        if (row < 0)
            row = ui->listView->model()->rowCount() - 1;

        ui->listView->setCurrentIndex(ui->listView->model()->index(row, 0));
    } else if (btn == Vario::GPIO3) {
        choicePressed(ui->listView->currentIndex());
    }

}

void USBDialog::addDevice(const QString &dev)
{
    QRegularExpression re("/dev/sd.\\d+");
    if (re.match(dev).hasMatch()) {
        listMenu.addDevice(dev);
    }
}

void USBDialog::removeDevice(const QString &dev)
{
    listMenu.removeDevice(dev);
}

int USBDialog::deviceCount() const
{
    return listMenu.deviceCount();
}

void USBDialog::choicePressed(const QModelIndex &index)
{
    int row = index.row();

    if (state == 1) {
        if (row == USBMenuHide) {
            MainUI *mainUi = static_cast<MainUI*>(parent());
            mainUi->hideUSBDialog();
        } else {
            if (row == USBMenuGlides) {
                state = 2;
            } else if (row == USBMenuBoot) {
                state = 3;
            } else if (row == USBMenuDump) {
            state = 4;
            }
            ui->listView->setModel(&listMenu);
        }

    } else if (state == 2) {
        if (row == 0) {
            state = 1;
            ui->listView->setModel(&rootMenu);
        } else {
            listMenu.copyGlidesToIndex(row, *this);
            // return to rootMenu ?
        }
    } else if (state == 3) {
        if (row == 0) {
            state = 1;
            ui->listView->setModel(&rootMenu);
        } else {
            listMenu.copyBootloaderFromIndex(row, *this);
            // return to rootMenu ?
        }
    } else if (state == 4) {
        if (row == 0) {
            state = 1;
            ui->listView->setModel(&rootMenu);
        } else {
            listMenu.copyDumpToUSB(row, *this);
            // return to rootMenu ?
        }
    }
}

void USBDialog::resetHideTimeout()
{
    hideTimeout = 15;
}

void USBDialog::showEvent(QShowEvent *e)
{
    resetHideTimeout();
    QWidget::showEvent(e);
    VarioQt::instance().pushNewWidget(this);
}

void USBDialog::hideEvent(QHideEvent *e)
{
    QWidget::hideEvent(e);
    VarioQt::instance().deleteLastWidget();
}

void USBDialog::secTick()
{
    if(hideTimeout)
    {
        hideTimeout --;
        if(hideTimeout == 0)
        {
            close();
        }
    }
}

Ui::USBDialog& USBDialog::getUi() const
{
    return *ui;
}
