/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "battery.h"

#include <QPainter>


#include <QtDebug>
#include <string>

Battery::Battery(QWidget *parent) : QWidget(parent)
{
    _status = power.GetBatteryStatus();
    _value = power.GetBatteryCapacityPercent();
}


void Battery::paintEvent(QPaintEvent *event) {
    QPainter painter(this);

    int head = width() - 1 - 2;

    painter.setPen(QColor(Qt::black));

    painter.drawRect(0,0, head, height() - 1);
    painter.fillRect(head, 2, 2, 4, QColor(Qt::black));

    if(_status.compare("Charging")==0)
    {
        //force charging animation for low battery
        if(_value < 10)_value = 20;
        _barAnimationLevel+=3;
        if(_barAnimationLevel>_value)_barAnimationLevel = 0;
        int chargeBar = (_barAnimationLevel/10)*2;

        painter.fillRect(1,1,chargeBar,7,QColor(Qt::black));
//        fillRect(_x+1,_y+1,chargeBar,7,black);
    }
    else
    {
        //compensate integer division
        _value += 5;
        int chargeBar = (_value/5);
        painter.fillRect(1,1,chargeBar,7, QColor(Qt::black));

//        fillRect(_x+1,_y+1,chargeBar,7,black);
    }
}


void Battery::timerEvent(QTimerEvent *event) {
    int newVal = power.GetBatteryCapacityPercent();
    std::string newStatus = power.GetBatteryStatus();

    if ( newVal != _value || newStatus == "Charging" || newStatus != _status) {
        _value = newVal;
        update();
    }

    _status = newStatus;
}
