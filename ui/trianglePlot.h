/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TRIANGLEPLOT_H
#define TRIANGLEPLOT_H

#include <QWidget>
#include <QTimer>
#include "navWaypoint/task.h"
#include "navUtils/faiassistcompute.h"



class TrianglePlot : public QWidget
{
    Q_OBJECT
public:
    explicit TrianglePlot(QWidget *parent = nullptr);

    void zoomIn();
    void zoomOut();
    void zoomToExtent();
signals:

public slots:

private slots:
    void invalidateTimerTick();


private:
    Task *pTask;
    FaiAssistCompute *pFac;
    QTimer invalidateTimer;
    void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;

    double _metersToPixels = 0.05;
};

#endif // TASKPLOT_H
