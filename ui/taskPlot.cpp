/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "taskPlot.h"
#include "VarioQt.h"
#include <QPainter>
#include <QDebug>
#include "uiElements/mapscaleindicator.h"
#include "navUtils/distance.h"

TaskPlot::TaskPlot(QWidget *parent) : QWidget(parent)
{
    pTask = VarioQt::instance().getTask();
    pFac = VarioQt::instance().getFaiAssistCompute();
    invalidateTimer.setInterval(200);
    connect(&invalidateTimer,SIGNAL(timeout()),this,SLOT(invalidateTimerTick()));
    invalidateTimer.start();
}

void TaskPlot::invalidateTimerTick()
{
    update();
}

void TaskPlot::setShowTaskData(bool value)
{
    showTaskData = value;
}

void TaskPlot::setPTask(Task *value)
{
    pTask = value;
}

void TaskPlot::zoomIn()
{
    _metersToPixels *= 1.2;
}

void TaskPlot::zoomOut()
{
    _metersToPixels *= 0.8;
}

void TaskPlot::zoomToExtent()
{

}

void TaskPlot::paintEvent(QPaintEvent *event)
{
    if(pTask == nullptr)return;
    QPainter painter(this);
    Coord center;
    if(pTask->getCenterToTP1() == true && pTask->getFlying() == false)
    {
        if(pTask->getTurnPoints().count()>0)
            center = pTask->getTurnPoints().at(0).getWaypoint().coordonate();
    }
    else
    {
        center = pTask->getCenter();
    }

    painter.setPen(QPen(QColor("black")));


    double metersToPixels = _metersToPixels;

    if(pTask->zoom())metersToPixels = 0.5;

    int tpId=0;

    QList<QPointF> centers;
    QPolygonF poly;
    poly.clear();
    foreach (Coord coord, pFac->getTaskPoints()) {
        double distanceMeters = center.distanceKmAuto(coord) * 1000;

        double angleToCenter = center.bearingTo(coord);

        double distancePixels = distanceMeters * metersToPixels;
        //qDebug()<<distancePixels;
        QPointF center(distancePixels*sin(angleToCenter),distancePixels*cos(angleToCenter)*-1);
        poly.append(QPointF(center.x()+width()/2,center.y()+height()/2));
        //painter.drawPoint(center.x()+width()/2,center.y()+height()/2);
    }
    painter.drawPolyline(poly);

    foreach (TurnPoint tp, pTask->getTurnPoints()) {
        double distanceMeters = center.distanceKmAuto(tp.getWaypoint().coordonate()) * 1000;

        double angleToCenter = center.bearingTo(tp.getWaypoint().coordonate());

        double distancePixels = distanceMeters * metersToPixels;
        //qDebug()<<distancePixels;
        QPointF center(distancePixels*sin(angleToCenter),distancePixels*cos(angleToCenter)*-1);
        centers.append(center);

        QPen pen;
        pen.setColor(Qt::black);
        if(pTask->getNavTargetIndex() == tpId)
            pen.setStyle(Qt::PenStyle::SolidLine);
        else
            pen.setStyle(Qt::PenStyle::DotLine);
        painter.setPen(pen);

        if(tp.getGoal() != TurnPoint::GoalType::Line)
        {
            painter.drawEllipse(center.x() + width()/2 - tp.getRadiusLengthMeters() * metersToPixels,
                                center.y() + height()/2 - tp.getRadiusLengthMeters() * metersToPixels,
                                tp.getRadiusLengthMeters() * metersToPixels * 2,
                                tp.getRadiusLengthMeters() * metersToPixels * 2);
        }
        else
        {
            double goalLinePerpendicular = pTask->getGoalLineAngle(tpId);

            QPointF lineCenterWidget(center.x()+width()/2,center.y()+height()/2);
            double lineLength = tp.getRadiusLengthMeters() / 2 * metersToPixels;
            double lineOffsetX = lineLength * sin(goalLinePerpendicular+M_PI_2);
            double lineOffsetY = lineLength * cos(goalLinePerpendicular+M_PI_2);
            painter.drawLine(lineCenterWidget.x(),
                             lineCenterWidget.y(),
                             lineCenterWidget.x()+lineOffsetX,
                             lineCenterWidget.y()-lineOffsetY);

            painter.drawLine(lineCenterWidget.x(),
                             lineCenterWidget.y(),
                             lineCenterWidget.x()-lineOffsetX,
                             lineCenterWidget.y()+lineOffsetY);

        }
        tpId++;
    }

    int navTarget = pTask->getNavTargetIndex();

    if((navTarget<centers.count() && navTarget >=0) || pTask->getNavToSpecial()==true)
    {
        QPen pen;
        pen.setColor(Qt::black);
        pen.setStyle(Qt::PenStyle::DashLine);
        painter.setPen(pen);

        Coord targetWpOptimum = pTask->getNextNavTarget();

        double distanceMeters = center.distanceKmAuto(targetWpOptimum) * 1000;

        double angleToCenter = center.bearingTo(targetWpOptimum);

        double distancePixels = distanceMeters * metersToPixels;
        //qDebug()<<distancePixels;
        QPointF targetWpOptimumXY(distancePixels*sin(angleToCenter),distancePixels*cos(angleToCenter)*-1);


        painter.drawLine(targetWpOptimumXY.x()+width()/2,
                         targetWpOptimumXY.y()+height()/2,
                         width()/2,
                         height()/2
                         );
        if(pTask->getNavToSpecial()==false)
        {
            for(int i=navTarget+1;i<centers.count();i++)
            {
                Coord targetWpOptimumNext = pTask->getOptimisedLocation(i);
                double distanceMeters = center.distanceKmAuto(targetWpOptimumNext) * 1000;

                double angleToCenter = center.bearingTo(targetWpOptimumNext);

                double distancePixels = distanceMeters * metersToPixels;
                //qDebug()<<distancePixels;
                QPointF targetWpOptimumXYNext(distancePixels*sin(angleToCenter),distancePixels*cos(angleToCenter)*-1);
                painter.drawLine(targetWpOptimumXY.x()+width()/2,
                                 targetWpOptimumXY.y()+height()/2,
                                 targetWpOptimumXYNext.x()+width()/2,
                                 targetWpOptimumXYNext.y()+height()/2
                                 );
                targetWpOptimumXY = targetWpOptimumXYNext;
            }
        }
    }

    QPainter painter2(this);
    int _xc = width() / 2;
    int _yc = height() / 2;

    double lineOffsetX = 600 * sin(pTask->getCourse() * M_PI / 180);
    double lineOffsetY = 600 * cos(pTask->getCourse() * M_PI / 180);


    painter2.drawLine(width()/2,
                      height()/2,
                      width()/2+lineOffsetX,
                      height()/2-lineOffsetY
                      );

    int h1 = 6, h2 = 2, h3 = 6, wd = 5;
    //int h1 = 5, h2 = 3, h3 = 5, wd = 3;
    QPoint arrPts[] { {_xc, _yc-h1}, {_xc-wd, _yc+h3}, {_xc, _yc+h2}, {_xc+wd, _yc+h3} };

    float angle =  ( pTask->getCourse() * M_PI ) / 180;

    for  (QPoint &pt : arrPts) {
        qreal oldX = qreal(pt.x() - _xc);
        qreal oldY = qreal(pt.y() - _yc);

        int x = int(oldX * qCos(angle) - oldY * qSin(angle));
        int y = int(oldY * qCos(angle) + oldX * qSin(angle));

        pt.setX(x + _xc);
        pt.setY(y + _yc);
    }
    painter2.setBrush(Qt::white);
    painter2.drawEllipse(QPoint(width()/2,height()/2),9,9);


    painter2.setPen(QPen(QBrush("black"),2));
    painter2.drawPolygon(arrPts, 4);

    if(showTaskData)
    {
        QString OptimisedLength = tr("Total op: ");
        Distance totalLength(pTask->getOptimisedTaskLength()*1000);
        OptimisedLength += totalLength.getText(true,2);
        //painter.setPen(Qt::black);
        painter2.setPen(QPen(QBrush("black"),1));
        Vario::drawStr(0,0,OptimisedLength,14,painter2);
    }

    MapScaleIndicator mapScale(this);
    mapScale.setMetersToPixels(metersToPixels);
    mapScale.setTargetWidth(50);
    mapScale.paint(5,height()-14);
}

void TaskPlot::hide()
{
    QWidget::hide();
    showTaskData = false;
}

QString TaskPlot::getUnit() const
{
    return unit;
}

void TaskPlot::setUnit(const QString &value)
{
    unit = value;
}
