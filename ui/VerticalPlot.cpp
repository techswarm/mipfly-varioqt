/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "VerticalPlot.h"
#include <QPainter>
#include "utils/cstgraphics.h"
#include "utils/vario_common.h"
#include "utils/unitconverter.h"
#include <Qt>

VerticalPlot::VerticalPlot(QWidget *parent) : QWidget(parent)
{

}

void VerticalPlot::pushHeight(int height)
{
    if(_pushPostscale-- == 0)
    {
        _pushPostscale = 10;
        _points.append(height);
        if(_points.count()>=_maxPoints)
        {
            _points.removeFirst();
        }
        update();
    }
}

QString VerticalPlot::getUnit() const
{
    return unit;
}

void VerticalPlot::setUnit(const QString &value)
{
    unit = value;
}

void VerticalPlot::setDrawBorders(bool borders){
    drawBorders = borders;
}

void VerticalPlot::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    //painter.fillRect(0, 0, width(), height(), Qt::white);

    if(drawBorders){
        painter.drawLine(0, 0, width() - 1, 0);
        painter.drawLine(width() - 1, 0, width() - 1, height() - 1);
        painter.drawLine(0, height() - 1, width() - 1, height() - 1);
        painter.drawLine(0, 0, 0, height() - 1);
    }

    int numElements = _points.count();
    if(numElements==0)return;

    numElements = _maxPoints;

    float vRez = ((float)height()/2)/100;

    int lastElement = _points.last();

    QList<QPoint> graphPoints;

    int count = 0;

    foreach (int point, _points) {
        QPoint graphicPoint(count*width()/numElements,(lastElement-point)*vRez + height()/2);
        count++;
        graphPoints.append(graphicPoint);
    }

    for(int i = 1;i<graphPoints.count();i++)
    {
        painter.drawLine(graphPoints[i-1],graphPoints[i]);
    }

    if(unit == "ft")
    {
        Vario::drawStr(2, height()-16, QString::number(UnitConverter::mToFt(lastElement - 100),'f',0)+" ft", 14, painter, Qt::AlignLeft);
        Vario::drawStr(2, 2, QString::number(UnitConverter::mToFt(lastElement + 100),'f',0)+" ft", 14, painter, Qt::AlignLeft);
    }
    else
    {
        Vario::drawStr(2, height()-16, QString::number(lastElement - 100)+" m", 14, painter, Qt::AlignLeft);
        Vario::drawStr(2, 2, QString::number(lastElement + 100)+" m", 14, painter, Qt::AlignLeft);
    }
}
