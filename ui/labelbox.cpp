/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "labelbox.h"

#include <QPainter>

#include "utils/cstgraphics.h"
using Vario::drawStr;

LabelBox::LabelBox(QWidget *parent) : BorderBox(parent)
{

}

void LabelBox::paintEvent(QPaintEvent *event) {
    BorderBox::paintEvent(event);

    QPainter painter(this);
    painter.setPen(QPen(QColor("black")));

    if(drawBorders){
        painter.drawLine(0, 0, width() - 1, 0);
        painter.drawLine(width() - 1, 0, width() - 1, height() - 1);
        painter.drawLine(0, height() - 1, width() - 1, height() - 1);
        painter.drawLine(0, 0, 0, height() - 1);
    }

    int y = (height() - valHeight) / 2;
    int x;

    if (alignment == Qt::AlignLeft) {
        x = 0;
    } else if (alignment == Qt::AlignHCenter) {
        x = width() / 2;
    } else {
        x = width() - 1;
    }
    drawStr(x, y, val, valHeight, painter, alignment);
}

QString LabelBox::getValue() const
{
    return val;
}

void LabelBox::setSize(int size)
{
    valHeight = size;
}

void LabelBox::setValue(const QString &str) {
    val = str;
    update();
}

void LabelBox::setAlignment(Qt::Alignment flags) {
    alignment = flags;
}

void LabelBox::setDrawBorders(bool borders){
    drawBorders = borders;
}
