/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VERTICALPLOT_H
#define VERTICALPLOT_H

#include <QWidget>
#include <QList>

class VerticalPlot : public QWidget
{
    Q_OBJECT
public:
    explicit VerticalPlot(QWidget *parent = 0);
    void setDrawBorders(bool borders);
    QString getUnit() const;
    void setUnit(const QString &value);

signals:

public slots:
    void pushHeight(int height);
private:
    int _maxPoints = 240;
    QString unit;
    QList<int> _points;
    int _pushPostscale = 10;
    void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;
    bool drawBorders = false;
};

#endif // VERTICALPLOT_H
