/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gpswidget.h"
#include <QPainter>
#include <QLabel>

#include <iostream>

GPSWidget::GPSWidget(QWidget *parent) : QWidget(parent)
{
    connect(&tick,SIGNAL(timeout()),this,SLOT(timerTick()));
    tick.setInterval(500);
    tick.start();
}

void GPSWidget::setValue(int value){
    _GPSConn = value;
}

void GPSWidget::paintEvent(QPaintEvent *event) {

    QPainter painter(this);
    if(_GPSConn == 0){
        QPixmap image("resources/bmp/no-gps-conn.bmp");
        painter.drawPixmap(QPoint(0,0),image);
    }
    if(_GPSConn == 1){
        if(++showAnimation%2)
        {
            QPixmap image("resources/bmp/no-gps-fix.bmp");
            painter.drawPixmap(QPoint(0,0),image);
        }
    }
    if(_GPSConn == 2){
        QPixmap image("resources/bmp/gps-fix.bmp");
        painter.drawPixmap(QPoint(0,0),image);
    }
}

void GPSWidget::timerTick()
{
    update();
}
