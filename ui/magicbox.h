/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MAGICBOX_H
#define MAGICBOX_H

#include <QWidget>
#include <Qt>
#include "ui/borderbox.h"

class MagicBox : public BorderBox
{
    Q_OBJECT
public:
    explicit MagicBox(QWidget *parent = 0);

    void setAlignment(Qt::Alignment flags);
    void setSize(int size);


    void setCaption(const QString &value);

    void setUnit(const QString &value);

    void setBorder(bool value);

    void setHeight(int value);

    void setWidth(int value);

    void setUnitHeight(int value);

    QString getBaseUnit() const;
    void setBaseUnit(const QString &value);

    void setHideUnit(int value);

signals:

public slots:
    void setValue(const QString &str);

protected:
    virtual void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;

private:
    static const int padding = 3;
    int heightM = 1, widthM = 1;

    QString val;
    int valHeight = 22;
    int unitHeight = 14;
    int hideUnit = 0;

    Qt::Alignment alignment = Qt::AlignHCenter;


    QString caption;
    QString unit;

    QString baseUnit;

    bool border = false;
};

#endif // LABELBOX_H
