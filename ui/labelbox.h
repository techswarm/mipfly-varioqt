/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LABELBOX_H
#define LABELBOX_H

#include <QWidget>
#include <Qt>

#include "borderbox.h"

class LabelBox : public BorderBox
{
    Q_OBJECT
public:
    explicit LabelBox(QWidget *parent = 0);

    void setAlignment(Qt::Alignment flags);
    void setSize(int size);

    void setDrawBorders(bool borders);
    QString getValue() const;

signals:

public slots:
    void setValue(const QString &str);

protected:
    virtual void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;

private:
    QString val;

    int valHeight = 22;

    Qt::Alignment alignment = Qt::AlignHCenter;

    bool drawBorders = false;
};

#endif // LABELBOX_H
