/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "unitbox.h"

#include <QPainter>
#include <Qt>

#include "utils/cstgraphics.h"

using Vario::drawStr;

UnitBox::UnitBox(QWidget *parent) : BorderBox(parent)
{

}

void UnitBox::paintEvent(QPaintEvent *event) {
    BorderBox::paintEvent(event);

    QPainter painter(this);
    painter.setPen(QPen(QColor("black")));

    if(drawBorders){
        painter.drawLine(0, 0, width() - 1, 0);
        painter.drawLine(width() - 1, 0, width() - 1, height() - 1);
        painter.drawLine(0, height() - 1, width() - 1, height() - 1);
        painter.drawLine(0, 0, 0, height() - 1);
    }

    int un_y = (height() - unitHeight) / 2;
    int y = (height() - valHeight) / 2;
    int un_x = 3 * width() / 4 - 2*padding; // unit X

    drawStr(un_x, un_y, unit, unitHeight, painter);
    drawStr(un_x - padding, y, val, valHeight, painter, Qt::AlignRight);
}

void UnitBox::setSize(int size)
{
    valHeight = size;
}

void UnitBox::setUnit(const QString &str) {
    unit = str;
}

void UnitBox::setValue(const QString &str) {
    val = str;
    update();
}

void UnitBox::setDrawBorders(bool borders){
    drawBorders = borders;
}
