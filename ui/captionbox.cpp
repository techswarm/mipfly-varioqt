/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "captionbox.h"
#include "ui_captionbox.h"

#include <QPainter>
#include <Qt>

#include "utils/vario_common.h"
#include "utils/cstgraphics.h"

using Vario::drawStr;
using Vario::drawStrWithCursor;

CaptionBox::CaptionBox(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CaptionBox)
{
    ui->setupUi(this);
}

CaptionBox::~CaptionBox()
{
    delete ui;
}

void CaptionBox::setCaption(const QString &str) {
    //    ui->lblCaption->setText(str);
    caption = str;
}

void CaptionBox::setUnit(const QString &str) {
    //    ui->lblUnit->setText(str);
    unit = str;
}

void CaptionBox::setValue(const QString &str) {
    //    ui->lblVal->setText(str);
    val = str;
    update();
}

void CaptionBox::paintEvent(QPaintEvent *event) {
    QPainter painter(this);
    painter.setPen(QPen(QColor("black")));
    int un_x = 3 * width() / 4 - 2 * padding;
    int y = (height() + 14 - valHeight) / 2;

    if(valueAlign != Qt::AlignRight)
    {
        if(valueAlign == Qt::AlignCenter)
        {
            if(cursorPos>=0)
                drawStrWithCursor(width()/2, y, val, valHeight, painter, Qt::AlignHCenter,cursorPos);
            else
                drawStr(width()/2, y, val, valHeight, painter, Qt::AlignHCenter);
        }
        else
        {
            if(cursorPos>=0)
                drawStrWithCursor(2, y, val, valHeight, painter, Qt::AlignLeft,cursorPos);
            else
                drawStr(2, y, val, valHeight, painter, Qt::AlignLeft);
        }
    }
    else
    {
        if(cursorPos>=0)
            drawStrWithCursor(un_x - padding, y, val, valHeight, painter, Qt::AlignRight,cursorPos);
        else
            drawStr(un_x - padding, y, val, valHeight, painter, Qt::AlignRight);
    }

    drawStr(un_x, y, unit, unitHeight, painter);

    painter.fillRect(0,0, width() - 1, 14, QColor("black"));
    painter.drawRect(0,0, width() - 1, height()-1);
    painter.setPen(QPen(QColor("white")));
    drawStr(width()/2, 0, caption, 14, painter, Qt::AlignHCenter);
    prewCaption = caption;
}

void CaptionBox::setCursorPos(int value)
{
    cursorPos = value;
}

void CaptionBox::setSize(int size)
{
    valHeight = size;
}

void CaptionBox::setValueAlign(const Qt::Alignment &value)
{
    valueAlign = value;
}
