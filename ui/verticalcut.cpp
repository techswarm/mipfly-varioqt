/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "verticalcut.h"
#include "NMEA/NMEAParser.h"
#include "VarioQt.h"
#include "navUtils/elevation.h"
#include "navUtils/crosssectionpoints.h"
#include <QPainter>

VerticalCut::VerticalCut(QWidget *parent) : QWidget(parent)
{

}

void VerticalCut::redraw()
{
    update();
}

void VerticalCut::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    NmeaParser *pGpsParser = VarioQt::instance().getGpsParser();
    int gpsHeight = pGpsParser->getGPSHeight();
    float lat = pGpsParser->getLatitude().dec();
    float lon = pGpsParser->getLongitude().dec();
    float heading = pGpsParser->getCourse();

    CrossSectionPoints p;
    p.computePoints(Coord(lat,lon),heading);

    if(drawBorders){
        painter.drawLine(0, 0, width() - 1, 0);
        painter.drawLine(width() - 1, 0, width() - 1, height() - 1);
        painter.drawLine(0, height() - 1, width() - 1, height() - 1);
        painter.drawLine(0, 0, 0, height() - 1);
    }

    QList<QPoint> graphPoints;

    int hOffset = 0;
    if(gpsHeight>500)
    {
        hOffset = gpsHeight - 500;
    }

    int count = 0;
    float heightToPixels = (float)height()/1000;

    for(int i=0;i<240;i++)
    {
        QPoint graphicPoint(count,height()  -  (((float)(p.points[i] - hOffset))*heightToPixels));
        count++;
        graphPoints.append(graphicPoint);
    }
    for(int i = 1;i<graphPoints.count();i++)
    {
        painter.drawLine(graphPoints[i-1],graphPoints[i]);
    }
}

void VerticalCut::setDrawBorders(bool value)
{
    drawBorders = value;
}
