/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SATELLITESTATUSPAGE_H
#define SATELLITESTATUSPAGE_H

#include <QWidget>
#include "utils/vario_common.h"
#include "utils/cstgraphics.h"
#include "navUtils/coord.hpp"

using namespace Vario;

class SatelliteStatusPage : public QWidget
{
    Q_OBJECT
public:
    explicit SatelliteStatusPage(QWidget *parent = 0);
    void updateStatus(Coord position, int alt, int fix, QString strTime, int numSatellitesUsed, double vDop);
protected:
    virtual void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;
private:
    double vDop_ = 99.0;
    FixType fix_ = FIX_NONE;
    QMetaObject::Connection gpsGPGSVMsgCon;
    QPixmap imageEarth;
    QPixmap imageSat;
    int iconAngle = 0;
    QString strLat = "-";
    QString strLon = "-";
    QString strAlt = "-";
    QString strFix = "-";
    QString strTime = "--.--.--";
    QString strSatsUsed = "-";
    QString strVDop = "-";
    int earthIdx = 0;

};

#endif // SATELLITESTATUSPAGE_H
