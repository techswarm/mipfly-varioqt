/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "slider.h"

#include <QPainter>
#include <QtMath>

Slider::Slider(QWidget *parent) : QWidget(parent)
{

}

void Slider::paintEvent(QPaintEvent *event) {
    QPainter painter(this);

    int w = width() * qAbs<int>(min - val) / (max - min);

    painter.drawRect(0,0, width()-1, height()-1);
    painter.fillRect(0,0,w, height()-1, QColor(Qt::black));
}

void Slider::setMinimum(int minimum) {
    min = minimum;
}

void Slider::setMaximum(int maximum) {
    max = maximum;
}

void Slider::setValue(int value) {
    val = value;
    update();
}
