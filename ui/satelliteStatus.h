/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SATELLITESTATUS_H
#define SATELLITESTATUS_H

#include <QWidget>
#include "utils/vario_common.h"
#include "utils/cstgraphics.h"

using namespace Vario;

enum SatStatusDispType {
    SAT_STATUS_DISP_BARS,
    SAT_STATUS_DISP_EXTENDED
};

class SatelliteStatus : public QWidget
{
    Q_OBJECT
public:
    explicit SatelliteStatus(QWidget *parent = 0);
    void updateVDop(double vDop, int fix);
    void setSatStatusDispType(SatStatusDispType satStatusDispType);
    SatStatusDispType getDispType();
protected:
    virtual void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;
private:
    double vDop_ = 99.0;
    FixType fix_ = FIX_NONE;
    SatStatusDispType satStatusDispType_ = SAT_STATUS_DISP_BARS;
    void paintStatusBars(QPainter *painter);
    void paintExtendedStatusDisplay(QPainter *painter);
    QMetaObject::Connection gpsGPGSVMsgCon;

};

#endif // SATELLITESTATUS_H
