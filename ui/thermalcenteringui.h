/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef THERMALCENTERINGUI_H
#define THERMALCENTERINGUI_H

#include "navUtils/coord.hpp"
#include <QWidget>


class thermalCenteringUI : public QWidget
{
    Q_OBJECT
private:
    int _vspeedCMPS;
    int _vspeedCMPSLoc[600];
    Coord _loc[600];
    int _numSamples;
    int _lastPosIndex;
    double _heading;
    bool dispEnable = 0;
    double metersToPixels;
    int pathMarkerSize;


    void paintEvent(QPaintEvent *event)override;


public:
    thermalCenteringUI( QWidget *parent = 0, const int numSamples = 5*60);
    virtual ~thermalCenteringUI();

public slots:
    void pushCMPS(const int cmps);
    void pushPoint(const Coord location);
    void pushHeading(const double headingDgr);
};

#endif // THERMALCENTERINGUI_H
