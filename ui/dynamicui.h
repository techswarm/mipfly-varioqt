/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DYNAMICUI_H
#define DYNAMICUI_H

#include "ui_mainui.h"
#include <QObject>

class DynamicUI : public QObject
{
    Q_OBJECT
private:
    Ui::MainUI* _pMainUI;
    int _version;
    int _pages;
    int _actualPage = 1;
    void scanHead();
    void removeFocusFromElements();
    void focusElement();
    void pushShortPress(int btn);

    bool functionSet = false;

    int pageBeforeTermal;
    bool returnToPageBeforeTermal = false;
    QString oldUi = "";

public:
    enum class SpecialPages{GpsStatusPage = 100,TaskStatusPage};
    DynamicUI(Ui::MainUI* _pMainUI , QObject *parent = nullptr);
    void hideElements();
    void keyUp();
    void keyDown();
    void keyF();
    void keyEnter();

    void showPage(int pageNumber);
    void showPage(SpecialPages page);
    void showElement(QString type, int x, int y, int width, int height, int zoom, QString title, bool border, int size, QString unit, int hideUnit);
    int actualPage() const;
    void setActualPage(int actualPage);

public slots:
    void activateTermal();
    void deactivateTermal();

signals:
    void showNavMenu();
    void showFAIMenu();
};

#endif // DYNAMICUI_H
