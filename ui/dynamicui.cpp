/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "dynamicui.h"
#include "ui_mainui.h"
#include "QFile"
#include "QMessageBox"
#include "QXmlStreamReader"
#include "QDebug"
#include "utils/vario_common.h"

DynamicUI::DynamicUI(Ui::MainUI *pMainUI, QObject *parent) : QObject(parent)
{
    this->_pMainUI = pMainUI;
}

void DynamicUI::hideElements()
{
    _pMainUI->battery->hide();
    _pMainUI->GPSBox->hide();
    _pMainUI->RecBox->hide();
    _pMainUI->BNEPBox->hide();
    _pMainUI->BoxWeather->hide();
    _pMainUI->boxAltitude->hide();
    _pMainUI->boxCrtTime->hide();
    _pMainUI->lblDistStart->hide();
    _pMainUI->boxElapsed->hide();
    _pMainUI->lblFix->hide();
    _pMainUI->cboxGlide->hide();
    _pMainUI->boxGndSpeed->hide();
    _pMainUI->cboxHDiff->hide();
    _pMainUI->lblPilot->hide();
    _pMainUI->cboxTemp->hide();
    _pMainUI->thermalPlot->hide();
    _pMainUI->compass->hide();
    _pMainUI->vario->hide();
    _pMainUI->boxAvgClimb->hide();
    _pMainUI->lblDistThermal->hide();
    _pMainUI->airSpacePlot->hide();
    _pMainUI->verticalPlotUI->hide();
    _pMainUI->mapPlot->hide();
    _pMainUI->cboxAGL->hide();
    _pMainUI->verticalCut->hide();
    _pMainUI->cutPlot->hide();
    _pMainUI->taskPlot->hide();
    _pMainUI->distWp->hide();
    _pMainUI->distWpCenter->hide();
    _pMainUI->glideWp->hide();
    _pMainUI->heightWp->hide();
    _pMainUI->distGoal->hide();
    _pMainUI->glideGoal->hide();
    _pMainUI->heightGoal->hide();
    _pMainUI->timeGate->hide();
    _pMainUI->elapsedTime->hide();
    _pMainUI->distFlown->hide();
    _pMainUI->avgSpeed->hide();
    _pMainUI->avgClimb->hide();
    _pMainUI->avgSink->hide();
    _pMainUI->trianglePlot->hide();
    _pMainUI->boxElapsedShort->hide();
    _pMainUI->boxCrtTimeShort->hide();
    _pMainUI->boxActiveWaypoint->hide();
    _pMainUI->boxPressAltAsl->hide();
    _pMainUI->boxPressAltRef1->hide();
    _pMainUI->boxPressAltRef2->hide();
    _pMainUI->tas->hide();
    _pMainUI->headwind->hide();
    _pMainUI->satelliteStatus->hide();
    _pMainUI->satelliteStatusPage->hide();
    _pMainUI->tpRadius->hide();
    _pMainUI->speedSSS->hide();
    _pMainUI->boxTrackLength->hide();
    _pMainUI->dist5Tp->hide();
    _pMainUI->dist5TpP->hide();
}

int DynamicUI::actualPage() const
{
    return _actualPage;
}

void DynamicUI::setActualPage(int actualPage) {
    _actualPage = actualPage;
}

void DynamicUI::activateTermal()
{
    if(!functionSet)
    {
        pageBeforeTermal = _actualPage;
        returnToPageBeforeTermal = true;
        _actualPage = 2;
        showPage(2);
    }
}

void DynamicUI::deactivateTermal()
{
    if(returnToPageBeforeTermal && !functionSet)
    {
        _actualPage = pageBeforeTermal;
        showPage(pageBeforeTermal);
    }
}

void DynamicUI::scanHead()
{
    QSettings settings;
    QString savedUi = settings.value("uiName","default.xml").toString();

    QFile* xmlFile = new QFile("ui/"+savedUi);
    if (!xmlFile->open(QIODevice::ReadOnly | QIODevice::Text)) {
        return;
    }
    QXmlStreamReader* xmlReader = new QXmlStreamReader(xmlFile);

    //Parse the XML until we reach end of it
    while(!xmlReader->atEnd() && !xmlReader->hasError()) {
        // Read next element
        QXmlStreamReader::TokenType token = xmlReader->readNext();
        //If token is just StartDocument - go to next
        if(token == QXmlStreamReader::StartDocument) {
            continue;
        }
        //If token is StartElement - read it
        if(token == QXmlStreamReader::StartElement) {

            if(xmlReader->name() == "head") {
                continue;
            }

            if(xmlReader->name() == "version") {
                _version = xmlReader->readElementText().toInt();
                continue;
            }

            if(xmlReader->name() == "pages") {
                _pages = xmlReader->readElementText().toInt();
                continue;
            }
        }
    }

    //close reader and flush file
    xmlReader->clear();
    xmlFile->close();
}

void DynamicUI::removeFocusFromElements()
{
    //here you should remove focus from all focusable elements
    _pMainUI->airSpacePlot->unfocus();
    _pMainUI->mapPlot->unfocus();

    functionSet = false;
}

void DynamicUI::focusElement()
{
    //find a visible element to focus
    if(_pMainUI->airSpacePlot->isVisible())
    {
        _pMainUI->airSpacePlot->focus();
        functionSet = true;
    }
    if(_pMainUI->mapPlot->isVisible())
    {
        _pMainUI->mapPlot->focus();
        functionSet = true;
    }
    if(_pMainUI->taskPlot->isVisible())
    {
        emit showNavMenu();
    }
    if(_pMainUI->trianglePlot->isVisible())
    {
        emit showFAIMenu();
    }
}

void DynamicUI::pushShortPress(int btn)
{
    if(_pMainUI->airSpacePlot->isVisible())
    {
        _pMainUI->airSpacePlot->shortPress(btn);
    }
    if(_pMainUI->mapPlot->isVisible())
    {
        _pMainUI->mapPlot->shortPress(btn);
    }
}

void DynamicUI::keyUp()
{

    /*Predefined pages, not included in ui.xml file, activated from menu, id > 99*/
    if (_actualPage >= 100) return;

    if(!functionSet)
    {
        scanHead();
        _actualPage++;
        if(_actualPage>_pages)
            _actualPage = 1;

        returnToPageBeforeTermal = false;
        showPage(_actualPage);
    }
    else
    {
        pushShortPress(Vario::GPIO1);
    }
}

void DynamicUI::keyDown()
{
    /*Predefined pages, not included in ui.xml file, activated from menu, id > 99*/
    if (_actualPage >= 100) return;

    if(!functionSet)
    {
        scanHead();
        _actualPage--;
        if(_actualPage<=0)
            _actualPage = _pages;

        returnToPageBeforeTermal = false;
        showPage(_actualPage);
    }
    else
    {
        pushShortPress(Vario::GPIO2);
    }
}

void DynamicUI::keyEnter()
{
    showPage(_actualPage);
}

void DynamicUI::keyF()
{
    if(functionSet)
    {
        functionSet = false;
        removeFocusFromElements();
    }
    else
    {
        //try to focus element
        focusElement();
    }
}

void DynamicUI::showPage(int pageNumber)
{
    hideElements();
    QSettings settings;
    QString savedUi = settings.value("uiName","default.xml").toString();

    if(savedUi != oldUi)
    {
        oldUi = savedUi;
        scanHead();
    }

    QFile* xmlFile = new QFile("ui/"+savedUi);
    if (!xmlFile->open(QIODevice::ReadOnly | QIODevice::Text)) {
        return;
    }
    QXmlStreamReader* xmlReader = new QXmlStreamReader(xmlFile);

    int qmlPageIndex = 0;
    //Parse the XML until we reach end of it
    while(!xmlReader->atEnd() && !xmlReader->hasError()) {
        // Read next element
        QXmlStreamReader::TokenType token = xmlReader->readNext();
        //qDebug()<<xmlReader->name();
        //If token is just StartDocument - go to next
        if(token == QXmlStreamReader::StartDocument) {
            continue;
        }
        //If token is StartElement - read it
        if(token == QXmlStreamReader::StartElement) {

            if(xmlReader->name() == "page") {
                qmlPageIndex++;
                if(qmlPageIndex == pageNumber)
                {
                    while(!xmlReader->atEnd() && !xmlReader->hasError()) {
                        // Read next element
                        token = xmlReader->readNext();
                        if(token == QXmlStreamReader::EndElement)
                        {
                            if(xmlReader->name() == "page")
                            {
                                //page is done. return
                                xmlReader->clear();
                                xmlFile->close();
                            }
                        }
                        if(token == QXmlStreamReader::StartElement)
                        {
                            if(xmlReader->name() == "ui")
                            {
                                //read attributes
                                QXmlStreamAttributes attribs = xmlReader->attributes();
                                showElement(attribs.value("type").toString(),
                                            attribs.value("x").toInt(),
                                            attribs.value("y").toInt(),
                                            attribs.value("width").toInt(),
                                            attribs.value("height").toInt(),
                                            attribs.value("zoom").toInt(),
                                            attribs.value("title").toString(),
                                            attribs.value("border").toInt(),
                                            attribs.value("size").toInt(),
                                            attribs.value("unit").toString(),
                                            attribs.value("hideUnit").toInt()
                                            );
                            }
                        }
                    }
                }
                else continue;
            }
        }
    }

    //close reader and flush file
    xmlReader->clear();
    xmlFile->close();
}

void DynamicUI::showPage(DynamicUI::SpecialPages page)
{
    /* Predefined pages are not defined in xml file */
    hideElements();
    switch (page) {
    case SpecialPages::GpsStatusPage:
        showElement("SatelliteStatusPage",0, 0, 240, 320, 0, "", false, 0, "", 0);
        break;
    case SpecialPages::TaskStatusPage:
        showElement("TaskStatusPage",0, 0, 240, 320, 0, "", false, 0, "", 0);
        break;
    default:
        break;
    }
}

void DynamicUI::showElement(QString type, int x, int y, int width, int height, int zoom, QString title, bool border, int size, QString unit, int hideUnit)
{
    //limmit the minimum size because of ui builder bug
    if(size<14)size=14;
    if(type == "BoxWeather")
    {
        _pMainUI->BoxWeather->move(x,y);
        _pMainUI->BoxWeather->setWidth(width);
        _pMainUI->BoxWeather->setHeight(height);
        _pMainUI->BoxWeather->setBorder(border);
        _pMainUI->BoxWeather->setCaption(title);
        _pMainUI->BoxWeather->setSize(size);
        _pMainUI->BoxWeather->show();
    }
    if(type == "Vario")
    {
        _pMainUI->vario->move(x,y);
        _pMainUI->vario->resize(width,height);
        _pMainUI->vario->setUnit(unit);
        _pMainUI->vario->show();
    }
    if(type == "Compass")
    {
        _pMainUI->compass->move(x,y);
        _pMainUI->compass->resize(width,height);
        _pMainUI->compass->setUnit(unit);
        _pMainUI->compass->show();
    }
    if(type == "Battery")
    {
        _pMainUI->battery->move(x,y);
        _pMainUI->battery->show();
    }
    if(type == "GPSBox")
    {
        _pMainUI->GPSBox->move(x,y);
        _pMainUI->GPSBox->show();
    }
    if(type == "BNEPBox")
    {
        _pMainUI->BNEPBox->move(x,y);
        _pMainUI->BNEPBox->show();
    }
    if(type == "RecBox")
    {
        _pMainUI->RecBox->move(x,y);
        _pMainUI->RecBox->show();
    }
    if(type == "ThermalPlot")
    {
        _pMainUI->thermalPlot ->move(x,y);
        _pMainUI->thermalPlot->resize(width,height);
        _pMainUI->thermalPlot->show();
    }
    if(type == "LblDistStart")
    {
        _pMainUI->lblDistStart->move(x,y);
        _pMainUI->lblDistStart->setWidth(width);
        _pMainUI->lblDistStart->setHeight(height);
        _pMainUI->lblDistStart->setBorder(border);
        _pMainUI->lblDistStart->setCaption(title);
        _pMainUI->lblDistStart->setSize(size);
        _pMainUI->lblDistStart->setUnit(unit);
        _pMainUI->lblDistStart->setBaseUnit(unit);
        _pMainUI->lblDistStart->show();
        _pMainUI->lblDistStart->setHideUnit(hideUnit);
    }
    if(type == "boxTrackLength")
    {
        _pMainUI->boxTrackLength->move(x,y);
        _pMainUI->boxTrackLength->setWidth(width);
        _pMainUI->boxTrackLength->setHeight(height);
        _pMainUI->boxTrackLength->setBorder(border);
        _pMainUI->boxTrackLength->setCaption(title);
        _pMainUI->boxTrackLength->setSize(size);
        _pMainUI->boxTrackLength->setUnit(unit);
        _pMainUI->boxTrackLength->setBaseUnit(unit);
        _pMainUI->boxTrackLength->show();
        _pMainUI->boxTrackLength->setHideUnit(hideUnit);
    }
    if(type == "tpRadius")
    {
        _pMainUI->tpRadius->move(x,y);
        _pMainUI->tpRadius->setWidth(width);
        _pMainUI->tpRadius->setHeight(height);
        _pMainUI->tpRadius->setBorder(border);
        _pMainUI->tpRadius->setCaption(title);
        _pMainUI->tpRadius->setSize(size);
        _pMainUI->tpRadius->setUnit(unit);
        _pMainUI->tpRadius->setBaseUnit(unit);
        _pMainUI->tpRadius->show();
        _pMainUI->tpRadius->setHideUnit(hideUnit);
    }
    if(type == "LblDistThermal")
    {
        _pMainUI->lblDistThermal->move(x,y);
        _pMainUI->lblDistThermal->setWidth(width);
        _pMainUI->lblDistThermal->setHeight(height);
        _pMainUI->lblDistThermal->setBorder(border);
        _pMainUI->lblDistThermal->setCaption(title);
        _pMainUI->lblDistThermal->setSize(size);
        _pMainUI->lblDistThermal->setUnit(unit);
        _pMainUI->lblDistThermal->setBaseUnit(unit);
        _pMainUI->lblDistThermal->show();
        _pMainUI->lblDistThermal->setHideUnit(hideUnit);
    }
    if(type == "LblFix")
    {
        _pMainUI->lblFix->move(x,y);
        _pMainUI->lblFix->setWidth(width);
        _pMainUI->lblFix->setHeight(height);
        _pMainUI->lblFix->setBorder(border);
        _pMainUI->lblFix->setCaption(title);
        _pMainUI->lblFix->setSize(size);
        _pMainUI->lblFix->show();
    }
    if(type == "LblPilot")
    {
        _pMainUI->lblPilot->move(x,y);
        _pMainUI->lblPilot->setWidth(width);
        _pMainUI->lblPilot->setHeight(height);
        _pMainUI->lblPilot->setBorder(border);
        _pMainUI->lblPilot->setCaption(title);
        _pMainUI->lblPilot->setSize(size);
        _pMainUI->lblPilot->show();
    }

    if(type == "CBoxTemp")
    {
        _pMainUI->cboxTemp->move(x,y);
        _pMainUI->cboxTemp->setWidth(width);
        _pMainUI->cboxTemp->setHeight(height);
        _pMainUI->cboxTemp->setBorder(border);
        _pMainUI->cboxTemp->setCaption(title);
        _pMainUI->cboxTemp->setSize(size);
        _pMainUI->cboxTemp->setUnit(unit);
        _pMainUI->cboxTemp->show();
    }
    if(type == "CBoxHDiff")
    {
        _pMainUI->cboxHDiff->move(x,y);
        _pMainUI->cboxHDiff->setWidth(width);
        _pMainUI->cboxHDiff->setHeight(height);
        _pMainUI->cboxHDiff->setBorder(border);
        _pMainUI->cboxHDiff->setCaption(title);
        _pMainUI->cboxHDiff->setSize(size);
        _pMainUI->cboxHDiff->setUnit(unit);
        _pMainUI->cboxHDiff->setBaseUnit(unit);
        _pMainUI->cboxHDiff->show();
        _pMainUI->cboxHDiff->setHideUnit(hideUnit);
    }
    if(type == "CBoxGlide")
    {
        _pMainUI->cboxGlide->move(x,y);
        _pMainUI->cboxGlide->setWidth(width);
        _pMainUI->cboxGlide->setHeight(height);
        _pMainUI->cboxGlide->setBorder(border);
        _pMainUI->cboxGlide->setCaption(title);
        _pMainUI->cboxGlide->setSize(size);
        _pMainUI->cboxGlide->show();
    }

    if(type == "CBoxAGL")
    {
        _pMainUI->cboxAGL->move(x,y);
        _pMainUI->cboxAGL->setWidth(width);
        _pMainUI->cboxAGL->setHeight(height);
        _pMainUI->cboxAGL->setBorder(border);
        _pMainUI->cboxAGL->setCaption(title);
        _pMainUI->cboxAGL->setSize(size);
        _pMainUI->cboxAGL->setUnit(unit);
        _pMainUI->cboxAGL->setBaseUnit(unit);
        _pMainUI->cboxAGL->show();
        _pMainUI->cboxAGL->setHideUnit(hideUnit);
    }

    if(type == "BoxGndSpeed")
    {
        _pMainUI->boxGndSpeed->move(x,y);
        _pMainUI->boxGndSpeed->setWidth(width);
        _pMainUI->boxGndSpeed->setHeight(height);
        _pMainUI->boxGndSpeed->setBorder(border);
        _pMainUI->boxGndSpeed->setCaption(title);
        _pMainUI->boxGndSpeed->setSize(size);
        _pMainUI->boxGndSpeed->setUnit(unit);
        _pMainUI->boxGndSpeed->setBaseUnit(unit);
        _pMainUI->boxGndSpeed->show();
        _pMainUI->boxGndSpeed->setHideUnit(hideUnit);
    }

    if(type == "tas")
    {
        _pMainUI->tas->move(x,y);
        _pMainUI->tas->setWidth(width);
        _pMainUI->tas->setHeight(height);
        _pMainUI->tas->setBorder(border);
        _pMainUI->tas->setCaption(title);
        _pMainUI->tas->setSize(size);
        _pMainUI->tas->setUnit(unit);
        _pMainUI->tas->setBaseUnit(unit);
        _pMainUI->tas->show();
        _pMainUI->tas->setHideUnit(hideUnit);
    }

    if(type == "headwind")
    {
        _pMainUI->headwind->move(x,y);
        _pMainUI->headwind->setWidth(width);
        _pMainUI->headwind->setHeight(height);
        _pMainUI->headwind->setBorder(border);
        _pMainUI->headwind->setCaption(title);
        _pMainUI->headwind->setSize(size);
        _pMainUI->headwind->setUnit(unit);
        _pMainUI->headwind->setBaseUnit(unit);
        _pMainUI->headwind->show();
        _pMainUI->headwind->setHideUnit(hideUnit);
    }

    if(type == "speedSSS")
    {
        _pMainUI->speedSSS->move(x,y);
        _pMainUI->speedSSS->setWidth(width);
        _pMainUI->speedSSS->setHeight(height);
        _pMainUI->speedSSS->setBorder(border);
        _pMainUI->speedSSS->setCaption(title);
        _pMainUI->speedSSS->setSize(size);
        _pMainUI->speedSSS->setUnit(unit);
        _pMainUI->speedSSS->setBaseUnit(unit);
        _pMainUI->speedSSS->show();
        _pMainUI->speedSSS->setHideUnit(hideUnit);
    }
    if(type == "BoxAltitude")
    {
        _pMainUI->boxAltitude->move(x,y);
        _pMainUI->boxAltitude->setWidth(width);
        _pMainUI->boxAltitude->setHeight(height);
        _pMainUI->boxAltitude->setBorder(border);
        _pMainUI->boxAltitude->setCaption(title);
        _pMainUI->boxAltitude->setSize(size);
        _pMainUI->boxAltitude->setUnit(unit);
        _pMainUI->boxAltitude->setBaseUnit(unit);
        _pMainUI->boxAltitude->show();
        _pMainUI->boxAltitude->setHideUnit(hideUnit);
    }
    if(type == "BoxPressAltAsl")
    {
        _pMainUI->boxPressAltAsl->move(x,y);
        _pMainUI->boxPressAltAsl->setWidth(width);
        _pMainUI->boxPressAltAsl->setHeight(height);
        _pMainUI->boxPressAltAsl->setBorder(border);
        _pMainUI->boxPressAltAsl->setCaption(title);
        _pMainUI->boxPressAltAsl->setSize(size);
        _pMainUI->boxPressAltAsl->setUnit(unit);
        _pMainUI->boxPressAltAsl->setBaseUnit(unit);
        _pMainUI->boxPressAltAsl->show();
        _pMainUI->boxPressAltAsl->setHideUnit(hideUnit);
    }
    if(type == "BoxPressAltRef1")
    {
        _pMainUI->boxPressAltRef1->move(x,y);
        _pMainUI->boxPressAltRef1->setWidth(width);
        _pMainUI->boxPressAltRef1->setHeight(height);
        _pMainUI->boxPressAltRef1->setBorder(border);
        _pMainUI->boxPressAltRef1->setCaption(title);
        _pMainUI->boxPressAltRef1->setSize(size);
        _pMainUI->boxPressAltRef1->setUnit(unit);
        _pMainUI->boxPressAltRef1->setBaseUnit(unit);
        _pMainUI->boxPressAltRef1->show();
        _pMainUI->boxPressAltRef1->setHideUnit(hideUnit);
    }
    if(type == "BoxPressAltRef2")
    {
        _pMainUI->boxPressAltRef2->move(x,y);
        _pMainUI->boxPressAltRef2->setWidth(width);
        _pMainUI->boxPressAltRef2->setHeight(height);
        _pMainUI->boxPressAltRef2->setBorder(border);
        _pMainUI->boxPressAltRef2->setCaption(title);
        _pMainUI->boxPressAltRef2->setSize(size);
        _pMainUI->boxPressAltRef2->setUnit(unit);
        _pMainUI->boxPressAltRef2->setBaseUnit(unit);
        _pMainUI->boxPressAltRef2->show();
        _pMainUI->boxPressAltRef2->setHideUnit(hideUnit);
    }
    if(type == "BoxCrtTime")
    {
        _pMainUI->boxCrtTime->move(x,y);
        _pMainUI->boxCrtTime->setWidth(width);
        _pMainUI->boxCrtTime->setHeight(height);
        _pMainUI->boxCrtTime->setBorder(border);
        _pMainUI->boxCrtTime->setCaption(title);
        _pMainUI->boxCrtTime->setSize(size);
        _pMainUI->boxCrtTime->show();
    }
    if(type == "BoxCrtTimeShort")
    {
        _pMainUI->boxCrtTimeShort->move(x,y);
        _pMainUI->boxCrtTimeShort->setWidth(width);
        _pMainUI->boxCrtTimeShort->setHeight(height);
        _pMainUI->boxCrtTimeShort->setBorder(border);
        _pMainUI->boxCrtTimeShort->setCaption(title);
        _pMainUI->boxCrtTimeShort->setSize(size);
        _pMainUI->boxCrtTimeShort->show();
    }
    if(type == "BoxElapsed")
    {
        _pMainUI->boxElapsed->move(x,y);
        _pMainUI->boxElapsed->setWidth(width);
        _pMainUI->boxElapsed->setHeight(height);
        _pMainUI->boxElapsed->setBorder(border);
        _pMainUI->boxElapsed->setCaption(title);
        _pMainUI->boxElapsed->setSize(size);
        _pMainUI->boxElapsed->show();
    }
    if(type == "BoxElapsedShort")
    {
        _pMainUI->boxElapsedShort->move(x,y);
        _pMainUI->boxElapsedShort->setWidth(width);
        _pMainUI->boxElapsedShort->setHeight(height);
        _pMainUI->boxElapsedShort->setBorder(border);
        _pMainUI->boxElapsedShort->setCaption(title);
        _pMainUI->boxElapsedShort->setSize(size);
        _pMainUI->boxElapsedShort->show();
    }
    if (type == "BoxActiveWaypoint") {
        _pMainUI->boxActiveWaypoint->move(x,y);
        _pMainUI->boxActiveWaypoint->setWidth(width);
        _pMainUI->boxActiveWaypoint->setHeight(height);
        _pMainUI->boxActiveWaypoint->setBorder(border);
        _pMainUI->boxActiveWaypoint->setCaption(title);
        _pMainUI->boxActiveWaypoint->setSize(size);
        _pMainUI->boxActiveWaypoint->show();
    }
    if(type == "BoxAvgClimb")
    {
        _pMainUI->boxAvgClimb->move(x,y);
        _pMainUI->boxAvgClimb->setWidth(width);
        _pMainUI->boxAvgClimb->setHeight(height);
        _pMainUI->boxAvgClimb->setBorder(border);
        _pMainUI->boxAvgClimb->setCaption(title);
        _pMainUI->boxAvgClimb->setSize(size);
        _pMainUI->boxAvgClimb->setUnit(unit);
        _pMainUI->boxAvgClimb->setBaseUnit(unit);
        _pMainUI->boxAvgClimb->show();
        _pMainUI->boxAvgClimb->setHideUnit(hideUnit);
    }
    if(type == "avgClimb")
    {
        _pMainUI->avgClimb->move(x,y);
        _pMainUI->avgClimb->setWidth(width);
        _pMainUI->avgClimb->setHeight(height);
        _pMainUI->avgClimb->setBorder(border);
        _pMainUI->avgClimb->setCaption(title);
        _pMainUI->avgClimb->setSize(size);
        _pMainUI->avgClimb->setUnit(unit);
        _pMainUI->avgClimb->setBaseUnit(unit);
        _pMainUI->avgClimb->show();
        _pMainUI->avgClimb->setHideUnit(hideUnit);
    }
    if(type == "AvgSink")
    {
        _pMainUI->avgSink->move(x,y);
        _pMainUI->avgSink->setWidth(width);
        _pMainUI->avgSink->setHeight(height);
        _pMainUI->avgSink->setBorder(border);
        _pMainUI->avgSink->setCaption(title);
        _pMainUI->avgSink->setSize(size);
        _pMainUI->avgSink->setUnit(unit);
        _pMainUI->avgSink->setBaseUnit(unit);
        _pMainUI->avgSink->show();
        _pMainUI->avgSink->setHideUnit(hideUnit);
    }
    if(type == "AirSpacePlot")
    {
        _pMainUI->airSpacePlot->move(x,y);
        _pMainUI->airSpacePlot->show();
        _pMainUI->airSpacePlot->resize(width,height);
        _pMainUI->airSpacePlot->setUnit(unit);
    }
    if(type == "TaskPlot")
    {
        _pMainUI->taskPlot->move(x,y);
        _pMainUI->taskPlot->show();
        _pMainUI->taskPlot->resize(width,height);
        _pMainUI->taskPlot->setUnit(unit);
    }
    if(type == "TaskStatusPage")
    {
        _pMainUI->taskPlot->move(x,y);
        _pMainUI->taskPlot->show();
        _pMainUI->taskPlot->resize(width,height);
        _pMainUI->taskPlot->setUnit(unit);
        _pMainUI->taskPlot->setShowTaskData(true);
    }
    if(type == "TrianglePlot")
    {
        _pMainUI->trianglePlot->move(x,y);
        _pMainUI->trianglePlot->show();
        _pMainUI->trianglePlot->resize(width,height);
    }
    if(type == "VerticalPlotUI")
    {
        _pMainUI->verticalPlotUI->move(x,y);
        _pMainUI->verticalPlotUI->show();
        _pMainUI->verticalPlotUI->resize(width,height);
        _pMainUI->verticalPlotUI->setDrawBorders(border);
        _pMainUI->verticalPlotUI->setUnit(unit);
    }
    if(type == "VerticalCut")
    {
        _pMainUI->verticalCut->move(x,y);
        _pMainUI->verticalCut->show();
        _pMainUI->verticalCut->resize(width,height);
        _pMainUI->verticalCut->setDrawBorders(border);
    }
    if(type == "MapPlot")
    {
        _pMainUI->mapPlot->move(x,y);
        _pMainUI->mapPlot->show();
        _pMainUI->mapPlot->resize(width,height);
        _pMainUI->mapPlot->setZoomLevel(zoom);
    }
    if(type == "CutPlot")
    {
        _pMainUI->cutPlot->move(x,y);
        _pMainUI->cutPlot->show();
        _pMainUI->cutPlot->resize(width,height);
    }
    if(type == "distWp")
    {
        _pMainUI->distWp->move(x,y);
        _pMainUI->distWp->setWidth(width);
        _pMainUI->distWp->setHeight(height);
        _pMainUI->distWp->setBorder(border);
        _pMainUI->distWp->setCaption(title);
        _pMainUI->distWp->setSize(size);
        _pMainUI->distWp->setUnit(unit);
        _pMainUI->distWp->setBaseUnit(unit);
        _pMainUI->distWp->show();
        _pMainUI->distWp->setHideUnit(hideUnit);
    }
    if(type == "dist5Tp")
    {
        _pMainUI->dist5Tp->move(x,y);
        _pMainUI->dist5Tp->setWidth(width);
        _pMainUI->dist5Tp->setHeight(height);
        _pMainUI->dist5Tp->setBorder(border);
        _pMainUI->dist5Tp->setCaption(title);
        _pMainUI->dist5Tp->setSize(size);
        _pMainUI->dist5Tp->setUnit(unit);
        _pMainUI->dist5Tp->setBaseUnit(unit);
        _pMainUI->dist5Tp->show();
        _pMainUI->dist5Tp->setHideUnit(hideUnit);
    }
    if(type == "dist5TpP")
    {
        _pMainUI->dist5TpP->move(x,y);
        _pMainUI->dist5TpP->setWidth(width);
        _pMainUI->dist5TpP->setHeight(height);
        _pMainUI->dist5TpP->setBorder(border);
        _pMainUI->dist5TpP->setCaption(title);
        _pMainUI->dist5TpP->setSize(size);
        _pMainUI->dist5TpP->setUnit(unit);
        _pMainUI->dist5TpP->setBaseUnit(unit);
        _pMainUI->dist5TpP->show();
        _pMainUI->dist5TpP->setHideUnit(hideUnit);
    }
    if(type == "distWpCenter")
    {
        _pMainUI->distWpCenter->move(x,y);
        _pMainUI->distWpCenter->setWidth(width);
        _pMainUI->distWpCenter->setHeight(height);
        _pMainUI->distWpCenter->setBorder(border);
        _pMainUI->distWpCenter->setCaption(title);
        _pMainUI->distWpCenter->setSize(size);
        _pMainUI->distWpCenter->setUnit(unit);
        _pMainUI->distWpCenter->setBaseUnit(unit);
        _pMainUI->distWpCenter->show();
        _pMainUI->distWpCenter->setHideUnit(hideUnit);
    }
    if(type == "glideWp")
    {
        _pMainUI->glideWp->move(x,y);
        _pMainUI->glideWp->setWidth(width);
        _pMainUI->glideWp->setHeight(height);
        _pMainUI->glideWp->setBorder(border);
        _pMainUI->glideWp->setCaption(title);
        _pMainUI->glideWp->setSize(size);
        _pMainUI->glideWp->setUnit(unit);
        _pMainUI->glideWp->show();
    }
    if(type == "heightWp")
    {
        _pMainUI->heightWp->move(x,y);
        _pMainUI->heightWp->setWidth(width);
        _pMainUI->heightWp->setHeight(height);
        _pMainUI->heightWp->setBorder(border);
        _pMainUI->heightWp->setCaption(title);
        _pMainUI->heightWp->setSize(size);
        _pMainUI->heightWp->setUnit(unit);
        _pMainUI->heightWp->setBaseUnit(unit);
        _pMainUI->heightWp->show();
        _pMainUI->heightWp->setHideUnit(hideUnit);
    }
    if(type == "distGoal")
    {
        _pMainUI->distGoal->move(x,y);
        _pMainUI->distGoal->setWidth(width);
        _pMainUI->distGoal->setHeight(height);
        _pMainUI->distGoal->setBorder(border);
        _pMainUI->distGoal->setCaption(title);
        _pMainUI->distGoal->setSize(size);
        _pMainUI->distGoal->setUnit(unit);
        _pMainUI->distGoal->setBaseUnit(unit);
        _pMainUI->distGoal->show();
        _pMainUI->distGoal->setHideUnit(hideUnit);
    }
    if(type == "glideGoal")
    {
        _pMainUI->glideGoal->move(x,y);
        _pMainUI->glideGoal->setWidth(width);
        _pMainUI->glideGoal->setHeight(height);
        _pMainUI->glideGoal->setBorder(border);
        _pMainUI->glideGoal->setCaption(title);
        _pMainUI->glideGoal->setSize(size);
        _pMainUI->glideGoal->setUnit(unit);
        _pMainUI->glideGoal->show();
    }
    if(type == "heightGoal")
    {
        _pMainUI->heightGoal->move(x,y);
        _pMainUI->heightGoal->setWidth(width);
        _pMainUI->heightGoal->setHeight(height);
        _pMainUI->heightGoal->setBorder(border);
        _pMainUI->heightGoal->setCaption(title);
        _pMainUI->heightGoal->setSize(size);
        _pMainUI->heightGoal->setUnit(unit);
        _pMainUI->heightGoal->setBaseUnit(unit);
        _pMainUI->heightGoal->show();
        _pMainUI->heightGoal->setHideUnit(hideUnit);
    }
    if(type == "timeGate")
    {
        _pMainUI->timeGate->move(x,y);
        _pMainUI->timeGate->setWidth(width);
        _pMainUI->timeGate->setHeight(height);
        _pMainUI->timeGate->setBorder(border);
        _pMainUI->timeGate->setCaption(title);
        _pMainUI->timeGate->setSize(size);
        _pMainUI->timeGate->setUnit(unit);
        _pMainUI->timeGate->show();
    }
    if(type == "elapsedTime")
    {
        _pMainUI->elapsedTime->move(x,y);
        _pMainUI->elapsedTime->setWidth(width);
        _pMainUI->elapsedTime->setHeight(height);
        _pMainUI->elapsedTime->setBorder(border);
        _pMainUI->elapsedTime->setCaption(title);
        _pMainUI->elapsedTime->setSize(size);
        _pMainUI->elapsedTime->setUnit(unit);
        _pMainUI->elapsedTime->show();
    }
    if(type == "distTFlown")
    {
        _pMainUI->distFlown->move(x,y);
        _pMainUI->distFlown->setWidth(width);
        _pMainUI->distFlown->setHeight(height);
        _pMainUI->distFlown->setBorder(border);
        _pMainUI->distFlown->setCaption(title);
        _pMainUI->distFlown->setSize(size);
        _pMainUI->distFlown->setUnit(unit);
        _pMainUI->distFlown->setBaseUnit(unit);
        _pMainUI->distFlown->show();
        _pMainUI->distFlown->setHideUnit(hideUnit);
    }
    if(type == "avgTSpeed")
    {
        _pMainUI->avgSpeed->move(x,y);
        _pMainUI->avgSpeed->setWidth(width);
        _pMainUI->avgSpeed->setHeight(height);
        _pMainUI->avgSpeed->setBorder(border);
        _pMainUI->avgSpeed->setCaption(title);
        _pMainUI->avgSpeed->setSize(size);
        _pMainUI->avgSpeed->setUnit(unit);
        _pMainUI->avgSpeed->setBaseUnit(unit);
        _pMainUI->avgSpeed->show();
        _pMainUI->avgSpeed->setHideUnit(hideUnit);
    }

    if(type == "SatelliteStatus")
    {
        _pMainUI->satelliteStatus->move(x,y);
        _pMainUI->satelliteStatus->resize(20,8);
        _pMainUI->satelliteStatus->show();
    }

    if(type == "SatelliteStatusPage")
    {
        _pMainUI->satelliteStatusPage->move(x,y);
        _pMainUI->satelliteStatusPage->resize(240,320);
        _pMainUI->satelliteStatusPage->show();
    }
}
