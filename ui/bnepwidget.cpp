/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "bnepwidget.h"
#include <QPainter>
#include <QLabel>
#include "utils/netstats.h"

#include <iostream>

BNEPWidget::BNEPWidget(QWidget *parent) : QWidget(parent)
{
    connect(&tick,SIGNAL(timeout()),this,SLOT(timerTick()));
    tick.setInterval(500);
    tick.start();
}

void BNEPWidget::paintEvent(QPaintEvent *event) {

    QPainter painter(this);
    if(status == NetStats::connectionStatus::NoConnection){
        QPixmap image("resources/bmp/BNEP-no-conn.bmp");
        painter.drawPixmap(QPoint(0,0),image);
    }
    if(status == NetStats::connectionStatus::BNEPConnected){
        if(++showAnimation%2)
        {
            QPixmap image("resources/bmp/BNEP-no-internet.bmp");
            painter.drawPixmap(QPoint(0,0),image);
        }
    }
    if(status == NetStats::connectionStatus::InternetConnected){
        QPixmap image("resources/bmp/BNEP-ok.bmp");
        painter.drawPixmap(QPoint(0,0),image);
    }
}

void BNEPWidget::setStatus(NetStats::connectionStatus s)
{
    status = s;
}

void BNEPWidget::timerTick()
{
    update();
}
