/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef BORDERBOX_H
#define BORDERBOX_H

#include <QWidget>

class BorderBox : public QWidget
{
    Q_OBJECT
public:
    enum Border {None = 0, Top = 1, Right = 2, Bottom = 4, Left = 8, All = 15};

    explicit BorderBox(Border flags = Border::All, QWidget *parent = 0);
    explicit BorderBox(QWidget *parent = 0);

    void setNoBorder();
    void setAllBorders();

signals:

public slots:

protected:
    virtual void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;

private:
    unsigned borderMask;
};

#endif // BORDERBOX_H
