/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "magicbox.h"

#include <QPainter>

#include "utils/cstgraphics.h"
using Vario::drawStr;
using Vario::strWidth;

MagicBox::MagicBox(QWidget *parent) : BorderBox(parent)
{
    resize(widthM,heightM);
}

void MagicBox::paintEvent(QPaintEvent *event) {

    QPainter painter(this);
    painter.setPen(QPen(QColor("black")));

    if(border)
    {
        painter.drawLine(0, 0, width() - 1, 0);
        painter.drawLine(width() - 1, 0, width() - 1, height() - 1);
        painter.drawLine(0, height() - 1, width() - 1, height() - 1);
        painter.drawLine(0, 0, 0, height() - 1);
    }

    int captionHeight = 0;

    if(caption !="")
    {
        painter.fillRect(0,0, width() - 1, 14, QColor("black"));
        painter.setPen(QPen(QColor("white")));
        drawStr(width()/2, 0, caption, 14, painter, Qt::AlignHCenter);
        captionHeight = 14;
    }
    painter.setPen(QPen(QColor("black")));

    int unitStrWidth = 0;

    if(unit != "" && !hideUnit)
    {
        int un_y = (height() + captionHeight - unitHeight) / 2;

        unitStrWidth = strWidth(unit,unitHeight);
        //int un_x = 3 * width() / 4 - 2*padding; // unit X
        int un_x = width() - unitStrWidth - 3;
        drawStr(un_x, un_y, unit, unitHeight, painter);
    }

    int y = (height() + captionHeight - valHeight) / 2;
    int x = width()-unitStrWidth - 6;
    drawStr(x, y, val, valHeight, painter, Qt::AlignRight);
}

void MagicBox::setHideUnit(int value)
{
    hideUnit = value;
}

QString MagicBox::getBaseUnit() const
{
    return baseUnit;
}

void MagicBox::setBaseUnit(const QString &value)
{
    baseUnit = value;
}

void MagicBox::setUnitHeight(int value)
{
    unitHeight = value;
}

void MagicBox::setWidth(int value)
{
    widthM = value;
    resize(widthM,height());
}

void MagicBox::setHeight(int value)
{
    heightM = value;
    resize(width(),heightM);
}

void MagicBox::setBorder(bool value)
{
    border = value;
}

void MagicBox::setUnit(const QString &value)
{
    unit = value;
}

void MagicBox::setCaption(const QString &value)
{
    caption = value;
    //we need border for caption
    if(caption != ""){
        border = true;
    }
}

void MagicBox::setSize(int size)
{
    valHeight = size;
}

void MagicBox::setValue(const QString &str) {
    val = str;
    update();
}

void MagicBox::setAlignment(Qt::Alignment flags) {
    alignment = flags;
}
