/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MAPSCALEINDICATOR_H
#define MAPSCALEINDICATOR_H

#include <QWidget>

class MapScaleIndicator : public QWidget
{
    Q_OBJECT
public:
    explicit MapScaleIndicator(QWidget *parent = nullptr);

    void setTargetWidth(int value);
    void paint(int x, int y);
    void setMetersToPixels(float value);

    QString getUnit() const;
    void setUnit(const QString &value);

signals:

public slots:

private:
    int targetWidth = 100;
    float metersToPixels = 0;
    QString unit = "km";

    QWidget *parent;
};

#endif // MAPSCALEINDICATOR_H
