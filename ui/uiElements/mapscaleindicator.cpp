/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mapscaleindicator.h"
#include <QPainter>
#include "utils/cstgraphics.h"

MapScaleIndicator::MapScaleIndicator(QWidget *parent) : QWidget(parent)
{
    this->parent = parent;
}

void MapScaleIndicator::setTargetWidth(int value)
{
    targetWidth = value;
}

void MapScaleIndicator::paint(int x, int y)
{
    //scalesInMeters must end with 0 value to mark the end
    int scalesInMeters[] = {100,200,500,1000,2000,5000,10000,20000,50000,0};
    int scalesInTousandOfAMile[] = {100,200,500,1000,2000,5000,10000,20000,50000,0};
    QPainter painter(parent);

    if(unit == "mi")
    {
        //determine scale to be used
        int scale = 1;
        for(int i=0;scalesInTousandOfAMile[i]!=0;i++)
        {
            if((scalesInTousandOfAMile[i] * metersToPixels) / 1.609 <= targetWidth)scale = scalesInTousandOfAMile[i];
        }
        int barWidth = (scale * metersToPixels)/1.609;
        QString scaleStr = QString::number((double)scale / 1000,'f',1) + "mi";
        int strWidth = Vario::strWidth(scaleStr,14);

        painter.fillRect(x,y,barWidth+2+strWidth,14,Qt::white);
        painter.fillRect(x,y+2,barWidth,10,Qt::black);

        painter.setPen(Qt::black);
        Vario::drawStr(x+barWidth+1,y,scaleStr,14,painter);
    }
    else
    {
        //determine scale to be used
        int scale = 1;
        for(int i=0;scalesInMeters[i]!=0;i++)
        {
            if(scalesInMeters[i] * metersToPixels <= targetWidth)scale = scalesInMeters[i];
        }
        int barWidth = scale * metersToPixels;
        QString scaleStr = QString::number((double)scale / 1000,'f',1) + "km";
        int strWidth = Vario::strWidth(scaleStr,14);

        painter.fillRect(x,y,barWidth+2+strWidth,14,Qt::white);
        painter.fillRect(x,y+2,barWidth,10,Qt::black);

        painter.setPen(Qt::black);
        Vario::drawStr(x+barWidth+1,y,scaleStr,14,painter);
    }
}

void MapScaleIndicator::setMetersToPixels(float value)
{
    metersToPixels = value;
}

QString MapScaleIndicator::getUnit() const
{
    return unit;
}

void MapScaleIndicator::setUnit(const QString &value)
{
    unit = value;
}
