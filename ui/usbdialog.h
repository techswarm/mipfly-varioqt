/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef USBDIALOG_H
#define USBDIALOG_H

#include <QDialog>
#include <QTimer>

#include "menu/usbDialog/usbdialogmenu.h"
#include "menu/usbDialog/usblistmenu.h"

class GPIOEmulator;

namespace Ui {
class USBDialog;
}

class USBDialog : public QDialog
{
    Q_OBJECT

public:
    explicit USBDialog(const NameManager &nameMgr, QWidget *parent = 0);
    ~USBDialog();

    GPIOEmulator& gpioEmulator() const;

    void gpioPressed(int btn);

    void addDevice(const QString &dev);
    void removeDevice(const QString &dev);
    int deviceCount() const;

    Ui::USBDialog& getUi() const;

    QTimer secTimer;

private:
    Ui::USBDialog *ui;    
    int state = 1;
    int hideTimeout = 15;

    USBDialogMenu rootMenu;
    USBListMenu listMenu;

    void choicePressed(const QModelIndex &index);

    void resetHideTimeout();

    void showEvent(QShowEvent *e) Q_DECL_OVERRIDE;
    void hideEvent(QHideEvent *e) Q_DECL_OVERRIDE;

private slots:
    void secTick();
};

#endif // USBDIALOG_H
