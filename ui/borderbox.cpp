/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "borderbox.h"

#include <QPainter>

BorderBox::BorderBox(Border flags, QWidget *parent):
    QWidget(parent), borderMask(flags)
{

}

BorderBox::BorderBox(QWidget *parent) : QWidget(parent), borderMask(Border::All)
{

}

void BorderBox::paintEvent(QPaintEvent *event) {
    QPainter painter(this);

    painter.setPen(QPen(QColor("black")));

    if (borderMask & Border::Top) {
        painter.drawLine(0, 0, width() - 1, 0);
    }
    if (borderMask & Border::Right) {
        painter.drawLine(width() - 1, 0, width() - 1, height() - 1);
    }
    if (borderMask & Border::Bottom) {
        painter.drawLine(0, height() - 1, width() - 1, height() - 1);
    }
    if (borderMask & Border::Left) {
        painter.drawLine(0, 0, 0, height() - 1);
    }
}

void BorderBox::setNoBorder() {
    borderMask = None;
    update();
}

void BorderBox::setAllBorders() {
    borderMask = All;
    update();
}
