/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef AIRSPACEPLOT_H
#define AIRSPACEPLOT_H

#include <QWidget>
#include "navUtils/coord.hpp"
#include "navUtils/OpenAirParser.h"
#include <QFuture>
#include <QFutureWatcher>

class AirSpacePlot : public QWidget
{
    Q_OBJECT
private:
    void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;
    Coord mapCenter;
    //double _metersToPixels = 0.005;
    double _metersToPixels = 0.0025;
    OpenAirParser *_p = nullptr;
    QString oldAirspaceFile;
    bool gotFocus = false;
    QFuture<void> computeFuture;
    QFutureWatcher<void> computeWtcher;
    int course;
    QString unit;

protected:
    void timerEvent(QTimerEvent *event);

public:
    AirSpacePlot(QWidget *parent);
    void focus();
    void unfocus();
    void shortPress(int btn);

    QString getUnit() const;
    void setUnit(const QString &value);

public slots:
    void setCenter(Coord coord);
    void setCourse(qreal course);
private slots:
    void computeFinished();
};

#endif // AIRSPACEPLOT_H
