/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "MapPlot.h"
#include "maps/MapUtils.h"
#include <QImage>
#include <QPainter>
#include <QDebug>
#include <QString>
#include <QSettings>
#include "utils/vario_common.h"
#include "utils/qfonts.h"


MapPlot::MapPlot(QWidget *parent):QWidget(parent)
{
    zoomLevel = 11;
    QSettings settings;
    QString map = settings.value("navigation/Map","Romania").toString();
    mapName = map;

    double lat = settings.value("navigation/lastLatitude", 42.21).toDouble();
    double lon = settings.value("navigation/lastLongitude", 21.42).toDouble();

    mapCenter.setLat(lat);
    mapCenter.setLon(lon);

    openDatabase();

}

void MapPlot::openDatabase(){
    QDir mapDir("maps");
    if(mapNameSettings == "AUTO"){
        mapNames.clear();
        mapNames = mapDir.entryList(QStringList() << "*.mbtiles",QDir::Files);
    }
    else
    {
        mapNames.clear();
        mapNames.append(mapNameSettings + ".mbtiles");
    }

    foreach (mapName, mapNames) {
        QString path = QString("maps/")+mapName;
        if(!QFileInfo::exists(path))continue;
        databaseGlobal = QSqlDatabase::addDatabase("QSQLITE", mapName);
        databaseGlobal.setDatabaseName(path);
        if (!databaseGlobal.open()) {
            qDebug() << databaseGlobal.lastError().text();
        } else {
            qDebug() << "Using map database" << path;
        }
    }
    mapName = "default";
}

void MapPlot::setCenter(Coord coord)
{
    mapCenter.setLat(coord.getLat());
    mapCenter.setLon(coord.getLon());

    //mapCenter.setLat(45.05);
    //mapCenter.setLon(9.884);
}

QString MapPlot::getMapFolder() const
{
    return mapName;
}

void MapPlot::setMapFolder(const QString &value)
{
    mapName = value;
}

int MapPlot::getZoomLevel() const
{
    return zoomLevel;
}

void MapPlot::setZoomLevel(int value)
{
    zoomLevel = value;
}

void MapPlot::timerEvent(QTimerEvent *event)
{
    //mapCenter.setLat(mapCenter.getLat()-0.001);
    update();
}

void MapPlot::drawTile(QImage tile, int x, int y)
{
    if(tile.isNull())return;
    QPainter painter(this);
    tile = tile.convertToFormat(QImage::Format_RGB888);
    uchar *bits = tile.bits();
    //QVector<QRgb> v = tile.colorTable();

    QSettings settings;
    bool showWoods = settings.value("navigation/showWoods",true).toBool();


    for(int i=0;i<256;i++)
    {
        if(i+y>0 && i+y<=height())
        {
            for(int j=0;j<256;j++)
            {
                //qDebug()<<v[bits[256*i+j]];

                if(j+x>0 && j+x<=width())
                {
                    if(bits[(256*i+j)*3]<100 && bits[(256*i+j)*3+1]<100 && bits[(256*i+j)*3+2]<100)
                    {
                        painter.drawPoint(j+x,i+y);
                    }
                    if(showWoods == true)
                    {
                        if(bits[(256*i+j)*3]<10 && bits[(256*i+j)*3+1]>100 && bits[(256*i+j)*3+2]<10)
                        {
                            painter.drawPoint(j+x,i+y);
                        }
                    }
                    if(bits[(256*i+j)*3]>55 && bits[(256*i+j)*3+1]<55 && bits[(256*i+j)*3+2]<55)
                    {
                        if(i%2 == 0 || j%2 == 0)painter.drawPoint(j+x,i+y);
                    }
                    if(bits[(256*i+j)*3]<55 && bits[(256*i+j)*3+1]<55 && bits[(256*i+j)*3+2]>55)
                    {
                        if((i+j)%2==0)painter.drawPoint(j+x,i+y);
                    }
                }
            }
        }
    }
}

QByteArray MapPlot::getTileData(int x,int y)
{
    QByteArray data;

    y = flipY(zoomLevel,y);

    qDebug()<<"mapName "<<mapName;
    QSqlDatabase databaseLoc = QSqlDatabase::database(mapName);
    QSqlQuery query(mapName,databaseLoc);
    query.prepare("SELECT tile_data FROM tiles WHERE zoom_level = :zoom AND tile_column = :column AND tile_row = :row");
    query.bindValue(":zoom", zoomLevel);
    query.bindValue(":column", x);
    query.bindValue(":row", y);

    if (query.exec()) {
        query.first();
        if(query.isValid())
        {
            data = query.value(0).toByteArray();
            return data;
        }
    }

    //if we are here no data was returned and we have to slect a database to be used
    foreach (QString mapNameToTry, mapNames) {
        QSqlDatabase databaseLoc = QSqlDatabase::database(mapNameToTry);
        QSqlQuery query(mapNameToTry,databaseLoc);

        if(!databaseLoc.isOpen())
        {
            continue;
        }

        query.prepare("SELECT tile_data FROM tiles WHERE zoom_level = :zoom AND tile_column = :column AND tile_row = :row");
        query.bindValue(":zoom", zoomLevel);
        query.bindValue(":column", x);
        query.bindValue(":row", y);
        qDebug()<<"mapName to try "<<mapNameToTry;

        if (query.exec()) {
            qDebug()<<"exec complete on "<<mapNameToTry;
            query.first();
            if(query.isValid())
            {
                data = query.value(0).toByteArray();
                //save new map name
                mapName = mapNameToTry;
                qDebug()<<"mapName "<<mapName;
                return data;
            }
        }
        qDebug()<<"mapName invalid"<<mapNameToTry;
    }

    querySuccesfull = false;
    return data;
}

int MapPlot::flipY(int zoom,int y){
    return (pow(2,zoom) -1) - y;
}

void MapPlot::setHeading(qreal h)
{
    heading = h;
}


void MapPlot::focus()
{
    gotFocus = true;
    update();
}

void MapPlot::unfocus()
{
    gotFocus = false;
    update();
}

void MapPlot::shortPress(int btn)
{
    if(gotFocus)
    {
        if(btn == Vario::GPIO1)
        {
            //zoom in
            if(zoomLevel < 12)zoomLevel++;
        }
        if(btn == Vario::GPIO2)
        {
            //zoom out
            if(zoomLevel > 8)zoomLevel--;
        }
        update();
    }
}


void MapPlot::paintEvent(QPaintEvent *event)
{
    QSettings settings;
    QString map = settings.value("navigation/Map","Romania").toString();
    if(map != mapNameSettings){
        mapNameSettings = map;
        openDatabase();
    }

    QPainter painter(this);
    painter.fillRect(0, 0, width(), height(), Qt::white);

    //get center tile x/y
    int xTile = MapUtils::long2TileX(mapCenter.getLon(),zoomLevel);
    int yTile = MapUtils::lat2TileY(mapCenter.getLat(),zoomLevel);

    int xTilePixel = MapUtils::long2TilePixelX(mapCenter.getLon(),zoomLevel);
    int yTilePixel = MapUtils::lat2TilePixelY(mapCenter.getLat(),zoomLevel);

    int xOffset = width()/2 - xTilePixel;
    int yOffset = height()/2 - yTilePixel;

    QByteArray data;

    if(xTile != loadedXTile || yTile != loadedYtile || !querySuccesfull){

        querySuccesfull = true;

        data = getTileData(xTile-1,yTile-1);
        if(data.count()>20)
        {
            tile1 = QImage::fromData(data,"PNG");
            drawTile(tile1,xOffset-256,yOffset-256);
        }

        data = getTileData(xTile,yTile-1);
        if(data.count()>20)
        {
            tile2 = QImage::fromData(data,"PNG");
            drawTile(tile2,xOffset,yOffset-256);
        }

        data = getTileData(xTile+1,yTile-1);
        if(data.count()>20)
        {
            tile3 = QImage::fromData(data,"PNG");
            drawTile(tile3,xOffset+256,yOffset-256);
        }

        data = getTileData(xTile-1,yTile);
        if(data.count()>20)
        {
            tile4 = QImage::fromData(data,"PNG");
            drawTile(tile4,xOffset-256,yOffset);
        }

        data = getTileData(xTile,yTile);
        if(data.count()>20)
        {
            tile5 = QImage::fromData(data,"PNG");
            drawTile(tile5,xOffset,yOffset);
        }

        data = getTileData(xTile+1,yTile);
        if(data.count()>20)
        {
            tile6 = QImage::fromData(data,"PNG");
            drawTile(tile6,xOffset+256,yOffset);
        }

        data = getTileData(xTile-1,yTile+1);
        if(data.count()>20)
        {
            tile7 = QImage::fromData(data,"PNG");
            drawTile(tile7,xOffset-256,yOffset+256);
        }

        data = getTileData(xTile,yTile+1);
        if(data.count()>20)
        {
            tile8 = QImage::fromData(data,"PNG");
            drawTile(tile8,xOffset,yOffset+256);
        }

        data = getTileData(xTile+1,yTile+1);
        if(data.count()>20)
        {
            tile9 = QImage::fromData(data,"PNG");
            drawTile(tile9,xOffset+256,yOffset+256);
        }

        loadedXTile = xTile;
        loadedYtile = yTile;

    } else {

        //tiles are already  loaded from database
        drawTile(tile1,xOffset-256,yOffset-256);
        drawTile(tile2,xOffset,yOffset-256);
        drawTile(tile3,xOffset+256,yOffset-256);
        drawTile(tile4,xOffset-256,yOffset);
        drawTile(tile5,xOffset,yOffset);
        drawTile(tile6,xOffset+256,yOffset);
        drawTile(tile7,xOffset-256,yOffset+256);
        drawTile(tile8,xOffset,yOffset+256);
        drawTile(tile9,xOffset+256,yOffset+256);

    }

    QPainter painter2(this);
    int _xc = width() / 2;
    int _yc = height() / 2;

    int h1 = 6, h2 = 2, h3 = 6, wd = 5;
    //int h1 = 5, h2 = 3, h3 = 5, wd = 3;
    QPoint arrPts[] { {_xc, _yc-h1}, {_xc-wd, _yc+h3}, {_xc, _yc+h2}, {_xc+wd, _yc+h3} };

    float angle =  ( heading * M_PI ) / 180;

    for  (QPoint &pt : arrPts) {
        qreal oldX = qreal(pt.x() - _xc);
        qreal oldY = qreal(pt.y() - _yc);

        int x = int(oldX * qCos(angle) - oldY * qSin(angle));
        int y = int(oldY * qCos(angle) + oldX * qSin(angle));

        pt.setX(x + _xc);
        pt.setY(y + _yc);
    }
    painter2.setBrush(Qt::white);
    painter2.drawEllipse(QPoint(width()/2,height()/2),9,9);


    painter2.setPen(QPen(QBrush("black"),2));
    painter2.drawPolygon(arrPts, 4);

    if(gotFocus)
    {
        painter2.setFont(Vario::get8pxFont());
        painter2.fillRect(width()-20,10,10,10,QBrush("black"));
        painter2.setPen(QColor("white"));
        painter2.drawText(width()-18,18,"F");
    }
}
