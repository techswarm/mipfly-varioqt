/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CAPTIONBOX_H
#define CAPTIONBOX_H

#include <QWidget>

namespace Ui {
class CaptionBox;
}

class CaptionBox : public QWidget
{
    Q_OBJECT

public:
    explicit CaptionBox(QWidget *parent = 0);
    ~CaptionBox();

    void setValueAlign(const Qt::Alignment &value);

    void setSize(int size);
    void setCursorPos(int value);

public slots:
    void setCaption(const QString &str);
    void setUnit(const QString &str);
    void setValue(const QString &str);

protected:
    virtual void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;

private:
    Ui::CaptionBox *ui;

    static const int padding = 3;
    static const int vpadding = 0;

    int cursorPos = -1;

    QString caption;
    QString prewCaption = "";
    QString unit;
    QString val;

    int captionHeight = 14;
    int unitHeight = 14;
    int valHeight = 22;
    Qt::Alignment valueAlign = Qt::AlignRight;
};

#endif // CAPTIONBOX_H
