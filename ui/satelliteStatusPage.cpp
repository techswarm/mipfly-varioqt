/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "satelliteStatusPage.h"
#include <QPainter>
#include <QtDebug>
#include <string>
#include <cmath>
#include "utils/cstgraphics.h"

SatelliteStatusPage::SatelliteStatusPage(QWidget *parent) : QWidget(parent)
{
    fix_ = FIX_UNKNOWN;
    imageEarth.load("resources/bmp/earth0.bmp");
    imageSat.load("resources/bmp/no-gps-fix.bmp");
}

void SatelliteStatusPage::paintEvent(QPaintEvent *event) {

    QPainter painter(this);

    painter.setPen(QColor(Qt::black));

    painter.drawLine(0, 0, width() - 1, 0);
    painter.drawLine(width() - 1, 0, width() - 1, height() - 1);
    painter.drawLine(0, height() - 1, width() - 1, height() - 1);
    painter.drawLine(0, 0, 0, height() - 1);


    double radians;
    double px;
    double py;

    radians = iconAngle * M_PI / 180;
    px = 105 + (70 * cos(radians));
    py = 85 + (70 * sin(radians));

    drawStr(240/2, 0, strTime, 16, painter, Qt::AlignHCenter);

    painter.drawPixmap(QPoint(40,20),imageEarth);
    painter.drawPixmap(QPoint(px,py),imageSat);

    painter.drawLine(0, 170, width(), 170);
    drawStr(10, 175, "vDOP", 16, painter, Qt::AlignLeft);
    drawStr(150, 175, strVDop, 16, painter, Qt::AlignLeft);

    painter.drawLine(0, 195, width(), 195);
    drawStr(10, 200, "Fix" , 16, painter, Qt::AlignLeft);
    drawStr(150, 200, strFix, 16, painter, Qt::AlignLeft);

    painter.drawLine(0, 220, width(), 220);
    drawStr(10, 225, tr("Lon") , 16, painter, Qt::AlignLeft);
    drawStr(150, 225, strLon, 16, painter, Qt::AlignLeft);

    painter.drawLine(0, 245, width(), 245);
    drawStr(10, 250, tr("Lat") , 16, painter, Qt::AlignLeft);
    drawStr(150, 250, strLat, 16, painter, Qt::AlignLeft);

    painter.drawLine(0, 270, width(), 270);
    drawStr(10, 275, tr("Height") , 16, painter, Qt::AlignLeft);
    drawStr(150, 275, strAlt, 16, painter, Qt::AlignLeft);

    painter.drawLine(0, 295, width(), 295);
    painter.drawLine(240/2, 170, 240/2, 295);

    drawStr(240/2, 300, tr("PRESS ENTER TO CLOSE"), 16, painter, Qt::AlignHCenter);

}

void SatelliteStatusPage::updateStatus(Coord position, int alt, int fix, QString strTime, int numSatellitesUsed, double vDop) {

    strLat = position.getLatStr();
    strLon = position.getLonStr();
    strAlt = QString::number(alt);
    strFix = getFixStr(fix);
    this->strTime = strTime;
    strSatsUsed = QString::number(numSatellitesUsed);
    strVDop = QString::number(vDop);
    iconAngle=iconAngle+5;
    update();

}



