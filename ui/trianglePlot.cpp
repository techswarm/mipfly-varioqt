/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "trianglePlot.h"
#include "VarioQt.h"
#include <QPainter>
#include <QDebug>
#include "uiElements/mapscaleindicator.h"

TrianglePlot::TrianglePlot(QWidget *parent) : QWidget(parent)
{
    pTask = VarioQt::instance().getTask();
    pFac = VarioQt::instance().getFaiAssistCompute();
    invalidateTimer.setInterval(200);
    connect(&invalidateTimer,SIGNAL(timeout()),this,SLOT(invalidateTimerTick()));
    invalidateTimer.start();
}

void TrianglePlot::invalidateTimerTick()
{
    update();
}

void TrianglePlot::zoomIn()
{
    _metersToPixels *= 1.2;
}

void TrianglePlot::zoomOut()
{
    _metersToPixels *= 0.8;
}

void TrianglePlot::zoomToExtent()
{
    if(pFac->getMaxDistToTrianglePoint()!=0)_metersToPixels = min(width()/2,height()/2) / pFac->getMaxDistToTrianglePoint();
}

void TrianglePlot::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    Coord center = pTask->getCenter();
    painter.setPen(QPen(QColor("black")));


    double metersToPixels = _metersToPixels;

    //determine best zoom to fit triangles
    //if(pFac->getMaxDistToTrianglePoint()!=0)metersToPixels = min(width()/2,height()/2) / pFac->getMaxDistToTrianglePoint();

    double distanceMeters = center.distanceKm(pFac->getTp1()) * 1000;

    double angleToCenter = center.bearingTo(pFac->getTp1());

    double distancePixels = distanceMeters * metersToPixels;
    //qDebug()<<distancePixels;
    QPointF centerTp1(distancePixels*sin(angleToCenter),distancePixels*cos(angleToCenter)*-1);

    distanceMeters = center.distanceKm(pFac->getTp2()) * 1000;

    angleToCenter = center.bearingTo(pFac->getTp2());

    distancePixels = distanceMeters * metersToPixels;
    //qDebug()<<distancePixels;
    QPointF centerTp2(distancePixels*sin(angleToCenter),distancePixels*cos(angleToCenter)*-1);

    painter.drawLine(centerTp1.x()+width()/2,centerTp1.y()+height()/2,centerTp2.x()+width()/2,centerTp2.y()+height()/2);

    QPolygonF poly;
    poly.clear();
    foreach (Coord coord, pFac->getLeftAreaPoints()) {
        double distanceMeters = center.distanceKm(coord) * 1000;

        double angleToCenter = center.bearingTo(coord);

        double distancePixels = distanceMeters * metersToPixels;
        //qDebug()<<distancePixels;
        QPointF center(distancePixels*sin(angleToCenter),distancePixels*cos(angleToCenter)*-1);
        poly.append(QPointF(center.x()+width()/2,center.y()+height()/2));
        //painter.drawPoint(center.x()+width()/2,center.y()+height()/2);
    }
    painter.drawPolygon(poly);

    poly.clear();
    foreach (Coord coord, pFac->getRightAreaPoints()) {
        double distanceMeters = center.distanceKm(coord) * 1000;

        double angleToCenter = center.bearingTo(coord);

        double distancePixels = distanceMeters * metersToPixels;
        //qDebug()<<distancePixels;
        QPointF center(distancePixels*sin(angleToCenter),distancePixels*cos(angleToCenter)*-1);
        poly.append(QPointF(center.x()+width()/2,center.y()+height()/2));
        //painter.drawPoint(center.x()+width()/2,center.y()+height()/2);
    }
    painter.drawPolygon(poly);

    poly.clear();
    foreach (Coord coord, pFac->getTaskPoints()) {
        double distanceMeters = center.distanceKm(coord) * 1000;

        double angleToCenter = center.bearingTo(coord);

        double distancePixels = distanceMeters * metersToPixels;
        //qDebug()<<distancePixels;
        QPointF center(distancePixels*sin(angleToCenter),distancePixels*cos(angleToCenter)*-1);
        poly.append(QPointF(center.x()+width()/2,center.y()+height()/2));
        //painter.drawPoint(center.x()+width()/2,center.y()+height()/2);
    }
    painter.drawPolyline(poly);

    if(pFac->getAreaComputed() == true)
    {
        QPen pen;
        pen.setColor(Qt::black);
        pen.setStyle(Qt::PenStyle::DashLine);
        painter.setPen(pen);

        Coord targetWpOptimum = pTask->getNextNavTarget();

        double distanceMeters = center.distanceKmAuto(targetWpOptimum) * 1000;

        double angleToCenter = center.bearingTo(targetWpOptimum);

        double distancePixels = distanceMeters * metersToPixels;
        //qDebug()<<distancePixels;
        QPointF targetWpOptimumXY(distancePixels*sin(angleToCenter),distancePixels*cos(angleToCenter)*-1);


        painter.drawLine(targetWpOptimumXY.x()+width()/2,
                         targetWpOptimumXY.y()+height()/2,
                         width()/2,
                         height()/2
                         );
    }

    QPainter painter2(this);
    int _xc = width() / 2;
    int _yc = height() / 2;

    double lineOffsetX = 600 * sin(pTask->getCourse() * M_PI / 180);
    double lineOffsetY = 600 * cos(pTask->getCourse() * M_PI / 180);


    painter2.drawLine(width()/2,
                      height()/2,
                      width()/2+lineOffsetX,
                      height()/2-lineOffsetY
                      );

    int h1 = 6, h2 = 2, h3 = 6, wd = 5;
    //int h1 = 5, h2 = 3, h3 = 5, wd = 3;
    QPoint arrPts[] { {_xc, _yc-h1}, {_xc-wd, _yc+h3}, {_xc, _yc+h2}, {_xc+wd, _yc+h3} };

    float angle =  ( pTask->getCourse() * M_PI ) / 180;

    for  (QPoint &pt : arrPts) {
        qreal oldX = qreal(pt.x() - _xc);
        qreal oldY = qreal(pt.y() - _yc);

        int x = int(oldX * qCos(angle) - oldY * qSin(angle));
        int y = int(oldY * qCos(angle) + oldX * qSin(angle));

        pt.setX(x + _xc);
        pt.setY(y + _yc);
    }
    painter2.setBrush(Qt::white);
    painter2.drawEllipse(QPoint(width()/2,height()/2),9,9);


    painter2.setPen(QPen(QBrush("black"),2));
    painter2.drawPolygon(arrPts, 4);

    MapScaleIndicator mapScale(this);
    mapScale.setMetersToPixels(metersToPixels);
    mapScale.setTargetWidth(50);
    mapScale.paint(5,height()-14);
}
