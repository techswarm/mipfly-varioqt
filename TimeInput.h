/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TIMEINPUT_H
#define TIMEINPUT_H

#include <QDialog>
#include <QTime>

namespace Ui {
class TimeInput;
}

class TimeInput : public QDialog
{
    Q_OBJECT

public:
    explicit TimeInput(QWidget *parent = 0);
    ~TimeInput();

    QTime time() const;
    void setTime(const QTime &time);

private:
    Ui::TimeInput *ui;
    void showEvent(QShowEvent *e) Q_DECL_OVERRIDE;
    void hideEvent(QHideEvent *e) Q_DECL_OVERRIDE;

    QTime _time;

    void advanceVirtualPointer();
    void reverseVirtualPointer();

    int virtualCursorPoint = 0;

public slots:
    void gpioPressed(int btn);
    void gpioLongPressed(int btn);
};

#endif // TIMEINPUT_H
