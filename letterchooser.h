/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LETTERCHOOSER_H
#define LETTERCHOOSER_H

#include <QWidget>

namespace Ui {
class LetterChooser;
}

class LetterChooser : public QWidget
{
    Q_OBJECT

public:
    explicit LetterChooser(QWidget *parent = 0);
    ~LetterChooser();

    void setLetters();
    void setNumbers();
    void setMixt();
    void setAllPrintAscii();
    void reset();

public slots:
    void next();
    void previous();
    void choose();
    void esc();

signals:
    void select(QChar letter);
    void escape();

protected:
//    void keyPressEvent(QKeyEvent *event) Q_DECL_OVERRIDE;
    void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;

private:
    Ui::LetterChooser *ui;

    int pos = 1;

    QString const *letterSrc;

    void updateView();

    const QChar prevLetter();
    const QChar nextLetter();
};

#endif // LETTERCHOOSER_H
