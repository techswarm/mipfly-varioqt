/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef WARNING_H
#define WARNING_H

#include <QDialog>

namespace Ui {
class Warning;
}

class Warning : public QDialog
{
    Q_OBJECT
public:
    explicit Warning(QWidget *parent = 0);
    ~Warning();

    static void showWarning(QString caption, QString message, int timeout = 5);
    static void showWarningSound(QString caption, QString message, int timeout = 5);
    static void showWarningSound(QString caption, QString message, int timeout, QString alarmName);

    void setCaption(QString caption);
    void setMessage(QString message);
    void setTimeout(int seconds);

    void showEvent(QShowEvent * event) Q_DECL_OVERRIDE;
    void hideEvent(QHideEvent *event) Q_DECL_OVERRIDE;

private:
    Ui::Warning *ui;
    int timeoutSec = 5;
    bool ignoreFirstGPIO = true;

private slots:
    void timeout();
    void GPIOActivationTimeout();

public slots:
    void gpioPressed(int btn);
    void gpioLongPressed(int btn);
};

#endif // WARNING_H
