/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "WptFileHandler.h"
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QXmlStreamReader>

WptFileHandler::WptFileHandler(QString filename)
{
    _fileName = filename;
}

WptFileHandler::WptFileHandler()
{

}

bool WptFileHandler::isAllChrAscii(const QByteArray &byteArray) {

    qDebug() <<  "isAllChrAscii : " << byteArray;

	for (int i = 0; i < byteArray.size(); i++) {
		quint8 chr = (quint8)byteArray.at(i);
		if (chr > 0x7f && chr != 0xD1)
            qDebug() << "\n\nNOT ASCII";
			return false;
	}

	return true;
}

void WptFileHandler::setFileName(const QString &fileName)
{
    _fileName = fileName;
}

FileFormats WptFileHandler::getType()
{
    qDebug() << "getType();";

    QFile file(_fileName);

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
            return eFORMATUNKOWN;

    /*
        File formats identified by fileextention
    */

    if (_fileName.endsWith(".gpx", Qt::CaseInsensitive)) {
            file.close();
            return eFORMATGPX;
    }

    if (_fileName.endsWith(".cup", Qt::CaseInsensitive)) {
            file.close();
            return eFORMATCUP;
    }

    QString head = file.readLine();

    qDebug() << head;

    /*
        File formats identified by fileextention and header
    */
    if (_fileName.endsWith(".wpt", Qt::CaseInsensitive)) {
        if (head.startsWith(compeGpsHead)) {
            file.close();
            return eFORMATCOMPEGPS;
        }
    }

    /*
        File formats identified by header
    */
    if (head.trimmed() == formatGeoHead) {
        file.close();
        return eFORMATGEO;
    }

    if (head.startsWith(oziExplorerHead)) {
        file.close();
        return eFORMATOZI;
    }

    file.close();

    return eFORMATUNKOWN;
}

bool WptFileHandler::readFormatGeo()
{

    parserResult = eWPTLOAD_OK;

    QFile file(_fileName);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
            return false;

    QString head = file.readLine();
    if(head.trimmed() != formatGeoHead) return false;

    while(!file.atEnd())
    {
        QString line = file.readLine();
        QStringList lineTockens = line.split(QRegExp("\\s+"), QString::SkipEmptyParts);

        Waypoint wp;
        if(lineTockens.count() >=10)
        {
            wp.setName(lineTockens.at(0));
            int sign;
            if(lineTockens.at(1)=="N")sign = 1;
            else sign = -1;

            double degreesLat;

            degreesLat = lineTockens.at(2).toInt();

            double seconds = lineTockens.at(3).toInt()*60;
            seconds += lineTockens.at(4).toDouble();

            degreesLat += (double)seconds / 3600;
            degreesLat *= sign;

            if(lineTockens.at(5)=="E")sign = 1;
            else sign = -1;

            double degreesLon;

            degreesLon = lineTockens.at(6).toInt();

            seconds = lineTockens.at(7).toInt()*60;
            seconds += lineTockens.at(8).toDouble();

            degreesLon += (double)seconds / 3600;
            degreesLon *= sign;

            Coord c;
            c.setLat(degreesLat);
            c.setLon(degreesLon);
            wp.setCoordonate(c);

            wp.setHeight(lineTockens.at(9).toFloat());

            if(lineTockens.length()>=11)
            {
                QString desc;
                for(int i=10;i<lineTockens.length();i++)
                {
                    desc += lineTockens[i];
                    desc += " ";
                }
                desc=desc.simplified();
                wp.setDescription(desc);
            }

            //save initial GPSDump string to prevent data degradation
            wp.setGPSDumpStrLat(lineTockens.at(1) + " " + lineTockens.at(2) + " " + lineTockens.at(3) + " " + lineTockens.at(4));
            wp.setGPSDumpStrLon(lineTockens.at(5) + " " + lineTockens.at(6) + " " + lineTockens.at(7) + " " + lineTockens.at(8));

            waypoints.append(wp);
        }
    }

    return true;
}

bool WptFileHandler::readOzi() {

    qDebug() << "readOzi("+_fileName+")";

    int lineNumber = 0;
    bool error = false;
    bool result;

    parserResult = eWPTLOAD_OK;

    QFile file(_fileName);

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qDebug() << "File open error:" << file.errorString();
        return false;
    }

    while ((!file.atEnd())&&(!error)) {

        QByteArray line = file.readLine();
        lineNumber++;

        if (lineNumber == 1) {
            if (!line.startsWith("OziExplorer Waypoint File")) {
                qDebug() << "Wrong filetype";
                error = true;
            }
        }

        if (lineNumber == 2) {
            if (!line.startsWith("WGS 84")) {
                qDebug() << "Wrong mapdatum";
                error = true;
            }

        }

        /*OziExplorer has 4 header rows at top of the file*/
        if (lineNumber > 4) {

            QList<QByteArray> list = line.split(',');

            /* Waypoint must include lat, lon, name and alt to be valid.*/
            if (list.size() < 15) {
                qDebug() << "Line contains to few fields to be parsed into a waypoint";
                error = true;
            } else {
                float lat = list.at(2).trimmed().toFloat(&result);
                if (!result) lat = -999.0;
                float lon = list.at(3).trimmed().toFloat(&result);
                if (!result) lon = -999.0;
                QByteArray name(list.at(1).trimmed());

                double alt_ft = list.at(14).trimmed().toDouble(&result);
                QByteArray descr(list.at(10).trimmed());

                /* -777 is used as invalid altitude in oziExplorer wpt files. Also sanity check for lon/lat*/
                if ((alt_ft != -777)&&(lat >= -90.0)&&(lat <= 90.0)&&(lon >= -180.0)&&(lon <= 180.0)) {
                    int alt_m = (int)(alt_ft*0.3048);
                    qDebug() << lat << ":" << lon << ":" <<name << ":" <<alt_ft<< ":" << alt_m;
                    Waypoint waypoint;
                    waypoint.setName(name);
                    waypoint.setDescription(descr);
                    Coord coord;
                    coord.setLat(lat);
                    coord.setLon(lon);
                    waypoint.setCoordonate(coord);
                    waypoint.setHeight(alt_m);

                    waypoints.append(waypoint);
                } else {
                    parserResult = eWPTLOAD_PARTIALOK;
                }
            }
        }
    }

    return true;
}

bool WptFileHandler::readGpx() {

    qDebug() << "readGpx("+_fileName+")";

    parserResult = eWPTLOAD_OK;

    QFile file(_fileName);

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qDebug() << "File open error:" << file.errorString();
        return false;
    }

    QXmlStreamReader xml(&file);

    bool inWpt = false;


    Waypoint waypoint;

    while (!xml.atEnd()) {
        xml.readNext();

        if (xml.isStartElement()) {

            if (xml.name() == "wpt") {
                inWpt = true;

                /* Description might not exist, set to empty */
                waypoint.setDescription("");

                Coord coord;
                coord.setLon(xml.attributes().value("lon").toFloat());
                coord.setLat(xml.attributes().value("lat").toFloat());
                waypoint.setCoordonate(coord);
            }
        }

        if (inWpt) {

            if (xml.name() == "name") {
                waypoint.setName(xml.readElementText());
            }

            if (xml.name() == "ele") {
                waypoint.setHeight((int)xml.readElementText().toDouble());
            }

            if (xml.name() == "desc") {
                waypoint.setDescription(xml.readElementText());
            }

        }


        if (xml.isEndElement()) {
            if (xml.name() == "wpt") {
                inWpt = false;
                if  (waypoint.description().length() == 0) waypoint.setDescription(waypoint.name());
                waypoints.append(waypoint);
            }
        }


    }

    return true;
}

bool WptFileHandler::readCompeGps()
{
    qDebug() << "readCompeGps("+_fileName+")";

    int lineNumber = 0;
    bool result;
    bool isSupportedMapDatum = false;
    bool isSupportedCoordFormat = false;
    enum CompeGpsFieldType {eFldNotIdentified, eFldG, eFldW, eFldU};

    QByteArray name;
    QString descr;

    float lat;
    float lon;
    float alt_m;
    QString latStr;
    QString latSign;
    QString lonStr;
    QString lonSign;
    CompeGpsFieldType compeGpsFieldType;

    parserResult = eWPTLOAD_OK;

    QFile file(_fileName);

    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << "File open error:" << file.errorString();
        return false;
    }

    QTextStream in(&file);
    in.setCodec("windows-1252");

    while (!in.atEnd()) {

        QString tmpStr = in.readLine();

        QByteArray line = tmpStr.simplified().toUtf8();
        lineNumber++;
        compeGpsFieldType = eFldNotIdentified;

        QList<QString> columnList = tmpStr.split(' ', QString::SkipEmptyParts);

        const QChar *c = columnList.at(0).constBegin();

        switch (c->unicode()) {
        case 'G':
            if (columnList.at(1) == "WGS") {
                isSupportedMapDatum = true;
            }

            compeGpsFieldType = eFldG;

            break;
        case 'W':
            if ((isSupportedMapDatum == true)&&(isSupportedCoordFormat == true)) {
                name = columnList.at(1).trimmed().toUtf8();

                latStr = columnList.at(3).trimmed();
                latSign = latStr.right(1);
                lat = latStr.left(latStr.length()-2).toFloat(&result);
                lonStr = columnList.at(4).trimmed();
                lonSign = lonStr.right(1);
                lon = lonStr.left(lonStr.length()-2).toFloat(&result);
                alt_m = columnList.at(7).trimmed().toDouble(&result);

                descr = "";
                for (int descrWordIdx=8;descrWordIdx<columnList.size();descrWordIdx++) {
                    descr.append(columnList.at(descrWordIdx).trimmed());
                    if (descrWordIdx < (columnList.size()-1)) descr.append(" ");
                }

                if (latSign == "S") {
                    lat = lat * -1;
                }

                if (lonSign == "W") {
                    lon = lon * -1;
                }

                qDebug() << "Name:" << name << " Lat:" << lat << " Lon: " << lon << " Alt:" << alt_m << " Latsign:" << latSign << " Lonsign:" << lonSign;

                compeGpsFieldType = eFldW;
            }

            break;

        case 'U':
            if (columnList.at(1).toInt() == 1) {
                isSupportedCoordFormat = true;
                compeGpsFieldType = eFldU;
            break;
            }
        }


        if (compeGpsFieldType == eFldW) {

            Waypoint waypoint;
            waypoint.setName(name);
            waypoint.setDescription(descr);

            Coord coord;
            coord.setLat(lat);
            coord.setLon(lon);
            waypoint.setCoordonate(coord);
            waypoint.setHeight((int)alt_m);
            waypoints.append(waypoint);
        }
    }

    if ((isSupportedCoordFormat == false) || (isSupportedMapDatum == false)) {
        parserResult = eWPTUNSUPPORTEDFORMAT;
        return false;
    }

    return true;
}


bool WptFileHandler::readCup() {

    qDebug() << "readCup("+_fileName+")";

    int lineNumber = 0;
    bool error = false;
    bool result;
    float alt;
    QString signChar;
    float lat;
    float lon;
    float degrees;
    float minutes;

    parserResult = eWPTLOAD_OK;

    QFile file(_fileName);

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qDebug() << "File open error:" << file.errorString();
        return false;
    }

    QTextStream in(&file);
    in.setCodec("windows-1252");

    while ((!in.atEnd())&&(!error)) {

        QString tmpStr = in.readLine();
        QByteArray line = tmpStr.simplified().toUtf8();

        lineNumber++;

        if (lineNumber == 1) {
            if (!line.contains("name,code,country,lat,lon,elev,style,rwdir,rwlen,freq,desc")) {
                qDebug() << "Wrong filetype";
                error = true;
            }
        }

        if (lineNumber > 1) {

            if (line.startsWith("-----Related Tasks-----") == true) break;

            QList<QByteArray> list = line.split(',');

            for (int i=0;i<list.size();i++) {
                qDebug() << i << "[" << list.at(i) << "]";
            }

            /* Waypoint must include lat, lon, name and alt to be valid.*/
            if (list.size() < 10) {
                qDebug() << "Line contains to few fields to be parsed into a waypoint";
                error = true;
                parserResult = eWPTLOAD_PARTIALOK;
            } else {

                QByteArray name(list.at(0).trimmed().mid(1, list.at(0).trimmed().size()-2));

                QString strLat = list.at(3).trimmed().left(list.at(3).trimmed().length()-1);
                degrees = strLat.left(2).toFloat(&result);

                if (result) {
                    minutes = strLat.mid(3, 6).toFloat(&result);
                    lat = degrees + (minutes/60);
               }

                if (!result) lat = -999.0;

                signChar = list.at(3).trimmed().right(1);
                if (signChar.contains("S", Qt::CaseInsensitive) == true) {
                    lat = lat * -1;
                }

                QString strLon = list.at(4).trimmed().left(list.at(4).trimmed().length()-1);
                degrees = strLon.left(3).toFloat(&result);

                if (result) {
                    minutes =  strLon.mid(4, 6).toFloat(&result);
                    lon = degrees + (minutes/60);
                }

                if (!result) lon = -999.0;

                signChar = list.at(4).trimmed().right(1);
                if (signChar.contains("S", Qt::CaseInsensitive) == true) {
                    lon = lon * -1;
                }

                alt = list.at(5).trimmed().left(list.at(5).trimmed().length()-1).toFloat(&result);

                QString altUnit = list.at(5).trimmed().right(1);

                if (altUnit.contains("ft", Qt::CaseInsensitive) == true) {
                    alt = (int)(alt*0.3048);
                }

                QByteArray descr(list.at(10).trimmed().mid(1, list.at(10).trimmed().size()-2));

                if ((lat >= -90.0)&&(lat <= 90.0)&&(lon >= -180.0)&&(lon <= 180.0)) {
                    Waypoint waypoint;
                    waypoint.setName(name);
                    waypoint.setDescription(descr);
                    Coord coord;
                    coord.setLat(lat);
                    coord.setLon(lon);
                    waypoint.setCoordonate(coord);
                    waypoint.setHeight(alt);
                    waypoints.append(waypoint);
                } else {
                    parserResult = eWPTLOAD_PARTIALOK;
                }
            }
        }
    }

    return true;
}


bool WptFileHandler::writeFormatGeo()
{
    QFile file(_fileName);
    if (!file.open(QIODevice::Truncate | QIODevice::Text | QIODevice::WriteOnly))
        return false;

    QTextStream out(&file);
    out<<formatGeoHead<<endl;

    foreach (Waypoint wp, waypoints) {
        out<<wp.name()<<" "<<wp.GPSDumpStrLat()<<" "<<wp.GPSDumpStrLon()<<" "<<wp.height()<<" "<<wp.description()<<endl;
    }

    file.close();
    return true;
}
