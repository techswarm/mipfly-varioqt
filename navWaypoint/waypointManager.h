/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef WAYPOINTMANAGER_H
#define WAYPOINTMANAGER_H

#include <QObject>
#include "waypoint.h"
#include <QList>
#include <QString>

enum WptLoadResult {eWPTLOAD_UNKNOWN, eWPTLOAD_OK, eWPTLOAD_FAIL, eWPTLOAD_PARTIALOK, eWPTUNSUPPORTEDFORMAT, eWPTLOAD_FORMATUNKOWN};

class waypointManager : public QObject
{
    Q_OBJECT
public:
    explicit waypointManager(QObject *parent = nullptr);
    QList<Waypoint> waypoints;
    QList<Waypoint> waypointsSorted;

    ~waypointManager();

    void loadDefaultWPs();
    void saveDefaultWPs();
    void clearDefaultWPs();

    void sortAlphabetically();
    void sortByDistance(Coord center);

    WptLoadResult loadWPsFromFile(QString filename);
    void saveWPsToFile(QString filename);

    bool waypointFileExists(QString filename);
    void remove(QString filename);

    static const QString waypointPath;

    static QString getWaypoitPath();

signals:

public slots:

private:
    const QString defaultWptFileName = "default.wpt";

};

#endif // WAYPOINTMANAGER_H
