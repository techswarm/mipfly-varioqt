/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef WAYPOINT_H
#define WAYPOINT_H

#include <QObject>
#include "navUtils/coord.hpp"

class Waypoint
{
public:
    explicit Waypoint();
    explicit Waypoint(Coord coordonate,int height,QString name);



    Coord coordonate() const;
    void setCoordonate(const Coord &coordonate);

    float height() const;
    void setHeight(float height);

    QString name() const;
    void setName(const QString &name);

    bool isValid();
    void reset();

    QString GPSDumpStrLat();
    void setGPSDumpStrLat(const QString &GPSDumpStrLat);

    QString GPSDumpStrLon();
    void setGPSDumpStrLon(const QString &GPSDumpStrLon);

    void setEdited();

    double sortDistance() const;
    void setSortDistance(double sortDistance);

    void computeSortDistance(Coord coord);

    QString description() const;
    void setDescription(const QString &description);

signals:

public slots:

private:
    Coord _coordonate;
    QString _GPSDumpStrLat = "";
    QString _GPSDumpStrLon = "";
    double _sortDistance = 0;
    void resetGPSDumpStr();
    float _height;
    QString _name;
    QString _description;
    bool _edited = false;
};
QDataStream &operator<<(QDataStream &out, const Waypoint &waypoint);
QDataStream &operator>>(QDataStream &in, Waypoint &waypoint);


#endif // WAYPOINT_H
