/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "task.h"
#include <QDataStream>
#include "Warning.h"
#include <QDebug>
#include "VarioQt.h"
#include <QTime>
#include "navUtils/e6b.h"

Task::Task(QObject *parent) : QObject(parent)
{
    taskType = TaskType::Race;
    SSSTime.setHMS(12,0,0);
    taskDeadline.setHMS(17,0,0);

    pGlider = VarioQt::instance().getGlider();
    pWind = VarioQt::instance().getWind();
    pFAC = VarioQt::instance().getFaiAssistCompute();

    loatMTKFile(taskFilename);

    taskProgressionTimer.setInterval(100);
    connect(&taskProgressionTimer,SIGNAL(timeout()),this, SLOT(taskProgressionTimerTick()));
    taskProgressionTimer.start();

    if(SSSIndex < 0)computeIDs();
    prepareFlight();
    //SSSIndex = -1;
    recomputeArrivalHeightTime=QTime::currentTime();
}

Task::~Task()
{
    saveMTKFile(taskFilename);
}

Task::TaskType Task::getTaskType() const
{
    return taskType;
}

void Task::setTaskType(const TaskType &value)
{
    taskType = value;
    saveMTKFile(taskFilename);
    prepareFlight();
}

QTime Task::getSSSTime() const
{
    return SSSTime;
}

void Task::setSSSTime(const QTime &value)
{
    SSSTime = value;
    saveMTKFile(taskFilename);
    prepareFlight();
}

QTime Task::getTaskDeadline() const
{
    return taskDeadline;
}

void Task::setTaskDeadline(const QTime &value)
{
    taskDeadline = value;
    saveMTKFile(taskFilename);
    prepareFlight();
}

void Task::loatMTKFile(QString filename)
{
    QFile file(filename);
    if(!file.open(QIODevice::ReadOnly))
    {
        return;
    }
    QDataStream in(&file);
    in>>*this;
}

void Task::saveMTKFile(QString filename)
{
    QFile file(filename);
    if(!file.open(QIODevice::WriteOnly | QIODevice::Truncate))
    {
        return;
    }
    QDataStream out(&file);
    out<<*this;
}

void Task::saveMTKFileDefault()
{
    saveMTKFile(taskFilename);
}

void Task::moveTpUp(int index)
{
    if(index>0)
    {
        TurnPoint tp = turnPoints.at(index);
        turnPoints[index]=turnPoints.at(index-1);
        turnPoints[index-1]=tp;
    }
}

void Task::moveTpDown(int index)
{
    if(index<turnPoints.count()-1)
    {
        TurnPoint tp = turnPoints.at(index);
        turnPoints[index]=turnPoints.at(index+1);
        turnPoints[index+1]=tp;
    }
}

int Task::getSSSIndex() const
{
    return SSSIndex;
}

void Task::setSSSIndex(int value)
{
    SSSIndex = value;
    if(turnPoints.count()>=4)computeSetIDs();
    saveMTKFile(taskFilename);
    prepareFlight();
}

int Task::getESSIndex() const
{
    return ESSIndex;
}

void Task::setESSIndex(int value)
{
    ESSIndex = value;
    if(turnPoints.count()>=4)computeSetIDs();
    saveMTKFile(taskFilename);
    prepareFlight();
}

Task::TaskTurnpointType Task::getTurnpointType(int tpid)
{
    if(tpid == SSSIndex)return TaskTurnpointType::SSS;
    if(tpid == ESSIndex)return TaskTurnpointType::ESS;
    if(tpid == 0)return TaskTurnpointType::Takeoff;
    if(tpid == turnPoints.count()-1)return TaskTurnpointType::GOAL;
    return TaskTurnpointType::TP;
}

QString Task::getTurnpointTypeStr(int tpid)
{
    TaskTurnpointType tpt;
    tpt = getTurnpointType(tpid);
    switch (tpt) {
    case TaskTurnpointType::GOAL:
        return QString("GOAL");
        break;
    case TaskTurnpointType::SSS:
        return QString("SSS");
        break;
    case TaskTurnpointType::ESS:
        return QString("ESS");
        break;
    case TaskTurnpointType::TP:
        return QString("TP");
        break;
    case TaskTurnpointType::Takeoff:
        return QString("TO");
        break;
    default:
        return QString("");
        break;
    }
}

QList<TurnPoint> Task::getTurnPoints() const
{
    return turnPoints;
}

void Task::setTurnPoints(const QList<TurnPoint> &value)
{
    turnPoints = value;
    saveMTKFile(taskFilename);
    prepareFlight();
}

void Task::setSSSTimeGates(const QList<QTime> &gates)
{
    timeGates = gates;
}

void Task::clear()
{
    timeGates.clear();
    turnPoints.clear();
    if(turnPoints.count()>=4)computeIDs();
    saveMTKFile(taskFilename);
    prepareFlight();
}

void Task::addTurnpoint(TurnPoint tp)
{
    turnPoints.append(tp);
    if(turnPoints.count()>=4)computeIDs();
    saveMTKFile(taskFilename);
    prepareFlight();
}

void Task::saveTurnpoint(TurnPoint tp, int tpId)
{
    if(tpId<turnPoints.count())
    {
        turnPoints[tpId]=tp;
    }
    saveMTKFile(taskFilename);
    prepareFlight();
}

void Task::deleteTurnpoint(int tpId)
{
    if(tpId<turnPoints.count())
    {
        turnPoints.removeAt(tpId);
    }
    if(turnPoints.count()>=4)computeIDs();
    saveMTKFile(taskFilename);
    prepareFlight();
}

void Task::clearTurnpoints()
{
    turnPoints.clear();
    computeIDs();
    saveMTKFile(taskFilename);
    prepareFlight();
}

void Task::computeIDs()
{
    if(turnPoints.count()==0)
    {
        SSSIndex = -1;
        ESSIndex = -1;
        return;
    }
    if(turnPoints.count()==1)
    {
        SSSIndex = 0;
        ESSIndex = -1;
        return;
    }
    if(turnPoints.count()==2)
    {
        SSSIndex=0;
        ESSIndex=1;
        return;
    }
    if(turnPoints.count()==3)
    {
        SSSIndex=0;
        ESSIndex=1;
        return;
    }
    if(turnPoints.count()>=4)
    {
        if(SSSIndex <= 0)SSSIndex = 1;
        ESSIndex = turnPoints.count()-2;
        if(SSSIndex>=ESSIndex)SSSIndex = ESSIndex-1;
        return;
    }
    return;
}

void Task::computeSetIDs()
{
    if(ESSIndex<0 || SSSIndex<0)return;
    if(SSSIndex>=ESSIndex)SSSIndex = ESSIndex-1;
}

bool Task::getFlying() const
{
    return flying;
}

Coord Task::getCenter() const
{
    return center;
}

Coord Task::getNextNavTarget()
{
    if(navToSpecial)
    {
        return getSpecialTurnpoint().getWaypoint().coordonate();
    }
    else
    {
        return (taskOptimiser.getTargetCoord(center,navTargetIndex));
    }
}

Coord Task::getOptimisedLocation(int id)
{
    return taskOptimiser.getOptimisedLocation(id);
}

double Task::getNextTargetRadius()
{
    if(navTargetIndex < turnPoints.count())
    return turnPoints.at(navTargetIndex).getRadiusLengthMeters();
    return 0;
}

void Task::setCenter(const Coord &value)
{
    center = value;
}

void Task::doTakeoff()
{
    prepareFlight();
    //check if there is a task defined and if there is one set navtospecial to false
    if(turnPoints.count()>0)navToSpecial = false;
    flying = true;
}

void Task::doLand()
{
    flying = false;
}

bool Task::getOptimumStart() const
{
    return optimumStart;
}

void Task::setOptimumStart(bool value)
{
    optimumStart = value;
    taskOptimiser.setOptimumStart(value);
}

bool Task::getCenterToTP1() const
{
    return centerToTP1;
}

void Task::setCenterToTP1(bool value)
{
    centerToTP1 = value;
}

bool Task::getNavToSpecial() const
{
    return navToSpecial;
}

int Task::getNavTargetIndex() const
{
    return navTargetIndex;
}

void Task::setNavTargetIndex(int value)
{
    navTargetIndex = value;
}

bool Task::zoom()
{
    return _zoom;
}

float Task::getDistWp(bool getCenter)
{
    if(navToSpecial)
    {
        return center.distanceKm(getSpecialTurnpoint().getWaypoint().coordonate());
    }
    else
    {
        if(getCenter)
        {
            if(navTargetIndex < turnPoints.count()-1)
                return center.distanceKmAuto(turnPoints.at(navTargetIndex).getWaypoint().coordonate());
        }
        return taskOptimiser.getDistanceTarget(center,navTargetIndex);
    }
}

float Task::getSpeedToSSS()
{
    if(navTargetIndex!=SSSIndex)return 0;
    QTime now = QTime::currentTime();
    if(now >= SSSTime)return 0;
    float h = now.secsTo(SSSTime);
    if(h<10)return 0;
    h = h/3600;
    float distKm = getDistWp();

    return distKm/h;
}

float Task::getDistGoal()
{
    if(turnPoints.count()>=1)
        return taskOptimiser.getDistanceGoal(center,turnPoints.count()-1);
    else return 0;
}

int Task::getHeightWp()
{
    if(navToSpecial)
    {
        float totalAirDistance = taskOptimiser.getHeightCost(center,getSpecialTurnpoint().getWaypoint().coordonate(),pWind->getWindSpeed(),pWind->getWindDir(),pGlider->getTrimSpeed());
        VarioCompute *pVc = VarioQt::instance().getVarioCompute();
        float averageDaySink = 2;
        if(pVc->getAverageSink() <= -0.5)averageDaySink = fabsf(pVc->getAverageSink());
        int height = pGlider->getHeight() - totalAirDistance / (((float)pGlider->getTrimSpeed()*0.27)/averageDaySink)*1000;
        int wpMslHeight = getSpecialTurnpoint().getWaypoint().height();
        height -= wpMslHeight;
        return height;
    }
    else
    {
        if(turnPoints.count()<=navTargetIndex)return 10000;
        float totalAirDistance = taskOptimiser.getHeightCost(center,navTargetIndex,pWind->getWindSpeed(),pWind->getWindDir(),pGlider->getTrimSpeed(),navTargetIndex);

        VarioCompute *pVc = VarioQt::instance().getVarioCompute();

        float averageDaySink = 2;
        if(pVc->getAverageSink() <= -0.5)averageDaySink = fabsf(pVc->getAverageSink());

        int height = pGlider->getHeight() - totalAirDistance / (((float)pGlider->getTrimSpeed()*0.27)/averageDaySink)*1000;
        int wpMslHeight = 0;
        int targetElevation = VarioQt::instance().getElevation()->getElevation(getNextNavTarget());
        if(targetElevation>0)
            wpMslHeight = targetElevation;
        else
            wpMslHeight = turnPoints.at(getNavTargetIndex()).getWaypoint().height();
        height -= wpMslHeight;
        return height;
    }
}

int Task::getHeightGoal()
{
    if(turnPoints.count()<=1)return 10000;
    float totalAirDistance = taskOptimiser.getHeightCost(center,navTargetIndex,pWind->getWindSpeed(),pWind->getWindDir(),pGlider->getTrimSpeed(),turnPoints.count()-1);
    //return pGlider->getHeight() - totalAirDistance/pGlider->getGlide()*1000;
    //change wp arrival height to be defined base on average sink

    VarioCompute *pVc = VarioQt::instance().getVarioCompute();

    float averageDaySink = 2;
    if(pVc->getAverageSink() <= -0.5)averageDaySink = fabsf(pVc->getAverageSink());
    int height = pGlider->getHeight() - totalAirDistance / (((float)pGlider->getTrimSpeed()*0.27)/averageDaySink)*1000;
    int wpMslHeight = turnPoints.last().getWaypoint().height();
    height -= wpMslHeight;
    return height;
}

float Task::getGlideWp()
{

    if(navToSpecial)
    {
        int hDiff = pGlider->getHeight() - getSpecialTurnpoint().getWaypoint().height();

        if(hDiff == 0)return 0;

        float disntace = getDistWp();

        disntace *= 1000;

        return disntace / hDiff;
    }
    else
    {
        if(navTargetIndex < 0 || navTargetIndex >= turnPoints.count())return 0;
        int hDiff = pGlider->getHeight() - turnPoints.at(navTargetIndex).getWaypoint().height();

        if(hDiff == 0)return 0;

        float disntace = getDistWp();

        disntace *= 1000;

        return disntace / hDiff;
    }

}

float Task::getGlideGoal()
{
    if(turnPoints.count() == 0)return 0;
    int hDiff = pGlider->getHeight() - turnPoints.last().getWaypoint().height();
    if(hDiff == 0)return 0;

    float disntace = getDistGoal();

    disntace *= 1000;

    return disntace / hDiff;
}

QTime Task::getNextTimeGate()
{
    QTime now = QTime::currentTime();
    if(now < SSSTime)return SSSTime;

    for(int i=0;i<timeGates.count();i++)
    {
        if(now < timeGates[i])return timeGates[i];
    }
    return QTime(0,0,0,0);
}

QTime Task::getElapsedTime()
{
    QTime now = QTime::currentTime();
    if(now<SSSTime)
    {
        QTime retTime = QTime(0,0);
        retTime = retTime.addSecs(-1 * SSSTime.secsTo(now));
        return retTime;
    }

    if(navTargetIndex == SSSIndex)
    {
        //we didn't cross the start line yet
        QTime retTime = QTime(0,0);
        retTime = retTime.addSecs(SSSTime.secsTo(now));
        return retTime;
    }

    QTime retTime = QTime(0,0);
    retTime = retTime.addSecs(taskStartTimeVirtual.secsTo(now));
    return retTime;
}

float Task::getDistFlown()
{
    return taskOptimiser.getOptimisedTaskLength() - taskOptimiser.getDistanceGoal(center,navTargetIndex);
}

float Task::getAverageSpeed()
{
    float seconds = QTime(0,0,0).secsTo(getElapsedTime());
    float distFlown = getDistFlown();
    return distFlown / (seconds / 3600);
}

float Task::getOptimisedTaskLength()
{
    return taskOptimiser.getOptimisedTaskLength();
}

TurnPoint Task::getSpecialTurnpoint()
{
    if(navToOptimisedFai == true)
    {
        return pFAC->getTargetTp();
    }
    else
    {
        return specialTurnpoint;
    }
}

void Task::navToSpecialWp(Waypoint wp)
{
    specialTurnpoint.setWaypoint(wp);
    specialTurnpoint.setGoal(TurnPoint::GoalType::CylinterEnter);
    specialTurnpoint.setRadiusLengthMeters(100);
    navToSpecial = true;
    navToOptimisedFai = false;
}

QString Task::getNextWpName() {

    if (navToSpecial) {
        return getSpecialTurnpoint().getWaypoint().name();
    } else {
        if(navTargetIndex>=turnPoints.count())return QString("-");
        return turnPoints.at(navTargetIndex).getWaypoint().name();
    }
}

void Task::navToTaskTp(int tpId)
{
    setNavTargetIndex(tpId);
    navToSpecial = false;
}

void Task::navToTaskTp()
{
    navToSpecial = false;
}

void Task::resetTask()
{
    navToSpecial = false;
    alarm5MinSSSActivated = false;
    alarmSSSActivated = false;
    prepareFlight();
}



void Task::taskProgressionTimerTick()
{
    bool SSSZoom = false;
    if(flying == false)return;
    if(turnPoints.count()<2)return;
    if(navTargetIndex>=turnPoints.count())return;

    //bool alarm5MinSSSActivated = false;
    //bool alarmSSSActivated = false;

    if(recomputeArrivalHeightTime.secsTo(QTime::currentTime())>10)
    {
        getHeightGoal();
        recomputeArrivalHeightTime = QTime::currentTime();
    }

    if(!alarm5MinSSSActivated)
    {
        QTime now = QTime::currentTime();
        if(now.secsTo(SSSTime)<60*5)
        {
            alarm5MinSSSActivated = true;
            Warning::showWarningSound(tr("TASK INFO"),tr("5 MIN TO SSS"),5,"fiveMinToStart.wav");
        }
    }

    if(!alarmSSSActivated)
    {
        QTime now = QTime::currentTime();
        if(now.secsTo(SSSTime)<0)
        {
            alarmSSSActivated = true;
            emit tasktp(navTargetIndex);
            Warning::showWarningSound(tr("TASK INFO"),tr("START OPEN"),5,"taskOpen.wav");
        }
    }

    if(navTargetIndex == SSSIndex || navTargetIndex == SSSIndex+1)
    {
        //we are still at or arround start

        bool SSSGoalReached = turnPoints[SSSIndex].goalReached(center);
        SSSZoom = turnPoints[SSSIndex].isClose();
        if(SSSIndex<turnPoints.count() && SSSGoalReached)
        {
            QTime now = QTime::currentTime();

            if(now>SSSTime)
            {
                if(navTargetIndex == SSSIndex)
                {
                    //task is started for the first time
                    emit tasktp(navTargetIndex);
                    navTargetIndex ++;
                    Warning::showWarningSound(tr("TASK INFO"),tr("TASK STARTED"),5,"taskStarted.wav");                    
                }
                else
                {
                    //task restarted
                    Warning::showWarningSound(tr("TASK INFO"),tr("TASK RESTARTED"),5,"taskStarted.wav");
                }
                taskStartTimeVirtual = debateSSSTime(now);
                taskStartTime = now;                
            }
            else
            {
                qDebug()<<"Time is not to start";
            }
        }
    }
    if(turnPoints[navTargetIndex].goalReached(center) && navTargetIndex != SSSIndex)
    {
        emit tasktp(navTargetIndex);
        if(navTargetIndex == ESSIndex)
        {            
            Warning::showWarningSound(tr("TASK INFO"),tr("ESS COMPLETED"),5,"ESSReached.wav");            
        }
        if(navTargetIndex<turnPoints.count()-1)
        {
            navTargetIndex ++;
            Warning::showWarningSound(tr("TASK INFO"),tr("TP REACHED"),5,"turnPointReached.wav");

        }
        else
        {
            Warning::showWarningSound(tr("TASK INFO"),tr("TASK FINISHED"),5,"goalReached.wav");            
        }
    }

    _zoom = turnPoints[navTargetIndex].isClose() | SSSZoom;
}

void Task::prepareFlight()
{
    if(turnPoints.count()==0)return;
    for(int tpId=0;tpId<turnPoints.count();tpId++)
    {
        if(tpId>0 && turnPoints.at(tpId).getGoal() == TurnPoint::GoalType::Line)
        {
            turnPoints[tpId].setGoalLinePerpendicular(getGoalLineAngle(tpId));
        }
    }
    if(getTurnpointType(0)==TaskTurnpointType::Takeoff)navTargetIndex = 1;
    else navTargetIndex = 0;

    taskOptimiser.setPTurnPoints(&turnPoints);
    taskOptimiser.setSSSId(SSSIndex);
    taskOptimiser.setESSId(ESSIndex);
    taskOptimiser.computeTree();
}

bool Task::getNavToOptimisedFai() const
{
    return navToOptimisedFai;
}

void Task::setNavToOptimisedFai(bool value)
{
    navToOptimisedFai = value;
    navToSpecial = true;
}

int Task::getTaskFileVersion() const
{
    return taskFileVersion;
}

QTime Task::debateSSSTime(QTime now)
{
    if(taskType == Race)
    {
        return SSSTime;
    }
    if(taskType == TimeGates)
    {
        QTime lastTime = SSSTime;
        foreach (QTime timestamp, timeGates) {
            if(now >= timestamp)
            {
                lastTime = timestamp;
            }
        }
        return lastTime;
    }
    return now;
}

double Task::getCourse() const
{
    return course;
}

double Task::getCourseToWp()
{
    if(turnPoints.size() <= navTargetIndex)return 10;
    return center.bearingTo(turnPoints.at(navTargetIndex).getWaypoint().coordonate());
}

double Task::getCourseToWpOptimised()
{
    Coord nextNavTarget = getNextNavTarget();
    if(nextNavTarget.getLat()==0)return 10;
    return center.bearingTo(getNextNavTarget());
}

double Task::getGoalLineAngle(int tpId)
{
    if(flying)
    {
        return turnPoints.at(tpId).getGoalLinePerpendicular();
    }
    else
    {
        if(tpId < 1)return -1;
        else
        {
            int i;
            //search for the first tp that is more than 10m away from this one
            for(i=tpId-1;i>=0;i--)
            {
                if(turnPoints.at(i).getWaypoint().coordonate().distanceKm(turnPoints.at(tpId).getWaypoint().coordonate())>0.1)break;
            }
            return turnPoints.at(i).getWaypoint().coordonate().bearingTo(turnPoints.at(tpId).getWaypoint().coordonate());
        }
    }
}

void Task::setCourse(qreal value)
{
    course = value;
}

QDataStream &operator<<(QDataStream &out, const Task &task)
{
    out<<task.getTaskFileVersion()<<(int)task.getTaskType()<<task.getSSSTime()<<task.getTaskDeadline()<<task.timeGates<<task.getTurnPoints()<<task.getSSSIndex()<<task.getESSIndex();
    return out;
}

QDataStream &operator>>(QDataStream &in, Task &task)
{
    int taskType;
    QTime SSSTime;
    QTime DeadlineTime;
    task.timeGates.clear();
    QList<TurnPoint> turnPoints;
    int SSSIndex = -1;
    int ESSIndex = -1;
    int detectedTaskFileVersion = 0;
    in>>detectedTaskFileVersion;

    if(detectedTaskFileVersion != task.getTaskFileVersion())return in;

    in>>taskType>>SSSTime>>DeadlineTime>>task.timeGates>>turnPoints>>SSSIndex>>ESSIndex;
    task.setTaskType((Task::TaskType)taskType);
    task.setSSSTime(SSSTime);
    task.setTaskDeadline(DeadlineTime);
    task.setTurnPoints(turnPoints);
    task.setSSSIndex(SSSIndex);
    task.setESSIndex(ESSIndex);
    return in;
}
