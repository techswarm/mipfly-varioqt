/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef WPTFILEHANDLER_H
#define WPTFILEHANDLER_H

#include <QString>
#include "waypoint.h"
#include "waypointManager.h"

enum FileFormats {eFORMATUNKOWN, eFORMATGEO, eFORMATOZI, eFORMATGPX, eFORMATCOMPEGPS, eFORMATCUP};

class WptFileHandler
{
public:

    WptFileHandler(QString filename);
    WptFileHandler();

    void setFileName(const QString &fileName);
    FileFormats getType();
    bool readFormatGeo();
    bool readGpx();
    bool readOzi();
    bool readCompeGps();
    bool writeFormatGeo();
    bool readCup();
    QList<Waypoint> waypoints;
    WptLoadResult parserResult;

private:
    QString _fileName;
    const QString formatGeoHead = "$FormatGEO";
    const QString oziExplorerHead = "OziExplorer Waypoint File";
    const QString compeGpsHead = "G  WGS 84";
    bool isAllChrAscii(const QByteArray &byteArray);

};

#endif // WPTFILEHANDLER_H
