/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "turnpoint.h"
#include <QDataStream>
#include "navUtils/coord.hpp"
#include <QPoint>
#include <math.h>
#include <QPainterPath>
#include <QDebug>

TurnPoint::TurnPoint()
{
    radiusLengthMeters = 0;
    goal = GoalType::CylinterEnter;
}

Waypoint TurnPoint::getWaypoint() const
{
    return waypoint;
}

void TurnPoint::setWaypoint(const Waypoint &value)
{
    waypoint = value;
}

TurnPoint::GoalType TurnPoint::getGoal() const
{
    return goal;
}

void TurnPoint::setGoal(const TurnPoint::GoalType &value)
{
    goal = value;
}

float TurnPoint::getRadiusLengthMeters() const
{
    return radiusLengthMeters;
}

void TurnPoint::setRadiusLengthMeters(float value)
{
    radiusLengthMeters = value;
}

double TurnPoint::getGoalLinePerpendicular() const
{
    return goalLinePerpendicular;
}

void TurnPoint::setGoalLinePerpendicular(double value)
{
    goalLinePerpendicular = value;
}

bool TurnPoint::goalReached(Coord center)
{
    GoalPos goalPos = insideOutside(center);
    if(lastPos == Invalid){lastPos = goalPos;return false;}
    if(lastPos != goalPos)
    {
        if(lastPos != Invalid)
        {
            if(lastPos == Inside && goal == CylinderExit){lastPos = goalPos;return true;}
            if(lastPos == Outside && goal == CylinterEnter){lastPos = goalPos;return true;}
            if(goal == Line && goalPos != Invalid){lastPos = goalPos;return true;}
        }
    }
    lastPos = goalPos;
    return false;
}

TurnPoint::GoalPos TurnPoint::insideOutside(Coord center)
{
    if(goal != GoalType::Line)
    {
        double distance = center.distanceKmAuto(waypoint.coordonate())*1000;

        if(distance<radiusLengthMeters+300 && distance>radiusLengthMeters-300)
            tpClose = true;
        else
            tpClose = false;

        if(distance>=radiusLengthMeters)
        {
            return Outside;
        }
        else
        {
            return Inside;
        }
    }
    else
    {
        double distance = center.distanceKm(waypoint.coordonate())*1000;
        if(distance>=radiusLengthMeters/2 + 200)
        {
            tpClose = false;
            return Invalid;
        }
        else
        {
            double distanceMeters = center.distanceKm(waypoint.coordonate()) * 1000;

            double angleToCenter = center.bearingTo(waypoint.coordonate());

            //qDebug()<<distancePixels;
            QPointF center(distanceMeters*sin(angleToCenter),distanceMeters*cos(angleToCenter));

            QPointF lineRight;
            lineRight.setX(center.x()+sin(goalLinePerpendicular+M_PI_2)*radiusLengthMeters/2);
            lineRight.setY(center.y()+cos(goalLinePerpendicular+M_PI_2)*radiusLengthMeters/2);

            QPointF lineLeft;
            lineLeft.setX(center.x()+sin(goalLinePerpendicular-M_PI_2)*radiusLengthMeters/2);
            lineLeft.setY(center.y()+cos(goalLinePerpendicular-M_PI_2)*radiusLengthMeters/2);

            QPointF lineRightOut;
            lineRightOut.setX(lineRight.x()+sin(goalLinePerpendicular)*200);
            lineRightOut.setY(lineRight.y()+cos(goalLinePerpendicular)*200);

            QPointF lineLeftOut;
            lineLeftOut.setX(lineLeft.x()+sin(goalLinePerpendicular)*200);
            lineLeftOut.setY(lineLeft.y()+cos(goalLinePerpendicular)*200);

            QPointF lineRightIn;
            lineRightIn.setX(lineRight.x()+sin(goalLinePerpendicular-M_PI)*200);
            lineRightIn.setY(lineRight.y()+cos(goalLinePerpendicular-M_PI)*200);

            QPointF lineLeftIn;
            lineLeftIn.setX(lineLeft.x()+sin(goalLinePerpendicular-M_PI)*200);
            lineLeftIn.setY(lineLeft.y()+cos(goalLinePerpendicular-M_PI)*200);

            QPainterPath outsidePath;
            outsidePath.setFillRule(Qt::WindingFill);
            QPolygonF polyOutside;
            polyOutside.append(lineRight);
            polyOutside.append(lineRightOut);
            polyOutside.append(lineLeftOut);
            polyOutside.append(lineLeft);
            outsidePath.addPolygon(polyOutside);
            if(outsidePath.contains(QPointF(0,0)))
            {
                tpClose = true;
                return Outside;
            }

            QPainterPath insidePath;
            insidePath.setFillRule(Qt::WindingFill);
            QPolygonF polyInside;
            polyInside.append(lineRight);
            polyInside.append(lineRightIn);
            polyInside.append(lineLeftIn);
            polyInside.append(lineLeft);
            insidePath.addPolygon(polyInside);

            if(insidePath.contains(QPointF(0,0)))
            {
                tpClose = true;
                return Inside;
            }
        }
        tpClose = false;
        return Invalid;
    }
}

bool TurnPoint::isClose()
{
    return tpClose;
}

QDataStream &operator<<(QDataStream &out, const TurnPoint &turnpoint)
{
    out<<turnpoint.getWaypoint()<<(int)turnpoint.getGoal()<<turnpoint.getRadiusLengthMeters();
    return out;
}

QDataStream &operator>>(QDataStream &in, TurnPoint &turnpoint)
{
    Waypoint wp;
    int goal;
    float length;
    in >> wp >> goal >> length;
    turnpoint.setWaypoint(wp);
    turnpoint.setRadiusLengthMeters(length);
    turnpoint.setGoal((TurnPoint::GoalType)goal);
    return in;
}
