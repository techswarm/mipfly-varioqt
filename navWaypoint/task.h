/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TASK_H
#define TASK_H

#include <QObject>
#include <QTime>
#include <QList>
#include "turnpoint.h"
#include <QTimer>
#include "taskOptimiser.h"
#include "utils/glider.h"
#include "navUtils/Wind.hpp"
#include "navUtils/faiassistcompute.h"

class Task : public QObject
{
    Q_OBJECT
public:
    explicit Task(QObject *parent = nullptr);
    ~Task();

    enum TaskType {Race, TimeGates, ElapsedTime};
    enum TaskTurnpointType {Takeoff, SSS, ESS, GOAL, TP};


    TaskType getTaskType() const;
    void setTaskType(const TaskType &value);

    QTime getSSSTime() const;
    void setSSSTime(const QTime &value);
    void setSSSTimeGates(const QList<QTime> &gates);

    QTime getTaskDeadline() const;
    void setTaskDeadline(const QTime &value);

    QList<QTime> timeGates;

    void loatMTKFile(QString filename);
    void saveMTKFile(QString filename);
    void saveMTKFileDefault();

    TurnPoint editedTurnpoint;

    void moveTpUp(int index);
    void moveTpDown(int index);

    int getSSSIndex() const;
    void setSSSIndex(int value);

    int getESSIndex() const;
    void setESSIndex(int value);

    Task::TaskTurnpointType getTurnpointType(int tpid);
    QString getTurnpointTypeStr(int tpid);

    QList<TurnPoint> getTurnPoints() const;
    void setTurnPoints(const QList<TurnPoint> &value);

    void clear();

    void addTurnpoint(TurnPoint tp);
    void saveTurnpoint(TurnPoint tp, int tpId);
    void deleteTurnpoint(int tpId);
    void clearTurnpoints();

    void computeIDs();
    void computeSetIDs();

    bool getFlying() const;

    Coord getCenter() const;
    Coord getNextNavTarget();
    Coord getOptimisedLocation(int id);

    double getCourse() const;
    double getCourseToWp();

    double getGoalLineAngle(int tpId);
    int getNavTargetIndex() const;
    void setNavTargetIndex(int value);

    bool zoom();

    float getDistWp(bool getCenter = false);
    float getDistGoal();

    int getHeightWp();
    int getHeightGoal();

    float getGlideWp();
    float getGlideGoal();

    QTime getNextTimeGate();
    QTime getElapsedTime();
    float getDistFlown();
    float getAverageSpeed();

    float getOptimisedTaskLength();

    void navToSpecialWp(Waypoint wp);
    void navToTaskTp(int tpId);
    void navToTaskTp();
    void resetTask();


    bool getNavToSpecial() const;

    QString getNextWpName();

    double getCourseToWpOptimised();
    double getNextTargetRadius();
    float getSpeedToSSS();
    bool getCenterToTP1() const;
    void setCenterToTP1(bool value);

    int getTaskFileVersion() const;

    TurnPoint getSpecialTurnpoint();
    bool getNavToOptimisedFai() const;
    void setNavToOptimisedFai(bool value);

    bool getOptimumStart() const;
    void setOptimumStart(bool value);

signals:
    void tasktp(int tpIndex);

private slots:
    void taskProgressionTimerTick();

public slots:
    void setCourse(qreal value);
    void setCenter(const Coord &value);
    void doTakeoff();
    void doLand();

private:

#ifdef __armel__
    const QString taskFilename = "/mnt/mipfly/defaultTask.mtk";
#else
    const QString taskFilename = "defaultTask.mtk";
#endif

    TaskType taskType;
    QTime SSSTime;
    QTime taskDeadline;

    QTime taskStartTimeVirtual;     //task start time according to competition rules (Race, TimeGates etc)
    QTime taskStartTime;
    QTime recomputeArrivalHeightTime;

    int SSSIndex = -1;
    int ESSIndex = -1;

    int navTargetIndex = 0;

    QList<TurnPoint> turnPoints;
    TurnPoint specialTurnpoint;
    bool navToSpecial;
    bool optimumStart = false;

    bool flying = false;

    Coord center;
    double course;

    bool centerToTP1 = false;

    void prepareFlight();

    QTimer taskProgressionTimer;

    bool _zoom;

    const int taskFileVersion = 100;

    bool alarm5MinSSSActivated = false;
    bool alarmSSSActivated = false;

    bool navToOptimisedFai = false;

    QTime debateSSSTime(QTime now);

    TaskOptimiser taskOptimiser;

    Glider *pGlider;
    Wind *pWind;
    FaiAssistCompute *pFAC;    

};
QDataStream &operator<<(QDataStream &out, const Task &task);
QDataStream &operator>>(QDataStream &in, Task &task);

#endif // TASK_H
