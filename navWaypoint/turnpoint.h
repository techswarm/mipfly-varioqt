/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TURNPOINT_H
#define TURNPOINT_H

#include "waypoint.h"

class TurnPoint
{
public:
    enum GoalType{Line,CylinterEnter,CylinderExit};
    enum GoalPos{Invalid, Inside, Outside};
    TurnPoint();
    Waypoint getWaypoint() const;
    void setWaypoint(const Waypoint &value);

    GoalType getGoal() const;
    void setGoal(const GoalType &value);

    float getRadiusLengthMeters() const;
    void setRadiusLengthMeters(float value);

    double getGoalLinePerpendicular() const;
    void setGoalLinePerpendicular(double value);

    bool goalReached(Coord center);

    GoalPos insideOutside(Coord center);

    bool isClose();

private:

    GoalPos lastPos = GoalPos::Invalid;

    Waypoint waypoint;

    GoalType goal;

    float radiusLengthMeters = 0;

    bool tpClose = false;

    double goalLinePerpendicular = -1;
};
QDataStream &operator<<(QDataStream &out, const TurnPoint &turnpoint);
QDataStream &operator>>(QDataStream &in, TurnPoint &turnpoint);

#endif // TURNPOINT_H
