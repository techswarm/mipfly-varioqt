/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "waypointManager.h"
#include "WptFileHandler.h"
#include "Warning.h"
#include <QDebug>

#ifdef __ARMEL__
    const QString waypointManager::waypointPath = "/mnt/mipfly/waypointFiles/";
#else
    const QString waypointManager::waypointPath = "waypointFiles/";
#endif

waypointManager::waypointManager(QObject *parent) : QObject(parent)
{
    loadDefaultWPs();
}

waypointManager::~waypointManager()
{
    saveDefaultWPs();
}

void waypointManager::clearDefaultWPs()
{
    WptFileHandler wptFileHandler(waypointPath+defaultWptFileName);

    wptFileHandler.waypoints.clear();
    waypoints.clear();
    wptFileHandler.writeFormatGeo();
}

bool nameLessThan( Waypoint p1, Waypoint p2 )
{
       return p1.name() < p2.name();
    return false;
}

bool distLessThan( Waypoint p1, Waypoint p2 )
{
       return p1.sortDistance() < p2.sortDistance();
    return false;
}

void waypointManager::sortAlphabetically()
{
    waypointsSorted.clear();
    //waypointsSorted = waypoints; Make sure there is a deep copy performed
    foreach (Waypoint wp, waypoints) {
        waypointsSorted.append(wp);
    }

    qSort(waypointsSorted.begin(),waypointsSorted.end(),nameLessThan);
}

void waypointManager::sortByDistance(Coord center)
{
    waypointsSorted.clear();
    //waypointsSorted = waypoints; Make sure there is a deep copy performed
    foreach (Waypoint wp, waypoints) {
        waypointsSorted.append(wp);
        waypointsSorted.last().computeSortDistance(center);
    }

    qSort(waypointsSorted.begin(),waypointsSorted.end(),distLessThan);
}

WptLoadResult waypointManager::loadWPsFromFile(QString filename)
{
    WptFileHandler fileParser(waypointPath+filename);

    FileFormats fileFormat = fileParser.getType();

    qDebug() << "Waypoint file format :" << fileFormat << " : " << waypointPath+filename;

    switch (fileFormat) {
        case eFORMATUNKOWN:
            return eWPTLOAD_FORMATUNKOWN;
            break;
        case eFORMATGEO:
            if (fileParser.readFormatGeo()) waypoints = fileParser.waypoints;
            break;
        case eFORMATOZI:
            if (fileParser.readOzi()) waypoints = fileParser.waypoints;
            break;
        case eFORMATGPX:
            if (fileParser.readGpx()) waypoints = fileParser.waypoints;
            break;
        case eFORMATCOMPEGPS:
            if (fileParser.readCompeGps()) waypoints = fileParser.waypoints;
            break;
        case eFORMATCUP:
            if (fileParser.readCup()) waypoints = fileParser.waypoints;
            break;
    }


    return fileParser.parserResult;
}

void waypointManager::saveWPsToFile(QString filename)
{
    WptFileHandler wptFileHandler(waypointPath+filename);

    wptFileHandler.waypoints = waypoints;
    wptFileHandler.writeFormatGeo();
}

bool waypointManager::waypointFileExists(QString filename)
{
    QFile file(waypointPath+filename);
    return file.exists();
}

void waypointManager::remove(QString filename)
{
    QFile file(waypointPath+filename);
    file.remove();
}

QString waypointManager::getWaypoitPath()
{
    return waypointPath;
}

void waypointManager::loadDefaultWPs()
{
    qDebug() << "loadDefaultWPs()";
    loadWPsFromFile(defaultWptFileName);
}

void waypointManager::saveDefaultWPs()
{
    WptFileHandler wptFileHandler(waypointPath+defaultWptFileName);
    wptFileHandler.waypoints = waypoints;
    wptFileHandler.writeFormatGeo();
}
