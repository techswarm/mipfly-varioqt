/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TASKOPTIMISER_H
#define TASKOPTIMISER_H

#include "turnpoint.h"
#include "boundaryFoundry.h"
#include "turnpointBoundary.h"
#include <QList>

class TaskOptimiser
{
public:
    TaskOptimiser();

    void setPTurnPoints(QList<TurnPoint> *value);

    void computeTree();

    float optimiseRoute(Coord location, int targetId, Coord & target, float & distanceTarget);

    void setSSSId(int value);

    void setESSId(int value);



    float getOptimisedTaskLength() const;
    Coord getTargetCoord(Coord location, int targetId);
    Coord getOptimisedLocation(int id);
    float getDistanceGoal(Coord location, int targetId);
    float getDistanceTarget(Coord location, int targetId);
    float getHeightCost(Coord location, int currentId, int windSpeed, int windDirDegrees, int trimSpeed,int targetId);
    float getHeightCost(Coord location, Coord target, int windSpeed, int windDirDegrees, int trimSpeed);

    bool getOptimumStart() const;
    void setOptimumStart(bool value);

private:
    int SSSId;
    int ESSId;
    QList<TurnPoint> * pTurnPoints;
    QList< QList<TurnPointBoundary> > turnPointBoundryes;
    QList<int> optimisedPointsID;

    float optimisedTaskLength = -1;

    int cacheTimestamp = 0;
    bool optimumStart = false;

    float cachedDistanceGoal;
    float cachedDistanceTarget;
    Coord cachedTargetCoord;

    void cacheValues(Coord location, int targetId);

    void resetCosts(int id);
    void maximiseCosts(int id);
};

#endif // TASKOPTIMISER_H
