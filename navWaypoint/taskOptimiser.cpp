/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "taskOptimiser.h"
#include <QDebug>
#include <QDateTime>
#include "navUtils/e6b.h"

TaskOptimiser::TaskOptimiser()
{

}

void TaskOptimiser::setPTurnPoints(QList<TurnPoint> *value)
{
    pTurnPoints = value;
}

void TaskOptimiser::computeTree()
{
    if(pTurnPoints != nullptr)
    {
        //clear previous data
        turnPointBoundryes.clear();
        optimisedPointsID.clear();

        //start from the first turnpoint and forge boundary
        for(int i = 0;i < pTurnPoints->count();i++)
        {
            optimisedPointsID.append(0);
            if(i<SSSId)
            {
                //only add one tp in the boundry for coherence
                QList<TurnPointBoundary> tpbs;

                TurnPointBoundary tpb;
                tpb.center = pTurnPoints->at(i).getWaypoint().coordonate();
                tpbs.append(tpb);

                turnPointBoundryes.append(tpbs);
            }
            else
            {
                BoundaryFoundry bf(pTurnPoints->at(i));
                bf.compute();
                turnPointBoundryes.append(bf.getBoundaryPoints());
            }
        }

        //start from the last TP and compute costs

        for(int i = turnPointBoundryes.count() - 2;i>=0;i--)
        {

            //maximise costs before computing best option
            maximiseCosts(i);

            for(int j=0;j<turnPointBoundryes[i].count();j++)
            {
                for( int k=0;k<turnPointBoundryes[i+1].count();k++)
                {
                    float distance = turnPointBoundryes[i][j].center.distanceKm(turnPointBoundryes[i+1][k].center);

                    if(distance + turnPointBoundryes[i+1][k].costKm < turnPointBoundryes[i][j].costKm)
                    {
                        turnPointBoundryes[i][j].costKm = distance + turnPointBoundryes[i+1][k].costKm;
                        //qDebug()<<"New distance:"<<i<<" "<<j<<" "<<distance + turnPointBoundryes[i+1][k].costKm;
                    }
                }
            }
        }

        //compute optimised task length
        //get smallest cost for SSSIndex
        optimisedTaskLength = 10000;

        if(SSSId >= turnPointBoundryes.count() || SSSId < 0)return;
        for( int i=0;i<turnPointBoundryes[SSSId].count();i++)
        {
            if(turnPointBoundryes[SSSId][i].costKm < optimisedTaskLength)optimisedTaskLength = turnPointBoundryes[SSSId][i].costKm;
        }

    }
}

float TaskOptimiser::optimiseRoute(Coord location, int targetId, Coord &target, float &distanceTarget)
{
    if(targetId < turnPointBoundryes.count())
    {
        float distanceMin = 10000;
        int minId = 0;

        for(int i=0; i<turnPointBoundryes[targetId].count();i++)
        {
            float distanceToWp = 0;
            distanceToWp = turnPointBoundryes[targetId][i].center.distanceKm(location);
            float distance = 0;

            if(targetId == SSSId and optimumStart)distance = turnPointBoundryes[targetId][i].costKm;
            else distance = distanceToWp + turnPointBoundryes[targetId][i].costKm;

            if(distance < distanceMin)
            {
                minId = i;
                distanceMin = distance;
                distanceTarget = distanceToWp;
                target = turnPointBoundryes[targetId][i].center;
            }
        }
        optimisedPointsID[targetId]=minId;
        return distanceMin;
    }
    return 0;
}

void TaskOptimiser::setSSSId(int value)
{
    SSSId = value;
}

void TaskOptimiser::setESSId(int value)
{
    ESSId = value;
}

float TaskOptimiser::getOptimisedTaskLength() const
{
    return optimisedTaskLength;
}

Coord TaskOptimiser::getTargetCoord(Coord location,int targetId)
{
    int currentTimestamp = QDateTime::currentMSecsSinceEpoch()/1000;
    if(currentTimestamp == cacheTimestamp)
    {
        return cachedTargetCoord;
    }
    else
    {
        cacheTimestamp = currentTimestamp;
        cacheValues(location,targetId);
        return cachedTargetCoord;
    }
}

Coord TaskOptimiser::getOptimisedLocation(int id)
{
    if(id<turnPointBoundryes.count())
        return turnPointBoundryes[id][optimisedPointsID[id]].center;
    return Coord();
}

float TaskOptimiser::getDistanceGoal(Coord location,int targetId)
{
    int currentTimestamp = QDateTime::currentMSecsSinceEpoch()/1000;
    if(currentTimestamp == cacheTimestamp)
    {
        return cachedDistanceGoal;
    }
    else
    {
        cacheTimestamp = currentTimestamp;
        cacheValues(location,targetId);
        return cachedDistanceGoal;

    }
}

float TaskOptimiser::getDistanceTarget(Coord location,int targetId)
{
    int currentTimestamp = QDateTime::currentMSecsSinceEpoch()/1000;
    if(currentTimestamp == cacheTimestamp)
    {
        return cachedDistanceTarget;
    }
    else
    {
        cacheTimestamp = currentTimestamp;
        cacheValues(location,targetId);
        return cachedDistanceTarget;
    }
}

//this function returns a total distance in km that has to be flown in respect to air mass
float TaskOptimiser::getHeightCost(Coord location, int currentId, int windSpeed, int windDirDegrees, int trimSpeed, int targetId)
{
    Coord targetLocation;
    float targetDistance;
    optimiseRoute(location,currentId,targetLocation,targetDistance);
    double course = location.bearingTo(targetLocation);
    E6B e6b(course*57.2957795,trimSpeed,windDirDegrees,windSpeed);
    int groundSpeed = e6b.getGroundSpeed();
    //qDebug()<<"groundSpeed="<<groundSpeed;
    if(groundSpeed == E6B::invalid || groundSpeed == 0)
    {
        return 100000;
    }


    if (pTurnPoints == nullptr) {
        qDebug() << "No pointer to waypoints";
        return 0.0;
    }

    if(currentId > pTurnPoints->count() || currentId >= targetId)
    {
        float delta = (float)trimSpeed / (float)groundSpeed;
        return delta * targetDistance;
    }
    else
    {
        float delta = (float)trimSpeed / (float)groundSpeed;
        return delta * targetDistance + getHeightCost(targetLocation,currentId+1,windSpeed,windDirDegrees,trimSpeed,targetId);
    }
}

//this function returns a total distance in km that has to be flown in respect to air mass
float TaskOptimiser::getHeightCost(Coord location, Coord target, int windSpeed, int windDirDegrees, int trimSpeed)
{
    float targetDistance;
    double course = location.bearingTo(target);
    targetDistance = location.distanceKm(target);
    E6B e6b(course*57.2957795,trimSpeed,windDirDegrees,windSpeed);
    int groundSpeed = e6b.getGroundSpeed();
    if(groundSpeed == E6B::invalid || groundSpeed == 0)
    {
        return 100000;
    }

    float delta = trimSpeed / groundSpeed;
    return delta * targetDistance;
}

bool TaskOptimiser::getOptimumStart() const
{
    return optimumStart;
}

void TaskOptimiser::setOptimumStart(bool value)
{
    optimumStart = value;
}

void TaskOptimiser::cacheValues(Coord location,int targetId)
{
    cachedDistanceGoal = optimiseRoute(location,targetId,cachedTargetCoord,cachedDistanceTarget);
}

void TaskOptimiser::resetCosts(int id)
{
    for(int i=0; i < turnPointBoundryes[id].count();i++)
    {
        turnPointBoundryes[id][i].costKm = 0;
    }
}

void TaskOptimiser::maximiseCosts(int id)
{
    //put a distance that is big enough to be ov rw
    for(int i=0; i < turnPointBoundryes[id].count();i++)
    {
        turnPointBoundryes[id][i].costKm = 10000;
    }
}
