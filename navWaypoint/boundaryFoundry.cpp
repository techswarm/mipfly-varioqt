/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "boundaryFoundry.h"
#include <math.h>
#include <QDebug>

BoundaryFoundry::BoundaryFoundry(TurnPoint tp)
{
    turnPoint = tp;
}

void BoundaryFoundry::setAngleStep(int value)
{
    angleStep = value;
}

void BoundaryFoundry::setLineStep(int value)
{
    lineStep = value;
}

QList<TurnPointBoundary> BoundaryFoundry::getBoundaryPoints() const
{
    return boundaryPoints;
}

void BoundaryFoundry::compute()
{
    if(turnPoint.getGoal() == TurnPoint::GoalType::Line)
    {
        //add the center point of the line
        TurnPointBoundary tpbCenter;
        tpbCenter.center = turnPoint.getWaypoint().coordonate();
        boundaryPoints.append(tpbCenter);

        int offset = lineStep;

        while(offset < turnPoint.getRadiusLengthMeters() / 2)
        {
            TurnPointBoundary tpbLeft;
            TurnPointBoundary tpbRight;

            float angle = turnPoint.getGoalLinePerpendicular();

            tpbLeft.center = turnPoint.getWaypoint().coordonate();
            tpbLeft.center.translateToAuto(offset,angle + M_PI_2);
            tpbRight.center = turnPoint.getWaypoint().coordonate();
            tpbRight.center.translateToAuto(offset,angle - M_PI_2);

            boundaryPoints.append(tpbLeft);
            boundaryPoints.append(tpbRight);
            offset += lineStep;
        }

        //add the line edges to the list
        TurnPointBoundary tpbLeft;
        TurnPointBoundary tpbRight;

        float angle = turnPoint.getGoalLinePerpendicular();

        offset = turnPoint.getRadiusLengthMeters() / 2;
        tpbLeft.center = turnPoint.getWaypoint().coordonate();
        tpbLeft.center.translateToAuto(offset,angle + M_PI_2);
        tpbRight.center = turnPoint.getWaypoint().coordonate();
        tpbRight.center.translateToAuto(offset,angle - M_PI_2);

        boundaryPoints.append(tpbLeft);
        boundaryPoints.append(tpbRight);

    }
    else
    {
        int angleOffset = 0;
        while(angleOffset<360)
        {
            TurnPointBoundary tpb;
            tpb.center = turnPoint.getWaypoint().coordonate();
            tpb.center.translateToAuto(turnPoint.getRadiusLengthMeters(),(float)angleOffset * 0.0174532925);
            boundaryPoints.append(tpb);
            angleOffset += angleStep;
        }
    }
}
