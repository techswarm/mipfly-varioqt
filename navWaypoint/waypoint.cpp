/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "waypoint.h"
#include <cmath>
#include <QDataStream>

Waypoint::Waypoint()
{
    _height = 0;
    _name = "";
}

Waypoint::Waypoint(Coord coordonate, int height, QString name)
{
    _coordonate = coordonate;
    _height = height;
    _name = name;
}

Coord Waypoint::coordonate() const
{
    return _coordonate;
}

void Waypoint::setCoordonate(const Coord &coordonate)
{
    _coordonate = coordonate;
}

float Waypoint::height() const
{
    return _height;
}

void Waypoint::setHeight(float height)
{
    _height = height;
}

QString Waypoint::name() const
{
    return _name;
}

void Waypoint::setName(const QString &name)
{
    _name = name.trimmed();
    _name = _name.replace(' ','_');
}

bool Waypoint::isValid()
{
    if(_height != 0 || _name != 0)return true;
    return false;
}

void Waypoint::reset()
{
    _name = "";
    _height = 0;
    _coordonate = Coord();
}

QString Waypoint::GPSDumpStrLat()
{
    if(_edited)
    {
        resetGPSDumpStr();
        _edited = false;
    }
    if(_GPSDumpStrLat == "")
    {
        if(coordonate().getLat() > 0)
            _GPSDumpStrLat = "N";
        else
            _GPSDumpStrLat = "S";
        double degrees;
        modf(coordonate().getLat(),&degrees);
        double minutes;
        modf((coordonate().getLat()-degrees)*60, &minutes);
        float seconds = (coordonate().getLat() - degrees - (minutes)/60) * 3600;

        _GPSDumpStrLat += " " + QString::number(degrees,'f',0)+" "+QString::number(minutes,'f',0)+" "+QString::number(seconds,'f',2);
    }
    return _GPSDumpStrLat;
}

void Waypoint::setGPSDumpStrLat(const QString &GPSDumpStrLat)
{
    _GPSDumpStrLat = GPSDumpStrLat;
}

QString Waypoint::GPSDumpStrLon()
{
    if(_edited)
    {
        resetGPSDumpStr();
        _edited = false;
    }
    if(_GPSDumpStrLon == "")
    {
        if(coordonate().getLat() > 0)
            _GPSDumpStrLon = "E";
        else
            _GPSDumpStrLon = "W";
        double degrees;
        modf(coordonate().getLon(),&degrees);
        double minutes;
        modf((coordonate().getLon()-degrees)*60, &minutes);
        float seconds = ((float)coordonate().getLon() - (float)degrees - ((float)minutes)/60) * 3600;

        _GPSDumpStrLon += " " + QString::number(degrees,'f',0)+" "+QString::number(minutes,'f',0)+" "+QString::number(seconds,'f',2);
    }
    return _GPSDumpStrLon;
}

void Waypoint::setGPSDumpStrLon(const QString &GPSDumpStrLon)
{
    _GPSDumpStrLon = GPSDumpStrLon;
}

void Waypoint::setEdited()
{
    _edited = true;
}

double Waypoint::sortDistance() const
{
    return _sortDistance;
}

void Waypoint::setSortDistance(double sortDistance)
{
    _sortDistance = sortDistance;
}

void Waypoint::computeSortDistance(Coord coord)
{
    _sortDistance = coord.distanceKm(_coordonate);
}

void Waypoint::resetGPSDumpStr()
{
    _GPSDumpStrLat = "";
    _GPSDumpStrLon = "";
}

QString Waypoint::description() const
{
    return _description;
}

void Waypoint::setDescription(const QString &description)
{
    _description = description;
}

QDataStream &operator<<(QDataStream &out, const Waypoint &waypoint)
{
    out << waypoint.coordonate()<<waypoint.height()<<waypoint.name()<<waypoint.description();
    return out;
}

QDataStream &operator>>(QDataStream &in, Waypoint &waypoint)
{
    Coord coordonate;
    float height;
    QString name;
    QString desc;
    in>>coordonate>>height>>name>>desc;
    waypoint.setCoordonate(coordonate);
    waypoint.setHeight(height);
    waypoint.setName(name);
    waypoint.setDescription(desc);
    return in;
}
