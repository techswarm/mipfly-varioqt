/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __VARIOGUI_VARIO_H
#define __VARIOGUI_VARIO_H

#include <string>

#define VARIOUI_BUFLEN 100 // 10 seconsd of samples

class VarioUI
{
	private:
		int _x, _y;
		int _step = 15;
		int _width = 40;
		int _cmps = 10;
		int _averageMax = 0, _averageMin = 0, _average = 0;
		int _minMaxTextHeight = 19;
		int _averageTextHeight = 37;
		int _cmpsBuf[VARIOUI_BUFLEN];
		int _cmpsBufPos = 0;
		
		bool _reset;
		
		void Compute();

	public:
		VarioUI(int x, int y);
		void Draw();
		void PushCMPS(int cmps);
		void reset();
};

class TextBoxUnitsUI
{
	private:
		int _x, _y;
		int _w, _h;

	public:
		std::string Text;
		std::string Units;
		int TextOffsetRight = 15;
		TextBoxUnitsUI(int x, int y, int w, int h);
		void Draw();
};

class TextBoxCaptionUI
{
	private:
		int _x, _y;
		int _w, _h;

	public:
		std::string Text;
		std::string Caption;
		TextBoxCaptionUI(int x, int y, int w, int h);
		void Draw();
};

class TextBoxMultilineUI
{
	private:
		int _x, _y;
		int _w, _h;

	public:
		std::string Text;
		int Size = 18;
		TextBoxMultilineUI(int x, int y, int w, int h);
		void Draw();
};

#endif
