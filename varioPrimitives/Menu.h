/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __VARIOGUI_MENU_H
#define __VARIOGUI_MENU_H

#include <functional>
#include <string>
#include <vector>
#include <memory>

//#include "widgets.h"

typedef std::function<void(void)> item_callback;

class Menu;

class MenuItem
{
	std::string caption;
	Menu *subMenu;
	item_callback enterFn;
	std::string description;
	
public:
	bool isBackMenuItem;
	
	// Constructors
	MenuItem() {
		this->caption = std::string("BACK");
		subMenu = NULL;
		isBackMenuItem = true;
		
	}
	
	MenuItem(std::string caption, Menu *menu) {
		this->caption = caption;
		subMenu = menu;
		isBackMenuItem = false;
	}
	
	MenuItem(std::string caption, item_callback fn) {
		this->caption = caption;
		enterFn = fn;
		subMenu = NULL;
		isBackMenuItem = false;
	}
	
	std::string getCaption() const;
	Menu* getSubmenu();
	item_callback getFn() const;
	void setDescription(std::string desc);
	const std::string getDescription() const;
};

typedef std::vector<MenuItem>::iterator menu_iterator;

class Menu
{
	const static int DEFAULT_NR_OF_ITEMS = 5;

	std::vector<MenuItem> items;
	menu_iterator iter;

	int startPos;

	int nrItemsDisplayed;
	
	Menu *working;
	Menu *parent;
	unsigned int choice;
	bool locked;
	
	item_callback onChooseFn;
	item_callback onBackFn;
	
	inline void incrementChoice();
	inline void decrementChoice();
	inline menu_iterator iterAtPosition(int position);

	
	Menu* root();

public:

	// Setup
	Menu(int initialSize, int numberOfItemsDisplayed = Menu::DEFAULT_NR_OF_ITEMS);
	void addItem(std::string caption, Menu *menu);
	void addItem(std::string caption, Menu *menu, std::string description);
	void addItem(std::string caption, item_callback fn);
	void addItem(std::string caption, item_callback fn, std::string description);
	void addBackItem();

	// Iteration
	menu_iterator endIter();
	menu_iterator startIter();
	
	// Operations
	void choose();
	void down();
	void up();
	void back();
	std::unique_ptr<std::string> next();
	void resetIter();
	void backToRoot();
	
	// Events
	void onChoose();
	void onBack();
	
	// Setters
	void setParent(Menu *parent);
	void setLocked(bool locked);
	void setOnBack(item_callback onBackFn);
	void setOnChoose(item_callback onChooseFn);
	
	// Getters
	unsigned int getChoice() const;
	int size() const;
	std::string description();

	bool scrollableUpwards();
	bool scrollableDownwards();

	// Debug
	void printText();
};

class MenuUI
{
	bool visible;
	Menu &menu;
	
	int x;
	int y;
	int width;
	int itemHeight;
	
public:
	MenuUI(Menu &menuModel, int x, int y, int width, int itemHeight);
	bool isVisible();
	void setVisible(bool visible);
	void setY(int y);
	
	int getHeight();

	void draw();
};

class UpDownMenu: public Menu
{
public:
	UpDownMenu(item_callback upFn, item_callback downFn);

	void setActions(int max, int min,
					std::function<int(void)> srcFunction,
					std::function<std::string(int)> srcToStrFunction,
					HSliderUI& sliderUI);
};

#endif // __VARIOGUI_MENU_H
