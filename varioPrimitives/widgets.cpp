/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "widgets.h"
#include "varioPower.h"

#include <iostream>
#include <cmath>

// Slider
Slider::Slider(int x, int y, int width, int height, int min, int max)
{
	this->x = x;
	this->y = y;
	this->width = width;
	this->height = height;

	setDefaults(min, max, 20);

	this->visible = false;
}

void Slider::setDefaults(int min, int max, int val)
{
	this->min = min;
	this->max = max;
	this->val = val;
}

void Slider::setVal(int val)
{
	if (val >= min && val <= max) {
		this->val = val;
	}
}

void Slider::setY(int y)
{
	this->y = y;
}

void Slider::setMaximum(int maximum) {
	this->max = maximum;
}

void Slider::setMinimum(int minimum) {
	this->min = minimum;
}

void Slider::draw()
{
	fillRect(x, y, width, height, white);
	drawRect(x, y, width, height, black);
}

int Slider::getHeight() {
	return height;
}

// HSlider
HSlider::HSlider(int x, int y, int width, int height, int min, int max)
	: Slider(x,y,width, height) {}

void HSlider::draw()
{
	int w = width * abs(min - val) / (max - min);

	Slider::draw();
	fillRect(x, y, w, height, black);
}

// BatteryUI

BatteryUI::BatteryUI(int x,int y)
{
	this->_x = x;
	this->_y = y;
}

void BatteryUI::SetValue(int charge)
{
	this->_value = charge;
}
void BatteryUI::SetStatus(std::string status)
{
	this->_status = status;
}
void BatteryUI::Draw()
{
	drawRect(_x,_y,22,8,black);
	fillRect(_x+22,_y+2,2,4,black);
	
	//should auto get status and charge
	varioPower power;
	_value = power.GetBatteryCapacityPercent();
	_status = power.GetBatteryStatus();
	
	if(_status.compare("Charging")==0)
	{
		//force charging animation for low battery
		if(_value < 10)_value = 20;
		_barAnimationLevel+=3;
		if(_barAnimationLevel>_value)_barAnimationLevel = 0;
		int chargeBar = (_barAnimationLevel/10)*2;
		fillRect(_x+1,_y+1,chargeBar,7,black);
	}
	else
	{
		int chargeBar = (_value/10)*2;
		fillRect(_x+1,_y+1,chargeBar,7,black);
	}
	
}

// LetterChooserModel

const std::string LetterChooserModel::UPPERCASE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ ";
const std::string LetterChooserModel::LOWERCASE = "abcdefghijklmnopqrstuvwxyz ";

LetterChooserModel::LetterChooserModel(const std::string &letterString)
	: letters(letterString) {
	pos = 1;
}

std::string LetterChooserModel::letterAtIndex(size_t index) {
	char buf[2]{0};	// init buffer with 0 on all positions
	buf[0] = letters.at(index);
	return std::string(buf);
}

std::string LetterChooserModel::crtLetter() {
	return letterAtIndex(pos);
}

std::string LetterChooserModel::prevLetter() {
	int index = (pos - 1 < 0 ? letters.length() - 1 : pos - 1);
	return letterAtIndex(index);
}

std::string LetterChooserModel::nextLetter() {
	int index = (pos + 1 == letters.length() ? 0 : pos + 1);
	return letterAtIndex(index);
}

void LetterChooserModel::next() {
	pos++;
	if (pos == letters.length()) {
		pos = 0;
	}
}

void LetterChooserModel::previous() {
	pos--;
	if (pos < 0) {
		pos = letters.length() - 1;
	}
}

// LetterChooser

LetterChooser::LetterChooser(LetterChooserModel &model, int x, int y, int width, int height, const int fontSize)
	: model(model), fontSize(fontSize) {
	this->x = x;
	this->y = y;
	this->width = width;
	this->height = height;
	
	cBoxLength = LetterChooser::INNER_BOX;
	padding = LetterChooser::PADDING;
}

void LetterChooser::draw() {
	fillRect(x, y, width, height, white);
	drawRect(x, y, width, height, black);
}

void LetterChooser::setY(int y) {
	this->y = y;
}

int LetterChooser::getHeight() {
	return height;
}

// HLetterChooser

HLetterChooser::HLetterChooser(LetterChooserModel &model, int x, int y, int width, int height, const int fontSize)
	: LetterChooser(model, x, y, width, height, fontSize) {}

void HLetterChooser::draw() {
	LetterChooser::draw();
	
	drawRect(cBoxX(), y + 1, cBoxLength, height - 2, black);
	drawStr(x + width/2, y, model.crtLetter(), fontSize, black, Center);
	drawStr(width/2 - cBoxLength/2 + x - padding, y, model.prevLetter(), fontSize, black, Right);
	drawStr(width/2 + cBoxLength/2 + x + padding, y, model.nextLetter(), fontSize, black, Left);
}

int HLetterChooser::cBoxX() {
	return width/2 - cBoxLength/2 + x;
}

// InputField

InputField::InputField(int x, int y, int width, int height, int fontSize) : Hideable() {
	this->x = x;
	this->y = y;
	this->width = width;
	this->height = height;
	this->fontSize = fontSize;
}

void InputField::draw() {
	// TODO: draw rectangle, letters from buffer & cursor
	fillRect(x, y, width, height, white);
	drawRect(x, y, width, height, black);
	drawStr(x + 2, y, buffer, fontSize, black, Left);
	
	if (shouldDrawCursor()) {
		fillRect(strWidth(buffer, fontSize) + x, y + 2, 3, height - 4, black);
	}
}

bool InputField::shouldDrawCursor() {
	bool ans = (count < 4 ? true : false);
	
	if (count == 8) {
		count = 0;
	}
	count++;
	return ans;
}


void InputField::accept(std::string text) {
	buffer.append(text);
}

void InputField::clear() {
	buffer.clear();
}

void InputField::backspace() {
	if (!buffer.empty()) {
		buffer.pop_back();
	}
}

const std::string InputField::value() {
	return buffer;
}

void InputField::setY(int y) {
	this->y = y;
}

int InputField::getHeight() {
	return height;
}

// ArrowRect

ArrowRect::ArrowRect(int x, int y, int w, int h, VerticalDirection dir) {
	_x = x;
	_y = y;
	_w = w;
	_h = h;
	_direction = dir;
}

void ArrowRect::draw() {
	fillRect(_x, _y, _w, _h, white);
	drawRect(_x, _y, _w, _h, black);
	drawVTriangle(_x + _w/2, _y + 1, 10, 10, black, _direction);
}

void ArrowRect::setY(int y) {
	_y = y;
}

int ArrowRect::getHeight() {
	return _h;
}

// SliderUI

SliderUI::SliderUI()
	: valueTB("0", 14, 50, 20)
{
	valueTB.setPan(Center);
	valueTB.setOpaque(true);
}

void SliderUI::draw() {
	valueTB.draw();
}

void SliderUI::updateVal() {
	val = source();
	valueTB.setVal(sourceStr(val));
}

void SliderUI::setSourceStr(std::function<std::string(int)> fn) {
	sourceStr = fn;
}

void SliderUI::setSource(std::function<int(void)> fn) {
	source = fn;
}

void SliderUI::setY(int y) {
	valueTB.setY(y - valueTB.getHeight());
}

int SliderUI::getHeight() {
	return valueTB.getHeight();
}

void SliderUI::setVal(int v) {
	val = v;
}

// HSliderUI

HSliderUI::HSliderUI(int x, int y, int width, int height)
	: slider(x, y, width, height)
{
	/*              w   w_tb
		x_TB = x + -- - ----
	                2     2
	*/
	valueTB.setX((2*x + width - valueTB.getWidth())/2);
}

void HSliderUI::draw() {
	SliderUI::draw();
	slider.draw();
}

void HSliderUI::updateVal() {
	SliderUI::updateVal();
	slider.setVal(val);
}

int HSliderUI::getHeight() {
	return SliderUI::getHeight() + slider.getHeight();
}

void HSliderUI::setY(int y) {
	SliderUI::setY(y);
	slider.setY(y);
}

void HSliderUI::setMinimum(int min) {
	slider.setMinimum(min);
}

void HSliderUI::setMaximum(int max) {
	slider.setMaximum(max);
}
