/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//#include "../fbuitk/fbuitk.h"
#include "vario.h"
#include <math.h>
#include <string>
#include <iostream>

VarioUI::VarioUI(int x, int y)
{
	_reset = true;
	this->_x = x;
	this->_y = y;
	int i;
//	for(i = 0; i < VARIOUI_BUFLEN; i++) {
//		this->_cmpsBuf[i] = 0;
//	}
}

void VarioUI::PushCMPS(int cmps)
{
	this->_cmps = cmps;
	this->_cmpsBuf[this->_cmpsBufPos] = cmps;
	this->_cmpsBufPos++;
	if(this->_cmpsBufPos >= VARIOUI_BUFLEN)
		this->_cmpsBufPos = 0;
}

void VarioUI::Compute()
{
	int i;
	int sum = 0;
	for(i = 0; i < VARIOUI_BUFLEN; i++) {
		if (_reset) {
			_cmpsBuf[i] = 0;
		} else {
			sum += this->_cmpsBuf[i];
		}
	}
	this->_average = sum / VARIOUI_BUFLEN;	// if sum == 0 then _average == 0
	if (_reset) {
		_averageMax = 0;
		_averageMin = 0;
		_reset = false;
	} else {
		if(this->_average > this->_averageMax)
			this->_averageMax = this->_average;
		if(this->_average < this->_averageMin)
			this->_averageMin = this->_average;
	}
}

void VarioUI::reset()
{
	_reset = true;
}

#if 0
void VarioUI::Draw()
{
	// draw container
	Compute();
	drawRect(this->_x - this->_width, this->_y, this->_width, this->_step * 10, black);
	int i;
	for(i = 1; i < 10; i++) {
		drawHLine(this->_x - this->_width / 2, this->_x, this->_y + i * this->_step, black);
	}
	drawHLine(this->_x - this->_width, this->_x, this->_y + 5 * this->_step, black);
	// fill vspeed rectangle
	if(this->_cmps > 0) {
		if(this->_cmps < 500) {
			int height = (int)(double(this->_step * 5) * ((double)this->_cmps / 500));
			fillRect(this->_x - this->_width, this->_y + this->_step * 5 - height, this->_width * 2 / 3, height, black);
		} else {
			if(this->_cmps < 1000) {
				int height = this->_step * 5 - (int)(double(this->_step * 5) * ((double)(this->_cmps - 500) / 500));
				fillRect(this->_x - this->_width, this->_y, this->_width * 2 / 3, height, black);
			}
		}
	} else {
		if(this->_cmps > -500) {
			int height = (int)(double(this->_step * 5) * ((double)this->_cmps / -500));
			fillRect(this->_x - this->_width, this->_y + this->_step * 5, this->_width * 2 / 3, height, black);
		} else {
			if(this->_cmps > -1000) {
				int height = this->_step * 5 - (int)(double(this->_step * 5) * ((double)(this->_cmps + 500) / -500));
				fillRect(
				    this->_x - this->_width, this->_y + this->_step * 10 - height, this->_width * 2 / 3, height, black);
			}
		}
	}

	// void drawStr(int x, int y, std::string str, int size, const Color& color, Pan pan, int drawLimit = 0);

	char buf[50];
	sprintf(buf, "%.1f", ((double)this->_averageMax) / 100);
	drawStr(this->_x - this->_width - 2,
	        this->_y + this->_step * 1 - this->_minMaxTextHeight / 2,
	        buf,
	        this->_minMaxTextHeight,
	        black,
	        Right);
	drawStr(
	    this->_x - this->_width - 2, this->_y + this->_step * 2 - this->_minMaxTextHeight / 2, "Max", 14, black, Right);

	sprintf(buf, "%.1f", ((double)this->_averageMin) / 100);
	drawStr(this->_x - this->_width - 2,
	        this->_y + this->_step * 9 - this->_minMaxTextHeight / 2,
	        buf,
	        this->_minMaxTextHeight,
	        black,
	        Right);
	drawStr(this->_x - this->_width - 2,
	        this->_y + this->_step * 10 - this->_minMaxTextHeight / 2,
	        "Min",
	        14,
	        black,
	        Right);

	sprintf(buf, "%.1f", ((double)this->_average) / 100);
	drawStr(this->_x - this->_width - 2,
	        this->_y + this->_step * 5 - this->_averageTextHeight,
	        buf,
	        this->_averageTextHeight,
	        black,
	        Right);
	drawStr(this->_x - this->_width - 2, this->_y + this->_step * 5 - 5, "m/s", 19, black, Right);
}
#endif

TextBoxUnitsUI::TextBoxUnitsUI(int x, int y, int w, int h)
{
	this->_x = x;
	this->_y = y;
	this->_w = w;
	this->_h = h;
}

#if 0
void TextBoxUnitsUI::Draw()
{
	drawRect(this->_x, this->_y, this->_w, this->_h + 2, black);
	drawStr(this->_x + this->_w - 2, this->_y + this->_h / 2 - 9, this->Units, 19, black, Right);
	drawStr(this->_x + this->_w - this->TextOffsetRight, this->_y + 1, this->Text, this->_h, black, Right);
}
#endif

TextBoxCaptionUI::TextBoxCaptionUI(int x, int y, int w, int h)
{
	this->_x = x;
	this->_y = y;
	this->_w = w;
	this->_h = h;
}

#if 0
void TextBoxCaptionUI::Draw()
{
	drawRect(this->_x, this->_y, this->_w, this->_h + 2 + 14, black);
	fillRect(this->_x, this->_y, this->_w, 14, black);
	drawStr(this->_x + 5, this->_y, this->Caption, 14, white, Left);
	drawStr(this->_x + 5, this->_y + 14, this->Text, this->_h, black, Left);
}
#endif

TextBoxMultilineUI::TextBoxMultilineUI(int x, int y, int w, int h)
{
	this->_x = x;
	this->_y = y;
	this->_w = w;
	this->_h = h;
}

/*
void TextBoxMultilineUI::Draw()
{
	fillRect(this->_x, this->_y, this->_w, this->_h, white);
	drawRect(this->_x, this->_y, this->_w, this->_h, black);

	vector<string> matches = explode(Text, "\\n");

	int hPos = 0;
	int vPos = 0;
	int strWidthPixel;
	auto it = matches.begin();

	for (; it != matches.end(); ++it) {

		//drawStr(this->_x + 2 + hPos, this->_y + 2 + vPos, itLine->str(), this->Size, black, Left);
		vector<string> submatches = explode(*it, "\\s");
		auto subIt = submatches.begin();


		for (; subIt != submatches.end(); ++subIt) {
			//std::cout << it->str() << std::endl;
			strWidthPixel = strWidth(*subIt,Size);
			if(hPos + strWidthPixel < _w) {
				//there is enought place to write the new word
				//basically do nothing
			} else {
				//there is not enought space on this line
				hPos = 0;
				vPos += Size;
				if(vPos + Size > _h - 2)return;
				if(hPos + strWidthPixel > _w)return;
			}
			drawStr(this->_x + 2 + hPos, this->_y + 2 + vPos, *subIt, this->Size, black, Left);
			hPos += strWidthPixel;
			hPos += strWidth(" ",Size);
		}
		if(hPos != 0) {
			hPos = 0;
			vPos += Size;
			if(vPos + Size > _h - 2)return;
			if(hPos + strWidthPixel > _w)return;
		}

	}
}
*/
