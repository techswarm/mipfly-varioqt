/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Menu.h"
#include "fbuitk/fbuitk.h"

#include <iostream>
#include <sstream>
#include "utils.h"

using std::cout;
using std::endl;

#define LOCKED if (root()->locked) return;

// MenuItem

std::string MenuItem::getCaption() const {
	return caption;
}

Menu* MenuItem::getSubmenu() {
	return subMenu;
}

item_callback MenuItem::getFn() const {
	return enterFn;
}

void MenuItem::setDescription(std::string desc) {
	description = desc;
}

const std::string MenuItem::getDescription() const {
	return description;
}

// Menu

// Private helper methods

void Menu::incrementChoice() {
	if (choice < items.size() - 1) {
		if (choice == startPos+nrItemsDisplayed - 1) {
			startPos++;
		}
		choice++;
	}
}

void Menu::decrementChoice() {
	if (choice > 0) {
		if (choice == startPos) {
			startPos--;
		}
		choice--;
	}
}

menu_iterator Menu::iterAtPosition(int position) {
	return items.begin() + position;
}

menu_iterator Menu::endIter() {
	if (working == this){
		auto end = (startPos + nrItemsDisplayed > items.size() ? items.end() : items.begin() + startPos + nrItemsDisplayed);
		return end;
	} else {
		return working->endIter();
	}
}

menu_iterator Menu::startIter() {
	if (working == this){
		auto start = items.begin() + startPos;
		return start;
	} else {
		return working->startIter();
	}
}


Menu* Menu::root() {
	auto menu = this;
	while(menu->parent) {
		menu = menu->parent;
	}
	return menu;
}

// Public

Menu::Menu(int initialSize, int numberOfItemsDisplayed) {
	items.reserve(initialSize);
	nrItemsDisplayed = numberOfItemsDisplayed;
	
	choice = 0;
	working = this;
	locked = true;
	parent = nullptr;
	startPos = 0;
}

void Menu::addItem(std::string caption, Menu *menu) {
	menu->setParent(this);
	items.push_back(MenuItem(caption, menu));
}

void Menu::addItem(std::string caption, Menu *menu, std::string description) {
	menu->setParent(this);
	MenuItem menuItem(caption, menu);
	menuItem.setDescription(description);
	items.push_back(menuItem);
}

void Menu::addItem(std::string caption, item_callback fn) {
	items.push_back(MenuItem(caption, fn));
}

void Menu::addItem(std::string caption, item_callback fn, std::string description) {
	MenuItem menuItem(caption, fn);
	menuItem.setDescription(description);
	items.push_back(menuItem);	
}

void Menu::addBackItem() {
	items.push_back(MenuItem());
}

//void Menu::root() {
//	puts("reset to root menu");
//	if (parent) {
//		parent->root();
//	} else {
//		working = this;
//	}
//}

void Menu::choose() {
	LOCKED
	if (working == this) {
		auto item = items.at(choice);
		if (item.isBackMenuItem) {
			// Go back to previosu menu
			if (parent) {
				onBack();
				parent->back();
			}
		} else {
			Menu *sub = item.getSubmenu();
			if (sub != NULL) {
				sub->onChoose();
				working = sub;
			} else {
				auto fn = item.getFn();
				fn();
			}
		}
	} else {
		working->choose();
	}
}


void Menu::down() {
	LOCKED
	if (working == this) {
		incrementChoice();
	} else {
		working->down();
	}
}

void Menu::up() {
	LOCKED
	if (working == this) {
		decrementChoice();
	} else {
		working->up();
	}
}


void Menu::back() {
	if (working->working == working) {
		working = this;
	} else {
		working->back();
	}
}

void Menu::backToRoot() {
	if (working != this) {
		Menu *tmp = working;
		working = this;
		tmp->backToRoot();
	}
}

void Menu::onBack() {
	if (working == this) {
		if (onBackFn) {
			onBackFn();
		}
	} else {
		working->onBack();
	}
}

void Menu::onChoose() {
	if (onChooseFn) {
		onChooseFn();
	
	}
}

void Menu::setParent(Menu *parent) {
	this->parent = parent;
}

void Menu::setLocked(bool locked) {
	this->locked = locked;
}

void Menu::setOnBack(item_callback onBackFn) {
	this->onBackFn = onBackFn;
}

void Menu::setOnChoose(item_callback onChooseFn) {
	this->onChooseFn = onChooseFn;
}

std::string Menu::description() {
	if (this == working) {
		auto item = items.at(choice);
		return item.getDescription();
	} else {
		return working->description();
	}
}

std::unique_ptr<std::string> Menu::next() {
	if (working == this) {
		if (iter != endIter()) {
			auto item = *iter;
			iter++;
			auto title = item.getCaption();
			return std::unique_ptr<std::string>(new std::string(title));
		} else {
			return std::unique_ptr<std::string>(nullptr);
		}
	} else {
		return working->next();
	}
}

void Menu::resetIter() {
	if (working == this) {
		iter = startIter();
	} else {
		working->resetIter();
	}
}

unsigned int Menu::getChoice() const {
	return (working == this ? choice - startPos : working->getChoice());
}

int Menu::size() const {
	if (working == this) {
		return MIN(items.size(),nrItemsDisplayed);
	} else {
		return working->size();
	}
}

bool Menu::scrollableUpwards() {
	return working == this ? startPos > 0 : working->scrollableUpwards();
}

bool Menu::scrollableDownwards() {
	return working == this ? startPos + nrItemsDisplayed < items.size() : working->scrollableDownwards();
}
 
void Menu::printText() {
	if (working == this) {
		auto iter = startIter();
		auto end = endIter();

		cout << "----" << endl;
		for (unsigned i = 0; iter != end; i++, iter++) {
			auto item = *iter;
			auto title = item.getCaption();

			const bool selected = (i == getChoice());

			if (selected) cout << "[";
			cout << title;
			if (selected) cout << "]";
			cout << endl;
		}

	} else {
		working->printText();
	}
}
	
// MenuUI
MenuUI::MenuUI(Menu &menuModel, int x, int y, int width, int itemHeight)
	: menu(menuModel) {
		visible = false;
		
		this->x = x;
		this->y = y;
		this->width = width;
		this->itemHeight = itemHeight;
}

int MenuUI::getHeight() {
	return menu.size() * itemHeight;
}

void MenuUI::draw() {
	fillRect(x, y, width, getHeight(), white);

	auto iter = menu.startIter();
	auto end = menu.endIter();

	for (unsigned i = 0; iter != end; i++, iter++) {
		auto item = *iter;
		auto title = item.getCaption();

		int Y = y + i * itemHeight;
		const bool selected = (i == menu.getChoice());
		
		if (selected) {
			fillRect(x, Y, width, itemHeight, black);
		} else {
			drawRect(x, Y, width, itemHeight, black);
		}
		drawStr(x + width/2, Y, title, 18, (selected ? white : black), Center);		
	}
}


bool MenuUI::isVisible() {
	return visible;
}

void MenuUI::setVisible(bool visible) {
	this->visible = visible;
	menu.setLocked(!visible);
	if (!visible) {
		menu.backToRoot();
	}
}

void MenuUI::setY(int y) {
	this->y = y;
}

// UpDownMenu

UpDownMenu::UpDownMenu(item_callback upFn, item_callback downFn)
	: Menu(3)
{
	addItem("UP", upFn);
	addItem("DOWN", downFn);
	addBackItem();
}

void UpDownMenu::setActions(int max, int min,
							std::function<int(void)> src,
							std::function<std::string(int)> srcToStr,
							HSliderUI& slider) {
	setOnChoose([&slider, &src, &srcToStr, &max, &min](){
		slider.setMaximum(max);
		slider.setMinimum(min);
		slider.setSource(src);
		slider.setSourceStr(srcToStr);
		slider.setVisible(true);
	});
	setOnBack([&slider](){
		slider.setVisible(false);
	});
}
