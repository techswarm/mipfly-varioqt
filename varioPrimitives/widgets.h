/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __VARIOGUI_WIDGETS_H
#define __VARIOGUI_WIDGETS_H

#define DEFAULT_SLIDER_MIN 0
#define DEFAULT_SLIDER_MAX 100

#include "base.h"
//#include "fbuitk/fbuitk.h"

#include <string>
#include <functional>

class Slider : public Hideable
{
		void setDefaults(int min, int max, int val);

	protected:
		int min;
		int max;
		int val;

		int x;
		int y;
		int width;
		int height;

		Slider(int x, int y, int width, int height, int min = DEFAULT_SLIDER_MIN, int max = DEFAULT_SLIDER_MAX);

		virtual void draw();

	public:
		std::function<int(void)> source;

		void setVal(int val);
		void setY(int y);
		void setMinimum(int min);
		void setMaximum(int max);

		int getHeight();
};

// Horizotal slider
class HSlider : public Slider
{
	public:
		HSlider(int x, int y, int width, int height, int min = DEFAULT_SLIDER_MIN, int max = DEFAULT_SLIDER_MAX);

		void draw();
};


class BatteryUI
{
	private:
		int _x;
		int _y;
		int _value;
		int _barAnimationLevel;
		std::string _status;
	public:
		BatteryUI(int x,int y);
		void SetValue(int charge);
		void SetStatus(std::string status);
		void Draw();
};

class LetterChooserModel
{
	const std::string &letters;
	int pos;
	
	std::string letterAtIndex(size_t index);

public:
	const static std::string UPPERCASE;
	const static std::string LOWERCASE;

	LetterChooserModel(const std::string &letterString = LetterChooserModel::UPPERCASE);

	std::string crtLetter();
	std::string prevLetter();
	std::string nextLetter();
	
	void next();
	void previous();
};

class LetterChooser : public Hideable
{
protected:
	int x;
	int y;
	int width;
	int height;
	
	const int fontSize;
	int cBoxLength;
	int padding;
	
	LetterChooserModel &model;
	
	LetterChooser(LetterChooserModel &model, int x, int y, int width, int height, const int fontSize);

public:
	const static int PADDING = 5;
	const static int INNER_BOX = 34;
	
	int getHeight();
	void setY(int y);
	
	virtual void draw();
};

// Horizontal letter chooser
class HLetterChooser : public LetterChooser
{
	inline int cBoxX();
	
public:
	HLetterChooser(LetterChooserModel &model, int x, int y, int width, int height, const int fontSize = 32);

	void draw();
};

class InputField : public Hideable
{
	int x;
	int y;
	int width;
	int height;
	int fontSize;
	
	unsigned short count;
	
	std::string buffer;
	
	bool shouldDrawCursor();
	
public:
	InputField(int x, int y, int width, int height, int fontSize = 16);
	
	void accept(std::string text);
	void clear();
	void backspace();
	const std::string value();
	
	void setY(int y);
	int getHeight();
	
	void draw();
};

class ArrowRect
{
	int _x;
	int _y;
	int _w;
	int _h;

	VerticalDirection _direction;

public:
	ArrowRect(int x, int y, int w, int h, VerticalDirection dir = Up);
	void draw();

	void setY(int y);
	int getHeight();
};

// Contains: slider and textbox above with slider's value
// Base class -- derived classes are for vertical & horizontal sliders, as needed
class SliderUI: public Hideable
{
	std::function<std::string(int)> sourceStr;
	std::function<int(void)> source;

protected:
	TextBox valueTB;
	int val;

public:
	SliderUI();

	virtual void setY(int y);
	void setSourceStr(std::function<std::string(int)> fn);
	void setVal(int v);
	void setSource(std::function<int(void)> fn);

	virtual void draw();
	virtual void updateVal();
	virtual int getHeight();
};

// Textbox and horizontal slider
class HSliderUI: public SliderUI
{
	HSlider slider;

public:
	HSliderUI(int x, int y, int width, int height);

	void setY(int y);
	void setMinimum(int min);
	void setMaximum(int max);

	int getHeight();

	void draw();
	void updateVal();
};

#endif
