/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//#include "../fbuitk/fbuitk.h"
#include "compass.h"
#include <math.h>

CompassUI::CompassUI(int x, int y)
{
	this->_x = x;
	this->_y = y;
	this->_degrees = 180;
	this->_degreesRad = _degrees * 0.0174532925;
	this->_windDir = 180;
	this->_windSpeed = 88;
}

void CompassUI::SetHeading(int degrees)
{
	this->_degreesRad = (double)degrees * 0.0174532925;
	this->_degrees = degrees;
}

#if 0
void CompassUI::Draw()
{
	float pointX[4];
	float pointY[4];
	
	pointX[0] = this->_x;
	pointY[0] = this->_y;
	
	pointX[1] = this->_x - 10;
	pointY[1] = this->_y + 10;
	
	pointX[3] = this->_x + 10;
	pointY[3] = this->_y + 10;
	
	pointX[2] = this->_x;
	pointY[2] = this->_y -20;
	
	float windDirWork = _windDir - _degrees - 180;
	if(windDirWork < 0)windDirWork +=360;
	float windDegreeRad = windDirWork * 0.0174532925;
	
	rotatePoint(_x,_y,windDegreeRad,pointX[0],pointY[0]);
	rotatePoint(_x,_y,windDegreeRad,pointX[1],pointY[1]);
	rotatePoint(_x,_y,windDegreeRad,pointX[2],pointY[2]);
	rotatePoint(_x,_y,windDegreeRad,pointX[3],pointY[3]);
	
	
	drawCircle(this->_x, this->_y, 50, black);
	drawCircle(this->_x, this->_y, 30, black);
	
	drawLine(pointX[0], pointY[0],pointX[1], pointY[1], black);
	drawLine(pointX[1], pointY[1],pointX[2], pointY[2], black);
	drawLine(pointX[2], pointY[2],pointX[3], pointY[3], black);
	drawLine(pointX[3], pointY[3],pointX[0], pointY[0], black);
	
	
	
	//draw degrees
	drawStr(this->_x - 55, this->_y +40, std::to_string((int)_degrees), 16, black, Left);
	drawStr(this->_x - 55, this->_y +28, "H", 16, black, Left);
	
	//draw wind degrees
	drawStr(this->_x + 55, this->_y +40, std::to_string((int)_windDir), 16, black, Right);
	drawStr(this->_x + 55, this->_y +28, "W", 16, black, Right);
	
	//draw wind degrees
	drawStr(this->_x + 55, this->_y -50, std::to_string((int)_windSpeed), 16, black, Right);
	//drawStr(this->_x + 55, this->_y -38, "WS", 16, black, Right);
	
	// draw north
	double degrees = this->_degreesRad;
	drawStr(this->_x - sin(degrees) * 40, this->_y - cos(degrees) * 40 - 9, "N", 18, black, Center);
	// draw east
	degrees -= M_PI / 2;
	drawStr(this->_x - sin(degrees) * 40, this->_y - cos(degrees) * 40 - 9, "E", 18, black, Center);
	// draw south
	degrees -= M_PI / 2;
	drawStr(this->_x - sin(degrees) * 40, this->_y - cos(degrees) * 40 - 9, "S", 18, black, Center);
	// draw vest
	degrees -= M_PI / 2;
	drawStr(this->_x - sin(degrees) * 40, this->_y - cos(degrees) * 40 - 9, "W", 18, black, Center);
}
#endif

void CompassUI::setWindSpeed(double windSpeed)
{
	_windSpeed = windSpeed;
}
void CompassUI::setWindDir(double windDir)
{
	_windDir = windDir;
}
