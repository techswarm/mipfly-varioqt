/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QObject>
#include <QtTest/QtTest>
#include "../navUtils/e6b.h"

class Teste6B: public QObject
{
    Q_OBJECT
private slots:
    void testProcedure();
};

void Teste6B::testProcedure()
{
      E6B e6b(320,100,90,23);
      QCOMPARE(e6b.getWindCorrectionAngle(),10);
      QCOMPARE(e6b.getHeading(),330);
      QCOMPARE(e6b.getGroundSpeed(),113);

      e6b = E6B(320,10,90,23);
      QCOMPARE(e6b.getWindCorrectionAngle(),E6B::invalid);
      QCOMPARE(e6b.getHeading(),E6B::invalid);
      QCOMPARE(e6b.getGroundSpeed(),E6B::invalid);
}

QTEST_MAIN(Teste6B)
#include "e6btest.moc"
