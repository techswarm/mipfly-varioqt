/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "flightsummary.h"
#include <qdebug.h>
#include <QJsonArray>

QJsonObject jsonFlightSummary;
QJsonArray tpArray;

FlightSummary::FlightSummary()
{

}


FlightSummary::FlightSummary(QJsonObject  jsonFlightSummar)
{
    jsonFlightSummary = jsonFlightSummar;
}


void FlightSummary::newFlightSummary() {
    jsonFlightSummary = QJsonObject();
}

QTime FlightSummary::getFlightDuraton()
{

    if (this->getStartTime().length() < 10 || this->getLandingTime() < 10) { //Qtime format
        QTime qstarttime;
        QTime qfinishtime;

        if (this->getStartTime().length() > 10) {
            qstarttime = QDateTime().fromString(this->getStartTime()).time();

        } else {

            qstarttime = QTime().fromString(this->getStartTime());
        }
        if (this->getLandingTime().length() > 10) {
            qfinishtime = QDateTime().fromString(this->getLandingTime()).time();
        } else {
            qfinishtime = QTime().fromString(this->getLandingTime());
        }

        //qDebug( "qstarttime: %s", qPrintable(qstarttime.toString()));

        flightDuraton = QTime(0,0).addSecs(qstarttime.secsTo(qfinishtime));


    } else {

        QDateTime qstarttime;
        QDateTime qfinishtime;

        qstarttime = QDateTime().fromString(this->getStartTime());
        qfinishtime = QDateTime().fromString(this->getLandingTime());

        flightDuraton = QTime(0,0).addSecs(qstarttime.secsTo(qfinishtime));

    }

    return flightDuraton;
}


void FlightSummary::addTurnpoint(FlightTurnPoint *t_turnpoint)
{

     tpArray.append(t_turnpoint->jsonFlightTurnPoint);
     jsonFlightSummary["Turnpoints"] = tpArray;

}


QString FlightSummary::getPilot()
{
    pilot ="";
    if (jsonFlightSummary.contains("pilot") && jsonFlightSummary["pilot"].isString()) {
        pilot = jsonFlightSummary["pilot"].toString();
    }
    return pilot;
}

void FlightSummary::setPilot(const QString &value)
{
    pilot = value;
    if (pilot.length() > 0) {
        jsonFlightSummary["pilot"] = pilot;
    }

}

QString FlightSummary::getGliderType()
{
    gliderType = "";
    if (jsonFlightSummary.contains("gliderType") && jsonFlightSummary["gliderType"].isString()) {
        gliderType = jsonFlightSummary["gliderType"].toString();
    }
    return gliderType;
}

void FlightSummary::setGliderType(const QString &value)
{
    gliderType = value;
    jsonFlightSummary["gliderType"] = gliderType;
}

QString FlightSummary::getGliderId()
{
    gliderId = "";
    if (jsonFlightSummary.contains("gliderId") && jsonFlightSummary["gliderId"].isString()) {
        gliderId = jsonFlightSummary["gliderId"].toString();
    }
    return gliderId;
}

void FlightSummary::setGliderId(const QString &value)
{
    gliderId = value;
    jsonFlightSummary["gliderId"] = gliderId;
}

QString FlightSummary::getCompetitionId()
{
    gliderId = "";
    if (jsonFlightSummary.contains("competitionId") && jsonFlightSummary["competitionId"].isString()) {
        competitionId = jsonFlightSummary["competitionId"].toString();
    }
    return competitionId;
}

void FlightSummary::setCompetitionId(const QString &value)
{
    competitionId = value;
    jsonFlightSummary["competitionId"] = competitionId;
}

QString FlightSummary::getCompetitionClass()
{
    competitionClass = "";
    if (jsonFlightSummary.contains("competitionClass") && jsonFlightSummary["competitionClass"].isString()) {
        competitionClass = jsonFlightSummary["competitionClass"].toString();
    }
    return competitionClass;
}

void FlightSummary::setCompetitionClass(const QString &value)
{
    competitionClass = value;
    jsonFlightSummary["competitionClass"] = competitionClass;
}

double FlightSummary::getMaxClimb()
{
    maxClimb = 0;
    if (jsonFlightSummary.contains("maxClimb") && jsonFlightSummary["maxClimb"].isDouble()) {
       maxClimb = jsonFlightSummary["maxClimb"].toDouble();
    }

    return maxClimb;
}

void FlightSummary::setMaxClimb(const double &value)
{
    maxClimb = value;
    jsonFlightSummary["maxClimb"] = maxClimb;
}

qreal FlightSummary::getMaxSink()
{
    maxSink = 0;
    if (jsonFlightSummary.contains("maxSink") && jsonFlightSummary["maxSink"].isDouble()) {
        maxSink = jsonFlightSummary["maxSink"].toDouble();
    }
    return maxSink;
}

void FlightSummary::setMaxSink(const qreal &value)
{
    maxSink = value;
    jsonFlightSummary["maxSink"] = maxSink;
}

int FlightSummary::getMaxAltitude()
{

    maxAltitude = 0;
    if (jsonFlightSummary.contains("maxAltitude") && jsonFlightSummary["maxAltitude"].isDouble()) {
        maxAltitude = jsonFlightSummary["maxAltitude"].toInt();
    }
    return maxAltitude;
}

void FlightSummary::setMaxAltitude(int value)
{
    maxAltitude = value;
    jsonFlightSummary["maxAltitude"] = maxAltitude;
}

int FlightSummary::getMaxAltitudeGain()
{
    maxAltitudeGain = 0;
    if (jsonFlightSummary.contains("maxAltitudeGain") && jsonFlightSummary["maxAltitudeGain"].isDouble()) {
        maxAltitudeGain = jsonFlightSummary["maxAltitudeGain"].toInt();
    }
    return maxAltitudeGain;
}

void FlightSummary::setMaxAltitudeGain(const int &value)
{
    maxAltitudeGain = value;
    jsonFlightSummary["maxAltitudeGain"] = maxAltitudeGain;
}

int FlightSummary::getDistance()
{
    Distance = 0;
    if (jsonFlightSummary.contains("Distance") && jsonFlightSummary["Distance"].isDouble()) {
        Distance = jsonFlightSummary["Distance"].toDouble();
    }
    return Distance;
}

void FlightSummary::setDistance(const int &value)
{
    Distance = value;
    jsonFlightSummary["Distance"] = Distance;
}

QString FlightSummary::getStartTime()
{
    startTime ="";
    if (jsonFlightSummary.contains("startTime") && jsonFlightSummary["startTime"].isString()) {
        startTime = jsonFlightSummary["startTime"].toString();
    }
    return startTime;
}

void FlightSummary::setStartTime(const QString &value)
{
    startTime = value;
    jsonFlightSummary["startTime"] = startTime;
}

QString FlightSummary::getStartLocation()
{
    startLocation = "N/A";
    if (jsonFlightSummary.contains("startLocation") && jsonFlightSummary["startLocation"].isString()) {
        startLocation = jsonFlightSummary["startLocation"].toString();
    }
    return startLocation;
}

void FlightSummary::setStartLocation(const QString &value)
{
    startLocation = value;
    jsonFlightSummary["startLocation"] = startLocation;
}

int FlightSummary::getStartAltitude()
{

    startAltitude = 0;
    if (jsonFlightSummary.contains("startAltitude") && jsonFlightSummary["startAltitude"].isDouble()) {
        startAltitude = jsonFlightSummary["startAltitude"].toInt();
    }
    return startAltitude;
}

void FlightSummary::setStartAltitude(const int &value)
{
    startAltitude = value;
    jsonFlightSummary["startAltitude"] = startAltitude;
}

int FlightSummary::getStartLon()
{
    startLon=0;
    if (jsonFlightSummary.contains("startLon") && jsonFlightSummary["startLon"].isDouble()) {
        startLon = jsonFlightSummary["startLon"].toInt();
    }
    return startLon;
}

void FlightSummary::setStartLon(int value)
{
    startLon = value;
    jsonFlightSummary["startLon"] = startLon;
}

long FlightSummary::getStartLat()
{
    startLat=0;
    if (jsonFlightSummary.contains("startLat") && jsonFlightSummary["startLat"].isDouble()) {
        startLat = jsonFlightSummary["startLat"].toInt();
    }
    return startLat;
}

void FlightSummary::setStartLat(long value)
{
    startLat = value;
    jsonFlightSummary["startLat"] = startLat;
}

QString FlightSummary::getLandingTime()
{

    landingTime = "";
    if (jsonFlightSummary.contains("landingTime") && jsonFlightSummary["landingTime"].isString()) {
        landingTime = jsonFlightSummary["landingTime"].toString();
    }
    return landingTime;
}

void FlightSummary::setLandingTime(const QString &value)
{
    landingTime = value;
    jsonFlightSummary["landingTime"] = landingTime;
}

QString FlightSummary::getLandingLocation()
{
    landingLocation ="N/A";
    if (jsonFlightSummary.contains("landingLocation") && jsonFlightSummary["landingLocation"].isString()) {
        landingLocation = jsonFlightSummary["landingLocation"].toString();
    }
    return landingLocation;
}

void FlightSummary::setLandingLocation(const QString &value)
{
    landingLocation = value;
    jsonFlightSummary["landingLocation"] = landingLocation;
}

int FlightSummary::getLandingAltitude()
{
    landingAltitude = 0;
    if (jsonFlightSummary.contains("landingAltitude") && jsonFlightSummary["landingAltitude"].isDouble()) {
        landingAltitude = jsonFlightSummary["landingAltitude"].toInt();
    }
    return landingAltitude;
}

void FlightSummary::setLandingAltitude(const int &value)
{
    landingAltitude = value;
    jsonFlightSummary["landingAltitude"] = landingAltitude;
}

int FlightSummary::getLandingLon()
{

    landingLon= 0;
    if (jsonFlightSummary.contains("landingLon") && jsonFlightSummary["landingLon"].isDouble()) {
        landingLon = jsonFlightSummary["landingLon"].toInt();
    }
    return landingLon;
}

void FlightSummary::setLandingLon(int value)
{
    landingLon = value;
    jsonFlightSummary["landingLon"] = landingLon;
}

int FlightSummary::getLandingLat()
{
    landingLat = 0;
    if (jsonFlightSummary.contains("landingLat") && jsonFlightSummary["landingLat"].isDouble()) {
        landingLat = jsonFlightSummary["landingLat"].toInt();
    }
    return landingLat;
}

void FlightSummary::setLandingLat(int value)
{
    landingLat = value;
    jsonFlightSummary["landingLat"] = landingLat;
}

std::string FlightSummary::getJsonFlightSummaryString()
{


     QJsonDocument flightSummarydoc(jsonFlightSummary);

     return flightSummarydoc.toJson().toStdString();


}




