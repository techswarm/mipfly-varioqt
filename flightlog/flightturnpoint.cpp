/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "flightturnpoint.h"

FlightTurnPoint::FlightTurnPoint()
{
    jsonFlightTurnPoint = QJsonObject();
}

FlightTurnPoint::FlightTurnPoint(QJsonObject t_jsonFlightTurnPoint)
{
    jsonFlightTurnPoint = t_jsonFlightTurnPoint;
}

QString FlightTurnPoint::getTurnPointName()
{    
    turnPointName = "";
    if (jsonFlightTurnPoint.contains("TurnPointName") && jsonFlightTurnPoint["TurnPointName"].isString()) {
        turnPointName = jsonFlightTurnPoint["TurnPointName"].toString();
    }

    return turnPointName;
}

void FlightTurnPoint::setTurnPointName(const QString &value)
{
    turnPointName = value;
     jsonFlightTurnPoint["TurnPointName"] = value;
}

QString FlightTurnPoint::getTurnPointTime()
{
    turnPointTime = "";
    if (jsonFlightTurnPoint.contains("TurnPointTime") && jsonFlightTurnPoint["TurnPointTime"].isString()) {
        turnPointTime = jsonFlightTurnPoint["TurnPointTime"].toString();
    }

    return turnPointTime;
}

void FlightTurnPoint::setTurnPointTime(const QString &value)
{
    turnPointTime = value;
    jsonFlightTurnPoint["TurnPointTime"] = value;
}

QString FlightTurnPoint::getTurnPointLocation()
{
    turnPointLocation = "";
    if (jsonFlightTurnPoint.contains("TurnPointLocation") && jsonFlightTurnPoint["TurnPointLocation"].isString()) {
        turnPointLocation = jsonFlightTurnPoint["TurnPointLocation"].toString();
    }
    return turnPointLocation;
}

void FlightTurnPoint::setTurnPointLocation(const QString &value)
{
    turnPointLocation = value;
    jsonFlightTurnPoint["TurnPointLocation"] = value;
}


QString FlightTurnPoint::getTurnPointType()
{
    turnPointType = "";
    if (jsonFlightTurnPoint.contains("TurnPointType") && jsonFlightTurnPoint["TurnPointType"].isString()) {
        turnPointType = jsonFlightTurnPoint["TurnPointType"].toString();
    }
    return turnPointType;
}

void FlightTurnPoint::setTurnPointType(const QString &value)
{
    turnPointType = value;
    jsonFlightTurnPoint["TurnPointType"] = value;
}


int FlightTurnPoint::getTurnPointAltitude()
{    
    turnPointAltitude = 0;
    if (jsonFlightTurnPoint.contains("TurnPointAltitude") && jsonFlightTurnPoint["TurnPointAltitude"].isDouble()) {
        turnPointAltitude = jsonFlightTurnPoint["TurnPointAltitude"].toInt();
    }
    return turnPointAltitude;
}

void FlightTurnPoint::setTurnPointAltitude(const int &value)
{
    turnPointAltitude = value;
    jsonFlightTurnPoint["TurnPointAltitude"] = value;
}

int FlightTurnPoint::getTurnPointLon()
{
    turnPointLon = 0;
    if (jsonFlightTurnPoint.contains("TurnPointLon") && jsonFlightTurnPoint["TurnPointLon"].isDouble()) {
        turnPointLon = jsonFlightTurnPoint["TurnPointLon"].toInt();
    }
    return turnPointLon;
}

void FlightTurnPoint::setTurnPointLon(const int &value)
{
    turnPointLon = value;
    jsonFlightTurnPoint["TurnPointLon"] = value;
}

int FlightTurnPoint::getTurnPointLat()
{
    turnPointLat = 0;
    if (jsonFlightTurnPoint.contains("TurnPointLat") && jsonFlightTurnPoint["TurnPointLat"].isDouble()) {
        turnPointLat = jsonFlightTurnPoint["TurnPointLat"].toInt();
    }
    return turnPointLat;
}

void FlightTurnPoint::setTurnPointLat(const int &value)
{
    turnPointLat = value;
    jsonFlightTurnPoint["TurnPointLat"] = value;
}


int FlightTurnPoint::getTurnPointNumber()
{
    turnPointNumber = 0;
    if (jsonFlightTurnPoint.contains("TurnPointNumber") && jsonFlightTurnPoint["TurnPointNumber"].isDouble()) {
        turnPointNumber = jsonFlightTurnPoint["TurnPointNumber"].toInt();
    }
    return turnPointNumber;
}

void FlightTurnPoint::setTurnPointNumber(const int value)
{
    turnPointNumber = value;
    jsonFlightTurnPoint["TurnPointNumber"] = value;
}



