/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FLIGHTTURNPOINT_H
#define FLIGHTTURNPOINT_H


#include <QString>
#include <QJsonObject>

class FlightTurnPoint

{
public:

    QString turnPointName; //start, finish, TP
    QString turnPointTime;
    QString turnPointLocation;
    QString turnPointType;
    int turnPointAltitude;
    int turnPointLon;
    int turnPointLat;
    int turnPointNumber;

    QJsonObject jsonFlightTurnPoint;

    FlightTurnPoint();
    FlightTurnPoint(QJsonObject t_jsonFlightTurnPoint);

    QString getTurnPointName();
    void setTurnPointName(const QString &value);

    QString getTurnPointTime();
    void setTurnPointTime(const QString &value);

    QString getTurnPointLocation();
    void setTurnPointLocation(const QString &value);

    QString getTurnPointType();
    void setTurnPointType(const QString &value);

    int getTurnPointAltitude();
    void setTurnPointAltitude(const int &value);

    int getTurnPointLon();
    void setTurnPointLon(const int &value);

    int getTurnPointLat();
    void setTurnPointLat(const int &value);

    int getTurnPointNumber();
    void setTurnPointNumber(const int value);

};

#endif // FLIGHTTURNPOINT_H
