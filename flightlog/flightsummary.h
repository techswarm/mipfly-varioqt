/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FLIGHTSUMMARY_H
#define FLIGHTSUMMARY_H

#include <string>
#include <QJsonObject>
#include <QJsonDocument>
#include <QTime>
#include "flightturnpoint.h"

class FlightSummary
{


public:
    int lognumber;
    std::string filename;
    FlightSummary();

    FlightSummary(QJsonObject jsonFlightSummar);

    QString getPilot();
    void setPilot(const QString &value);

    QString getGliderType();
    void setGliderType(const QString &value);

    QString getGliderId();
    void setGliderId(const QString &value);

    QString getCompetitionId();
    void setCompetitionId(const QString &value);

    QString getCompetitionClass();
    void setCompetitionClass(const QString &value);

    double getMaxClimb() ;
    void setMaxClimb(const double &value);

    qreal getMaxSink();
    void setMaxSink(const qreal &value);

    int getMaxAltitude();
    void setMaxAltitude(int value);

    int getMaxAltitudeGain();
    void setMaxAltitudeGain(const int &value);

    int getDistance();
    void setDistance(const int &value);

    QString getStartTime();
    void setStartTime(const QString &value);

    QString getStartLocation();
    void setStartLocation(const QString &value);

    int getStartAltitude();
    void setStartAltitude(const int &value);

    int getStartLon();
    void setStartLon(int value);

    long getStartLat();
    void setStartLat(long value);

    QString getLandingTime();
    void setLandingTime(const QString &value);

    QString getLandingLocation();
    void setLandingLocation(const QString &value);

    int getLandingAltitude();
    void setLandingAltitude(const int &value);

    int getLandingLon();
    void setLandingLon(int value);

    int getLandingLat();
    void setLandingLat(int value);


    std::string getJsonFlightSummaryString();




    void newFlightSummary();
    QTime getFlightDuraton();


    void addTurnpoint(FlightTurnPoint *t_turnpoint);

private:

    QString pilot;
    QString gliderType;
    QString gliderId;
    QString competitionId;
    QString competitionClass;

    double maxClimb;
    double maxSink;
    int maxAltitude;
    int maxAltitudeGain;
    int Distance;

    QString startTime;
    QString startLocation;
    int startAltitude;
    int startLon;
    int startLat;

    QString landingTime;
    QString landingLocation;
    int landingAltitude;
    int landingLon;
    int landingLat;

    QTime flightDuraton;





};

#endif // FLIGHTSUMMARY_H
