/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <sstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>
#include <cstdio>
#include <iomanip>


#include <QtDebug>
#include <QSettings>
#include <QFile>
#include <QTextStream>
#include <QThread>


#include "flightsummarylog.h"
#include "flightsummary.h"
#include "utils/utils.h"
#include "utils/vario_common.h"
#include "Geo/locationsservice.h"

static const std::string BASELOGPATH (".");

void FlightSummaryLog::newflightsummarylog(FlightSummary *flightsummary)
{
    std::string ext = "json";
    evalFlightNumber("IGC");
    flightsummary->filename = getLogFilePath(ext);
    createDirIfNotExists(); //TODO: create common CreateDirNotExist

    if (summarylogfs.is_open()) {
        summarylogfs.close();
    }

    summarylogfs.open(flightsummary->filename);

    if(!summarylogfs) {
        std::cerr << "error opening Log summary file for writing: " << flightsummary->filename << std::endl;
        perror("ostream::open ");
    }

    summarylogfs.close();

}

FlightSummary FlightSummaryLog::getFlightSummaryLog(QString filename) {

    QString val;
    QFile jsonfile;
    jsonfile.setFileName(filename);
    jsonfile.open(QIODevice::ReadOnly | QIODevice::Text);
    val = jsonfile.readAll();
    jsonfile.close();
    qWarning() << val;
    QJsonDocument d = QJsonDocument::fromJson(val.toUtf8());
    return FlightSummary(d.object());

}

static std::string filenameDate() {
    time_t now = time(NULL);
    struct tm *time = localtime(&now);
    char buff[11];
    sprintf(buff, "%.4d-%.2d-%.2d", time->tm_year + 1900, time->tm_mon + 1, time->tm_mday);
    return std::string(buff);
}

void FlightSummaryLog::writeSummaryLogFile(FlightSummary *flightsummary) {

    if (summarylogfs.is_open()) {
        summarylogfs.close();
    }

    summarylogfs.open(flightsummary->filename, std::ofstream::out | std::ofstream::trunc);

    if(summarylogfs) {

        summarylogfs << flightsummary->getJsonFlightSummaryString();
        summarylogfs.close();
    }

}


std::__cxx11::string FlightSummaryLog::getLogFilePath(std::string extention) {
    std::ostringstream ss;
    ss << BASELOGPATH << "/flights/" << timestampStr() << '/' << filenameDate();


        const Vario::Location &crtLoc = LocationsService().currentLocation();
        ss << '-';
        if (!crtLoc.name.isEmpty()) {
            QByteArray ba = crtLoc.name.toLocal8Bit();
            ss << ba.data();
        } else {
            ss << "NO-LOCATION";
        }


    ss << '-' << getFlightNumberStr() << "." << extention;

     return ss.str();

}


std::string FlightSummaryLog::getFlightNumberStr() {
    if (flightNumber <= 9 || flightNumber > 35)
        return std::to_string(flightNumber);
    else {
        char c = flightNumber + 55;
        return std::string(&c);
    }
}

void FlightSummaryLog::evalFlightNumber(std::string extention)
{
    flightNumber = 1;
    while(std::ifstream(getLogFilePath(extention).c_str())) {
        flightNumber++;
    }
    flightNumber --; //new IGC file allready exist
}



void FlightSummaryLog::createDirIfNotExists() {
    std::ostringstream ss;
    ss << BASELOGPATH << "/flights";

    if (mkdirIfNotExists(ss.str()) < 0) {
        char *errnoDesc = strerror(errno);
        qCritical("error creating directory %s (%s)", ss.str().c_str(), errnoDesc);
    }

    if (mkdirIfNotExists(ss.str()) < 0) {
        char *errnoDesc = strerror(errno);
        qCritical("error creating directory %s (%s)", ss.str().c_str(), errnoDesc);
    }

    ss << '/' << timestampStr();

    if (mkdirIfNotExists(ss.str()) < 0) {
        char *errnoDesc = strerror(errno);
        qCritical("error creating directory %s (%s)", ss.str().c_str(), errnoDesc);
    }
}
