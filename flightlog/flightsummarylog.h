/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FLIGHTSUMMARYLOG_H
#define FLIGHTSUMMARYLOG_H

#include <string>
#include <qlist.h>

#include <fstream>
#include <string>
#include <QString>

#include "flightsummary.h"

class FlightSummaryLog
{
    std::ofstream summarylogfs;
    QString logsummaryfilename;
    int flightNumber = 0;
    std::string getLogFilePath(std::string extention);
    std::string getFlightNumberStr();
    void evalFlightNumber(std::string extention);

public:
    void newflightsummarylog(FlightSummary *flightsummary);

    void writeSummaryLogFile(FlightSummary *flightsummary);
    FlightSummary getFlightSummaryLog(QString filename);
private:


    void createDirIfNotExists();
};

#endif // FLIGHTSUMMARYLOG_H
