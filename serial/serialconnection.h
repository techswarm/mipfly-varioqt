/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SERIALCONNECTION_H
#define SERIALCONNECTION_H

#include <cstdint>
#include <string>

class SerialConnection
{
    int fd;
public:
    SerialConnection(const char *port, int baudRate);
    ~SerialConnection();

    void putc(uint8_t c) const;
    uint8_t getc() const;
    void puts(std::string str) const;

    void close() const;

    void setBaudRate(int baudRate) const;
    void flush() const;
    int numBytesAvailableRead() const;
};

#endif // SERIALCONNECTION_H
