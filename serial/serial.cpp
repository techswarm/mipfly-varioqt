/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <stdio.h>
#include <unistd.h>  //Used for UART
#include <fcntl.h>   //Used for UART
#include <termios.h> //Used for UART
#include "serial.h"
#include <stdlib.h>
#include <sys/ioctl.h>

#define devicename "/dev/ttyUSB0"

using namespace std;

int SerialInit(const char* ComPortName, long Baud_Rate)
{

	//-------------------------
	//----- SETUP USART 0 -----
	//-------------------------
	// At bootup, pins 8 and 10 are already set to UART0_TXD, UART0_RXD (ie the alt0 function) respectively

	// OPEN THE UART
	// The flags (defined in fcntl.h):
	//	Access modes (use 1 of these):
	//		O_RDONLY - Open for reading only.
	//		O_RDWR - Open for reading and writing.
	//		O_WRONLY - Open for writing only.
	//
	//	O_NDELAY / O_NONBLOCK (same function) - Enables nonblocking mode. When set read requests on the file can return
	//immediately with a failure status
	//											if there is no input immediately available (instead of blocking). Likewise, write requests
	//can also return
	//											immediately with a failure status if the output can't be written
	//immediately.
	//
	//	O_NOCTTY - When set and path identifies a terminal device, open() shall not cause the terminal device to become
	//the controlling terminal for the process.
    int uart0_filestream = open(ComPortName, O_RDWR | O_NOCTTY); // Open in non blocking read/write mode
    int rety = 10;
    while(uart0_filestream == -1)
    {
        uart0_filestream = open(ComPortName, O_RDWR | O_NOCTTY);
        rety -- ;
        sleep(1);
    }
	if(uart0_filestream == -1) {
		// ERROR - CAN'T OPEN SERIAL PORT
		printf("Error - Unable to open UART:%s  Ensure it is not in use by another application\n", ComPortName);
		exit(1);
	}

	// CONFIGURE THE UART
	// The flags (defined in /usr/include/termios.h - see
	// http://pubs.opengroup.org/onlinepubs/007908799/xsh/termios.h.html):
	//	Baud rate:- B1200, B2400, B4800, B9600, B19200, B38400, B57600, B115200, B230400, B460800, B500000, B576000,
	//B921600, B1000000, B1152000, B1500000, B2000000, B2500000, B3000000, B3500000, B4000000
	//	CSIZE:- CS5, CS6, CS7, CS8
	//	CLOCAL - Ignore modem status lines
	//	CREAD - Enable receiver
	//	IGNPAR = Ignore characters with parity errors
	//	ICRNL - Map CR to NL on input (Use for ASCII comms where you want to auto correct end of line characters - don't
	//use for bianry comms!)
	//	PARENB - Parity enable
	//	PARODD - Odd parity (else even)
	struct termios options;
	tcgetattr(uart0_filestream, &options);
	options.c_cflag = Baud_Rate | CS8 | CLOCAL | CREAD; //<Set baud rate
	options.c_iflag = IGNPAR;
	options.c_oflag = 0;
	options.c_lflag = 0;
	tcflush(uart0_filestream, TCIFLUSH);
	tcsetattr(uart0_filestream, TCSANOW, &options);

	return uart0_filestream;
} // end of main

void SetBaudRate(HANDLE uart0_filestream, long Baud_Rate) {
    struct termios options;
    tcgetattr(uart0_filestream, &options);
    options.c_cflag = Baud_Rate | CS8 | CLOCAL | CREAD; //<Set baud rate
    tcflush(uart0_filestream, TCIFLUSH);
    tcsetattr(uart0_filestream, TCSANOW, &options);
}

// write(fd,&Key,1);
// restore old port settings
// tcsetattr(fd,TCSANOW,&oldtio);
// close(fd); //close the com port

void SerialClose(HANDLE fd)
{
	close(fd);
}

int SerialGetNumBytes(HANDLE fd)
{
	int bytes_avaiable;
	ioctl(fd, FIONREAD, &bytes_avaiable);
	return bytes_avaiable;
}

uint8 SerialGetc(HANDLE fd)
{
	/*
	    uint8 rxchar[2];
	//    read(fd,&rxchar,1);
	read(fd, (void*)rxchar, 1);
	//return rxchar[0];
	return 61;
	*/
	unsigned char rx_buffer2[2];
	//----- CHECK FOR ANY RX BYTES -----

	// Read up to 255 characters from the port if they are there

	int rx_length = read(fd, (void*)rx_buffer2, 1); // Filestream, buffer to store in, number of bytes to read (max)
	if(rx_length < 0) {
		// printf("An error occured (will occur if there are no bytes)\n");
		// usleep(1000*100);
	} else if(rx_length == 0) {
		printf("No data waiting\n");
	} else {
		// Bytes received
		// rx_buffer2[rx_length] = '\0';
		// printf("%i bytes read : %s\n", rx_length, rx_buffer2);
	}
	return rx_buffer2[0];
}

void SerialPutc(HANDLE fd, uint8 txchar)
{
	write(fd, &txchar, 1);
}

#include <QtDebug>

void SerialPuts(HANDLE hCom, const char txchar[])
{
	int i;
	for(i = 0; txchar[i] != 0; i++) {
        //qDebug() << txchar[i];
		write(hCom, txchar + i, 1);
	}
}

void SerialPuth(HANDLE hCom, unsigned char txchar[], int length)
{
	int i;
	for(i = 0; i < length; i++) {
		write(hCom, txchar + i, 1);
		// printf("%x ",txchar[i]);
	}
	// printf("\n");
}
