/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "serialconnection.h"

#include <termios.h>

#include "serial/serial.h"

#include <iostream>

int baudRateFromInt(int baudRate) {
    if (baudRate == 9600) {
        return B9600;
    } else if (baudRate == 115200) {
        return B115200;
    }
    return B0;
}

SerialConnection::SerialConnection(const char *port, int baudRate)
{
#ifdef USE_SERIAL
    fd =  SerialInit(port, baudRateFromInt(baudRate));
#endif
}

SerialConnection::~SerialConnection() {
#ifdef USE_SERIAL
    SerialClose(fd);
#endif
}

void SerialConnection::putc(uint8_t c) const {
    std::cout << c;
#ifdef USE_SERIAL
    SerialPutc(fd, c);
#endif
}

void SerialConnection::puts(std::__cxx11::string str) const {
#ifdef USE_SERIAL
    SerialPuts(fd, str.c_str());
#endif
}

void SerialConnection::close() const
{
#ifdef USE_SERIAL
    SerialClose(fd);
#endif
}

uint8_t SerialConnection::getc() const {
#ifdef USE_SERIAL
    return SerialGetc(fd);
#else
    return 0;
#endif
}

void SerialConnection::setBaudRate(int baudRate) const {
    SetBaudRate(fd, baudRateFromInt(baudRate));
}

void SerialConnection::flush() const {
    tcflush(fd, TCIOFLUSH);
}

int SerialConnection::numBytesAvailableRead() const
{
#ifdef USE_SERIAL
    return SerialGetNumBytes(fd);
#else
    return 0;
#endif
}
