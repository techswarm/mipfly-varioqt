/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * serial.h
 *
 *  Created on: Mar 13, 2015
 *      Author: user
 */

#ifndef SERIAL_H_
#define SERIAL_H_

typedef unsigned char uint8;
typedef int HANDLE;

int SerialInit(const char *ComPortName, long BaudRate);
uint8 SerialGetc(HANDLE fd);
void SerialPutc(HANDLE fd, uint8 txchar);
void SerialPuts(HANDLE hCom,const  char txchar[]);
void SerialClose(HANDLE fd);
void SerialPuth(HANDLE hCom, unsigned char txchar[], int length);
int SerialGetNumBytes(HANDLE fd);

void SetBaudRate(HANDLE uart0_filestream, long BaudRate);

#endif /* SERIAL_H_ */
