/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NUMERICINPUT_H
#define NUMERICINPUT_H

#include <QDialog>

namespace Ui {
class NumericInput;
}

class NumericInput : public QDialog
{
    Q_OBJECT

public:
    explicit NumericInput(QWidget *parent = 0);
    ~NumericInput();

    float value() const;
    void setValue(float value);

    int step() const;
    void setStep(float step);

    int max() const;
    void setMax(int max);

    void setMin(int min);

    void setCaption(QString caption);

    float displayScale() const;
    void setDisplayScale(float displayScale);

    void setPrecision(int value);

signals:
    void valueChanged(float value);

public slots:
    void gpioPressed(int btn);
    void gpioLongPressed(int btn);

private:
    Ui::NumericInput *ui;

    void showEvent(QShowEvent *e) Q_DECL_OVERRIDE;
    void hideEvent(QHideEvent *e) Q_DECL_OVERRIDE;

    int precision = 0;

    float value_;
    float step_;
    int max_;
    int min_;
    float displayScale_;

};

#endif // NUMERICINPUT_H
