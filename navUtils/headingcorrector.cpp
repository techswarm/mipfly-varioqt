/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "headingcorrector.h"

#include <QtMath>
#include <cmath>
#include <sstream>
#include <iomanip>
#include <iostream>

#include "serial/serialconnection.h"
#include "NMEA/NMEAParser.h"

HeadingCorrector::HeadingCorrector(const NmeaParser &parser, QObject *parent) : QObject(parent)
{
    connect(&parser, SIGNAL(speedKmph(qreal)), this, SLOT(setSpeed(qreal)));
    connect(&parser, SIGNAL(course(qreal)), this, SLOT(setHeading(qreal)));
}

void HeadingCorrector::update(qreal heading, qreal speed) {
    qreal angleDiff = headingError(prevHeading, heading);
    prevHeading = heading;
    qreal angVelocity = qDegreesToRadians(angleDiff) * dt;

    qreal a = speed * angVelocity;
    qreal theta;


//    a = 100.0;

//    qDebug("UPDATE a %f, angVelocity %f", a, angleDiff * dt);

    if (a < 0) {
        theta = qDegreesToRadians(heading) + M_PI_2;
    } else {
        theta = qDegreesToRadians(heading) - M_PI_2;
    }

    _ax = qAbs(a) * qCos(theta);
    _ay = qAbs(a) * qSin(theta);

//    qDebug("angleDiff %f, theta %f, abs(a) %f, ax %f, ay %f", angleDiff, theta, qAbs(a), _ax, _ay);
}


void HeadingCorrector::setSpeed(qreal speed) {
    qreal speedMps = speed * 1000 / 3600;
    lastSpeed = speedMps;

    if (!speedInit) {
//        qDebug("INIT speed %f", speedMps);
        speedInit = true;
    }
    if (headingInit) {
//        qDebug("UPDATE CRTR with speed %f, lastHeading %f", speedMps, lastHeading);
        update(lastHeading, speedMps);
    }
}

void HeadingCorrector::setHeading(qreal heading) {
    if (!headingInit) {
//        qDebug("INIT heading %f", heading);
        prevHeading = heading;
        headingInit  = true;
    }
    if (speedInit) {
        lastHeading = heading;
//        qDebug("UPDATE CRTR with lastSpeed %f, heading %f", lastSpeed, heading);
        update(heading, lastSpeed);
    }
}

qreal HeadingCorrector::ax() {
    return _ax;
}

qreal HeadingCorrector::ay() {
    return _ay;
}

qreal HeadingCorrector::headingError(qreal initial, qreal final) {
      initial += 1000;
      final += 1000;

      bool flipped = false;
      if (initial > final)
      {
          double temp;
          temp = final;
          final = initial;
          initial = temp;
          flipped = true;
      }
      double error;
      if (final - initial > 180)
          final = final - 360;

      error = final - initial;

      if (flipped == true)
          error = -error;
      return error;
}
