/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "hgtcontainer.h"
#include "navUtils/coord.hpp"
#include "navUtils/elevation.h"
#include "VarioQt.h"
#include <QtConcurrent/QtConcurrent>


HGTContainer::HGTContainer(QObject *parent) : QObject(parent)
{
    computeMatrix();

    oneMinuteTimer.setInterval(60000);
    connect(&oneMinuteTimer,SIGNAL(timeout()),this,SLOT(oneMinuteTick()));
    oneMinuteTimer.start();

    connect(&computeWtcher,SIGNAL(finished()),this,SLOT(computeFinished()));
}

int HGTContainer::getYOffset(Coord target)
{
    lastTarget.setLat(target.getLat());
    lastTarget.setLon(target.getLon());

    double dinstance = center.distanceMLat(target);
    int offset = dinstance / metersPerPixel;

    if(center.getLat() > target.getLat())offset *= -1;
    return offset+500;
}

int HGTContainer::getXOffset(Coord target)
{
    double dinstance = center.distanceMLon(target);
    int offset = dinstance / metersPerPixel;

    if(center.getLon() > target.getLon())offset *= -1;
    return offset+500;
}

int HGTContainer::getHeight(int x, int y)
{
    if(x>0 && x<1000 && y>0 && y<1000)
    {
        return matrix[y][x];
    }
    else
    {
        //invalidate matrix
        invalidateMatrix = true;
        return 5000;
    }
}

void HGTContainer::validateCenter(Coord target)
{
    if(!computeFuture.isFinished())return;
    int xOffset = getXOffset(target);
    int yOffset = getYOffset(target);

    if(xOffset<200 || xOffset>800 || yOffset<200 || yOffset>800)
    {
        computeCenter.setLat(computeCenter.getLat());
        computeCenter.setLon(computeCenter.getLon());
        invalidateMatrix = true;
    }
}

void HGTContainer::computeMatrix()
{
    if(computeWtcher.isFinished())
    {
        computeFuture = QtConcurrent::run(this,&HGTContainer::computeMatrixWorker);
        computeWtcher.setFuture(computeFuture);
    }
    else
    {
        invalidateMatrix = true;
    }
}

void HGTContainer::computeMatrixWorker()
{
    //determine 00 coordonate
    //for this we translate it 500 pixels at 270 + 45 degrees
    qDebug()<<"Compute hgt Matrix";
    Coord zeroZeroCoord(computeCenter);
    zeroZeroCoord.translateTo(500*1.4142135 * metersPerPixel,315 * 0.0174532925);

    //determine lat and lon increments
    double latDelta = (computeCenter.getLat() - zeroZeroCoord.getLat())/500;
    double lonDelta = (computeCenter.getLon() - zeroZeroCoord.getLon())/500;

    valid = true;

    Elevation *pEl = VarioQt::instance().getElevation();
    int16_t localMatrix[1000][1000];
    for(int i=0;i<1000;i++)
    {
        for(int j=0;j<1000;j++)
        {
            Coord localPoint(zeroZeroCoord.getLat()+latDelta*i,
                             zeroZeroCoord.getLon()+lonDelta*j);
            //qDebug()<<localPoint.getLatStr()<<" "<<localPoint.getLonStr();
            int localElevation = pEl->getElevation(localPoint);
            if(localElevation == -1000)
            {
                valid = false;
                invalidateMatrix=true;
                qDebug()<<"Compute hgt Matrix done";
                goto nestedLoopExit;
            }
            localMatrix[i][j]=localElevation;
        }
    }
    nestedLoopExit:
    qDebug()<<"Compute hgt Matrix done";
    if(valid==false)invalidateMatrix=true;

    center.setLat(computeCenter.getLat());
    center.setLon(computeCenter.getLon());
    for(int i=0;i<1000;i++)
    {
        for(int j=0;j<1000;j++)
        {
            matrix[i][j]=localMatrix[i][j];
        }
    }
}

bool HGTContainer::getValid() const
{
    return valid;
}

void HGTContainer::oneMinuteTick()
{
    if(invalidateMatrix)
    {
        computeCenter.setLat(lastTarget.getLat());
        computeCenter.setLon(lastTarget.getLon());
        invalidateMatrix = false;
        computeMatrix();
    }
}

void HGTContainer::computeFinished()
{

}
