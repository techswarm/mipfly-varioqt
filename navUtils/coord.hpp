/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef COORDONATES_HPP
#define COORDONATES_HPP

class Latitude;
class Longitude;

#include <QString>

enum cardinalDir
{
    North,
    South,
    East,
    Vest
};

enum distanceMethods
{
    FAICircle,
    WGS84,
    None
};

class Coord
{
private:
	double _lat;
	double _lon;
        int _snaptime;
        int _gpsHeight;
public:
        static distanceMethods distanceMethod_;
        static void setDistanceMethod(distanceMethods dm);
        static distanceMethods distanceMethod();


        enum CoordInputFormat{Decimal,DMS};

	Coord();
        Coord(const Coord &other);
        Coord(const Latitude &lat, const Longitude &lon);
	~Coord();
	double distanceKm(const Coord &to);
        double distanceKmWGS84(const Coord &to);
        double distanceKmAuto(const Coord &to);

        static int getDegrees(double coordonate);
        static int getMinutes(double coordonate);
        static int getSeconds(double coordonate);
        static double getCoord(int degrees, int minutes, int seconds);

	double distanceMLat(const Coord &to);
	double distanceMLon(const Coord &to);
        double bearingTo(const Coord &to);
        void translateTo(double distancem, double bearing);
        void translateToWGS84(double distancem, double bearing);
        void translateToAuto(double distancem, double bearing);
	
	double distanceMi(const Coord &to);

	void setLat(double lat);
	void setLon(double lon);

        double getLat() const;
        double getLon() const;

        QString getLatStr();
        QString getLonStr();

        void revertLat();
        void revertLon();

        int getSnaptime() const;
        void setSnaptime(int snaptime);
        int getGpsHeight() const;
        void setGpsHeight(int gpsHeight);
};
QDataStream &operator<<(QDataStream &out, const Coord &painting);
QDataStream &operator>>(QDataStream &in, Coord &painting);

#endif // COORDONATES_HPP
