/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VARIOCOMPUTE_H
#define VARIOCOMPUTE_H

#include <QObject>

const int BUFLENMAX = 3000;

class NmeaParser;

class VarioCompute: public QObject
{
    Q_OBJECT
public:
    VarioCompute(const NmeaParser &parser, QObject *parent = 0);
    int getBuflen() const;
    qreal getAverageCmps();

    void reset();

    float getAverageClimb();
    float getAverageSink();

public slots:
    void pushCmps(int cmps);
    void increaseAverageSeconds();
    void decreaseAverageSeconds();

signals:
    void averageMax(qreal);
    void averageMin(qreal);
    void average(qreal);

private:
    void saveSetting();
    void loadSetting();

    int32_t totalClimbCm = 0;
    int32_t total200msClimbTicks = 0;

    int32_t totalSinkCm = 0;
    int32_t total200msSinkTicks = 0;

    int buflen = 100;
    qreal _averageMax = 0;
    qreal _averageMin = 0;
    qreal _average = 0;

    int _cmpsBuf[BUFLENMAX];
    int _cmpsBufPos = 0;
    int _sum = 0;

    bool _reset = false;

    void compute();
};

#endif // VARIOCOMPUTE_H
