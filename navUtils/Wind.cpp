/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Wind.hpp"

#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>

#include <unistd.h>

#include "circRegression/circle.h"
#include "utils/vario_common.h"
#include "settings/windmanager.h"
#include "NMEA/NMEAParser.h"

#include <QSettings>
#include <QDebug>

#define d2r (M_PI / 180.0)

void Wind::push(qreal flightSpeed, qreal flightHeading)
{
    qreal rad = flightHeading * d2r;
    //flightSpeed = 45;
    pushWindXY(flightSpeed * cos(rad),flightSpeed * sin(rad));
    if(processPostscale--<0)
    {
        Process();
        processPostscale = 25;
    }
}

float Wind::getWindDir()
{
    float windDir = atan2(_windSpeedY,-_windSpeedX) * 180 / M_PI;
    if(windDir<0)windDir+=360;
    return windDir;
    //return 180;
}
float Wind::getWindSpeed()
{
    return sqrt(_windSpeedX*_windSpeedX+_windSpeedY*_windSpeedY);

}

void Wind::pushWindXY(qreal speedX,qreal speedY)
{
    _speedData.X[_bufPos] = speedX;
    _speedData.Y[_bufPos] = speedY;
    _bufPos++;
    if(_bufPos >= _speedData.n)_bufPos = 0;
}


Wind::Wind(int numReadings):_speedData(numReadings)
{

}

Wind::~Wind()
{

}

Wind::Wind(const NmeaParser &parser, const WindManager &mgr, QObject *parent) : QObject(parent)
{
    int num = mgr.getNumReadings();
    _dataSpreed = mgr.getDataSpread();
    _numAverage = mgr.getNumAverage();

    _rmsError = mgr.getRmsError();

    _speedData.setNum(num);

    connect(&parser, SIGNAL(speedAndCourse(qreal,qreal)), this, SLOT(push(qreal,qreal)));
    connect(&mgr, SIGNAL(numRdChanged(int)), this, SLOT(setNumReadings(int)));
    connect(&mgr, SIGNAL(numAvgChanged(int)), this, SLOT(setNumAverage(int)));
    connect(&mgr, SIGNAL(datSpdChanged(int)), this, SLOT(setDataSpread(int)));
    connect(&mgr, SIGNAL(rmsErrorChanged(float)), this, SLOT(setRmsError(float)));
}

float Wind::determineSpread()
{
    float minX,minY,maxX,maxY;

    minX = _speedData.X[0];
    maxX = _speedData.X[0];
    minY = _speedData.Y[0];
    maxY = _speedData.Y[0];

    for(int i=1; i<_speedData.n; i++) {
        if(_speedData.X[i]<minX)minX = _speedData.X[i];
        else if(_speedData.X[i]>maxX)maxX = _speedData.X[i];

        if(_speedData.Y[i]<minY)minY = _speedData.Y[i];
        else if(_speedData.Y[i]>maxY)maxY = _speedData.Y[i];
    }

    float difX = fabs(maxX - minX);
    float difY = fabs(maxY - minY);

    //float difMin;
    //float difMax;

    //if(difX>difY) {
    //	difMax = difX;
    //	difMin = difY;
    //} else {
    //	difMax = difY;
    //	difMin = difX;
    //}

    //if(difMin/difMax<0.2)difMax = 0;
    return difX + difY;
}

bool Wind::dataSetValidAfterWindCompute(float x, float y,float speed)
{
    //this function is broken
    int totalDeviation = 0;

    for(int i=0;i<=_speedData.n;i++)
    {
        //place each point in a certain quadrant
        int speedX = _speedData.X[i] - x;
        int speedY = _speedData.Y[i] - y;
        totalDeviation += speedX*speedX+speedY*speedY;
    }

    qDebug()<<"sqrt(totalDeviation)"<<sqrt(totalDeviation);

    if(sqrt(totalDeviation)<_rmsError)
        return true;
    return false;
}

int Wind::Process()
{
    Circle circleIni(0,0,40);
    Circle circle(0,0,0);
    reals LambdaIni=0.01;

    float spreed = determineSpread();

    //qDebug()<<"spreed="<<spreed;

    if(spreed<_dataSpreed) {
        //we should consider at least a spreed of x degrees
        return -1;
    }

    int code = CircleFitByLevenbergMarquardtReduced (_speedData,circleIni,LambdaIni,circle);
    //qDebug()<<"RMS:"<<circle.s;
    if(code == 0 && circle.r < 70 && circle.r > 25 && circle.s < _rmsError) {
        _windSpeedX = (circle.a + _windSpeedX*(_numAverage-1))/_numAverage;
        _windSpeedY = (circle.b + _windSpeedY*(_numAverage-1))/_numAverage;

        if(!qIsFinite(_windSpeedX))_windSpeedX = 0;
        if(!qIsFinite(_windSpeedY))_windSpeedY = 0;
        qreal dir = getWindDir();
        qreal speed = getWindSpeed();
        emit windDir(dir);
        emit windSpeed(speed);
    }
    //circle.print();
    return 0;
}

void Wind::windTester(int degree,float windSpeedX, float windSpeedY, int displayEvery)
{
    srand(time(NULL));
    while(displayEvery) {
        displayEvery -- ;
        //generate random heading limmited to degree variation
        float heading = (rand()%degree);
        float headingRad = heading * d2r;
        float speed = 40;

        speed += rand()%7;
        speed += rand()%7;

        float airspeedX = speed * cos(headingRad);
        float airspeedY = speed * sin(headingRad);

        pushWindXY(airspeedX+windSpeedX,airspeedY+windSpeedY);
        printf("heading %2.f\t speed %2.f \r\n",heading,speed);
    }
}

//caca copiat
/*
#ifdef WIND_TEST

clock_t begin = clock();

printf("start \r\n");

Wind wind(30);

wind.windTester(180,-10,10,40);
wind.Process();

printf("%f %f \r\n",wind.getWindSpeed(),wind.getWindDir());

clock_t end = clock();
double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;

printf("%lf s \r\n",time_spent);

return 0;
#endif
*/

void Wind::resetWindSpeed()
{
    _windSpeedX = 0;
    _windSpeedY = 0;
}

void Wind::setNumAverage(int num) {
    if(num == 0)
        num = 1;
    else
        _numAverage = num;
    resetWindSpeed();
}

void Wind::setDataSpread(int spread) {
    _dataSpreed = spread;
    resetWindSpeed();
}

void Wind::setNumReadings(int num) {
    _speedData.setNum(num);
    resetWindSpeed();
}

void Wind::setRmsError(float num) {
    _rmsError = num;
    resetWindSpeed();
}
