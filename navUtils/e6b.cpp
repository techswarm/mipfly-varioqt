/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "e6b.h"
#include <climits>

int E6B::invalid = INT_MAX;

E6B::E6B(int targetCourse,int trueAirSpeed, int windDir, int windSpeed)
{
    _targetCourse = targetCourse;
    _trueAirSpeed = trueAirSpeed;
    _windDir = windDir;
    _windSpeed = windSpeed;
    getSwc();
}

//setters
void E6B::setTargetCourse( int value) {
    _targetCourse = value;
    getSwc();
}
void E6B::setTrueAirSpeed( int value) {
    _trueAirSpeed = value;
    getSwc();
}
void E6B::setWindDirection( int value) {
    _windDir = value;
    getSwc();
}
void E6B::setWindSpeed( int value) {
    _windSpeed = value;
    getSwc();
}
//getters
int E6B::getTargetCourse() {
    return _targetCourse;
}
int E6B::getTrueAirSpeed() {
    return _trueAirSpeed;
}
int E6B::getWindDirection() {
    return _windDir;
}
int E6B::getWindSpeed() {
    return _windSpeed;
}

//get values
int E6B::getWindCorrectionAngle() {
    if(!valid)return invalid;
    double hd = getHd(),
            wd = getWd();
    return round(1 / C * (atan2(getWindSpeed() * sin(hd - wd), getTrueAirSpeed() - getWindSpeed() * cos(hd - wd)) * -1));
}

int E6B::getHeading() {
    if(!valid)return invalid;
    return round(1 / C * getHd());
}
int E6B::getGroundSpeed() {
    if(!valid)return invalid;
    return round(getTrueAirSpeed() * sqrt(1 - pow(getSwc(), 2)) - (getWindSpeed() * cos(getWd() - getCrs())));
}
