/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "coord.hpp"
#include <cmath>

#include "Geo/Geo.hpp"
#include <QString>
#include <QDataStream>
#include <QSettings>

#define d2r (M_PI / 180.0)
#define r2d (180.0 / M_PI)

distanceMethods Coord::distanceMethod_ = distanceMethods::None;

//calculate haversine distance for linear distance
double haversine_km(double lat1, double long1, double lat2, double long2)
{
    double dlong = (long2 - long1) * d2r;
    double dlat = (lat2 - lat1) * d2r;
    double a = pow(sin(dlat/2.0), 2) + cos(lat1*d2r) * cos(lat2*d2r) * pow(sin(dlong/2.0), 2);
    double c = 2 * atan2(sqrt(a), sqrt(1-a));
    double d = 6367 * c;

    return d;
}

double haversine_mi(double lat1, double long1, double lat2, double long2)
{
    double dlong = (long2 - long1) * d2r;
    double dlat = (lat2 - lat1) * d2r;
    double a = pow(sin(dlat/2.0), 2) + cos(lat1*d2r) * cos(lat2*d2r) * pow(sin(dlong/2.0), 2);
    double c = 2 * atan2(sqrt(a), sqrt(1-a));
    double d = 3956 * c;

    return d;
}

double vincenty_km(double lat1, double long1, double lat2, double long2)
{
    float a = 6378137,
            b = 6356752.3142,
            f = 1 / 298.257223563, // WGS-84 ellipsoid params
            L = (long2-long1) * d2r,
            U1 = atan((1 - f) * tan(lat1 * d2r)),
            U2 = atan((1 - f) * tan(lat2 * d2r)),
            sinU1 = sin(U1),
            cosU1 = cos(U1),
            sinU2 = sin(U2),
            cosU2 = cos(U2),
            lambda = L,
            lambdaP,
            iterLimit = 10;
    float cosSigma,
            sigma ,
            sinAlpha ,
            cosSqAlpha,
            cos2SigmaM,
            C,sinLambda,cosLambda,sinSigma;

    do {
        sinLambda = sin(lambda);
        cosLambda = cos(lambda);
        sinSigma = sqrt((cosU2 * sinLambda) * (cosU2 * sinLambda) + (cosU1 * sinU2 - sinU1 * cosU2 * cosLambda) * (cosU1 * sinU2 - sinU1 * cosU2 * cosLambda));
        if (0 == sinSigma) {
            return 0; // co-incident points
        };
        cosSigma = sinU1 * sinU2 + cosU1 * cosU2 * cosLambda;
        sigma = atan2(sinSigma, cosSigma);
        sinAlpha = cosU1 * cosU2 * sinLambda / sinSigma;
        cosSqAlpha = 1 - sinAlpha * sinAlpha;
        cos2SigmaM = cosSigma - 2 * sinU1 * sinU2 / cosSqAlpha;
        C = f / 16 * cosSqAlpha * (4 + f * (4 - 3 * cosSqAlpha));
        lambdaP = lambda;
        lambda = L + (1 - C) * f * sinAlpha * (sigma + C * sinSigma * (cos2SigmaM + C * cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM)));
    } while (fabsf(lambda - lambdaP) > 1e-12 && --iterLimit > 0);

    float uSq = cosSqAlpha * (a * a - b * b) / (b * b);
    float A = 1 + uSq / 16384.0 * (4096 + uSq * (-768 + uSq * (320 - 175 * uSq)));
    float B = uSq / 1024.0 * (256 + uSq * (-128 + uSq * (74 - 47 * uSq)));
    float deltaSigma = B * sinSigma * (cos2SigmaM + B / 4.0 * (cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM) - B / 6.0 * cos2SigmaM * (-3 + 4 * sinSigma * sinSigma) * (-3 + 4 * cos2SigmaM * cos2SigmaM)));
    float s = b * A * (sigma - deltaSigma);

    return s/1000;
}

double Coord::distanceKm(const Coord &to)
{
    //double hdist = haversine_km(_lat,_lon,to._lat,to._lon);
    //double vdist = vincenty_km(_lat,_lon,to._lat,to._lon);
    return haversine_km(_lat,_lon,to._lat,to._lon);
}

double Coord::distanceKmWGS84(const Coord &to)
{
    return vincenty_km(_lat,_lon,to._lat,to._lon);
}

double Coord::distanceKmAuto(const Coord &to)
{
    if(distanceMethod()==distanceMethods::WGS84)
    {
        return distanceKmWGS84(to);
    }
    else
    {
        return distanceKm(to);
    }
}

double Coord::distanceMLat(const Coord &to)
{
    double ret = haversine_km(_lat,_lon,to._lat,_lon)*1000;
    if(_lat > to._lat)return -ret;
    else return ret;
}

double Coord::distanceMLon(const Coord &to)
{
    double ret = haversine_km(_lat,_lon,_lat,to._lon)*1000;
    if(_lon > to._lon)return -ret;
    else return ret;
}

double Coord::bearingTo(const Coord &to)
{
    double dy = to._lat - this->_lat;
    double dx = cosf(M_PI/180*this->_lat)*(to._lon - this->_lon);
    double angle = atan2f(dx, dy);
    return angle;
}

void Coord::translateTo(double distancem, double bearing)
{
    //get distance on x and y
    double distanceX = distancem * cos(bearing);
    double distanceY = distancem * sin(bearing);

    double latDifference = distanceX / 111045;

    double _1DegreOfLongitude = cos(_lat * M_PI / 180) * 111045;

    double lonDifference = distanceY / _1DegreOfLongitude;

    _lat+=latDifference;
    _lon+=lonDifference;
}

void Coord::translateToWGS84(double distancem, double bearing)
{
    float a = 6378137,  f = 1.0/298.257223563;  // WGS-84 ellipsiod
    float b = (1-f)*a;;
    float s = distancem;
    float alpha1 = bearing;
    float sinAlpha1 = sin(alpha1);
    float cosAlpha1 = cos(alpha1);

    float tanU1 = (1-f) * tan(d2r * _lat);
    float cosU1 = 1 / sqrt((1 + tanU1*tanU1)), sinU1 = tanU1*cosU1;
    float sigma1 = atan2(tanU1, cosAlpha1);
    float sinAlpha = cosU1 * sinAlpha1;
    float cosSqAlpha = 1 - sinAlpha*sinAlpha;
    float uSq = cosSqAlpha * (a*a - b*b) / (b*b);
    float A = 1 + uSq/16384*(4096+uSq*(-768+uSq*(320-175*uSq)));
    float B = uSq/1024 * (256+uSq*(-128+uSq*(74-47*uSq)));
    float sinSigma,cosSigma,cos2SigmaM;

    float sigma = s / (b*A), sigmaP = 2*M_PI;
    int iterLimmit = 10;
    do{
        cos2SigmaM = cos(2*sigma1 + sigma);
        sinSigma = sin(sigma);
        cosSigma = cos(sigma);
        float deltaSigma = B*sinSigma*(cos2SigmaM+B/4*(cosSigma*(-1+2*cos2SigmaM*cos2SigmaM)-
                                                       B/6*cos2SigmaM*(-3+4*sinSigma*sinSigma)*(-3+4*cos2SigmaM*cos2SigmaM)));
        sigmaP = sigma;
        sigma = s / (b*A) + deltaSigma;
    }while(fabsf(sigma-sigmaP) > 1e-12 && --iterLimmit>0);

    float tmp = sinU1*sinSigma - cosU1*cosSigma*cosAlpha1;
    float lat2 = atan2(sinU1*cosSigma + cosU1*sinSigma*cosAlpha1,
                       (1-f)*sqrt(sinAlpha*sinAlpha + tmp*tmp));
    float lambda = atan2(sinSigma*sinAlpha1, cosU1*cosSigma - sinU1*sinSigma*cosAlpha1);
    float C = f/16*cosSqAlpha*(4+f*(4-3*cosSqAlpha));
    float L = lambda - (1-C) * f * sinAlpha *
            (sigma + C*sinSigma*(cos2SigmaM+C*cosSigma*(-1+2*cos2SigmaM*cos2SigmaM)));

    //float revAz = atan2(sinAlpha, -tmp);  // final bearing

    _lat = lat2 * r2d;
    _lon = _lon + L * r2d;
}

void Coord::translateToAuto(double distancem, double bearing)
{
    if(distanceMethod()==distanceMethods::WGS84)
    {
        translateToWGS84(distancem,bearing);
    }
    else
    {
        translateTo(distancem,bearing);
    }
}

double Coord::distanceMi(const Coord &to)
{
    return haversine_mi(_lat,_lon,to._lat,to._lon);
}


void Coord::setLat(double lat)
{
    _lat = lat;
}

void Coord::setLon(double lon)
{
    _lon = lon;
}

double Coord::getLat() const
{
    return _lat;
}
double Coord::getLon() const
{
    return _lon;
}

QString Coord::getLatStr()
{
    QSettings settings;
    CoordInputFormat format = (Coord::CoordInputFormat)settings.value("coordFormat",Coord::Decimal).toUInt();

    QString valueStr;

    if(_lat >=0)valueStr += "N";
    else valueStr += "S";
    if(format == Decimal)
    {
        valueStr += QString("%1").arg(fabs(_lat),8,'f',4,'0');
        valueStr[1]=' ';
        return valueStr;
    }
    if(format == Coord::CoordInputFormat::DMS)
    {
        valueStr += QString::number(Coord::getDegrees(_lat)).rightJustified(3,'0');
        valueStr += "°";
        valueStr += QString::number(Coord::getMinutes(_lat)).rightJustified(2,'0');
        valueStr += "'";
        valueStr += QString::number(Coord::getSeconds(_lat)).rightJustified(2,'0');
        valueStr += "\"";
        valueStr[1]=' ';
        return valueStr;
    }
    return QString();
}

QString Coord::getLonStr()
{
    QSettings settings;
    CoordInputFormat format = (Coord::CoordInputFormat)settings.value("coordFormat",Coord::Decimal).toUInt();

    QString valueStr;
    if(_lon >=0)valueStr += "E";
    else valueStr += "W";

    if(format == Decimal)
    {
        valueStr += QString("%1").arg(fabs(_lon),8,'f',4,'0');
        return valueStr;
    }
    if(format == Coord::CoordInputFormat::DMS)
    {
        valueStr += QString::number(Coord::getDegrees(_lon)).rightJustified(3,'0');
        valueStr += "°";
        valueStr += QString::number(Coord::getMinutes(_lon)).rightJustified(2,'0');
        valueStr += "'";
        valueStr += QString::number(Coord::getSeconds(_lon)).rightJustified(2,'0');
        valueStr += "\"";
        return valueStr;
    }
    return QString();
}

void Coord::revertLat()
{
    _lat = _lat * -1;
}

void Coord::revertLon()
{
    _lon = _lon * -1;
}

int Coord::getDegrees(double coordonate)
{
    coordonate = fabs(coordonate);
    double degrees;
    modf(coordonate,&degrees);
    //double minutes;
    //modf((coordonate-degrees)*60, &minutes);
    //float seconds = ((float)coordonate - (float)degrees - ((float)minutes)/60) * 3600;
    return (int)degrees;
}

int Coord::getMinutes(double coordonate)
{
    coordonate = fabs(coordonate);
    double degrees;
    modf(coordonate,&degrees);
    double minutes;
    modf((coordonate-degrees)*60, &minutes);
    //float seconds = ((float)coordonate - (float)degrees - ((float)minutes)/60) * 3600;
    return (int)minutes;
}

int Coord::getSeconds(double coordonate)
{
    coordonate = fabs(coordonate);
    double degrees;
    modf(coordonate,&degrees);
    double minutes;
    modf((coordonate-degrees)*60, &minutes);
    float seconds = ((float)coordonate - (float)degrees - ((float)minutes)/60) * 3600;
    return (int)seconds;
}

double Coord::getCoord(int degrees, int minutes, int seconds)
{
    return (double)degrees + (double)minutes/60 + (double)seconds/3600;
}

int Coord::getSnaptime() const
{
    return _snaptime;
}

void Coord::setSnaptime(int snaptime)
{
    _snaptime = snaptime;
}

int Coord::getGpsHeight() const
{
    return _gpsHeight;
}

void Coord::setGpsHeight(int gpsHeight)
{
    _gpsHeight = gpsHeight;
}

void Coord::setDistanceMethod(distanceMethods dm)
{
    distanceMethod_ = dm;
    QSettings settings;
    settings.setValue("distanceMethod",(int)distanceMethod_);
}

distanceMethods Coord::distanceMethod()
{
    if(distanceMethod_ == distanceMethods::None)
    {
        QSettings settings;
        distanceMethod_=static_cast<distanceMethods>(settings.value("distanceMethod",0).toInt());
        return distanceMethod_;
    }
    return distanceMethod_;
}

Coord::Coord()
{
}

Coord::Coord(const Coord &other) {
    _lat = other._lat;
    _lon = other._lon;
    _snaptime = other._snaptime;
    _gpsHeight = other._gpsHeight;
}

Coord::Coord(const Latitude &lat, const Longitude &lon)
    : _lat(lat.dec()), _lon(lon.dec())
{}

Coord::~Coord()
{
}

QDataStream &operator<<(QDataStream &out, const Coord &coord)
{
    out << coord.getLat() << coord.getLon();
    return out;
}

QDataStream &operator>>(QDataStream &in, Coord &coord)
{
    double lat,lon;
    in >> lat >> lon;
    coord.setLat(lat);
    coord.setLon(lon);
    return in;
}
