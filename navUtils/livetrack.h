/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LIVETRACK_H
#define LIVETRACK_H

#include <QObject>
#include <QTimer>
#include "navUtils/coord.hpp"
#include <QString>
#include <QtNetwork>
#include "threads/takeoffthread.h"


class LiveTrack : public QObject
{
    Q_OBJECT
public:
    explicit LiveTrack(QObject *parent = 0);
    void updateAccount();

    QString id;
    QString key;

    void setFlightId(int id);
signals:

public slots:
    void setCoord(Coord coord);
    void setHeight(int height);
    void setHeading(qreal degrees);
    void setSpeed(qreal kmph);
    void timerEvent();

private:
    QTimer timer;
    Coord locationNow;
    int heightNow;
    int headingNow;
    int speedNow;
    bool liveTrackingEnabled;
    int flightId=0;

    TakeoffThread *tot;

    QNetworkAccessManager *manager;
    bool managerFinishedState = true;
    QNetworkRequest request;

private slots:
    void managerFinished(QNetworkReply *reply);
};

#endif // LIVETRACK_H
