/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ELEVATION_H
#define ELEVATION_H

#include <QString>
#include "navUtils/coord.hpp"
#include "threads/SystemThread.h"

class Elevation
{
public:
    Elevation();
    int getElevation(const Coord &coord);

private:
    quint8 buf[9][2884802];
    QString bufFile[9];
    int lastUsed = -1;
    SystemThread lrzipThread;

#ifdef __ARMEL__
    const QString lrzipPath = "/mnt/mipfly/hgt/";
#else
    const QString lrzipPath = "/home/mipi/Downloads/hgt/";
#endif

    QString getStdBaseFilename(const Coord &coord);
    bool loadFile(QString fileName,int bufNumber);
    bool prepareFile(QString fileName);
    int getElevationFromBuf(const Coord &coord, int bufNumber);
};

#endif // ELEVATION_H
