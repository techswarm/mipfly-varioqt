/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "glide.hpp"
#include <string>     // std::string, std::to_string
#include <sstream>
#include <iomanip>

#include <QSettings>

#include "settings/glidemanager.h"
#include "NMEA/NMEAParser.h"

#include "VarioQt.h"

#ifdef USE_SOUND
#include "sound/varioSound.h"
#endif

/*
Glide::Glide(int numAverage, int glideMax)
{
	_numAverage = numAverage;
	_glideMax = glideMax;
}
*/

Glide::Glide(const NmeaParser &gpsParser, const NmeaParser &varParser, const GlideManager &mgr, QObject *parent)
    : QObject(parent), _numAverage(mgr.getNumAverage()), _glideMax(GlideManager::GLIDE_MAX)
{
    connect(&mgr, SIGNAL(numAvgChanged(int)), this, SLOT(setNumAverage(int)));
    connect(&gpsParser, SIGNAL(speedKmph(qreal)), this, SLOT(pushSpeed(qreal)));
    connect(&varParser, SIGNAL(vSpeed(int)), this, SLOT(pushSink(int)));     // Vario NMEA
}

float Glide::cmpsToKmh(float cmps)
{
	return cmps * 3.6 / 100;
}

void Glide::pushSink(int sinkCmps)
{
#ifdef USE_SOUND
    VarioQt::instance().getVarioSound()->varioSoundSetCMPS(sinkCmps);
#endif
    if(!_initialisedSink) {
        _averageSink = cmpsToKmh(qreal(sinkCmps));
		_initialisedSink = true;
	} else {
        _averageSink = (_averageSink * (_numAverage - 1) + cmpsToKmh(qreal(sinkCmps))) / _numAverage;
	}

    checkAndEmit();
}

void Glide::pushSpeed(qreal speedKmh)
{
	if(!_initialisedSpeed) {
		_averageSpeed = speedKmh;
		_initialisedSpeed = true;
	} else {
        _averageSpeed = (_averageSpeed * ((_numAverage / 2) - 1) + speedKmh) / (_numAverage / 2);
	}

    checkAndEmit();
}

void Glide::checkAndEmit() {
    if (_initialisedSink && _initialisedSpeed) {
        qreal val = getGlide();
        emit glideChanged(val);
    }
}

float Glide::getGlide() const
{
	if(_averageSink != 0) {
        float ret = _averageSpeed/_averageSink * -1;
        if(ret > 99.9)
                ret = 99.9;
        return ret;
	}
    return 0.0;
}

std::string Glide::getGlideStr()
{
	float glideValue = getGlide();
	if(glideValue > _glideMax) return "---";

	if(glideValue < 0) return "---";
	
	std::stringstream stream;
	stream << std::fixed << std::setprecision(2) << glideValue;
	return stream.str();
}

void Glide::setNumAverage(int num) {
	_numAverage = num;
}
