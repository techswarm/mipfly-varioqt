/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FAIASSISTCOMPUTE_H
#define FAIASSISTCOMPUTE_H

#include "coord.hpp"
#include <QObject>
#include <QList>
#include <QTime>
#include "navWaypoint/turnpoint.h"

class FaiAssistCompute : public QObject
{
    Q_OBJECT
public:
    FaiAssistCompute();

    void clearTaskPoints();
    //this function adds a new taskpoint when called only if a certain time has passed since the prewious point was added
    void pushTaskPoint(Coord coord);

    //this function processes all points and determine the gratest dinstance beetween two points
    void computeBaseLeg();

    //Ximn adn xmax are the shortest and longest sides of an isocel triangle satisfying FAI 28% rule
    float computeXmin(float a);
    float computeXmax(float a);

    //x is the longest third arm that will satisfy FAI 28% rule
    float computeX(float a, float b);

    float computeBaseAngle(float a, float b, float c);

    void computeFaiArea();

    QList<Coord> getTaskPoints() const;

    QList<Coord> getLeftAreaPoints() const;

    Coord getTp1() const;
    void setTp1(const Coord &value);

    Coord getTp2() const;
    void setTp2(const Coord &value);

    Coord getTp3() const;
    void setTp3(const Coord &value);

    void markTp1();
    void markTp2();

    float getMaxDistToTrianglePoint() const;

    QList<Coord> getRightAreaPoints() const;

    Coord getTargetCoord();

    bool getTp1Marked() const;

    bool getTp2Marked() const;

    bool getAreaComputed() const;

    TurnPoint getTargetTp();

    float getTotalPathLength() const;

public slots:
    void doPushTaskPoint(Coord coord);
private:
    int pushPostscale = 5*5;
    QList<Coord> taskPoints;
    QList<Coord> leftAreaPoints,rightAreaPoints;

    Coord tp1,tp2,tp3;
    Coord targetCoord;
    float distTarget = 0;
    bool tp1Marked = false;
    bool tp2Marked = false;
    bool areaComputed = false;

    QTime latestCoordSnapshot;

    float totalPathLength = 0;
    float baseDistance = 0;
    float xMinKm;
    float xMaxKm;

    bool faiAreaReached = false;

    TurnPoint targetTp;

    float maxDistToTrianglePoint = 1;
    void computeOptimisedPoint();
    bool inFaiArea();
};

#endif // FAIASSISTCOMPUTE_H
