/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "crosssectionpoints.h"
#include "VarioQt.h"
#include "elevation.h"
CrossSectionPoints::CrossSectionPoints(QObject *parent) : QObject(parent)
{

}

void CrossSectionPoints::computePoints(Coord center, float heading)
{
    QElapsedTimer timer;
    timer.start();
    Coord destination(center);
    heading = heading * 0.0174532925;
    destination.translateTo(5000,heading);
    float latDelta = (destination.getLat()-center.getLat())/240;
    float lonDelta = (destination.getLon()-center.getLon())/240;

    max = -10000;
    min = 10000;

    Elevation *pElev = VarioQt::instance().getElevation();

    for(int i=0;i<240;i++)
    {
        points[i] = pElev->getElevation(Coord(center.getLat()+latDelta*i,center.getLon()+lonDelta*i));
        if(points[i]<0)return;
        if(points[i]>max)max = points[i];
        if(points[i]<min)min = points[i];
    }
    //qDebug() << "The slow operation took" << timer.elapsed() << "milliseconds";
}

int CrossSectionPoints::getMax() const
{
    return max;
}

int CrossSectionPoints::getMin() const
{
    return min;
}
