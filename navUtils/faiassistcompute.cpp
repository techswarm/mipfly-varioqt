/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "faiassistcompute.h"
#include <QDebug>
#include <math.h>
#include "VarioQt.h"

FaiAssistCompute::FaiAssistCompute()
{
    latestCoordSnapshot = QTime::currentTime();
    targetTp.setGoal(TurnPoint::GoalType::CylinterEnter);
    targetTp.setRadiusLengthMeters(100);
}

void FaiAssistCompute::clearTaskPoints()
{
    taskPoints.clear();
}

void FaiAssistCompute::computeOptimisedPoint()
{
    if(areaComputed)
    {
        float distLeft = taskPoints.last().distanceKm(leftAreaPoints.at(0));
        float distRight = taskPoints.last().distanceKm(rightAreaPoints.at(0));

        float distMax = 1000;
        if(distLeft < distRight)
        {
            foreach (Coord pt, leftAreaPoints) {
                float dist = taskPoints.last().distanceKm(pt);
                if(dist<distMax)
                {
                    distMax = dist;
                    targetCoord = pt;
                }
            }
        }
        else
        {
            foreach (Coord pt, rightAreaPoints) {
                float dist = taskPoints.last().distanceKm(pt);
                if(dist<distMax)
                {
                    distMax = dist;
                    targetCoord = pt;
                }
            }
        }

        distTarget = distMax;
        Waypoint wp;
        wp.setCoordonate(targetCoord);
        wp.setHeight(VarioQt::instance().getElevation()->getElevation(targetCoord));
        wp.setName("FAI op");
        targetTp.setWaypoint(wp);
    }
}

void FaiAssistCompute::pushTaskPoint(Coord coord)
{
    //only push every 5 seconds. Changed to postscale so that it works correctly on speed replay
    if(pushPostscale--<=0)
    {
        if(taskPoints.count())totalPathLength += taskPoints.last().distanceKm(coord);
        pushPostscale=5*5;
        taskPoints.append(coord);
        qDebug()<<"totalPathLength="<<totalPathLength;
        computeOptimisedPoint();
    }
    if(inFaiArea())
    {
        if(!faiAreaReached)
        {
            Warning::showWarningSound(tr("TASK INFO"),tr("FAI AREA REACHED"),5);
            faiAreaReached = true;
        }
    }
}

void FaiAssistCompute::computeBaseLeg()
{
    baseDistance = 0;
    for(int i=0;i<taskPoints.count()-1;i++)
    {
        for(int j=i+1;j<taskPoints.count();j++)
        {
            float dist = taskPoints[i].distanceKm(taskPoints[j]);
            if(dist > baseDistance)
            {
                baseDistance = dist;
                tp1 = taskPoints.at(i);
                tp2 = taskPoints.at(j);
            }
        }
    }
}

float FaiAssistCompute::computeXmin(float a)
{
    return 28 * a / 44;
}

float FaiAssistCompute::computeXmax(float a)
{
    return 72 * a / 56;
}

float FaiAssistCompute::computeX(float a, float b)
{
    return qMin((72 * a - 28 * b)/28,(-28 * a + 72 * b)/28);
}

float FaiAssistCompute::computeBaseAngle(float a, float b, float c)
{
    return acos((a*a+b*b-c*c)/(2 * a * b));
}

bool FaiAssistCompute::inFaiArea()
{
    if(!areaComputed)return false;
    float tp1Dist = taskPoints.last().distanceKm(tp1);
    float tp2Dist = taskPoints.last().distanceKm(tp2);
    float totalDist = tp1Dist+tp2Dist+baseDistance;
    float minDist = totalDist*28.0/100.0;
    if(tp1Dist < minDist) return false;
    if(tp2Dist < minDist) return false;
    if(baseDistance < minDist) return false;
    return true;
}

void FaiAssistCompute::computeFaiArea()
{
    //compute the longest distance beetween 2 points and the respective points
    baseDistance = tp1.distanceKm(tp2);


    xMinKm = computeXmin(baseDistance);
    xMaxKm = computeXmax(baseDistance);

    float baseAngle = tp1.bearingTo(tp2);

    leftAreaPoints.clear();
    rightAreaPoints.clear();
    float maxDistance = 0;


    //run from xMin to xMax and compute the 3rd arm
    //compute increment step
    float increment = (xMaxKm-xMinKm)/100;
    for(float b=xMinKm;b<xMaxKm; b+=increment)
    {
        //determine the length of the 3rd arm
        float c = computeX(baseDistance,b);

        //determine the angle beetween the base and the second arm
        float angleC = computeBaseAngle(baseDistance,b,c);

        Coord outherPoint = tp1;
        outherPoint.translateTo(b * 1000,angleC + baseAngle);
        leftAreaPoints.append(outherPoint);
        float localDistance = taskPoints.last().distanceKm(outherPoint);
        if(localDistance > maxDistance)maxDistance = localDistance;
    }
    for(float b=xMinKm;b<xMaxKm; b+=increment)
    {
        //determine the length of the 3rd arm
        float c = computeX(baseDistance,b);

        //determine the angle beetween the base and the second arm
        float angleC = computeBaseAngle(baseDistance,c,b);

        Coord outherPoint = tp1;
        outherPoint.translateTo(c * 1000,angleC + baseAngle);
        leftAreaPoints.insert(0,outherPoint);
        float localDistance = taskPoints.last().distanceKm(outherPoint);
        if(localDistance > maxDistance)maxDistance = localDistance;
    }

    for(float b=xMinKm;b<xMaxKm; b+=increment)
    {
        //determine the length of the 3rd arm
        float c = computeX(baseDistance,b);

        //determine the angle beetween the base and the second arm
        float angleC = computeBaseAngle(baseDistance,b,c);

        Coord outherPoint = tp2;
        outherPoint.translateTo(b * 1000,angleC + baseAngle + M_PI);
        rightAreaPoints.append(outherPoint);
        float localDistance = taskPoints.last().distanceKm(outherPoint);
        if(localDistance > maxDistance)maxDistance = localDistance;
    }
    for(float b=xMinKm;b<xMaxKm; b+=increment)
    {
        //determine the length of the 3rd arm
        float c = computeX(baseDistance,b);

        //determine the angle beetween the base and the second arm
        float angleC = computeBaseAngle(baseDistance,c,b);

        Coord outherPoint = tp2;
        outherPoint.translateTo(c * 1000,angleC + baseAngle + M_PI);
        rightAreaPoints.insert(0,outherPoint);
        float localDistance = taskPoints.last().distanceKm(outherPoint);
        if(localDistance > maxDistance)maxDistance = localDistance;
    }

    maxDistToTrianglePoint = maxDistance;
    areaComputed = true;
    faiAreaReached = false;
}

void FaiAssistCompute::doPushTaskPoint(Coord coord)
{
    pushTaskPoint(coord);
}

float FaiAssistCompute::getTotalPathLength() const
{
    return totalPathLength;
}

bool FaiAssistCompute::getAreaComputed() const
{
    return areaComputed;
}

TurnPoint FaiAssistCompute::getTargetTp()
{
    return targetTp;
}

bool FaiAssistCompute::getTp2Marked() const
{
    return tp2Marked;
}

bool FaiAssistCompute::getTp1Marked() const
{
    return tp1Marked;
}

QList<Coord> FaiAssistCompute::getRightAreaPoints() const
{
    return rightAreaPoints;
}

Coord FaiAssistCompute::getTargetCoord()
{
    return targetCoord;
}

float FaiAssistCompute::getMaxDistToTrianglePoint() const
{
    return maxDistToTrianglePoint * 1000;
}

Coord FaiAssistCompute::getTp3() const
{
    return tp3;
}

void FaiAssistCompute::setTp3(const Coord &value)
{
    tp3 = value;
}

void FaiAssistCompute::markTp1()
{
    tp1Marked = true;
    setTp1(taskPoints.last());
}

void FaiAssistCompute::markTp2()
{
    tp2Marked = true;
    setTp2(taskPoints.last());
}

Coord FaiAssistCompute::getTp2() const
{
    return tp2;
}

void FaiAssistCompute::setTp2(const Coord &value)
{
    tp2 = value;
}

Coord FaiAssistCompute::getTp1() const
{
    return tp1;
}

void FaiAssistCompute::setTp1(const Coord &value)
{
    tp1 = value;
}

QList<Coord> FaiAssistCompute::getLeftAreaPoints() const
{
    return leftAreaPoints;
}

QList<Coord> FaiAssistCompute::getTaskPoints() const
{
    return taskPoints;
}
