/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "metartaf.h"
#include "VarioQt.h"
#include <QDateTime>

MetarTaf::MetarTaf(QObject *parent) : QObject(parent)
{
    manager = new QNetworkAccessManager();
    QObject::connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(managerFinished(QNetworkReply*)));
    updateAccount();
    sendRequest();
}

void MetarTaf::updateAccount()
{
    QSettings settings;
    key = settings.value("accountKey","").toString();
    id = settings.value("accountId","").toString();
}

QString MetarTaf::getMetar()
{
    update();
    return metar;
}

QString MetarTaf::getTaf()
{
    update();
    return taf;
}

void MetarTaf::sendRequest()
{
    Coord locationNow = VarioQt::instance().getGpsParser()->coord();
    if(!managerFinishedState && id!="")return;
    if(locationNow.getLat()==0)return;

    QString url = Vario::host + "metartaf";
    url+="?lat="+QString::number(locationNow.getLat());
    url+="&lon="+QString::number(locationNow.getLon());
    url+="&id="+id;
    url+="&apiKey="+key;

    request.setUrl(QUrl(url));
    managerFinishedState = false;
    manager->get(request);
}

void MetarTaf::update()
{
    int currentTimestamp = QDateTime::currentSecsSinceEpoch();
    if(currentTimestamp - timestamp > 60*5)sendRequest();
}

void MetarTaf::managerFinished(QNetworkReply *reply)
{
    managerFinishedState = true;
    if (reply->error()) {
        qDebug() << reply->errorString();
        return;
    }

    QString answer = reply->readLine();
    QStringList tokens = answer.split(",");
    //TODO process incomming message

    timestamp = QDateTime::currentSecsSinceEpoch();

    VarioQt::instance().getNetStats()->resetTimeout();
}
