/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "variocompute.h"

#include <QtDebug>

#include "NMEA/NMEAParser.h"
#include <QSettings>
#include "utils/vario_common.h"


void VarioCompute::saveSetting()
{
    QSettings settings;
    settings.setValue(Vario::SETTINGS_VARIOINTSEC,buflen);
}

void VarioCompute::loadSetting()
{
    QSettings settings;
    buflen = settings.value(Vario::SETTINGS_VARIOINTSEC,10).toInt();
}

VarioCompute::VarioCompute(const NmeaParser &parser, QObject *parent)
    : QObject(parent)
{
    loadSetting();
    for (int i=0; i < buflen; i++) {
        _cmpsBuf[i] = 0;
    }

    connect(&parser, SIGNAL(vSpeed(int)), this, SLOT(pushCmps(int)));
}

qreal VarioCompute::getAverageCmps()
{
    return _average;
}

void VarioCompute::pushCmps(int cmps) {
    //    this->_cmps = cmps;

    _sum = _sum - _cmpsBuf[_cmpsBufPos] + cmps;

    _average = qreal(_sum) / qreal(buflen);	// if sum == 0 then _average == 0

    //only consider instant climbs larger than 1m/s
    if(cmps>50)
    {
        totalClimbCm += cmps/5;
        total200msClimbTicks ++;
    }
    //only consider instant sink larger than 1.5m/s
    if(cmps<-150)
    {
        totalSinkCm += cmps/5;
        total200msSinkTicks ++;
    }

    emit average(_average);

    if(_average > _averageMax) {
        _averageMax = _average;
        emit averageMax(_averageMax);
    }
    if(_average < _averageMin) {
        _averageMin = _average;
        emit averageMin(_averageMin);
    }

    //    qDebug() << "cmps " << cmps << " avg " << _average << " max " << _averageMax << " min " << _averageMin;

    _cmpsBuf[_cmpsBufPos++] = cmps;
    if(_cmpsBufPos >= buflen)
        _cmpsBufPos = 0;

}

void VarioCompute::decreaseAverageSeconds()
{
    if(buflen>10)buflen-=10;
    saveSetting();
}

void VarioCompute::increaseAverageSeconds()
{
    if(buflen<BUFLENMAX-10)buflen+=10;
    saveSetting();
}

int VarioCompute::getBuflen() const
{
    return buflen;
}

void VarioCompute::compute() {
    int i;
    int sum = 0;
    for(i = 0; i < buflen; i++) {
        sum += this->_cmpsBuf[i];
    }


    this->_average = sum / buflen;	// if sum == 0 then _average == 0

    qDebug("%d / %d == %d", sum, buflen, sum / buflen);


    if(this->_average > this->_averageMax)
        this->_averageMax = this->_average;
    if(this->_average < this->_averageMin)
        this->_averageMin = this->_average;

    emit average(_average / 100);
    emit averageMax(_averageMax / 100);
    emit averageMin(_averageMin / 100);
}

void VarioCompute::reset() {
    _reset = true;

    _average = 0;
    _averageMax = 0;
    _averageMin = 0;

    for (int i=0; i < buflen; i++) {
        _cmpsBuf[i] = 0;
    }

    emit average(_average);
    emit averageMax(_averageMax);
    emit averageMin(_averageMin);

    _reset = false;
}

float VarioCompute::getAverageClimb()
{
    return float(totalClimbCm/100)/(float)(total200msClimbTicks/5);
}

float VarioCompute::getAverageSink()
{
    return float(totalSinkCm/100)/(float)(total200msSinkTicks/5);
}
