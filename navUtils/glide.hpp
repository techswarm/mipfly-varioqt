/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GLIDE_HPP
#define GLIDE_HPP

#include <string>     // std::string, std::to_string
#include <QObject>

class NmeaParser;
class GlideManager;

class Glide: public QObject
{
    Q_OBJECT

public:
//        Glide(int numAverage, int glideMax);
        Glide(const NmeaParser &gpsParser, const NmeaParser &varParser, const GlideManager &mgr, QObject *parent = 0);
        float getGlide() const;
        std::string getGlideStr();

public slots:
        void pushSink(int sinkCmps);
        void pushSpeed(qreal speedKmh);
        void setNumAverage(int);

signals:
        void glideChanged(qreal glide);

private:
	bool _initialisedSpeed;
	bool _initialisedSink;
	int _numAverage;
	float _averageSpeed;
	float _averageSink;
	int _glideMax;
	
	float cmpsToKmh(float cmps);
    void checkAndEmit();
};

#endif
