/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LASTLIFT_H
#define LASTLIFT_H

#include "navUtils/coord.hpp"
#include "navUtils/variocompute.h"
#include <QTime>

class LastLift : public QObject
{
    Q_OBJECT
private:
    Coord _lastLiftCoordonates;
    double _lastLiftAverageCmps;
    double _lastDecayed;
    double _decayCmps;
    double _minCmps;
    Coord _lastPushedCoordonates;
    QTime _time;
    VarioCompute* _VC;

public:
    LastLift(QObject *parent, VarioCompute* VC, double dacayCmps = 1, double minCmps = 50);
    Coord getCoordonates();
    double getAverageLift();
public slots:
    void pushCoord(Coord location);
signals:
    void liftDir(qreal dir);
    void liftDist(int distanceM);
};

#endif // LASTLIFT_H
