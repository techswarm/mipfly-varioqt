/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "elevation.h"
#include <QString>
#include <QDir>
#include <QFile>
#include <QDebug>
#include <math.h>
#include <QElapsedTimer>


Elevation::Elevation()
{
    QString tempPath = "temp/hgt";
    QDir dir(tempPath);
    if(!dir.exists())
    {
        dir.mkpath(".");
    }

    //delete all temp files in hgt if more than 10 to prev filling the disk
    if(dir.count()>10)
    {
        dir.setNameFilters(QStringList() << "*.*");
        dir.setFilter(QDir::Files);
        foreach(QString dirFile, dir.entryList())
        {
            dir.remove(dirFile);
        }
    }
}

int Elevation::getElevationFromBuf(const Coord &coord,int bufNumber)
{
    /*
    for(int g=0;g<2884802;g+=2)
    {
        int elevation = (buf[bufNumber][g]<<8)+buf[bufNumber][g+1];
        if(elevation==0)
        {
            qDebug()<<"CV";
        }
        qDebug()<<"Elevation: "<<elevation;
    }
    */
    const int secondsPerPx = 3;
    const int totalPx = 1201;


    float lat = coord.getLat();
    float lon = coord.getLon();
    int latDec = (int)floor(lat);
    int lonDec = (int)floor(lon);

    float secondsLat = (lat-latDec) * 60 * 60;
    float secondsLon = (lon-lonDec) * 60 * 60;

    int y = secondsLat/secondsPerPx;
    int x = secondsLon/secondsPerPx;

    int row = (totalPx-1) - y;
    int col = x;
    int pos = (row * totalPx + col) * 2;

    int ret = buf[bufNumber][pos];
    ret = ret << 8;
    ret += buf[bufNumber][pos + 1];

    return ret;
}

int Elevation::getElevation(const Coord &coord)
{
    QString baseFileName = getStdBaseFilename(coord)+".hgt";

    //qDebug()<<"check if file isn't already cached";
    for(int i=0;i<=lastUsed;i++)
    {
        if(baseFileName == bufFile[i])
        {
            return getElevationFromBuf(coord,i);
        }
    }

    qDebug()<<"if we are here no buffered files were found";
    if(lrzipThread.isRunning())
    {
        qDebug()<<"LRZip thread is still running";
        return -1000;
    }
    lastUsed ++;
    if(lastUsed > 8)
    {
        lastUsed = 0;
        bufFile[0]="";
    }
    if(!loadFile(baseFileName,lastUsed))return -1000;
    else
    {
        for(int i=0;i<=lastUsed;i++)
        {
            if(baseFileName == bufFile[i])
            {
                return getElevationFromBuf(coord,i);
            }
        }
    }
    return -1000;
}

QString Elevation::getStdBaseFilename(const Coord &coord)
{
    QString fileName = "";
    if(coord.getLat()>0)
    {
        fileName+="N";
    }
    else
    {
        fileName+="S";
    }
    int degree = fabs(floor(coord.getLat()));
    QString number = QString("%1").arg(degree, 2, 10, QChar('0'));
    fileName+=number;

    if(coord.getLon()>0)
    {
        fileName+="E";
    }
    else
    {
        fileName+="W";
    }
    degree = fabs(floor(coord.getLon()));
    number = QString("%1").arg(degree, 3, 10, QChar('0'));
    fileName+=number;
    return fileName;
}

bool Elevation::loadFile(QString fileName, int bufNumber)
{
    QElapsedTimer timer;
    timer.start();
    if(!prepareFile(fileName))return false;
    QFile file("temp/hgt/"+fileName);
    try
    {
        file.open(QIODevice::ReadOnly);
        if(file.size()<2884802)
        {
            file.close();
            file.remove();
            return false;
        }

        file.read((char*)buf[bufNumber],2884802);
        bufFile[bufNumber] = fileName;
        qDebug() << "The slow load operation took" << timer.elapsed() << "milliseconds";
    }
    catch (int e)
    {
        return false;
    }
    return true;
}

bool Elevation::prepareFile(QString fileName)
{
    if(lrzipThread.isRunning())return 0;
    QFile file("temp/hgt/"+fileName);
    qDebug()<<"Prepare file"+fileName;
    if(file.exists())return true;

    //model: lrunzip ~/Downloads/N47E021.hgt.lrz -O temp/hgt/
    QFile lrzFile(lrzipPath+fileName+".lrz");
    if(!lrzFile.exists())return false;
    QString cmd;
    cmd+="lrunzip "+lrzipPath+fileName+".lrz ";
    cmd+="-O temp/hgt/";
    //qDebug()<<cmd;
    lrzipThread.setCmd(cmd);
    lrzipThread.start();

    if(file.exists())
    {
        if(file.size()>1024)
            return true;
        return false;
    }
    return false;
}



