/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QDebug>
#include "termalingDetector.h"
#include <QSettings>

TermalingDetector::TermalingDetector(QObject *parent) : QObject(parent)
{
    QSettings settings;
    _termalingActiveDetectEnable = settings.value("termaling/termalActiveDetect",true).toBool();
    _turnIntegralSaturation = settings.value("termaling/turnIntegralSaturation",150).toInt();
    _turnIntegralDecay = settings.value("termaling/turnIntegralDecay",1).toFloat();
    _activeateTreshold = settings.value("termaling/activateTreshold",80).toInt();
    _deactivateTreshold = settings.value("termaling/deactivateTreshold",60).toInt();
    _targetPage = settings.value("termaling/targetPage",30).toInt();

    pVc = VarioQt::instance().getVarioCompute();
    pGlider = VarioQt::instance().getGlider();
}

int TermalingDetector::turnIntegralSaturation() const
{
    return _turnIntegralSaturation;
}

void TermalingDetector::setTurnIntegralSaturation(int turnIntegralSaturation)
{
    _turnIntegralSaturation = turnIntegralSaturation;
    QSettings settings;
    settings.setValue("termaling/turnIntegralSaturation",turnIntegralSaturation);
    settings.sync();
}

float TermalingDetector::turnIntegralDecay() const
{
    return _turnIntegralDecay;
}

void TermalingDetector::setTurnIntegralDecay(float turnIntegralDecay)
{
    _turnIntegralDecay = turnIntegralDecay;
    QSettings settings;
    settings.setValue("termaling/turnIntegralDecay",turnIntegralDecay);
    settings.sync();
}

int TermalingDetector::activeateTreshold() const
{
    return _activeateTreshold;
}

void TermalingDetector::setActiveateTreshold(int activeateTreshold)
{
    _activeateTreshold = activeateTreshold;
    QSettings settings;
    settings.setValue("termaling/activateTreshold",activeateTreshold);
    settings.sync();
}

int TermalingDetector::deactivateTreshold() const
{
    return _deactivateTreshold;
}

void TermalingDetector::setDeactivateTreshold(int deactivateTreshold)
{
    _deactivateTreshold = deactivateTreshold;
    QSettings settings;
    settings.setValue("termaling/deactivateTreshold",deactivateTreshold);
    settings.sync();
}

bool TermalingDetector::termalingActiveDetectEnable() const
{
    return _termalingActiveDetectEnable;
}

void TermalingDetector::setTermalingActiveDetectEnable(bool termalingActiveDetectEnable)
{
    _termalingActiveDetectEnable = termalingActiveDetectEnable;
    QSettings settings;
    settings.setValue("termaling/termalActiveDetect",termalingActiveDetectEnable);
    settings.sync();
}

int TermalingDetector::targetPage() const
{
    return _targetPage;
}

void TermalingDetector::setTargetPage(int targetPage)
{
    _targetPage = targetPage;
    QSettings settings;
    settings.setValue("termaling/targetPage",targetPage);
    settings.sync();
}

void TermalingDetector::doHeadingPush(qreal heading)
{
    if(!_termalingActiveDetectEnable)return;
    int diff = _lastCourse - heading;
    _lastCourse = heading;
    diff = abs(diff);
    //qDebug()<<"diff = "<<diff;
    if(diff<90)
    {
        _absTurnIntegral+=diff;
    }
    if(_absTurnIntegral > _turnIntegralSaturation)_absTurnIntegral = _turnIntegralSaturation;
    if(_absTurnIntegral > _turnIntegralDecay)_absTurnIntegral-=_turnIntegralDecay;
    //qDebug()<<"_absTurnIntegral = "<<_absTurnIntegral;

    if(!_termalingActive && _absTurnIntegral>_activeateTreshold && pVc->getAverageCmps()>-30 && pGlider->getGroundSpeed()>10)
    {
        _termalingActive = true;
        emit termalingStart();
    }
    if(_termalingActive && _absTurnIntegral<_deactivateTreshold)
    {
        _termalingActive = false;
        emit termalingStop();
    }
}
