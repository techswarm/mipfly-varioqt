/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef METARTAF_H
#define METARTAF_H

#include <QObject>
#include <QString>
#include <QtNetwork>
#include <QTimer>

class MetarTaf : public QObject
{
    Q_OBJECT
public:
    explicit MetarTaf(QObject *parent = nullptr);
    void updateAccount();

    QString id;
    QString key;

    QString getMetar();

    QString getTaf();

signals:

public slots:

private:
    QNetworkAccessManager *manager;
    bool managerFinishedState = true;
    QNetworkRequest request;
    int timestamp;

    QString metar="";
    QString taf="";

    void sendRequest();
    void update();

private slots:
    void managerFinished(QNetworkReply *reply);
};

#endif // METARTAF_H
