/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TERMALINGDETECTOR_H
#define TERMALINGDETECTOR_H

#include <QObject>
#include "VarioQt.h"
#include "navUtils/variocompute.h"

class TermalingDetector : public QObject
{
    Q_OBJECT
public:
    explicit TermalingDetector(QObject *parent = nullptr);

    int turnIntegralSaturation() const;
    void setTurnIntegralSaturation(int turnIntegralSaturation);

    float turnIntegralDecay() const;
    void setTurnIntegralDecay(float turnIntegralDecay);

    int activeateTreshold() const;
    void setActiveateTreshold(int activeateTreshold);

    int deactivateTreshold() const;
    void setDeactivateTreshold(int deactivateTreshold);

    bool termalingActiveDetectEnable() const;
    void setTermalingActiveDetectEnable(bool termalingActiveDetectEnable);


    int targetPage() const;
    void setTargetPage(int targetPage);

private:
    int _lastCourse;
    float _absTurnIntegral = 0;

    int _turnIntegralSaturation = 150;
    float _turnIntegralDecay = 3;

    bool _termalingActive = false;

    int _activeateTreshold = 100;
    int _deactivateTreshold = 30;

    int _targetPage = 2;

    bool _termalingActiveDetectEnable = true;

    VarioCompute *pVc;
    Glider *pGlider;

signals:
    void termalingStart();
    void termalingStop();

public slots:
    void doHeadingPush(qreal heading);
};

#endif // TERMALINGDETECTOR_H
