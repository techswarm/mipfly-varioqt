/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "livetrack.h"
#include <QDateTime>
#include <QString>
#include <QDebug>
#include <QSettings>
#include "VarioQt.h"
#include "threads/takeoffthread.h"
#include "utils/vario_common.h"

LiveTrack::LiveTrack(QObject *parent) : QObject(parent)
{
    updateAccount();

    timer.setInterval(1000);
    connect(&timer,SIGNAL(timeout()),this,SLOT(timerEvent()));
    if (liveTrackingEnabled) timer.start();

    tot = VarioQt::instance().getTakeOffThread();

    manager = new QNetworkAccessManager();
    QObject::connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(managerFinished(QNetworkReply*)));

}

void LiveTrack::updateAccount()
{
    QSettings settings;
    key = settings.value("accountKey","").toString();
    id = settings.value("accountId","").toString();
    liveTrackingEnabled = settings.value("liveTrackingingEnabled", "true").toBool();
    timer.start();
}

void LiveTrack::setCoord(Coord coord)
{
    locationNow = coord;
}

void LiveTrack::setHeight(int height)
{
    heightNow = height;
}

void LiveTrack::setHeading(qreal degrees)
{
    headingNow = (int)degrees;
}

void LiveTrack::setSpeed(qreal kmph)
{
    speedNow = (int)kmph;
}

void LiveTrack::setFlightId(int id)
{
    flightId = id;
}

void LiveTrack::timerEvent()
{
    if(tot==nullptr)return;
    if(!liveTrackingEnabled)
    {
        timer.stop();
        return;
    }
    if(!managerFinishedState && id!="")return;
    if(locationNow.getLat()==0 || heightNow == 0)return;
    QString url = Vario::host + "store?ts=";
    url+=QString::number(QDateTime::currentMSecsSinceEpoch()/1000);

    url+="&lat="+QString::number(locationNow.getLat());
    url+="&long="+QString::number(locationNow.getLon());
    url+="&alt="+QString::number(heightNow);
    url+="&dir="+QString::number(headingNow);
    url+="&speed="+QString::number(speedNow);
    url+="&id="+id;
    url+="&apiKey="+key;
    url+="&flightId="+tot->getFlightId();
    url+="&fly=";

    url += (tot->getFlying()) ? "1" : "0";

    request.setUrl(QUrl(url));
    managerFinishedState = false;
    manager->get(request);

    //qDebug()<<url;
}

void LiveTrack::managerFinished(QNetworkReply *reply)
{

    managerFinishedState = true;
    if (reply->error()) {
        qDebug() << reply->errorString();
        return;
    }

    QString answer = reply->readAll();
    VarioQt::instance().getNetStats()->resetTimeout();
}

