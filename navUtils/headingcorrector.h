/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef HEADINGCORRECTOR_H
#define HEADINGCORRECTOR_H

#include <QtGlobal>
#include <QObject>

class SerialConnection;
class NmeaParser;

class HeadingCorrector: public QObject
{
    Q_OBJECT
public:
    HeadingCorrector(const NmeaParser& parser, QObject *parent = 0);

    void update(qreal heading, qreal speed);

    qreal ax();
    qreal ay();

public slots:
    void setHeading(qreal heading);
    void setSpeed(qreal speed);

private:
    qreal prevHeading;
    qreal dt = 5.0;

    bool headingInit = false;
    bool speedInit = false;

    qreal lastHeading;
    qreal lastSpeed;

    qreal _ax = 0.0;
    qreal _ay = 0.0;

    qreal headingError(qreal initial, qreal final);
};

#endif // HEADINGCORRECTOR_H
