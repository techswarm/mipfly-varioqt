/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CROSSSECTIONPOINTS_H
#define CROSSSECTIONPOINTS_H

#include <QObject>
#include "coord.hpp"

class CrossSectionPoints : public QObject
{
    Q_OBJECT
public:
    explicit CrossSectionPoints(QObject *parent = nullptr);
    int16_t points[240];
    void computePoints(Coord center,float heading);

    int getMax() const;

    int getMin() const;

signals:

public slots:
private:
    int max = 10000;
    int min = -10000;
};

#endif // CROSSSECTIONPOINTS_H
