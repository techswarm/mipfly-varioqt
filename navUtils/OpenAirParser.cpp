/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "OpenAirParser.h"
#include "QString"
#include <QRegExp>
#include "xcsoar/Math/Angle.hpp"
#include <QDebug>
#include "navUtils/distance.h"
#include "utils/unitconverter.h"

OpenAirParser::OpenAirParser(QStringList filenames)
{
    airspaces = filenames;
}


AirSpaceDistanceType AirSpace::getDistance() const
{
    return distance;
}

void AirSpace::setDistance(const AirSpaceDistanceType &value)
{
    distance = value;
}

bool OpenAirParser::repathRequired(Coord center, float metersToPixels)
{
    if(metersToPixels != pathMetersToPixel)
    {
        return true;
    }
    if(center.distanceKm(pathCenter)>100)
    {
        return true;
    }
    return false;
}

void OpenAirParser::repathPath(Coord center, float metersToPixels)
{
    pathMetersToPixel = metersToPixels;
    pathCenter = center;

    for(int i=0;i<airSpaceUnits.count();i++)
    {
        if(airSpaceUnits[i].isDrawable())
        {
            airSpaceUnits[i].computePath(pathCenter,pathMetersToPixel);
        }
    }
}

void OpenAirParser::repathInset()
{
    if(repathInsetIsBuisy)
    {
        repathInsetStop = true;
        while(repathInsetIsBuisy);
    }
    repathInsetStop = false;
    repathInsetIsBuisy = true;
    for(int i=0;i<airSpaceUnits.count();i++)
    {
        if(repathInsetStop)
        {
            repathInsetIsBuisy = false;
            return;
        }
        airSpaceUnits[i].computeInset();
    }
    repathInsetIsBuisy = false;
}

AirSpace::AirSpace()
{

}

void AirSpace::computePath(Coord center, float _metersToPixels)
{
    //clear prev path and inset
    inset = QPainterPath();
    path = QPainterPath();
    foreach (AirSpacePrimitive primitive, aPrimitives) {
        if(primitive.type == AirSpacePrimitivesType::CIRCLE)
        {
            //determine distance to circle center
            double distanceMeters = center.distanceKm(primitive.x) * 1000;

            double angleToCenter = center.bearingTo(primitive.x);

            double distancePixels = distanceMeters * _metersToPixels;
            //qDebug()<<distancePixels;

            path.addEllipse(distancePixels*sin(angleToCenter) - primitive.radiusMeters * _metersToPixels,
                            distancePixels*cos(angleToCenter) *-1 - primitive.radiusMeters * _metersToPixels,
                            primitive.radiusMeters * _metersToPixels * 2,
                            primitive.radiusMeters * _metersToPixels * 2);
        }
        if(primitive.type == AirSpacePrimitivesType::POLY)
        {
            //QList<QPointF> poly;
            QPolygonF poly;
            foreach (Coord c, primitive.point) {
                double distanceMeters = center.distanceKm(c) * 1000;
                double angleToCenter = center.bearingTo(c);
                double distancePixels = distanceMeters * _metersToPixels;

                poly.append(QPointF(
                                distancePixels*sin(angleToCenter),
                                distancePixels*cos(angleToCenter) *-1
                                ));
            }
            path.addPolygon(poly);
        }
    }
    path = path.simplified();
    if(path.isEmpty())
    {
        qDebug()<<"cacat";
    }
}

void AirSpace::computePathAndInset(Coord center, int _metersToPixels)
{
    //clear prev path and inset
    path = QPainterPath();
    inset = QPainterPath();
    foreach (AirSpacePrimitive primitive, aPrimitives) {
        if(primitive.type == AirSpacePrimitivesType::CIRCLE)
        {
            //determine distance to circle center
            double distanceMeters = center.distanceKm(primitive.x) * 1000;

            double angleToCenter = center.bearingTo(primitive.x);

            double distancePixels = distanceMeters * _metersToPixels;
            //qDebug()<<distancePixels;


            path.addEllipse(distancePixels*sin(angleToCenter) - primitive.radiusMeters * _metersToPixels,
                            distancePixels*cos(angleToCenter) *-1 - primitive.radiusMeters * _metersToPixels,
                            primitive.radiusMeters * _metersToPixels * 2,
                            primitive.radiusMeters * _metersToPixels * 2);

            QPainterPath strokePath;
            QPainterPathStroker pathStroke;
            pathStroke.setWidth(4);
            strokePath = pathStroke.createStroke(path);

            inset = strokePath.intersected(path);
        }
        if(primitive.type == AirSpacePrimitivesType::POLY)
        {
            //QList<QPointF> poly;
            QPolygonF poly;
            foreach (Coord c, primitive.point) {
                double distanceMeters = center.distanceKm(c) * 1000;
                double angleToCenter = center.bearingTo(c);
                double distancePixels = distanceMeters * _metersToPixels;

                poly.append(QPointF(
                                distancePixels*sin(angleToCenter),
                                distancePixels*cos(angleToCenter) *-1
                                ));
            }
            path.addPolygon(poly);



            QPainterPath strokePath;
            QPainterPathStroker pathStroke;
            pathStroke.setWidth(4);
            strokePath = pathStroke.createStroke(path);

            inset = strokePath.intersected(path);
        }
    }
    path = path.simplified();
    inset = inset.simplified();
}

void AirSpace::computeInset()
{
    //clear prev inset
    inset = QPainterPath();

    QPainterPath strokePath;
    QPainterPathStroker pathStroke;
    pathStroke.setWidth(4);
    strokePath = pathStroke.createStroke(path);

    inset = strokePath.intersected(path);

    inset = inset.simplified();
}

void AirSpace::setDrawable(bool draw)
{
    drawable = draw;
}

bool AirSpace::isDrawable()
{
    return drawable;
}

AirSpacePrimitive::AirSpacePrimitive()
{

}


QString OpenAirParser::airSpaceClassToString(AirSpaceClass aclass)
{
    switch (aclass) {
    case AirSpaceClass::R:
        return QString("R");
    case AirSpaceClass::Q:
        return QString("Q");
    case AirSpaceClass::P:
        return QString("P");
    case AirSpaceClass::A:
        return QString("A");
    case AirSpaceClass::B:
        return QString("B");
    case AirSpaceClass::C:
        return QString("C");
    case AirSpaceClass::D:
        return QString("D");
    case AirSpaceClass::G:
        return QString("G");
    case AirSpaceClass::GP:
        return QString("GP");
    case AirSpaceClass::CTR:
        return QString("CTR");
    case AirSpaceClass::W:
        return QString("W");
    case AirSpaceClass::TMZ:
        return QString("TMZ");
    case AirSpaceClass::RMZ:
        return QString("RMZ");
    default:
        return QString("UNK");
        break;
    }
}

void OpenAirParser::parseAltitude(QString& line, AirSpaceHeightType &type, uint& alt)
{
    bool convertFromMeters = false;
    bool altitudeIsFeet = false;
    QString input = line;
    QStringList elements;
    int len = 0, pos = 0;
    QString part;
    bool ok;
    uint num = 0;

    type = AirSpaceHeightType::NotSet;
    alt = 0;

    // qDebug("line %d: parsing altitude '%s'", _lineNumber, line.toLatin1().data());
    // Fist, split the string in parsable elements. We start with the text parts.
    QRegExp reg("[A-Za-z]+");

    while (line.length()>0)
    {
        pos = reg.indexIn(line, pos+len);
        len = reg.matchedLength();
        if (pos<0)
        {
            break;
        }
        elements.append(line.mid(pos, len));
    }

    // now, get our number parts

    reg.setPattern("[0-9]+");
    pos=0;
    len=0;

    while (line.length() > 0)
    {
        pos = reg.indexIn(line, pos+len);
        len = reg.matchedLength();

        if (pos < 0)
        {
            break;
        }

        elements.append(line.mid(pos, len));
        line = line.mid(len);
    }

    // now, try parsing piece by piece
    for ( QStringList::Iterator it = elements.begin(); it != elements.end(); ++it )
    {
        part = (*it).toUpper();
        AirSpaceHeightType newType = AirSpaceHeightType::NotSet;

        // first, try to interpret as elevation type
        if ( part == "AMSL" || part == "MSL" || part == "ALT" )
        {
            newType=AirSpaceHeightType::MSL;
        }
        else if ( part == "GND" || part == "SFC" || part == "ASFC" || part == "AGL" || part == "GROUND" )
        {
            newType=AirSpaceHeightType::GND;
        }
        else if (part.startsWith("UNL"))
        {
            newType=AirSpaceHeightType::UNLTD;
        }
        else if (part=="FL")
        {
            newType=AirSpaceHeightType::FL;
        }
        else if (part=="STD")
        {
            newType=AirSpaceHeightType::STD;
        }

        if ( type == AirSpaceHeightType::NotSet && newType != AirSpaceHeightType::NotSet )
        {
            type = newType;
            continue;
        }

        if ( type != AirSpaceHeightType::NotSet && newType != AirSpaceHeightType::NotSet )
        {
            // @AP: Here we stepped into a problem. We found a second
            // elevation type. That can be only a mistake in the data
            // and will be ignored.
            continue;
        }

        // see if it is a way of setting units to feet
        if (part == "FT")
        {
            altitudeIsFeet = true;
            continue;
        }

        // see if it is a way of setting units to meters
        if (part == "M")
        {
            convertFromMeters = true;
            continue;
        }

        // try to interpret as a number
        num = part.toUInt(&ok);

        if (ok)
        {
            alt = num;
        }

        // ignore other parts
    }

    if ( altitudeIsFeet && type == AirSpaceHeightType::NotSet )
    {
        type = AirSpaceHeightType::MSL;
    }

    if (convertFromMeters)
    {
        alt = (int) rint( alt/Distance::mFromFeet);
    }

    if( alt == 0 && type == AirSpaceHeightType::NotSet )
    {
        // @AP: Altitude is zero but no type is assigned. In this case GND
        // is assumed. Found that in a polish airspace file.
        type = AirSpaceHeightType::GND;
    }

    // qDebug("Line %d: Returned altitude %d, type %d", _lineNumber, alt, int(type));
}


int OpenAirParser::heightMeters(AirSpaceHeightType type, uint units)
{
    if(type == AirSpaceHeightType::GND)
    {
        return -10000;
    }
    if(type == AirSpaceHeightType::MSL)
    {
        return units*Distance::mFromFeet;
    }
    if(type == AirSpaceHeightType::FL)
    {
        return (units * 100)*Distance::mFromFeet;
    }
    return -10000;
}

bool OpenAirParser::parseCoordinatePart(QString& line, double& lat, double& lon)
{
    bool ok, ok1, ok2 = false;
    double value = 0;

    if( line.isEmpty() )
    {
        qWarning("OAP: Tried to parse empty coordinate part! Line %d", linenumber);
        return false;
    }

    // A input line can contain elements like:
    // P1= "50:11:31.1504N" P2= " 17:42:38.5171E"

    QStringList sl = line.split(QChar(':'));
    QString skyDirection;

    if( sl.size() == 1 )
    {
        // One element is contained, that means decimal degrees
        QString deg = sl.at(0).trimmed();
        skyDirection = deg.right(1);

        if( skyDirection != "N" && skyDirection != "S" && skyDirection != "W" && skyDirection != "E" )
        {
            qWarning() << "OAP::parseCoordinatePart: wrong sky direction at line" << linenumber;
            return false;
        }

        deg = deg.left( deg.size() - 1 );

        double ddeg = deg.toDouble(&ok);

        if( ! ok )
        {
            qWarning() << "OAP::parseCoordinatePart: wrong coordinate value"
                       << line << "at line" << linenumber;
            return false;
        }

        value = ddeg;
    }
    else if( sl.size() == 2 )
    {
        // Two elements are contained
        QString deg = sl.at(0).trimmed();
        QString min = sl.at(1).trimmed();

        skyDirection = min.right(1);

        if( skyDirection != "N" && skyDirection != "S" && skyDirection != "W" && skyDirection != "E" )
        {
            qWarning() << "OAP::parseCoordinatePart: wrong sky direction at line" << linenumber;
            return false;
        }

        min = min.left( min.size() - 1 );

        double ddeg = deg.toDouble(&ok);
        double dmin = min.toDouble(&ok1);

        if( ! ok || ! ok1 )
        {
            qWarning() << "OAP::parseCoordinatePart: wrong coordinate value"
                       << line << "at line" << linenumber;
            return false;
        }

        value = (ddeg) + (dmin / 60);
    }
    else if( sl.size() == 3 )
    {
        // Three elements are contained
        QString deg = sl.at(0).trimmed();
        QString min = sl.at(1).trimmed();
        QString sec = sl.at(2).trimmed();

        skyDirection = sec.right(1);

        if( skyDirection != "N" && skyDirection != "S" && skyDirection != "W" && skyDirection != "E" )
        {
            qWarning() << "OAP::parseCoordinatePart: wrong sky direction" << skyDirection << "at line" << linenumber;
            return false;
        }

        sec = sec.left( sec.size() - 1 );

        double ddeg = deg.toDouble(&ok);
        double dmin = min.toDouble(&ok1);
        double dsec = sec.toDouble(&ok2);

        if( ! ok || ! ok1 || ! ok2 )
        {
            qWarning() << "OAP::parseCoordinatePart: wrong coordinate value"
                       << line << "at line" << linenumber;
            return false;
        }

        value = (ddeg) + (dmin/60) + (dsec/3600);
    }
    else
    {
        qWarning("OAP::parseCoordinatePart: unknown format! Line %d", linenumber);
        return false;
    }

    if( skyDirection == "N" )
    {
        lat = value;
        return true;
    }

    if( skyDirection == "S" )
    {
        lat = -value;
        return true;
    }

    if( skyDirection == "E" )
    {
        lon = value;
        return true;
    }

    if( skyDirection == "W" )
    {
        lon = -value;
        return true;
    }

    return false;
}

Coord OpenAirParser::getPathCenter() const
{
    return pathCenter;
}


bool OpenAirParser::parseCoordinate(QString& line, double &lat, double &lon)
{
    bool result=true;
    line=line.toUpper();

    int pos=0;
    lat=0;
    lon=0;

    QRegExp reg("[NSEW]");
    pos = reg.indexIn(line, 0);

    if( pos == -1 )
    {
        qWarning() << "OAP::parseCoordinate: line"
                   << linenumber
                   << "missing sky directions!";

        return false;
    }

    QString part1 = line.left(pos+1);
    QString part2 = line.mid(pos+1);

    result &= parseCoordinatePart(part1, lat, lon);
    result &= parseCoordinatePart(part2, lat, lon);

    return result;
}


int OpenAirParser::parse()
{
    AirSpace* aSpace = nullptr;
    AirSpacePrimitive* aSpacePrimitive = nullptr;
    Coord lastVCoord;
    int lastVDir = 1;
    linenumber = 0;
    QString airspaceName = "";

    for(int i=0;i<airspaces.count();i++)
    {
        QFile airSpaceF("airspace/"+airspaces.at(i));
        airSpaceF.open(QIODevice::ReadOnly|QIODevice::Text);

        while(!airSpaceF.atEnd())
        {
            QString line(airSpaceF.readLine().simplified());
            line = line.trimmed();

            linenumber ++;
            qDebug()<<"Line:"<<linenumber;


            //ignore comments
            if( line.startsWith( "*" ) || line.startsWith( "#" ) || line.isEmpty() )
            {
                continue;
            }

            //strip whitespaces near = sign
            line = line.replace(" =","+");
            line = line.replace("= ","=");

            line = line.split( '*' )[0];
            line = line.split( '#' )[0];

            QString command = line.mid(0,line.indexOf(" "));
            QString data = line.mid(line.indexOf(" ")+1);

            if(command == "AC")
            {
                if(aSpace != nullptr)
                {
                    //we have an airspace and we have to add it to the list
                    //TODO add to airspace list
                    if(aSpacePrimitive != nullptr)
                    {
                        aSpace->aPrimitives.append(*aSpacePrimitive);
                        aSpacePrimitive = nullptr;
                    }

                    airSpaceUnits.append(*aSpace);

                    //create a new object to hold the newly discovered airspace
                    //TODO save the last airspace when EOF is encountered
                    aSpace = new AirSpace;
                }
                else
                {
                    //this is the first airspace
                    aSpace = new AirSpace;
                }

                aSpace->aClass = AirSpaceClass::UNK;

                if((QString)(data).trimmed().toLower() == "r")
                    aSpace->aClass = AirSpaceClass::R;
                if((QString)(data).trimmed().toLower() == "g")
                    aSpace->aClass = AirSpaceClass::G;
                if((QString)(data).trimmed().toLower() == "q")
                    aSpace->aClass = AirSpaceClass::Q;
                if((QString)(data).trimmed().toLower() == "p")
                    aSpace->aClass = AirSpaceClass::P;
                if((QString)(data).trimmed().toLower() == "a")
                    aSpace->aClass = AirSpaceClass::A;
                if((QString)(data).trimmed().toLower() == "b")
                    aSpace->aClass = AirSpaceClass::B;
                if((QString)(data).trimmed().toLower() == "c")
                    aSpace->aClass = AirSpaceClass::C;
                if((QString)(data).trimmed().toLower() == "d")
                    aSpace->aClass = AirSpaceClass::D;
                if((QString)(data).trimmed().toLower() == "gp")
                    aSpace->aClass = AirSpaceClass::GP;
                if((QString)(data).trimmed().toLower() == "ctr")
                    aSpace->aClass = AirSpaceClass::CTR;
                if((QString)(data).trimmed().toLower() == "w")
                    aSpace->aClass = AirSpaceClass::W;
                if((QString)(data).trimmed().toLower() == "tmz")
                    aSpace->aClass = AirSpaceClass::TMZ;
                if((QString)(data).trimmed().toLower() == "rmz")
                    aSpace->aClass = AirSpaceClass::RMZ;

                if(airspaceName != "")
                {
                    aSpace->aName = airspaceName;
                    airspaceName="";
                }
            }
            if(command == "AN")
            {
                if(aSpace == nullptr)
                {
                    airspaceName = data;
                }
                else
                {
                    aSpace->aName = data;
                }
            }

            if(command == "AH")
            {
                aSpace->aHiStr = data;
                AirSpaceHeightType heightType;
                uint units;
                parseAltitude(data,heightType,units);
                //qDebug()<<"Altitude:"<<heightType<<" "<<units;
                aSpace->setAHiType(heightType);
                aSpace->setAHiUnits(units);
                aSpace->setAHiMeters(heightMeters(heightType,units));
                //check if altitude H is not compared to ground level and set it to 10 KM if required (if referenced to GND)
                if(aSpace->getAHiMeters()<0)aSpace->setAHiMeters(10000);
            }

            if(command == "AL")
            {
                aSpace->aLowStr = data;
                AirSpaceHeightType heightType;
                uint units;
                parseAltitude(data,heightType,units);
                //qDebug()<<"Altitude:"<<heightType<<" "<<units;
                aSpace->setALowType(heightType);
                aSpace->setALowUnits(units);
                aSpace->setALowMeters(heightMeters(heightType,units));

            }


            if(command == "V")
            {
                QStringList arguments=data.split('=');
                if (arguments.count()<2)
                    return linenumber;

                QString variable=arguments[0].simplified().toUpper();
                QString value=arguments[1].simplified();
                if(variable=="X")
                {
                    //we expect a coodonate
                    double lat, lon;
                    parseCoordinate(value,lat,lon);
                    lastVCoord.setLat(lat);
                    lastVCoord.setLon(lon);
                }
                if(variable=="D")
                {
                    if(value=="-")
                    {
                        lastVDir = -1;
                    }
                    else
                    {
                        lastVDir = 1;
                    }
                }
            }

            if(command == "DP")
            {
                double lat, lon;
                parseCoordinate(data,lat,lon);
                lastVCoord.setLat(lat);
                lastVCoord.setLon(lon);
                Coord c;
                c.setLat(lat);

                c.setLon(lon);

                if(aSpacePrimitive == nullptr)
                {
                    aSpacePrimitive = new AirSpacePrimitive();
                    aSpacePrimitive->type = AirSpacePrimitivesType::POLY;
                    aSpacePrimitive->point.append(c);

                }
                else
                {
                    aSpacePrimitive->point.append(c);
                }
            }
            else
            {
                if(aSpacePrimitive != nullptr && command == "AC")
                {
                    aSpace->aPrimitives.append(*aSpacePrimitive);
                    aSpacePrimitive = nullptr;
                }
                else
                {
                    if(command == "DB")
                    {
                        QStringList arguments = data.split(',');
                        if (arguments.count()<2)
                        return linenumber;
                        double lat,lon;
                        parseCoordinate(arguments[0],lat,lon);
                        Coord start;
                        start.setLat(lat);
                        start.setLon(lon);

                        parseCoordinate(arguments[1],lat,lon);
                        Coord stop;
                        stop.setLat(lat);
                        stop.setLon(lon);


                        Angle start_bearing = Angle::Radians(lastVCoord.bearingTo(start));
                        //Angle start_bearing = Angle::Radians(M_PI/2);

                        double radius_m = lastVCoord.distanceKm(start)*1000;

                        qDebug()<<radius_m;

                        double rotation = lastVDir;
                        double step = Angle::Degrees(rotation).Degrees();
                        auto threshold = 1.5;

                        if(radius_m < 1000)
                        {
                            step*=15;
                            threshold*=15;
                        }else if(radius_m < 5000)
                        {
                            step*=5;
                            threshold*=5;
                        }


                        Angle end_bearing = Angle::Radians(lastVCoord.bearingTo(stop));
                        //Angle end_bearing = Angle::Radians(-M_PI/2);

                        if (rotation > 0) {
                            while (end_bearing < start_bearing)
                                end_bearing += Angle::FullCircle();
                        } else if (rotation < 0) {
                            while (end_bearing > start_bearing)
                                end_bearing -= Angle::FullCircle();
                        }

                        if(aSpacePrimitive == nullptr)
                        {
                            aSpacePrimitive = new AirSpacePrimitive();
                            aSpacePrimitive->type = AirSpacePrimitivesType::POLY;
                        }

                        aSpacePrimitive->point.append(start);


                        while ((end_bearing - start_bearing).AbsoluteDegrees() > threshold) {
                            start_bearing += Angle::Degrees(step);

                            //qDebug()<<"(end_bearing - start_bearing).AbsoluteDegrees()"<<(end_bearing - start_bearing).AbsoluteDegrees();

                            //points.push_back(FindLatitudeLongitude(center, start_bearing, radius));
                            Coord intermediate(lastVCoord);
                            intermediate.translateTo(radius_m , start_bearing.Radians());
                            aSpacePrimitive->point.append(intermediate);

                            //qDebug()<<start_bearing.Degrees();
                        }

                        //aSpacePrimitive->point.append(lastVCoord);
                        lastVDir = 1;

                        aSpacePrimitive->point.append(stop);
                    }
                    if(command == "DA")
                    {
                        QStringList arguments = data.split(',');
                        double radiusNm = arguments[0].toDouble();
                        double angleStart = arguments[1].toDouble();
                        double angleEnd = arguments[2].toDouble();

                        Angle start_bearing = Angle::Degrees(angleStart);
                        //Angle start_bearing = Angle::Radians(M_PI/2);

                        double radius_m = UnitConverter::MiTokm(radiusNm)*1000;

                        qDebug()<<radius_m;

                        double rotation = lastVDir;
                        double step = Angle::Degrees(rotation).Degrees();
                        auto threshold = 1.5;

                        if(radius_m < 1000)
                        {
                            step*=15;
                            threshold*=15;
                        }else if(radius_m < 5000)
                        {
                            step*=5;
                            threshold*=5;
                        }


                        Angle end_bearing = Angle::Degrees(angleEnd);
                        //Angle end_bearing = Angle::Radians(-M_PI/2);

                        if (rotation > 0) {
                            while (end_bearing < start_bearing)
                                end_bearing += Angle::FullCircle();
                        } else if (rotation < 0) {
                            while (end_bearing > start_bearing)
                                end_bearing -= Angle::FullCircle();
                        }

                        if(aSpacePrimitive == nullptr)
                        {
                            aSpacePrimitive = new AirSpacePrimitive();
                            aSpacePrimitive->type = AirSpacePrimitivesType::POLY;
                        }

                        Coord intermediate(lastVCoord);
                        intermediate.translateTo(radius_m , start_bearing.Radians());
                        aSpacePrimitive->point.append(intermediate);

                        while ((end_bearing - start_bearing).AbsoluteDegrees() > threshold) {
                            start_bearing += Angle::Degrees(step);

                            //qDebug()<<"(end_bearing - start_bearing).AbsoluteDegrees()"<<(end_bearing - start_bearing).AbsoluteDegrees();

                            //points.push_back(FindLatitudeLongitude(center, start_bearing, radius));
                            Coord intermediate(lastVCoord);
                            intermediate.translateTo(radius_m , start_bearing.Radians());
                            aSpacePrimitive->point.append(intermediate);

                            //qDebug()<<start_bearing.Degrees();
                        }

                        Coord intermediateLast(lastVCoord);
                        intermediateLast.translateTo(radius_m , end_bearing.Radians());
                        aSpacePrimitive->point.append(intermediateLast);

                        //aSpacePrimitive->point.append(lastVCoord);
                        lastVDir = 1;
                    }
                }
            }

            if(command == "DC")
            {
                aSpacePrimitive = new AirSpacePrimitive();
                aSpacePrimitive->type = AirSpacePrimitivesType::CIRCLE;
                aSpacePrimitive->radiusMeters = ((QString)data).toDouble() * 1852;
                aSpacePrimitive->x = lastVCoord;
                aSpace->aPrimitives.append(*aSpacePrimitive);
                aSpacePrimitive = nullptr;
            }

        }
        //add the lastt primitive to the airspace
        if(aSpacePrimitive != nullptr)
        {
            aSpace->aPrimitives.append(*aSpacePrimitive);
            aSpacePrimitive = nullptr;
        }
        //add the last airspace to the list
        if(aSpace != nullptr) airSpaceUnits.append(*aSpace);
        aSpace = nullptr;
        airSpaceF.close();
    }
    pathMetersToPixel = 0;
    return 0;
}

double AirSpace::getALowMeters() const
{
    return aLowMeters;
}

void AirSpace::setALowMeters(double value)
{
    aLowMeters = value;
}

double AirSpace::getAHiMeters() const
{
    return aHiMeters;
}

void AirSpace::setAHiMeters(double value)
{
    aHiMeters = value;
}

double AirSpace::getALowUnits() const
{
    return aLowUnits;
}

void AirSpace::setALowUnits(double value)
{
    aLowUnits = value;
}

double AirSpace::getAHiUnits() const
{
    return aHiUnits;
}

void AirSpace::setAHiUnits(double value)
{
    aHiUnits = value;
}

AirSpaceHeightType AirSpace::getALowType() const
{
    return aLowType;
}

void AirSpace::setALowType(const AirSpaceHeightType &value)
{
    aLowType = value;
}

AirSpaceHeightType AirSpace::getAHiType() const
{
    return aHiType;
}

void AirSpace::setAHiType(const AirSpaceHeightType &value)
{
    aHiType = value;
}
