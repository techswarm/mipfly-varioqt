/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef WIND_HPP
#define WIND_HPP

#include "circRegression/data.h"

#include <QObject>

class NmeaParser;
class WindManager;

class Wind: public QObject
{
    Q_OBJECT
public:
    Wind(const NmeaParser& parser, const WindManager& mgr, QObject *parent = 0);
	Wind(int numReadings);
	~Wind();
	
	float getWindDir();
	float getWindSpeed();
	
	void windTester(int degree,float windSpeedX, float windSpeedY, int displayEvery);
	
public slots:
    void push(qreal flightSpeed,qreal flightHeading);

    void setNumReadings(int);
    void setNumAverage(int);
    void setDataSpread(int);
    void setRmsError(float);

signals:
    void windDir(qreal dir);
    void windSpeed(qreal speed);

private:
    int _bufPos = 0;
    Data _speedData;
    int _numAverage = 10;

    float _rmsError = 5;

    qreal _dataSpreed = 60;

    qreal _windSpeedX = 0;
    qreal _windSpeedY = 0;

    bool dataSetValidAfterWindCompute(float x, float y, float speed);

    void pushWindXY(qreal speedX, qreal speedY);
    int Process();
    float determineSpread();
    void resetWindSpeed();

    int processPostscale = 25;
};

#endif // WIND_HPP
