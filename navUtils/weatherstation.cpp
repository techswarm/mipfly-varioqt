/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "weatherstation.h"
#include "VarioQt.h"
#include <QDateTime>
#include <iostream>
#include <QDebug>

WeatherStation::WeatherStation(QObject *parent) : QObject(parent)
{
    manager = new QNetworkAccessManager();
    QObject::connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(managerFinished(QNetworkReply*)));
    updateAccount();
    sendRequest();
}

void WeatherStation::updateAccount()
{
    QSettings settings;
    key = settings.value("accountKey","").toString();
    id = settings.value("accountId","").toString();
}

void WeatherStation::sendRequest()
{
    Coord locationNow = VarioQt::instance().getGpsParser()->coord();
    if(!managerFinishedState && id!="")return;
    if(locationNow.getLat()==0)return;

    QString url = Vario::host + "weather";
    url+="?lat="+QString::number(locationNow.getLat());
    url+="&lon="+QString::number(locationNow.getLon());
    url+="&id="+id;
    url+="&apiKey="+key;

    request.setUrl(QUrl(url));
    managerFinishedState = false;
    manager->get(request);
}

void WeatherStation::update()
{
    int currentTimestamp = QDateTime::currentSecsSinceEpoch();
    if(currentTimestamp - timestamp > 60*5)sendRequest();
}

float WeatherStation::getWindDir()
{
    return windDir;
}

float WeatherStation::getGust()
{
    return gust;
}

float WeatherStation::getSpeed()
{
    update();
    return speed;
}

void WeatherStation::setSpeed(float value)
{
    update();
    speed = value;
}

QString WeatherStation::getUnit()
{
    return unit;
}

QString WeatherStation::getStationName() const
{
    return stationName;
}

void WeatherStation::managerFinished(QNetworkReply *reply)
{
    managerFinishedState = true;
    if (reply->error()) {
        qDebug() << "Error: " << reply->errorString();
        return;
    }

    QString answer = reply->readAll();
    qDebug()<<answer;
    //reply format 338,Clopotiva,2018-05-20,18:12:14,1.7,2.2,m/s,322,71.8,1017,499,0,13.1

    QStringList tokens = answer.split(",");

    if(tokens.count()>=7)
    {

        stationName = tokens.at(1);
        speed = ((QString)(tokens.at(4))).toFloat();
        gust = ((QString)(tokens.at(5))).toFloat();

        unit = tokens.at(6);

        windDir = ((QString)(tokens.at(7))).toFloat();

        timestamp = QDateTime::currentSecsSinceEpoch();
    }
    VarioQt::instance().getNetStats()->resetTimeout();
}
