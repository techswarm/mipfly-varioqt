/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef E6B_H
#define E6B_H
#include <cmath>
#include <climits>
using namespace std;


class E6B
{
public:
    E6B(int targetCourse,int trueAirSpeed, int windDir, int windSpeed);
    static int invalid;
    void setTargetCourse( int value);
    void setTrueAirSpeed( int value);
    void setWindDirection( int value);
    void setWindSpeed( int value);

    int getTargetCourse();
    int getTrueAirSpeed();
    int getWindDirection();
    int getWindSpeed();

    int getWindCorrectionAngle();
    int getHeading();
    int getGroundSpeed();

private:
    int _targetCourse;
    int _trueAirSpeed;
    int _windDir;
    int _windSpeed;
    double C = M_PI / 180;
    bool valid = true;

    void setValid(float swc) {
      valid = !(fabs(swc) > 1 || !getTrueAirSpeed());
    };

    double getCrs () {
        return C * (getTargetCourse() % 360);
    };
    double getWd() {
        return C * getWindDirection();
    };
    float getSwc() {
        if(getTrueAirSpeed()==0){
            setValid(0);
            return 0;
        }
        float swc = ((float) getWindSpeed() / getTrueAirSpeed()) * sin(getWd() - getCrs());
        setValid(swc);
        return swc;
    };
    double getHd() {
        double hd = getCrs() + asin(getSwc());
        return (hd < 0) ? (hd + 2 * M_PI) : ((hd > 2 * M_PI) ? (hd - 2 * M_PI) : hd);
    };
};

#endif // E6B_H
