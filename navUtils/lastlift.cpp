/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "lastlift.h"
#include <QTime>
#include "navUtils/variocompute.h"
#include <QDebug>


LastLift::LastLift(QObject *parent, VarioCompute* VC, double dacayCmps, double minCmps) : QObject(parent)
{
    _VC = VC;
    _decayCmps = dacayCmps;
    _minCmps = minCmps;
    _time.start();
}

void LastLift::pushCoord(Coord location)
{
    //compute last termal relevance
    double dx = _time.elapsed();
    _time.restart();
    if(_lastDecayed > 0)
    {
        dx/=1000;
        _lastDecayed -= _decayCmps * dx;
//        qDebug()<<_lastDecayed;
    }
    //get actual average lift
    double averageLift = _VC->getAverageCmps();

    if(averageLift > _lastDecayed && averageLift > _minCmps)
    {
        //save the new termal
        _lastLiftCoordonates = location;
        _lastLiftAverageCmps = _lastDecayed = averageLift;
    }

    emit liftDir(location.bearingTo(_lastLiftCoordonates));
    emit liftDist(location.distanceKm(_lastLiftCoordonates)*1000);
}

Coord LastLift::getCoordonates()
{
    return _lastLiftCoordonates;
}

double LastLift::getAverageLift()
{
    return _lastLiftAverageCmps;
}

