/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef OPENAIRPARSER_H
#define OPENAIRPARSER_H

#include <QString>
#include <QList>
#include <QFile>
#include "navUtils/coord.hpp"
#include <QPainterPath>


enum AirSpaceClass
{
    R,
    Q,
    P,
    A,
    B,
    C,
    D,
    GP,
    G,
    CTR,
    W,
    TMZ,
    RMZ,
    UNK
};

enum AirSpacePrimitivesType
{
    POLY,
    CIRCLE,
    ARCH
};

enum AirSpaceHeightType
{
    NotSet,
    MSL,
    GND,
    UNLTD,
    FL,
    STD
};

enum AirSpaceDistanceType
{
    OK,
    NEAR,
    VERYNEAR,
    VIOLATED
};



class AirSpacePrimitive
{
private:


public:

    AirSpacePrimitivesType type;
    Coord x;
    double radiusMeters;
    QList<Coord> point;

    AirSpacePrimitive();
};




class AirSpace
{
private:
    double aLowMeters = -1000;
    double aHiMeters = -1000;
    double aLowUnits, aHiUnits;
    AirSpaceHeightType aLowType, aHiType;
    bool drawable = false;
    AirSpaceDistanceType distance = AirSpaceDistanceType::OK;

public:
    AirSpace();
    AirSpaceClass aClass;
    QString aName;
    QString aLowStr;
    QString aHiStr;
    QList<AirSpacePrimitive> aPrimitives;
    void computePath(Coord center, float _metersToPixels);
    void computePathAndInset(Coord center,int _metersToPixels);
    void computeInset();

    void setDrawable(bool draw = true);
    bool isDrawable();

    double getALowMeters() const;
    void setALowMeters(double value);
    double getAHiMeters() const;
    void setAHiMeters(double value);
    double getALowUnits() const;
    void setALowUnits(double value);
    double getAHiUnits() const;
    void setAHiUnits(double value);
    AirSpaceHeightType getALowType() const;
    void setALowType(const AirSpaceHeightType &value);
    AirSpaceHeightType getAHiType() const;
    void setAHiType(const AirSpaceHeightType &value);
    AirSpaceDistanceType getDistance() const;
    void setDistance(const AirSpaceDistanceType &value);

    QPainterPath path;
    QPainterPath inset;
};




class OpenAirParser
{
private:
    QStringList airspaces;
    void parseAltitude(QString &line, AirSpaceHeightType &type, uint &alt);
    int heightMeters(AirSpaceHeightType type, uint units);
    float pathMetersToPixel = 0;
    Coord pathCenter;
    bool repathInsetIsBuisy = false;
    bool repathInsetStop = false;

    bool parseCoordinatePart(QString &line, double &lat, double &lon);
    bool parseCoordinate(QString &line, double &lat, double &lon);
    int linenumber;
public:
    static QString airSpaceClassToString(AirSpaceClass aclass);
    QList<AirSpace> airSpaceUnits;
    OpenAirParser(QStringList filenames);
    int parse();
    bool repathRequired(Coord center, float metersToPixels);

    void repathPath(Coord center, float metersToPixels);
    void repathInset();



    Coord getPathCenter() const;
};

#endif // OPENAIRPARSER_H
