/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MAINUI_H
#define MAINUI_H

#include <QWidget>
#include <ctime>

#include "utils/vario_common.h"
#include "navUtils/coord.hpp"
#include "navUtils/lastlift.h"
#include "settings/namemanager.h"
#include "GlideDetails.h"
#include "menudialog.h"

#ifdef MAKE_TESTS
#include "threads/sendcorrectionsthread.h"
#endif

#include "testing/gpioemulator.h"
#include "threads/inputThread.h"

#include "libQDeviceWatcher/qdevicewatcher.h"
#include "ui/dynamicui.h"
#include "ui/usbdialog.h"
#include "ChooseInput.h"
#include "MenuContainer.h"
#include "navUtils/weatherstation.h"
#include "GPIO/gpiowatcher.h"
#include "NMEA/rawlogger.h"

namespace Ui {
class MainUI;
}

class NmeaParser;
class Glide;
class Wind;
class VarioCompute;
class WindManager;
class GPIOListenThread;
class LocationManager;
class TakeoffThread;
class Fb2SPIThread;
class GPSThread;


class MainUI : public QWidget
{
    Q_OBJECT

public:
    DynamicUI* dynamicUI;

    GPIOEmulator gpioEmulator;

    MainUI(QWidget *parent = 0);
    ~MainUI();

    void connectTo(const GPIOListenThread &btnThread);
    void connectTo(const GPIOWatcher &btnThread);

    void hideUSBDialog();

    void connectTo(const InputThread &btnThread);

    void setConfirmTurnOn(bool value);

public slots:
    void setPilot(QString pilot);
    void setFix(int fix);
    void setTemp(int temp);
    void setHDiff(int hdiff);
    void setGlide(qreal glide);
    void setGndSpeed(qreal gndSpeed);
    void setTAS(qreal tas);
    void setAltitude(int altitude);
    void setAvgClimb(double avgClimb);
    void setPressureAltitude(int pressureAltitude);

    void setStartCoord(Coord coord);

    void setInitAltitude(int alt);
    void setDistance(Coord crtCoord);
    void setDistanceThermal(int distanceMeters);

    void resetTime();

    void gpioPressed(int btn);
    void gpioLongPressed(int btn);

    // start and stop displaying elapsed time (when takeoff and land events)
    void startDisplayElapsed();
    void stopDisplayElapsed();

    void deviceAdded(const QString& dev);
    void deviceRemoved(const QString& dev);

    void resetInitValues();

    void setGpsCoord(Coord coord);

    void landedDetected();
    void takeoffDetected();

    void displayMessageAlarm(QString message);
    void showNavMenu();
    void showFAIMenu();

signals:
    void mainGpioPressed(int btn);
    void mainGpioLongPressed(int btn);

protected:
    void keyPressEvent(QKeyEvent *event) Q_DECL_OVERRIDE;
    void timerEvent(QTimerEvent *event) Q_DECL_OVERRIDE;

private:
    Ui::MainUI *ui;

    bool confirmTurnOn = false;

    MenuDialog menuDialog;

    NameManager *nameMgr;
    USBDialog *usbDialog = nullptr;

    RawLogger rawLogger;
    int menuHideTimeout = 0;
    int startPage_ = 1;
    int setHDiffPostscale=0;
    int altitude_;
    Coord coord_;
    int fix_ = 0;

    time_t start, crtTimer;
    Coord startCoord;

    int uiTimerId = 0;

    int startAlt;
    bool displayElapsed = false;

    int pressureAltitudeAsl_;
    int pressureAltitudeAslOffset_ = 0;
    int pressureAltitudeRef1Offset_ = 0;
    double pressureAltitudeAslOffsetAtVdop_ = 99.0;

    float gndSpeed = 0;


    QDeviceWatcher watcher;

    WeatherStation _weatherStation;

    ChooseInput *turnOnConfirm;
    ChooseInput *turnOffConfirm;
    int turnOnTimeout = 180;

    void performWeatherStationTasks();

    bool timerTaskFlag = false;

    GPIOPin powerPin;

#ifdef MAKE_TESTS
    SendCorrectionsThread &sndt;
#endif

    int tickCount = 0;

    void setupFonts();
    void setupUserInterface();
    void hideMenuDialog();
    void showEvent(QShowEvent *e) Q_DECL_OVERRIDE;
    void hideEvent(QHideEvent *e) Q_DECL_OVERRIDE;
    void performTaskTasks();
};

#endif // MAINUI_H
