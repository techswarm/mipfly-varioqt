/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GLIDESMANAGER_H
#define GLIDESMANAGER_H

#include <QDate>
#include <QList>
#include <QString>

#include <ctime>
#include <functional>

class LocationsService;
class NameManager;

namespace Vario
{
    struct GlideSession
    {
        int duration;
        QDate date;
        QString fileName;
        QString locName;
        QString filePath;
    };
}


class GlidesManager
{
public:
    GlidesManager(const LocationsService& locSvc, const NameManager& nameMgr);
#if 0
    void glidesForPilot(const QString &pilot);
    void saveGlidesForPilot(const QString &pilot) const;

    void startSession();
    void endSession();
#endif

    const QList<Vario::GlideSession>& getGlides() const;
    void glidesFromDir(const QString &pilot);

    void iterateGlides(const QString &pilot, std::function<void(const QString&, const QString&)> callback);
    void iterateGlides(std::function<void(const QString&, const QString&)> callback);
    QList<Vario::GlideSession> glidesList;


private:
    time_t startTime;

    const LocationsService& locService;
    const NameManager& nameMgr;

//    QString getFilePath(const QString &pilot) const;
};

#endif // GLIDESMANAGER_H
