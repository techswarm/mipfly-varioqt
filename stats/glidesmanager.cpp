/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "glidesmanager.h"

#include <QFile>
#include <QFileInfo>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>

#include <iostream>

#include <dirent.h>

#include "utils/vario_common.h"
#include "Geo/locationsservice.h"
#include "utils/utils.h"
#include "settings/namemanager.h"

#include <QDebug>

using Vario::GlideSession;
using Vario::DATE_FORMAT;
using Vario::DEFAULT_PILOT;

GlidesManager::GlidesManager(const LocationsService &locSvc, const NameManager &nameMgr)
    : locService(locSvc), nameMgr(nameMgr)
{
    const QString &name = nameMgr.getName();
//    glidesForPilot(name.isEmpty() ? DEFAULT_PILOT : name);
    glidesFromDir(name.isEmpty() ? DEFAULT_PILOT : name);  
}

const QList<Vario::GlideSession> &GlidesManager::getGlides() const {
    return glidesList;
}

void GlidesManager::glidesFromDir(const QString &pilot) {
    auto appendGlideFunction = [this](const QString &fullPath, const QString &name) {
        GlideSession gld;
        gld.locName = "-UNKNOWN-";
        gld.date = QDate::fromString(name.left(10), "yyyy-MM-dd");
        gld.filePath = fullPath;
        QFile f(fullPath);

        f.open(QIODevice::ReadOnly | QIODevice::Text);

        QFileInfo fileInfo(f.fileName());
        gld.fileName = fileInfo.fileName();

        QTextStream in(&f);
        QString line;
        int nr = 0;
        while(!in.atEnd() && nr <= 15) {
            line = in.readLine();
            if (line.at(0) == QChar('L')) {
                int idx;
                if ((idx = line.lastIndexOf("LOC")) != -1) {
                    gld.locName = line.right(idx);
                    break;
                }
            }
            nr++;
        }
        f.close();

        glidesList.append(gld);
    };

    iterateGlides(pilot, appendGlideFunction);
}

void GlidesManager::iterateGlides(const QString &pilot, std::function<void (const QString &, const QString &)> callback)
{
    DIR *dir;
    struct dirent *dEnt;
    struct dirent *dSubEnt;
    unsigned char isFile =0x8;

    DIR *subDir;

    QString path("flights");

    QByteArray ba = path.toLocal8Bit();

    if ((dir = opendir(ba.data())) == NULL) {
        qWarning() << "Error opening directory " << pilot;
        return;
    }
    while ((dEnt = readdir(dir)) != NULL) {
        if (dEnt->d_type == DT_DIR && strcmp(dEnt->d_name, ".") && strcmp(dEnt->d_name, "..")) {
            QString subPath(path);
            subPath.append("/");
            subPath.append(dEnt->d_name);

            QByteArray baSub = subPath.toLocal8Bit();

            if ((subDir = opendir(baSub.data())) == NULL) {
                qWarning() << "Error opening subdirectory " << subPath;
                return;
            }
            while ((dSubEnt = readdir(subDir)) != NULL) {
                int len = strlen(dSubEnt->d_name);
                const char *extension = &dSubEnt->d_name[len-3];
                if (dSubEnt->d_type == isFile && strcmp(dSubEnt->d_name, ".") && strcmp(dSubEnt->d_name, "..") && !strcasecmp(extension,"igc" )) {
                    QString igcPath(subPath);
                    igcPath.append("/");
                    igcPath.append(dSubEnt->d_name);
                    callback(igcPath, QString(dSubEnt->d_name));
                }
            }
            closedir(subDir);
        }
    }
    closedir(dir);
}

void GlidesManager::iterateGlides(std::function<void(const QString&, const QString&)> callback)
{
    const QString &name = nameMgr.getName();
    iterateGlides(name.isEmpty() ? DEFAULT_PILOT : name, callback);
}

#if 0
void GlidesManager::glidesForPilot(const QString &pilot) {
    QFile glidesFile(getFilePath(pilot));

    if (glidesFile.exists()) {
        if (glidesFile.open(QIODevice::ReadOnly)) {
                QByteArray data = glidesFile.readAll();
                QJsonObject rootObj = (QJsonDocument::fromJson(data)).object();
                QJsonArray locArray = rootObj[KEY_GLIDES].toArray();
                for (auto it = locArray.begin(); it != locArray.end(); ++it) {
                    QJsonObject glideObj = (*it).toObject();

                    GlideSession glide;
                    glide.duration = glideObj[KEY_DURATION].toInt();
                    glide.locName = glideObj[KEY_LOCATION].toString();
                    glide.date = QDate::fromString(glideObj[KEY_DATE].toString(), DATE_FORMAT);

                    glidesList.append(glide);
                }
                glidesFile.close();
        } else {
            std::cerr << "Could not open glides.json for reading saved glides" << std::endl;
        }
    }
}


void GlidesManager::saveGlidesForPilot(const QString &pilot) const {
    mkdirIfNotExists(pilot.toStdString());
    QFile glidesFile(getFilePath(pilot));

    QJsonArray gldArray;

    for (auto glide : glidesList) {
        QJsonObject glideObj;
        glideObj[KEY_LOCATION] = glide.locName;
        glideObj[KEY_DATE] = glide.date.toString(DATE_FORMAT);
        glideObj[KEY_DURATION] = glide.duration;

        gldArray.append(glideObj);
    }

    QJsonObject rootObj;
    rootObj[KEY_GLIDES] = gldArray;

    QJsonDocument doc(rootObj);

    qDebug() << "Saving to " << getFilePath(pilot);

    if (glidesFile.open(QIODevice::WriteOnly | QIODevice::Text)) {
        glidesFile.write(doc.toJson());
        glidesFile.close();
    } else {
        std::cerr << "Could not open file glides.json for writing" << std::endl;
    }
}

void GlidesManager::startSession() {
    startTime = time(NULL);
}

void GlidesManager::endSession() {
    time_t endTime = time(NULL);
    double diff = std::difftime(endTime, startTime);

    GlideSession glide;
    glide.date = QDate::currentDate();
    glide.locName = locService.currentLocation().name;
    glide.duration = static_cast<int>(diff);

    glidesList.append(glide);

    const QString &name = nameMgr.getName();
    saveGlidesForPilot(name.isEmpty() ? DEFAULT_PILOT : name);
}

QString GlidesManager::getFilePath(const QString &pilot) const {
    QString path = pilot;
    path.append("/glides.json");
    return path;
}
#endif
