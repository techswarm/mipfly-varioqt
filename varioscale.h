/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VARIOSCALE_H
#define VARIOSCALE_H

#include <QWidget>
#include "navUtils/variocompute.h"

namespace Ui {
class VarioScale;
}

class VarioScale : public QWidget
{
    Q_OBJECT

    int _X = 20;
    int _Width = 40;
    int _height = 0;
    int _start;
    qreal _avg;
    qreal _min;
    qreal _max;
    QString unit = "";

public:
    explicit VarioScale(QWidget *parent = 0);
    void setUnit(QString u);
    ~VarioScale();

public slots:
    void setVSpeed(int cmps);
    void setAverage(qreal avg);
    void setMax(qreal max);
    void setMin(qreal min);

protected:
    void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;

private:
    Ui::VarioScale *ui;

    VarioCompute *vComp;

};

#endif // VARIOSCALE_H
