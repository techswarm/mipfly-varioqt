/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "rawlogger.h"
#include <QFile>
#include <QString>
#include <QSettings>

RawLogger::RawLogger()
{
    logFile.setFileName("raw.log");
}

RawLogger::~RawLogger()
{
    if(logFile.isOpen())
    {
        logFile.close();
    }
}

void RawLogger::open()
{
    QSettings settings;
    bool rawLogging = settings.value("rawLogging",false).toBool();
    if(!logFile.isOpen() && rawLogging)
    {
        logFile.open(QFile::WriteOnly|QFile::Truncate);
    }
}

void RawLogger::doMessageLog(QString message)
{
    if(logFile.isOpen())logFile.write(message.toUtf8());
}
