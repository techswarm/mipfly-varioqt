/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "NMEAParser.h"
#include "utils/utils.h"

#include <cstring>
#include <cstdlib>
#include <cmath>
#include <cstdio>
#include <time.h>

#include <iostream>
#include <string>

#include <QtDebug>
#include <QSettings>
#include <utils/Version.h>
#include <QMutexLocker>

using namespace std;


int NmeaParser::pushCh(unsigned char ch)
{
    if(ch < ' ' || ch > 127)
    {
        if(ch == '\r' || ch == '\n')
        {
        }
        else
        {
            _recPos = 0;
        }
    }
    if(_recPos>400)_recPos = 0;
    try {
        _messageBytes[_recPos++] = ch;

        if(ch == '\n') {
            _messageBytes[_recPos] = 0;

            _recPos = 0;

            _tokens = explode(std::string(_messageBytes), ",");

            if(_tokens.size()<1)return 1;

            if(_tokens[0] == "$GNRMC") {
                emit messageReceived(QString(_messageBytes));
                comOk = true;
                processGNRMC();
                return 1;
            }
            if(_tokens[0] == "$GVAR") {
                emit messageReceived(QString(_messageBytes));
                processGVAR();
                return 1;
            }

            if(_tokens[0] == "$PUBX") {
                processPubx();
                qDebug()<<_messageBytes;
                return 1;
            }

            if(_tokens[0] == "$GSN") {
                processGSN();
                return 1;
            }

            if(_tokens[0] == "$GNGSA") {
                //emit messageReceived(QString(_messageBytes));
                processGNGSA();
                return 1;
            }
            if (_tokens[0] == "$GNGGA") {
                emit messageReceived(QString(_messageBytes));
                processGNGGA();
                return 1;
            }
            if (_tokens[0] == "$GVER") {
                processGVER();
                return 1;
            }

        }
    } catch(...) {

    }
    return 0;
}

int NmeaParser::pushLine(char messageBytes[] )
{
    try {
        _tokens = explode(std::string(messageBytes), ",");

        if(_tokens.size()<1)return 1;

        if(_tokens[0] == "$GNRMC") {
            emit messageReceived(QString(messageBytes));
            comOk = true;
            processGNRMC();
            return 1;
        }
        if(_tokens[0] == "$GVAR") {
            emit messageReceived(QString(messageBytes));
            processGVAR();
            return 1;
        }

        if(_tokens[0] == "$PUBX") {
            processPubx();
            qDebug()<<messageBytes;
            return 1;
        }

        if(_tokens[0] == "$GSN") {
            processGSN();
            return 1;
        }

        if(_tokens[0] == "$GNGSA") {
            //emit messageReceived(QString(_messageBytes));
            processGNGSA();
            return 1;
        }
        if (_tokens[0] == "$GNGGA") {
            emit messageReceived(QString(messageBytes));
            processGNGGA();
            return 1;
        }
        if (_tokens[0] == "$GVER") {
            processGVER();
            return 1;
        }
    } catch(...) {

    }
    return 0;
}

static int extractNumber(string str, int i)
{
    char buf[3];
    if(str.length()<3)return 0;
    buf[0] = str[i];
    buf[1] = str[i+1];
    buf[2] = '\0';
    return std::stod(buf);
}

int datePartFromStr(std::string s, int i) {
    char buf[3];
    if(s.length()<3)return 0;
    buf[0] = s[i];
    buf[1] = s[i+1];
    buf[2] = '\0';
    return atoi(buf);
}

static double str2double(const std::string &str) {
    double val = 0;
    std::istringstream ss(str);
    ss >> val;
    return val;
}

QString NmeaParser::addChecksum(QString msg)
{
    int16_t cheksum = 0;
    QByteArray buf = msg.toUtf8();
    for(int i=1;i<msg.length();i++)
    {
        cheksum ^=  ((unsigned char)buf[i]);
    }
    msg+="*"+QString::number(cheksum,16);
    return msg;
}

int NmeaParser::getTecCoefficient() const
{
    return _tecCoefficient;
}

void NmeaParser::setTecCoefficient(int tecCoefficient)
{
    _tecCoefficient = tecCoefficient;
    QSettings settings;
    settings.setValue("tecCoefficient",tecCoefficient);
}

int NmeaParser::getSpeedAverage() const
{
    return _speedAverage;
}

void NmeaParser::setSpeedAverage(int speedAverage)
{
    _speedAverage = speedAverage;
    QSettings settings;
    settings.setValue("speedAverage",_speedAverage);
}

int NmeaParser::getPressureCoefficient() const
{
    return _pressureCoefficient;
}

void NmeaParser::setPressureCoefficient(int pressureCoefficient)
{
    _pressureCoefficient = pressureCoefficient;
    QSettings settings;
    settings.setValue("pressureCoefficient",_pressureCoefficient);
}

string NmeaParser::getGPSTimestamp() const
{
    return _GPSTimestamp;
}

bool NmeaParser::getTimeIsValid() const
{
    return _timeIsValid;
}

QString NmeaParser::getSerialNumber() const
{
    return _serialNumber;
}

bool NmeaParser::getTimeSetComplete() const
{
    return _timeSetComplete;
}

void NmeaParser::processGNRMC()
{
    //_coord.setLat(44.49908);
    //_coord.setLon(7.38188);

    //_latitudeE7 = _coord.getLat()*1e7;
    //_longitudeE7 = _coord.getLon()*1e7;

    //emit coordUpdate(_coord);
    //emit speedAndCourse(_speedKmph, _course);
    //return;
    if(_tokens.size()<10)return;
    try {
        _hour = extractNumber(_tokens[1], 0);
        _min = extractNumber(_tokens[1], 2);
        _sec = extractNumber(_tokens[1], 4);
        _GPSTimestamp = _tokens[1].substr(0,6);

        int timestamp = _hour * 3600 + _min * 60 + _sec;

        if(_tokens[3].compare("")==0 ||
                _tokens[4].compare("")==0 ||
                _tokens[5].compare("")==0 ||
                _tokens[6].compare("")==0 ||
                _tokens[7].compare("")==0
                )return;

        double degrees = std::stod(_tokens[3].substr(0, 2));
        double minutes = str2double(_tokens[3].substr(2));

        QMutexLocker locker(&_locker);
        _coord.setLat(degrees + minutes / 60);
        _latitudeE7 = degrees * 1e7 + (minutes / 60) * 1e7;


        if(_tokens[4][0] == 'S') {
            _coord.revertLat();
            _latitudeE7 *= -1;
        }

        // longitude
        degrees = std::stod(_tokens[5].substr(0,3));

        minutes = str2double(_tokens[5].substr(3));

        _coord.setLon(degrees + minutes / 60);
        _longitudeE7 = degrees * 1e7 + (minutes / 60) * 1e7;

        if(_tokens[6][0] == 'W') {
            _coord.revertLon();
            _longitudeE7 *= -1;
        }

        _coord.setSnaptime(timestamp);
        _coord.setGpsHeight(_gpsHeight);

        emit coordUpdate(_coord);

        _day = datePartFromStr(_tokens[9], 0);
        _month = datePartFromStr(_tokens[9], 2);
        _year = datePartFromStr(_tokens[9], 4);

        if(_recheckTimePostscale > 0)_recheckTimePostscale--;

        if(_timeIsValid && _recheckTimePostscale == 0)
        {
            _recheckTimePostscale = 60 * 5;
            time_t now = time(NULL);
            struct tm *time = gmtime(&now);
            if(time->tm_sec != _sec || !_timeSetComplete)
            {
                qDebug()<<"time->tm_sec="<<time->tm_sec;
                qDebug()<<"_sec="<<_sec;
                qDebug()<<"Time is updated";
                if (setTimeFromGPS) setTimeFromGPS(*this, _year, _month, _day, _hour, _min, _sec);
                _timeSetComplete = true;
            }
        }
    } catch (const std::invalid_argument&) {
        std::cerr << "Argument is invalid\n";

    } catch (const std::out_of_range&) {
        std::cerr << "Argument is out of range for a double\n";

    } catch (const std::exception &e) {
        std::cerr << e.what() << std::endl;
    }


    try {
        if(_tokens[7][0] != 0) {
            _speed = stod(_tokens[7]);
            _speedKmph = _speed * 1.852;
        } else {
            _speed = 0;
            _speedKmph = 0;
        }

        if(_tokens[8][0] != 0) {
            _course = stod(_tokens[8]);
        }

        //_speedKmph = _absTurnIntegral;
        emit speedKmph(_speedKmph);
        emit course(_course);
        emit speedAndCourse(_speedKmph, _course);

    } catch(...) {
        //printf("Process GPRMC exeption 2\n");
    }
}

void NmeaParser::processGVAR()
{
    float vhVar = 0;
    if(_tokens.size()>=5)
    {
        float dinPressure = (float)(std::stoi(_tokens[4]))/100;
        dinPressure = dinPressure * _pressureCoefficient / 100.0;
        /*
        qDebug()<<"adcTAS:"<<adcTAS;
        if (adcTAS<512*4){
            _trueAirSpeedkmh = -sqrt((-10000.0*((adcTAS/4095.0)-0.5))/1.204);
        } else{
            _trueAirSpeedkmh = sqrt((10000.0*((adcTAS/4095.0)-0.5))/1.204);
        }
        _trueAirSpeedkmh *= 3.6;
        */

        //air density at sea level is 1.2 and it decreeses almost proportionally to 0.8 at 3000m
        float airDensity = 1.2 - 0.4 * _baroHeight/3000;

        _trueAirSpeedkmh = (_trueAirSpeedkmh * (_speedAverage - 1) + sqrt(fabs(dinPressure)*2/airDensity))/_speedAverage;
        //qDebug()<<"Airspeed received:"<<_trueAirSpeedkmh;

        //_trueAirSpeedkmh = (float)(std::stoi(_tokens[5]))/100;
        float equivalentHeight = _trueAirSpeedkmh * _trueAirSpeedkmh / 19.6;
        vhVar = (equivalentHeight - prevEquivalentHeight) * 1000;
        prevEquivalentHeight = equivalentHeight;



        if(_trueAirSpeedkmh == _trueAirSpeedkmh)emit tasChanged(_trueAirSpeedkmh * 3.6);
    }

    try {
        this->_verticalSpeed = std::stoi(_tokens[1]);
        this->_baroHeight = std::stoi(_tokens[2]);
        this->_temperature = std::stoi(_tokens[3]);


        vhVar = vhVar * _tecCoefficient / 100.0;
        emit vSpeed(_verticalSpeed + vhVar);
        emit baroHeight(_baroHeight);
        emit temperature(_temperature);
    }

    catch(...) {
        qWarning("Error converting tokens to integer: verticalSpeed, baroHeight, temperature");
    }
}
void NmeaParser::processGSN()
{
    if(_tokens.size()<2)return;

    try {
        this->_serialNumber = QString(_tokens[1].c_str());

    }

    catch(...) {
        qWarning("Error converting tokens to integer: verticalSpeed, baroHeight, temperature");
    }
}

void NmeaParser::processGVER()
{
    if(_tokens.size()<3)return;
    Version::setFirmwareVersion(QString::fromStdString(_tokens[1]));
    if(_tokens[2].front()=='B')
        Version::setFirmwareType("Standard");
    else
        Version::setFirmwareType("Instant");
    //if we are here wa are processing a vario message so we should stop sending time messages to vario real time part
    _timeIsValid = true;
}

void NmeaParser::processPubx()
{
    if(_tokens.size()<2)return;
    {
        if(_tokens[1]=="04")
        {
            if(_tokens.size()>8)
            {
                if(!QString(_tokens[6].c_str()).contains("D"))_timeIsValid = true;
                else _timeIsValid = false;
            }
        }
    }
}

void NmeaParser::processGNGSA()
{

    //qDebug() << "processGNGSA";

    try {
        fixType = std::stoi(_tokens[2].c_str());
        emit fixChange(fixType);

        hdop = str2double(_tokens[16]);
        vdop = str2double(_tokens[17]);


        if(fixType == 3 && firstFix == true){
            QSettings settings;

            settings.setValue("navigation/lastLatitude", static_cast<double>(NmeaParser::getLatitudeE7() / 1.0e7) );
            settings.setValue("navigation/lastLongitude", static_cast<double>(NmeaParser::getLongitudeE7() / 1.0e7) );
            settings.sync();

            //only save the settings once
            firstFix = false;
        }

        emit hdopChange(hdop);
        emit vdopChange(vdop);
    } catch(...) {
    }
}

void NmeaParser::processGNGGA() {
    //_gpsHeight = 1000;
    //emit gpsHeightChanged(_gpsHeight);
    //return;

    if(_tokens.size()<10)return;
    _gpsHeight = std::stoi(_tokens[9]);
    _satellitesUsed = std::stoi(_tokens[7]);
    //std::cout << "set sattelites to " << _satellitesUsed << "\n";
    emit gpsHeightChanged(_gpsHeight);
}

int NmeaParser::getBaroHeight() const
{
    return _baroHeight;
}

Coord NmeaParser::coord() const
{
    QMutexLocker locker(&_locker);
    return _coord;
}

double NmeaParser::getCourse() const
{
    return _course;
}

double NmeaParser::getSpeedKmph() const
{
    return _speedKmph;
}

int NmeaParser::getSatellitesUsed() const
{
    return _satellitesUsed;
}

int NmeaParser::getHdop() const
{
    return hdop;
}

double NmeaParser::getVdop() const
{
    return vdop;
}

NmeaParser::NmeaParser()
{
    QSettings settings;
    _pressureCoefficient = settings.value("pressureCoefficient",100).toInt();
    _speedAverage = settings.value("speedAverage",20).toInt();
    _tecCoefficient = settings.value("tecCoefficient",0).toInt();

}

long NmeaParser::getLatitudeE7() const {
    QMutexLocker locker(&_locker);
    return _latitudeE7;
}

long NmeaParser::getLongitudeE7() const {
    QMutexLocker locker(&_locker);
    return _longitudeE7;
}

Latitude NmeaParser::getLatitude() const {
    QMutexLocker locker(&_locker);
    return Latitude(static_cast<double>(_latitudeE7) / 1.0e7);
}

Longitude NmeaParser::getLongitude() const {
    QMutexLocker locker(&_locker);
    return Longitude(static_cast<double>(_longitudeE7) / 1.0e7);
}

// Datetime getters
int NmeaParser::getHour() const {
    return _hour;
}

int NmeaParser::getMin() const {
    return _min;
}
int NmeaParser::getSec() const {
    return _sec;
}
int NmeaParser::getDay() const {
    return _day;
}
int NmeaParser::getMonth() const {
    return _month;
}
int NmeaParser::getYear() const {
    return _year;
}

int NmeaParser::getGPSHeight() const {
    return _gpsHeight;
}

int NmeaParser::getFixType() const
{
    return fixType;
}

bool NmeaParser::getComOk() const
{
    return comOk;
}
