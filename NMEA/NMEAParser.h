/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __VARIOGUI_NMEAGPS_H
#define __VARIOGUI_NMEAGPS_H

#include "sound/varioSound.h"
#include "varioPrimitives/varioPrimitives.h"
#include "navUtils/navUtils.hpp"
#include "Geo/Geo.hpp"
#include "utils/vario_common.h"

#include <ctime>
#include <regex>
#include <string>
#include <iostream>
#include <fstream>
#include <QString>
#include <QMutexLocker>

using std::regex;
using std::string;
using std::sregex_token_iterator;
using std::vector;

#include <QObject>

#include <functional>

class NmeaParser: public QObject
{
    Q_OBJECT
public:
    static QString addChecksum(QString msg);

private:

//	ofstream out;

    int _lastCourse;
    int _absTurnIntegral = 0;

    char _messageBytes[402];
    int _recPos = 0;
	vector<string> _tokens;
    string _GPSTimestamp;

	// GNRMC
	//double _latitude, _longitude;
	Coord _coord;
	long _latitudeE7, _longitudeE7;

    double _course = 0;
	double _speed;
	double _speedKmph;

	int _hour;
	int _min;
	int _sec;
	int _day;
	int _month;
	int _year;
    int _recheckTimePostscale = 1;

	// GNVTG
	double _trueTrackMadeGood;
    double _trueAirSpeedkmh = 0;
	double _mgnTrackMadeGood;
	double _groundSpeedKnots;
	double _groundSpeedKmh;
    int _pressureCoefficient;

    int _speedAverage = 9;

    bool _simpleLock = false;
    mutable QMutex _locker;

    QString _serialNumber = "";

    bool _timeSetComplete = false;
#if defined(FAKE_GPS) || defined(FAKE_CONDOR)
    bool _timeIsValid = true;
#else
    bool _timeIsValid = false;
#endif

	//vario
	//vertical speed in cmps
    int _verticalSpeed = 0;
	//height in m
	int _baroHeight;
	//temperature in 0.1 degrees (divide by 10 to get degrees Celsius)
	int _temperature;

	// GNGGA
	int _gpsHeight;

    int _tecCoefficient=0;

	// Processing functions
	void processGNRMC();
	void processGNCC();
	void processGNAR();
	void processGNVTG();
	void processGNGGA();
	void processGNGSA();
    void processGVAR();
    void processGSN();
    void processGVER();
    void processPubx();

signals:
    void vSpeed(int verticalSpeed);
    void baroHeight(int baroHeight);
    void gpsHeightChanged(int gpsHeight);
    void temperature(int temp);
    void speedKmph(qreal gndSpeed);
    void course(qreal course);
    void coordUpdate(Coord coord);
    void fixChange(int type);
    void hdopChange(qreal value);
    void vdopChange(qreal value);
    void tasChanged(qreal value);
    void dateTime(int year, int month, int day, int hour, int minute, int second);

    void speedAndCourse(qreal speed, qreal course);

    void messageReceived(QString msg);


public:

    std::function<void(NmeaParser&, int, int, int, int, int, int)> setTimeFromGPS;

    Coord coord() const;
	NmeaParser();

    int pushCh(unsigned char ch);
    int pushLine(char messageBytes[]);

	long getLatitudeE7() const;
	long getLongitudeE7() const;
	Latitude getLatitude() const;		// object version of above
	Longitude getLongitude() const;

	double getCourse() const;
	double getSpeedKmph() const;

    /*
	VarioUI* vario = nullptr;
	Glide* glide = nullptr;
	Wind* wind = nullptr;
	ThermalPlot* thermalPlot = nullptr;
    */

	int getVerticalSpeed();		// not yet implemented
	int getBaroHeight() const;
	int getBaroTemperature();	// not yet implemented

	// Not implemented:
	int getHour() const;
	int getMin() const;
	int getSec() const;
	int getDay() const;
	int getMonth() const;
	int getYear() const;

	int getGPSHeight() const;

	int fixType = 0;
    int _satellitesUsed = 0;
    double hdop;
    double vdop = 99.0;
    bool firstFix = true;
    bool comOk = false;
    bool getTimeSetComplete() const;
    int getSatellitesUsed() const;
    int getHdop() const;
    double getVdop() const;
    bool getComOk() const;
    QString getSerialNumber() const;
    bool getTimeIsValid() const;
    int getFixType() const;
    string getGPSTimestamp() const;
    int getPressureCoefficient() const;
    void setPressureCoefficient(int pressureCoefficient);
    float prevEquivalentHeight = 0;
    int getSpeedAverage() const;
    void setSpeedAverage(int speedAverage);
    int getTecCoefficient() const;
    void setTecCoefficient(int tecCoefficient);
};

#endif
