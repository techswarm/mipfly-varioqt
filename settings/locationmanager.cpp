/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "locationmanager.h"

#include "NMEA/NMEAParser.h"

LocationManager::LocationManager(const NmeaParser &gpsParser, const NmeaParser &varParser)
    : gpsParser(gpsParser), varParser(varParser)
{

}

Latitude LocationManager::latitude() const {
    return gpsParser.getLatitude();
}

Longitude LocationManager::longitude() const {
    return gpsParser.getLongitude();
}

int LocationManager::altitude() const {
    return varParser.getBaroHeight();
}
