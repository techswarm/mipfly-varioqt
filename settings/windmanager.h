/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef WINDMANAGER_H
#define WINDMANAGER_H

#include <QSettings>

class WindManager: public QObject
{
    Q_OBJECT

    int numReadings;
    int numAverage;
    int dataSpread;
    float rmsError;

    QSettings settings;
public:
    static const int NUMRD_MIN = 0;     // Num readings minimum value
    static const int NUMRD_MAX = 250;
    static const int NUMAVG_MIN = 0;    // Num average minimum value
    static const int NUMAVG_MAX = 100;
    static const int SPREAD_MIN = 0;    // Data spread minimum value
    static const int SPREAD_MAX = 100;
    static const int RMSERROR_MIN = 3;    // RMS Error minimum value
    static const int RMSERROR_MAX = 5;

    static const int NUMRD_DEFAULT = 170;
    static const int NUMAVG_DEFAULT = 50;
    static const int SPREAD_DEFAULT = 40;
    static const int RMSERROR_DEFAULT = 4;

    WindManager();

    void incNumReadings();
    void decNumReadings();
    int getNumReadings() const;

    void incNumAverage();
    void decNumAverage();
    int getNumAverage() const;

    void incDataSpread();
    void decDataSpread();
    int getDataSpread() const;

    void incRmsError();
    void decRmsError();
    float getRmsError() const;

    void setNumReadings(int value);

    void setNumAverage(int value);

    void setDataSpread(int value);

    void setRmsError(float value);

signals:
    void numRdChanged(int);
    void numAvgChanged(int);
    void datSpdChanged(int);
    void rmsErrorChanged(float);
};

#endif // WINDMANAGER_H
