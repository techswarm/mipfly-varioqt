/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TZMANAGER_H
#define TZMANAGER_H

#include <QSettings>

class TzManager
{
    int timezone;
    QSettings settings;
public:
    TzManager();

#ifdef __ARMEL__
    void setActTZ();
    void setEnvTZ(int timezone);
#endif

    void incTimezone();
    void decTimezone();
    int getTimezone() const;
};

#endif // TZMANAGER_H
