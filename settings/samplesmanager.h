/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SAMPLESMANAGER_H
#define SAMPLESMANAGER_H

#include <QObject>

#include "utils/microcontroller.h"

class SamplesManager : public QObject
{
    Q_OBJECT
public:
    static const int NUM_MIN = 50;      // not recommended
    static const int NUM_MAX = 1000;    // not tested

    static int loadNumSmpl();

    SamplesManager(const SerialConnection &serial);

    void incNumSamples();
    void decNumSamples();
    void saveNumSamples();
    void reset();
    int value() const;

signals:
    void pause();
    void resume();

private:
    int numSamples;
    Microcontroller mc;
};

#endif // SAMPLESMANAGER_H
