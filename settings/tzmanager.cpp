/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "tzmanager.h"

#include <string>
#include <cstdlib>
#include <iostream>


#include "utils/vario_common.h"

#include <QtDebug>

TzManager::TzManager()
{
    timezone = settings.value(Vario::SETTINGS_TZ, 0).toInt();
}

void TzManager::incTimezone() {
    if (timezone < 14) {
        ++timezone;
        settings.setValue(Vario::SETTINGS_TZ, timezone);
#ifdef __ARMEL__
        setEnvTZ(1 * timezone);
#endif
    }
}

void TzManager::decTimezone() {
    if (timezone > -14) {
        --timezone;
        settings.setValue(Vario::SETTINGS_TZ, timezone);
#ifdef __ARMEL__
        setEnvTZ(1 * timezone);
#endif
    }
}

#ifdef __ARMEL__
void TzManager::setEnvTZ(int timezone) {
    QString tzVal ("cp /usr/share/zoneinfo/Etc/GMT");
    if(timezone * -1 >= 0)tzVal.append("+");
    tzVal.append(QString::number(timezone * -1));
    tzVal.append(" /etc/localtime");
    qDebug()<<tzVal;
    system(qPrintable(tzVal));
}

//cp /usr/share/zoneinfo/Etc/GMT-2 /etc/localtime
void TzManager::setActTZ() {
    QString tzVal ("cp /usr/share/zoneinfo/Etc/GMT");
    if(timezone * -1 >= 0)tzVal.append("+");
    tzVal.append(QString::number(timezone * -1));
    tzVal.append(" /etc/localtime");
    qDebug()<<tzVal;
    system(qPrintable(tzVal));
}
#endif


int TzManager::getTimezone() const {
    return timezone;
}
