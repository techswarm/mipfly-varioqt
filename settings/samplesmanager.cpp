/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "samplesmanager.h"

#include <QSettings>
#include <sstream>
#include <QThread>

#include "utils/vario_common.h"

int SamplesManager::loadNumSmpl()
{
    return QSettings().value(Vario::SETTINGS_SMPL, 201).toInt();
}

SamplesManager::SamplesManager(const SerialConnection &serial)
    : mc(serial)
{
    numSamples = loadNumSmpl();
}

void SamplesManager::incNumSamples() {
    if (numSamples < NUM_MAX) {
        numSamples+=10;
    }
    if (numSamples > NUM_MAX)numSamples = NUM_MAX;
}

void SamplesManager::decNumSamples() {
    if (numSamples > NUM_MIN) {
        numSamples-=10;
    }
    if(numSamples < NUM_MIN)numSamples = NUM_MIN;
}

void SamplesManager::saveNumSamples() {
    QSettings settings;
    settings.setValue(Vario::SETTINGS_SMPL, numSamples);

    emit pause();
    mc.setNumSample(numSamples);
    emit resume();
}

int SamplesManager::value() const {
    return numSamples;
}

void SamplesManager::reset() {
    numSamples = loadNumSmpl();
}
