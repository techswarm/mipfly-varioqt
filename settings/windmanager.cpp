/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "windmanager.h"

#include "utils/vario_common.h"

void WindManager::setNumReadings(int value)
{
    numReadings = value;
    settings.setValue(Vario::SETTINGS_WNUMRD, numReadings);
    emit numRdChanged(numReadings);
}

void WindManager::setNumAverage(int value)
{
    numAverage = value;
    settings.setValue(Vario::SETTINGS_WNUMAVG, numAverage);
    emit numAvgChanged(numAverage);
}

void WindManager::setDataSpread(int value)
{
    dataSpread = value;
    settings.setValue(Vario::SETTINGS_WDTSPRD, dataSpread);
    emit datSpdChanged(dataSpread);
}

void WindManager::setRmsError(float value)
{
    rmsError = value;
    settings.setValue(Vario::SETTINGS_RMSERROR, rmsError);
    emit rmsErrorChanged(rmsError);
}

WindManager::WindManager()
{
    numReadings = settings.value(Vario::SETTINGS_WNUMRD, NUMRD_DEFAULT).toInt();
    numAverage = settings.value(Vario::SETTINGS_WNUMAVG, NUMAVG_DEFAULT).toInt();
    dataSpread = settings.value(Vario::SETTINGS_WDTSPRD, SPREAD_DEFAULT).toInt();
    rmsError = settings.value(Vario::SETTINGS_RMSERROR, RMSERROR_DEFAULT).toFloat();
}

void WindManager::incNumReadings() {
    if(numReadings < NUMRD_MAX){
        numReadings++;
        settings.setValue(Vario::SETTINGS_WNUMRD, numReadings);
        emit numRdChanged(numReadings);
    }
}

void WindManager::decNumReadings() {
    if(numReadings > NUMRD_MIN){
        numReadings--;
        settings.setValue(Vario::SETTINGS_WNUMRD, numReadings);
        emit numRdChanged(numReadings);
    }
}

int WindManager::getNumReadings() const {
    return numReadings;
}

void WindManager::incNumAverage() {
    if(numAverage < NUMAVG_MAX){
        numAverage++;
        settings.setValue(Vario::SETTINGS_WNUMAVG, numAverage);
        emit numAvgChanged(numAverage);
    }
}

void WindManager::decNumAverage() {
    if(numAverage > NUMAVG_MIN){
        numAverage--;
        settings.setValue(Vario::SETTINGS_WNUMAVG, numAverage);
        emit numAvgChanged(numAverage);
    }
}

int WindManager::getNumAverage() const {
    return numAverage;
}

void WindManager::incDataSpread() {
    if(dataSpread < SPREAD_MAX){
        dataSpread++;
        settings.setValue(Vario::SETTINGS_WDTSPRD, dataSpread);
        emit datSpdChanged(dataSpread);
    }
}

void WindManager::decDataSpread() {
    if(dataSpread > SPREAD_MIN){
        dataSpread--;
        settings.setValue(Vario::SETTINGS_WDTSPRD, dataSpread);
        emit datSpdChanged(dataSpread);
    }
}

int WindManager::getDataSpread() const {
    return dataSpread;
}

void WindManager::incRmsError() {
    if(rmsError < RMSERROR_MAX){
        rmsError+=0.1;
        settings.setValue(Vario::SETTINGS_RMSERROR, rmsError);
        emit rmsErrorChanged(rmsError);
    }
}

void WindManager::decRmsError() {
    if(rmsError > RMSERROR_MIN){
        rmsError-=0.1;
        settings.setValue(Vario::SETTINGS_RMSERROR, rmsError);
        emit rmsErrorChanged(rmsError);
    }
}

float WindManager::getRmsError() const {
    return rmsError;
}
