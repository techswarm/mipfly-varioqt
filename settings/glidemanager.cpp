/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "glidemanager.h"

#include "utils/vario_common.h"

GlideManager::GlideManager()
{
    numAverage = settings.value(Vario::SETTINGS_GNUMAVG, GLIDE_MAX).toInt();
}

void GlideManager::incNumAverage() {
    if (numAverage < GLIDE_MAX) {
        numAverage++;
        settings.setValue(Vario::SETTINGS_GNUMAVG, numAverage);
        emit numAvgChanged(numAverage);
    }
}

void GlideManager::decNumAverage() {
    if (numAverage > GLIDE_MIN) {
        numAverage--;
        settings.setValue(Vario::SETTINGS_GNUMAVG, numAverage);
        emit numAvgChanged(numAverage);
    }
}

int GlideManager::getNumAverage() const {
    return numAverage;
}
