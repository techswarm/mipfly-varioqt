/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "namemanager.h"

#include <QtDebug>
#include <QSettings>

#include "utils/vario_common.h"

NameManager::NameManager()
{
    loadFromSettings();
}

void NameManager::clear() {
    name.clear();
    emit nameChange(name);
}

void NameManager::backspace() {
    name.remove(name.length()- 1, 1);
    emit nameChange(name);
}

void NameManager::accept(QChar letter) {
    name.append(letter);
    emit nameChange(name);
}

void NameManager::save() {
    QSettings settings;
    settings.setValue(Vario::SETTINGS_PILOT, name);
    settings.sync();
    emit nameSet(name);
}

const QString &NameManager::getName() const {
    return name;
}

void NameManager::loadFromSettings()
{
    QSettings settings;
    name = settings.value(Vario::SETTINGS_PILOT, QString()).toString();
}
