/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "NumericInputDecimal.h"
#include "ui_NumericInputDecimal.h"
#include "VarioQt.h"
#include "mainui.h"
#include "utils/vario_common.h"

NumericInputDecimal::NumericInputDecimal(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NumericInputDecimal)
{
    ui->setupUi(this);

    value_ = 0;
    step_ = 1;

    max_ = INT_MAX;
    min_ = INT_MIN;

    ui->valueBox->setValue(QString::number(value_));

    displayDecimals_ = 0;

    connect(VarioQt::instance().getMainUI(),SIGNAL(mainGpioPressed(int)),this,SLOT(gpioPressed(int)));
    connect(VarioQt::instance().getMainUI(),SIGNAL(mainGpioLongPressed(int)),this,SLOT(gpioLongPressed(int)));

    move(240/2 - width()/2,320/2 - height()/2);
}

NumericInputDecimal::~NumericInputDecimal()
{
    delete ui;
}

void NumericInputDecimal::showEvent(QShowEvent *e)
{
    QWidget::showEvent(e);
    VarioQt::instance().pushNewWidget(this);
}

void NumericInputDecimal::hideEvent(QHideEvent *e)
{
    QWidget::hideEvent(e);
    VarioQt::instance().deleteLastWidget();
}

float NumericInputDecimal::displayDecimals() const
{
    return displayDecimals_;
}

void NumericInputDecimal::setDisplayDecimals(float displayScale)
{
    displayDecimals_ = displayScale;
}

void NumericInputDecimal::setCaption(QString caption)
{
    ui->valueBox->setCaption(caption);
    update();
}




void NumericInputDecimal::gpioPressed(int btn)
{
    if(btn == Vario::GPIO1)
    {
        if(value_%10 != 9)
            value_+=step_;
        else
            value_-=9;
        if(value_>max_)value_=max_;
        setValue(value_);
        emit valueChanged(value_);
    }
    if(btn == Vario::GPIO2)
    {
        if(value_%10 != 0)
            value_ -= step_;
        else value_ += 9;

        setValue(value_);
        emit valueChanged(value_);
    }
    if(btn == Vario::GPIO3)
    {
        if(value_ * 10 < max_)
        {
            value_*=10;
            setValue(value_);
            emit valueChanged(value_);
        }
        else
        {
            close();
        }
    }
}

void NumericInputDecimal::gpioLongPressed(int btn)
{
    if(btn == Vario::GPIO1)
    {
        value_ /= 10;
        setValue(value_);
        emit valueChanged(value_);
    }
    if(btn == Vario::GPIO3)
    {
        close();
    }
}

int NumericInputDecimal::max() const
{
    return max_;
}

void NumericInputDecimal::setMax(int max)
{
    max_ = max;
}

int NumericInputDecimal::value() const
{
    return value_;
}

void NumericInputDecimal::setValue(int value)
{
    value_ = value;
    ui->valueBox->setValue(QString::number((float)value_/(pow(10,displayDecimals_)),'f',displayDecimals_));
    repaint();
}
