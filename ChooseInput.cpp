/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ChooseInput.h"
#include "ui_ChooseInput.h"
#include "VarioQt.h"
#include "utils/vario_common.h"

ChooseInput::ChooseInput(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ChooseInput)
{
    ui->setupUi(this);

    connect(VarioQt::instance().getMainUI(),SIGNAL(mainGpioPressed(int)),this,SLOT(gpioPressed(int)));
    connect(VarioQt::instance().getMainUI(),SIGNAL(mainGpioLongPressed(int)),this,SLOT(gpioLongPressed(int)));

    move(240/2 - width()/2,320/2 - height()/2);

    ui->msg1->setAlignment(Qt::AlignHCenter);
    ui->msg2->setAlignment(Qt::AlignHCenter);
    ui->msg3->setAlignment(Qt::AlignHCenter);
    ui->valueBox->setValueAlign(Qt::AlignCenter);

    ui->msg1->setSize(14);
    ui->msg2->setSize(14);
    ui->msg3->setSize(14);

    ui->msg1->setNoBorder();
    ui->msg2->setNoBorder();
    ui->msg3->setNoBorder();
}

ChooseInput::~ChooseInput()
{
    if(isVisible())hide();
    delete ui;
}

void ChooseInput::setValue(QString value)
{
    ui->valueBox->setValue(value);
}

void ChooseInput::setCaption(QString caption)
{
    ui->valueBox->setCaption(caption);
}

void ChooseInput::setChouse1(QString value)
{
    ui->msg1->setValue(value);
    if(value != "")ui->msg1->setAllBorders();
    else ui->msg1->setNoBorder();
}

void ChooseInput::setChouse2(QString value)
{
    ui->msg2->setValue(value);
    if(value != "")ui->msg2->setAllBorders();
    else ui->msg2->setNoBorder();
}

void ChooseInput::setChouse3(QString value)
{
    ui->msg3->setValue(value);
    if(value != "")ui->msg3->setAllBorders();
    else ui->msg3->setNoBorder();
}

void ChooseInput::showEvent(QShowEvent *e)
{
    QWidget::showEvent(e);
    VarioQt::instance().pushNewWidget(this);
}

void ChooseInput::hideEvent(QHideEvent *e)
{
    QWidget::hideEvent(e);
    VarioQt::instance().deleteLastWidget();
}

void ChooseInput::gpioPressed(int btn)
{
    if(btn == Vario::GPIO1 && ui->msg1->getValue()!="")_choice = 1;
    if(btn == Vario::GPIO2 && ui->msg2->getValue()!="")_choice = 2;
    if(btn == Vario::GPIO3 && ui->msg3->getValue()!="")_choice = 3;
    if(_choice!=-1) close();
}

void ChooseInput::gpioLongPressed(int btn)
{
    _choice = -1;
    close();
}

int ChooseInput::choice() const
{
    return _choice;
}
