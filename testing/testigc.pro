TARGET = igcTest
TEMPLATE = app
CONFIG += debug
QMAKE_CXXFLAGS += -std=c++11

SOURCES += testigc.cpp \
		  ../IGC/IGCFile.cpp \
		  ../IGC/IGCWriter.cpp \
		  ../IGC/IGCFileHeader.cpp \
		  ../Geo/Geo.cpp \
		  ../utils/utils.cpp

HEADERS += ../IGC/IGCFile.hpp \
           ../utils/utils.h \
           ../Geo/Geo.hpp

INCLUDEPATH += ..