/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PRINTSERIAL2THREAD_H
#define PRINTSERIAL2THREAD_H

#include "serial/serialconnection.h"
#include "threads/stoppablethread.h"

class PrintSerial2Thread: public StoppableThread
{
public:
    PrintSerial2Thread(const SerialConnection &serial);

protected:
    void run() Q_DECL_OVERRIDE;

private:
    const SerialConnection &serial;
};

#endif // PRINTSERIAL2THREAD_H
