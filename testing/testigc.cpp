/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "IGC/IGCFile.hpp"

int main()
{
    IGCFile igcFile;
    IGCWriter writer;

    igcFile.setPilot("IGC_TEST");
    writer.newFlight(igcFile);

    Latitude lat(44.5);
    Longitude lon(22.3);
    int alt = 223;

    for(int i=0; i<5; ++i) {
        writer.append(lat, lon, alt, 0);
    }

    writer.flushStream();

    writer.copyFlightWithDetails("BETFIA", 7281.4);
}
