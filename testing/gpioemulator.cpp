/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gpioemulator.h"

#include "utils/vario_common.h"

#include <QDebug>

GPIOEmulator::GPIOEmulator(QObject *parent) : QObject(parent)
{

}
void GPIOEmulator::keyPressed(int key) {
    if (key == Qt::Key_Q)
        emit gpioPressed(Vario::GPIO1);
    if (key == Qt::Key_W)
        emit gpioPressed(Vario::GPIO2);
    if (key == Qt::Key_E)
        emit gpioPressed(Vario::GPIO3);

    if (key == Qt::Key_A)
        emit gpioLongPressed(Vario::GPIO1);
    if (key == Qt::Key_S)
        emit gpioLongPressed(Vario::GPIO2);
    if (key == Qt::Key_D)
        emit gpioLongPressed(Vario::GPIO3);
}
