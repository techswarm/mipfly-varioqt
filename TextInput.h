/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TEXTINPUT_H
#define TEXTINPUT_H

#include <QDialog>

namespace Ui {
class TextInput;
}

class TextInput : public QDialog
{
    Q_OBJECT

public:
    explicit TextInput(QWidget *parent = 0);
    ~TextInput();

    QString value() const;
    void setValue(const QString &value);

    void setLetters();
    void setNumbers();
    void setMixt();
    void setAllPrintAscii();

private:
    Ui::TextInput *ui;
    void showEvent(QShowEvent *e) Q_DECL_OVERRIDE;
    void hideEvent(QHideEvent *e) Q_DECL_OVERRIDE;

    QString _value;

public slots:
    void gpioPressed(int btn);
    void gpioLongPressed(int btn);

private slots:
    void letterChoused(QChar letter);
};

#endif // TEXTINPUT_H
