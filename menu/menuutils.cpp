/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "menuutils.h"

#include "menudialog.h"
#include "ui_menudialog.h"

void setupSlider(const MenuDialog &dlg, int val, int min, int max) {
    dlg.getUi().valLabel->setText(QString::number(val));

    dlg.getUi().hSlider->setMinimum(min);
    dlg.getUi().hSlider->setMaximum(max);
    dlg.getUi().hSlider->setValue(val);

    dlg.getUi().valLabel->setVisible(true);
    dlg.getUi().hSlider->setVisible(true);
}

void updateSlider(const MenuDialog &dlg, int val) {
    dlg.getUi().valLabel->setText(QString::number(val));
    dlg.getUi().hSlider->setValue(val);
}
