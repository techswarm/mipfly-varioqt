/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "varioaveragemenu.h"
#include "menudialog.h"
#include "ui_menudialog.h"
#include "sound/varioSound.h"
#include "utils/vario_common.h"
#include "navUtils/variocompute.h"

enum
{
    VarioAvgUp,
    VarioAvgDown,
    VArioAvgBack
};

VarioAverageMenu::VarioAverageMenu(VarioCompute &varioCompute, MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent), varioCompute(varioCompute)
{

}

int VarioAverageMenu::rowCount(const QModelIndex &parent) const
{
    return 3;
}

const QString VarioAverageMenu::captionForIndex(int index) const
{
    switch(index) {
    case VarioAvgUp:
        return QString(tr("UP"));
    case VarioAvgDown:
        return QString(tr("DOWN"));
    case VArioAvgBack:
        return QString(tr("BACK"));
    default:
        return QString();
    }
}

void VarioAverageMenu::before(const MenuDialog &menuDialog)
{
    int seconds = varioCompute.getBuflen()/10;
    menuDialog.getUi().valLabel->setText(QString("%1 s").arg(seconds));

    menuDialog.getUi().hSlider->setMinimum(0);
    menuDialog.getUi().hSlider->setMaximum(300);
    menuDialog.getUi().hSlider->setValue(seconds);

    menuDialog.getUi().valLabel->setVisible(true);
    menuDialog.getUi().hSlider->setVisible(true);
}


MenuType VarioAverageMenu::typeForIndex(int index) const
{
    if (index == VArioAvgBack) {
        return Back;
    }
    return Choice;
}

void VarioAverageMenu::execAtIndex(int index, const MenuDialog &menuDialog)
{
    switch(index) {
    case VarioAvgUp:
        varioCompute.increaseAverageSeconds();
        break;
    case VarioAvgDown:
        varioCompute.decreaseAverageSeconds();
        break;
    }
    int seconds = varioCompute.getBuflen()/10;
    menuDialog.getUi().hSlider->setValue(seconds);
    menuDialog.getUi().valLabel->setText(QString("%1 s").arg(seconds));
}
