/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LOCEDITLONMENU_H
#define LOCEDITLONMENU_H

#include "menu/inputboxmenu.h"

class LocEditLonMenu : public InputBoxMenu
{
    Q_OBJECT
public:
    LocEditLonMenu(MenuListModel *parentMenu = nullptr, QObject *parent = Q_NULLPTR);

    void before(const MenuDialog &menuDialog) Q_DECL_OVERRIDE;
};

#endif // LOCEDITLONMENU_H
