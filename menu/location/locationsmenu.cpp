/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "locationsmenu.h"

#include "locdeletemenu.h"
#include "loceditmenu.h"
#include "locsetcrtmenu.h"
#include "settings/locationmanager.h"
#include "menudialog.h"
#include "ui_menudialog.h"
#include "threads/locationsthread.h"
#include "Geo/locationsthreadmgr.h"

#include <Warning.h>

enum LocationsIndex
{
    LocationsEdit,
    LocationsDelete,
    LocationsAutoSet,
    LocationsSetCrt,
    LocationsClearCrt,
    LocationsBack
};

LocationsMenu::LocationsMenu(const LocationManager &locationMgr, LocationsThreadMgr &locThreadMgr, MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent), locMgr(locationMgr), locThreadMgr(locThreadMgr)
{}

int LocationsMenu::rowCount(const QModelIndex &parent) const
{
    return 6;
}

MenuType LocationsMenu::typeForIndex(int index) const
{
    switch(index) {
    case LocationsAutoSet:
    case LocationsClearCrt:
        return Choice;
    case LocationsBack:
        return Back;
    default:
        return List;
    }
}

void LocationsMenu::before(const MenuDialog &menuDialog)
{
    menuDialog.getUi().locWidget->setVisible(false);

    menuDialog.connectLocThreadMgr();
}

const QString LocationsMenu::captionForIndex(int index) const
{
    LocationsService locSvc;

    switch(index) {
    case LocationsEdit:
        return QString(tr("ADD / EDIT"));
    case LocationsAutoSet:
        if (locSvc.isSetManually())
            return QString(tr("AUTO-SET: OFF"));
        else
            return QString(tr("AUTO-SET: ON"));
    case LocationsSetCrt:
        return QString(tr("SET CURRENT"));
    case LocationsClearCrt:
        return QString(tr("CLEAR CURRENT"));
    case LocationsDelete:
        return QString(tr("DELETE"));
    case LocationsBack:
        return QString(tr("BACK"));
    default:
        return QString();
    }
}

MenuListModel* LocationsMenu::modelForIndex(int index)
{
    switch(index) {
    case LocationsEdit:
        return new LocEditMenu(locMgr, this);
    case LocationsSetCrt:
        return new LocSetCrtMenu(this);
    case LocationsDelete:
        return new LocDeleteMenu(this);

    default:
        return nullptr;
    }
}

const QString LocationsMenu::descAtIndex(int index) const
{
    switch(index) {
    case LocationsEdit:
        return QString(tr("Modify data for existing location name or insert new location"));
    case LocationsAutoSet:
        return QString(tr("Toggle manual vs automatic location set for flight recording"));
    default:
        return QString();
    }
}

void LocationsMenu::execAtIndex(int index, const MenuDialog &menuDialog)
{
    LocationsService locSvc;
    if (index == LocationsClearCrt) {
        locSvc.clearCurrent();
        Warning::showWarning(tr("INFO"),tr("Location was cleared"), 1);
    } else if (index == LocationsAutoSet) {
        bool val = locSvc.isSetManually();
        locSvc.setManually(!val);
        emit dataChanged(QModelIndex(), QModelIndex());

        if (val == true) {
            // Start locations detect thread
            locThreadMgr.startThread();
        } else {
            // Stop locations detect thread
            locThreadMgr.stopThread();
        }
    }
}
