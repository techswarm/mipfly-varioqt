/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "locdeletemenu.h"

#include "menudialog.h"

#include <Warning.h>

using Vario::Location;

void LocDeleteMenu::execAtIndex(int index, const MenuDialog &menuDialog)
{
    const Location &loc = locService.locationAtIndex(index - 1);

    if (loc.name == locService.currentLocation().name) {
        locService.clearCurrent();
    }

    locService.remove(loc);
    locService.save();
    emit dataChanged(QModelIndex(), QModelIndex());
    menuDialog.selectIndex(index - 1);
    Warning::showWarning(tr("INFO"),tr("Location was removed"), 1);
}
