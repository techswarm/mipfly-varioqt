/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "loclistmenu.h"

using Vario::Location;

int LocListMenu::rowCount(const QModelIndex &parent) const
{
    return 1 + locService.getAll().size();
}

const QString LocListMenu::captionForIndex(int index) const
{
    if (index == 0) {
        return QString(tr("-- BACK --"));
    } else {
        const Location &loc = locService.locationAtIndex(index - 1);
        return loc.name;
    }
}

MenuType LocListMenu::typeForIndex(int index) const
{
    return index == 0 ? Back : Choice;
}

void LocListMenu::before(const MenuDialog &menuDialog)
{

}

