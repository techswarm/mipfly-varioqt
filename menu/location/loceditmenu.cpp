/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "loceditmenu.h"

#include "menudialog.h"
#include "ui_menudialog.h"
#include "settings/locationmanager.h"
#include "Geo/locationsservice.h"

#include "loceditinputmenu.h"
#include "loceditlatmenu.h"
#include "loceditlonmenu.h"
#include "loceditaltmenu.h"

#include "Warning.h"

enum LocEditIndex
{
    LocEditFromGPS,
    LocEditInput,
    LocEditLatitude,
    LocEditLongitude,
    LocEditAltitude,
    LocEditSave,
    LocEditBack
};

static void uiSetCoords(const MenuDialog &dlg, const Latitude& lat, const Longitude& lon, int alt)
{
    dlg.getUi().locWidget->setLatitude(lat);
    dlg.getUi().locWidget->setLongitude(lon);
    dlg.getUi().locWidget->setAltitude(alt);
}

LocEditMenu::LocEditMenu(const LocationManager &locationsMgr, MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent), locMgr(locationsMgr)
{}

int LocEditMenu::rowCount(const QModelIndex &parent) const
{
    return 7;
}

MenuType LocEditMenu::typeForIndex(int index) const
{
    if (index == LocEditBack) {
        return Back;
    } else if (index == LocEditFromGPS || index == LocEditSave) {
        return Choice;
    } else {
        return List;
    }
}

const QString LocEditMenu::captionForIndex(int index) const
{
    switch(index) {
    case LocEditFromGPS:
        return QString(tr("COORDS FROM GPS"));
    case LocEditInput:
        return QString(tr("INPUT NAME"));
    case LocEditLatitude:
        return QString(tr("SET LATITUDE"));
    case LocEditLongitude:
        return QString(tr("SET LONGITUDE"));
    case LocEditAltitude:
        return QString(tr("SET ALTITUDE"));
    case LocEditSave:
        return QString(tr("SAVE"));
    case LocEditBack:
        return QString(tr("BACK"));
    default:
        return QString();
    }
}

void LocEditMenu::before(const MenuDialog &menuDialog)
{
    menuDialog.getUi().locWidget->setVisible(true);
    menuDialog.getUi().locWidget->setNoBorders();
}


void LocEditMenu::execAtIndex(int index, const MenuDialog &menuDialog) {
    if (index == LocEditFromGPS) {
        Latitude lat = locMgr.latitude();
        Longitude lon = locMgr.longitude();
        int alt = locMgr.altitude();

        uiSetCoords(menuDialog, lat, lon, alt);

    } else if (index == LocEditSave) {
        LocationsService locService;
        const InputLocation &widget = *(menuDialog.getUi().locWidget);
        QString locName = widget.name();

        Vario::Location *loc = locService.locationByName(locName);
        if (loc == nullptr) {
            // Save new location
            Vario::Location newLoc;
            newLoc.name = locName;
            newLoc.lat = widget.latitude();
            newLoc.lon = widget.longitude();
            newLoc.alt = widget.altitude();
            locService.addLocation(newLoc);
        } else {
            // Update existing location
            loc->lat = widget.latitude();
            loc->lon = widget.longitude();
            loc->alt = widget.altitude();
        }
        locService.save();
        Warning::showWarning(tr("INFO"),tr("Location was saved"), 1);
    }
}

MenuListModel *LocEditMenu::modelForIndex(int index)
{
    switch(index) {
    case LocEditInput:
        return new LocEditInputMenu(this);
    case LocEditLatitude:
        return new LocEditLatMenu(this);
    case LocEditLongitude:
        return new LocEditLonMenu(this);
    case LocEditAltitude:
        return new LocEditAltMenu(this);
    default:
        return nullptr;
    }
}
