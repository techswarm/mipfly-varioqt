/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mainmenu.h"

#include <QApplication>
#include <QStringList>

#ifdef __ARMEL__
#include <QtDebug>
#include <unistd.h>
#include <sys/reboot.h>
#include <linux/reboot.h>
#endif

#ifdef USE_SOUND
#include "sound/varioSound.h"
#endif

#include "settings/locationmanager.h"
#include "menudialog.h"
#include "ui_menudialog.h"
#include "threads/fb2spithread.h"

#include "glides/glidesmenu.h"
#include "glideAverage/glidemenu.h"
#include "bootloader/bootloadermenu.h"
#include "sound/soundMenu.h"
#include "navigation/navigationMenu.h"
#include "recovery/recoverymenu.h"
#include "general/generalMenu.h"
#include "avionics/avionicsMenu.h"
#include "shutdown/shutdownmenu.h"
#include "task/taskMenu.h"
#include "system/systemMenu.h"

#include "VarioQt.h"

#include "Warning.h"

#include "utils/Version.h"


enum
{
#ifdef USE_SOUND
    MainSound,
#endif
    MainNavigation,
    MainTask,
    MainGeneral,
    MainGlides,
    MainAvionics,
    MainAbout,
#ifndef __ARMEL__
    MainExit,
#else
    MainTheme,
    MainShutdown,
#endif
    MainRecovery
};

MainMenu::MainMenu(const MenuDialog &menuDialog,
                   MenuListModel *parentMenu,
                   QObject *parent)
    : MenuListModel(parentMenu, parent),
      menuDialog(menuDialog)
{}

int MainMenu::rowCount(const QModelIndex &parent) const
{
    int rows = 6;
#ifndef __ARMEL__
    rows += 1;
#else
    rows += 2;
#endif
#ifdef USE_SOUND
    rows += 1;
#endif
    if(VarioQt::instance().getRecoveryMode())
        rows += 1;
    return rows;
}

const QString MainMenu::captionForIndex(int index) const
{
    switch(index)
    {
#ifdef USE_SOUND
    case MainSound:
        return QString(tr("SOUND"));
#endif
    case MainNavigation:
        return QString(tr("NAVIGATION/MAPS"));
    case MainTask:
        return QString(tr("NAVIGATION/TASK"));
    case MainGeneral:
        return QString(tr("GENERAL"));
    case MainGlides:
        return QString(tr("FLIGHTS"));
    case MainAvionics:
        return QString(tr("AVIONICS"));
    case MainAbout:
        return QString(tr("ABOUT"));
    case MainRecovery:
        return QString(tr("RECOVERY"));
#ifndef __ARMEL__
    case MainExit:
        return QString(tr("EXIT PROGRAM"));
#else
    case MainTheme:
        return QString(tr("SWITCH THEME"));
    case MainShutdown:
        return QString(tr("TURN OFF"));
#endif
    default:
        return QString();
    }
}

MenuType MainMenu::typeForIndex(int index) const
{
    switch(index) {
    #ifdef __ARMEL__
    case MainTheme:
        return Choice;
    #endif
/*    case MainAbout:
        return Choice;*/
    default:
        return List;
    }
}

void MainMenu::before(const MenuDialog &menuDialog)
{
    menuDialog.disconnectLocThreadMgr();

    menuDialog.getUi().valLabel->setVisible(false);
    menuDialog.getUi().hSlider->setVisible(false);
    menuDialog.getUi().chooser->setVisible(false);
    menuDialog.getUi().valName->setVisible(false);
    menuDialog.getUi().locWidget->setVisible(false);

#ifdef USE_SOUND
    VarioQt::instance().getVarioSound()->varioSoundSetCMPS(0);
#endif
}

MenuListModel* MainMenu::modelForIndex(int index)
{
    switch(index) {
#ifdef USE_SOUND
    case MainSound:
        return new SoundMenu(*(VarioQt::instance().getVarioSound()),this);
#endif
    case MainNavigation:
        return new NavigationMenu(this);
    case MainTask:
        return new TaskMenu(this);
    case MainGeneral:
        return new GeneralMenu(menuDialog,this);
    case MainGlides:
        return new GlidesMenu(*(VarioQt::instance().getNameManager()), this);
    case MainAvionics:
        return new AvionicsMenu(menuDialog,this);
    case MainAbout:
        return new SystemMenu(menuDialog, this);
    case MainRecovery:
        return new RecoveryMenu(menuDialog, this);
    #ifdef __ARMEL__
    case MainShutdown:
        return new ShutdownMenu(menuDialog,this);
    #else
    case MainExit:
        return new ShutdownMenu(menuDialog,this);
    #endif
        break;
    default:
        return nullptr;
    }
}

const QString MainMenu::descAtIndex(int index) const
{
    switch(index) {
    case MainNavigation:
        return QString(tr("Maps and navigation options"));
    case MainGlides:
        return QString(tr("View recorded glides"));
    case MainTask:
        return QString(tr("Task navigation options"));
    case MainAvionics:
        return QString(tr("Flight algorithms related settings"));
    case MainSound:
        return QString(tr("Volume and sound curve settings"));
    case MainGeneral:
        return QString(tr("General, localization and connectivity settings"));
    case MainRecovery:
        return QString(tr("Try different recovery methods for the main RootFs"));
    case MainAbout:
    {
        QString ret = Version::VarioQTVersion()+"\n";
        ret += Version::OSVersion()+"\n";
        ret += Version::getFirmwareType()+":"+Version::getFirmwareVersion()+"\n";

        QSettings settings;
        QString id = settings.value("accountId","").toString();
        ret+= "id = " + id;

        ret += "\n\n"+(QString)__DATE__ + " " + (QString)__TIME__"\n";
        ret += VarioQt::instance().getVarioParser()->getSerialNumber();

        return ret;
    }
    default:
        return QString();
    }
}

void MainMenu::execAtIndex(int index, const MenuDialog &menuDialog)
{
    switch(index) {

    #ifdef __ARMEL__
        case MainTheme:
            VarioQt::instance().getFb2SPIThread()->switchTheme();
            break;
    #endif

    }
}
