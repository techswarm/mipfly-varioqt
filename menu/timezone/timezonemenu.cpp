/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "timezonemenu.h"

#include "menu/menuutils.h"

enum
{
    SetTimezoneUp,
    SetTimezoneDown,
    SetTimezoneBack
};

int TimezoneMenu::rowCount(const QModelIndex &parent) const
{
    return 3;
}

const QString TimezoneMenu::captionForIndex(int index) const
{
    switch(index) {
    case SetTimezoneUp:
        return QString(tr("UP"));
    case SetTimezoneDown:
        return QString(tr("DOWN"));
    case SetTimezoneBack:
        return QString(tr("BACK"));
    default:
        return QString();
    }
}


MenuType TimezoneMenu::typeForIndex(int index) const
{
    if (index == SetTimezoneBack) {
        return Back;
    }
    return Choice;
}

void TimezoneMenu::before(const MenuDialog &menuDialog)
{
    setupSlider(menuDialog, mgr.getTimezone(), -12, 12);
}

void TimezoneMenu::execAtIndex(int index, const MenuDialog &menuDialog)
{
    if (index == SetTimezoneUp) {
        mgr.incTimezone();
        updateSlider(menuDialog, mgr.getTimezone());
    } else {
        mgr.decTimezone();
        updateSlider(menuDialog, mgr.getTimezone());
    }
}

const QString TimezoneMenu::descAtIndex(int index) const
{
    return QString(tr("Timezone modifications requires reboot to take effect!"));
}
