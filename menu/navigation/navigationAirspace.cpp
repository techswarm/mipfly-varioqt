/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "navigationAirspace.h"
#include "menudialog.h"
#include "ui_menudialog.h"
#include "sound/varioSound.h"
#include "utils/vario_common.h"
#include "navUtils/variocompute.h"
#include <QSettings>
#include <QDir>
#include <Warning.h>


NavigationAirspaceM::NavigationAirspaceM(MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent)
{
    QDir dir("airspace");
    dir.setFilter(QDir::Files);
    dir.setSorting(QDir::Name);
    airspaces = dir.entryList();

    QSettings settings;
    selectedAirspaces = settings.value("navigation/Airspaces").toStringList();
}

NavigationAirspaceM::~NavigationAirspaceM()
{
    QSettings settings;
    settings.setValue("navigation/Airspaces", QVariant::fromValue(selectedAirspaces));
    settings.setValue("navigation/Airspace",selectedAirspaces.join(','));
}

int NavigationAirspaceM::rowCount(const QModelIndex &parent) const
{
    return airspaces.count()+1;
}

const QString NavigationAirspaceM::captionForIndex(int index) const
{
    if(index < airspaces.count())
    {
        QString str = airspaces[index];
        if(selectedAirspaces.indexOf(str)>=0)str="*"+str;
        str.replace(QString(".txt"), QString(""));
        return str;
    }
    else
        return QString(tr("BACK"));
}

void NavigationAirspaceM::before(const MenuDialog &menuDialog)
{

}


MenuType NavigationAirspaceM::typeForIndex(int index) const
{
    if (index == airspaces.count()) {
        return Back;
    }
    return Choice;
}

void NavigationAirspaceM::execAtIndex(int index, const MenuDialog &menuDialog)
{
    if(index < airspaces.count())
    {
        //QSettings settings;
        //settings.setValue("navigation/Airspace",airspaces[index]);
        //Warning::showWarning(tr("INFO"),tr("Airspace was saved"), 1);
        QString str = airspaces[index];
        int airspaceIndex = selectedAirspaces.indexOf(str);
        if(airspaceIndex>=0)
        {
            //we have clicked an active airspace
            selectedAirspaces.removeAt(airspaceIndex);
        }
        else
        {
            selectedAirspaces.append(str);
        }
    }
    emit dataChanged(QModelIndex(), QModelIndex());
}

MenuListModel* NavigationAirspaceM::modelForIndex(int index)
{
    return nullptr;
}
