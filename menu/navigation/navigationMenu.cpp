/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "navigationMenu.h"
#include "navigationAirspace.h"
#include "navigationMap.h"
#include "menudialog.h"
#include "ui_menudialog.h"
#include "sound/varioSound.h"
#include "utils/vario_common.h"
#include "navUtils/variocompute.h"
#include "menu/location/locationsmenu.h"
#include <QSettings>
#include "VarioQt.h"
#include "NumericInput.h"
#include "utils/unitmanager.h"

enum
{
    NavigationAirspace,
    NavigationAirspaceWarningHeight,
    NavigationAirspaceWarningNear,
    NavigationAirspaceWarningVeryNear,
    NavigationAirspaceIgnoreOver,
    NavigationAirspaceFrontUp,
    NavigationMap,
    NavigationWoods,
    NavigationLocation,
    NavigationBack
};

NavigationMenu::NavigationMenu(MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent)
{

}

int NavigationMenu::rowCount(const QModelIndex &parent) const
{
    return 10;
}

const QString NavigationMenu::captionForIndex(int index) const
{
    switch(index) {
    case NavigationAirspace:
    {
        return QString(tr("AIRSPACE"));
    }
    case NavigationMap:
    {
        QSettings settings;
        QString map = settings.value("navigation/Map","Romania").toString();
        if(map == "AUTO")return QString(tr("MAP - AUTO"));
        return QString(tr("MAP - "))+map;
    }
    case NavigationWoods:
    {
        QSettings settings;
        bool showWoods = settings.value("navigation/showWoods",true).toBool();
        if(showWoods == true)
            return QString(tr("SHOW WOODS - ON"));
        else
            return QString(tr("SHOW WOODS - OFF"));
    }
    case NavigationAirspaceFrontUp:
    {
        if(VarioQt::instance().getAirSpaceThread()->getAirspaceFrontUp())
            return QString(tr("Airspace Front-Up - ON"));
        else
            return QString(tr("Airspace Front-Up - OFF"));
    }
    case NavigationAirspaceIgnoreOver:
    {
        SIHeight height(VarioQt::instance().getAirSpaceThread()->getDrawHLimit());
        float airspaceIgnoreLimitPreffered = height.getValuePrefferedFormat();
        return QString("Ignore AS Over-")+QString::number(airspaceIgnoreLimitPreffered,'f',0)+" "+SIHeight::prefferedUnit();
    }
    case NavigationAirspaceWarningHeight:
    {
        SIHeight distance = VarioQt::instance().getAirSpaceThread()->getVerticalWarningMeters();
        return QString(tr("V.Near-"))+
                QString::number(distance.getValuePrefferedFormat(),'f',1)+
                " "+
                distance.getPrefferedFormat();
    }
    case NavigationAirspaceWarningNear:
    {
        SIHeight distance = VarioQt::instance().getAirSpaceThread()->getNear();
        return QString(tr("H.Near-"))+
                QString::number(distance.getValuePrefferedFormat(),'f',1)+
                " "+
                distance.getPrefferedFormat();
    }
    case NavigationAirspaceWarningVeryNear:
    {
        SIHeight distance = VarioQt::instance().getAirSpaceThread()->getVeryNear();
        return QString(tr("H.Very Near-"))+
                QString::number(distance.getValuePrefferedFormat(),'f',1)+
                " "+
                distance.getPrefferedFormat();
    }
    case NavigationLocation:
        return QString(tr("LOCATION"));
    case NavigationBack:
        return QString(tr("BACK"));
    default:
        return QString();
    }
}

void NavigationMenu::before(const MenuDialog &menuDialog)
{

}


MenuType NavigationMenu::typeForIndex(int index) const
{
    switch (index) {
    case NavigationBack:
        return Back;
        break;
    case NavigationAirspaceWarningHeight:
    case NavigationAirspaceWarningNear:
    case NavigationAirspaceIgnoreOver:
    case NavigationAirspaceWarningVeryNear:
    case NavigationWoods:
    case NavigationAirspaceFrontUp:
        return Choice;
    default:
        return List;
        break;
    }
}

void NavigationMenu::execAtIndex(int index, const MenuDialog &menuDialog)
{
    switch (index) {
    case NavigationAirspaceWarningHeight:
    {
        NumericInput *ni = new NumericInput;
        SIHeight distance(VarioQt::instance().getAirSpaceThread()->getVerticalWarningMeters());
        ni->setValue(distance.getValuePrefferedFormat());
        if(distance.getPrefferedFormat() == "m")
        {
            ni->setStep(10);
            ni->setMax(1000);
        }
        else
        {
            ni->setStep(10);
            ni->setMax(3000);
        }
        ni->setMin(0);
        ni->setCaption(tr("Vertical warning")+" "+distance.getPrefferedFormat());

        ni->move(240/2 - ni->width()/2,320/2 - ni->height()/2);
        ni->exec();
        distance.setValuePrefferedFormat(ni->value());
        VarioQt::instance().getAirSpaceThread()->setVerticalWarningMeters(distance.value());
        delete ni;
    }
        break;
    case NavigationAirspaceWarningNear:
    {
        NumericInput *ni = new NumericInput;
        SIHeight distance(VarioQt::instance().getAirSpaceThread()->getNear());
        ni->setValue(distance.getValuePrefferedFormat());
        if(distance.getPrefferedFormat() == "m")
        {
            ni->setStep(10);
            ni->setMax(1000);
        }
        else
        {
            ni->setStep(10);
            ni->setMax(3000);
        }
        ni->setMin(0);
        ni->setCaption(tr("Near warning")+" "+distance.getPrefferedFormat());
        ni->move(240/2 - ni->width()/2,320/2 - ni->height()/2);
        ni->exec();
        distance.setValuePrefferedFormat(ni->value());
        VarioQt::instance().getAirSpaceThread()->setNear(distance.value());
        delete ni;
    }
        break;
    case NavigationAirspaceWarningVeryNear:
    {
        NumericInput *ni = new NumericInput;
        SIHeight distance(VarioQt::instance().getAirSpaceThread()->getVeryNear());
        ni->setValue(distance.getValuePrefferedFormat());
        if(distance.getPrefferedFormat() == "m")
        {
            ni->setStep(10);
            ni->setMax(1000);
        }
        else
        {
            ni->setStep(10);
            ni->setMax(3000);
        }
        ni->setMin(0);
        ni->setCaption(tr("Very near warning"));
        ni->move(240/2 - ni->width()/2,320/2 - ni->height()/2);
        ni->exec();
        distance.setValuePrefferedFormat(ni->value());
        VarioQt::instance().getAirSpaceThread()->setVeryNear(distance.value());
        delete ni;
        break;
    }
    case NavigationAirspaceIgnoreOver:
    {
        NumericInput *ni = new NumericInput;
        SIHeight height(VarioQt::instance().getAirSpaceThread()->getDrawHLimit());
        ni->setValue(height.getValuePrefferedFormat());
        ni->setStep(100);
        ni->setMax(30000);
        ni->setPrecision(0);
        ni->setCaption(tr("Ignore over") +" " + height.getPrefferedFormat());
        ni->move(240/2 - ni->width()/2,320/2 - ni->height()/2);
        ni->exec();
        height.setValuePrefferedFormat(ni->value());
        VarioQt::instance().getAirSpaceThread()->setDrawHLimit(height.value());
        delete ni;
        break;
    }
    case NavigationWoods:
    {
        QSettings settings;
        bool showWoods = settings.value("navigation/showWoods",true).toBool();
        if(showWoods == true)settings.setValue("navigation/showWoods",false);
        else settings.setValue("navigation/showWoods",true);
        emit dataChanged(QModelIndex(), QModelIndex());
        break;
    }
    case NavigationAirspaceFrontUp:
    {
        bool frontUp = VarioQt::instance().getAirSpaceThread()->getAirspaceFrontUp();
        if(frontUp == true)VarioQt::instance().getAirSpaceThread()->setAirspaceFrontUp(false);
        else VarioQt::instance().getAirSpaceThread()->setAirspaceFrontUp(true);
        emit dataChanged(QModelIndex(), QModelIndex());
        break;
    }

    default:
        break;
    }
}

MenuListModel* NavigationMenu::modelForIndex(int index)
{
    switch(index) {
    case NavigationAirspace:
        return new NavigationAirspaceM(this);
    case NavigationMap:
        return new NavigationMapM(this);
    case NavigationLocation:
        return new LocationsMenu(*(VarioQt::instance().getLocationManager()), *(VarioQt::instance().getLocationsThreadMgr()), this);
        break;
    default:
        return nullptr;
    }
}

const QString NavigationMenu::descAtIndex(int index) const
{
    switch(index) {
    case NavigationAirspace:
        return QString(tr("Select airspace file to be used from root/airspace folder on SD card."));
    case NavigationAirspaceWarningHeight:
        return QString(tr("Set vertical separation to airspace for warning."));
    case NavigationAirspaceWarningNear:
        return QString(tr("Set horizontal separation to airspace for NEAR warning."));
    case NavigationAirspaceWarningVeryNear:
        return QString(tr("Set horizontal separation to airspace for VERY NEAR warning."));
    case NavigationMap:
        return QString(tr("Maps are autodetected from root/maps folder on SD card."));
    case NavigationAirspaceIgnoreOver:
        return QString(tr("Set superior limmit for airspace display. Note that the automatic airspace infrigement algorithm will still consider the hidden airspaces."));
    case NavigationAirspaceFrontUp:
        return QString(tr("Toggle airspace plotting. If disables the map will always display with the N to the top. If enabled the map will rotate to display the pront position on the top of the map."));
    case NavigationWoods:
        return QString(tr("Hide or show woods on the topo map."));
    case NavigationLocation:
        return QString(tr("Add or edit/delete location information."));
    default:
        return QString();
    }
}
