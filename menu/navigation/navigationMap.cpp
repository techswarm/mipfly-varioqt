/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "navigationMap.h"
#include "menudialog.h"
#include "ui_menudialog.h"
#include "sound/varioSound.h"
#include "utils/vario_common.h"
#include "navUtils/variocompute.h"
#include <QSettings>
#include <QDir>
#include <QDebug>
#include <Warning.h>


NavigationMapM::NavigationMapM(MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent)
{
    QDir dir("maps");
    dir.setFilter(QDir::Files);
    dir.setSorting(QDir::Name);
    maps = dir.entryList();
}

int NavigationMapM::rowCount(const QModelIndex &parent) const
{
    return maps.count()+2;
}

const QString NavigationMapM::captionForIndex(int index) const
{
    if(index==0)return QString(tr("AUTO"));
    if(index-1 < maps.count())
    {
        QString str = maps[index-1];
        str.replace(QString(".mbtiles"), QString(""));
        return str;
    }
    else
        return QString(tr("BACK"));
}

void NavigationMapM::before(const MenuDialog &menuDialog)
{

}


MenuType NavigationMapM::typeForIndex(int index) const
{
    if (index == maps.count()+1) {
        return Back;
    }
    return Choice;
}

void NavigationMapM::execAtIndex(int index, const MenuDialog &menuDialog)
{
    if(index == 0)
    {
        QSettings settings;
        settings.setValue("navigation/Map","AUTO");
        Warning::showWarning(tr("INFO"),tr("Map set to auto"), 1);
    }
    else if(index - 1 < maps.count())
    {
        QSettings settings;
        QString str = maps[index - 1];
        str.replace(QString(".mbtiles"), QString(""));
        settings.setValue("navigation/Map",str);
        Warning::showWarning(tr("INFO"),tr("Map was saved"), 1);
    }
}

MenuListModel* NavigationMapM::modelForIndex(int index)
{
    return nullptr;
}
