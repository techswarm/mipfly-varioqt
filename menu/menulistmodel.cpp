/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "menulistmodel.h"
#include "GlideDetails.h"

#include <QtDebug>

MenuListModel::MenuListModel(MenuListModel *parentMenu, QObject *parent)
    : QAbstractListModel(parent),
      _parentMenu(parentMenu)
{

}

QVariant MenuListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role == Qt::TextAlignmentRole) {
        return int(Qt::AlignCenter | Qt::AlignHCenter);
    } else if (role == Qt::DisplayRole) {
        return captionForIndex(index.row());
    }

    return QVariant();
}

MenuListModel* MenuListModel::modelForIndex(int index)
{
    qDebug() << "modelForIndex(int) not implemented in this subclass";
    return nullptr;
}

void MenuListModel::execAtIndex(int index, const MenuDialog &menuDialog)
{
    qDebug() << "execAtIndex(int, const MainWindow&) not implemented in this subclass";
}

void MenuListModel::execAtIndexNC(int index, MenuDialog &menuDialog)
{
    qDebug() << "execAtIndex(int, MainWindow&) not implemented in this subclass";
}

const QString MenuListModel::descAtIndex(int index) const
{
    return QString();
}

MenuListModel *MenuListModel::parentMenu() const
{
    return _parentMenu;
}

void MenuListModel::beforeNC(MenuDialog &menuDialog)
{
    qDebug() << "beforeNC(MenuDialog &menuDialog) not implemented in this subclass";
}

int MenuListModel::savedIndex() const
{
    if(_savedIndex>=rowCount())return rowCount()-1;
    return _savedIndex;
}

void MenuListModel::rowChanged(const QModelIndex &current, const QModelIndex &previous)
{
    _savedIndex = current.row();
}
