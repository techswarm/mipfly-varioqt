/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "taskMenu.h"
#include "menudialog.h"
#include "ui_menudialog.h"
#include "sound/varioSound.h"
#include "utils/vario_common.h"
#include "navUtils/variocompute.h"
#include "menu/location/locationsmenu.h"
#include <QSettings>
#include "VarioQt.h"
#include "NumericInput.h"
#include "waypointEdit.h"
#include "CoordInput.h"
#include "TextInput.h"
#include "NumericInputDecimal.h"
#include "Warning.h"
#include "utils/unitmanager.h"

enum
{
    WaypointEditMenuLatCaption,
    WaypointEditMenuLat,
    WaypointEditMenuLonCaption,
    WaypointEditMenuLon,
    WaypointEditMenuNameCaption,
    WaypointEditMenuName,
    WaypointEditMenuDescCaption,
    WaypointEditMenuDesc,
    WaypointEditMenuHeightCaption,
    WaypointEditMenuHeight,
    WaypointEditMenuSaveAdd,
    WaypointEditMenuDelete,
    WaypointEditMenuBack
};

WaypointEditMenu::WaypointEditMenu(int waypointId, MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent)
{
    _waypointId = waypointId;
    wpm = VarioQt::instance().getWaypointManager();
    if(_waypointId >=0 )
    {
        if(_waypointId<wpm->waypoints.count())
        {
            waypoint = wpm->waypoints.at(_waypointId);
        }
        else
        {
            _waypointId = -1;
        }
    }
}

int WaypointEditMenu::rowCount(const QModelIndex &parent) const
{
    return 13;
}

const QString WaypointEditMenu::captionForIndex(int index) const
{
    switch(index) {
    case WaypointEditMenuLatCaption:
        return QString(tr("-LATITUDE-"));
    case WaypointEditMenuLonCaption:
        return QString(tr("-LONGITUDE-"));
    case WaypointEditMenuNameCaption:
        return QString(tr("-NAME-"));
    case WaypointEditMenuDescCaption:
        return QString(tr("-DESC-"));
    case WaypointEditMenuHeightCaption:
        return QString(tr("-HEIGHT-"));
    case WaypointEditMenuSaveAdd:
    {
        if(_waypointId == -1)
            return QString(tr("ADD"));
        else
            return QString(tr("SAVE"));
    }
    case WaypointEditMenuDelete:
        return QString(tr("DELETE"));

    case WaypointEditMenuBack:
        return QString(tr("BACK"));

    case WaypointEditMenuLat:
        return waypoint.coordonate().getLatStr();

    case WaypointEditMenuLon:
        return waypoint.coordonate().getLonStr();

    case WaypointEditMenuName:
        return waypoint.name();

    case WaypointEditMenuDesc:
        return waypoint.description();

    case WaypointEditMenuHeight:
    {
        SIHeight height(waypoint.height());
        return QString::number(height.getValuePrefferedFormat(),'f',0)+" "+height.getPrefferedFormat();
    }
    }

    return QString("");
}

void WaypointEditMenu::before(const MenuDialog &menuDialog)
{

}


MenuType WaypointEditMenu::typeForIndex(int index) const
{
    switch (index) {
    case WaypointEditMenuBack:
        return Back;
        break;
    case WaypointEditMenuLatCaption:
    case WaypointEditMenuLonCaption:
    case WaypointEditMenuNameCaption:
    case WaypointEditMenuHeightCaption:
        return Caption;
        break;

    case WaypointEditMenuDelete:
    {
        if(_waypointId < 0)
            return Caption;
        else
            return Choice;
    }

    default:
        return Choice;
        break;
    }
}

void WaypointEditMenu::execAtIndexNC(int index, MenuDialog &menuDialog)
{
    switch (index) {
    case WaypointEditMenuLat:
    {
        CoordInput * ci = new CoordInput();
        ci->setType(CoordInput::LAT);
        ci->setValue(waypoint.coordonate().getLat());
        ci->exec();
        waypoint.setCoordonate(Coord(ci->value(),waypoint.coordonate().getLon()));
        delete ci;
        break;
    }
    case WaypointEditMenuLon:
    {
        CoordInput * ci = new CoordInput();
        ci->setType(CoordInput::LON);
        ci->setValue(waypoint.coordonate().getLon());
        ci->exec();
        waypoint.setCoordonate(Coord(waypoint.coordonate().getLat(),ci->value()));
        delete ci;
        break;
    }
    case WaypointEditMenuName:
    {
        TextInput *ti = new TextInput;
        ti->setValue(waypoint.name());
        ti->setMixt();
        ti->exec();

        waypoint.setName(ti->value());
        delete ti;
        break;
    }
    case WaypointEditMenuDesc:
    {
        TextInput *ti = new TextInput;
        ti->setValue(waypoint.description());
        ti->setMixt();
        ti->exec();

        waypoint.setDescription(ti->value());
        delete ti;
        break;
    }
    case WaypointEditMenuHeight:
    {
        NumericInputDecimal *ni = new NumericInputDecimal;
        SIHeight height(waypoint.height());
        ni->setValue(height.getValuePrefferedFormat());
        ni->setCaption(tr("Height")+" "+height.getPrefferedFormat());
        ni->exec();

        height.setValuePrefferedFormat(ni->value());
        waypoint.setHeight(height.value());
        delete ni;
        break;
    }
    case WaypointEditMenuSaveAdd:
    {
        if(_waypointId < 0)
        {
            //we have to add a new waypoint
            if(waypoint.isValid())
            {
                wpm->waypoints.append(waypoint);
                Warning::showWarning(tr("INFO"),tr("WP ADDED"),1);
                waypoint.reset();
            }
            else
            {
                Warning::showWarning(tr("INFO"),tr("WP INVALID"),1);
            }
        }
        else
        {
            wpm->waypoints[_waypointId] = waypoint;
            Warning::showWarning(tr("INFO"),tr("WP UPDATED"),1);
        }
        break;
    }
    case WaypointEditMenuDelete:
    {
        wpm->waypoints.removeAt(_waypointId);
        _waypointId = -1;
        menuDialog.goBack();
        break;
    }
    }
}

MenuListModel* WaypointEditMenu::modelForIndex(int index)
{
    switch(index) {

    break;
    }
    return nullptr;
}

const QString WaypointEditMenu::descAtIndex(int index) const
{
    switch(index) {
    default:
        return QString();
    }
}
