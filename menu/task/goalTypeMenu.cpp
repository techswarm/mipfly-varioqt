/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "menudialog.h"
#include "ui_menudialog.h"
#include "sound/varioSound.h"
#include "utils/vario_common.h"
#include "navUtils/variocompute.h"
#include "menu/location/locationsmenu.h"
#include <QSettings>
#include "VarioQt.h"
#include "NumericInput.h"
#include "waypointMenu.h"
#include "goalTypeMenu.h"
#include "navWaypoint/task.h"

enum
{
    GoalTypeMenuLine,
    GoalTypeMenuCylinderEnter,
    GoalTypeMenuCylinderExit
};

GoalTypeMenu::GoalTypeMenu(MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent)
{

}

int GoalTypeMenu::rowCount(const QModelIndex &parent) const
{
    return 3;
}

const QString GoalTypeMenu::captionForIndex(int index) const
{
    switch(index) {
    case GoalTypeMenuLine:
        return QString(tr("LINE"));
    case GoalTypeMenuCylinderEnter:
        return QString(tr("CYLINDER ENTER"));
    case GoalTypeMenuCylinderExit:
        return QString(tr("CYLINDER EXIT"));
    default:
        return QString();
    }
}

void GoalTypeMenu::before(const MenuDialog &menuDialog)
{

}


MenuType GoalTypeMenu::typeForIndex(int index) const
{
    switch(index) {
    case GoalTypeMenuLine:
    case GoalTypeMenuCylinderEnter:
    case GoalTypeMenuCylinderExit:
        return Choice;
        break;
    default:
        return List;
    }
}

void GoalTypeMenu::execAtIndexNC(int index, MenuDialog &menuDialog)
{
    Task * pTask = VarioQt::instance().getTask();
    switch (index) {
    case GoalTypeMenuLine:
        pTask->editedTurnpoint.setGoal(TurnPoint::GoalType::Line);
        break;
    case GoalTypeMenuCylinderEnter:
        pTask->editedTurnpoint.setGoal(TurnPoint::GoalType::CylinterEnter);
        break;
    case GoalTypeMenuCylinderExit:
        pTask->editedTurnpoint.setGoal(TurnPoint::GoalType::CylinderExit);
        break;
    default:
        break;
    }
    menuDialog.goBack();
}

MenuListModel* GoalTypeMenu::modelForIndex(int index)
{

    switch(index) {
        break;
    }
    return nullptr;
}

const QString GoalTypeMenu::descAtIndex(int index) const
{
    switch(index) {
    default:
        return QString();
    }
}
