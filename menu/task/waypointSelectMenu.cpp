/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "waypointMenu.h"
#include "menudialog.h"
#include "ui_menudialog.h"
#include "sound/varioSound.h"
#include "utils/vario_common.h"
#include "navUtils/variocompute.h"
#include "menu/location/locationsmenu.h"
#include <QSettings>
#include "VarioQt.h"
#include "NumericInput.h"
#include "waypointSelectMenu.h"
#include "navWaypoint/task.h"
#include "utils/unitmanager.h"

enum
{
    WaypointSelectMenuBack
};

WaypointSelectMenu::WaypointSelectMenu(MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent)
{
    wpm = VarioQt::instance().getWaypointManager();
}

int WaypointSelectMenu::rowCount(const QModelIndex &parent) const
{
    return 1 + wpm->waypoints.count();
}

const QString WaypointSelectMenu::captionForIndex(int index) const
{
    switch(index) {
    case WaypointSelectMenuBack:
        return QString(tr("BACK"));
    }
    if(index>WaypointSelectMenuBack)
    {
        return wpm->waypoints.at(index-(WaypointSelectMenuBack+1)).name();
    }
    return QString();
}

void WaypointSelectMenu::before(const MenuDialog &menuDialog)
{

}


MenuType WaypointSelectMenu::typeForIndex(int index) const
{
    switch (index) {
    case WaypointSelectMenuBack:
        return Back;
        break;

    default:
        return Choice;
        break;
    }
}

void WaypointSelectMenu::execAtIndexNC(int index, MenuDialog &menuDialog)
{
    pTask->editedTurnpoint.setWaypoint(wpm->waypoints.at(index-(WaypointSelectMenuBack+1)));
    menuDialog.goBack();
}

MenuListModel* WaypointSelectMenu::modelForIndex(int index)
{
    return nullptr;
}

const QString WaypointSelectMenu::descAtIndex(int index) const
{
    if(index>WaypointSelectMenuBack)
    {
        QString ret;
        ret += wpm->waypoints.at(index-(WaypointSelectMenuBack+1)).name()+"\n";
        ret += tr("Lat")+" "+wpm->waypoints.at(index-(WaypointSelectMenuBack+1)).coordonate().getLatStr()+"\n";
        ret += tr("Lon")+" "+wpm->waypoints.at(index-(WaypointSelectMenuBack+1)).coordonate().getLonStr()+"\n";
        SIHeight height(wpm->waypoints.at(index-(WaypointSelectMenuBack+1)).height());
        ret += "Height "+QString::number(height.getValuePrefferedFormat(),'f',0)+height.getPrefferedFormat()+"\n";
        ret += "["+wpm->waypoints.at(index-(WaypointSelectMenuBack+1)).description()+"]";
        return ret;
    }
    return QString();
}
