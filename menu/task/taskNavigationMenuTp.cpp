/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "menudialog.h"
#include "ui_menudialog.h"
#include "sound/varioSound.h"
#include "utils/vario_common.h"
#include "navUtils/variocompute.h"
#include "menu/location/locationsmenu.h"
#include <QSettings>
#include "VarioQt.h"
#include "NumericInput.h"
#include "waypointMenu.h"
#include "taskNavigationMenuTp.h"
#include "navWaypoint/task.h"

enum
{
    TaskNavigationMenuTpBack
};

TaskNavigationMenuTp::TaskNavigationMenuTp(MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent)
{
    pTask = VarioQt::instance().getTask();
}

int TaskNavigationMenuTp::rowCount(const QModelIndex &parent) const
{
    return pTask->getTurnPoints().count()+1;
}

const QString TaskNavigationMenuTp::captionForIndex(int index) const
{
    switch(index) {
    case TaskNavigationMenuTpBack:
        return QString(tr("Back"));
    }

    if(index <= pTask->getTurnPoints().count())
    {
        return pTask->getTurnPoints().at(index-1).getWaypoint().name();
    }
    return QString();
}

void TaskNavigationMenuTp::before(const MenuDialog &menuDialog)
{

}

MenuType TaskNavigationMenuTp::typeForIndex(int index) const
{
    switch(index) {
    case TaskNavigationMenuTpBack:
        return Back;
        break;
    default:
        return Choice;
    }
}

void TaskNavigationMenuTp::execAtIndexNC(int index, MenuDialog &menuDialog)
{
    switch (index) {
    default:
        break;
    }
    if(index <= pTask->getTurnPoints().count() && index > TaskNavigationMenuTpBack)
    {
        pTask->navToTaskTp(index - 1 - TaskNavigationMenuTpBack);
        Warning::showWarning(tr("NAV"),tr("Turnpoint set"),3);
    }

    menuDialog.goBack();
    menuDialog.hide();
}

MenuListModel* TaskNavigationMenuTp::modelForIndex(int index)
{
    switch(index) {
        break;
    }
    return nullptr;
}

const QString TaskNavigationMenuTp::descAtIndex(int index) const
{
    switch(index) {

    }
    if(index>TaskNavigationMenuTpBack)
    {
        QString ret = QString(tr("Navigate to:"));
        ret += "\n ---- \n";

        ret += pTask->getTurnpointTypeStr(index-(TaskNavigationMenuTpBack+1))+"-";
        ret += pTask->getTurnPoints().at(index-(TaskNavigationMenuTpBack+1)).getWaypoint().name()+"\n";
        ret += "Radius/Line length:"+QString::number(pTask->getTurnPoints().at(index-(TaskNavigationMenuTpBack+1)).getRadiusLengthMeters())+"m \n";
        ret += "Lat "+ pTask->getTurnPoints().at(index-(TaskNavigationMenuTpBack+1)).getWaypoint().coordonate().getLatStr()+"\n";
        ret += "Lon "+ pTask->getTurnPoints().at(index-(TaskNavigationMenuTpBack+1)).getWaypoint().coordonate().getLonStr()+"\n";
        ret += "Height "+ QString::number(pTask->getTurnPoints().at(index-(TaskNavigationMenuTpBack+1)).getWaypoint().height())+"\n";
        if(pTask->getTurnPoints().at(index-(TaskNavigationMenuTpBack+1)).getGoal() == TurnPoint::GoalType::Line)ret += QString(tr("LINE"));
        if(pTask->getTurnPoints().at(index-(TaskNavigationMenuTpBack+1)).getGoal() == TurnPoint::GoalType::CylinterEnter)ret += QString(tr("CYLINDER ENTER"));
        if(pTask->getTurnPoints().at(index-(TaskNavigationMenuTpBack+1)).getGoal() == TurnPoint::GoalType::CylinderExit)ret += QString(tr("CYLINDER EXIT"));

        return ret;
    }
    return QString();
}
