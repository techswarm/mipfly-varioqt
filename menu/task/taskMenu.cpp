/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "taskMenu.h"
#include "menudialog.h"
#include "ui_menudialog.h"
#include "sound/varioSound.h"
#include "utils/vario_common.h"
#include "navUtils/variocompute.h"
#include "menu/location/locationsmenu.h"
#include <QSettings>
#include "VarioQt.h"
#include "NumericInput.h"
#include "waypointMenu.h"
#include "taskEditMenu.h"
#include "taskImportMenu.h"

enum
{
    TaskMenuWaypoints,
    TaskMenuEditTask,
    TaskMenuImportTask,
    TaskMenuTaskInfo,
    TaskMenuBack
};

TaskMenu::TaskMenu(MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent)
{

}

int TaskMenu::rowCount(const QModelIndex &parent) const
{
    return 5;
}

const QString TaskMenu::captionForIndex(int index) const
{
    switch(index) {
    case TaskMenuWaypoints:
        return QString(tr("WAYPOINTS"));
    case TaskMenuEditTask:
        return QString(tr("TASK"));
    case TaskMenuImportTask:
        return QString(tr("IMPORT TASK"));
    case TaskMenuTaskInfo:
        return QString(tr("TASK INFO"));
    case TaskMenuBack:
        return QString(tr("BACK"));
    default:
        return QString();
    }
}

void TaskMenu::before(const MenuDialog &menuDialog)
{

}


MenuType TaskMenu::typeForIndex(int index) const
{
    switch (index) {
    case TaskMenuBack:
        return Back;
    case TaskMenuTaskInfo:
        return Choice;
    default:
        return List;
        break;
    }
}

void TaskMenu::execAtIndex(int index, const MenuDialog &menuDialog)
{
    switch (index) {
        case TaskMenuTaskInfo:
            VarioQt::instance().getMainUI()->dynamicUI->showPage(DynamicUI::SpecialPages::TaskStatusPage);
            VarioQt::instance().getMainUI()->gpioLongPressed(Vario::GPIO3);
            break;
        }
}

MenuListModel* TaskMenu::modelForIndex(int index)
{
    switch(index) {
    case TaskMenuWaypoints:
        return new WaypointMenu(this);
    case TaskMenuEditTask:
        return new TaskEditMenu(this);
    case TaskMenuImportTask:
        return new TaskImportMenu(this);
    default:
        return nullptr;
    }
}

const QString TaskMenu::descAtIndex(int index) const
{
    switch(index) {
    case TaskMenuWaypoints:
        return QString(tr("Manage waypoints for tasks."));
    case TaskMenuEditTask:
        return QString(tr("Create/Edit task."));
    case TaskMenuImportTask:
        return QString(tr("Import task."));
  case TaskMenuTaskInfo:
        return QString(tr("Show task info page"));
    default:
        return QString();
    }
}
