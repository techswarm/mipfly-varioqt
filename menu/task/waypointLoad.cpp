/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "menudialog.h"
#include "ui_menudialog.h"
#include "sound/varioSound.h"
#include "utils/vario_common.h"
#include "navUtils/variocompute.h"
#include "menu/location/locationsmenu.h"
#include <QSettings>
#include "VarioQt.h"
#include "NumericInput.h"
#include "waypointLoad.h"
#include "ChooseInput.h"
#include "TextInput.h"
#include <QDir>

enum
{
    WaypointMenuLoadBack
};

WaypointLoad::WaypointLoad(MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent)
{
    wpm = VarioQt::instance().getWaypointManager();

    QDir dir(waypointManager::getWaypoitPath());

    filenames = dir.entryList(QDir::NoDotAndDotDot | QDir::Files ,QDir::Name);
}

int WaypointLoad::rowCount(const QModelIndex &parent) const
{
    return 1 + filenames.count();
}

const QString WaypointLoad::captionForIndex(int index) const
{
    switch(index) {
    case WaypointMenuLoadBack:
        return QString(tr("BACK"));
    }
    if(index>WaypointMenuLoadBack)
    {
        return filenames.at(index-(WaypointMenuLoadBack+1));
    }
    return QString();
}

void WaypointLoad::before(const MenuDialog &menuDialog)
{

}


MenuType WaypointLoad::typeForIndex(int index) const
{
    switch (index) {
    case WaypointMenuLoadBack:
        return Back;
        break;

    default:
        return Choice;
        break;
    }
}

void WaypointLoad::execAtIndexNC(int index, MenuDialog &menuDialog)
{
    if(index >= WaypointMenuLoadBack)
    {
        ChooseInput *ci = new ChooseInput;
        ci->setCaption(tr("FILE"));
        ci->setValue(tr("ACTION?"));
        ci->setChouse1(tr("REMOVE"));
        ci->setChouse2(tr("CANCEL"));
        ci->setChouse3(tr("LOAD"));

        ci->exec();

        int selected = ci->choice();
        delete ci;

        if(selected == 3)
        {
            WptLoadResult wptLoadResult = wpm->loadWPsFromFile(filenames.at(index-(WaypointMenuLoadBack+1)));

            switch (wptLoadResult) {
                case eWPTLOAD_UNKNOWN:
                    Warning::showWarning(tr("ERROR"),tr("ERROR"),1);
                    break;
                case eWPTLOAD_OK:
                    Warning::showWarning(tr("INFO"),tr("WPT LOADED"),1);
                    break;
                case eWPTLOAD_FAIL:
                    Warning::showWarning(tr("ERROR"),tr("LOAD FAILED"),1);
                    break;
                case eWPTLOAD_PARTIALOK:
                    Warning::showWarning(tr("INFO"),tr("NOT ALL WPT LOADED"),1);
                    break;
                case eWPTUNSUPPORTEDFORMAT:
                    Warning::showWarning(tr("ERROR"),tr("UNSUPPORTED FORMAT"),1);
                    break;
                case eWPTLOAD_FORMATUNKOWN:
                    Warning::showWarning(tr("ERROR"),tr("UNKNOWN FORMAT"),1);
                    break;
                default:
                    Warning::showWarning(tr("ERROR"),tr("UNHANDLED STATE"),1);
            }

            menuDialog.goBack();
        }
        if(selected == 1)
        {
            wpm->remove(filenames.at(index-(WaypointMenuLoadBack+1)));
            Warning::showWarning(tr("INFO"),tr("WPT REMOVED"),1);
            emit dataChanged(QModelIndex(),QModelIndex());
        }
    }
}

MenuListModel* WaypointLoad::modelForIndex(int index)
{
    return nullptr;
}

const QString WaypointLoad::descAtIndex(int index) const
{
    return QString();
}
