/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "menudialog.h"
#include "ui_menudialog.h"
#include "sound/varioSound.h"
#include "utils/vario_common.h"
#include "navUtils/variocompute.h"
#include "menu/location/locationsmenu.h"
#include <QSettings>
#include "VarioQt.h"
#include "NumericInput.h"
#include "waypointMenu.h"
#include "taskNavigationMenu.h"
#include "taskNavigationMenuTp.h"
#include "taskNavigationMenuWp.h"
#include "taskNavigationMenuZoom.h"
#include "navWaypoint/task.h"

enum
{
    TaskNavigationMenuNavToTo,
    TaskNavigationMenuNavToTask,
    TaskNavigationMenuNavToWp,
    TaskNavigationMenuResetTask,
    TaskNavigationMenuBackToTask,
    TaskNavigationMenuZoomFunctions,
    TaskNavigationMenuCenterToTO,
    TaskNavigationMenuOptimumStart,
    TaskNavigationMenuBack
};

TaskNavigationMenu::TaskNavigationMenu(MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent)
{

}

int TaskNavigationMenu::rowCount(const QModelIndex &parent) const
{
    return 9;
}

const QString TaskNavigationMenu::captionForIndex(int index) const
{
    Task * pTask = VarioQt::instance().getTask();
    switch(index) {
    case TaskNavigationMenuNavToTo:
        return QString(tr("Nav to TO"));
    case TaskNavigationMenuNavToTask:
        return QString(tr("Nav to task Tp"));
    case TaskNavigationMenuNavToWp:
        return QString(tr("Nav to WP"));
    case TaskNavigationMenuBackToTask:
        return QString(tr("Back to task"));
    case TaskNavigationMenuResetTask:
        return QString(tr("Reset task"));
    case TaskNavigationMenuZoomFunctions:
        return QString(tr("Zoom"));
    case TaskNavigationMenuCenterToTO:
    {
        if(pTask->getCenterToTP1() == true)
            return QString(tr("C. to TP1 ON"));
        else return QString(tr("C. to TP1 OFF"));
    }
    case TaskNavigationMenuOptimumStart:
    {
        if(pTask->getOptimumStart() == true)
            return QString(tr("Optimum start ON"));
        else return QString(tr("Optimum start OFF"));
    }
    case TaskNavigationMenuBack:
        return QString(tr("Back"));
    default:
        return QString();
    }
}

void TaskNavigationMenu::before(const MenuDialog &menuDialog)
{

}

void TaskNavigationMenu::beforeNC(MenuDialog &menuDialog)
{
    menuDialog.move(0,0);
}

MenuType TaskNavigationMenu::typeForIndex(int index) const
{
    switch(index) {
    case TaskNavigationMenuNavToTo:
    case TaskNavigationMenuResetTask:
    case TaskNavigationMenuBackToTask:
    case TaskNavigationMenuCenterToTO:
    case TaskNavigationMenuOptimumStart:
    case TaskNavigationMenuBack:
        return Choice;
        break;
    default:
        return List;
    }
}

void TaskNavigationMenu::execAtIndexNC(int index, MenuDialog &menuDialog)
{
    Task * pTask = VarioQt::instance().getTask();
    TakeoffThread* toth = VarioQt::instance().getTakeOffThread();
    switch (index) {
    case TaskNavigationMenuNavToTo:
    {
        Waypoint wp;
        wp.setCoordonate(toth->getTakeoffPos());
        wp.setHeight(toth->getTakeoffHeight());
        wp.setName(QString("TO"));
        pTask->navToSpecialWp(wp);
        Warning::showWarning(tr("NAV"),tr("Nav to T.O."),3);
        menuDialog.goBack();
        menuDialog.hide();
        break;
    }
    case TaskNavigationMenuCenterToTO:
    {
        pTask->setCenterToTP1(!pTask->getCenterToTP1());
        menuDialog.goBack();
        menuDialog.hide();
        break;
    }
    case TaskNavigationMenuOptimumStart:
    {
        pTask->setOptimumStart(!pTask->getOptimumStart());
        emit dataChanged(QModelIndex(),QModelIndex());
        break;
    }
    case TaskNavigationMenuResetTask:
    {
        pTask->resetTask();
        Warning::showWarning(tr("NAV"),tr("Task reset"),3);
        menuDialog.goBack();
        menuDialog.hide();
        break;
    }
    case TaskNavigationMenuBackToTask:
    {
        pTask->navToTaskTp();
        Warning::showWarning(tr("NAV"),tr("Nav to task"),3);
        menuDialog.goBack();
        menuDialog.hide();
        break;
    }
    case TaskNavigationMenuBack:
    {
        menuDialog.goBack();
        menuDialog.hide();
        break;
    }
    default:
        break;
    }
}

MenuListModel* TaskNavigationMenu::modelForIndex(int index)
{
    switch(index) {
    case TaskNavigationMenuNavToTask:
        return new TaskNavigationMenuTp(this);
    case TaskNavigationMenuNavToWp:
        return new TaskNavigationMenuWp(this);
    case TaskNavigationMenuZoomFunctions:
        return new TaskNavigationMenuZoom(this);
        break;
    }
    return nullptr;
}

const QString TaskNavigationMenu::descAtIndex(int index) const
{
    switch(index) {
    case TaskNavigationMenuNavToTo:
        return QString(tr("Navigate to takeoff point"));
    case TaskNavigationMenuResetTask:
        return QString(tr("Reset task to first turnpoint"));
    case TaskNavigationMenuBackToTask:
        return QString(tr("Go back to task navigation after a waypoint navigation was activated. Task will be resumed from the last activated turnpoint"));
    case TaskNavigationMenuNavToTask:
        return QString(tr("Navigate to a turnpoint defined in the task"));
    case TaskNavigationMenuNavToWp:
        return QString(tr("Navigate to a loaded waypoint, not necessary from the active task"));
    case TaskNavigationMenuZoomFunctions:
        return QString(tr("Zoom functionality for task widget."));
    default:
        return QString();
    }
}
