/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "taskImportMenu.h"
#include "menudialog.h"
#include "ui_menudialog.h"
#include "ChooseInput.h"
#include "Warning.h"
#include <QDir>

#ifdef __ARMEL__
const QString TASKPATH = "/mnt/mipfly/taskFiles/";
#else
const QString TASKPATH = "taskFiles/";
#endif

enum
{
    TaskImportMenuBack
};

TaskImportMenu::TaskImportMenu(MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent)
{

    QDir dir(TASKPATH);

    filenames = dir.entryList(QDir::NoDotAndDotDot | QDir::Files ,QDir::Name);
}

int TaskImportMenu::rowCount(const QModelIndex &parent) const
{
    return 1 + filenames.count();
}

const QString TaskImportMenu::captionForIndex(int index) const
{
    switch(index) {
    case TaskImportMenuBack:
        return QString(tr("BACK"));
    }
    if(index>TaskImportMenuBack)
    {
        return filenames.at(index-(TaskImportMenuBack+1));
    }
    return QString();
}

void TaskImportMenu::before(const MenuDialog &menuDialog)
{

}


MenuType TaskImportMenu::typeForIndex(int index) const
{
    switch (index) {
    case TaskImportMenuBack:
        return Back;
        break;

    default:
        return Choice;
        break;
    }
}

void TaskImportMenu::execAtIndexNC(int index, MenuDialog &menuDialog)
{
    if(index >= TaskImportMenuBack)
    {
        ChooseInput *ci = new ChooseInput;
        ci->setCaption(tr("FILE"));
        ci->setValue(tr("ACTION?"));
        ci->setChouse1(tr("CANCEL"));
        ci->setChouse2(tr("DELETE"));
        ci->setChouse3(tr("IMPORT"));

        ci->exec();

        int selected = ci->choice();
        delete ci;

        if(selected == 3)
        {
            //wpm->loadWPsFromFile(filenames.at(index-(WaypointMenuLoadBack+1)));

            bool impResult = false;

            impResult = importXcTsk(filenames.at(index-(TaskImportMenuBack+1)));

            if (impResult) {
                Warning::showWarning(tr("INFO"),tr("TASK IMPORTED"),1);
            } else {
                Warning::showWarning(tr("INFO"),tr("TASK IMPORT FAILED"),1);
            }

            menuDialog.goBack();
        }
        if(selected == 2)
        {
            //implement delete procedure
            removeXcTsk(filenames.at(index-(TaskImportMenuBack+1)));
            Warning::showWarning(tr("INFO"),tr("REMOVED"),1);
        }
    }
}

MenuListModel* TaskImportMenu::modelForIndex(int index)
{
    return nullptr;
}

const QString TaskImportMenu::descAtIndex(int index) const
{
    return QString();
}
/*
taskType - the type of task used, right now only type CLASSIC is used - for other task types different set of the following attributes can be defined
version natural number corresponding to the version of the format for specific taskType - now fixed to 1
earthModel - settings for distance computation for cylinder sizes; either WGS84 or a FAI sphere
turnpoints[i].type one of TAKEOFF / SSS / ESS - if present, sets the special meaning of this turnpoint
TAKEOFF type can be used only for the first turnpoint and is not required. If present, means that the first turnpoint is not used for navigation
SSS and ESS turnpoints must appear exactly once and SSS turnpoint must appear before ESS
the last turnpoint is always goal. If the last turnpoint is marked ESS than it is identical to goal and shares his line/cylinder settings. Otherwise ESS is always cylinder.
turnpoints[i].waypoint.name and - turnpoints[i].waypoint.description correspond to the name/description of the waypoint in the waypoints file
turnpoints[i].waypoint.lat, turnpoints[i].waypoint.lon - numbers corresponding to wgs84 latitude and longitude in degrees, with positive value for east and north hemisphere
turnpoints[i].waypoint.altSmoothed - altitude of the waypoint in meters AMSL
turnpoints[i].radius - radius of the turnpoint in meters
takeoff.timeOpen, takeoff.timeClose - takeoff window open/close times (see below how the times are stored)
sss.type - RACE for the race to goal start type or time gates, ELAPSED-TIME for elapsed-time start type
sss.direction - direction of crossing the start cylinder, one of ENTER or EXIT
sss.timeGates - in most cases array with just one window opening time. For time gates start, array of times for each gate, ordered chronologically
sss.timeClose - start close time
goal.type - line or cylinder. In the case of line, the radius of the turnpoint corresponds to the 1/2 of the total length of the goal line. Ground orientation of the line is always perpendicular to the azimuth to the center of last turnpoint
goal.deadline - time corresponding to both the task deadline and goal deadline
Time format
Times are stored in UTC as 9 characters long JSON string of the form HH:MM:SSZ. The Z character at the end has no real meaning and its main purpose is to remind all the implementators and pilots who may read the task (e.g. if stored in the file) that the time is in UTC.

*/

bool TaskImportMenu::importXcTsk(QString fileName) {

    QString settings;
    QFile file;
    int SSSIndex = -1;
    int ESSIndex = -1;
    int wpIndex = 0;
    bool importedOk = true;

    file.setFileName(TASKPATH+"//"+fileName);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    settings = file.readAll();
    file.close();

    Task *pTask = VarioQt::instance().getTask();
    pTask->timeGates.clear();
    pTask->clearTurnpoints();
    QList<TurnPoint> turnPoints;

    QJsonDocument jsonDocument = QJsonDocument::fromJson(settings.toUtf8());
    QJsonObject jsonObjectXcTsk = jsonDocument.object();

    //string,required
    QString taskType = jsonObjectXcTsk.value(QString("taskType")).toString();
    if (taskType.contains("CLASSIC", Qt::CaseInsensitive) == false) importedOk = false;

    //int version =  jsonObjectXcTsk.value(QString("version")).toInt();
    //QString earthModel = jsonObjectXcTsk.value(QString("earthModel")).toString();

    /*Start gate parameters*/
    QJsonObject sssObj = jsonObjectXcTsk.value("sss").toObject();
    QString sssType = sssObj.value("type").toString();
    QString sssDirection = sssObj.value("direction").toString();
    QJsonArray sssTimeGateArray = sssObj["timeGates"].toArray();

    QList<QTime> timeGates;

    foreach (const QJsonValue & timeGateStr, sssTimeGateArray) {
        qDebug() << timeGateStr.toString();
        QTime timeGate;
        timeGate = QTime::fromString(timeGateStr.toString(), "hh:mm:ss'Z'");
        qDebug() << "Parsed:" << timeGate.toString("hh:mm:ss.zzz");
        timeGates.append(timeGate);
    }

    //QJsonObject takeoffObj = jsonObjectXcTsk.value("takeoff").toObject();
    qDebug() << "timeOpen" << sssObj.value("timeOpen").toString();
    qDebug() << "timeClose" << sssObj.value("timeClose").toString();

    QJsonObject goalObj = jsonObjectXcTsk.value("goal").toObject();
    QString goalType =  goalObj.value("type").toString();

    QTime goalDeadline;
    goalDeadline = QTime::fromString(goalObj.value("deadline").toString(), "hh:mm:ss'Z'");

    //array of objects, required
    QJsonArray arrayOfTurnpoints = jsonObjectXcTsk["turnpoints"].toArray();

    int wpCount = arrayOfTurnpoints.size();

    foreach (const QJsonValue & jsonValue, arrayOfTurnpoints) {

        QJsonObject obj = jsonValue.toObject();

        //string - one of "TAKEOFF" / "SSS" / "ESS"
        QString wpType = obj.value("type").toString();

        //number,required
        int radius =  obj.value( "radius").toInt();

        QJsonObject wpObj = obj.value("waypoint").toObject();

        /* Task object requires takeoff as tp index 0... xctsk includes takeoff optionaly*/
        if ((wpIndex == 0) && (wpType.contains("TAKEOFF", Qt::CaseInsensitive) == false)) {
            /*Code to create a wp for the takeoff*/
        }

        if (wpType.contains("SSS", Qt::CaseInsensitive) == true) SSSIndex = wpIndex;
        if (wpType.contains("ESS", Qt::CaseInsensitive) == true) ESSIndex = wpIndex;

        TurnPoint tp;
        tp.setRadiusLengthMeters(radius);

        Waypoint wp;
        Coord coord;
        coord.setLat(wpObj.value("lat").toDouble());
        coord.setLon(wpObj.value("lon").toDouble());
        wp.setCoordonate(coord);

        wp.setName(wpObj.value("name").toString());
        wp.setHeight(wpObj.value("altSmoothed").toInt());
        tp.setWaypoint(wp);

        qDebug() << "Index:" << wpIndex;

        if (wpIndex == SSSIndex) {
            if (sssDirection.contains("ENTER", Qt::CaseInsensitive) == true) {
                tp.setGoal(TurnPoint::GoalType::CylinterEnter);
            } else if (sssDirection.contains("EXIT", Qt::CaseInsensitive) == true) {
                tp.setGoal(TurnPoint::GoalType::CylinderExit);
            } else {
                importedOk = false;
            }
        } else {
            tp.setGoal(TurnPoint::GoalType::CylinterEnter);
        }

        /* For goal, set to cylinder or line */
        if (wpIndex == (wpCount-1)) {
            if (goalType.contains("CYLINDER", Qt::CaseInsensitive) == true) {
                tp.setGoal(TurnPoint::GoalType::CylinterEnter);
            } else if (goalType.contains("LINE", Qt::CaseInsensitive) == true) {
                tp.setGoal(TurnPoint::GoalType::Line);
            } else {
                importedOk = false;
            }
        }

        pTask->addTurnpoint(tp);
        wpIndex++;
    }

    if (SSSIndex != -1) pTask->setSSSIndex(SSSIndex);
    if (ESSIndex != -1) pTask->setESSIndex(ESSIndex);

    pTask->setTaskDeadline(goalDeadline);

    if (sssType.contains("RACE", Qt::CaseInsensitive) == true) {
        if (timeGates.size() > 1) {
            /* Change to multiple timegates */
            pTask->setTaskType(Task::TaskType::TimeGates);
            pTask->setSSSTimeGates(timeGates);
        } else {
            pTask->setTaskType(Task::TaskType::Race);
            pTask->setSSSTime((QTime)timeGates[0]);
        }
    } else if (sssType.contains("ELAPSED-TIME", Qt::CaseInsensitive) == true) {
        pTask->setTaskType(Task::TaskType::ElapsedTime);
    } else {
        importedOk = false;
    }

    pTask->saveMTKFileDefault();
    pTask->resetTask();

    return importedOk;
}

bool TaskImportMenu::removeXcTsk(QString fileName)
{
    QFile file;
    file.setFileName(TASKPATH+"//"+fileName);
    return file.remove();
}
