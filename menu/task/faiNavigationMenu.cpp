/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "menudialog.h"
#include "ui_menudialog.h"
#include "sound/varioSound.h"
#include "utils/vario_common.h"
#include "navUtils/variocompute.h"
#include "menu/location/locationsmenu.h"
#include <QSettings>
#include "VarioQt.h"
#include "NumericInput.h"
#include "waypointMenu.h"
#include "faiNavigationMenu.h"
#include "taskNavigationMenuTp.h"
#include "taskNavigationMenuWp.h"
#include "taskNavigationMenuZoom.h"
#include "navWaypoint/task.h"
#include "navUtils/faiassistcompute.h"

enum
{
    FAINavigationMenuSetTp1,
    FAINavigationMenuSetTp2,
    FAINavigationMenuDetBase,
    FAINavigationMenuNavToFai,
    FAINavigationMenuNavToTp1,
    FAINavigationMenuZoom,
    FAINavigationMenuBack
};

FAINavigationMenu::FAINavigationMenu(MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent)
{

}

int FAINavigationMenu::rowCount(const QModelIndex &parent) const
{
    return 7;
}

const QString FAINavigationMenu::captionForIndex(int index) const
{
    Task * pTask = VarioQt::instance().getTask();
    switch(index) {
    case FAINavigationMenuSetTp1:
        return QString(tr("Set TP1"));
    case FAINavigationMenuSetTp2:
        return QString(tr("Set TP2"));
    case FAINavigationMenuDetBase:
        return QString(tr("Det Base"));
    case FAINavigationMenuNavToFai:
        return QString(tr("Nav to FAI"));
    case FAINavigationMenuNavToTp1:
        return QString(tr("Nav to Tp1"));
    case FAINavigationMenuZoom:
        return QString(tr("Zoom"));
    case FAINavigationMenuBack:
        return QString(tr("Back"));
    default:
        return QString();
    }
}

void FAINavigationMenu::before(const MenuDialog &menuDialog)
{

}

void FAINavigationMenu::beforeNC(MenuDialog &menuDialog)
{
    menuDialog.move(0,0);
}

MenuType FAINavigationMenu::typeForIndex(int index) const
{
    switch(index) {
    case FAINavigationMenuSetTp1:
    case FAINavigationMenuSetTp2:
    case FAINavigationMenuDetBase:
    case FAINavigationMenuNavToFai:
    case FAINavigationMenuNavToTp1:
    case FAINavigationMenuBack:
        return Choice;
        break;
    default:
        return List;
    }
}

void FAINavigationMenu::execAtIndexNC(int index, MenuDialog &menuDialog)
{
    Task * pTask = VarioQt::instance().getTask();
    TakeoffThread* toth = VarioQt::instance().getTakeOffThread();
    FaiAssistCompute *pFac = VarioQt::instance().getFaiAssistCompute();
    switch (index) {
    case FAINavigationMenuSetTp1:
    {
        if(pFac->getTp1Marked() == true)
        {
            ChooseInput *ci = new ChooseInput;
            ci->setCaption(tr("WARNING"));
            ci->setValue(tr("Reset TP1?"));
            ci->setChouse1(tr("CANCEL"));
            ci->setChouse3(tr("CONFIRM"));
            ci->exec();
            int selected = ci->choice();
            delete ci;

            if(selected == 3)
            {
                pFac->markTp1();
                Warning::showWarning(tr("INFO"),tr("TP1 marked!"));
            }
        }
        else
        {
            pFac->markTp1();
            Warning::showWarning(tr("INFO"),tr("TP1 marked!"));
        }
        break;
    }
    case FAINavigationMenuSetTp2:
    {
        if(pFac->getTp1Marked() == false)
        {
            Warning::showWarning(tr("ERROR"),tr("TP1 not marked!"));
        }
        else
        {
            if(pFac->getTp2Marked() == true)
            {
                ChooseInput *ci = new ChooseInput;
                ci->setCaption(tr("WARNING"));
                ci->setValue(tr("Reset TP2?"));
                ci->setChouse1(tr("CANCEL"));
                ci->setChouse3(tr("CONFIRM"));
                ci->exec();
                int selected = ci->choice();
                delete ci;

                if(selected == 3)
                {
                    pFac->markTp2();
                    Warning::showWarning(tr("INFO"),tr("TP2 marked!"));
                    pFac->computeFaiArea();
                    pTask->setNavToOptimisedFai(true);
                }
            }
            else
            {
                pFac->markTp2();
                Warning::showWarning(tr("INFO"),tr("TP2 marked!"));
                pFac->computeFaiArea();
                pTask->setNavToOptimisedFai(true);
            }
        }
        break;
    }
    case FAINavigationMenuDetBase:
    {
        if(pFac->getAreaComputed() == true)
        {
            ChooseInput *ci = new ChooseInput;
            ci->setCaption(tr("WARNING"));
            ci->setValue(tr("Recompute area?"));
            ci->setChouse1(tr("CANCEL"));
            ci->setChouse3(tr("CONFIRM"));
            ci->exec();
            int selected = ci->choice();
            delete ci;

            if(selected == 3)
            {
                pFac->computeBaseLeg();
                pFac->computeFaiArea();
                pTask->setNavToOptimisedFai(true);
            }
        }
        else
        {
            pFac->computeBaseLeg();
            pFac->computeFaiArea();
            pTask->setNavToOptimisedFai(true);
        }
        break;
    }
    case FAINavigationMenuNavToFai:
    {
        pTask->setNavToOptimisedFai(true);
        break;
    }
    case FAINavigationMenuNavToTp1:
    {
        Waypoint wp;
        wp.setCoordonate(pFac->getTp1());
        wp.setHeight(VarioQt::instance().getElevation()->getElevation(wp.coordonate()));
        wp.setName("Tp1");
        pTask->navToSpecialWp(wp);
        break;
    }
    default:
        break;
    }
    menuDialog.goBack();
    menuDialog.hide();
}

MenuListModel* FAINavigationMenu::modelForIndex(int index)
{
    switch(index) {
    case FAINavigationMenuZoom:
        return new TaskNavigationMenuZoom(this);
    }
    return nullptr;
}

const QString FAINavigationMenu::descAtIndex(int index) const
{
    switch(index) {
    default:
        return QString();
    }
}
