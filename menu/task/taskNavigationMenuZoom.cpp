/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "menudialog.h"
#include "ui_menudialog.h"
#include "sound/varioSound.h"
#include "utils/vario_common.h"
#include "navUtils/variocompute.h"
#include "menu/location/locationsmenu.h"
#include <QSettings>
#include "VarioQt.h"
#include "NumericInput.h"
#include "waypointMenu.h"
#include "taskNavigationMenuZoom.h"
#include "taskNavigationMenuTp.h"
#include "taskNavigationMenuWp.h"
#include "navWaypoint/task.h"

enum
{
    TaskNavigationMenuZoomIn,
    TaskNavigationMenuZoomOut,
    TaskNavigationMenuZoomToExtent,
    TaskNavigationMenuZoomBack
};

TaskNavigationMenuZoom::TaskNavigationMenuZoom(MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent)
{

}

int TaskNavigationMenuZoom::rowCount(const QModelIndex &parent) const
{
    return 4;
}

const QString TaskNavigationMenuZoom::captionForIndex(int index) const
{
    switch(index) {
    case TaskNavigationMenuZoomIn:
        return QString(tr("Zoom In"));
    case TaskNavigationMenuZoomOut:
        return QString(tr("Zoom Out"));
    case TaskNavigationMenuZoomToExtent:
        return QString(tr("Zoom to Fit"));
    case TaskNavigationMenuZoomBack:
        return QString(tr("Back"));
    default:
        return QString();
    }
}

void TaskNavigationMenuZoom::before(const MenuDialog &menuDialog)
{

}

void TaskNavigationMenuZoom::beforeNC(MenuDialog &menuDialog)
{
    menuDialog.move(0,252);
}

MenuType TaskNavigationMenuZoom::typeForIndex(int index) const
{
    switch(index) {
    case TaskNavigationMenuZoomBack:
        return Back;
        break;
    default:
        return Choice;
    }
}

void TaskNavigationMenuZoom::execAtIndexNC(int index, MenuDialog &menuDialog)
{
    TaskPlot * pTaskPlot = VarioQt::instance().getTaskPlot();
    TrianglePlot * pTrPlt = VarioQt::instance().getTrianglePlot();
    switch (index) {
    case TaskNavigationMenuZoomIn:
    {
        if(pTaskPlot->isVisible())
            pTaskPlot->zoomIn();
        if(pTrPlt->isVisible())
            pTrPlt->zoomIn();
        break;
    }
    case TaskNavigationMenuZoomOut:
    {
        if(pTaskPlot->isVisible())
            pTaskPlot->zoomOut();
        if(pTrPlt->isVisible())
            pTrPlt->zoomOut();
        break;
    }
    case TaskNavigationMenuZoomToExtent:
    {
        if(pTaskPlot->isVisible())
            pTaskPlot->zoomToExtent();
        if(pTrPlt->isVisible())
            pTrPlt->zoomToExtent();
        break;
    }
    default:
        break;
    }
}

MenuListModel* TaskNavigationMenuZoom::modelForIndex(int index)
{
    return nullptr;
}

const QString TaskNavigationMenuZoom::descAtIndex(int index) const
{
    return QString();
}
