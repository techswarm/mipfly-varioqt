/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "taskEditMenu.h"
#include "menudialog.h"
#include "ui_menudialog.h"
#include "sound/varioSound.h"
#include "utils/vario_common.h"
#include "navUtils/variocompute.h"
#include "menu/location/locationsmenu.h"
#include <QSettings>
#include "VarioQt.h"
#include "NumericInput.h"
#include "taskTypeMenu.h"
#include "TimeInput.h"
#include "timeGatesMenu.h"
#include "turnpointMenu.h"
#include "ChooseInput.h"


enum
{
    TaskEditMenuTypeCaption,
    TaskEditMenuType,
    TaskEditMenuSSSOpenCaption,
    TaskEditMenuSSSOpen,
    TaskEditMenuDeadlineCaption,
    TaskEditMenuDeadline,
    TaskEditMenuGates,
    TaskEditMenuTurnPoints,
    TaskEditMenuPrepare,
    TaskEditMenuClear,
    TaskEditMenuBack
};

TaskEditMenu::TaskEditMenu(MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent)
{

}

int TaskEditMenu::rowCount(const QModelIndex &parent) const
{
    return 11;
}

const QString TaskEditMenu::captionForIndex(int index) const
{
    switch(index) {
    case TaskEditMenuTypeCaption:
        return QString(tr("-TASK TYPE-"));
    case TaskEditMenuType:
    {
        if(pTask->getTaskType() == Task::ElapsedTime)return QString(tr("ELAPSED TIME"));
        if(pTask->getTaskType() == Task::Race)return QString(tr("RACE"));
        if(pTask->getTaskType() == Task::TimeGates)return QString(tr("TIME GATES"));
    }
    case TaskEditMenuSSSOpenCaption:
        return QString(tr("-SSS OPEN-"));
    case TaskEditMenuSSSOpen:
        return pTask->getSSSTime().toString("hh:mm:ss");
    case TaskEditMenuDeadlineCaption:
        return QString(tr("-TASK DEADLINE-"));
    case TaskEditMenuDeadline:
        return pTask->getTaskDeadline().toString("hh:mm:ss");
    case TaskEditMenuGates:
        return QString(tr("TIME GATES"));
    case TaskEditMenuTurnPoints:
        return QString(tr("TURN POINTS"));
    case TaskEditMenuPrepare:
        return QString(tr("SAVE/PREPARE/RESET"));
    case TaskEditMenuClear:
        return QString(tr("CLEAR"));
    case TaskEditMenuBack:
        return QString(tr("BACK"));
    default:
        return QString();
    }
}

void TaskEditMenu::before(const MenuDialog &menuDialog)
{

}


MenuType TaskEditMenu::typeForIndex(int index) const
{
    switch (index) {
    case TaskEditMenuBack:
        return Back;
        break;
    case TaskEditMenuDeadlineCaption:
    case TaskEditMenuSSSOpenCaption:
    case TaskEditMenuTypeCaption:
        return Caption;
        break;
    case TaskEditMenuDeadline:
    case TaskEditMenuSSSOpen:
    case TaskEditMenuPrepare:
    case TaskEditMenuClear:
        return Choice;
        break;
    default:
        return List;
        break;
    }
}

void TaskEditMenu::execAtIndex(int index, const MenuDialog &menuDialog)
{
    switch (index) {
    case TaskEditMenuSSSOpen:
    {
        TimeInput *ti = new TimeInput;
        ti->setTime(pTask->getSSSTime());
        ti->exec();
        pTask->setSSSTime(ti->time());
        delete ti;
        emit dataChanged(QModelIndex(), QModelIndex());
        break;
    }
    case TaskEditMenuDeadline:
    {
        TimeInput *ti = new TimeInput;
        ti->setTime(pTask->getTaskDeadline());
        ti->exec();
        pTask->setTaskDeadline(ti->time());
        delete ti;
        emit dataChanged(QModelIndex(), QModelIndex());
        break;
    }
    case TaskEditMenuPrepare:
    {
        pTask->resetTask();
        pTask->saveMTKFileDefault();
        Warning::showWarning(tr("INFO"),tr("TASK PREPARED"));
        break;
    }
    case TaskEditMenuClear:
    {
        ChooseInput *ci = new ChooseInput;
        ci->setCaption(tr("CLEAR"));
        ci->setValue(tr("DELETE?"));
        ci->setChouse1(tr("CANCEL"));
        ci->setChouse3(tr("DELETE"));

        ci->exec();

        if(ci->choice() == 3)pTask->clear();
        emit dataChanged(QModelIndex(),QModelIndex());
        delete ci;
        break;
    }
    }
}

MenuListModel* TaskEditMenu::modelForIndex(int index)
{
    switch(index) {
    case TaskEditMenuType:
        return new TaskTypeMenu(this);
    case TaskEditMenuGates:
        return new TimeGatesMenu(this);
    case TaskEditMenuTurnPoints:
        return new TurnpointMenu(this);
    default:
        return nullptr;
    }
}

const QString TaskEditMenu::descAtIndex(int index) const
{
    switch(index) {
    case TaskEditMenuType:
        return QString(tr("Select competition rules for the task."));
    case TaskEditMenuSSSOpen:
        return QString(tr("Choose the opening time for the task. If this is a Time Gate competition this will also be the first time gate."));
    case TaskEditMenuDeadline:
        return QString(tr("This is the time until you can score competition points."));
    case TaskEditMenuGates:
        return QString(tr("If this is a Time Gate competition you can add new Time Gates here."));
    case TaskEditMenuTurnPoints:
        return QString(tr("Add/Edit turnpoints."));
    case TaskEditMenuPrepare:
        return QString(tr("Prepare task to be flown. This action is also done automatically at takeoff."));
    case TaskEditMenuClear:
        return QString(tr("Clear task."));
    default:
        return QString();
    }
}
