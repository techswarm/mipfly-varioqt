/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "menudialog.h"
#include "ui_menudialog.h"
#include "sound/varioSound.h"
#include "utils/vario_common.h"
#include "navUtils/variocompute.h"
#include "menu/location/locationsmenu.h"
#include <QSettings>
#include "VarioQt.h"
#include "NumericInput.h"
#include "waypointMenu.h"
#include "taskNavigationMenuTp.h"
#include "taskNavigationMenuWp.h"
#include "navWaypoint/task.h"

enum
{
    TaskNavigationMenuWpBack,
    TaskNavigationMenuWpSort
};

TaskNavigationMenuWp::TaskNavigationMenuWp(MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent)
{
    pTask = VarioQt::instance().getTask();
    pWpm = VarioQt::instance().getWaypointManager();
    pWpm->sortAlphabetically();
}

int TaskNavigationMenuWp::rowCount(const QModelIndex &parent) const
{
    return pWpm->waypointsSorted.count()+2;
}

const QString TaskNavigationMenuWp::captionForIndex(int index) const
{
    switch(index) {
    case TaskNavigationMenuWpBack:
        return QString(tr("Back"));
    case TaskNavigationMenuWpSort:
        if(sortByName)return QString(tr("Sort by dist"));
        else return QString(tr("Sort by name"));
    }

    if(index <= pWpm->waypointsSorted.count()+1)
    {
        return pWpm->waypointsSorted.at(index-2).name();
    }
    return QString();
}

void TaskNavigationMenuWp::before(const MenuDialog &menuDialog)
{

}

MenuType TaskNavigationMenuWp::typeForIndex(int index) const
{
    switch(index) {
    case TaskNavigationMenuWpBack:
        return Back;
        break;
    default:
        return Choice;
    }
}

void TaskNavigationMenuWp::execAtIndexNC(int index, MenuDialog &menuDialog)
{
    switch (index) {
    case TaskNavigationMenuWpSort:
    {
        if(sortByName)
        {
            Glider * pGlider = VarioQt::instance().getGlider();
            pWpm->sortByDistance(pGlider->getPosition());
            sortByName = false;
            emit dataChanged(QModelIndex(), QModelIndex());
        }
        else
        {
            pWpm->sortAlphabetically();
            sortByName = true;
            emit dataChanged(QModelIndex(), QModelIndex());
        }
        break;
    }
    default:
        break;
    }
    if(index <= pWpm->waypointsSorted.count() && index > 1)
    {
        pTask->navToSpecialWp(pWpm->waypointsSorted.at(index-2));
        Warning::showWarning(tr("NAV"),tr("Waypoint set"),3);
        menuDialog.goBack();
        menuDialog.hide();
    }
}

MenuListModel* TaskNavigationMenuWp::modelForIndex(int index)
{
    switch(index) {
        break;
    }
    return nullptr;
}

const QString TaskNavigationMenuWp::descAtIndex(int index) const
{
    switch(index) {
        case TaskNavigationMenuWpSort:
    {
        return QString("Change wp navigation sorting to either alphabetical or distance based sort");
        break;
    }
    }

    if(index>TaskNavigationMenuWpSort)
    {
        QString ret;
        ret += pWpm->waypointsSorted.at(index-(TaskNavigationMenuWpSort+1)).name()+"\n";
        ret += "Lat "+pWpm->waypointsSorted.at(index-(TaskNavigationMenuWpSort+1)).coordonate().getLatStr()+"\n";
        ret += "Lon "+pWpm->waypointsSorted.at(index-(TaskNavigationMenuWpSort+1)).coordonate().getLonStr()+"\n";
        ret += "Height "+QString::number(pWpm->waypointsSorted.at(index-(TaskNavigationMenuWpSort+1)).height());
        return ret;
    }
    return QString();
}
