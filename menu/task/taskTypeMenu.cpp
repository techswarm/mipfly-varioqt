/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "menudialog.h"
#include "ui_menudialog.h"
#include "sound/varioSound.h"
#include "utils/vario_common.h"
#include "navUtils/variocompute.h"
#include "menu/location/locationsmenu.h"
#include <QSettings>
#include "VarioQt.h"
#include "NumericInput.h"
#include "waypointMenu.h"
#include "taskTypeMenu.h"
#include "navWaypoint/task.h"

enum
{
    TaskTypeMenuRace,
    TaskTypeMenuTimeGates,
    TaskTypeMenuElapsedTime
};

TaskTypeMenu::TaskTypeMenu(MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent)
{

}

int TaskTypeMenu::rowCount(const QModelIndex &parent) const
{
    return 3;
}

const QString TaskTypeMenu::captionForIndex(int index) const
{
    switch(index) {
    case TaskTypeMenuRace:
        return QString(tr("RACE"));
    case TaskTypeMenuTimeGates:
        return QString(tr("TIME GATES"));
    case TaskTypeMenuElapsedTime:
        return QString(tr("ELAPSED TIME"));
    default:
        return QString();
    }
}

void TaskTypeMenu::before(const MenuDialog &menuDialog)
{

}


MenuType TaskTypeMenu::typeForIndex(int index) const
{
    switch(index) {
    case TaskTypeMenuRace:
    case TaskTypeMenuTimeGates:
    case TaskTypeMenuElapsedTime:
        return Choice;
        break;
    default:
        return List;
    }
}

void TaskTypeMenu::execAtIndexNC(int index, MenuDialog &menuDialog)
{
    Task * pTask = VarioQt::instance().getTask();
    switch (index) {
    case TaskTypeMenuRace:
        pTask->setTaskType(Task::Race);
        break;
    case TaskTypeMenuTimeGates:
        pTask->setTaskType(Task::TimeGates);
        break;
    case TaskTypeMenuElapsedTime:
        pTask->setTaskType(Task::ElapsedTime);
        break;
    default:
        break;
    }
    menuDialog.goBack();
}

MenuListModel* TaskTypeMenu::modelForIndex(int index)
{

    switch(index) {
    case TaskTypeMenuRace:
    case TaskTypeMenuTimeGates:
    case TaskTypeMenuElapsedTime:
        break;
    }
    return nullptr;
}

const QString TaskTypeMenu::descAtIndex(int index) const
{
    switch(index) {
    default:
        return QString();
    }
}
