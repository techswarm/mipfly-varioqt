/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "taskMenu.h"
#include "menudialog.h"
#include "ui_menudialog.h"
#include "sound/varioSound.h"
#include "utils/vario_common.h"
#include "navUtils/variocompute.h"
#include "menu/location/locationsmenu.h"
#include <QSettings>
#include "VarioQt.h"
#include "NumericInput.h"
#include "waypointMenu.h"
#include "timeGatesMenu.h"
#include "TimeInput.h"
#include "ChooseInput.h"

enum
{
    TimeGatesMenuAdd,
    TimeGatesMenuClear,
    TimeGatesMenuBack,
    TimeGatesMenuCaption,
    TimeGatesMenuSSS,
};

TimeGatesMenu::TimeGatesMenu(MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent)
{

}

int TimeGatesMenu::rowCount(const QModelIndex &parent) const
{
    return 5 + pTask->timeGates.count();
}

const QString TimeGatesMenu::captionForIndex(int index) const
{
    switch(index) {
    case TimeGatesMenuAdd:
        return QString(tr("ADD"));
    case TimeGatesMenuClear:
        return QString(tr("CLEAR"));
    case TimeGatesMenuBack:
        return QString(tr("BACK"));
    case TimeGatesMenuCaption:
        return QString(tr("-TIME GATES-"));
    case TimeGatesMenuSSS:
        return pTask->getSSSTime().toString("hh:mm:ss");
    }
    return pTask->timeGates.at(index-(TimeGatesMenuSSS+1)).toString("hh:mm:ss");
}

void TimeGatesMenu::before(const MenuDialog &menuDialog)
{

}


MenuType TimeGatesMenu::typeForIndex(int index) const
{
    switch (index) {
    case TimeGatesMenuBack:
        return Back;
        break;
    case TimeGatesMenuAdd:
    case TimeGatesMenuClear:
        return Choice;
    case TimeGatesMenuCaption:
        return Caption;
    default:
        return Choice;
        break;
    }
}

void TimeGatesMenu::execAtIndex(int index, const MenuDialog &menuDialog)
{
    switch (index) {
    case TimeGatesMenuAdd:
    {
        TimeInput *ti = new TimeInput;
        if(pTask->timeGates.count() == 0)
        {
            ti->setTime(pTask->getSSSTime());
        }
        if(pTask->timeGates.count() == 1)
        {
            int seconds = pTask->getSSSTime().secsTo(pTask->timeGates.at(0));
            QTime t = pTask->timeGates.at(0);
            t = t.addSecs(seconds);
            ti->setTime(t);
        }
        if(pTask->timeGates.count()>1)
        {
            int count = pTask->timeGates.count();
            int seconds = pTask->timeGates.at(count - 2).secsTo(pTask->timeGates.at(count - 1));
            QTime t = pTask->timeGates.at(count - 1);
            t = t.addSecs(seconds);
            ti->setTime(t);
        }
        ti->exec();
        pTask->timeGates.append(ti->time());

        pTask->saveMTKFileDefault();

        delete ti;
        emit dataChanged(QModelIndex(),QModelIndex());
        break;
    }
    case TimeGatesMenuClear:
    {
        ChooseInput *ci = new ChooseInput;
        ci->setCaption(tr("CLEAR"));
        ci->setValue(tr("DELETE?"));
        ci->setChouse1(tr("CANCEL"));
        ci->setChouse3(tr("DELETE"));

        ci->exec();

        if(ci->choice() == 3)
            pTask->timeGates.clear();
        emit dataChanged(QModelIndex(),QModelIndex());

        delete ci;
        break;
    }
    }
    if(index > TimeGatesMenuSSS)
    {
        TimeInput *ti = new TimeInput;
        ti->setTime(pTask->timeGates.at(index-(TimeGatesMenuSSS+1)));

        ti->exec();
        pTask->timeGates[index-(TimeGatesMenuSSS+1)] = ti->time();

        delete ti;
        pTask->saveMTKFileDefault();
        emit dataChanged(QModelIndex(),QModelIndex());
    }
}

MenuListModel* TimeGatesMenu::modelForIndex(int index)
{
    switch(index) {

    break;
    }
    return nullptr;
}

const QString TimeGatesMenu::descAtIndex(int index) const
{
    switch(index) {
    //case TaskMenuWaypoints:
    //    return QString(tr("Manage waypoints for tasks."));
    default:
        return QString();
    }
}
