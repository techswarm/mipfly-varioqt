/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "turnpointMenu.h"
#include "menudialog.h"
#include "ui_menudialog.h"
#include "sound/varioSound.h"
#include "utils/vario_common.h"
#include "navUtils/variocompute.h"
#include "menu/location/locationsmenu.h"
#include <QSettings>
#include "VarioQt.h"
#include "navWaypoint/task.h"
#include "turnpointEditMenu.h"
#include "ChooseInput.h"


enum
{
    TurnpointMenuAdd,
    TurnpointMenuClear,
    TurnpointMenuBack,
    TurnpointMenuTPsCaption
};

TurnpointMenu::TurnpointMenu(MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent)
{

}

int TurnpointMenu::rowCount(const QModelIndex &parent) const
{
    return 4+pTask->getTurnPoints().count();
}

const QString TurnpointMenu::captionForIndex(int index) const
{
    switch(index) {
    case TurnpointMenuAdd:
        return QString(tr("ADD"));
    case TurnpointMenuClear:
        return QString(tr("CLEAR"));
    case TurnpointMenuBack:
        return QString(tr("BACK"));
    case TurnpointMenuTPsCaption:
        return QString(tr("-TPs-"));
    }
    if(index>TurnpointMenuTPsCaption)
    {
        QString tpStr;
        tpStr += pTask->getTurnpointTypeStr(index-(TurnpointMenuTPsCaption+1))+"-";
        tpStr += pTask->getTurnPoints().at(index-(TurnpointMenuTPsCaption+1)).getWaypoint().name();
        return tpStr;
    }
    return QString();
}

void TurnpointMenu::before(const MenuDialog &menuDialog)
{

}


MenuType TurnpointMenu::typeForIndex(int index) const
{
    switch (index) {
    case TurnpointMenuBack:
        return Back;
        break;
    case TurnpointMenuClear:
        return Choice;
        break;
    case TurnpointMenuTPsCaption:
        return Caption;
        break;
    default:
        return List;
        break;
    }
}

void TurnpointMenu::execAtIndex(int index, const MenuDialog &menuDialog)
{
    switch (index) {
    case TurnpointMenuClear:
    {
        ChooseInput *ci = new ChooseInput;
        ci->setCaption(tr("CLEAR"));
        ci->setValue(tr("DELETE?"));
        ci->setChouse1(tr("CANCEL"));
        ci->setChouse3(tr("DELETE"));

        ci->exec();

        if(ci->choice() == 3)pTask->clearTurnpoints();
        emit dataChanged(QModelIndex(),QModelIndex());
        delete ci;
        break;
    }

    }
}

MenuListModel* TurnpointMenu::modelForIndex(int index)
{
    switch(index) {
    case TurnpointMenuAdd:
        return new TurnpointEditMenu(-1,this);
        break;
    }
    if(index>TurnpointMenuTPsCaption)
    {
        int actionTpIndex = index-(TurnpointMenuTPsCaption+1);

        ChooseInput *ci = new ChooseInput;
        ci->setCaption(tr("Move/Edit"));
        QString tpStr;
        tpStr += pTask->getTurnPoints().at(index-(TurnpointMenuTPsCaption+1)).getWaypoint().name();
        ci->setValue(tpStr);
        ci->setChouse1(tr("Up"));
        ci->setChouse2(tr("Down"));
        ci->setChouse3(tr("Edit"));

        ci->exec();

        if(ci->choice() == 3)
        {
            delete ci;
            return new TurnpointEditMenu(actionTpIndex,this);
        }
        if(ci->choice() == 1)
        {
            pTask->moveTpUp(actionTpIndex);
        }
        if(ci->choice() == 2)
        {
            pTask->moveTpDown(actionTpIndex);
        }
        emit dataChanged(QModelIndex(),QModelIndex());
        delete ci;
    }
    return nullptr;
}

const QString TurnpointMenu::descAtIndex(int index) const
{
    switch(index) {
    case TurnpointMenuAdd:
        return QString(tr("Add a new turnpoint from an existing waypoint."));
    case TurnpointMenuClear:
        return QString(tr("Clear all turnpoints."));
    }
    if(index>TurnpointMenuTPsCaption)
    {
        QString ret = QString(tr("Edit/modify turnpoint and set custom SSS and ESS."));
        ret += "\n["+pTask->getTurnPoints().at(index-(TurnpointMenuTPsCaption+1)).getWaypoint().description()+"]\n";
        ret += pTask->getTurnpointTypeStr(index-(TurnpointMenuTPsCaption+1))+"-";
        ret += pTask->getTurnPoints().at(index-(TurnpointMenuTPsCaption+1)).getWaypoint().name()+"\n";
        ret += "Radius/Line length:"+QString::number(pTask->getTurnPoints().at(index-(TurnpointMenuTPsCaption+1)).getRadiusLengthMeters())+"m \n";
        ret += "Lat "+ pTask->getTurnPoints().at(index-(TurnpointMenuTPsCaption+1)).getWaypoint().coordonate().getLatStr()+"\n";
        ret += "Lon "+ pTask->getTurnPoints().at(index-(TurnpointMenuTPsCaption+1)).getWaypoint().coordonate().getLonStr()+"\n";
        ret += "Height "+ QString::number(pTask->getTurnPoints().at(index-(TurnpointMenuTPsCaption+1)).getWaypoint().height())+"\n";
        if(pTask->getTurnPoints().at(index-(TurnpointMenuTPsCaption+1)).getGoal() == TurnPoint::GoalType::Line)ret += QString(tr("LINE"));
        if(pTask->getTurnPoints().at(index-(TurnpointMenuTPsCaption+1)).getGoal() == TurnPoint::GoalType::CylinterEnter)ret += QString(tr("CYLINDER ENTER"));
        if(pTask->getTurnPoints().at(index-(TurnpointMenuTPsCaption+1)).getGoal() == TurnPoint::GoalType::CylinderExit)ret += QString(tr("CYLINDER EXIT"));

        return ret;
    }
    return QString("");
}
