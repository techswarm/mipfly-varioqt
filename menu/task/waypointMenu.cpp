/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "waypointMenu.h"
#include "menudialog.h"
#include "ui_menudialog.h"
#include "sound/varioSound.h"
#include "utils/vario_common.h"
#include "navUtils/variocompute.h"
#include "menu/location/locationsmenu.h"
#include <QSettings>
#include "VarioQt.h"
#include "NumericInput.h"
#include "waypointEdit.h"
#include "waypointLoad.h"
#include "ChooseInput.h"
#include "TextInput.h"
#include "utils/unitmanager.h"

enum
{
    WaypointMenuAdd,
    WaypointMenuClear,
    WaypointMenuSave,
    WaypointMenuLoad,
    WaypointMenuBack
};

WaypointMenu::WaypointMenu(MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent)
{
    wpm = VarioQt::instance().getWaypointManager();
}

int WaypointMenu::rowCount(const QModelIndex &parent) const
{
    return 5 + wpm->waypoints.count();
}

const QString WaypointMenu::captionForIndex(int index) const
{
    switch(index) {
    case WaypointMenuAdd:
        return QString(tr("ADD WP"));
    case WaypointMenuClear:
        return QString(tr("CLEAR ALL WPs"));
    case WaypointMenuSave:
        return QString(tr("SAVE WP LIST"));
    case WaypointMenuLoad:
        return QString(tr("LOAD WP LIST"));
    case WaypointMenuBack:
        return QString(tr("BACK"));
    }
    if(index>WaypointMenuBack)
    {
        return wpm->waypoints.at(index-(WaypointMenuBack+1)).name();
    }

    return QString();
}

void WaypointMenu::before(const MenuDialog &menuDialog)
{

}


MenuType WaypointMenu::typeForIndex(int index) const
{
    switch (index) {
    case WaypointMenuBack:
        return Back;
        break;
    case WaypointMenuClear:
        return Choice;
        break;
    case WaypointMenuSave:
        return Choice;
        break;

    default:
        return List;
        break;
    }
}

void WaypointMenu::execAtIndexNC(int index, MenuDialog &menuDialog)
{
    switch (index) {
    case WaypointMenuClear:
    {
        ChooseInput *ci = new ChooseInput;
        ci->setCaption(tr("CLEAR"));
        ci->setValue(tr("DELETE?"));
        ci->setChouse1(tr("CANCEL"));
        ci->setChouse3(tr("DELETE"));

        ci->exec();
        if(ci->choice() == 3)
            wpm->clearDefaultWPs();
        dataChanged(QModelIndex(), QModelIndex());
        delete ci;
        break;
    }
    case WaypointMenuSave:
    {
        TextInput *ti = new TextInput;
        ti->setMixt();
        ti->exec();
        QString fileName = ti->value();
        delete ti;

        waypointManager *pWaypointMgr = VarioQt::instance().getWaypointManager();

        bool save = true;

        if(pWaypointMgr->waypointFileExists(fileName))
        {
            save = false;
            ChooseInput *ci = new ChooseInput;
            ci->setCaption(tr("FILE EXISTS"));
            ci->setValue(tr("REPLACE?"));
            ci->setChouse1(tr("CANCEL"));
            ci->setChouse3(tr("REPLACE"));

            ci->exec();
            if(ci->choice() == 3)save = true;
            delete ci;
        }

        if(save)
        {
            fileName += ".wpt";
            pWaypointMgr->saveWPsToFile(fileName);
            Warning::showWarning(tr("INFO"),tr("File saved"));
        }
        else
        {
            Warning::showWarning(tr("ERROR"),tr("File not saved"));
        }

        break;
    }
    }
}

MenuListModel* WaypointMenu::modelForIndex(int index)
{
    switch(index) {
    case WaypointMenuAdd:
        return new WaypointEditMenu(-1,this);
        break;
    case WaypointMenuLoad:
        return new WaypointLoad(this);
        break;
    }

    if(index>WaypointMenuBack)
    {
        return new WaypointEditMenu(index-(WaypointMenuBack+1),this);
    }
    return nullptr;
}

const QString WaypointMenu::descAtIndex(int index) const
{
    switch(index) {
    }
    if(index>WaypointMenuBack)
    {
        QString ret;
        ret += wpm->waypoints.at(index-(WaypointMenuBack+1)).name()+"\n";
        ret += tr("Lat")+" "+wpm->waypoints.at(index-(WaypointMenuBack+1)).coordonate().getLatStr()+"\n";
        ret += tr("Lon")+" "+wpm->waypoints.at(index-(WaypointMenuBack+1)).coordonate().getLonStr()+"\n";
        SIHeight height(wpm->waypoints.at(index-(WaypointMenuBack+1)).height());
        ret += "Height "+QString::number(height.getValuePrefferedFormat(),'f',0)+height.getPrefferedFormat()+"\n";
        ret += "["+wpm->waypoints.at(index-(WaypointMenuBack+1)).description()+"]";
        return ret;
    }
    return QString();
}
