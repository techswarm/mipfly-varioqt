/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "turnpointEditMenu.h"
#include "menudialog.h"
#include "ui_menudialog.h"
#include "sound/varioSound.h"
#include "utils/vario_common.h"
#include "navUtils/variocompute.h"
#include "menu/location/locationsmenu.h"
#include <QSettings>
#include "VarioQt.h"
#include "NumericInput.h"
#include "NumericInputDecimal.h"
#include "taskTypeMenu.h"
#include "TimeInput.h"
#include "timeGatesMenu.h"
#include "turnpointMenu.h"
#include "goalTypeMenu.h"
#include "utils/unitmanager.h"

#include "waypointSelectMenu.h"


enum
{
    TurnpointEditMenuWPCaption,
    TurnpointEditMenuWP,
    TurnpointEditMenuGoalCaption,
    TurnpointEditMenuGoal,
    TurnpointEditMenuRadiusLengthCaption,
    TurnpointEditMenuRadiusLength,
    TurnpointEditMenuAdd,
    TurnpointEditMenuSetSSS,
    TurnpointEditMenuSetESS,
    TurnpointEditMenuDelete,
    TurnpointEditMenuBack
};

TurnpointEditMenu::TurnpointEditMenu(int tpId, MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent)
{
    turnpointId = tpId;
    if(tpId >= 0)
    {
        pTask->editedTurnpoint = pTask->getTurnPoints().at(tpId);
    }
}

int TurnpointEditMenu::rowCount(const QModelIndex &parent) const
{
    return 11;
}

const QString TurnpointEditMenu::captionForIndex(int index) const
{
    switch(index) {
    case TurnpointEditMenuWPCaption:
        return QString(tr("-TURN POINT-"));
    case TurnpointEditMenuWP:
        return pTask->editedTurnpoint.getWaypoint().name();
    case TurnpointEditMenuGoalCaption:
        return QString(tr("-GOAL-"));
    case TurnpointEditMenuGoal:
    {
        if(pTask->editedTurnpoint.getGoal() == TurnPoint::GoalType::Line)return QString(tr("LINE"));
        if(pTask->editedTurnpoint.getGoal() == TurnPoint::GoalType::CylinterEnter)return QString(tr("CILINDER ENTER"));
        if(pTask->editedTurnpoint.getGoal() == TurnPoint::GoalType::CylinderExit)return QString(tr("CILINDER EXIT"));
    }
    case TurnpointEditMenuRadiusLengthCaption:
        return QString(tr("-RADIUS/LENGTH-"));
    case TurnpointEditMenuRadiusLength:
    {
        SIDistance dist(pTask->editedTurnpoint.getRadiusLengthMeters()/1000);
        return QString::number(dist.getValuePrefferedFormat(),'f',3)+" "+dist.getPrefferedFormat();
    }
    case TurnpointEditMenuAdd:
    {
        if(turnpointId < 0)
        {
            return QString(tr("ADD"));
        }
        else
        {
            return QString(tr("SAVE"));
        }
    }
    case TurnpointEditMenuBack:
        return QString(tr("BACK"));
    case TurnpointEditMenuSetSSS:
        return QString(tr("SET SSS"));
    case TurnpointEditMenuSetESS:
        return QString(tr("SET ESS"));
    case TurnpointEditMenuDelete:
        return QString(tr("DELETE"));
    }
    return QString();
}

void TurnpointEditMenu::before(const MenuDialog &menuDialog)
{

}


MenuType TurnpointEditMenu::typeForIndex(int index) const
{
    switch (index) {
    case TurnpointEditMenuBack:
        return Back;
        break;
    case TurnpointEditMenuWPCaption:
    case TurnpointEditMenuGoalCaption:
    case TurnpointEditMenuRadiusLengthCaption:
        return Caption;
        break;
    case TurnpointEditMenuAdd:
    case TurnpointEditMenuRadiusLength:
        return Choice;
        break;
    case TurnpointEditMenuSetSSS:
        if(turnpointId >= 0)return Choice;
        else return Caption;
    case TurnpointEditMenuSetESS:
        if(turnpointId >= 0)return Choice;
        else return Caption;
    case TurnpointEditMenuDelete:
        if(turnpointId >= 0)return Choice;
        else return Caption;
    default:
        return List;
        break;
    }
}

void TurnpointEditMenu::execAtIndex(int index, const MenuDialog &menuDialog)
{
    switch (index) {
    case TurnpointEditMenuRadiusLength:
    {
        NumericInputDecimal *ni = new NumericInputDecimal();
        ni->setDisplayDecimals(3);
        SIDistance dist(pTask->editedTurnpoint.getRadiusLengthMeters()/1000);
        ni->setValue(dist.getValuePrefferedFormat()*1000);
        ni->setCaption(dist.getPrefferedFormat());
        ni->exec();
        dist.setValuePrefferedFormat(((float)(ni->value()))/1000);
        //limmit cillinder size to 100 km
        if(dist.value()>100)dist.setValue(100);
        pTask->editedTurnpoint.setRadiusLengthMeters(dist.value()*1000);
        delete ni;
        break;
    }

    case TurnpointEditMenuSetSSS:
    {
        pTask->setSSSIndex(turnpointId);
        Warning::showWarning(tr("INFO"),tr("SSS SAVED"),1);
        break;
    }
    case TurnpointEditMenuSetESS:
    {
        pTask->setESSIndex(turnpointId);
        Warning::showWarning(tr("INFO"),tr("ESS SAVED"),1);
    }
    case TurnpointEditMenuDelete:
    {
        ChooseInput *ci = new ChooseInput;
        ci->setCaption(tr("CLEAR"));
        ci->setValue(tr("DELETE?"));
        ci->setChouse1(tr("CANCEL"));
        ci->setChouse3(tr("DELETE"));

        ci->exec();

        if(ci->choice()==3)
        {
            pTask->deleteTurnpoint(turnpointId);
            turnpointId = -1;
            pTask->editedTurnpoint = TurnPoint();
            Warning::showWarning(tr("INFO"),tr("TP DELETED"),1);
        }

        delete ci;
        emit dataChanged(QModelIndex(),QModelIndex());
    }

    }
}

void TurnpointEditMenu::execAtIndexNC(int index, MenuDialog &menuDialog)
{
    switch (index) {
    case TurnpointEditMenuAdd:
    {
        if(turnpointId < 0)
        {
            //add new turnpoint
            pTask->addTurnpoint(pTask->editedTurnpoint);
            pTask->editedTurnpoint = TurnPoint();
            emit dataChanged(QModelIndex(),QModelIndex());
            Warning::showWarning(tr("INFO"),tr("TP ADDED"),1);
        }
        else
        {
            pTask->saveTurnpoint(pTask->editedTurnpoint,turnpointId);
            Warning::showWarning(tr("INFO"),tr("TP SAVED"),1);
        }
        pTask->resetTask();
        pTask->saveMTKFileDefault();
        menuDialog.goBack();
        break;
    }
    }
}

MenuListModel* TurnpointEditMenu::modelForIndex(int index)
{
    switch(index) {
    case TurnpointEditMenuWP:
        return new WaypointSelectMenu(this);
    case TurnpointEditMenuGoal:
        return new GoalTypeMenu(this);
        break;
    }
    return nullptr;
}

const QString TurnpointEditMenu::descAtIndex(int index) const
{
    switch(index) {
    default:
        return QString();
    }
}
