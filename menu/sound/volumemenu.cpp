/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "volumemenu.h"

#include <QSettings>

#include "menudialog.h"
#include "ui_menudialog.h"
#include "sound/varioSound.h"
#include "utils/vario_common.h"
#include "VarioQt.h"

enum VolumeIndex
{
    VolumeUp,
    VolumeDown,
    VolumeBack
};

int VolumeMenu::rowCount(const QModelIndex &parent) const
{
    return 3;
}

const QString VolumeMenu::captionForIndex(int index) const
{
    switch(index) {
    case VolumeUp:
        return QString(tr("UP"));
    case VolumeDown:
        return QString(tr("DOWN"));
    case VolumeBack:
        return QString(tr("BACK"));
    default:
        return QString();
    }
}

MenuType VolumeMenu::typeForIndex(int index) const
{
    if (index == VolumeBack) {
        return Back;
    }
    return Choice;
}

void VolumeMenu::before(const MenuDialog &menuDialog)
{
    int percent = volMgr.getPercent();
    menuDialog.getUi().valLabel->setText(QString("%1\%").arg(percent));

    menuDialog.getUi().hSlider->setMinimum(0);
    menuDialog.getUi().hSlider->setMaximum(100);
    menuDialog.getUi().hSlider->setValue(percent);

    menuDialog.getUi().valLabel->setVisible(true);
    menuDialog.getUi().hSlider->setVisible(true);
}

void VolumeMenu::execAtIndex(int index, const MenuDialog &menuDialog)
{
    if (index == VolumeUp) {
        volMgr.increase();

        int percent = volMgr.getPercent();
        menuDialog.getUi().valLabel->setText(QString("%1\%").arg(percent));
        menuDialog.getUi().hSlider->setValue(percent);

        VarioQt::instance().getVarioSound()->varioSoundSetCMPS(15);

        QSettings settings;
        settings.setValue(Vario::SETTINGS_VOL, percent);
        //settings.sync();
    } else {
        volMgr.decrease();

        int percent = volMgr.getPercent();
        menuDialog.getUi().valLabel->setText(QString("%1\%").arg(percent));
        menuDialog.getUi().hSlider->setValue(percent);

        VarioQt::instance().getVarioSound()->varioSoundSetCMPS(15);

        QSettings settings;
        settings.setValue(Vario::SETTINGS_VOL, percent);
        //settings.sync();
    }
}
