/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "soundMenu.h"

#include <QSettings>

#include "menudialog.h"
#include "ui_menudialog.h"
#include "sound/varioSound.h"
#include "utils/vario_common.h"
#include "VarioQt.h"
#include "menu/sound/volumemenu.h"
#include "menu/sound/mixermenu.h"
#include "NumericInput.h"
#include "QDebug"
#include "mixerMenuSelect.h"

enum SoundIndex
{
    SoundVolume,
    SoundSilentOnGround,
    SoundSquare,
    SoundMixer,
    SoundMixerSelect,
    SoundBack
};

SoundMenu::SoundMenu(VarioSound &varioSound,  MenuListModel *parentMenu, QObject *parent) : MenuListModel(parentMenu, parent), sndMgr(&varioSound)
{

}

int SoundMenu::rowCount(const QModelIndex &parent) const
{
    return 6;
}

const QString SoundMenu::captionForIndex(int index) const
{
    switch(index) {
    case SoundVolume:
        return QString(tr("VOLUME"));
        break;
    case SoundSilentOnGround:
        if(sndMgr.getSilentOnGround() == true) return QString(tr("SILENT ON GROUND ON"));
        else return QString(tr("SILENT ON GROUND OFF"));
        break;
    case SoundSquare:
        if(sndMgr.getSquare() == true) return QString(tr("SQUARE ON"));
        else return QString(tr("SQUARE OFF"));
        break;
    case SoundMixer:
        return QString(tr("SOUND MIXER"));
        break;
    case SoundMixerSelect:
        return QString(tr("SOUND MIXER SELECT"));
        break;
    case SoundBack:
        return QString(tr("BACK"));
        break;
    default:
        return QString();
    }
}

MenuType SoundMenu::typeForIndex(int index) const
{
    switch (index) {
    case SoundVolume:
        return List;
        break;
    case SoundMixerSelect:
        return List;
        break;
    case SoundMixer:
        return List;
        break;
    case SoundBack:
        return Back;
        break;
    default:
        return Choice;
        break;
    }
}

void SoundMenu::before(const MenuDialog &menuDialog)
{

}

void SoundMenu::execAtIndex(int index, const MenuDialog &menuDialog)
{
    switch (index) {
    case SoundSquare:
        sndMgr.setSquare(!sndMgr.getSquare());
        emit dataChanged(QModelIndex(), QModelIndex());
        break;
    case SoundSilentOnGround:
        sndMgr.setSilentOnGround(!sndMgr.getSilentOnGround());
        emit dataChanged(QModelIndex(), QModelIndex());
        break;
    default:
        break;
    }
}

MenuListModel* SoundMenu::modelForIndex(int index)
{
    if(index == SoundVolume)return new VolumeMenu(this);
    if(index == SoundMixer)return new MixerMenu(*(VarioQt::instance().getVarioSound()),this);
    if(index == SoundMixerSelect)return new MixerMenuSelect(this);
    return nullptr;
}

const QString SoundMenu::descAtIndex(int index) const
{
    switch(index) {
    case SoundVolume:
        return QString(tr("Change system volume."));
    case SoundSilentOnGround:
        return QString(tr("Set if vario is silent on ground."));
    case SoundSquare:
        return QString(tr("Toggle square/sine wave. Sine is more pleasant for the ear but less loud especially for low frequency."));
    case SoundMixer:
        return QString(tr("Use to set the vario tone."));
    case SoundMixerSelect:
        return QString(tr("Select sound mixer to be used."));
    default:
        return QString();
    }
}
