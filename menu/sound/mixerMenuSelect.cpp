/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mixerMenuSelect.h"
#include "menudialog.h"
#include "ui_menudialog.h"
#include "sound/varioSound.h"
#include "utils/vario_common.h"
#include "navUtils/variocompute.h"
#include <QSettings>
#include <QDir>
#include <QDebug>
#include <Warning.h>
#include "VarioQt.h"

enum MixerMenuSelectActions
{
    MixerMenuSelectActionNothing,
    MixerMenuSelectActionGet,
    MixerMenuSelectActionConnect
};

MixerMenuSelect::MixerMenuSelect(MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent)
{
    QDir dir("soundm");
    dir.setFilter(QDir::Files);
    dir.setSorting(QDir::Name);
    mixers = dir.entryList();

    connect(&updateManager,SIGNAL(grabUpdateResult(bool)),this,SLOT(updateGrabResult(bool)),Qt::UniqueConnection);
    connect(&updateManager,SIGNAL(connectionStatusResult(bool)),this,SLOT(internetAvailable(bool)),Qt::UniqueConnection);

    QSettings settings;
    QString key = settings.value("accountKey","").toString();
    QString id = settings.value("accountId","").toString();
    updateManager.setUserId(id);
    updateManager.setUserKey(key);

    timer.setInterval(1000);
    connect(&timer,SIGNAL(timeout()),this,SLOT(timerEvent()));
    timer.start();
}

int MixerMenuSelect::rowCount(const QModelIndex &parent) const
{
    return mixers.count()+3;
}

const QString MixerMenuSelect::captionForIndex(int index) const
{
    if(index==0)return QString(tr("Legacy"));
    if(index==1)return QString(tr("Get mixers"));
    if(index-2 < mixers.count())
    {
        QString str = mixers[index-2];
        str.replace(QString(".txt"), QString(""));
        return str;
    }
    else
        return QString(tr("BACK"));
}

void MixerMenuSelect::before(const MenuDialog &menuDialog)
{

}


MenuType MixerMenuSelect::typeForIndex(int index) const
{
    if (index == mixers.count()+2) {
        return Back;
    }
    return Choice;
}

void MixerMenuSelect::execAtIndex(int index, const MenuDialog &menuDialog)
{
    if(index == 0)
    {
        VarioQt::instance().getVarioSound()->setUseConfigFile(false);
        Warning::showWarning(tr("INFO"),tr("Config saved"), 1);
    }
    else if(index == 1)
    {
        generalMenuAction = MixerMenuSelectActionGet;
        updateManager.checkConnection();
        qDebug()<<"Check internet connection...";
        Warning::showWarning(tr("INFO"),tr("Check Conn"));
    }
    else if(index - 2 < mixers.count())
    {
        QString str = mixers[index - 2];
        VarioQt::instance().getVarioSound()->setConfigFilename(str);
        Warning::showWarning(tr("INFO"),tr("Config saved"), 1);
    }
}

MenuListModel* MixerMenuSelect::modelForIndex(int index)
{
    return nullptr;
}

void MixerMenuSelect::internetAvailable(bool connected)
{
    if(connected)
    {
        if(generalMenuAction == MixerMenuSelectActionGet)
        {
            Warning::showWarning(tr("INFO"),tr("Conn OK"));
            updateManager.grabMixerUpdateFiles();
        }
    }
    else
    {
        Warning::showWarning(tr("ERROR"),tr("Not Connected"));
        generalMenuAction = MixerMenuSelectActionNothing;
    }
}

void MixerMenuSelect::updateGrabResult(bool success)
{
    if(success)
    {
        if(generalMenuAction == MixerMenuSelectActionGet)
        {
            updateManager.deployMixer();
            Warning::showWarning(tr("INFO"),tr("Success!"));
            emit dataChanged(QModelIndex(), QModelIndex());
        }
    }
    else
    {
        Warning::showWarning(tr("ERROR"),tr("Not Updated"));
    }
    generalMenuAction = MixerMenuSelectActionNothing;
}

void MixerMenuSelect::timerEvent()
{
    if(generalMenuAction == MixerMenuSelectActionGet)
    {
        marquee+=1;
        QString dots;
        if(marquee%3 == 0)dots=".";
        if(marquee%3 == 1)dots="..";
        if(marquee%3 == 2)dots="...";
        Warning::showWarning(tr("INFO"),tr("Downloading")+dots,2);
    }
}
