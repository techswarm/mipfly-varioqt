/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SOUNDEMENU_H
#define SOUNDEMENU_H

#include "menu/menulistmodel.h"
#include "sound/varioMixer.h"
#include "sound/varioSound.h"

class SoundMenu : public MenuListModel
{
    Q_OBJECT
    using MenuListModel::MenuListModel;
public:
    SoundMenu(VarioSound &varioSound, MenuListModel *parentMenu = nullptr, QObject *parent = Q_NULLPTR);
    int rowCount(const QModelIndex &parent) const Q_DECL_OVERRIDE;
    const QString captionForIndex(int index) const Q_DECL_OVERRIDE;
    MenuType typeForIndex(int index) const Q_DECL_OVERRIDE;
    void before(const MenuDialog &menuDialog) Q_DECL_OVERRIDE;

    void execAtIndex(int index, const MenuDialog &menuDialog) Q_DECL_OVERRIDE;

    MenuListModel *modelForIndex(int index)Q_DECL_OVERRIDE;
    const QString descAtIndex(int index) const;
private:
    SoundManager sndMgr;

};

#endif // VOLUMEMENU_H
