/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mixermenu.h"

#include <QSettings>

#include "menudialog.h"
#include "ui_menudialog.h"
#include "sound/varioSound.h"
#include "utils/vario_common.h"
#include "VarioQt.h"
#include "NumericInput.h"
#include "QDebug"

enum MixerIndex
{
    SoundTest,
    SoundTestCMPS,
    SoundLiniar,
    SoundSnifferTh,
    SoundUpThr,
    SoundDownThr,
    SoundUpF0,
    SoundUpF1000,
    SoundI0,
    SoundI1000,
    SoundDownF0,
    SoundDownF1000,
    SoundBack
};

MixerMenu::MixerMenu(VarioSound &varioSound,  MenuListModel *parentMenu, QObject *parent) : MenuListModel(parentMenu, parent), mixMgr(&varioSound)
{

}

int MixerMenu::rowCount(const QModelIndex &parent) const
{
    return 13;
}

const QString MixerMenu::captionForIndex(int index) const
{
    switch(index) {
    case SoundTest:
        if(mixMgr.getTesting() == true) return QString(tr("TEST ON"));
        else return QString(tr("TEST OFF"));
        break;
    case SoundTestCMPS:
        return QString(tr("Test cm/s ")) + QString::number(mixMgr.getTestCMPS());
    case SoundLiniar:
    {
        if(mixMgr.getLinear())
        {
            return QString(tr("Lin. Increase: ON"));
        }
        else
        {
            return QString(tr("Lin. Increase: OFF"));
        }

    }
    case SoundSnifferTh:
    {
        QString ret = QString(tr("Sniff cm/s "));
        if(mixMgr.getSniffThr()==0)ret+=tr("OFF");
        else ret+=QString::number(mixMgr.getSniffThr());
        return ret;
    }
    case SoundUpThr:
        return QString(tr("UpThr cm/s ")) + QString::number(mixMgr.getUpThr());
    case SoundDownThr:
        return QString(tr("DownThr cm/s ")) + QString::number(mixMgr.getDownThr());
    case SoundUpF0:
        return QString(tr("Up F0 Hz")) + QString::number(mixMgr.getUpF0());
    case SoundUpF1000:
        return QString(tr("Up F10 Hz")) + QString::number(mixMgr.getUpF1000());
    case SoundI0:
        return QString(tr("Up I0 ms")) + QString::number(mixMgr.getUpI0());
    case SoundI1000:
        return QString(tr("Up I10 ms")) + QString::number(mixMgr.getUpI1000());
    case SoundDownF0:
        return QString(tr("Down F0 Hz")) + QString::number(mixMgr.getDownF0());
    case SoundDownF1000:
        return QString(tr("Down F10 Hz")) + QString::number(mixMgr.getDownF1000());
    case SoundBack:
        return QString(tr("BACK"));
    default:
        return QString();
    }
}

MenuType MixerMenu::typeForIndex(int index) const
{
    switch (index) {
    case SoundBack:
        return Back;
        break;
    default:
        return Choice;
        break;
    }
}

void MixerMenu::before(const MenuDialog &menuDialog)
{

}

void MixerMenu::execAtIndex(int index, const MenuDialog &menuDialog)
{
    switch (index) {
    case SoundTest:
        mixMgr.setTesting(!mixMgr.getTesting());
        emit dataChanged(QModelIndex(), QModelIndex());
        break;
    case SoundLiniar:
        mixMgr.setLinear(!mixMgr.getLinear());
        emit dataChanged(QModelIndex(), QModelIndex());
        break;
    case SoundTestCMPS:
    {
        NumericInput *ni = new NumericInput;

        ni->setValue(mixMgr.getTestCMPS());
        ni->setStep(10);
        ni->setMax(1000);
        ni->setMin(-1000);
        ni->setDisplayScale(1);
        ni->setCaption(tr("Test cm/s"));

        connect(ni,&NumericInput::valueChanged,this,&MixerMenu::testCMPSChanged);

        ni->move(240/2 - ni->width()/2,320/2 - ni->height()/2);
        ni->exec();
        mixMgr.setTestCMPS(ni->value());
        disconnect(ni,&NumericInput::valueChanged,this,&MixerMenu::testCMPSChanged);
        delete ni;
    }
        break;
    case SoundSnifferTh:
    {
        NumericInput *ni = new NumericInput;

        ni->setValue(mixMgr.getSniffThr());
        ni->setStep(10);
        ni->setMax(0);
        ni->setMin(-100);
        ni->setCaption(tr("Sniff. thr cm/s"));

        ni->move(240/2 - ni->width()/2,320/2 - ni->height()/2);
        ni->exec();
        mixMgr.setSniffThr(ni->value());
        delete ni;
    }
        break;
    case SoundUpThr:
    {
        NumericInput *ni = new NumericInput;

        ni->setValue(mixMgr.getUpThr());
        ni->setStep(10);
        ni->setMax(1000);
        ni->setMin(0);
        ni->setCaption(tr("Up thr cm/s"));

        ni->move(240/2 - ni->width()/2,320/2 - ni->height()/2);
        ni->exec();
        mixMgr.setUpThr(ni->value());
        delete ni;
    }
        break;
    case SoundDownThr:
    {
        NumericInput *ni = new NumericInput;

        ni->setValue(mixMgr.getDownThr());
        ni->setStep(10);
        ni->setMax(0);
        ni->setMin(-1000);
        ni->setCaption(tr("Down thr cm/s"));

        ni->move(240/2 - ni->width()/2,320/2 - ni->height()/2);
        ni->exec();
        mixMgr.setDownThr(ni->value());
        delete ni;
    }
        break;
    case SoundUpF0:
    {
        NumericInput *ni = new NumericInput;

        ni->setValue(mixMgr.getUpF0());
        ni->setStep(10);
        ni->setMax(mixMgr.getUpF1000());
        ni->setMin(100);
        ni->setCaption(tr("Up F0 Hz"));

        ni->move(240/2 - ni->width()/2,320/2 - ni->height()/2);
        ni->exec();
        mixMgr.setUpF0(ni->value());
        delete ni;
    }
        break;
    case SoundUpF1000:
    {
        NumericInput *ni = new NumericInput;

        ni->setValue(mixMgr.getUpF1000());
        ni->setStep(10);

        ni->setMin(mixMgr.getDownF0());
        ni->setCaption(tr("Up F10 Hz"));

        ni->move(240/2 - ni->width()/2,320/2 - ni->height()/2);
        ni->exec();
        mixMgr.setUpF1000(ni->value());
        delete ni;
    }
        break;
    case SoundI0:
    {
        NumericInput *ni = new NumericInput;

        ni->setValue(mixMgr.getUpI0());
        ni->setStep(10);

        ni->setMin(mixMgr.getUpI1000());

        ni->setCaption(tr("Up I0 ms"));

        ni->move(240/2 - ni->width()/2,320/2 - ni->height()/2);
        ni->exec();
        mixMgr.setUpI0(ni->value());
        delete ni;
    }
        break;
    case SoundI1000:
    {
        NumericInput *ni = new NumericInput;

        ni->setValue(mixMgr.getUpI1000());
        ni->setStep(10);
        ni->setMax(mixMgr.getUpI0());
        ni->setMin(50);
        ni->setCaption(tr("Up I10 ms"));
        ni->move(240/2 - ni->width()/2,320/2 - ni->height()/2);
        ni->exec();
        mixMgr.setUpI1000(ni->value());
        delete ni;
    }
        break;
    case SoundDownF0:
    {
        NumericInput *ni = new NumericInput;

        ni->setValue(mixMgr.getDownF0());
        ni->setStep(10);

        ni->setMin(mixMgr.getDownF1000());

        ni->setCaption(tr("Down F0 Hz"));

        ni->move(240/2 - ni->width()/2,320/2 - ni->height()/2);
        ni->exec();
        mixMgr.setDownF0(ni->value());
        delete ni;
    }
        break;
    case SoundDownF1000:
    {
        NumericInput *ni = new NumericInput;

        ni->setValue(mixMgr.getDownF1000());
        ni->setStep(10);
        ni->setMax(mixMgr.getDownF0());
        ni->setCaption(tr("Down F10 Hz"));

        ni->move(240/2 - ni->width()/2,320/2 - ni->height()/2);
        ni->exec();
        mixMgr.setDownF1000(ni->value());
        delete ni;
    }
        break;
    default:
        break;
    }

}

void MixerMenu::testCMPSChanged(int cmps)
{
    mixMgr.setTestCMPS(cmps);
}

MenuListModel* MixerMenu::modelForIndex(int index)
{
    return nullptr;
}

const QString MixerMenu::descAtIndex(int index) const
{
    switch(index) {
    case SoundTest:
        return QString(tr("Toggle test on off. Use to test your settings and accomodate with the beeping."));
    case SoundTestCMPS:
        return QString(tr("Set test climb/sink rate."));
    case SoundUpThr:
        return QString(tr("Set climb tone threshold in cm per second."));
    case SoundDownThr:
        return QString(tr("Set sink tone threshold in cm per second."));
    case SoundSnifferTh:
        return QString(tr("Set thermal sniffer threshold. Go all way to 0 to turn it off."));
    case SoundUpF0:
        return QString(tr("Set start frequency for climb."));
    case SoundUpF1000:
        return QString(tr("Set end frequency for climb. Equivalent to 10 m/s."));
    case SoundI0:
        return QString(tr("Set start sound interval for climb in ms."));
    case SoundI1000:
        return QString(tr("Set end sound interval for climb in ms. Equivalent to 10 m/s."));
    case SoundDownF0:
        return QString(tr("Set start frequency for sink."));
    case SoundDownF1000:
        return QString(tr("Set end frequency for sink. Equivalent to -10 m/s."));
    default:
        return QString();
    }
}
