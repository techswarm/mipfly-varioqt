/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "setnumsamplemenu.h"

#include "menu/menuutils.h"
#include "threads/gpsthread.h"
#include "settings/samplesmanager.h"

#include <Warning.h>

enum
{
    SetNumSmplUp,
    SetNumSmplDn,
    SetNumSmplOK,
    SetNumSmplBack
};

SetNumSampleMenu::SetNumSampleMenu(const GPSThread &serialThread, MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent), smplMgr(serialThread.connection())
{
    connect(&smplMgr, &SamplesManager::pause, &serialThread, &GPSThread::pauseRead);
    connect(&smplMgr, &SamplesManager::resume, &serialThread, &GPSThread::resumeRead);
}

int SetNumSampleMenu::rowCount(const QModelIndex &parent) const
{
    return 4;
}

const QString SetNumSampleMenu::captionForIndex(int index) const
{
    switch(index) {
    case SetNumSmplUp:
        return QString(tr("UP"));
    case SetNumSmplDn:
        return QString(tr("DOWN"));
    case SetNumSmplOK:
        return QString(tr("OK"));
    case SetNumSmplBack:
        return QString(tr("BACK"));
    default:
        return QString();
    }
}

MenuType SetNumSampleMenu::typeForIndex(int index) const
{
    if (index == SetNumSmplBack) {
        return Back;
    }
    return Choice;
}

void SetNumSampleMenu::before(const MenuDialog &menuDialog)
{
    smplMgr.reset();
    setupSlider(menuDialog, smplMgr.value(), SamplesManager::NUM_MIN, SamplesManager::NUM_MAX);
}

void SetNumSampleMenu::execAtIndex(int index, const MenuDialog &menuDialog)
{
    switch(index) {
    case SetNumSmplUp:
        this->smplMgr.incNumSamples();
        updateSlider(menuDialog, smplMgr.value());
        break;
    case SetNumSmplDn:
        this->smplMgr.decNumSamples();
        updateSlider(menuDialog, smplMgr.value());
        break;
    case SetNumSmplOK:
        this->smplMgr.saveNumSamples();
        Warning::showWarning(tr("INFO"),tr("Value was saved"));
        break;
    }
}
