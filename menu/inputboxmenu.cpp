/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "inputboxmenu.h"

#include "menudialog.h"
#include "ui_menudialog.h"

enum InputBoxIndex
{
    InputBoxInput,
    InputBoxClear,
    InputBoxBackspace,
    InputBoxBack
};

InputBoxMenu::InputBoxMenu(InputBoxType type, MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent), iBoxType(type)
{}

int InputBoxMenu::rowCount(const QModelIndex &parent) const
{
    return 4;
}

const QString InputBoxMenu::captionForIndex(int index) const
{
    switch(index) {
    case InputBoxInput:
        return QString("INPUT");
    case InputBoxClear:
        return QString("CLEAR");
    case InputBoxBackspace:
        return QString("BACKSPACE");
    case InputBoxBack:
        return QString("BACK");
    default:
        return QString();
    }
}

MenuType InputBoxMenu::typeForIndex(int index) const
{
    if (index == InputBoxBack) {
        return Back;
    }

    return Choice;
}

void InputBoxMenu::before(const MenuDialog &menuDialog)
{

}

void InputBoxMenu::execAtIndex(int index, const MenuDialog &menuDialog)
{
    switch(index) {
    case InputBoxInput:
        input(const_cast<MenuDialog&>(menuDialog));
        break;
    case InputBoxClear:
        clear(menuDialog);
        break;
    case InputBoxBackspace:
        backspace(menuDialog);
        break;
    }
}

void InputBoxMenu::clear(const MenuDialog &menuDialog)
{
    switch(iBoxType) {
    case IBoxLat:
        menuDialog.getUi().locWidget->clearLat();
        break;
    case IBoxLon:
        menuDialog.getUi().locWidget->clearLon();
        break;
    case IBoxAlt:
        menuDialog.getUi().locWidget->clearAlt();
        break;
    case IBoxName:
        menuDialog.getUi().locWidget->clear();
    }
}

void InputBoxMenu::backspace(const MenuDialog &menuDialog)
{
    switch(iBoxType) {
    case IBoxLat:
        menuDialog.getUi().locWidget->backspaceLat();
        break;
    case IBoxLon:
        menuDialog.getUi().locWidget->backspaceLon();
        break;
    case IBoxAlt:
        menuDialog.getUi().locWidget->backspaceAlt();
        break;
    case IBoxName:
        menuDialog.getUi().locWidget->backspace();
        break;
    }
}

void InputBoxMenu::input(MenuDialog &menuDialog)
{
    menuDialog.state = 2;

    QMetaObject::Connection conn;

    switch(iBoxType) {
    case IBoxLat:
        menuDialog.getUi().chooser->setNumbers();
        conn = QObject::connect(menuDialog.getUi().chooser, &LetterChooser::select,
                                menuDialog.getUi().locWidget, &InputLocation::acceptLat);
        break;
    case IBoxLon:
        menuDialog.getUi().chooser->setNumbers();
        conn = QObject::connect(menuDialog.getUi().chooser, &LetterChooser::select,
                                menuDialog.getUi().locWidget, &InputLocation::acceptLon);
        break;
    case IBoxAlt:
        menuDialog.getUi().chooser->setNumbers();
        conn = QObject::connect(menuDialog.getUi().chooser, &LetterChooser::select,
                                menuDialog.getUi().locWidget, &InputLocation::acceptAlt);
        break;
    case IBoxName:
        menuDialog.getUi().chooser->setLetters();
        conn = QObject::connect(menuDialog.getUi().chooser, &LetterChooser::select,
                                menuDialog.getUi().locWidget, &InputLocation::accept);
        break;
    }
    menuDialog.setChooserConn(conn);
    menuDialog.getUi().chooser->setVisible(true);
}
