/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "shutdownmenu.h"

#include <QSettings>

#include "menudialog.h"
#include "ui_menudialog.h"
#include <utils/PowerOffManager.h>
#include <Warning.h>

#ifdef __ARMEL__
#include <QtDebug>

#include <unistd.h>
#include <sys/reboot.h>
#include <linux/reboot.h>
#endif

enum ShutdownIndex
{
#ifndef __ARMEL__
    ExitOK,
#else
    ShutdownOK,
#endif
    ShutdownBack
};

ShutdownMenu::ShutdownMenu(const MenuDialog &menuDialog, MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent)
{

}

void ShutdownMenu::before(const MenuDialog &menuDialog)
{

}

int ShutdownMenu::rowCount(const QModelIndex &parent) const
{
    return 2;
}

const QString ShutdownMenu::descAtIndex(int index) const
{
    switch(index) {
    #ifndef __ARMEL__
    case ExitOK:
        return QString(tr("Exit program"));
    #else
    case ShutdownOK:
        return QString(tr("Shutdown vario"));
    #endif
    case ShutdownBack:
        return QString(tr("Cancel shutdown"));
    default:
        return QString();
    }
}

const QString ShutdownMenu::captionForIndex(int index) const
{
    switch(index) {
    #ifndef __ARMEL__
    case ExitOK:
        return QString(tr("OK"));
    #else
    case ShutdownOK:
        return QString(tr("OK"));
    #endif
    case ShutdownBack:
        return QString(tr("CANCEL"));
    default:
        return QString();
    }
}

MenuType ShutdownMenu::typeForIndex(int index) const
{
    if (index == ShutdownBack) {
        return Back;
    }
    return Choice;
}

void ShutdownMenu::execAtIndex(int index, const MenuDialog &menuDialog)
{
#ifndef __ARMEL__
    if(index == ExitOK){
        PowerOffManager *p = new PowerOffManager();
        p->exitProgram();
        Warning::showWarning(tr("INFO"), tr("Exiting"), 5);
    }
#else
    if (index == ShutdownOK) {
        PowerOffManager *p = new PowerOffManager();
        p->powerOff();
        //workarround for shutdown not happening bug.
        system("shutdown -P 2");
        Warning::showWarning(tr("INFO"), tr("Powering Off!"), 5);
    }
#endif
}
