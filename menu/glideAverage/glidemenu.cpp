/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "glidemenu.h"

#include "settings/glidemanager.h"
#include "menu/menuutils.h"

enum
{
    GlideUpIndex,
    GlideDnIndex,
    GlideBackIndex
};

GlideMenu::GlideMenu(GlideManager &mgr, MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent), glideMgr(mgr)
{

}

int GlideMenu::rowCount(const QModelIndex &parent) const
{
    return 3;
}

const QString GlideMenu::captionForIndex(int index) const
{
    switch(index) {
    case GlideUpIndex:
        return QString(tr("UP"));
    case GlideDnIndex:
        return QString(tr("DOWN"));
    case GlideBackIndex:
        return QString(tr("BACK"));
    default:
        return QString();
    }
}

void GlideMenu::before(const MenuDialog &menuDialog)
{
    setupSlider(menuDialog, glideMgr.getNumAverage(), GlideManager::GLIDE_MIN, GlideManager::GLIDE_MAX);
}

MenuType GlideMenu::typeForIndex(int index) const
{
    if (index == GlideBackIndex) {
        return Back;
    }
    return Choice;
}

void GlideMenu::execAtIndex(int index, const MenuDialog &menuDialog)
{
    if (index == GlideUpIndex) {
        this->glideMgr.incNumAverage();
        updateSlider(menuDialog, glideMgr.getNumAverage());
    } else {
        this->glideMgr.decNumAverage();
        updateSlider(menuDialog, glideMgr.getNumAverage());
    }
}
