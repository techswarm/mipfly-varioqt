/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "windaveragemenu.h"

#include "menu/menuutils.h"
#include "settings/windmanager.h"

enum
{
    WindAverageUp,
    WindaverageDown,
    WindAverageBack
};

WindAverageMenu::WindAverageMenu(WindManager &mgr, MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent), windMgr(mgr)
{

}

int WindAverageMenu::rowCount(const QModelIndex &parent) const
{
    return 3;
}

MenuType WindAverageMenu::typeForIndex(int index) const
{
    if (index == WindAverageBack) {
        return Back;
    }
    return Choice;
}

const QString WindAverageMenu::captionForIndex(int index) const
{
    switch(index) {
    case WindAverageUp:
        return QString(tr("UP"));
    case WindaverageDown:
        return QString(tr("DOWN"));
    case WindAverageBack:
        return QString(tr("BACK"));
    default:
        return QString();
    }
}

void WindAverageMenu::before(const MenuDialog &menuDialog)
{
    setupSlider(menuDialog, windMgr.getNumAverage(), WindManager::NUMAVG_MIN, WindManager::NUMAVG_MAX);
}

void WindAverageMenu::execAtIndex(int index, const MenuDialog &menuDialog)
{
    if (index == WindAverageUp) {
        this->windMgr.incNumAverage();
        updateSlider(menuDialog, windMgr.getNumAverage());

    } else {
        this->windMgr.decNumAverage();
        updateSlider(menuDialog, windMgr.getNumAverage());

    }
}
