/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "winddspreadmenu.h"

#include "settings/windmanager.h"
#include "menu/menuutils.h"

enum
{
    WindSpreadUp,
    WindSpreadDown,
    WindSpreadBack
};

WindDSpreadMenu::WindDSpreadMenu(WindManager &mgr, MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent), windMgr(mgr)
{

}

int WindDSpreadMenu::rowCount(const QModelIndex &parent) const
{
    return 3;
}

const QString WindDSpreadMenu::captionForIndex(int index) const
{
    switch(index) {
    case WindSpreadUp:
        return QString(tr("UP"));
    case WindSpreadDown:
        return QString(tr("DOWN"));
    case WindSpreadBack:
        return QString(tr("BACK"));
    default:
        return QString();
    }
}

MenuType WindDSpreadMenu::typeForIndex(int index) const
{
    if (index == WindSpreadBack) {
        return Back;
    }
    return Choice;
}

void WindDSpreadMenu::before(const MenuDialog &menuDialog)
{
    setupSlider(menuDialog, windMgr.getDataSpread(), WindManager::SPREAD_MIN, WindManager::SPREAD_MAX);
}

void WindDSpreadMenu::execAtIndex(int index, const MenuDialog &menuDialog)
{
    if (index == WindSpreadUp) {
        this->windMgr.incDataSpread();
        updateSlider(menuDialog, windMgr.getDataSpread());

    } else {
        this->windMgr.decDataSpread();
        updateSlider(menuDialog, windMgr.getDataSpread());
    }
}
