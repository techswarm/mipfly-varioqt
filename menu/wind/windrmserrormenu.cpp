/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "windrmserrormenu.h"

#include "menu/menuutils.h"
#include "settings/windmanager.h"

enum
{
    WindRmsErrorUp,
    WindRmsErrorDown,
    WindRmsErrorBack
};

WindRmsErrorMenu::WindRmsErrorMenu(WindManager &mgr, MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent), windMgr(mgr)
{

}

int WindRmsErrorMenu::rowCount(const QModelIndex &parent) const
{
    return 3;
}

MenuType WindRmsErrorMenu::typeForIndex(int index) const
{
    if (index == WindRmsErrorBack) {
        return Back;
    }
    return Choice;
}

const QString WindRmsErrorMenu::captionForIndex(int index) const
{
    switch(index) {
    case WindRmsErrorUp:
        return QString(tr("UP"));
    case WindRmsErrorDown:
        return QString(tr("DOWN"));
    case WindRmsErrorBack:
        return QString(tr("BACK"));
    default:
        return QString();
    }
}

void WindRmsErrorMenu::before(const MenuDialog &menuDialog)
{
    setupSlider(menuDialog, windMgr.getRmsError(), WindManager::RMSERROR_MIN, WindManager::RMSERROR_MAX);
}

void WindRmsErrorMenu::execAtIndex(int index, const MenuDialog &menuDialog)
{
    if (index == WindRmsErrorUp) {
        this->windMgr.incRmsError();
        updateSlider(menuDialog, windMgr.getRmsError());
    } else {
        this->windMgr.decRmsError();
        updateSlider(menuDialog, windMgr.getRmsError());
    }
}
