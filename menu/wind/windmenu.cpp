/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "windmenu.h"

#include "windreadingsmenu.h"
#include "windaveragemenu.h"
#include "winddspreadmenu.h"
#include "windrmserrormenu.h"
#include "menudialog.h"
#include "ui_menudialog.h"
#include "settings/windmanager.h"
#include "NumericInput.h"

enum
{
    WindNumReadingsCaption,
    WindNumReadings,
    WindNumAverageCaption,
    WindNumAverage,
    WindDataSpreadCaption,
    WindDataSpread,
    WindRmsErrorCaption,
    WindRmsError,
    WindBack
};

WindMenu::WindMenu(WindManager& mgr, MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent), windMgr(mgr)
{

}

int WindMenu::rowCount(const QModelIndex &parent) const
{
    return 9;
}

const QString WindMenu::captionForIndex(int index) const
{
    switch(index) {
    case WindNumReadingsCaption:
        return QString(tr("NUM READINGS"));
    case WindNumReadings:
    {
        return QString::number(windMgr.getNumReadings());
    }
    case WindNumAverageCaption:
        return QString(tr("NUM AVERAGE"));
    case WindNumAverage:
    {
        return QString::number(windMgr.getNumAverage());
    }
    case WindDataSpreadCaption:
        return QString(tr("DATA SPREAD"));
    case WindDataSpread:
        return QString::number(windMgr.getDataSpread());
    case WindRmsErrorCaption:
        return QString(tr("RMS ERROR"));
    case WindRmsError:
        return QString::number(windMgr.getRmsError(),'f',1);
    case WindBack:
        return QString(tr("BACK"));
    default:
        return QString("");
    }
}

MenuType WindMenu::typeForIndex(int index) const
{
    switch (index) {
    case WindNumReadingsCaption:
    case WindNumAverageCaption:
    case WindDataSpreadCaption:
    case WindRmsErrorCaption:
        return Caption;
        break;
    default:
        break;
    }

    if (index == WindBack) {
        return Back;
    }
    return Choice;
}

void WindMenu::before(const MenuDialog &menuDialog)
{
    menuDialog.getUi().valLabel->setVisible(false);
    menuDialog.getUi().hSlider->setVisible(false);
}


MenuListModel* WindMenu::modelForIndex(int index)
{
    switch(index) {
    }
    return nullptr;
}

void WindMenu::execAtIndex(int index, const MenuDialog &menuDialog)
{
    switch (index) {
    case WindNumReadings:
    {
        NumericInput *ni = new NumericInput;

        ni->setValue(windMgr.getNumReadings());
        ni->setStep(5);
        ni->setMax(1500);
        ni->setMin(10);
        ni->setPrecision(0);
        ni->setCaption(tr("Num Readings"));
        ni->move(240/2 - ni->width()/2,320/2 - ni->height()/2);
        ni->exec();
        windMgr.setNumReadings(ni->value());
        delete ni;
        break;
    }
    case WindNumAverage:
    {
        NumericInput *ni = new NumericInput;

        ni->setValue(windMgr.getNumAverage());
        ni->setStep(1);
        ni->setMax(30);
        ni->setMin(2);
        ni->setPrecision(0);
        ni->setCaption(tr("Num Avg."));
        ni->move(240/2 - ni->width()/2,320/2 - ni->height()/2);
        ni->exec();
        windMgr.setNumAverage(ni->value());
        delete ni;
        break;
    }
    case WindDataSpread:
    {
        NumericInput *ni = new NumericInput;

        ni->setValue(windMgr.getDataSpread());
        ni->setStep(1);
        ni->setMax(150);
        ni->setMin(10);
        ni->setPrecision(0);
        ni->setCaption(tr("Data spreed."));
        ni->move(240/2 - ni->width()/2,320/2 - ni->height()/2);
        ni->exec();
        windMgr.setDataSpread(ni->value());
        delete ni;
        break;
    }
    case WindRmsError:
    {
        NumericInput *ni = new NumericInput;

        ni->setValue(windMgr.getRmsError());
        ni->setStep(0.1);
        ni->setMax(windMgr.RMSERROR_MAX);
        ni->setMin(windMgr.RMSERROR_MIN);
        ni->setPrecision(1);
        ni->setCaption(tr("RMS Err."));
        ni->move(240/2 - ni->width()/2,320/2 - ni->height()/2);
        ni->exec();
        windMgr.setRmsError(ni->value());
        delete ni;
        break;
    }
    default:
        break;
    }
    emit dataChanged(QModelIndex(),QModelIndex());
}

const QString WindMenu::descAtIndex(int index) const
{
    switch(index) {
    case WindNumReadings:
        return QString(tr("Set number of readings to for wind algorithm."));
    case WindNumAverage:
        return QString(tr("Set average number of readings for wind algorithm."));
    case WindDataSpread:
        return QString(tr("Set data spread for wind algorithm."));
    case WindRmsError:
        return QString(tr("Set RMS error for wind algorithm."));
    default:
        return QString();
    }
}
