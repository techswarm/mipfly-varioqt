/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "windreadingsmenu.h"

#include "menu/menuutils.h"
#include "settings/windmanager.h"

enum
{
    WindReadingsUp,
    WindReadingsDown,
    WindReadingsBack
};

WindReadingsMenu::WindReadingsMenu(WindManager &mgr, MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent), windMgr(mgr)
{

}

int WindReadingsMenu::rowCount(const QModelIndex &parent) const
{
    return 3;
}

MenuType WindReadingsMenu::typeForIndex(int index) const
{
    if (index == WindReadingsBack) {
        return Back;
    }
    return Choice;
}

const QString WindReadingsMenu::captionForIndex(int index) const
{
    switch(index) {
    case WindReadingsUp:
        return QString(tr("UP"));
    case WindReadingsDown:
        return QString(tr("DOWN"));
    case WindReadingsBack:
        return QString(tr("BACK"));
    default:
        return QString();
    }
}

void WindReadingsMenu::before(const MenuDialog &menuDialog)
{
    setupSlider(menuDialog, windMgr.getNumReadings(), WindManager::NUMRD_MIN, WindManager::NUMRD_MAX);
}

void WindReadingsMenu::execAtIndex(int index, const MenuDialog &menuDialog)
{
    if (index == WindReadingsUp) {
        this->windMgr.incNumReadings();
        updateSlider(menuDialog, windMgr.getNumReadings());
    } else {
        this->windMgr.decNumReadings();
        updateSlider(menuDialog, windMgr.getNumReadings());
    }
}
