/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "systemMenu.h"

#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>

#include <QSettings>
#include "menudialog.h"
#include "ui_menudialog.h"
#include "Warning.h"
//#include "uiMenu.h"

#include "VarioQt.h"
//#include <QTimer>
#include <qdebug.h>

/*Is it using systemd
cat /proc/1/comm
*/

char *topCmd = (char*)"top -b -n 1";
char *sysdAnalyze = (char*)"systemd-analyze plot > ./systemdBoot.svg";

enum SystemMenuIndex
{
	SystemMenuCpuLoad,
	SystemMenuBootTime,
	SystemMenuBack
};

enum SystemMenuActions
{
    SystemMenuActionNothing,
	SystemMenuBackAction
};

SystemMenu::SystemMenu(const MenuDialog &menuDialog, MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent),menuDialog(menuDialog)
{

}

int SystemMenu::rowCount(const QModelIndex &parent) const
{
    return 3;
}

const QString SystemMenu::captionForIndex(int index) const
{
    switch(index) {
    case SystemMenuCpuLoad:
        return QString(tr("CPU LOAD"));
    case SystemMenuBootTime:
        return QString(tr("BOOT TIME"));
    case SystemMenuBack:
        return QString(tr("BACK"));
    default:
        return QString();
    }
}

void SystemMenu::before(const MenuDialog &menuDialog)
{
    menuDialog.getUi().valLabel->setVisible(false);
    menuDialog.getUi().hSlider->setVisible(false);
    menuDialog.getUi().chooser->setVisible(false);
    menuDialog.getUi().valName->setVisible(false);
    menuDialog.getUi().locWidget->setVisible(false);
}

MenuType SystemMenu::typeForIndex(int index) const
{
    if (index == SystemMenuBack) {
        return Back;
    }

    return Choice;
}


void SystemMenu::execAtIndex(int index, const MenuDialog &menuDialog)
{


    if (index == SystemMenuBootTime) {
        exec(sysdAnalyze);
        Warning::showWarning(tr("sysd boot"), tr("Created"), 1);
    }
/*
    if(index == GeneralMenuTty)
    {
        if(VarioQt::instance().getTtyMode() == true)
        {
            Warning::showWarning(tr("TTY error"), tr("Active"), 1);
        }
        else
        {
            Warning::showWarning(tr("TTY mode"), tr("ACTIVATED"), 5);
            system("modprobe g_serial");
            VarioQt::instance().setTtyMode(true);
        }
    }
*/
}

MenuListModel* SystemMenu::modelForIndex(int index)
{
    switch(index) {


    }
    return nullptr;
}

const QString SystemMenu::descAtIndex(int index) const
{
    switch(index) {
    case SystemMenuCpuLoad:
//        return QString(tr("View current cpuload."));
        return QString(exec(topCmd));
    case SystemMenuBootTime:
        return QString("Create sysd boot svg.");
    default:
        return QString();
    }
}

QString SystemMenu::exec(char* cmd) const {
    std::array<char, 128> buffer;
    std::string result;
    std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
    if (!pipe) {
        throw std::runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result += buffer.data();
    }

        return QString::fromStdString(result);
}


