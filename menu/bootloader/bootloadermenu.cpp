/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "bootloadermenu.h"
#include "menudialog.h"
#include "ui_menudialog.h"
#include <QtConcurrent/QtConcurrent>
#include "VarioQt.h"

enum
{
    BootMenuFlash,
    BootMenuFlashExperimental,
    BootMenuBack
};

BootloaderMenu::BootloaderMenu(GPSThread &thread, const MenuDialog &menuDialog, MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent), mgr(thread, menuDialog)
{
    watcher = new QFutureWatcher<int>();
    connect(watcher,SIGNAL(finished()),this,SLOT(futureFinished()));
    watcher->setFuture(future);
}

int BootloaderMenu::rowCount(const QModelIndex &parent) const
{
    return 3;
}

const QString BootloaderMenu::captionForIndex(int index) const
{
    if (index == BootMenuFlash) {
        return QString(tr("FLASH STANDARD"));
    }
    if (index == BootMenuFlashExperimental) {
        return QString(tr("FLASH INSTANT"));
    }
    return QString(tr("BACK"));
}

void BootloaderMenu::before(const MenuDialog &menuDialog)
{
    menuDialog.getUi().valLabel->setText("READY");
    menuDialog.getUi().valLabel->setVisible(true);
}

MenuType BootloaderMenu::typeForIndex(int index) const
{
    if (index == BootMenuFlash) {
        return Choice;
    }
    if (index == BootMenuFlashExperimental) {
        return Choice;
    }
    if(future.isRunning())return Choice;
    return Back;
}


void BootloaderMenu::execAtIndex(int index, const MenuDialog &menuDialog)
{
    if(future.isRunning())return;
    if(index == BootMenuFlash)
    {
        //VarioQt::instance().getVarioSerialThread()->pauseRead();
        future=QtConcurrent::run(&mgr,&BootloaderManager::flash,false);
        //mgr.flash(false);
    }
    if(index == BootMenuFlashExperimental)
    {
        //VarioQt::instance().getVarioSerialThread()->pauseRead();
        future=QtConcurrent::run(&mgr,&BootloaderManager::flash,true);
    }
}

const QString BootloaderMenu::descAtIndex(int index) const
{
    switch(index) {
    case BootMenuFlash:
        return QString(tr("Load the standard firmware based only on pressure data."));
    case BootMenuFlashExperimental:
        return QString(tr("Load the instant vario firmware based on pressure + accelerometer."));
    default:
        return QString();
    }
}

void BootloaderMenu::futureFinished()
{
    //VarioQt::instance().getVarioSerialThread()->resumeRead();
}

