/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "avionicsMenu.h"

#include <QSettings>

#include "menudialog.h"
#include "ui_menudialog.h"
#include "settings/namemanager.h"
#include "utils/vario_common.h"
#include "menu/name/inputnamemenu.h"
#include "menu/timezone/timezonemenu.h"
#include "menu/bootloader/bootloadermenu.h"

#include "menu/varioAverage/varioaveragemenu.h"
#include "menu/wind/windmenu.h"
#include "menu/numSamples/setnumsamplemenu.h"
#include "menu/glideAverage/glidemenu.h"

#include "VarioQt.h"
#include "utils/glider.h"
#include "NumericInput.h"

#include "menu/avionics/thermalingDetectorMenu.h"
#include "utils/unitmanager.h"
#include "navUtils/coord.hpp"

enum GeneralMenuIndex
{
    AvionicsMenuPitotCompensation,
    AvionicsMenuPitotAverage,
    AvionicsMenuPitotTEC,
    AvionicsMenuIGCName,
    AvionicsMenuAutoTurnOffAfterLand,
    AvionicsMenuSetTrimSpeed,
    AvionicsMenuDistMode,
    AvionicsMenuSetNumSamples,
    AvionicsMenuSetVarioAverage,
    AvionicsMenuSetWind,
    AvionicsMenuGlideNumAverage,
    AvionicsMenuTermalingDetect,
    AvionicsMenuRawLog,
    AvionicsMenuBack
};

AvionicsMenu::AvionicsMenu(const MenuDialog &menuDialog, MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent),menuDialog(menuDialog)
{}

int AvionicsMenu::rowCount(const QModelIndex &parent) const
{
    return 14;
}

const QString AvionicsMenu::captionForIndex(int index) const
{
    QSettings settings;
    switch(index) {
    case AvionicsMenuAutoTurnOffAfterLand:
    {
        QString ret = tr("TurnOff a. land - ");
        bool turnOffAfterLand = settings.value("turnOffAfterLand",true).toBool();
        if(turnOffAfterLand) ret += "YES";
        else ret += "NO";
        return ret;
    }
    case AvionicsMenuIGCName:
    {
        QString ret = tr("IGC Name - ");
        bool stdIGCName = settings.value(Vario::SETTINGS_IGCSTDNM, false).toBool();
        if(stdIGCName) ret += "COMP";
        else ret += "STD";
        return ret;
    }
    case AvionicsMenuDistMode:
    {
        QString ret = tr("Dist. mode - ");
        if(Coord::distanceMethod() == distanceMethods::WGS84)ret+="WGS84";
        else ret+="FAI Sphere";
        return ret;
    }
    case AvionicsMenuPitotCompensation:
    {
        QString ret = tr("Pitot comp. - ");
        int percent = VarioQt::instance().getVarioParser()->getPressureCoefficient();
        ret+=QString::number(percent,'f',0);
        ret+="%";
        return ret;
    }
    case AvionicsMenuPitotTEC:
    {
        QString ret = tr("Pitot TEC - ");
        int percent = VarioQt::instance().getVarioParser()->getTecCoefficient();
        ret+=QString::number(percent,'f',0);
        ret+="%";
        return ret;
    }
    case AvionicsMenuPitotAverage:
    {
        QString ret = tr("Pitot avg. - ");
        int percent = VarioQt::instance().getVarioParser()->getSpeedAverage();
        ret+=QString::number(percent,'f',0);
        return ret;
    }
    case AvionicsMenuSetTrimSpeed:
    {
        QString ret = tr("Trim speed - ");
        float speed = VarioQt::instance().getGlider()->getTrimSpeed();
        SISpeed siSpeed(speed);
        ret+=QString::number(siSpeed.getValuePrefferedFormat(),'f',0);
        ret+=" "+siSpeed.getPrefferedFormat();
        return ret;
    }
    case AvionicsMenuRawLog:
    {
        bool rawLogging = settings.value("rawLogging",false).toBool();
        if(!rawLogging)return QString("Raw Log OFF");
        else return QString("Raw Log ON");

    }
    case AvionicsMenuSetNumSamples:
        return QString(tr("SET NUM. SAMPLES"));
    case AvionicsMenuSetVarioAverage:
        return QString(tr("SET VARIO AVERAGE"));
    case AvionicsMenuSetWind:
        return QString(tr("WIND"));

    case AvionicsMenuGlideNumAverage:
        return QString(tr("GLIDE NUM. AVERAGE"));
    case AvionicsMenuTermalingDetect:
        return QString(tr("THERMALING DETECT"));
    case AvionicsMenuBack:
        return QString(tr("BACK"));
    default:
        return QString();
    }
}

void AvionicsMenu::before(const MenuDialog &menuDialog)
{
    menuDialog.getUi().valLabel->setVisible(false);
    menuDialog.getUi().hSlider->setVisible(false);
    menuDialog.getUi().chooser->setVisible(false);
    menuDialog.getUi().valName->setVisible(false);
    menuDialog.getUi().locWidget->setVisible(false);
}

MenuType AvionicsMenu::typeForIndex(int index) const
{
    switch (index) {
    case AvionicsMenuBack:
        return Back;
    case AvionicsMenuAutoTurnOffAfterLand:
    case AvionicsMenuSetTrimSpeed:
    case AvionicsMenuRawLog:
    case AvionicsMenuDistMode:
    case AvionicsMenuIGCName:
    case AvionicsMenuPitotCompensation:
    case AvionicsMenuPitotAverage:
    case AvionicsMenuPitotTEC:
        return Choice;
    default:
        break;
    }
    return List;
}


void AvionicsMenu::execAtIndex(int index, const MenuDialog &menuDialog)
{
    switch (index) {
    case AvionicsMenuIGCName:
    {
        QSettings settings;
        bool stdIGCName = settings.value(Vario::SETTINGS_IGCSTDNM, false).toBool();
        if(stdIGCName == true)
        {
            settings.setValue(Vario::SETTINGS_IGCSTDNM,false);
        }
        else
        {
            settings.setValue(Vario::SETTINGS_IGCSTDNM,true);
        }
        emit dataChanged(QModelIndex(),QModelIndex());
        break;
    }
    case AvionicsMenuRawLog:
    {
        QSettings settings;
        bool rawLogging = settings.value("rawLogging",false).toBool();
        if(rawLogging == true)
        {
            settings.setValue("rawLogging",false);
        }
        else
        {
            settings.setValue("rawLogging",true);
        }
        emit dataChanged(QModelIndex(),QModelIndex());
        break;
    }
    case AvionicsMenuAutoTurnOffAfterLand:
    {
        QSettings settings;
        bool turnOffAfterLand = settings.value("turnOffAfterLand",true).toBool();
        settings.setValue("turnOffAfterLand",!turnOffAfterLand);
        emit dataChanged(QModelIndex(),QModelIndex());
        break;
    }
    case AvionicsMenuDistMode:
    {
        if(Coord::distanceMethod() == distanceMethods::FAICircle)Coord::setDistanceMethod(distanceMethods::WGS84);
        else Coord::setDistanceMethod(distanceMethods::FAICircle);
        emit dataChanged(QModelIndex(),QModelIndex());
        break;
    }
    case AvionicsMenuSetTrimSpeed:
    {
        NumericInput *ni = new NumericInput;

        SISpeed siSpeed(VarioQt::instance().getGlider()->getTrimSpeed());


        ni->setValue(siSpeed.getValuePrefferedFormat());
        ni->setStep(1);
        ni->setMax(100);
        ni->setPrecision(0);
        ni->setCaption(tr("Trim Speed") +" "+ siSpeed.getPrefferedFormat());
        ni->move(240/2 - ni->width()/2,320/2 - ni->height()/2);
        ni->exec();
        siSpeed.setValuePrefferedFormat(ni->value());
        VarioQt::instance().getGlider()->setTrimSpeed(siSpeed.value());
        delete ni;
        emit dataChanged(QModelIndex(),QModelIndex());
        break;
    }
    case AvionicsMenuPitotCompensation:
    {
        NumericInput *ni = new NumericInput;

        ni->setValue(VarioQt::instance().getVarioParser()->getPressureCoefficient());
        ni->setStep(1);
        ni->setMax(200);
        ni->setPrecision(0);
        ni->setCaption(tr("Pressure coefficient"));
        ni->move(240/2 - ni->width()/2,320/2 - ni->height()/2);
        ni->exec();
        VarioQt::instance().getVarioParser()->setPressureCoefficient(ni->value());
        delete ni;
        emit dataChanged(QModelIndex(),QModelIndex());
        break;
    }
    case AvionicsMenuPitotTEC:
    {
        NumericInput *ni = new NumericInput;

        ni->setValue(VarioQt::instance().getVarioParser()->getTecCoefficient());
        ni->setStep(5);
        ni->setMax(100);
        ni->setMin(0);
        ni->setPrecision(0);
        ni->setCaption(tr("TEC coefficient"));
        ni->move(240/2 - ni->width()/2,320/2 - ni->height()/2);
        ni->exec();
        VarioQt::instance().getVarioParser()->setTecCoefficient(ni->value());
        delete ni;
        emit dataChanged(QModelIndex(),QModelIndex());
        break;
    }
    case AvionicsMenuPitotAverage:
    {
        NumericInput *ni = new NumericInput;

        ni->setValue(VarioQt::instance().getVarioParser()->getSpeedAverage());
        ni->setStep(1);
        ni->setMax(200);
        ni->setMin(1);
        ni->setPrecision(0);
        ni->setCaption(tr("Pitot average"));
        ni->move(240/2 - ni->width()/2,320/2 - ni->height()/2);
        ni->exec();
        VarioQt::instance().getVarioParser()->setSpeedAverage(ni->value());
        delete ni;
        emit dataChanged(QModelIndex(),QModelIndex());
        break;
    }
    default:
        break;
    }
}

MenuListModel* AvionicsMenu::modelForIndex(int index)
{
    switch(index) {
    case AvionicsMenuSetNumSamples:
        return new SetNumSampleMenu(*(VarioQt::instance().getVarioSerialThread()), this);
    case AvionicsMenuSetVarioAverage:
        return new VarioAverageMenu(*(VarioQt::instance().getVarioCompute()), this);
    case AvionicsMenuSetWind:
        return new WindMenu(*(VarioQt::instance().getWindManager()), this);
    case AvionicsMenuGlideNumAverage:
        return new GlideMenu(*(VarioQt::instance().getGlideManager()), this);
    case AvionicsMenuTermalingDetect:
        return new TermalingDetectorMenu(this);
    default:
        return nullptr;
    }
}

const QString AvionicsMenu::descAtIndex(int index) const
{
    switch(index) {
    case AvionicsMenuIGCName:
        return QString(tr("Set IGC naming mode. STD for name with date and location and COMP for competition naming with pilot name."));
    case AvionicsMenuDistMode:
        return QString(tr("Set distance computation method for task navigation."));
    case AvionicsMenuSetNumSamples:
        return QString(tr("Set average for fast vario. 1s = 201 samples. Not used for instant vario."));
    case AvionicsMenuSetVarioAverage:
        return QString(tr("Set integration time for average climb."));
    case AvionicsMenuSetWind:
        return QString(tr("Modify wind algorithm parameters."));
    case AvionicsMenuGlideNumAverage:
        return QString(tr("Set number of samples for glide algorithm."));
    case AvionicsMenuTermalingDetect:
        return QString(tr("Activate/deactivate and set parameters for thermaling detect."));
    case AvionicsMenuRawLog:
        return QString(tr("Activate/deactivate raw logging. WARNING! High space consumming."));
    default:
        return QString();
    }
}
