/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "thermalingDetectorMenu.h"
#include "menudialog.h"
#include "ui_menudialog.h"
#include "settings/windmanager.h"
#include "NumericInput.h"

enum
{
    TermalingDetectorOn,
    TermalingDetectorSaturationCaption,
    TermalingDetectorSaturation,
    TermalingDetectorDecayCaption,
    TermalingDetectorDecay,
    TermalingDetectorActivateTresholdCaption,
    TermalingDetectorActivateTreshold,
    TermalingDetectorDeactivateTresholdCaption,
    TermalingDetectorDeactivateTreshold,
    TermalingDetectorBack
};

TermalingDetectorMenu::TermalingDetectorMenu(MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent)
{

}

int TermalingDetectorMenu::rowCount(const QModelIndex &parent) const
{
    return 10;
}

const QString TermalingDetectorMenu::captionForIndex(int index) const
{
    switch(index) {
    case TermalingDetectorOn:
    {
        if(td->termalingActiveDetectEnable())
            return QString(tr("DETECT ON"));
        else
            return QString(tr("DETECT OFF"));
    }
    case TermalingDetectorSaturationCaption:
    {
        return QString(tr("SATURATION"));
    }
    case TermalingDetectorSaturation:
        return QString::number(td->turnIntegralSaturation());
    case TermalingDetectorDecayCaption:
    {
        return QString(tr("DECAY"));
    }
    case TermalingDetectorDecay:
        return QString::number(td->turnIntegralDecay(),'f',1);
    case TermalingDetectorActivateTresholdCaption:
    {
        return QString(tr("ACTIVATE THRESHOLD"));
    }
    case TermalingDetectorActivateTreshold:
        return QString::number(td->activeateTreshold());
    case TermalingDetectorDeactivateTresholdCaption:
    {
        return QString(tr("DEACTIVATE THRESHOLD"));
    }
    case TermalingDetectorDeactivateTreshold:
        return QString::number(td->deactivateTreshold());

    case TermalingDetectorBack:
        return QString(tr("BACK"));
    default:
        return QString();
    }
}

MenuType TermalingDetectorMenu::typeForIndex(int index) const
{
    switch (index) {
    case TermalingDetectorSaturationCaption:
    case TermalingDetectorDecayCaption:
    case TermalingDetectorActivateTresholdCaption:
    case TermalingDetectorDeactivateTresholdCaption:
        return Caption;
        break;
    default:
        break;
    }

    if (index == TermalingDetectorBack) {
        return Back;
    }
    return Choice;
}

void TermalingDetectorMenu::before(const MenuDialog &menuDialog)
{
    menuDialog.getUi().valLabel->setVisible(false);
    menuDialog.getUi().hSlider->setVisible(false);
}


MenuListModel* TermalingDetectorMenu::modelForIndex(int index)
{
    switch(index) {
    }
    return nullptr;
}

void TermalingDetectorMenu::execAtIndex(int index, const MenuDialog &menuDialog)
{
    switch (index) {
    case TermalingDetectorOn:
    {
        if(td->termalingActiveDetectEnable())
        {
            td->setTermalingActiveDetectEnable(false);
        }
        else
        {
            td->setTermalingActiveDetectEnable(true);
        }
        break;
    }
    case TermalingDetectorSaturation:
    {
        NumericInput *ni = new NumericInput;

        ni->setValue(td->turnIntegralSaturation());
        ni->setStep(10);
        ni->setMax(250);
        ni->setMin(0);
        ni->setPrecision(0);
        ni->setCaption(tr("Saturation"));
        ni->move(240/2 - ni->width()/2,320/2 - ni->height()/2);
        ni->exec();
        td->setTurnIntegralSaturation(ni->value());
        delete ni;
        break;
    }
    case TermalingDetectorDecay:
    {
        NumericInput *ni = new NumericInput;

        ni->setValue(td->turnIntegralDecay());
        ni->setStep(0.1);
        ni->setMax(30);
        ni->setMin(0.5);
        ni->setPrecision(1);
        ni->setCaption(tr("Decay"));
        ni->move(240/2 - ni->width()/2,320/2 - ni->height()/2);
        ni->exec();
        td->setTurnIntegralDecay(ni->value());
        delete ni;
        break;
    }
    case TermalingDetectorActivateTreshold:
    {
        NumericInput *ni = new NumericInput;

        ni->setValue(td->activeateTreshold());
        ni->setStep(1);
        ni->setMax(td->turnIntegralSaturation());
        ni->setMin(10);
        ni->setPrecision(0);
        ni->setCaption(tr("Activate th."));
        ni->move(240/2 - ni->width()/2,320/2 - ni->height()/2);
        ni->exec();
        td->setActiveateTreshold(ni->value());
        delete ni;
        break;
    }
    case TermalingDetectorDeactivateTreshold:
    {
        NumericInput *ni = new NumericInput;

        ni->setValue(td->deactivateTreshold());
        ni->setStep(1);
        ni->setMin(0);
        ni->setMax(td->activeateTreshold()-10);
        ni->setPrecision(0);
        ni->setCaption(tr("Deactivate th."));
        ni->move(240/2 - ni->width()/2,320/2 - ni->height()/2);
        ni->exec();
        td->setDeactivateTreshold(ni->value());
        delete ni;
        break;
    }
    default:
        break;
    }
    emit dataChanged(QModelIndex(),QModelIndex());
}

const QString TermalingDetectorMenu::descAtIndex(int index) const
{
    switch(index) {
    case TermalingDetectorOn:
        return QString(tr("Activate/Deactivate thermaling detector."));
    case TermalingDetectorSaturation:
        return QString(tr("Saturation value. Used to limmit maximum turn integral to facilitate deactivation. Recommended 150."));
    case TermalingDetectorDecay:
        return QString(tr("Decay value. Each unit is equivalent to 5 deg/sec decay. Recommended 1."));
    case TermalingDetectorActivateTreshold:
        return QString(tr("Threshold that will trigger an activate event. Recommended 80."));
    case TermalingDetectorDeactivateTreshold:
        return QString(tr("Threshold that will trigger an deactivate event. Recommended 60."));
    default:
        return QString();
    }
}
