/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "usblistmenu.h"

#include <sys/mount.h>
#include <dirent.h>
#include <unistd.h>
#include <stdio.h>

#include <QFile>
#include <QtDebug>

#include <vector>

#include "utils/utils.h"
#include "bootloader/bootloadermanager.h"
#include "settings/namemanager.h"
#include "utils/vario_common.h"
#include "ui/usbdialog.h"
#include "ui_usbdialog.h"

std::vector<std::string> fsTypes {
    "auto","vfat", "ext4", "ext3", "ext2", "msdos"
};

bool mountDev(const QString &dev)
{
    system("mkdir /mnt/USB");
    for (auto fs : fsTypes) {
        QByteArray ba = dev.toLocal8Bit();
        if (mount(ba.data(), "/mnt/USB", fs.c_str(), 0, nullptr) == 0) {
            qDebug() << "Mounted " << dev << " on /mnt with filesystem type " << QString::fromStdString(fs);
            return true;
        }
    }

    return false;
}

// ----

USBListMenu::USBListMenu(const NameManager &nameManager, QObject *parent)
    : QAbstractListModel(parent), mgr(locSvc, nameManager), nameMgr(nameManager)
{}

int USBListMenu::rowCount(const QModelIndex &parent) const
{
    return devList.count() + 1;
}

QVariant USBListMenu::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role == Qt::TextAlignmentRole) {
        return int(Qt::AlignCenter | Qt::AlignHCenter);
    } else if (role == Qt::DisplayRole) {
        if (index.row() == 0) {
            return QString("-- BACK --");
        }
        return devList.at(index.row() - 1);
    }

    return QVariant();
}

static inline const QString& getPilotName(const NameManager &nameManager)
{
    return nameManager.getName().isEmpty() ? Vario::DEFAULT_PILOT : nameManager.getName();
}

void USBListMenu::copyGlidesToIndex(int i, const USBDialog &usbDialog)
{
    if(i<1)
    {
        usbDialog.getUi().statusBox->setValue(tr("ERROR"));
        return;
    }
    QString dev = devList.at(i - 1);
    QString dest("/mnt/USB/flights");

    auto copyFileFunction = [&dest](const QString &fullPath, const QString &name) {
        if (!QFile::copy(fullPath, dest + name)) {
            qWarning() << "Error copying file: " << fullPath << " -> " << (dest + name);
        } else {
            qDebug() << "Copied " << fullPath << " -> " << dest + name;
        }
    };

    qDebug() << "copy glides to " << dev;

    usbDialog.getUi().statusBox->setValue("MOUNT FS TO /mnt/USB");

    if (!mountDev(dev)) {
        perror("mountUSB");
        usbDialog.getUi().statusBox->setValue("ERROR MOUNTING /mnt/USB");
    } else {

        usbDialog.getUi().statusBox->setValue("MAKE DIRS ON USB");

        // Create 'flights' directory on target
        if (mkdirIfNotExists(dest.toStdString()) != 0) {
            qWarning() << "Cannot create directory 'flights'";
        }

        // Create <pilot> directory on target
        dest.append("/");
        dest.append(getPilotName(nameMgr));

        if (mkdirIfNotExists(dest.toStdString()) != 0) {
            qWarning() << "Cannot create directory "<< getPilotName(nameMgr);
        }

        dest.append("/");

        usbDialog.getUi().statusBox->setValue(tr("COPYING GLIDES..."));

        mgr.iterateGlides(copyFileFunction);

        sync();

        if (umount2("/mnt/USB", 0) < 0) {
            qWarning() << "Error unmounting /mnt/USB";
            usbDialog.getUi().statusBox->setValue(tr("ERROR UNMOUNT /mnt/USB"));
        }

        usbDialog.getUi().statusBox->setValue(tr("DONE"));
    }
}

void USBListMenu::copyDumpToUSB(int i, const USBDialog &usbDialog)
{
    if(i<1)
    {
        usbDialog.getUi().statusBox->setValue(tr("ERROR"));
        return;
    }
    QString dev = devList.at(i - 1);

    usbDialog.getUi().statusBox->setValue("MOUNT FS TO /mnt/USB");

    if (!mountDev(dev)) {
        perror("mountUSB");
        usbDialog.getUi().statusBox->setValue("ERROR MOUNTING /mnt/USB");
    } else {

        usbDialog.getUi().statusBox->setValue("COPYING");

        system("mv /root/core /mnt/USB/");

        sync();

        if (umount2("/mnt/USB", 0) < 0) {
            qWarning() << "Error unmounting /mnt/USB";
            usbDialog.getUi().statusBox->setValue(tr("ERROR UNMOUNT /mnt/USB"));
        }

        usbDialog.getUi().statusBox->setValue(tr("DONE"));
    }
}

void USBListMenu::copyBootloaderFromIndex(int i, const USBDialog &usbDialog)
{
    if(i<1)
    {
        usbDialog.getUi().statusBox->setValue(tr("ERROR"));
        return;
    }
    QString dev = devList.at(i - 1);

    usbDialog.getUi().statusBox->setValue(tr("MOUNT FS TO /mnt/USB"));

    if (!mountDev(dev)) {
        qWarning() << "Error mounting USB device " << dev << " at /mnt/USB";
        usbDialog.getUi().statusBox->setValue(tr("ERROR MOUNTING /mnt/USB"));

    } else {

        QString srcPath = QString("/mnt/USB/").append(BootloaderManager::BOOT_FILENAME);
        QString dest = QString("./").append(BootloaderManager::BOOT_FILENAME);

        // if dest exists, remove it first, otherwise QFile::copy() fails
        QByteArray ba = dest.toLocal8Bit();
        if (access(ba.data(), F_OK) != -1) {
            if (remove(ba.data()) < 0) {
                qWarning() << "Cannot delete existing file: " << dest;
            }
        }

        usbDialog.getUi().statusBox->setValue("COPYING FIRMWARE");

        if (!QFile::copy(srcPath, dest)) {
            qWarning() << "Error copying file: " << srcPath << " -> " << dest;
        }

        if (umount2("/mnt/USB", 0) < 0) {
            qWarning() << "Error unmounting /mnt/USB";
        }

        usbDialog.getUi().statusBox->setValue("DONE");
    }
}

void USBListMenu::addDevice(const QString &device)
{
    qDebug() << "Device added: " << device;
    devList.append(device);
    emit dataChanged(QModelIndex(), QModelIndex());
}

void USBListMenu::removeDevice(const QString &device)
{
    if (devList.removeOne(device)) {
        emit dataChanged(QModelIndex(), QModelIndex());
        qDebug() << "Device removed: " << device;
    }
}

int USBListMenu::deviceCount() const
{
    return devList.count();
}
