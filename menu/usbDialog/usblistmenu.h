/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef USBLISTMENU_H
#define USBLISTMENU_H

#include <QAbstractListModel>

#include <Geo/locationsservice.h>
#include <stats/glidesmanager.h>

class USBDialog;

class USBListMenu : public QAbstractListModel
{
    Q_OBJECT
public:
    USBListMenu(const NameManager &nameManager, QObject *parent = Q_NULLPTR);

    int rowCount(const QModelIndex &parent) const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;

    void copyGlidesToIndex(int i, const USBDialog &usbDialog);
    void copyBootloaderFromIndex(int i,  const USBDialog &usbDialog);

    void addDevice(const QString &device);
    void removeDevice(const QString &device);
    int deviceCount() const;

    void copyDumpToUSB(int i, const USBDialog &usbDialog);
private:
    QStringList devList;

    LocationsService locSvc;
    GlidesManager mgr;
    const NameManager &nameMgr;
};

#endif // USBLISTMENU_H
