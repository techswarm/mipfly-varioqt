/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "usbdialogmenu.h"


USBDialogMenu::USBDialogMenu(QObject *parent)
    : QAbstractListModel(parent)
{
}

int USBDialogMenu::rowCount(const QModelIndex &parent) const
{
        return 4;
}

QVariant USBDialogMenu::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role == Qt::TextAlignmentRole) {
        return int(Qt::AlignCenter | Qt::AlignHCenter);
    } else if (role == Qt::DisplayRole) {
        switch(index.row()) {
        case USBMenuGlides:
            return QString(tr("GLIDES TO USB"));
        case USBMenuBoot:
            return QString(tr("COPY BOOTLOADER"));
        case USBMenuDump:
            return QString(tr("DUMP TO USB"));
        case USBMenuHide:
            return QString(tr("HIDE"));
        }
    }

    return QVariant();
}
