/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "inputnamemenu.h"

#include <QSettings>

#include "menudialog.h"
#include "ui_menudialog.h"
#include "settings/namemanager.h"
#include "utils/vario_common.h"

#include "Warning.h"

enum InputNameIndex
{
    InputNameInput,
    InputNameClear,
    InputNameOK,
    InputNameBackspace,
    InputNameBack
};

InputNameMenu::InputNameMenu(NameManager &nameMgr, MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent), mgr(nameMgr)
{}

int InputNameMenu::rowCount(const QModelIndex &parent) const
{
    return 5;
}

const QString InputNameMenu::captionForIndex(int index) const
{
    switch(index) {
    case InputNameInput:
        return QString(tr("INPUT"));
    case InputNameClear:
        return QString(tr("CLEAR"));
    case InputNameOK:
        return QString(tr("OK"));
    case InputNameBackspace:
        return QString(tr("BACKSPACE"));
    case InputNameBack:
        return QString(tr("BACK"));
    default:
        return QString();
    }
}

void InputNameMenu::before(const MenuDialog &menuDialog)
{
    mgr.loadFromSettings();
    menuDialog.getUi().valName->setText(mgr.getName());
    menuDialog.getUi().valName->setVisible(true);
}

MenuType InputNameMenu::typeForIndex(int index) const
{
    if (index == InputNameBack) {
        return Back;
    }
    return Choice;
}


void InputNameMenu::execAtIndex(int index, const MenuDialog &menuDialog)
{
    MenuDialog &dlg = const_cast<MenuDialog&>(menuDialog);
    QMetaObject::Connection conn;

    switch(index) {
    case InputNameInput:
        conn = connect(menuDialog.getUi().chooser, SIGNAL(select(QChar)), &mgr, SLOT(accept(QChar)));
        dlg.setChooserConn(conn);

        menuDialog.getUi().chooser->setLetters();
        menuDialog.getUi().chooser->setVisible(true);
        dlg.state = 2;
        break;
    case InputNameClear:
        mgr.clear();
        break;
    case InputNameOK:
        mgr.save();
        Warning::showWarning(tr("INFO"),tr("Pilot name was saved"), 1);
        break;
    case InputNameBackspace:
        mgr.backspace();
        break;
    }
}
