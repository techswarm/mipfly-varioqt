/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef INPUTNAMEMENU_H
#define INPUTNAMEMENU_H

#include "menu/menulistmodel.h"

class NameManager;

class InputNameMenu : public MenuListModel
{
    Q_OBJECT
public:
    InputNameMenu(NameManager &nameMgr, MenuListModel *parentMenu = nullptr, QObject *parent = Q_NULLPTR);

    int rowCount(const QModelIndex &parent) const Q_DECL_OVERRIDE;
    const QString captionForIndex(int index) const Q_DECL_OVERRIDE;
    void before(const MenuDialog &menuDialog) Q_DECL_OVERRIDE;
    MenuType typeForIndex(int index) const Q_DECL_OVERRIDE;

    void execAtIndex(int index, const MenuDialog &menuDialog) Q_DECL_OVERRIDE;

    private:
        NameManager& mgr;
};

#endif // INPUTNAMEMENU_H
