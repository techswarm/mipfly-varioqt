/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "recoverymenu.h"

#include <QSettings>

#include "menudialog.h"
#include "ui_menudialog.h"
#include <utils/PowerOffManager.h>
#include <Warning.h>
#include "ChooseInput.h"

#ifdef __ARMEL__
#include <QtDebug>

#include <unistd.h>
#include <sys/reboot.h>
#include <linux/reboot.h>
#endif

enum RecoveryMenuIndex
{
    RecoveryMenuWriteVarioQt,
    RecoveryMenuDeleteTask,
    RecoveryMenuFactoryReset,
    RecoveryMenuBack
};

RecoveryMenu::RecoveryMenu(const MenuDialog &menuDialog, MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent)
{

}

void RecoveryMenu::before(const MenuDialog &menuDialog)
{

}

int RecoveryMenu::rowCount(const QModelIndex &parent) const
{
    return 4;
}

const QString RecoveryMenu::descAtIndex(int index) const
{
    switch(index) {
    case RecoveryMenuWriteVarioQt:
        return QString(tr("Restore VarioQt and other important files to the fat32 partition."));
    case RecoveryMenuDeleteTask:
        return QString(tr("Delete task from the fat32 partition."));
    case RecoveryMenuFactoryReset:
        return QString(tr("Restore a factory defaults image to the RootFs partition and restore VarioQt + important files to fat32. System will look freezed. Do not poweroff untill done."));
    case RecoveryMenuBack:
        return QString(tr("Back"));
    default:
        return QString();
    }
}

const QString RecoveryMenu::captionForIndex(int index) const
{
    switch(index) {
    case RecoveryMenuWriteVarioQt:
        return QString(tr("Restore VarioQt"));
    case RecoveryMenuDeleteTask:
        return QString(tr("Delete Task"));
    case RecoveryMenuFactoryReset:
        return QString(tr("Factory resset"));
    case RecoveryMenuBack:
        return QString(tr("Back"));
    default:
        return QString();
    }
}

MenuType RecoveryMenu::typeForIndex(int index) const
{
    if (index == RecoveryMenuBack) {
        return Back;
    }
    return Choice;
}

void RecoveryMenu::execAtIndex(int index, const MenuDialog &menuDialog)
{
#ifndef __ARMEL__
    //protect from damage to PC simulation env
    return;
#endif
    switch(index)
    {
        case RecoveryMenuWriteVarioQt:
            system("cp /mnt/mipfly/BTConnect.sh /mnt/mipflyMain/");
            system("cp /mnt/mipfly/MipFlyLogger /mnt/mipflyMain/");
            system("cp /mnt/mipfly/VarioQt /mnt/mipflyMain/");
            system("sync");
            Warning::showWarningSound("Success!","Finished",10);
        break;
        case RecoveryMenuDeleteTask:
            system("rm /mnt/mipflyMain/defaultTask.mtk");
            system("sync");
            Warning::showWarningSound("Success!","Finished",10);
        break;
        case RecoveryMenuFactoryReset:
    {
        ChooseInput *ci = new ChooseInput;
        ci->setCaption(tr("Long opp.! Do not pwr. off"));
        ci->setValue(tr("PROCEED?"));
        ci->setChouse1(tr("CANCEL"));
        ci->setChouse3(tr("CONTINUE"));

        ci->exec();

        if(ci->choice() == 3){
            system("dd if=/home/mipfly/recovery.img of=/dev/mmcblk0p2 bs=1M");
            system("cp /mnt/mipfly/BTConnect.sh /mnt/mipflyMain/");
            system("cp /mnt/mipfly/MipFlyLogger /mnt/mipflyMain/");
            system("cp /mnt/mipfly/VarioQt /mnt/mipflyMain/");
            system("sync");
            Warning::showWarningSound("Success!","Finished",10);
        }
    }
        break;
    }
}
