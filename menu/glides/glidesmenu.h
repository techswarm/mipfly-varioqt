
#ifndef GLIDESMENU_H
#define GLIDESMENU_H

#include "menu/menulistmodel.h"
#include "stats/glidesmanager.h"
#include "Geo/locationsservice.h"
#include "utils/igcuploader.h"
#include "IGC/igcparser.h"
#include <QString>

class GlidesMenu : public MenuListModel
{
    Q_OBJECT
public:
    GlidesMenu(const NameManager& nameMgr, MenuListModel *parentMenu = nullptr, QObject *parent = Q_NULLPTR);

    int rowCount(const QModelIndex &parent) const Q_DECL_OVERRIDE;
    MenuType typeForIndex(int index) const Q_DECL_OVERRIDE;
    void before(const MenuDialog &menuDialog) Q_DECL_OVERRIDE;
    const QString captionForIndex(int index) const Q_DECL_OVERRIDE;
    void execAtIndexNC(int index, MenuDialog &menuDialog)Q_DECL_OVERRIDE;
    const QString descAtIndex(int index) const Q_DECL_OVERRIDE;
    const QString flightSummary(QString filePath) const;

private:
    LocationsService locSvc;
    GlidesManager mgr;
    IGCUploader igcUploader;

    const QString flightSummaryjson(QString jsonfile) const;
    bool flightSummaryigc(QString filePath, QString jsonfile) const;
    QString appendSummary(QString name, QString value, QString unit);
private slots:
    void messageReceived(QString msg, QString capt);
};

#endif // GLIDESMENU_H
