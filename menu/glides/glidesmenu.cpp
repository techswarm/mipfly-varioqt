/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "glidesmenu.h"
#include "globals.h"
#include "utils/vario_common.h"
#include "ChooseInput.h"
#include "utils/fileUtils.h"
#include "menudialog.h"
#include "Warning.h"
#include "flightlog/flightsummary.h"
#include "flightlog/flightsummarylog.h"

GlidesMenu::GlidesMenu(const NameManager &nameMgr, MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent), mgr(locSvc, nameMgr)
{
    QSettings settings;
    QString key = settings.value("accountKey","").toString();
    QString id = settings.value("accountId","").toString();
    igcUploader.setUserId(id);
    igcUploader.setUserKey(key);

    connect(&igcUploader,SIGNAL(message(QString,QString)),this,SLOT(messageReceived(QString,QString)));
}

int GlidesMenu::rowCount(const QModelIndex &parent) const
{
    return 1 + mgr.getGlides().count();
}

MenuType GlidesMenu::typeForIndex(int index) const
{
    if (index == 0) {
        return Back;
    }
    return Choice;
}

void GlidesMenu::before(const MenuDialog &menuDialog)
{

}

void GlidesMenu::execAtIndexNC(int index, MenuDialog &menuDialog)
{
    ChooseInput *ci = new ChooseInput;
    ci->setCaption(tr("FLIGHT"));
    ci->setValue(tr("ACTION?"));
    ci->setChouse1(tr("UPLOAD"));
    ci->setChouse2(tr("DELETE"));
    ci->setChouse3(tr("SHOW"));

    ci->exec();

    int selected = ci->choice();
    delete ci;

    if(selected == 3)
    {
        if(glideDetailsUi == nullptr)
        {
            glideDetailsUi = new glideDetails();
            glideDetailsUi->setIgcPath(mgr.glidesList[index - 1].filePath);

            glideDetailsUi->move(0,0);
            glideDetailsUi->setFocus();
            glideDetailsUi->show();
            glideDetailsUi->setFocus();
        }
        else
        {
            delete glideDetailsUi;
            glideDetailsUi = new glideDetails();
            glideDetailsUi->setIgcPath(mgr.glidesList[index - 1].filePath);

            glideDetailsUi->move(0,0);
            glideDetailsUi->setFocus();
            glideDetailsUi->show();
            glideDetailsUi->setFocus();
        }
    }

    if(selected == 2)
    {

        ChooseInput *ci = new ChooseInput;
        ci->setCaption(tr("DELETE"));
        ci->setValue(tr("CONFIRM?"));
        ci->setChouse1(tr("CANCEL"));
        ci->setChouse3(tr("OK"));

        ci->exec();

        int selectedConfirm = ci->choice();
        delete ci;

        if(selectedConfirm == 3)
        {
            FileUtils::remove(mgr.glidesList[index - 1].filePath);
            mgr.glidesList.removeAt(index-1);
            if(mgr.glidesList.count()==index - 1)menuDialog.goBack();
            dataChanged(QModelIndex(), QModelIndex());
        }
        else
        {

        }
    }

    if(selected == 1)
    {
        //begin upload procedure
        igcUploader.setFilename(mgr.glidesList[index - 1].filePath);
        igcUploader.upload();

        dataChanged(QModelIndex(), QModelIndex());
    }
}

void GlidesMenu::messageReceived(QString msg, QString capt)
{
    Warning::showWarning(capt,msg,3);
}

const QString GlidesMenu::captionForIndex(int index) const
{
    if (index == 0) {
        return QString(tr("-- BACK --"));
    }
    const Vario::GlideSession &glide = mgr.getGlides().at(index - 1);
    return glide.fileName;
}

const QString GlidesMenu::descAtIndex(int index) const
{
    switch(index) {
    }

    if(index>0) {
        const Vario::GlideSession &glide = mgr.getGlides().at(index - 1);


        return flightSummary(glide.filePath);

    }

        return QString();

}
const QString GlidesMenu::flightSummary(QString filePath) const{

    QString jsonfile =  filePath.left(filePath.length() - 3);;
    jsonfile.append("json");


    if (QFileInfo::exists(jsonfile)) {
        return flightSummaryjson(jsonfile);
    } else {
        if (flightSummaryigc(filePath, jsonfile)) {
            return flightSummaryjson(jsonfile);
        } else {
            return "N/A \r\n";
        }
    }

}

const QString GlidesMenu::flightSummaryjson(QString jsonfile) const{
    QString ret;

    FlightSummaryLog flightsummarylog;


    FlightSummary flightsummary = flightsummarylog.getFlightSummaryLog(jsonfile);

    if (flightsummary.getStartTime().length() > 10) {
      QDateTime  starttime = QDateTime().fromString(flightsummary.getStartTime());
     // ret = "Date: " + starttime.date().toString("dd-MM-yy") + "\r\n";
      ret = "Start Time: " + starttime.time().toString() + "\r\n";
    } else {
        ret = "Start Time: " + flightsummary.getStartTime() + "\r\n";
    }

    QTime d = flightsummary.getFlightDuraton();
    ret += "Duration: " + d.toString("hh:mm:ss") + "\r\n";
    QString text;
    ret += "Distance: " + text.sprintf("%.02f",(float)flightsummary.getDistance()/1000)  + "Km\r\n";
    ret += "Start Alt: " + QString::number(flightsummary.getStartAltitude())+ "m\r\n";
    ret += "Max Alt: " + QString::number(flightsummary.getMaxAltitude()) + "m\r\n";
    ret += "Max Alt Gain: " + QString::number(flightsummary.getMaxAltitudeGain()) + "m\r\n";
    ret += "Max Climb: " + QString::number(flightsummary.getMaxClimb()/100) + "ms\r\n";
    ret += "Max Sink: " + QString::number(flightsummary.getMaxSink()/100) + "ms\r\n";

    return ret;
}

bool GlidesMenu::flightSummaryigc(QString filePath, QString jsonfile) const{
    QTime t(0,0,0,0);
    igcParser igcP(filePath);
    igcP.parse();

        FlightSummary flightsummary;
        flightsummary.filename = jsonfile.toStdString();


        if (igcP.igcLogEntrys.isEmpty()) {
            return false;
        } else {
            int startTimeSecOfDay = (igcP.igcLogEntrys.first().snapTime).hour() * 3600 +
                    (igcP.igcLogEntrys.first().snapTime).minute() * 60 +
                    (igcP.igcLogEntrys.first().snapTime).second();
            int endTimeSecOfDay = (igcP.igcLogEntrys.last().snapTime).hour() * 3600 +
                    (igcP.igcLogEntrys.last().snapTime).minute() * 60 +
                    (igcP.igcLogEntrys.last().snapTime).second();


            t = t.addSecs(startTimeSecOfDay);

            flightsummary.setStartTime(t.toString("hh:mm:ss"));
            t.setHMS(0,0,0);
            t = t.addSecs(endTimeSecOfDay);

            flightsummary.setLandingTime(t.toString("hh:mm:ss"));
            flightsummary.setStartAltitude(igcP.igcLogEntrys.first().altitude);

            double flightDistanceKm = (igcP.igcLogEntrys.first().coordonates).distanceKm(igcP.igcLogEntrys.last().coordonates);
            flightsummary.setDistance(flightDistanceKm * 1000);
            int maxAltitude = 0;
            int maxClimb = 0;
            int maxSink = 0;

            for(int i=0; i<igcP.igcLogEntrys.size(); i++) {
                if (igcP.igcLogEntrys[i].altitude > maxAltitude) {
                    maxAltitude = igcP.igcLogEntrys[i].altitude;
                }

                if (i>10 && i < igcP.igcLogEntrys.size() - 10 ) { //filter out start and landing

                    int varioms = igcP.igcLogEntrys[i-1].altitude - igcP.igcLogEntrys[i].altitude; //Not yet valid

                    if (varioms > maxClimb) {
                        maxClimb = varioms;
                    }

                    if (varioms < maxSink) {
                        maxSink = varioms;
                    }
                }

            }

            flightsummary.setMaxAltitude((QString::number(maxAltitude)).toInt());
            flightsummary.setMaxAltitudeGain((QString::number(maxAltitude - igcP.igcLogEntrys.first().altitude)).toInt());

            FlightSummaryLog flightsummarylog;
            flightsummarylog.writeSummaryLogFile(&flightsummary);

        }
       return true;
}
