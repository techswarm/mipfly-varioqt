/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "BTDevice.h"
#include "BTDeviceAction.h"
#include <qbluetoothaddress.h>
#include <qbluetoothdevicediscoveryagent.h>
#include <qbluetoothlocaldevice.h>
#include <QDebug>
#include <QSettings>
#include "Warning.h"

enum
{
    BTDeviceMenuVisible,
    BTDeviceMenuScanning,
    BTDeviceMenuRemoveRemote,
    BTDeviceMenuBack
};

BTDeviceMenu::BTDeviceMenu(MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent), localDevice(new QBluetoothLocalDevice)
{
    discoveryAgent = new QBluetoothDeviceDiscoveryAgent();

    connect(localDevice, SIGNAL(hostModeStateChanged(QBluetoothLocalDevice::HostMode)),
            this, SLOT(hostModeStateChanged(QBluetoothLocalDevice::HostMode)));

    connect(discoveryAgent, SIGNAL(deviceDiscovered(QBluetoothDeviceInfo)),
               this, SLOT(addDevice(QBluetoothDeviceInfo)));

    connect(discoveryAgent, SIGNAL(finished()), this, SLOT(scanFinished()));

    discoveryAgent->start();
    scanning = true;
}

BTDeviceMenu::~BTDeviceMenu()
{
    delete discoveryAgent;
}

int BTDeviceMenu::rowCount(const QModelIndex &parent) const
{
    return 4 + devices.count();
}

const QString BTDeviceMenu::captionForIndex(int index) const
{
    switch(index) {

    case BTDeviceMenuVisible:
    {
        if(localDevice->hostMode() == QBluetoothLocalDevice::HostDiscoverable)
        {
            return QString(tr("VISIBLE ON"));
        }
        else
        {
            return QString(tr("VISIBLE OFF"));
        }
    }
    case BTDeviceMenuScanning:
    {
        if(scanning == true)
        {
            return QString(tr("SCAN ON"));
        }
        else
        {
            return QString(tr("SCAN OFF"));
        }
    }
    case BTDeviceMenuRemoveRemote:
        return QString(tr("REMOVE REMOTE"));
    case BTDeviceMenuBack:
        return QString(tr("BACK"));
    }

    //if still not returned we have devices
    if(index>BTDeviceMenuBack)
    {
        if(index-BTDeviceMenuBack-1<devices.count())return deviceNames[(index-BTDeviceMenuBack)-1];
        else return QString(tr("NULL"));
    }
    return QString();
}

void BTDeviceMenu::before(const MenuDialog &menuDialog)
{
}

MenuType BTDeviceMenu::typeForIndex(int index) const
{
    switch (index) {
    case BTDeviceMenuBack:
        return Back;
    case BTDeviceMenuVisible:
    case BTDeviceMenuScanning:
    case BTDeviceMenuRemoveRemote:
        return Choice;
    default:
        return List;
    }
}

void BTDeviceMenu::execAtIndex(int index, const MenuDialog &menuDialog)
{
    switch (index) {
    case BTDeviceMenuVisible:
    {
        if(localDevice->hostMode() == QBluetoothLocalDevice::HostDiscoverable)
        {
            localDevice->setHostMode(QBluetoothLocalDevice::HostConnectable);
        }
        else
        {
            localDevice->setHostMode(QBluetoothLocalDevice::HostDiscoverable);
        }
        emit dataChanged(QModelIndex(), QModelIndex());
        break;
    }
    case BTDeviceMenuScanning:
    {
        if(scanning == false)
        {
            scanning = true;
            discoveryAgent->start();
        }
        else
        {
            scanning = false;
            discoveryAgent->stop();
        }
        emit dataChanged(QModelIndex(), QModelIndex());
        break;
    }
    case BTDeviceMenuRemoveRemote:
    {
        QSettings settings;
        settings.setValue("remoteAddr","");
        Warning::showWarning(tr("INFO"),tr("Remote removed"),1);
        break;
    }
    }
}

MenuListModel* BTDeviceMenu::modelForIndex(int index)
{
    if(index > BTDeviceMenuBack && index-BTDeviceMenuBack-1 < devices.count()) {
        return new BTDeviceActionMenu(this,this,devices[(index-BTDeviceMenuBack)-1]);
    }
    return nullptr;
}

const QString BTDeviceMenu::descAtIndex(int index) const
{
    switch(index) {
    case BTDeviceMenuVisible:
        return QString(tr("Set MipFly visibility over bluetooth for outside connectivity."));
    case BTDeviceMenuScanning:
        return QString(tr("Toggle BT devices scan mode."));
    case BTDeviceMenuBack:
        return QString(tr("BACK"));
    }

    //if still not returned we have devices
    if(index>BTDeviceMenuBack)
    {
        QString ret = "MAC: "+devices[(index-BTDeviceMenuBack)-1]+"\n";
        QBluetoothLocalDevice::Pairing pairingStatus = localDevice->pairingStatus(QBluetoothAddress(devices[(index-BTDeviceMenuBack)-1]));
        if (pairingStatus == QBluetoothLocalDevice::Paired || pairingStatus == QBluetoothLocalDevice::AuthorizedPaired )
        {
            ret+=tr("paired");
        }
        else
        {
            ret+=tr("not paired");
        }
        return ret;
    }
    return QString();
}

void BTDeviceMenu::resetList()
{
    devices.clear();
    deviceNames.clear();
}

void BTDeviceMenu::hostModeStateChanged(QBluetoothLocalDevice::HostMode)
{
    emit dataChanged(QModelIndex(), QModelIndex());
}

void BTDeviceMenu::scanFinished()
{
    scanning=false;
    emit dataChanged(QModelIndex(), QModelIndex());
}

void BTDeviceMenu::addDevice(const QBluetoothDeviceInfo &info)
{
    QString label = QString("%1 %2").arg(info.address().toString()).arg(info.name());
    qDebug()<<label;

    int i = devices.indexOf(info.address().toString());

    if(i<0)
    {
        devices.append(info.address().toString());
        deviceNames.append(info.name());
    }
    else
    {
        //only the name may be different
        deviceNames[i] = info.name();
    }
    emit dataChanged(QModelIndex(), QModelIndex());
}
