/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "localizationMenu.h"

#include <QSettings>

#include "menudialog.h"
#include "ui_menudialog.h"
#include "settings/namemanager.h"
#include "utils/vario_common.h"
#include "menu/name/inputnamemenu.h"
#include "menu/timezone/timezonemenu.h"
#include "menu/bootloader/bootloadermenu.h"
#include "languageMenu.h"
#include "connectivityMenu.h"
#include "utils/updatemanager.h"

#include "Warning.h"
#include "NumericInputDecimal.h"
#include <utils/PowerOffManager.h>
#include "uiMenu.h"
#include "ChooseInput.h"

#include "VarioQt.h"
#include <QTimer>
#include "utils/unitmanager.h"

enum LocalizationMenuIndex
{
    LocalizationMenuTimezone,
    LocalizationMenuLanguage,
    LocalizationMenuSpeed,
    LocalizationMenuVerticalSpeed,
    LocalizationMenuDistance,
    LocalizationMenuHeight,
    LocalizationMenuBack
};

LocalizationMenu::LocalizationMenu(MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent)
{
}

int LocalizationMenu::rowCount(const QModelIndex &parent) const
{
    return 7;
}

const QString LocalizationMenu::captionForIndex(int index) const
{
    switch(index) {
    case LocalizationMenuTimezone:
        return QString(tr("SET TIMEZONE"));
    case LocalizationMenuLanguage:
        return QString(tr("LANGUAGE"));
    case LocalizationMenuSpeed:
        return QString(tr("SPEED") + " " + SISpeed::prefferedUnit());
    case LocalizationMenuVerticalSpeed:
        return QString(tr("VSPEED") + " " + SIVerticalSpeed::prefferedUnit());
    case LocalizationMenuDistance:
        return QString(tr("DIST.") + " " + SIDistance::prefferedUnit());
    case LocalizationMenuHeight:
        return QString(tr("HEIGHT") + " " + SIHeight::prefferedUnit());
    case LocalizationMenuBack:
        return QString(tr("BACK"));
    default:
        return QString();
    }
}

void LocalizationMenu::before(const MenuDialog &menuDialog)
{
    menuDialog.getUi().valLabel->setVisible(false);
    menuDialog.getUi().hSlider->setVisible(false);
    menuDialog.getUi().chooser->setVisible(false);
    menuDialog.getUi().valName->setVisible(false);
    menuDialog.getUi().locWidget->setVisible(false);
}

MenuType LocalizationMenu::typeForIndex(int index) const
{
    if (index == LocalizationMenuBack) {
        return Back;
    }

    if( index == LocalizationMenuTimezone)
        return List;

    if( index == LocalizationMenuLanguage)
        return List;

    return Choice;
}


void LocalizationMenu::execAtIndex(int index, const MenuDialog &menuDialog)
{
    switch(index) {
    case LocalizationMenuSpeed:
    {
        ChooseInput *ci = new ChooseInput;
        ci->setCaption(tr("SPEED"));
        ci->setValue(tr("UNIT"));
        QStringList speedUnits = SISpeed::unitList();
        ci->setChouse1(speedUnits[0]);
        ci->setChouse2(speedUnits[1]);
        ci->setChouse3(speedUnits[2]);
        ci->exec();
        SISpeed::setPrefferedUnit(speedUnits[ci->choice()-1]);
        break;
    }
    case LocalizationMenuVerticalSpeed:
    {
        ChooseInput *ci = new ChooseInput;
        ci->setCaption(tr("VSPEED"));
        ci->setValue(tr("UNIT"));
        QStringList speedUnits = SIVerticalSpeed::unitList();
        ci->setChouse1(speedUnits[0]);
        ci->setChouse3(speedUnits[1]);
        ci->exec();
        if(ci->choice()==1)SIVerticalSpeed::setPrefferedUnit(speedUnits[0]);
        if(ci->choice()==3)SIVerticalSpeed::setPrefferedUnit(speedUnits[1]);
        break;
    }
    case LocalizationMenuDistance:
    {
        ChooseInput *ci = new ChooseInput;
        ci->setCaption(tr("DISTANCE"));
        ci->setValue(tr("UNIT"));
        QStringList speedUnits = SIDistance::unitList();
        ci->setChouse1(speedUnits[0]);
        ci->setChouse3(speedUnits[1]);
        ci->exec();
        if(ci->choice()==1)SIDistance::setPrefferedUnit(speedUnits[0]);
        if(ci->choice()==3)SIDistance::setPrefferedUnit(speedUnits[1]);
        break;
    }
    case LocalizationMenuHeight:
    {
        ChooseInput *ci = new ChooseInput;
        ci->setCaption(tr("HEIGHT"));
        ci->setValue(tr("UNIT"));
        QStringList speedUnits = SIHeight::unitList();
        ci->setChouse1(speedUnits[0]);
        ci->setChouse3(speedUnits[1]);
        ci->exec();
        if(ci->choice()==1)SIHeight::setPrefferedUnit(speedUnits[0]);
        if(ci->choice()==3)SIHeight::setPrefferedUnit(speedUnits[1]);
        break;
    }
    }
}

MenuListModel* LocalizationMenu::modelForIndex(int index)
{
    switch(index) {
    case LocalizationMenuTimezone:
        return new TimezoneMenu(this);
    case LocalizationMenuLanguage:
        return new LanguageMenu(this);
    }
    return nullptr;
}

const QString LocalizationMenu::descAtIndex(int index) const
{
    switch(index) {
    case LocalizationMenuTimezone:
        return QString(tr("Change the timezone you are in to adjust from the universal time."));
    case LocalizationMenuLanguage:
        return QString(tr("Change the language of the device."));
    default:
        return QString();
    }
}
