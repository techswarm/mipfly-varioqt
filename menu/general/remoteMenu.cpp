/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "remoteMenu.h"
#include "menu/menuutils.h"
#include "menudialog.h"
#include "ui_menudialog.h"
#include "Warning.h"

#include "VarioQt.h"

enum RemoteMenuIndex
{
    RemoteMenuKey1,
    RemoteMenuKey2,
    RemoteMenuKey3,
    RemoteMenuLongKey1,
    RemoteMenuLongKey2,
    RemoteMenuLongKey3,
    RemoteMenuBack
};


RemoteMenu::RemoteMenu(MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent)
{
    keyMapIndex = -1;
}

int RemoteMenu::rowCount(const QModelIndex &parent) const
{
    return 7;
}

const QString RemoteMenu::captionForIndex(int index) const
{
    switch(index) {
    case RemoteMenuKey1:
        return QString(tr("KEY:Left/Up"));
    case RemoteMenuKey2:
        return QString(tr("KEY:Right/Down"));
    case RemoteMenuKey3:
        return QString(tr("KEY:Enter"));
    case RemoteMenuLongKey1:
        return QString(tr("KEY:Esc"));
    case RemoteMenuLongKey2:
        return QString(tr("KEY:Function"));
    case RemoteMenuLongKey3:
        return QString(tr("KEY:Menu"));
    case RemoteMenuBack:
        return QString(tr("BACK"));
    default:
        return QString();
    }
}

void RemoteMenu::execAtIndex(int index, const MenuDialog &menuDialog)
{
    if(index >= RemoteMenuKey1 && index <= RemoteMenuLongKey3)
    {
        if(keyMapIndex < 0)
        {
            Warning::showWarning(tr("ACTION"),tr("Press remote button"),1);
            VarioQt::instance().getInputThread()->setEmitKeypress(false);
            keyMapIndex = index;
            connect(VarioQt::instance().getInputThread(),&InputThread::keyCode,this,&RemoteMenu::remoteKeyCode);
        }
        else
        {
            Warning::showWarning(tr("WARNING"),tr("Button not set"),2);
        }
    }
}

void RemoteMenu::remoteKeyCode(int key)
{
    if(keyMapIndex < 0)return;

    if(keyMapIndex == RemoteMenuKey1)
        VarioQt::instance().getInputThread()->setKeyShortPress1(key);
    if(keyMapIndex == RemoteMenuKey2)
        VarioQt::instance().getInputThread()->setKeyShortPress2(key);
    if(keyMapIndex == RemoteMenuKey3)
        VarioQt::instance().getInputThread()->setKeyShortPress3(key);

    if(keyMapIndex == RemoteMenuLongKey1)
        VarioQt::instance().getInputThread()->setKeyLongPress1(key);
    if(keyMapIndex == RemoteMenuLongKey2)
        VarioQt::instance().getInputThread()->setKeyLongPress2(key);
    if(keyMapIndex == RemoteMenuLongKey3)
        VarioQt::instance().getInputThread()->setKeyLongPress3(key);

    Warning::showWarning(tr("INFO"),tr("Key Mapped"),2);

    keyMapIndex = -1;
    VarioQt::instance().getInputThread()->setEmitKeypress(true);
}

void RemoteMenu::before(const MenuDialog &menuDialog)
{

}

MenuType RemoteMenu::typeForIndex(int index) const
{
    if (index == RemoteMenuBack) {
        return Back;
    }
    return Choice;
}

