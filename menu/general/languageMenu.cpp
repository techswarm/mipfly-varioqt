/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "languageMenu.h"

#include "menudialog.h"
#include "ui_menudialog.h"
#include "sound/varioSound.h"
#include "utils/vario_common.h"
#include "navUtils/variocompute.h"
#include <QSettings>
#include <QDir>
#include "VarioQt.h"
#include <QTranslator>

#include <Warning.h>


LanguageMenu::LanguageMenu(MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent)
{
    QDir dir("languages");
    dir.setFilter(QDir::Files);
    dir.setSorting(QDir::Name);
    languages = dir.entryList();
    //add English as the first language in list
    languages.insert(0,"English");
}

int LanguageMenu::rowCount(const QModelIndex &parent) const
{
    return languages.count()+1;
}

const QString LanguageMenu::captionForIndex(int index) const
{
    if(index < languages.count())
    {
        QString ret = languages[index];
        int lastDotPos = ret.lastIndexOf(QChar('.'));
        if(lastDotPos > 0)
            return ret.left(lastDotPos);
        else
            return ret;

    }
    else
        return QString(tr("BACK"));
}

void LanguageMenu::before(const MenuDialog &menuDialog)
{

}


MenuType LanguageMenu::typeForIndex(int index) const
{
    if (index == languages.count()) {
        return Back;
    }
    return Choice;
}

void LanguageMenu::execAtIndex(int index, const MenuDialog &menuDialog)
{
    if(index < languages.count())
    {
        QSettings settings;
        settings.setValue("language",languages[index]);
        VarioQt::instance().getTranslator()->load("languages/"+languages[index]);
        Warning::showWarning(tr("INFO"),tr("Language was set"), 1);
    }
}

MenuListModel* LanguageMenu::modelForIndex(int index)
{
    return nullptr;
}
