/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "menudialog.h"
#include "ui_menudialog.h"
#include "TextInput.h"

#include "WiFiDevice.h"
#include "utils/ssid.h"
#include <QDebug>
#include <QSettings>
#include "Warning.h"
#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>

/*
nmcli d
nmcli radio wifi
nmcli radio wifi <on|off>
nmcli device wifi list
nmcli device wifi rescan
nmcli device wifi connect <SSID|BSSID>
nmcli device wifi connect <SSID|BSSID> password <password>
*/

char *listInterfaces    =   (char *)"nmcli d";
char *listAccessPoints  =   (char *)"nmcli -t -f SSID,ACTIVE,CHAN,SECURITY  device wifi list";
char disconnectAP[]     =   "nmcli con down id ";
char connectAP[]        =   "nmcli device wifi connect ";

enum
{
    WiFiDeviceMenuUpdate,
    WiFiDeviceMenuBack
};

WiFiDeviceMenu::WiFiDeviceMenu(MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent)
{
    qDebug() << "WiFiDeviceMenu instance";

    wifiAdapterAvailable = isAdapterAvailable();
    if (wifiAdapterAvailable) ssidScan();

}

WiFiDeviceMenu::~WiFiDeviceMenu()
{

}

int WiFiDeviceMenu::rowCount(const QModelIndex &parent) const
{
    return 2+ssidList.size(); // + devices.count();
}

const QString WiFiDeviceMenu::captionForIndex(int index) const
{
    switch(index) {

    case WiFiDeviceMenuUpdate:
        return QString(tr("WIFI SEARCH"));
    case WiFiDeviceMenuBack:
        return QString(tr("BACK"));
    }

    if (ssidList.size() > 0) {
            return (ssidList[index-2]->getName());
    }
    return QString();
}


void WiFiDeviceMenu::before(const MenuDialog &menuDialog)
{
     qDebug() << "WiFiDeviceMenu before";
     ssidScan();

}

MenuType WiFiDeviceMenu::typeForIndex(int index) const
{
    switch (index) {
        case WiFiDeviceMenuUpdate:
            return Choice;
        case WiFiDeviceMenuBack:
            return Back;
        default:
            return Choice;
    }
}

void WiFiDeviceMenu::execAtIndex(int index, const MenuDialog &menuDialog)
{
    //char pConnectStr[1024];
    //QString retStr;

    switch (index) {
        case WiFiDeviceMenuUpdate:
            if (wifiAdapterAvailable == true) ssidScan();
            break;
    }

    if (index > 1) {

        int selectedWLan = index - 2;

        if (ssidList[selectedWLan]->isConnected() == true) {
            disconnectFromWiFi(selectedWLan);
        } else {
            connectToWiFi(selectedWLan);
        }

        /* Do a scan to refresh the list and properties of the sssid's*/
        ssidScan();

    }
}

MenuListModel* WiFiDeviceMenu::modelForIndex(int index)
{
    return nullptr;
}

const QString WiFiDeviceMenu::descAtIndex(int index) const
{
    QString retStr;

    switch(index) {
    case WiFiDeviceMenuUpdate:
        if (wifiAdapterAvailable) {
        return QString(tr("Rescan WiFi networks"));
        } else {
            return QString(tr("No WiFiAdapter available\n"));
        }
    case WiFiDeviceMenuBack:
        return QString(tr("BACK"));
    }

    if (ssidList.size() > 0) {
            retStr = ssidList[index-2]->getSecurity();
            if (ssidList[index-2]->isConnected()) retStr += "\nConnected";
            return retStr;
    }

    return QString("");
}

void WiFiDeviceMenu::connectToWiFi(int selectedWLan) {

    char pConnectStr[1024];
    QString password;
    QString retStr;

    qDebug() << "*******************************************************";
    qDebug() << "Connect to SSID:" + ssidList[selectedWLan]->getName();
    qDebug() << "*******************************************************";

    QByteArray ba = ssidList[selectedWLan]->getName().toLocal8Bit();

    strcpy(pConnectStr, connectAP);
    strcat(pConnectStr, ba.data());

    if (ssidList[selectedWLan]->isProtected()) {

        TextInput *ti = new TextInput;
        ti->setAllPrintAscii();
        ti->exec();
        password = ti->value();
        delete ti;

        strcat(pConnectStr, " password ");

        QByteArray ba2 = password.toLocal8Bit();
        strcat(pConnectStr, ba2.data());

    }

    qDebug() << QString::fromStdString(pConnectStr);
    retStr = exec(pConnectStr);
    qDebug() << retStr;

}


void WiFiDeviceMenu::disconnectFromWiFi(int selectedWLan) {

    char pDiscConnectStr[1024];
    QString retStr;

    qDebug() << "*******************************************************";
    qDebug() << "Disconnect from SSID:" + ssidList[selectedWLan]->getName();
    qDebug() << "*******************************************************";

    QByteArray ba = ssidList[selectedWLan]->getName().toLocal8Bit();

    strcpy(pDiscConnectStr, disconnectAP);
    strcat(pDiscConnectStr, ba.data());

    qDebug() << QString::fromStdString(pDiscConnectStr);

    retStr = exec(pDiscConnectStr);

    qDebug() << retStr;

}


bool WiFiDeviceMenu::isAdapterAvailable() {

    QString retStr;

    retStr = exec(listInterfaces);

    if (retStr.contains("wifi", Qt::CaseInsensitive)) return true;

    return false;
}

void WiFiDeviceMenu::ssidScan() {

    QString retStr;
    QStringList ssidStrList;

    /* Do I need to delete the ssid objs that might be in the list before
    clearing ? */

    ssidList.clear();

    retStr = exec(listAccessPoints);

    qDebug() << "Return:"+ retStr;

    ssidStrList = retStr.split(QRegExp("[\r\n]"));

    for (QString str : ssidStrList) {
        str = str.remove(QRegExp("[\\n\\t\\r]"));
        qDebug() << "Row:" + str;
        qDebug() << str.trimmed();
        QStringList row = str.trimmed().split(":");

        if (row.size() > 0)
        if (row[0].size() > 0) {
            ssidList.append(new Ssid(row[0], row[1], row[2], row[3]) );
        }
    }
}

QString WiFiDeviceMenu::exec(char* cmd) const {
    std::array<char, 128> buffer;
    std::string result;
    std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
    if (!pipe) {
        throw std::runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result += buffer.data();
    }

        return QString::fromStdString(result);
}


