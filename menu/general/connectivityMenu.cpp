/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "connectivityMenu.h"

#include <QSettings>
#include "menudialog.h"
#include "ui_menudialog.h"
#include "utils/updatemanager.h"
#include "menu/general/BTDevice.h"
#include "menu/general/WiFiDevice.h"
#include "utils/BTUtil.h"
#include "remoteMenu.h"
#include "Warning.h"
#include "VarioQt.h"
#include "NumericInputDecimal.h"
#include "NumericInput.h"
#include "utils/AccountConnectionManager.h"
#include <qdebug.h>

enum ConnectivityMenuIndex
{
	ConnectivityMenuBluetooth,
	ConnectivityMenuRemote,
	ConnectivityMenuWiFi,
    ConnectivityMenuAccount,
    ConnectivityMenuLive,
	ConnectivityMenuTTY,
    ConnectivityMenuGPSPostscale,
    ConnectivityMenuVarioPostscale,
	ConnectivityMenuBack
};

enum ConnectivityMenuActions
{
    ConnectivityMenuActionNothing,
    ConnectivityMenuActionConnect,
	ConnectivityMenuBackAction
};

ConnectivityMenu::ConnectivityMenu(UpdateManager *pUpdateManager, MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent)
{
        qDebug() << "ConnectivityMenu instance";
        this->updateManager = pUpdateManager;
        connect(updateManager,SIGNAL(connectionStatusResult(bool)),this,SLOT(internetAvailable(bool)),Qt::UniqueConnection);
        connect(&accountConnectionManager,SIGNAL(pairFinished(bool)),this,SLOT(accountConnectionFinished(bool)));
}

int ConnectivityMenu::rowCount(const QModelIndex &parent) const
{
    return 9;
}

const QString ConnectivityMenu::captionForIndex(int index) const
{
    switch(index) {
    case ConnectivityMenuBluetooth:
        return QString(tr("BLUETOOTH"));
    case ConnectivityMenuRemote:
        return QString(tr("REMOTE"));
    case ConnectivityMenuWiFi:
        return QString(tr("WIFI"));
    case ConnectivityMenuAccount:
        return QString(tr("ACCOUNT"));

    case ConnectivityMenuGPSPostscale:
    {
        if(VarioQt::instance().getBTServer()==nullptr)
            return QString(tr("GPSPostscale - NAN"));
        else
        {
            return QString(tr("GPSPostscale - ")) + QString::number(VarioQt::instance().getBTServer()->getGpsPostscale());
        }
    }
    case ConnectivityMenuVarioPostscale:
    {
        if(VarioQt::instance().getBTServer()==nullptr)
            return QString(tr("VarioPostscale - NAN"));
        else
        {
            return QString(tr("VarioPostscale - ")) + QString::number(VarioQt::instance().getBTServer()->getVarioPostscale());
        }
    }
    case ConnectivityMenuLive:
    {
        QSettings settings;
        bool liveTrackingEnabled = settings.value("liveTrackingingEnabled", "true").toBool();
        if(liveTrackingEnabled)return QString(tr("Live ON"));
        else return QString(tr("Live OFF"));
    }
	case ConnectivityMenuTTY:
        return QString(tr("ACTIVATE TTY"));
    case ConnectivityMenuBack:
        return QString(tr("BACK"));
    default:
        return QString();
    }
}

void ConnectivityMenu::before(const MenuDialog &menuDialog)
{
    qDebug() << "ConnectivityMenu before";

    menuDialog.getUi().valLabel->setVisible(false);
    menuDialog.getUi().hSlider->setVisible(false);
    menuDialog.getUi().chooser->setVisible(false);
    menuDialog.getUi().valName->setVisible(false);
    menuDialog.getUi().locWidget->setVisible(false);
}

MenuType ConnectivityMenu::typeForIndex(int index) const
{

    if( index == ConnectivityMenuBluetooth && BTUtil::hciDeviceIsAvailable()){
        return List;
    }

    if( index == ConnectivityMenuAccount)
        return Choice;

    if (index == ConnectivityMenuBack) {
        return Back;
    }

    if( index == ConnectivityMenuTTY || index == ConnectivityMenuGPSPostscale || index == ConnectivityMenuVarioPostscale)
        return Choice;

    if (index == ConnectivityMenuWiFi)
        return List;

    if (index == ConnectivityMenuRemote)
        return List;

    return Choice;
}


void ConnectivityMenu::execAtIndex(int index, const MenuDialog &menuDialog)
{

    if(index == ConnectivityMenuAccount && connectivityMenuAction == ConnectivityMenuActionNothing)
    {
        connectivityMenuAction = ConnectivityMenuAccount;
        updateManager->checkConnection();
        qDebug()<<"Check internet connection...";
        Warning::showWarning(tr("INFO"),tr("Check Conn"));
    }

    if(index == ConnectivityMenuLive)
    {
        QSettings settings;
        bool liveTrackingEnabled = settings.value("liveTrackingingEnabled", "true").toBool();
        if(liveTrackingEnabled)settings.setValue("liveTrackingingEnabled",false);
        else settings.setValue("liveTrackingingEnabled",true);
        emit dataChanged(QModelIndex(),QModelIndex());
    }

    if(index == ConnectivityMenuTTY)
    {
        if(VarioQt::instance().getTtyMode() == true)
        {
            Warning::showWarning(tr("TTY error"), tr("Active"), 1);
        }
        else
        {
            Warning::showWarning(tr("TTY mode"), tr("ACTIVATED"), 5);
            system("modprobe g_serial");
            VarioQt::instance().setTtyMode(true);
        }
    }

    if(index == ConnectivityMenuGPSPostscale)
    {
        if(VarioQt::instance().getBTServer() != nullptr)
        {
            NumericInput *ni = new NumericInput();
            ni->setMax(10);
            ni->setMin(1);
            ni->setValue(VarioQt::instance().getBTServer()->getGpsPostscale());
            ni->setCaption(tr("GPS Postscale"));
            ni->move(240/2 - ni->width()/2,320/2 - ni->height()/2);
            ni->exec();

            VarioQt::instance().getBTServer()->setGpsPostscale(ni->value());
            delete ni;
        }
    }
    if(index == ConnectivityMenuVarioPostscale)
    {
        if(VarioQt::instance().getBTServer() != nullptr)
        {
            NumericInput *ni = new NumericInput();
            ni->setMax(10);
            ni->setMin(1);
            ni->setValue(VarioQt::instance().getBTServer()->getVarioPostscale());
            ni->setCaption(tr("Vario Postscale"));
            ni->move(240/2 - ni->width()/2,320/2 - ni->height()/2);
            ni->exec();

            VarioQt::instance().getBTServer()->setVarioPostscale(ni->value());
            delete ni;
        }
    }
}

MenuListModel* ConnectivityMenu::modelForIndex(int index)
{
  switch(index) {
    case ConnectivityMenuBluetooth:
        return new BTDeviceMenu(this);
    case ConnectivityMenuWiFi:
        return new WiFiDeviceMenu(this);
    case ConnectivityMenuRemote:
        //TODO investigate overloading problems
        return new RemoteMenu(this,this);
  default:
      return nullptr;
    }
}

const QString ConnectivityMenu::descAtIndex(int index) const
{
    switch(index) {
    case ConnectivityMenuBluetooth:
        if(!BTUtil::hciDeviceIsAvailable())
        {
            return QString(tr("No Bluetooth available."));
        }
        else
        {
            return QString(tr("Scan, pair and unpair bluetooth devices."));
        }
    case ConnectivityMenuRemote:
        return QString(tr("Set remote settings and map keys."));
    case ConnectivityMenuVarioPostscale:
        return QString(tr("Set variometer data postscaler for BT transmission. 1 is equivalent to one message every 100ms. 2 -> 200ms etc"));
    case ConnectivityMenuGPSPostscale:
        return QString(tr("Set GPS data postscaler for BT transmission. 1 is equivalent to one message every 200ms. 2 -> 400ms etc"));
    case ConnectivityMenuLive:
        return QString(tr("Enable or disable Live Tracking functionality."));
    default:
        return QString();
    }
}

void ConnectivityMenu::internetAvailable(bool connected)
{
        if (connected)
        {
            connectToAccount();
        }
}

void ConnectivityMenu::accountConnectionFinished(bool success)
{
    if(success)
    {
        Warning::showWarning(tr("INFO"),tr("Success!"));
        VarioQt::instance().getLiveTrack()->updateAccount();
    }
    else
    {
        Warning::showWarning(tr("INFO"),tr("Fail!"));
    }
    connectivityMenuAction = ConnectivityMenuActionNothing;
}

void ConnectivityMenu::connectToAccount() {

    if(connectivityMenuAction == ConnectivityMenuAccount)
        {
            NumericInputDecimal *nid = new NumericInputDecimal;
            nid->setCaption("PIN");
            nid->setMax(9999);
            nid->move(240/2 - nid->width()/2,320/2 - nid->height()/2);
            nid->exec();

            int pin = nid->value();
            accountConnectionManager.connectAccount(pin);

            delete nid;
        }
}
