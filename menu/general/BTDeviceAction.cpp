/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "BTDeviceAction.h"
#include <qbluetoothaddress.h>
#include <qbluetoothdevicediscoveryagent.h>
#include <qbluetoothlocaldevice.h>
#include <QDebug>
#include <QSettings>
#include "Warning.h"
#include "utils/BTUtil.h"
#include "menudialog.h"
#include "menu/general/BTDevice.h"

enum
{
    BTDeviceActionMenuPair,
    BTDeviceActionMenuConnect,
    BTDeviceActionMenuRemove,
    BTDeviceActionMenuSetAsRemote,
    BTDeviceActionMenuBack
};

BTDeviceActionMenu::BTDeviceActionMenu(MenuListModel *parentMenu, QObject *parent, QString BTMAC)
    : MenuListModel(parentMenu, parent), localDevice(new QBluetoothLocalDevice), device(BTMAC)
{
    connect(localDevice, SIGNAL(pairingFinished(QBluetoothAddress,QBluetoothLocalDevice::Pairing))
            , this, SLOT(pairingDone(QBluetoothAddress,QBluetoothLocalDevice::Pairing)));
    parentMenuP = parentMenu;
}

BTDeviceActionMenu::~BTDeviceActionMenu()
{
}

int BTDeviceActionMenu::rowCount(const QModelIndex &parent) const
{
    return 5;
}

const QString BTDeviceActionMenu::captionForIndex(int index) const
{
    switch(index) {

    case BTDeviceActionMenuPair:
        return QString(tr("PAIR"));

    case BTDeviceActionMenuConnect:
        return QString(tr("CONNECT"));

    case BTDeviceActionMenuRemove:
        return QString(tr("REMOVE"));

    case BTDeviceActionMenuSetAsRemote:
        return QString(tr("SET AS REMOTE"));

    case BTDeviceActionMenuBack:
        return QString(tr("BACK"));
    default:
        return QString();
    }
}

void BTDeviceActionMenu::before(const MenuDialog &menuDialog)
{
}

MenuType BTDeviceActionMenu::typeForIndex(int index) const
{
    switch (index) {
    case BTDeviceActionMenuBack:
        return Back;
    default:
        return Choice;
    }
}

void BTDeviceActionMenu::execAtIndexNC(int index, MenuDialog &menuDialog)
{
    switch (index) {
    case BTDeviceActionMenuPair:
    {
        QBluetoothAddress address (device);
        localDevice->requestPairing(address, QBluetoothLocalDevice::Paired);
        Warning::showWarning(tr("INFO"),tr("Pair started"),1);
        break;
    }
    case BTDeviceActionMenuConnect:
    {
        QBluetoothAddress address (device);
        BTUtil::performBTConnect(address.toString());
        Warning::showWarning(tr("INFO"),tr("Connect started"),1);
        break;
    }
    case BTDeviceActionMenuRemove:
    {
        QBluetoothAddress address (device);
        localDevice->requestPairing(address, QBluetoothLocalDevice::Unpaired);
        BTUtil::removeBTEntry(device);

        Warning::showWarning(tr("INFO"),tr("Remove started"),1);
        ((BTDeviceMenu *)parentMenuP)->resetList();
        break;
    }
    case BTDeviceActionMenuSetAsRemote:
    {
        QSettings settings;
        settings.setValue("remoteAddr",device);
        Warning::showWarning(tr("INFO"),tr("Remote set"),1);
        break;
    }
    }
}

MenuListModel* BTDeviceActionMenu::modelForIndex(int index)
{
    switch(index) {

    }
    return nullptr;
}

const QString BTDeviceActionMenu::descAtIndex(int index) const
{
    switch(index) {
    default:
        return QString();
    }
}

void BTDeviceActionMenu::pairingDone(const QBluetoothAddress &address, QBluetoothLocalDevice::Pairing pairing)
{
    if (pairing == QBluetoothLocalDevice::Paired || pairing == QBluetoothLocalDevice::AuthorizedPaired )
    {
        Warning::showWarning(tr("INFO"),tr("Device paired"),1);
    }
    else
    {
        Warning::showWarning(tr("INFO"),tr("Device unpaired"),1);
    }
}
