/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "generalMenu.h"

#include <QSettings>

#include "menudialog.h"
#include "ui_menudialog.h"
#include "settings/namemanager.h"
#include "utils/vario_common.h"
#include "menu/name/inputnamemenu.h"
#include "menu/timezone/timezonemenu.h"
#include "menu/bootloader/bootloadermenu.h"
#include "languageMenu.h"
#include "connectivityMenu.h"
#include "utils/updatemanager.h"

#include "Warning.h"
#include "NumericInputDecimal.h"
#include <utils/PowerOffManager.h>
#include "uiMenu.h"
#include "ChooseInput.h"
#include "localizationMenu.h"

#include "VarioQt.h"
#include <QTimer>

enum GeneralMenuIndex
{
    GeneralMenuPilot,
    GeneralMenuLocalization,
    GeneralMenuGPSStats,
    GeneralMenuBoot,
    GeneralMenuConnectivity,
    GeneralMenuUpdate,
    GeneralMenuUI,
    GeneralMenuMass,
    GeneralMenuBack
};

enum GeneralMenuActions
{
    GeneralMenuActionNothing,
    GeneralMenuActionUpdate,
    GeneralMenuActionUpdateOs
};

GeneralMenu::GeneralMenu(const MenuDialog &menuDialog, MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent),menuDialog(menuDialog)
{
    connect(&updateManager,SIGNAL(grabUpdateResult(bool)),this,SLOT(updateGrabResult(bool)),Qt::UniqueConnection);
    connect(&updateManager,SIGNAL(connectionStatusResult(bool)),this,SLOT(internetAvailable(bool)),Qt::UniqueConnection);

    timer.setInterval(1000);
    connect(&timer,SIGNAL(timeout()),this,SLOT(timerEvent()));
    timer.start();
}

int GeneralMenu::rowCount(const QModelIndex &parent) const
{
    return 9;
}

const QString GeneralMenu::captionForIndex(int index) const
{
    switch(index) {
    case GeneralMenuPilot:
        return QString(tr("PILOT NAME"));
    case GeneralMenuLocalization:
        return QString(tr("LOCALIZATION"));
    case GeneralMenuBoot:
        return QString(tr("BOOTLOAD"));
    case GeneralMenuConnectivity:
        return QString(tr("CONNECTIVITY"));
    case GeneralMenuGPSStats:
        return QString(tr("GPS Info"));
    case GeneralMenuUpdate:
        return QString(tr("UPDATE"));
    case GeneralMenuUI:
        return QString(tr("USER INTERFACE"));
    case GeneralMenuMass:
        return QString(tr("MASS STORAGE"));
    case GeneralMenuBack:
        return QString(tr("BACK"));
    default:
        return QString();
    }
}

void GeneralMenu::before(const MenuDialog &menuDialog)
{
    menuDialog.getUi().valLabel->setVisible(false);
    menuDialog.getUi().hSlider->setVisible(false);
    menuDialog.getUi().chooser->setVisible(false);
    menuDialog.getUi().valName->setVisible(false);
    menuDialog.getUi().locWidget->setVisible(false);
}

MenuType GeneralMenu::typeForIndex(int index) const
{
    if (index == GeneralMenuBack) {
        return Back;
    }

    /*
    if( index == GeneralMenuConnectivity && !BTUtil::hciDeviceIsAvailable()){
        return Choice;
    }*/

    if( index == GeneralMenuGPSStats)
        return Choice;

    if( index == GeneralMenuUpdate)
        return Choice;

    if( index == GeneralMenuMass)
        return Choice;



    return List;
}


void GeneralMenu::execAtIndex(int index, const MenuDialog &menuDialog)
{
    if(index == GeneralMenuUpdate && generalMenuAction == GeneralMenuActionNothing)
    {
        ChooseInput *ci = new ChooseInput;
        ci->setCaption(tr("UPDATE"));
        ci->setValue(tr("TYPE"));
        ci->setChouse1(tr("BETA"));
        ci->setChouse2(tr("CANCEL"));
        ci->setChouse3(tr("STABLE"));

        ci->exec();

        if(ci->choice() == 3)updateManager.setUpdateToBeta(false);
        if(ci->choice() == 1)updateManager.setUpdateToBeta(true);
        if(ci->choice() != 2)
        {
            generalMenuAction = GeneralMenuActionUpdate;
            updateManager.checkConnection();
            qDebug()<<"Check internet connection...";
            Warning::showWarning(tr("INFO"),tr("Check Conn"));
        }
    }

    if(index == GeneralMenuGPSStats)
    {
        VarioQt::instance().getMainUI()->dynamicUI->showPage(DynamicUI::SpecialPages::GpsStatusPage);
        VarioQt::instance().getMainUI()->gpioLongPressed(Vario::GPIO3);
    }

    if(index == GeneralMenuMass)
    {
        if(VarioQt::instance().getTtyMode() == true)
        {
            Warning::showWarning(tr("TTY mode active"), tr("Pls. Reboot!"), 5);
        }
        else
        {
            PowerOffManager *p = new PowerOffManager();
            p->massStorageActivate();
            Warning::showWarning(tr("MASS STORAGE MODE"), tr("HOLD F TO EXIT"), 5);
        }
    }
}

MenuListModel* GeneralMenu::modelForIndex(int index)
{
    switch(index) {
    case GeneralMenuPilot:
        return new InputNameMenu(*(VarioQt::instance().getNameManager()), this);
    case GeneralMenuLocalization:
        return new LocalizationMenu(this);
    case GeneralMenuBoot:
        return new BootloaderMenu(*(VarioQt::instance().getVarioSerialThread()), menuDialog, this);
    case GeneralMenuConnectivity:
        return new ConnectivityMenu(&updateManager, this);
    case GeneralMenuUI:
        return new UiMenu(this);
    }
    return nullptr;
}

const QString GeneralMenu::descAtIndex(int index) const
{
    switch(index) {
    case GeneralMenuPilot:
        return QString(tr("Change the name of the owner. Used for IGC data and flight records."));
    case GeneralMenuLocalization:
        return QString(tr("Change settings like language, timezone or default units."));
    case GeneralMenuBoot:
        return QString(tr("Load a newer or different firmware to your device."));
    case GeneralMenuConnectivity:
        return QString(tr("Connectivity settings, Bluetooth/WiFi"));
    case GeneralMenuUI:
        return QString(tr("Update and select UI."));
    case GeneralMenuGPSStats:
        return QString(tr("Show GPS status page."));
    case GeneralMenuUpdate:
        return QString(tr("Update software to latest version. Restart will be required for the changes to take place. Requires working internet connection."));
        //case GeneralMenuUpdateOs:
        //    return QString(tr("Update OS to latest version. Restart will be required for the changes to take place. Requires working internet connection."));
    case GeneralMenuMass:
        return QString(tr("Switch to mass storage mode for USB OTG."));
    default:
        return QString();
    }
}

void GeneralMenu::internetAvailable(bool connected)
{
    if(connected)
    {
        if(generalMenuAction == GeneralMenuActionUpdate)
        {
            Warning::showWarning(tr("INFO"),tr("Conn OK"));
            updateManager.grabUpdateFiles();
        }
        if(generalMenuAction == GeneralMenuActionUpdateOs)
        {
            Warning::showWarning(tr("INFO"),tr("Conn OK"));
            updateManager.grabOsUpdateFiles();
        }

    }
    else
    {
        Warning::showWarning(tr("ERROR"),tr("Not Connected"));
        generalMenuAction = GeneralMenuActionNothing;
    }
}

void GeneralMenu::updateGrabResult(bool success)
{
    if(success)
    {
        if(generalMenuAction == GeneralMenuActionUpdate)
        {
            updateManager.deploy();
#ifndef __ARMEL__

#else
        PowerOffManager *p = new PowerOffManager();
        p->powerOff();
        //workarround for shutdown not happening bug.
        system("shutdown -P 2");
        Warning::showWarning(tr("Success"),tr("Powering Off!"), 5);
#endif
        }
        if(generalMenuAction == GeneralMenuActionUpdateOs)
        {
            updateManager.deployOs();
            Warning::showWarning(tr("INFO"),tr("Success! Pls Reboot"));
        }
    }
    else
    {
        Warning::showWarning(tr("ERROR"),tr("Not Updated"));
    }
    generalMenuAction = GeneralMenuActionNothing;
}

void GeneralMenu::timerEvent()
{
    if(generalMenuAction == GeneralMenuActionUpdate || generalMenuAction == GeneralMenuActionUpdateOs)
    {
        marquee+=1;
        QString dots;
        if(marquee%3 == 0)dots=".";
        if(marquee%3 == 1)dots="..";
        if(marquee%3 == 2)dots="...";
        Warning::showWarning(tr("INFO"),tr("Downloading")+dots,2);
    }
}
