/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "uiMenu.h"

#include <QSettings>

#include "menudialog.h"
#include "ui_menudialog.h"
#include "settings/namemanager.h"
#include "utils/vario_common.h"
#include "menu/name/inputnamemenu.h"
#include "menu/timezone/timezonemenu.h"
#include "menu/bootloader/bootloadermenu.h"
#include "menu/general/BTDevice.h"
#include "languageMenu.h"
#include "remoteMenu.h"
#include "utils/BTUtil.h"
#include "utils/updatemanager.h"
#include "utils/AccountConnectionManager.h"
#include "Warning.h"
#include "NumericInputDecimal.h"
#include <utils/PowerOffManager.h>

#include "VarioQt.h"
#include <QTimer>

enum UiMenuIndex
{
    UiMenuGet,
    UiMenuBack
};

enum GeneralMenuActions
{
    UiMenuActionNothing,
    UiMenuActionGet,
    UiMenuActionConnect
};

UiMenu::UiMenu(MenuListModel *parentMenu, QObject *parent)
    : MenuListModel(parentMenu, parent),menuDialog(menuDialog)
{
    connect(&updateManager,SIGNAL(grabUpdateResult(bool)),this,SLOT(updateGrabResult(bool)),Qt::UniqueConnection);
    connect(&updateManager,SIGNAL(connectionStatusResult(bool)),this,SLOT(internetAvailable(bool)),Qt::UniqueConnection);

    QSettings settings;
    QString key = settings.value("accountKey","").toString();
    QString id = settings.value("accountId","").toString();
    updateManager.setUserId(id);
    updateManager.setUserKey(key);

    timer.setInterval(1000);
    connect(&timer,SIGNAL(timeout()),this,SLOT(timerEvent()));
    timer.start();
}

int UiMenu::rowCount(const QModelIndex &parent) const
{
    QDir directory("ui");
    QStringList uis = directory.entryList(QDir::Files | QDir::NoDotAndDotDot);
    return 2+uis.count();
}

const QString UiMenu::captionForIndex(int index) const
{
    switch(index) {
    case UiMenuGet:
        return QString(tr("GET UI"));
    case UiMenuBack:
        return QString(tr("BACK"));
    }
    //if not returned we have to show the coresponding filename
    QDir directory("ui");
    QStringList uis = directory.entryList(QDir::Files | QDir::NoDotAndDotDot);
    return uis.at(index - UiMenuBack - 1);
}

void UiMenu::before(const MenuDialog &menuDialog)
{
    menuDialog.getUi().valLabel->setVisible(false);
    menuDialog.getUi().hSlider->setVisible(false);
    menuDialog.getUi().chooser->setVisible(false);
    menuDialog.getUi().valName->setVisible(false);
    menuDialog.getUi().locWidget->setVisible(false);
}

MenuType UiMenu::typeForIndex(int index) const
{
    if (index == UiMenuBack) {
        return Back;
    }
    return Choice;
}


void UiMenu::execAtIndex(int index, const MenuDialog &menuDialog)
{
    if(index == UiMenuGet && generalMenuAction == UiMenuActionNothing)
    {
        generalMenuAction = UiMenuActionGet;
        updateManager.checkConnection();
        qDebug()<<"Check internet connection...";
        Warning::showWarning(tr("INFO"),tr("Check Conn"));
    }
    if(index > UiMenuBack)
    {
        QDir directory("ui");
        QStringList uis = directory.entryList(QDir::Files | QDir::NoDotAndDotDot);
        QSettings settings;
        settings.setValue("uiName",uis.at(index - UiMenuBack - 1));
        Warning::showWarning(tr("INFO"),tr("UI Set"));
    }
}

MenuListModel* UiMenu::modelForIndex(int index)
{
    return nullptr;
}

const QString UiMenu::descAtIndex(int index) const
{
    switch(index) {
    case UiMenuGet:
        return QString(tr("Get the user interfaces form your account or the standard recommended one if no account connected."));
    case UiMenuBack:
        return QString(tr("Back"));
    default:
        return QString();
    }
}

void UiMenu::internetAvailable(bool connected)
{
    if(connected)
    {
        if(generalMenuAction == UiMenuActionGet)
        {
            Warning::showWarning(tr("INFO"),tr("Conn OK"));
            updateManager.grabUiUpdateFiles();
        }
    }
    else
    {
        Warning::showWarning(tr("ERROR"),tr("Not Connected"));
        generalMenuAction = UiMenuActionNothing;
    }
}

void UiMenu::updateGrabResult(bool success)
{
    if(success)
    {
        if(generalMenuAction == UiMenuActionGet)
        {
            updateManager.deployUi();
            Warning::showWarning(tr("INFO"),tr("Success!"));

            QDir directory("ui");
            QStringList uis = directory.entryList(QDir::Files | QDir::NoDotAndDotDot);
            QSettings settings;
            QString savedUi = settings.value("uiName","default.xml").toString();
            if(uis.indexOf(savedUi)>=0)
            {

            }
            else
            {
                //new list doesn't contain old saved ui
                QSettings settings;
                settings.setValue("uiName","default.xml");
                Warning::showWarning(tr("INFO"),tr("Set to default.xml"));
            }
            emit dataChanged(QModelIndex(), QModelIndex());
        }
    }
    else
    {
        Warning::showWarning(tr("ERROR"),tr("Not Updated"));
    }
    generalMenuAction = UiMenuActionNothing;
}

void UiMenu::timerEvent()
{
    if(generalMenuAction == UiMenuActionGet)
    {
        marquee+=1;
        QString dots;
        if(marquee%3 == 0)dots=".";
        if(marquee%3 == 1)dots="..";
        if(marquee%3 == 2)dots="...";
        Warning::showWarning(tr("INFO"),tr("Downloading")+dots,2);
    }
}
