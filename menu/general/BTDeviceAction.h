/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef BTDEVICEACTIONMENU_H
#define BTDEVICEACTIONMENU_H

#include <qbluetoothlocaldevice.h>

#include "menu/menulistmodel.h"
#include <QList>
#include <QString>

QT_FORWARD_DECLARE_CLASS(QBluetoothDeviceDiscoveryAgent)
QT_FORWARD_DECLARE_CLASS(QBluetoothDeviceInfo)

QT_USE_NAMESPACE

class BTDeviceActionMenu : public MenuListModel
{
    Q_OBJECT
public:
    BTDeviceActionMenu(MenuListModel *parentMenu = nullptr, QObject *parent = Q_NULLPTR, QString BTMAC="");
    ~BTDeviceActionMenu() Q_DECL_OVERRIDE;

    int rowCount(const QModelIndex &parent) const Q_DECL_OVERRIDE;
    const QString captionForIndex(int index) const Q_DECL_OVERRIDE;
    void before(const MenuDialog &menuDialog) Q_DECL_OVERRIDE;
    MenuType typeForIndex(int index) const Q_DECL_OVERRIDE;

    void execAtIndexNC(int index, MenuDialog &menuDialog) Q_DECL_OVERRIDE;
    MenuListModel *modelForIndex(int index) Q_DECL_OVERRIDE;

    const QString descAtIndex(int index)const Q_DECL_OVERRIDE;
private:
    QBluetoothLocalDevice *localDevice;
    QString device;
    MenuListModel *parentMenuP;

public slots:
    void pairingDone(const QBluetoothAddress&, QBluetoothLocalDevice::Pairing);
};

#endif // BTDEVICE_H
