/*
VarioQt
Copyright (C) 2019  SC TechSwarm SRL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MENULISTMODEL_H
#define MENULISTMODEL_H

#include <QAbstractListModel>

class MenuDialog;

enum MenuType {Back, List, Choice, Caption};

class MenuListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit MenuListModel(MenuListModel *parentMenu = nullptr, QObject *parent = Q_NULLPTR);

    QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;
    virtual MenuType typeForIndex(int index) const =0;

    virtual MenuListModel *modelForIndex(int index);
    virtual void execAtIndex(int index, const MenuDialog &menuDialog);
    virtual void execAtIndexNC(int index, MenuDialog &menuDialog);

    MenuListModel* parentMenu() const;

    virtual void beforeNC(MenuDialog& menuDialog);
    virtual void before(const MenuDialog& menuDialog) =0;

    int savedIndex() const;
    virtual const QString descAtIndex(int index) const;

public slots:
    void rowChanged(const QModelIndex &current, const QModelIndex &previous);

protected:
    virtual const QString captionForIndex(int index) const =0;

private:
    MenuListModel *_parentMenu;

    int _savedIndex = 0;
};

#endif // MENULISTMODEL_H
